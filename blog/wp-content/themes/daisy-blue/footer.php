<?php
/**
 * theme main footer template.
 */
?>



    
</div><!-- .wrap -->
<div id="footer"><footer>
	<div class="container">
		<div class="row">

			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

				<div class="imp-links col-lg-5 col-md-5 col-sm-5 col-xs-12">
					<h4>Important Links</h4>
						<ul>
							<li><a href="https://www.arvenehealthcare.com/cost-comparison">Medical cost of treatments</a></li>
							<li><a href="https://www.arvenehealthcare.com/organ-transplant">Organ Transplant</a></li>
							<li><a href="https://www.arvenehealthcare.com/cardiology-treatment">Cardiac treatments</a></li>
							<li><a href="https://www.arvenehealthcare.com/neuro-spine-surgery">Neuro and spine treatments</a></li>
							<li><a href="https://www.arvenehealthcare.com/orthopaedics-surgery">Knee and hip replacement</a></li>
							<li><a href="https://www.arvenehealthcare.com/http://blog.arvenehealthcare.com">Blog</a></li>
							<li><a href="https://www.arvenehealthcare.com/faq">FAQ</a></li>
						</ul>
				</div>

				<div class="intouch col-lg-4 col-md-4 col-sm-4 col-xs-12">
					<h4>Get in Touch</h4>
					<ul>
						<li><p>
							Arvene Healthcare pvt ltd., <br>
							 Quest Offices, 5th Floor,<br> 
							 Technopolis, Sector 54,<br>
							  Golf course road,
							   Gurgaon 122002.
						</p></li>
						<li><a href="tel:(+91) 81300-77375"><i class="fa fa-phone" aria-hidden="true"></i>(+91) 81300-77375</a></li>
						<li><a href="mailto:contact@arvenehealthcare.com"><i class="fa fa-envelope" aria-hidden="true"></i>contact@arvenehealthcare.com</a></li>
						<li><a href="https://www.arvenehealthcare.com/"><i class="fa fa-skype" aria-hidden="true"></i>Arvene Healthcare</a></li>
					</ul>
				</div>

				<div class="intouch col-lg-3 col-md-3 col-sm-3 col-xs-12">
					<h4>Join us</h4>
					<ul>
						<li><a href="https://twitter.com/Arvene_health"><img src="https://www.arvenehealthcare.com/images/twitter.png">Follow us at Twitter</a></li>
						<li><a href="https://www.facebook.com/Arvenehealthcare/"><img src="https://www.arvenehealthcare.com/images/google.png">Follow us on Facebook</a></li>
					</ul>
				</div>

			</div>
		</div>
	</div>
</footer><!-- footer -->

<section class="footer-btm">
	<div class="container">
		<div class="row">

			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<p>Copyright � 2017 Arvene Healthcare. All Rights Reserved. <a href="https://www.arvenehealthcare.com/privacy-policy">Privacy Policy</a> | <a href="https://www.arvenehealthcare.com/terms-conditions">Terms and conditions</a></p>
			</div>

		</div>
	</div>
</section>

<div class="modal" id="enqueryForm" tabindex="-1" role="dialog"
		aria-labelledby="queryLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="queryLabel">Send Us Your Query</h4>
				</div>
				<div class="modal-body">
					<form class="form-area" id="enquiry_form" method="post"
						action="contactUs">
						<div class="form-group has-feedback">
							<input type="text" name="name" class="form-control"
								placeholder="Enter Your Name"> <i
								class="icon-user form-control-feedback"></i>
						</div>
						<div class="form-group has-feedback">
							<input type="text" name="email" class="form-control"
								placeholder="Enter E-mail ID"> <i
								class="icon-email3 form-control-feedback"></i>
						</div>
						<div class="form-group has-feedback">
							<input type="text" name="phone" class="form-control"
								placeholder="Enter Contact Number"> <i
								class="icon-phone form-control-feedback"></i>
						</div>
						<div class="form-group has-feedback">
							<input type="text" name="country" class="form-control"
								placeholder="Enter Country"> <i
								class="icon-globe4 form-control-feedback"></i>
						</div>
						<div class="form-group has-feedback">
							<textarea rows="8" name="message" class="form-control"
								placeholder="Message"></textarea>
							<i class="icon-comments form-control-feedback"></i>
						</div>
						<div class="form-group">
							<div class="input-group">
								<div class="input-group-addon captcha-num" id="captchaOperation"></div>
								<input type="text" name="captcha" id="captcha"
									class="form-control" placeholder="Solve the Captcha">
								<div class="input-group-addon reset">
									<i class="icon-reply"></i>
								</div>
							</div>
						</div>
						<button type="submit" class="btn btn-sendquery" id="send"
							name="send">Submit</button>
					</form>
				</div>
				<!-- End Modal Body -->
			</div>
		</div>
	</div>
	<!-- Enquire Popup Ends Here -->
	<!-- modal sigin -->
	<div id="signin" class="modal fade" role="dialog">
		<div class="modal-dialog mo-co-dailog">
			<!-- Modal content-->
			<div class="modal-content mo-co-con">
				<div class="modal-header mo-co-head">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Sign In</h4>
				</div>
				<div class="modal-body mo-co-body">
					<div class="form-bot">
						<form name='loginForm' id="loginForm"
							action="<c:url value='/j_spring_security_check' />" method='POST'
							class="center has-validation-callback">
							<div class="form-main in-p contact-mo">
								<div class="form-group has-feedback">
									<input type="text" class="form-control" placeholder="Email ID"
										data-validation="email" name="username"
										data-validation-error-msg="Please enter a valid email address">
								</div>
								<div class="form-group has-feedback">
									<input type="password" class="form-control"
										placeholder="Password" data-validation="length"
										data-validation-length="min8" name="password"
										data-validation-error-msg="Passwords should be atleast 8 characters">
								</div>

								<div class="si-btn">
									<button class="btn btn-block bg-blue" type="submit">Sign
										In</button>
								</div>
								<div class="sig-che">
									Forget Password <a href="https://www.arvenehealthcare.com/forgotPwd">Click here</a>
								</div>
							</div>
						</form>
					</div>
				</div>
				<div class="modal-footer mo-co-foot">
					<div class="form-bot">
						<div class="form-main mo-co">
							<!--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
							<span>Not a member ?</span> <a href="#signup" data-toggle="modal"
								data-dismiss="modal"><button class="btn btn-default">Sign
									Up</button></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- modal sigin END -->

	<!-- SIGNUP START MODAL -->
	<div id="signup" class="modal fade" role="dialog">
		<div class="modal-dialog mo-co-dailog">
			<!-- Modal content-->
			<div class="modal-content mo-co-con">
				<div class="modal-header mo-co-head">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Sign Up</h4>
				</div>
				<div class="modal-body mo-co-body">
					<form name='userForm' action='register' method='POST'
						id="user_sign_up_form" role="form">
						<div class="form-bot">
							<div class="form-main in-p contact-mo">
								<div class="form-group has-feedback">
									<input name="username" type="text" class="form-control"
										placeholder="Email ID" maxlength="30" data-validation="email"
										data-validation-error-msg="Please enter a valid email address" />
								</div>
								<div class="form-group has-feedback">
									<input name="password" type="password" class="form-control"
										placeholder="Password" maxlength="15" data-validation="length"
										data-validation-length="min8"
										data-validation-error-msg="Passwords should be atleast 8 characters" />
								</div>
								<div class="sig-chec">
									<div class="form-group has-feedback">
										<label class="checkbox-inline" style="color: #000;"> <input
											type="checkbox" required name="agree" />By signing up I
											agree to <a onclick="myFunction()">Terms &amp; Conditions</a>

										</label>
									</div>
								</div>
								<div class="si-btn">
									<button class="btn btn-block bg-blue" type="submit">Sign
										Up</button>
								</div>
								<div class="sig-che">
									Already a member? <a id="login-modal" href="#signin"
										data-toggle="modal" data-dismiss="modal">Login here</a>
								</div>
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer mo-co-foot">
					<div class="form-bot">
						<div class="form-main mo-co">
							<!--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
							<a href="https://www.arvenehealthcare.com/specialistRegister" class="btn btn-default">Doctor's
								Sign Up</a>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
	<!-- SIGNUP END MODAL -->
	
	<!-- Div for showing up result -->
	

</div>
<!-- Footer Ends Here -->


<?php wp_footer(); ?>
<!-- Query Success Popup Ends Here -->

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://code.jquery.com/ui/1.11.3/jquery-ui.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="https://www.arvenehealthcare.com/js/bootstrap.min.js"></script>

<script src="https://www.arvenehealthcare.com/js/jquery-ui-timepicker.js"></script>
<script src="https://www.arvenehealthcare.com/js/moment.js"></script>
<script src="https://www.arvenehealthcare.com/js/fullcalendar.js"></script>
<script src="https://www.arvenehealthcare.com/js/bootstrap-tagsinput.js"></script>


<!-- Form Validation -->
<script src="https://www.arvenehealthcare.com/formvalidation/js/formValidation.min.js"></script>
<script src="https://www.arvenehealthcare.com/formvalidation/js/framework/bootstrap.min.js"></script>
<script src="https://www.arvenehealthcare.com/js/form-validation.js"></script>
<!-- Custom Javascript -->
<script src="https://www.arvenehealthcare.com/js/custom.js"></script>

<script>
	$(document).ready(function() {
		
		var mySelect = $('#first-disabled2');

		$('#special').on('click', function() {
			mySelect.find('option:selected').prop('disabled', true);
			mySelect.selectpicker('refresh');
		});

		$('#special2').on('click', function() {
			mySelect.find('option:disabled').prop('disabled', false);
			mySelect.selectpicker('refresh');
		});

		$('#basic2').selectpicker({
			liveSearch : true,
			maxOptions : 1
		});

		popUp();

	});
	</script>
	<script>
	$(document).ready(function() {
		popUp();

	});
	function popUp() {

		if ($("#result").text() != "" && $("#result").text().trim().length > 0) {

			/* $("body").css("opacity", "0.4"); */
			$("#result").show();
			if ("${msg}".length > 0) {
				$('.danger-main2').css("opacity", "1");
				$('.danger-main2').css("z-index", "11111");
				$("#result").css("color", "black");
			}

			else {
				$("#result").css("color", "red");
				$('.danger-main').css("opacity", "1");
				$('.danger-main').css("z-index", "11111");
			}

		} else {
			$("body").css("opacity", "1");
			$("#result").css("display", "none");
		}

		$('.clo').click(function() {
			$('.danger-main').css("opacity", "0");
			$('.danger-main').css("z-index", "-11111");

			$('.danger-main2').css("opacity", "0");
			$('.danger-main2').css("z-index", "-11111");
		});

	};
</script>



<script src="https://www.arvenehealthcare.com/js/bootstrap-select.js"></script>

<script>
	$( document ).ready(function() {
$('#alert').affix({
    offset: {
      top: 20
    , bottom: function () {
        return (this.bottom = $('#footer').outerHeight(true))
      }
    }
  })  
});
jQuery('.dropdown-toggle').click(function() {
			
			jQuery('.dropdown-menu').toggle();
		});
</script>
<!--Start of Zopim Live Chat Script-->
	<script type="text/javascript">
		window.$zopim || (function(d, s) {
			var z = $zopim = function(c) {
				z._.push(c)
			}, $ = z.s = d.createElement(s), e = d.getElementsByTagName(s)[0];
			z.set = function(o) {
				z.set._.push(o)
			};
			z._ = [];
			z.set._ = [];
			$.async = !0;
			$.setAttribute("charset", "utf-8");
			$.src = "//v2.zopim.com/?3sYnlcIsYBxgJlg5Fczo9X0tVM36lxVd";
			z.t = +new Date;
			$.type = "text/javascript";
			e.parentNode.insertBefore($, e)
		})(document, "script");
	</script>
	<!--End of Zopim Live Chat Script-->
	
</body>
</html>
<?php
/**
 * theme main menu
 */
?>
<?php header('Access-Control-Allow-Origin: *'); ?>
<!-- doctype & head section -->
<?php get_template_part('templates/head'); ?>
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta name="google-site-verification"
	content="eubmVPqv0Q4YvZRLYMfdDkp4GF5sjTzMXOvnTPNb5Nc" />
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title>Our Blog | Arvene Healthcare</title>
<meta name="description" content="Visit Arvene Healthcare Blog and read our latest update and news about cancer treatment, knee replacement surgery, kidney transplant surgery and more."/>
<meta name="keywords" content="Best Hospitals in India, Health Tourism in India, Medical Tourism Hospitals in India, Medical Tourism India, Medical Treatment in India"/>
<meta name="robots" content="index, follow"/>
<link href="https://www.arvenehealthcare.com/css/bootstrap.min.css" rel="stylesheet">
<!-- Medical Guide Icons -->
<link rel="stylesheet" href="https://www.arvenehealthcare.com/css/medical-guide-icons.css">
<!-- Medi Guardians Custom Stylesheet -->
<link rel="stylesheet" href="https://www.arvenehealthcare.com/css/medi-guardian.css">
<!-- Media Queries Stylesheet -->
<link rel="stylesheet" href="https://www.arvenehealthcare.com/css/media-queries.css">
<!-- SLIDER REVOLUTION 4.x CSS SETTINGS -->
<link rel="stylesheet" type="text/css" href="https://www.arvenehealthcare.com/rs-plugin/css/settings.css"
	media="screen" />
<link rel="stylesheet" type="text/css"
	href="https://www.arvenehealthcare.com/rs-plugin/css/extralayers.css" media="screen" />
<!-- Animations css -->
<link rel="stylesheet" href="https://www.arvenehealthcare.com/css/animations.css">
<!-- Owl Carousel -->
<link rel="stylesheet" href="https://www.arvenehealthcare.com/css/owl.carousel.css">
<link rel="stylesheet" href="https://www.arvenehealthcare.com/css/owl.theme.css">
<!-- Yamm Megamenu CSS -->
<link rel="stylesheet" href="https://www.arvenehealthcare.com/css/yamm.css">

<link rel="stylesheet" href="https://www.arvenehealthcare.com/css/bootstrap-dialog.css"></link>
<link rel="stylesheet" href="https://www.arvenehealthcare.com/css/fullcalendar.css"></link>
<link rel="stylesheet" href="https://www.arvenehealthcare.com/css/bootstrap-tagsinput.css"></link>

<!-- Form Validation -->
<link rel="stylesheet" href="https://www.arvenehealthcare.com/formvalidation/css/formValidation.min.css" >
<link rel="icon" href="https://www.arvenehealthcare.com/images/favicon.ico" type="image/x-icon">
<link rel="shortcut icon" href="https://www.arvenehealthcare.com/images/favicon.ico" type="image/x-icon">
<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" />
<link rel="stylesheet"
	href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="https://www.arvenehealthcare.com/css/bootstrap-select.css">
<link rel="stylesheet" type="text/css" href="https://www.arvenehealthcare.com/css/font-awesome.css">
<link rel="stylesheet" type="text/css" href="https://www.arvenehealthcare.com/css/bootstrap.css">

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

<!--THIS IS FOR STYLSHEET-->
<link href="https://www.arvenehealthcare.com/css/stylesheet.css" rel="stylesheet">
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
<!-- Enable media queries on older browsers -->
<!--[if lt IE 9]>
      <script src="js/respond.min.js"></script>
    <![endif]-->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="http://www.arvenehealthcare.com/js/bootstrap-paginator.js"></script>
<!--  AUTOCOMPLETE-->
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/jquery.devbridge-autocomplete/1.2.26/jquery.autocomplete.min.js"></script>
<script type="text/javascript">
	function myFunction() {
		window.open(
						"terms-conditions",
						"Terms and Conditions",
						"width=800, height=500,top=100,left=300,location=no,menubar=no,scrollbars=yes,toolbar=no,directories=0,titlebar=0,toolbar=0,location=0,status=0,menubar=0");
	}
	(function($) {
		$(window).load(function() {	
							$('a').filter(
											function() {
												return this.href.match(/.*\.(zip|mp3*|mpe*g|pdf|docx*|pptx*|xlsx*|rar*)(\?.*)?$/);
											}).click(
											function(e) {
												ga('send', 'event', 'download',
														'click', this.href);
											});

							$('a[href^="mailto"]').click(
									function(e) {
										ga('send', 'event', 'email', 'send',
												this.href);
									});

							$('a[href^="http"]').filter(
											function() {
												if (!this.href.match(/.*\.(zip|mp3*|mpe*g|pdf|docx*|pptx*|xlsx*|rar*)(\?.*)?$/)) {
													if (this.href.indexOf('arveneHealthcare.com') == -1)
														return this.href;
												}
											}).click(
											function(e) {
												ga('send', 'event', 'outbound',
														'click', this.href);
											});

							$('a').filter(
											function() {
												if (this.href.indexOf('arveneHealthcare.com') != -1
														|| this.href.indexOf('://') == -1)
													return this.hash;
											}).click(
											function(e) {
												ga('send', 'event', 'hashmark',
														'click', this.href);
											});
						});
	})(jQuery);
</script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-87289439-1', 'auto');
  ga('send', 'pageview');
</script>

<!-- Google Tag Manager -->
<script>
	(function(w, d, s, l, i) {
		w[l] = w[l] || [];
		w[l].push({
			'gtm.start' : new Date().getTime(),
			event : 'gtm.js'
		});
		var f = d.getElementsByTagName(s)[0], j = d.createElement(s), dl = l != 'dataLayer' ? '&l='
				+ l
				: '';
		j.async = true;
		j.src = 'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
		f.parentNode.insertBefore(j, f);
	})(window, document, 'script', 'dataLayer', 'GTM-MWXWB6M');
</script>
<!-- End Google Tag Manager -->
 <meta name="msvalidate.01" content="FC04AB12C3254744FC0A8FBA4488A6A9" />
</head>

<!-- starting body tags - classes -->
<body <?php body_class(); ?>>
<body>

	<!-- Google Tag Manager (noscript) -->
	<noscript>
		<iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MWXWB6M"
			height="0" width="0" style="display: none; visibility: hidden"></iframe>
	</noscript>
	<!-- End Google Tag Manager (noscript) -->

<!-- ***************************Menu Bar section********************** -->

<section class="menu-bar-sec">

		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding"
					id="alert">
					<div class="col-lg-3 col-md-3 col-sm-3 hidden-xs">
						<a href="https://www.arvenehealthcare.com//"><img src="http://www.arvenehealthcare.com/images/logo.png" alt="logo"
							class="img-responsive"></a>
					</div>

					<div class="contact-bar col-lg-7 col-md-9 col-sm-9 col-xs-12">
						<ul>
							<li><a href="tel:(+91) 81300-77375"><i
									class="fa fa-phone" aria-hidden="true"></i>(+91) 81300-77375</a></li>
							<li><a href="mailto:contact@arvenehealthcare.com"><i
									class="fa fa-envelope" aria-hidden="true"></i>contact@arvenehealthcare.com
							</a></li>
							<li>
										<a href="#signup" data-toggle="modal" class="loginbtn">Signup</a><a
								href="#signin" class="loginbtn" data-toggle="modal"
								data-dismiss="modal">Login</a>
								</li>
						</ul>
						<form action="${logoutUrl}" method="post" id="logoutForm">
								<input type="hidden" name="${_csrf.parameterName}"
									value="${_csrf.token}" />
							</form>
					</div>

					<div class="menu-bar col-lg-9 col-md-10 col-sm-12 col-xs-12">


						<nav class="navbar navbar-default list-button">

							<!-- Brand and toggle get grouped for better mobile display -->
							<div class="navbar-header">

								<div class="sendquery">
									<a href="#sendQuery" data-toggle="modal"
										data-target="#enqueryForm" class="querybtn">Send Query</a>
								</div>

								<button type="button" class="navbar-toggle collapsed"
									data-toggle="collapse" data-target="#navbar-collapse-1">
									<span class="sr-only">Toggle navigation</span> <span
										class="icon-bar"></span> <span class="icon-bar"></span> <span
										class="icon-bar"></span>
								</button>
							</div>


							<div class="collapse navbar-collapse" id="navbar-collapse-1">
								<ul class="nav navbar-nav navbar-right">
											<li><a href="#" data-toggle="modal"
												data-target="#enqueryForm" title="">Send Query</a></li>
											<li class="dropdown dropdown-large"><a href="#"
												class="dropdown-toggle" data-toggle="dropdown">Treatments
													<b class="caret"></b>
											</a>

												<ul class="dropdown-menu dropdown-menu-large row">
													<li class="col-sm-3">
														<ul>
															<li><a class="dropdown-header"
																href="https://www.arvenehealthcare.com/cardiology-treatment">Cardiology</a></li>
															<li><a href="https://www.arvenehealthcare.com/coronary-angiography">Coronary
																	Angiography</a></li>
															<li><a href="https://www.arvenehealthcare.com/coronary-angioplasty">Coronary
																	Angioplasty</a></li>
															<li><a href="https://www.arvenehealthcare.com/coronary-artery-bypass-surgery">Coronary
																	Artery Bypass</a></li>
															<li><a href="https://www.arvenehealthcare.com/paediatric-cardiac-surgery">Paediatric
																	Cardiac Surgery</a></li>
															<li><a href="https://www.arvenehealthcare.com/pacemaker-implantation">Pacemaker
																	Implantation</a></li>
															<li><a href="https://www.arvenehealthcare.com/vascular-surgery">Vascular Surgery</a></li>
															<li><a href="https://www.arvenehealthcare.com/heart-valve-replacement-surgery">Heart
																	Valve Replacement </a></li>
															<li><a href="https://www.arvenehealthcare.com/cardiac-diagnostic">Cardiac
																	Diagnostics</a></li>
															<li><a class="dropdown-header"
																href="https://www.arvenehealthcare.com/infertility-treatment">Infertility Treatment</a></li>
															<li><a href="https://www.arvenehealthcare.com/IVF-treatment">IVF</a></li>
															<li><a href="https://www.arvenehealthcare.com/IUI-treatment">IUI</a></li>

														</ul>
													</li>
													<li class="col-sm-3">
														<ul>
															<li><a class="dropdown-header"
																href="https://www.arvenehealthcare.com/cosmetic-surgery">Cosmetic Surgery</a></li>
															<li><a href="https://www.arvenehealthcare.com/breast-augmentation-surgery">Breast
																	Augmentation</a></li>
															<li><a href="https://www.arvenehealthcare.com/breast-reduction-surgery">Breast
																	Reduction</a></li>
															<li><a href="https://www.arvenehealthcare.com/liposuction-surgery">Liposuction</a></li>
															<li><a href="https://www.arvenehealthcare.com/facelift-surgery">Facelift</a></li>
															<li><a href="https://www.arvenehealthcare.com/rhinoplasty-surgery">Rhinoplasty</a></li>
															<li><a href="https://www.arvenehealthcare.com/cosmetic-laser-surgery">Cosmetic
																	Laser Surgery</a></li>
															<li><a href="https://www.arvenehealthcare.com/oral-maxillofacial-surgery">Maxillofacial
																	Surgery</a></li>
															<li><a href="https://www.arvenehealthcare.com/surgical-hair-transplant">Surgical
																	Hair Transplant</a></li>
															<li><a class="dropdown-header"
																href="https://www.arvenehealthcare.com/cancer-treatment">Cancer</a></li>
															<li><a href="https://www.arvenehealthcare.com/breast-cancer-treatment">Breast
																	Cancer</a></li>
															<li><a href="https://www.arvenehealthcare.com/cervical-cancer-treatment">Cervical
																	Cancer</a></li>
															<li><a href="https://www.arvenehealthcare.com/prostate-cancer-treatment">Postate
																	Cancer</a></li>
															<li><a href="https://www.arvenehealthcare.com/colon-cancer-treatment">Colon
																	Cancer</a></li>
															<li><a href="https://www.arvenehealthcare.com/cyberknife-radiation-therapy">Cyberknife
																	Radiation Therapy</a></li>
														</ul>
													</li>
													<li class="col-sm-3">
														<ul>
															<li><a href="https://www.arvenehealthcare.com/neuro-spine-surgery">Neuro and
																	Spine</a></li>
															<li><a class="dropdown-header" href="https://www.arvenehealthcare.com/neuro-surgery">Neurosurgery</a></li>
															<li><a href="https://www.arvenehealthcare.com/neurology">Neurology</a></li>
															<li><a href="https://www.arvenehealthcare.com/brain-tumors-treatment">Brain Tumor</a></li>
															<li><a href="https://www.arvenehealthcare.com/spinal-fusion-surgery">Spinal
																	Fusion</a></li>
															<li><a href="https://www.arvenehealthcare.com/intracranial-aneurysm-surgery">Aneurysms</a></li>
															<li><a href="https://www.arvenehealthcare.com/spinal-tumor-surgery">Spinal Tumor</a></li>
															<li><a href="https://www.arvenehealthcare.com/spinal-laminectomy-surgery">Laminectomy</a></li>
															<li><a class="dropdown-header"
																href="https://www.arvenehealthcare.com/organ-transplant">Organ Transplant</a></li>
															<li><a href="https://www.arvenehealthcare.com/kidney-transplant-surgery">Kidney
																	Transplant Surgery</a></li>
															<li><a href="https://www.arvenehealthcare.com/liver-transplant-surgery">Liver
																	Transplant Surgery</a></li>
															<li><a href="https://www.arvenehealthcare.com/heart-transplant">Heart Transplant</a></li>
															<li><a href="https://www.arvenehealthcare.com/bone-marrow-transplant">Bone Marrow
																	Transplants</a></li>
															<li><a href="https://www.arvenehealthcare.com/human-organs-transplant-laws-india">Organ
																	Transplant laws in India</a></li>
														</ul>
													</li>
													<li class="col-sm-3"><ul>
															<li><a class="dropdown-header"
																href="https://www.arvenehealthcare.com/orthopaedics-surgery">Orthopedic Surgery</a></li>
															<li><a href="https://www.arvenehealthcare.com/knee-replacement-surgery">Knee
																	Replacement</a></li>
															<li><a href="https://www.arvenehealthcare.com/hip-replacement-surgery">Hip
																	Replacement surgery </a></li>
															<li><a href="https://www.arvenehealthcare.com/shoulder-surgery">Shoulder Surgery
															</a></li>
															<li><a href="https://www.arvenehealthcare.com/foot-ankle-surgery">Foot and Ankle
																	surgery</a></li>
															<li><a href="https://www.arvenehealthcare.com/hand-wrist-surgery">Hand Wrist
																	surgery</a></li>
															<li><a href="https://www.arvenehealthcare.com/elbow-replacement-surgery">Elbow
																	Surgery</a></li>
															<li><a class="dropdown-header"
																href="https://www.arvenehealthcare.com/bariatric-surgery">Bariatric Treatment</a></li>
															<li><a href="https://www.arvenehealthcare.com/gastric-bypass-surgery">Gastric
																	Bypass</a></li>
															<li><a href="https://www.arvenehealthcare.com/gastric-sleeve-surgery">Sleeve
																	Gastrectomy </a></li>
															<li><a class="dropdown-header"
																href="https://www.arvenehealthcare.com/urology-treatment">Urology Treatment</a></li>
															<li><a href="https://www.arvenehealthcare.com/endoscopic-surgery">Endoscopy
																	surgery</a></li>
															<li><a href="https://www.arvenehealthcare.com/TURP-surgery">TURF</a></li>
															<li><a href="https://www.arvenehealthcare.com/radical-prostatectomy-surgery">Prostactomy</a></li>
														</ul></li>
												</ul></li>
											<li><a href="https://www.arvenehealthcare.com/medical-providers" title="">Partner
													Hospitals </a></li>
											<li><a href="https://www.arvenehealthcare.com/why-india" title="">Why India</a></li>
											<li><a href="https://www.arvenehealthcare.com/specialists" title="">Our Doctors</a></li>
											<li><a href="http://blog.arvenehealthcare.com" title="">Blog
											</a></li>
											<li><a href="https://www.arvenehealthcare.com/faq" title="">FAQ's</a></li>
											<li><a href="https://www.arvenehealthcare.com/about-us" title="">About Us</a></li>
										
								</ul>
							</div>
						</nav>
					</div>
				</div>
			</div>
		</div>
	<!-- Div for showing up result -END -->
	</section>
	  <!-- theme main wrapper -->
  <div class="wrap container" role="document">
    
    <!-- begining main menu -->
    <?php if ( has_nav_menu( 'primary_navigation' ) ) { ?>
        <nav class="main-menu navbar navbar-default" role="navigation">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
            </div>
            <?php
                wp_nav_menu( array(
                    'theme_location'    => 'primary_navigation',
                    'depth'             => 2,
                    'container'         => 'div',
                    'container_class'   => 'nav-container collapse navbar-collapse',
                    'container_id'      => 'navbar-collapse',
                    'menu_class'        => 'nav navbar-nav',
                    'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                    'walker'            => new wp_bootstrap_navwalker())
                );
            ?>
        </nav> <!-- .main-menu -->
    <?php } ?>

   

    <!-- content section -->
    <div class="content">

        <!-- before main content hook -->
        <?php do_action( 'db_before_main_content' ); ?>

        <!-- main content section -->
        <main class="<?php echo daisyblue_main_class(); ?>" role="main">

package com.medicine.arvene.controller.api;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.medicine.arvene.controller.web.TokBoxController;
import com.medicine.arvene.dao.AvailabilityDao;
import com.medicine.arvene.dao.BookingDao;
import com.medicine.arvene.dao.PaymentDao;
import com.medicine.arvene.dao.QueryDao;
import com.medicine.arvene.dao.SlotDao;
import com.medicine.arvene.dao.SpecialistDao;
import com.medicine.arvene.dao.SpecialistDetailsDao;
import com.medicine.arvene.dao.SpecialityDao;
import com.medicine.arvene.dao.UserDao;
import com.medicine.arvene.model.Booking;
import com.medicine.arvene.model.PaymentDetails;
import com.medicine.arvene.model.Result;
import com.medicine.arvene.model.Slot;
import com.medicine.arvene.model.SpecialistDetails;
import com.medicine.arvene.model.User;
import com.medicine.arvene.model.WeekSchedule;
import com.medicine.arvene.util.CommonUtil;
import com.medicine.arvene.util.Mailer;
import com.medicine.arvene.util.TokBoxSettings;

/**
 * Class that provides a JSON wrapper for all the core functions of the booking
 * 
 *
 */
@Controller
@RequestMapping("/api")
public class ApiBookingController {

	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private BookingDao bookingDao;

	@Autowired
	private UserDao userDao;

	@Autowired
	private SlotDao slotDao;

	@Autowired
	private SpecialistDao specialistDao;

	@Autowired
	private SpecialityDao specialityDao;

	@Autowired
	private SpecialistDetailsDao specialistDetailsDao;
	@Autowired
	private AvailabilityDao availabilityDao;

	@Autowired
	PaymentDao paymentDAO;

	@Autowired
	QueryDao queryDao;

	/**
	 * Method that sets details on a booking when the patient books the slot
	 * 
	 * @param title
	 *            - Meeting title
	 * @param desc
	 *            - A short description
	 * @param slotid
	 *            - The slot id to get marked for booking
	 * @param subscriberid
	 *            - the subscriber user id
	 * @param request
	 * @return the result of the add booking action - success or failure
	 *         response
	 */
	@RequestMapping(value = "/addbooking", method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody Result addBooking(@RequestBody Booking booking,
			HttpServletRequest request) {
		Result result = new Result();
		Session session = CommonUtil.getSession(sessionFactory);
		Slot slot = (Slot) CommonUtil.getEntity(session, Slot.class,
				booking.getSlotid());
		try {
			User user = userDao.findByUserName(request.getUserPrincipal()
					.getName(), session);
			if (slot != null) {
				if (slot.getStatus().equals("A")) {
					booking.setSubscriberid(user.getId());
					booking.setSubscriber(user);
					booking.setSpecialistDetails(specialistDetailsDao
							.fetchSpecialistDetails(session, slot.getUser()));
					booking = bookingDao.addBooking(booking, session);
					if (booking != null) {
						Query query = session
								.createQuery(
										"Update Slot set status =? where id = ?")
								.setParameter(0, "B")
								.setParameter(1, booking.getSlotid());
						query.executeUpdate();
						result.setMsg("Thanks for the Booking!!!");
						result.setData(booking.getId());
						result.setStatus(true);
					} else {
						result.setMsg("Something went wrong, Please try again after sometime");
					}
				} else
					result.setMsg("Slot already Booked");
			} else
				result.setMsg("Invalid Slot details");
		} catch (Exception e) {
			result.setMsg(CommonUtil.exceptionHandler(e));
		} finally {
			session.close();
		}
		return result;
	}

	@RequestMapping(value = "/bookingHistory", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody Result bookingHistory(
			@RequestParam(value = "start", defaultValue = "0", required = false) int start,
			@RequestParam(value = "range", defaultValue = "10", required = false) int range,
			HttpServletRequest request) {
		List<Booking> bookings = null;
		Session session = CommonUtil.getSession(sessionFactory);
		User user = null;
		Result result = new Result();
		try {
			user = userDao.findByUserName(request.getUserPrincipal().getName(),
					session);
			if (user.getUserType().equals("U")) {
				bookings = bookingDao.fetchBookingHistBySubscriber(user,
						session, start, range);
			} else {
				bookings = bookingDao.fetchBookingHistoryByOwner(user, session,
						start, range);
			}
			result.setData(bookings);
			result.setStatus(true);
		} catch (Exception e) {
			result.setMsg(CommonUtil.exceptionHandler(e));
		} finally {
			session.close();
		}
		return result;
	}

	@RequestMapping(value = "/dashboard", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody Result dashboard(HttpServletRequest request) {
		Result result = new Result();
		Session session = CommonUtil.getSession(sessionFactory);
		HashMap<String, String> docDetails = new HashMap<String, String>();
		try {
			User user = null;
			user = userDao.findByUserName(request.getUserPrincipal().getName(),
					session);
			int bookings = bookingDao.fetchBookingCountByOwner(user, session);

			SpecialistDetails specialistDetails = specialistDetailsDao
					.fetchSpecialistDetails(session, user);

			int queries = 0;
			if (specialistDetails != null) {
				queries = queryDao.fetchQueryCountBySpecialist(
						specialistDetails.getId(), session);
				docDetails.put("SpecialistName",
						specialistDetails.getFirstName() + " "
								+ specialistDetails.getLastName());
				docDetails.put("SpecialistImage",
						specialistDetails.getProfileImage());
			}
			docDetails.put("queries", Integer.toString(queries));
			docDetails.put("bookings", Integer.toString(bookings));
			result.setData(docDetails);
			result.setStatus(true);
		} catch (Exception e) {
			result.setMsg(CommonUtil.exceptionHandler(e));
		} finally {
			if (session.isOpen())
				session.close();
		}
		return result;
	};

	/**
	 * Method that returns the list of bookings of both users say Doctor or
	 * Patient
	 * 
	 * @param start
	 *            - starting booking id
	 * @param range
	 *            - number of max bookings
	 * @param request
	 * @return the list of bookings in JSOn format
	 */
	@RequestMapping(value = "/bookings", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody Result fetchBookings(
			@RequestParam(value = "start", defaultValue = "0", required = false) int start,
			@RequestParam(value = "range", defaultValue = "10", required = false) int range,
			HttpServletRequest request) {
		List<Booking> bookings = null;
		Session session = CommonUtil.getSession(sessionFactory);
		User user = null;
		Result result = new Result();
		try {
			user = userDao.findByUserName(request.getUserPrincipal().getName(),
					session);
			if (user.getUserType().equals("U")) {
				bookings = bookingDao.fetchBookingsBySubscriber(user, session,
						start, range);
			} else {
				bookings = bookingDao.fetchBookingsByOwner(user, session,
						start, range);
			}
			result.setData(bookings);
			result.setStatus(true);
		} catch (Exception e) {
			result.setMsg(CommonUtil.exceptionHandler(e));
		} finally {
			session.close();
		}
		return result;
	}

	/**
	 * Method that returns the list of specialities in JSON format
	 * 
	 * @return the list of specialities in JSOn format used for drop down values
	 */
	@RequestMapping(value = "/activeSpecialities", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody Result getActiveSpecialities() {
		Result result = new Result();
		Session session = CommonUtil.getSession(sessionFactory);
		try {
			Map<String, String> specialityValues = specialityDao
					.fetchActiveSpecialities(session);
			result.setData(specialityValues);
			result.setStatus(true);
		} catch (Exception e) {
			result.setMsg(CommonUtil.exceptionHandler(e));
		} finally {
			session.close();
		}
		return result;
	}

	/**
	 * Method that fetches all the available slots of the doctor
	 * 
	 * @param owner
	 *            the doctor id
	 * @param start
	 *            the starting date
	 * @param end
	 *            the ending date
	 * @param request
	 * @return the list of all available slots of the doc during the period
	 */
	@ResponseBody
	@RequestMapping(value = "/fetchavailability", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	public Result getAvailability(@RequestParam("uid") int owner,
			@RequestParam("start") String start,
			@RequestParam("end") String end, HttpServletRequest request) {
		Session session = CommonUtil.getSession(sessionFactory);
		Result result = new Result();
		try {
			List<Slot> slots = slotDao.fetchAvailableSlots(session, owner,
					start, end);
			if (slots == null || slots.size() == 0) {
				result.setMsg("No Slots available");
				result.setStatus(true);
			} else {
				result.setData(slots);
				result.setStatus(true);
			}
		} catch (Exception e) {
			result.setMsg(CommonUtil.exceptionHandler(e));
		} finally {
			session.close();
		}
		return result;
	}

	@RequestMapping(value = "/joinSession", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody Result getSession(@RequestParam int bookingId,
			HttpServletRequest request) {
		Session session = CommonUtil.getSession(sessionFactory);
		Result result = new Result();
		try {
			Booking booking = (Booking) CommonUtil.getEntity(session,
					Booking.class, bookingId);
			User user = userDao.findByUserName(request.getUserPrincipal()
					.getName(), session);
			if (booking != null && booking.getIsActive()
					&& booking.getSlot().getStatus().equals("B")
					&& (booking.getSubscriber().getId() == user.getId())
					|| booking.getSlot().getUser().getId() == user.getId()) {
				Date currentDate = CommonUtil.getIstDateTime();
				if (booking.getSlot().getStartdate().before(currentDate)
						&& booking.getSlot().getEnddate().after(currentDate)) {
					TokBoxSettings tokboxSettings = TokBoxController.getTokBox(
							booking, session, user);
					result.setData(tokboxSettings);
					result.setStatus(true);
				} else
					result.setMsg("This session is not currently happening");
			} else
				result.setMsg("Invalid Booking details");
		} catch (Exception e) {
			result.setMsg(CommonUtil.exceptionHandler(e));
		} finally {
			session.close();
		}
		return result;
	}

	/**
	 * Method that returns the list of specialities in JSON format
	 * 
	 * @return the list of specialities in JSOn format used for drop down values
	 */
	@RequestMapping(value = "/specialities", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody Result getSpecialities() {
		Result result = new Result();
		Session session = CommonUtil.getSession(sessionFactory);
		try {
			Map<String, String> specialityValues = specialityDao
					.fetchSpecialities(session);
			result.setData(specialityValues);
			result.setStatus(true);
		} catch (Exception e) {
			result.setMsg(CommonUtil.exceptionHandler(e));
		} finally {
			session.close();
		}
		return result;
	}

	@Transactional
	@RequestMapping(method = RequestMethod.POST, value = "/savePaymentDetails")
	public @ResponseBody Result processPayment(HttpServletRequest request,
			@RequestBody PaymentDetails paymentDetails,
			HttpServletResponse response) {
		Session session = null;
		Result result = new Result();
		try {
			session = CommonUtil.getSession(sessionFactory);
			paymentDetails.setPGType(paymentDetails.getPg_type());
			int id = paymentDAO.savePaymentDetail(paymentDetails, session);
			if (id > 0) {
				int udfId = Integer.parseInt(paymentDetails.getUdf1()
						.substring(2));
				if (paymentDetails.getUdf1().contains("B")) {
					int bookingId = bookingDao.updatePaymentId(session, udfId,
							paymentDetails);
					if (bookingId > 0) {
						Booking booking = (Booking) CommonUtil.getEntity(
								session, Booking.class, udfId);
						if (paymentDetails.getStatus().equalsIgnoreCase(
								"success")) {
							new Mailer().sendDocBookingMail(booking);
							result.setMsg("Payment Recieved");
							result.setStatus(true);
						} else {
							Query query = session
									.createQuery(
											"Update Slot set status =? where id = ?")
									.setParameter(0, "A")
									.setParameter(1, booking.getSlotid());
							query.executeUpdate();
							result.setMsg("Booking Reverted");
							result.setStatus(true);
						}
					} else
						result.setMsg("Invalid Booking Details");
				} else {
					int queryid = queryDao.updatePaymentId(session, udfId,
							paymentDetails);
					if (paymentDetails.getStatus().equalsIgnoreCase("success")
							&& queryid > 0) {
						com.medicine.arvene.model.Query query = (com.medicine.arvene.model.Query) CommonUtil
								.getEntity(
										session,
										com.medicine.arvene.model.Query.class,
										udfId);
						if (query != null) {
							new Mailer().sendDocQueryMail(query
									.getSpecialistDetails());
							result.setStatus(true);
							result.setMsg("Payment Recieved");
						}
					} else
						result.setMsg("Invalid Query Details");
				}

			}

		} catch (Exception e) {
			result.setMsg(CommonUtil.exceptionHandler(e));
		} finally {
			session.close();
		}
		return result;

	}

	@RequestMapping(value = "/askQuery", method = RequestMethod.POST)
	public @ResponseBody Result processQuery(
			@RequestBody com.medicine.arvene.model.Query query,
			HttpServletRequest request,
			@RequestParam(required = false) byte[] attachment,
			@RequestParam(required = false) String fileName,
			HttpServletResponse response,
			@RequestParam(required = false) boolean shareHistory)
			throws IOException {
		Session session = CommonUtil.getSession(sessionFactory);
		Result result = new Result();
		User user = null;
		try {
			if (attachment != null && attachment.length > 0 && fileName != null) {
				/*
				 * query.setAttachmentUrl(CommonUtil.uploadImage(attachment,
				 * fileName.substring(fileName.lastIndexOf('.') + 1)));
				 */
				// todo attachment
			}
			user = userDao.findByUserName(request.getUserPrincipal().getName(),
					session);
			Integer id = query.getId();
			if (id > 0)
				queryDao.updateQuery(session, query);
			else
				id = queryDao.saveQuery(session, query);
			if (id > 0) {
				result.setMsg("Thanks for the Query!!!");
				result.setStatus(true);
				result.setData(id);
			}
		} catch (Exception e) {
			result.setMsg(CommonUtil.exceptionHandler(e));
		} finally {
			session.close();
		}
		return result;

	}

	@RequestMapping(value = "/queryHistory", method = RequestMethod.GET)
	public @ResponseBody Result queryHistory(HttpServletRequest request,
			@RequestParam int start, @RequestParam int range) {
		Result result = new Result();
		Session session = CommonUtil.getSession(sessionFactory);
		List<com.medicine.arvene.model.Query> queries = null;
		User user = null;

		try {
			user = userDao.findByUserName(request.getUserPrincipal().getName(),
					session);

			if (user != null) {

				if (user.getUserType().equalsIgnoreCase("S")) {
					queries = queryDao.fetchQueryHistoryForSpecialist(
							session,
							specialistDetailsDao.fetchSpecialistDetails(
									session, user).getId(), start, range);
				}
				result.setData(queries);
				result.setStatus(true);
			}
		} catch (Exception e) {

			result.setMsg(CommonUtil.exceptionHandler(e));
		} finally {
			session.close();
		}
		return result;
	}

	@RequestMapping(value = "/queryReply", method = RequestMethod.POST)
	public @ResponseBody Result queryReply(HttpServletRequest request,
			HttpServletResponse response,
			@RequestBody com.medicine.arvene.model.Query query) {
		Result result = new Result();
		Session session = CommonUtil.getSession(sessionFactory);
		try {
			com.medicine.arvene.model.Query querydb = (com.medicine.arvene.model.Query) CommonUtil
					.getEntity(session,
							com.medicine.arvene.model.Query.class,
							query.getId());
			if (querydb != null
					&& userDao.findByUserName(
							request.getUserPrincipal().getName(), session)
							.equals(querydb.getSpecialistDetails().getUser())) {
				/*
				 * if (querydb.getResponse() == null) { int status =
				 * queryDao.addResponse(session, querydb.getId(),
				 * query.getResponse());
				 * 
				 * if (status > 0) { new
				 * Mailer().sendRespQueryMail(querydb.getUser());
				 * result.setMsg("Response Sent to the patient.");
				 * result.setStatus(true); } else
				 * result.setMsg("Please try again later"); } else
				 * result.setMsg("Response Already Sent to the patient");
				 */
				// todo query response
			} else
				result.setMsg("Invalid Data");
		} catch (Exception e) {
			result.setMsg(CommonUtil.exceptionHandler(e));
		} finally {
			session.close();
		}
		return result;
	}

	/**
	 * Method that performs the updation of the availability setting of the
	 * doctor
	 * 
	 * @param sun
	 *            sunday timings
	 * @param mon
	 *            monday timings
	 * @param tue
	 *            tuesday timings
	 * @param wed
	 *            wednesdat timings
	 * @param thu
	 *            thursday timings
	 * @param fri
	 *            friday timings
	 * @param sat
	 *            saturday timings
	 * @param startdate
	 *            startdate
	 * @param enddate
	 *            enddate
	 * @param request
	 * @return the result of the save availability action
	 */
	@RequestMapping(value = "/saveavailability", method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody Result saveAvailability(
			@RequestBody WeekSchedule weekSchedule, HttpServletRequest request) {
		Result result = new Result();
		Session session = CommonUtil.getSession(sessionFactory);
		try {
			User user = null;
			user = userDao.findByUserName(request.getUserPrincipal().getName(),
					session);
			availabilityDao.updateAvailabilityPeriod(user, session,
					weekSchedule.getStartDate(), weekSchedule.getEndDate());
			Query q = session.createQuery(
					"delete Availability where userid = ?").setInteger(0,
					user.getId());
			q.executeUpdate();
			weekSchedule.prepareWeekSessions();
			availabilityDao.generateAvailability(user.getId(),
					weekSchedule.getStartDate(), weekSchedule.getEndDate(),
					weekSchedule.getWeeksessions(), session);
			result.setMsg("Thanks for sharing your availability");
			result.setStatus(true);
		} catch (Exception e) {
			result.setMsg(CommonUtil.exceptionHandler(e));
		} finally {
			if (session.isOpen())
				session.close();
		}
		return result;
	}

	/**
	 * Method that returns the specialists using the speciality id, start and
	 * range
	 * 
	 * @param specialityId
	 *            the speciality id
	 * @param start
	 *            start
	 * @param range
	 *            range
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchspecialists", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	public Result searchSpecialits(
			@RequestParam(value = "specialityid") int specialityId,
			@RequestParam(value = "start", defaultValue = "0", required = false) int start,
			@RequestParam(value = "range", defaultValue = "10", required = false) int range) {
		Session session = CommonUtil.getSession(sessionFactory);
		Result result = new Result();
		try {
			List<SpecialistDetails> specialists = specialistDao
					.fetchSpecialists(session, specialityId, start, range);
			result.setData(specialists);
			result.setStatus(true);
		} catch (Exception e) {
			result.setMsg(CommonUtil.exceptionHandler(e));
		} finally {
			session.close();
		}
		return result;
	}

	/**
	 * Method that returns the availability setting of the doctor
	 * 
	 * @param request
	 * @return the weekschedule availability in JSOn format
	 */
	@RequestMapping(value = "/availability", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody Result showAvailability(HttpServletRequest request) {
		WeekSchedule weekSchedule = null;
		Result result = new Result();
		Session session = CommonUtil.getSession(sessionFactory);
		User user = null;
		try {
			user = userDao.findByUserName(request.getUserPrincipal().getName(),
					session);
			SpecialistDetails specialistDetails = specialistDetailsDao
					.fetchSpecialistDetails(session, user);
			weekSchedule = availabilityDao.getWeekSchedule(user.getId(),
					session, specialistDetails.getAvailstart(),
					specialistDetails.getAvailend());
			result.setData(weekSchedule);
			result.setStatus(true);
		} catch (Exception e) {
			result.setMsg(CommonUtil.exceptionHandler(e));
		} finally {
			session.close();
		}
		return result;
	}

	@RequestMapping(value = "/queries", method = RequestMethod.GET)
	public @ResponseBody Result showQueries(HttpServletRequest request,
			@RequestParam int start, @RequestParam int range,
			@RequestParam int uid) {
		Result result = new Result();
		Session session = CommonUtil.getSession(sessionFactory);
		List<com.medicine.arvene.model.Query> queries = null;
		User user = null;

		try {
			user = userDao.findByUserName(request.getUserPrincipal().getName(),
					session);

			if (user != null) {

				if (user.getUserType().equalsIgnoreCase("S")) {
					queries = queryDao.fetchQueryForSpecialist(
							session,
							specialistDetailsDao.fetchSpecialistDetails(
									session, user).getId(), start, range);
				} else {
					queries = queryDao.fetchQueryForUser(session, uid, start,
							range);
				}
				result.setData(queries);
				result.setStatus(true);
			}
		} catch (Exception e) {

			result.setMsg(CommonUtil.exceptionHandler(e));
		} finally {
			session.close();
		}
		return result;
	}

	@RequestMapping(value = "/specialistDetails", method = RequestMethod.GET)
	public @ResponseBody Result specialistDetails(
			@RequestParam("uid") int ownerid, HttpServletRequest request) {
		Session session = CommonUtil.getSession(sessionFactory);
		Result result = new Result();
		try {
			SpecialistDetails specialist = specialistDetailsDao
					.fetchSpecialistDetails(session, ownerid);
			result.setData(specialist);
			result.setStatus(true);
		} catch (Exception e) {
			result.setMsg(CommonUtil.exceptionHandler(e));
		} finally {
			session.close();
		}
		return result;

	}
}

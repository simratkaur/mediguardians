package com.medicine.arvene.controller.api;

import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.medicine.arvene.dao.UserDao;
import com.medicine.arvene.model.Result;
import com.medicine.arvene.model.User;
import com.medicine.arvene.util.CommonUtil;
import com.medicine.arvene.util.Mailer;

@Controller
@RequestMapping("/api")
public class ApiUserController {
	@Autowired
	UserDao userDao;
	@Autowired
	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public UserDao getUserDao() {
		return userDao;
	}

	@Transactional
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public @ResponseBody Result login(HttpServletRequest request,
			HttpServletResponse response) {
		Result result = new Result();
		Session session = CommonUtil.getSession(sessionFactory);
		try {
			User user = userDao.findByUserName(request.getUserPrincipal()
					.getName(), session);
			result.setData(user);
			result.setStatus(true);
		} catch (Exception e) {
			result.setMsg(CommonUtil.exceptionHandler(e));
		} finally {
			session.close();
		}

		return result;

	}

	@Transactional
	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public @ResponseBody Result register(@RequestBody User user,
			HttpServletRequest request) {
		Result result = new Result();
		Session session = CommonUtil.getSession(sessionFactory);
		try {
			user.setOTP(100000 + new Random().nextInt(900000));
			if (user.getUserType() == null)
				user.setUserType("U");
			userDao.addUser(user, session);
			CommonUtil.sendMsg(
					CommonUtil.getOTPMsgContent(user.getUsername(),
							user.getOTP()), String.valueOf(user.getPhone()));
			result.setMsg("User added successfully!!!!");
			result.setStatus(true);
			result.setData(user.getOTP());
		} catch (Exception e) {
			result.setMsg(CommonUtil.exceptionHandler(e));
		} finally {
			session.close();
		}

		return result;
	}

	@Transactional
	@RequestMapping(method = RequestMethod.GET, value = "/sendOtp")
	public @ResponseBody Result sendOtp(HttpServletRequest request,
			@RequestParam String phone, @RequestParam String userName) {
		Result result = new Result();
		int otp = 100000 + new Random().nextInt(900000);
		try {
			String response = CommonUtil.sendMsg(
					CommonUtil.getOTPMsgContent(userName, otp),
					String.valueOf(phone));
			result.setMsg(response);
			result.setData(otp);
			result.setStatus(true);
		} catch (Exception e) {
			result.setMsg(CommonUtil.exceptionHandler(e));
		}
		return result;
	}

	@Transactional
	@RequestMapping(method = RequestMethod.POST, value = "/forgotPwd")
	public @ResponseBody Result sendPwd(@RequestBody User user,
			HttpServletRequest request, HttpServletResponse response) {
		Result result = new Result();
		Session session = CommonUtil.getSession(sessionFactory);
		try {
			user = userDao.findByUserName(user.getUsername(), session);
			user.setVerificationCode(new BCryptPasswordEncoder().encode(String
					.valueOf(Math.random())));
			userDao.updateUser(user, session);
			new Mailer().sendPwdMail(user, request);
			result.setMsg("Password Reset Mail sent");
			result.setStatus(true);
		} catch (Exception e) {
			result.setMsg(CommonUtil.exceptionHandler(e));
		} finally {
			session.close();
		}

		return result;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/verifyOtp")
	public @ResponseBody Result verifyOtp(HttpServletRequest request,
			@RequestParam String otp, @RequestParam String userName) {
		Result result = new Result();
		Session session = CommonUtil.getSession(sessionFactory);
		try {
			User user = userDao.findByUserName(userName, session);
			if (user.getIsActive())
				result.setMsg("User is already Active");
			else {
				if (user.getOTP() == Integer.valueOf(otp)) {
					user.setIsActive(true);
					userDao.updateUser(user, session);
					//new Mailer().sendUserWelcomeMail(user);
					result.setMsg("OTP verified successfully");
					result.setStatus(true);
				} else
					result.setMsg("OTP does not match");
			}
		} catch (Exception e) {
			result.setMsg(CommonUtil.exceptionHandler(e));
		} finally {
			session.close();
		}
		return result;
	}

}

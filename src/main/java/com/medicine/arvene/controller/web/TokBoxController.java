package com.medicine.arvene.controller.web;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.medicine.arvene.dao.BookingDao;
import com.medicine.arvene.dao.UserDao;
import com.medicine.arvene.model.Booking;
import com.medicine.arvene.model.User;
import com.medicine.arvene.util.CommonUtil;
import com.medicine.arvene.util.TokBoxSettings;
import com.opentok.exception.OpenTokException;

@Controller
public class TokBoxController {

	public static TokBoxSettings getTokBox(Booking booking, Session session,
			User user) throws OpenTokException {
		TokBoxSettings tokboxSettings = new TokBoxSettings();
		tokboxSettings.buildOpenTok();

		if (booking.getToksession() == null) {
			tokboxSettings.setSessionId(TokBoxSettings.createVideoSession()
					.getSessionId());
		} else {
			tokboxSettings.setSessionId(booking.getToksession());
		}

		// Generate a token. Use the Role value appropriate for the
		// user.
		tokboxSettings
				.setExpiry(booking.getSlot().getEnddate().getTime() / 1000);

		tokboxSettings.setToken(TokBoxSettings.createToken(tokboxSettings
				.getSessionId()));

		Query query = null;
		if (user.getUserType().equals("U")) {
			query = session
					.createQuery(
							"Update Booking set toksession = ?,expiry = ?,subtkn = ? where id = ?")
					.setParameter(0, tokboxSettings.getSessionId())
					.setParameter(1, tokboxSettings.getExpiry())
					.setParameter(2, tokboxSettings.getToken())
					.setParameter(3, booking.getId());
			query.executeUpdate();
		} else {
			query = session
					.createQuery(
							"Update Booking set toksession = ?,expiry = ?,pubtkn = ? where id = ?")
					.setParameter(0, tokboxSettings.getSessionId())
					.setParameter(1, tokboxSettings.getExpiry())
					.setParameter(2, tokboxSettings.getToken())
					.setParameter(3, booking.getId());
			query.executeUpdate();
		}
		return tokboxSettings;
	}

	@Autowired
	private SessionFactory sessionFactory;
	@Autowired
	private BookingDao bookingDao;

	@Autowired
	UserDao userDao;

	@RequestMapping(value = "/joinsession", method = RequestMethod.GET)
	public ModelAndView home(@RequestParam("id") int bookingid,
			HttpServletRequest request) throws OpenTokException {
		ModelAndView mav = new ModelAndView();
		HttpSession httpSession = request.getSession(false);
		Booking booking = null;
		Session session = CommonUtil.getSession(sessionFactory);
		if (httpSession != null) {
			booking = (Booking) session.get(Booking.class, bookingid);
			Date currentDate = null;
			try {
				currentDate = CommonUtil.getIstDateTime();
			} catch (Exception e) {
				mav.addObject("error", CommonUtil.exceptionHandler(e));
			}
			if (booking.getSlot().getStartdate().before(currentDate)
					&& booking.getSlot().getEnddate().after(currentDate)) {

				User user = null;
				try {
					user = userDao.findByUserName(request.getUserPrincipal()
							.getName(), CommonUtil.getSession(sessionFactory));
				} catch (Exception e) {
					mav.addObject("error", CommonUtil.exceptionHandler(e));
				}
				if (user.getUserType().equals("U")) {
					if (booking.getPubtkn() == null) {
						mav.addObject("msgtitle", "Patience please..");
						mav.addObject("msg",
								"Specialist hasn't joined the session.Please wait for sometime");
					}
				} else {
					if (booking.getSubtkn() == null) {
						mav.addObject("msgtitle", "Patience please..");
						mav.addObject("msg",
								"Subscriber hasn't joined the session.Please wait for sometime");

					}
				}
				TokBoxSettings tokboxSettings = getTokBox(booking, session,
						user);
				mav.addObject("apikey", tokboxSettings.getApikey());
				mav.addObject("sessionid", tokboxSettings.getSessionId());
				mav.addObject("token", tokboxSettings.getToken());
				mav.addObject("userType", user.getUserType());
				mav.setViewName("Tokboxcall");
			} else {
				mav.addObject("errortitle", "Invalid session");
				mav.addObject("error",
						"This session is not currently happening");
				mav.setViewName("bookings");
			}
		}
		return mav;
	}
}
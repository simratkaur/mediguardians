/**
 * 
 */
package com.medicine.arvene.controller.web;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.amazonaws.util.json.JSONObject;
import com.medicine.arvene.dao.BookingDao;
import com.medicine.arvene.dao.ContactUsDao;
import com.medicine.arvene.dao.NewDocDao;
import com.medicine.arvene.dao.QueryDao;
import com.medicine.arvene.dao.ReportsDao;
import com.medicine.arvene.dao.SlotDao;
import com.medicine.arvene.dao.SpecialistDetailsDao;
import com.medicine.arvene.dao.SpecialityDao;
import com.medicine.arvene.dao.UserDao;
import com.medicine.arvene.model.Booking;
import com.medicine.arvene.model.BookingReport;
import com.medicine.arvene.model.ContactUsForm;
import com.medicine.arvene.model.Conversation;
import com.medicine.arvene.model.Query;
import com.medicine.arvene.model.Report;
import com.medicine.arvene.model.Slot;
import com.medicine.arvene.model.SpecialistDetails;
import com.medicine.arvene.model.User;
import com.medicine.arvene.util.CommonUtil;
import com.medicine.arvene.util.Mailer;

/**
 * @author maddie
 *
 */
@Controller
@RequestMapping
public class WebAdminController {

	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private UserDao userDao;

	@Autowired
	private SpecialistDetailsDao specialistDetailsDao;

	@Autowired
	private ContactUsDao contactUsDao;

	@Autowired
	private NewDocDao docDao;

	@Autowired
	private SpecialityDao specialityDao;

	@Autowired
	private QueryDao queryDao;

	@Autowired
	private BookingDao bookingDao;

	@Autowired
	private SlotDao slotDao;

	@RequestMapping(value = "/updateUserStatus", method = RequestMethod.POST)
	public String updateUserStatus(
			HttpServletRequest request,
			@RequestParam(value = "start", defaultValue = "0", required = false) int start,
			@RequestParam(value = "id") int userId,
			@RequestParam(value = "active") boolean isActive,
			RedirectAttributes redirectAttributes) {

		Session session = CommonUtil.getSession(sessionFactory);

		try {

			int rows = userDao.updateUserStatus(userId, isActive, session);
			if (rows > 0) {
				redirectAttributes.addFlashAttribute("msg",
						"User status successfully updated.");
			} else {
				redirectAttributes
						.addFlashAttribute("error",
								"Something went wrong while updating the user status. Please try again.");
			}

			redirectAttributes.addAttribute("start", start);

		} catch (Exception e) {
			// TODO: handle exception
			redirectAttributes.addFlashAttribute("error",
					CommonUtil.exceptionHandler(e));
		}

		return "redirect:viewUsers";
	}

	@RequestMapping(value = "/acceptBookingRequest", method = RequestMethod.POST)
	public String acceptBookingRequest(
			HttpServletRequest request,
			@RequestParam(value = "start", defaultValue = "0", required = false) int start,
			@RequestParam(value = "bookingId") int bookingId,
			@RequestParam(value = "date") String startTime,
			@RequestParam(value = "time") String time,
			@RequestParam(value = "ownerid") int ownerid,
			@RequestParam(value = "files", required = false) List<MultipartFile> files,
			RedirectAttributes redirectAttributes) {

		Session session = CommonUtil.getSession(sessionFactory);
		Slot slot = null;
		ModelAndView mav = new ModelAndView();

		try {

			Booking booking = (Booking) session.get(Booking.class, bookingId);

			if (booking != null) {

				int slotId = booking.getSlot().getId();
				//
				slot = slotDao.acceptBookingRequest(session, slotId,
						CommonUtil.pattern.parse(startTime), time, ownerid);

				if (files != null && !files.isEmpty()) {
					for (Iterator<MultipartFile> iterator = files.iterator(); iterator
							.hasNext();) {
						MultipartFile attachment = (MultipartFile) iterator
								.next();
						BookingReport report = new BookingReport();
						report.setUrl(CommonUtil.uploadImage(
								attachment.getBytes(),
								attachment.getOriginalFilename().substring(
										attachment.getOriginalFilename()
												.lastIndexOf('.') + 1)));
						report.setBooking(booking);
						report.setUser(booking.getSubscriber());
						report.setFileName(attachment.getOriginalFilename());
						booking.getReports().add(report);
					}

					bookingDao.saveReports(session, booking.getReports());

				}

				if (slot != null) {

					// send mail to doctor and user both informing booking
					// confirmation
					new Mailer().sendDocNewSlotMail(booking);
					new Mailer().sendUserBookingConfirmMail(booking);

					redirectAttributes.addFlashAttribute("msgtitle",
							"Booking sent to doctor!");
					redirectAttributes.addFlashAttribute("msg",
							"The Booking has been  updated.");
				} else {

					redirectAttributes
							.addFlashAttribute(
									"error",
									"Something went wrong while updating the booking. Please check if the slot is not booked.");
				}

				redirectAttributes.addAttribute("start", start);

			}

		} catch (Exception e) {
			mav.addObject("error", CommonUtil.exceptionHandler(e));
		}

		return "redirect:adminBookings";

	}

	@RequestMapping(value = "/adminBookings", method = RequestMethod.POST)
	public ModelAndView adminBookingsFilter(
			@RequestParam(value = "start", defaultValue = "0", required = false) int start,
			@RequestParam(value = "range", defaultValue = "10", required = false) int range,
			@RequestParam(value = "user", defaultValue = "-1", required = false) int userId,
			@RequestParam(value = "specialist", defaultValue = "-1", required = false) int specialistId,
			@RequestParam(value = "status", defaultValue = "", required = false) String status,
			HttpServletRequest request) {
		ModelAndView mav = new ModelAndView("adminBookings");
		HttpSession httpSession = request.getSession(false);
		List<Booking> bookings = null;
		int pagenumber = 0;
		int bookingCount = 0;
		Session session = CommonUtil.getSession(sessionFactory);

		if (httpSession != null) {
			try {

				bookings = bookingDao.fetchBookingsForAdmin(session, start,
						range, userId, specialistId, status);
				bookingCount = bookingDao.fetchBookingsCountForAdmin(session,
						start, range, userId, specialistId, status);

				if (bookingCount % 10 == 0)
					pagenumber = bookingCount / 10;
				else
					pagenumber = (bookingCount / 10) + 1;

				if (pagenumber == 0)
					pagenumber = 1;

				Map<String, String> userMap = userDao.getAllActiveUsers(
						session, "U", "1");
				JSONObject jsonObj = new JSONObject(userMap);
				mav.addObject("usersMap", jsonObj);

				Map<String, String> doctorMap = specialistDetailsDao
						.fetchAllActiveSpecialists(session);
				JSONObject jsonObjDoctors = new JSONObject(doctorMap);
				mav.addObject("specialistMap", jsonObjDoctors);

				mav.addObject("pageno", pagenumber);
				mav.addObject("currentpage", (start / 10) + 1);
				mav.addObject("bookings", bookings);
				httpSession.setAttribute("bookings", bookings);
			} catch (Exception e) {
				mav.addObject("error", CommonUtil.exceptionHandler(e));
			}

		}
		return mav;
	}

	@RequestMapping(value = "/adminBookings", method = RequestMethod.GET)
	public ModelAndView adminBookings(
			@RequestParam(value = "start", defaultValue = "0", required = false) int start,
			@RequestParam(value = "range", defaultValue = "10", required = false) int range,
			HttpServletRequest request) {
		ModelAndView mav = new ModelAndView("adminBookings");
		HttpSession httpSession = request.getSession(false);
		List<Booking> bookings = null;
		int pagenumber = 0;
		int bookingCount = 0;
		Session session = CommonUtil.getSession(sessionFactory);

		if (httpSession != null) {
			try {

				bookings = bookingDao.fetchBookingsForAdmin(session, start,
						range, -1, -1, "");
				bookingCount = bookingDao.fetchBookingsCountForAdmin(session,
						start, range, -1, -1, "");

				if (bookingCount % 10 == 0)
					pagenumber = bookingCount / 10;
				else
					pagenumber = (bookingCount / 10) + 1;

				if (pagenumber == 0)
					pagenumber = 1;

				Map<String, String> userMap = userDao.getAllActiveUsers(
						session, "U", "1");
				JSONObject jsonObj = new JSONObject(userMap);
				mav.addObject("usersMap", jsonObj);

				Map<String, String> doctorMap = specialistDetailsDao
						.fetchAllActiveSpecialists(session);
				JSONObject jsonObjDoctors = new JSONObject(doctorMap);
				mav.addObject("specialistMap", jsonObjDoctors);

				mav.addObject("pageno", pagenumber);
				mav.addObject("currentpage", (start / 10) + 1);
				mav.addObject("bookings", bookings);
				httpSession.setAttribute("bookings", bookings);
			} catch (Exception e) {
				mav.addObject("error", CommonUtil.exceptionHandler(e));
			}

		}
		return mav;
	}

	@RequestMapping(value = "/assignQuery", method = RequestMethod.POST)
	public String assignQueryToSpecialist(
			HttpServletRequest request,
			@RequestParam(value = "start", defaultValue = "0", required = false) int start,
			@RequestParam(value = "query") int queryId,
			@RequestParam(value = "specialist") int specialistId,
			RedirectAttributes redirectAttributes) {

		Session session = CommonUtil.getSession(sessionFactory);
		Query query = null;
		ModelAndView mav = new ModelAndView();

		try {

			query = queryDao.assignQueryToSpacialist(session, queryId,
					specialistId);

			if (query != null) {

				new Mailer().sendDocQueryMail(query.getSpecialistDetails());

				redirectAttributes.addFlashAttribute("msgtitle",
						"Query sent to doctor!");
				redirectAttributes.addFlashAttribute("msg",
						"The Query has been sent to the Specialist.");
			} else {

				redirectAttributes
						.addFlashAttribute(
								"msg",
								"Something went wrong while we were sending the query to the Specialist. Please try again.");
			}

			redirectAttributes.addAttribute("start", start);

		} catch (Exception e) {
			mav.addObject("error", CommonUtil.exceptionHandler(e));
		}

		return "redirect:newQueries";

	}

	@RequestMapping(value = "/viewUsers", method = RequestMethod.GET)
	public ModelAndView loadAllUsers(
			HttpServletRequest request,
			@RequestParam(value = "start", defaultValue = "0", required = false) int start,
			@RequestParam(value = "range", defaultValue = "10", required = false) int range,
			@RequestParam(value = "startDate", required = false) String startDate,
			@RequestParam(value = "endDate", required = false) String endDate,
			@RequestParam(value = "userType", defaultValue = "A", required = false) String userType,
			@RequestParam(value = "active", defaultValue = "-1", required = false) String isActive) {

		int pagenumber = 0;
		Session session = null;
		ModelAndView mav = new ModelAndView("viewUsers");
		List<User> users = new ArrayList<User>();
		Integer totalUsers = 0;

		try {

			session = CommonUtil.getSession(sessionFactory);

			users = userDao.getFilteredUsers(session, startDate, endDate,
					userType, isActive, start, range);
			totalUsers = userDao.getFilteredUsersCount(session, startDate,
					endDate, userType, isActive);
			if (totalUsers % 10 == 0)
				pagenumber = totalUsers / 10;
			else
				pagenumber = (totalUsers / 10) + 1;
			if (pagenumber == 0)
				pagenumber = 1;

			mav.addObject("users", users);

			Map<String, String> userMap = userDao.getAllActiveUsers(session,
					"U", "1");
			JSONObject jsonObj = new JSONObject(userMap);
			mav.addObject("usersMap", jsonObj);

			Map<String, String> doctorMap = specialistDetailsDao
					.fetchAllActiveSpecialists(session);
			JSONObject jsonObjDoctors = new JSONObject(doctorMap);
			mav.addObject("specialistMap", jsonObjDoctors);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		mav.addObject("totalUsers", totalUsers);
		mav.addObject("startDate", startDate);
		mav.addObject("endDate", endDate);
		mav.addObject("userType", userType);
		mav.addObject("active", isActive);
		mav.addObject("pageno", pagenumber);
		mav.addObject("currentpage", (start / 10) + 1);
		mav.addObject("start", start);

		return mav;

	}

	@RequestMapping(value = "/newQueries", method = RequestMethod.GET)
	public ModelAndView getAllQueries(
			HttpServletRequest request,
			@RequestParam(value = "start", defaultValue = "0", required = false) int start,
			@RequestParam(value = "range", defaultValue = "10", required = false) int range) {

		int pagenumber = 0;
		Session session = CommonUtil.getSession(sessionFactory);
		List<Query> queries = null;
		List<User> users = null;
		List<SpecialistDetails> specialists = null;
		ModelAndView mav = new ModelAndView();

		try {

			queries = queryDao.fetchFilteredQueries(session, start, range, -1,
					-1, -1);

			int count = queryDao.fetchFilteredQueriesCount(session, -1, -1, -1);

			if (count % 10 == 0)
				pagenumber = count / 10;
			else
				pagenumber = (count / 10) + 1;
			if (pagenumber == 0)
				pagenumber = 1;

			Map<String, String> userMap = userDao.getAllActiveUsers(session,
					"U", "1");
			JSONObject jsonObj = new JSONObject(userMap);
			mav.addObject("usersMap", jsonObj);

			Map<String, String> doctorMap = specialistDetailsDao
					.fetchAllActiveSpecialists(session);
			JSONObject jsonObjDoctors = new JSONObject(doctorMap);
			mav.addObject("specialistMap", jsonObjDoctors);

		} catch (Exception e) {
			mav.addObject("error", CommonUtil.exceptionHandler(e));
		}

		mav.addObject("queries", queries);
		mav.addObject("pageno", pagenumber);
		mav.addObject("currentpage", (start / 10) + 1);
		mav.addObject("start", start);
		mav.addObject("users", users);
		mav.setViewName("UnrespondedQuery");
		return mav;

	}

	@RequestMapping(value = "/newQueries", method = RequestMethod.POST)
	public ModelAndView filterQueries(
			HttpServletRequest request,
			@RequestParam(value = "start", defaultValue = "0", required = false) int start,
			@RequestParam(value = "range", defaultValue = "10", required = false) int range,
			@RequestParam(value = "user", defaultValue = "-1", required = false) int userId,
			@RequestParam(value = "specialist", defaultValue = "-1", required = false) int specialistId,
			@RequestParam(value = "responded", defaultValue = "-1", required = false) int responded) {

		int pagenumber = 0;
		Session session = CommonUtil.getSession(sessionFactory);
		List<Query> queries = null;
		List<User> users = null;
		List<SpecialistDetails> specialists = null;
		ModelAndView mav = new ModelAndView();

		try {

			queries = queryDao.fetchFilteredQueries(session, start, range,
					userId, specialistId, responded);

			int count = queryDao.fetchFilteredQueriesCount(session, userId,
					specialistId, responded);

			if (count % 10 == 0)
				pagenumber = count / 10;
			else
				pagenumber = (count / 10) + 1;
			if (pagenumber == 0)
				pagenumber = 1;

			Map<String, String> userMap = userDao.getAllActiveUsers(session,
					"U", "1");
			JSONObject jsonObj = new JSONObject(userMap);
			mav.addObject("usersMap", jsonObj);

			Map<String, String> doctorMap = specialistDetailsDao
					.fetchAllActiveSpecialists(session);
			JSONObject jsonObjDoctors = new JSONObject(doctorMap);
			mav.addObject("specialistMap", jsonObjDoctors);

			mav.addObject("query_status", responded);

			if (userId > 0) {
				mav.addObject("search_userId", userId);
				mav.addObject("search_username", userMap.get(userId + ""));

			} else {

				mav.addObject("search_userId", null);
				mav.addObject("search_username", null);
			}
			if (specialistId > 0) {
				mav.addObject("search_specialisId", specialistId);
				mav.addObject("search_specialistname",
						jsonObjDoctors.get(specialistId + ""));

			} else {
				mav.addObject("search_specialisId", null);
				mav.addObject("search_specialistname", null);
			}

		} catch (Exception e) {
			mav.addObject("error", CommonUtil.exceptionHandler(e));
		}

		mav.addObject("queries", queries);
		mav.addObject("pageno", pagenumber);
		mav.addObject("currentpage", (start / 10) + 1);
		mav.addObject("start", start);
		mav.addObject("users", users);
		mav.setViewName("UnrespondedQuery");
		return mav;

	}

	// @RequestMapping(value = "/addNewDoc", method = RequestMethod.POST)
	// public ModelAndView addNewDoc(@ModelAttribute("doc") NewDoc doc)
	// throws Exception {
	// ModelAndView mav = new ModelAndView();
	// Session session = CommonUtil.getSession(sessionFactory);
	//
	// boolean saved = docDao.addUser(doc, session);
	// if (saved)
	// mav.addObject("msg",
	// "Thanks for adding Doctor details, Your Medics Team will revert back asap!!!");
	// else
	// mav.addObject("error", "OOh,We are Sorry.Something went wrong!");
	// mav.setViewName("login");
	// return mav;
	// }

	@RequestMapping(value = "/approveDoctor", method = RequestMethod.POST)
	public ModelAndView approveDoctor(@RequestParam String action,
			@RequestParam int id) {
		Session session = CommonUtil.getSession(sessionFactory);
		ModelAndView model = new ModelAndView("redirect:allSpecialist");
		try {
			switch (action) {
			case "Approve":
				if (userDao.makeActive(id, session) > 0)
					new Mailer().sendDocWelcomeMail((User) CommonUtil
							.getEntity(session, User.class, id));
				break;
			case "Reject":
				specialistDetailsDao.deleteRecord(id, session);
				userDao.deleteRecord(id, session);
				break;
			default:
				break;
			}
		} catch (Exception e) {
			model.addObject("error", CommonUtil.exceptionHandler(e));
		} finally {
			session.close();
		}
		return model;

	}

	@RequestMapping(value = "/archiveDocInquiry", method = RequestMethod.GET)
	public String archiveDocInquiry(HttpServletRequest request,
			HttpServletResponse response, @RequestParam int id,
			RedirectAttributes redirectAttributes) {

		Session session = null;

		try {
			session = CommonUtil.getSession(sessionFactory);
			int status = docDao.archiveInquiry(session, id);
			if (status > 0) {
				redirectAttributes.addFlashAttribute("msg",
						"Doctor Inquiry Archived!");
			}

		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("error",
					CommonUtil.exceptionHandler(e));
		}
		return "redirect:newDocs";
	}

	@RequestMapping(value = "/archiveInquiry", method = RequestMethod.GET)
	public String archiveInquiry(HttpServletRequest request,
			HttpServletResponse response, @RequestParam int id,
			RedirectAttributes redirectAttributes) {

		Session session = null;

		try {
			session = CommonUtil.getSession(sessionFactory);
			int status = contactUsDao.archiveInquiry(session, id);
			if (status > 0) {
				redirectAttributes
						.addFlashAttribute("msg", "Inquiry Archived!");
			}

		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("error",
					CommonUtil.exceptionHandler(e));
		}
		return "redirect:inquiries";
	}

	@RequestMapping(value = "/inquiries", method = RequestMethod.GET)
	public ModelAndView getContactUsInqiries(
			HttpServletRequest request,
			@RequestParam(value = "start", defaultValue = "0", required = false) int start,
			@RequestParam(value = "range", defaultValue = "10", required = false) int range) {

		ModelAndView mav = new ModelAndView();
		Session session = null;
		List<ContactUsForm> inquiries = null;

		try {
			session = CommonUtil.getSession(sessionFactory);
			int pagenumber = contactUsDao.getActiveInquiriesCount(session);
			inquiries = contactUsDao.getAllActiveInquiries(session, start,
					range);

			mav.addObject("pageno", pagenumber);
			mav.addObject("currentpage", (start / 10) + 1);

			mav.addObject("inquiries", inquiries);
			mav.setViewName("adminInquiries");

		} catch (Exception e) {
			mav.addObject("error", CommonUtil.exceptionHandler(e));
		} finally {
			session.close();
		}

		return mav;

	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	@RequestMapping(value = "/viewDoctorProfile", method = RequestMethod.GET)
	public ModelAndView profile(HttpServletRequest request,
			@RequestParam("id") int userId) throws Exception {
		ModelAndView mav = new ModelAndView();
		Session session = CommonUtil.getSession(sessionFactory);

		User user = (User) session.get(User.class, userId);
		SpecialistDetails specialist = specialistDetailsDao
				.fetchSpecialistDetails(session, user);
		mav.addObject("specialist", specialist);
		Map<String, String> specialityValues = specialityDao
				.fetchSpecialities(session);
		mav.addObject("specialities", specialityValues);
		mav.setViewName("viewDoctorProfile");
		return mav;
	}

	@RequestMapping(value = "/inquiryReply", method = RequestMethod.POST)
	public String saveInquryReply(HttpServletRequest request,
			HttpServletResponse response,
			@ModelAttribute ContactUsForm contactUsForm,
			RedirectAttributes redirectAttributes) {

		Session session = null;

		try {
			session = CommonUtil.getSession(sessionFactory);
			ContactUsForm contactUsDB = (ContactUsForm) session.get(
					ContactUsForm.class, contactUsForm.getId());
			contactUsDB.setResponse(contactUsForm.getResponse());
			int status = contactUsDao.respondInquiry(session,
					contactUsForm.getId(), contactUsForm.getResponse());
			if (status > 0) {
				new Mailer().sendInquiryResponse(contactUsDB);
				redirectAttributes.addFlashAttribute("msg", "Response Sent!");
			}

		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("error",
					CommonUtil.exceptionHandler(e));

		}
		return "redirect:inquiries";
	}

	// @RequestMapping(value = "/newDocs", method = RequestMethod.GET)
	// public ModelAndView showAllNewDocss(
	// HttpServletRequest request,
	// @RequestParam(value = "start", defaultValue = "0", required = false) int
	// start,
	// @RequestParam(value = "range", defaultValue = "10", required = false) int
	// range) {
	//
	// ModelAndView mav = new ModelAndView();
	// Session session = null;
	// List<NewDoc> doctorsList = null;
	//
	// try {
	// session = CommonUtil.getSession(sessionFactory);
	// int pages = docDao.getNewDocCount(session);
	// doctorsList = docDao.fetchNewDoctorDetails(session, start, range);
	//
	// mav.addObject("pageno", pages);
	// mav.addObject("start", start);
	//
	// mav.addObject("currentpage", (start / 10) + 1);
	//
	// mav.addObject("doctorsList", doctorsList);
	//
	// mav.setViewName("newDoctors");
	// } catch (Exception e) {
	// mav.addObject("error", CommonUtil.exceptionHandler(e));
	// }
	// return mav;
	// }

	@RequestMapping(value = "/allSpecialist", method = RequestMethod.GET)
	public ModelAndView showAllSpecialists(
			HttpServletRequest request,
			@RequestParam(value = "start", defaultValue = "0", required = false) int start,
			@RequestParam(value = "range", defaultValue = "10", required = false) int range) {

		Session session = CommonUtil.getSession(sessionFactory);
		List<SpecialistDetails> allSpecialists = null;
		ModelAndView mav = new ModelAndView("allSpecialists");
		try {
			allSpecialists = specialistDetailsDao.fetchAllNewSpecialists(
					session, start, range);

			int pagenumber = specialistDetailsDao.getPageCount(session);

			mav.addObject("pageno", pagenumber);
			mav.addObject("currentpage", (start / 10) + 1);
			mav.addObject("activeUsers", userDao.fetchActiveUserCount(session));

			mav.addObject("specialists", allSpecialists);
		} catch (Exception e) {
			mav.addObject("error", CommonUtil.exceptionHandler(e));
		} finally {
			session.close();
		}
		return mav;
	}

	@RequestMapping(value = "/docDetails", method = RequestMethod.GET)
	public ModelAndView viewCalendar(@RequestParam("id") int userId,
			HttpServletRequest request) {

		ModelAndView mav = null;
		Session session = null;

		session = CommonUtil.getSession(sessionFactory);
		try {
			User user = (User) session.get(User.class, userId);
			SpecialistDetails specialistDetails = specialistDetailsDao
					.fetchSpecialistDetails(session, user);
			mav = new ModelAndView("doctorDetails");
			mav.addObject("user", specialistDetails.getUser());
			mav.addObject("specialist", specialistDetails);
			mav.addObject("speciality", specialistDetails.getSpeciality()
					.getName());
		} catch (Exception e) {
			mav.addObject("error", CommonUtil.exceptionHandler(e));
		} finally {
			session.close();
		}
		return mav;
	}

	@RequestMapping(value = "/uploadFiles", method = RequestMethod.POST)
	public ModelAndView uploadFiles(
			HttpServletRequest request,
			@RequestParam(value = "type") String type,
			@RequestParam(value = "id") int id,
			@RequestParam(value = "files", required = false) List<MultipartFile> files,
			RedirectAttributes redirectAttributes) {

		Session session = CommonUtil.getSession(sessionFactory);
		ModelAndView mav = new ModelAndView();
		String url = null;
		String name = null;
		try {
			if (files != null && !files.isEmpty()) {
				for (Iterator<MultipartFile> iterator = files.iterator(); iterator
						.hasNext();) {
					MultipartFile attachment = (MultipartFile) iterator.next();
					url = CommonUtil.uploadImage(
							attachment.getBytes(),
							attachment.getOriginalFilename().substring(
									attachment.getOriginalFilename()
											.lastIndexOf('.') + 1));
					name = attachment.getOriginalFilename();
				}
			}

			if (type.equalsIgnoreCase("query")) {
				Conversation conversation = queryDao.getLastConversation(id,
						session);
				Report report = new Report();
				report.setUrl(url);
				report.setConversation(conversation);
				report.setUser(conversation.getUser());
				report.setFileName(name);
				reportsDao.saveReport(session, report);
				mav.setViewName("redirect:newQueries");
			}
			else{
				Booking booking= (Booking) CommonUtil.getEntity(session, Booking.class, id);
				BookingReport report = new BookingReport();
				report.setUrl(url);
				report.setBooking(booking);
				report.setUser(booking.getSubscriber());
				report.setFileName(name);
				bookingDao.saveReport(session, report);
				mav.setViewName("redirect:adminBookings");
			}
			redirectAttributes.addFlashAttribute("msg","Reports uploaded !!!");
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("error", CommonUtil.exceptionHandler(e));
		}

		return mav;
	}

	@Autowired
	ReportsDao reportsDao;

	public ReportsDao getReportsDao() {
		return reportsDao;
	}

	public void setReportsDao(ReportsDao reportsDao) {
		this.reportsDao = reportsDao;
	}

}

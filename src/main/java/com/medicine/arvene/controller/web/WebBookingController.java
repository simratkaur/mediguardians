package com.medicine.arvene.controller.web;

import java.text.ParseException;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.medicine.arvene.dao.BookingDao;
import com.medicine.arvene.dao.PaymentDao;
import com.medicine.arvene.dao.SlotDao;
import com.medicine.arvene.dao.SpecialistDetailsDao;
import com.medicine.arvene.dao.UserDao;
import com.medicine.arvene.model.Booking;
import com.medicine.arvene.model.BookingReport;
import com.medicine.arvene.model.Event;
import com.medicine.arvene.model.PaymentDetails;
import com.medicine.arvene.model.Slot;
import com.medicine.arvene.model.SpecialistDetails;
import com.medicine.arvene.model.User;
import com.medicine.arvene.util.CommonUtil;
import com.medicine.arvene.util.IPaymentConstants;
import com.medicine.arvene.util.Mailer;
import com.medicine.arvene.util.PaymentUtil;
import com.medicine.arvene.util.SlotUtil;

@Controller
public class WebBookingController {

	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private BookingDao bookingDao;

	@Autowired
	private UserDao userDao;

	@Autowired
	private SlotDao slotDao;

	@Autowired
	private PaymentUtil paymentUtil;

	@Autowired
	SpecialistDetailsDao specialistDetailsDao;

	@Autowired
	PaymentDao paymentDAO;

	@RequestMapping(value = "/SPaypalBooking", method = RequestMethod.POST)
	public ModelAndView processSuccessPaymentForBookingPaypal(
			HttpServletRequest request, HttpServletResponse response) {

		ModelAndView mav = new ModelAndView();
		Session session = CommonUtil.getSession(sessionFactory);
		PaymentDetails paymentDetails = paymentUtil
				.populatePaymentDetailsPaypal(request);
		String udf1 = request
				.getParameter(IPaymentConstants.PAYPAL_PARAM_ITEM_NUMBER);

		int bookingId = Integer.parseInt(udf1.substring(2));

		List<Booking> bookings = null;

		User user = null;

		try {

			user = userDao.findByUserName(request.getUserPrincipal().getName(),
					session);

			Booking booking = (Booking) CommonUtil.getEntity(session,
					Booking.class, bookingId);

			session = CommonUtil.getSession(sessionFactory);
			int id = paymentDAO.savePaymentDetail(paymentDetails, session);
			if (id > 0) {
				paymentDetails = (PaymentDetails) session.get(
						PaymentDetails.class, id);
				bookingDao.updatePaymentId(session, bookingId, paymentDetails);

				if (paymentDetails.getStatus().equalsIgnoreCase(
						IPaymentConstants.PAYPAL_PAYMENT_STATUS_COMPLETED)) {

					if (booking.getSlot().getStatus().equalsIgnoreCase("R")) {

						// send it to admin
						new Mailer().sendAdminQueryMail();
						mav.addObject("msgtitle", "Query received!");
						mav.addObject(
								"msg",
								"Thank you.Your Booking Request has been received. We will send it to the Specialist.");

					} else {

						new Mailer().sendDocBookingMail(booking);

						mav.addObject("msgtitle", "Booking sent to doctor!");
						mav.addObject("msg",
								"Your Booking Details has been sent to the Specialist.");
					}

				} else {

					// mark the slot as available
					Query query = session
							.createQuery(
									"Update Slot set status =? where id = ?")
							.setParameter(0, "A")
							.setParameter(1, booking.getSlotid());
					query.executeUpdate();

					mav.addObject("errortitle",
							"Query could not be sent to doctor!");
					mav.addObject(
							"error",
							"Something went wrong while we were sending your booking to the Specialist. we regret for the "
									+ "inconvenience caused");
				}

				bookings = bookingDao.fetchBookings(user, session);

			} else {

				// mark the slot as available
				Query query = session
						.createQuery("Update Slot set status =? where id = ?")
						.setParameter(0, "A")
						.setParameter(1, booking.getSlotid());
				query.executeUpdate();

			}

		} catch (Exception e) {
			mav.addObject("error", CommonUtil.exceptionHandler(e));
		}

		mav.addObject("bookings", bookings);
		mav.setViewName("bookings");

		return mav;

	}

	// make entry in Slot with status R
	// make entry in booking
	// Redirect to payment

	@RequestMapping(value = "/requestBooking", method = RequestMethod.POST)
	public String requestBooking(@RequestParam("uid") int ownerid,
			@RequestParam("title") String title,
			@RequestParam("desc") String desc,
			@RequestParam("phone") String phone,
			@RequestParam("files") List<MultipartFile> files,
			HttpServletRequest request, HttpServletResponse response,
			RedirectAttributes redirectAttributes, Model model) {

		Session session = CommonUtil.getSession(sessionFactory);
		String redirectUrl = "viewcalendar";
		User user;
		Booking booking = null;

		try {

			Slot slot = new Slot();
			slot.setOwnerid(ownerid);
			slot.setStatus("R");

			slot = slotDao.saveSlot(session, slot);
			if (slot.getId() > 0) {

				booking = new Booking();
				booking.setTitle(title);
				booking.setDesc(desc);
				booking.setPhone(phone);
				booking.setSlot(slot);
				booking.setSlotid(slot.getId());

				SpecialistDetails specialist = specialistDetailsDao
						.fetchSpecialistDetails(session, booking.getSlot()
								.getUser());

				booking.setSpecialistDetails(specialist);
				booking.setFiles(files);

				if (request.getSession().getAttribute("sessionBooking") == null) {
					request.getSession()
							.setAttribute("sessionBooking", booking);
				}

				if (request.getUserPrincipal() != null) {

					user = userDao.findByUserName(request.getUserPrincipal()
							.getName(), session);

					booking.setSubscriberid(user.getId());

					booking = bookingDao.addBooking(booking, session);
					if (booking != null) {

						if (files != null && !files.isEmpty()) {
							for (Iterator<MultipartFile> iterator = files
									.iterator(); iterator.hasNext();) {
								MultipartFile attachment = (MultipartFile) iterator
										.next();
								BookingReport report = new BookingReport();
								report.setUrl(CommonUtil.uploadImage(
										attachment.getBytes(),
										attachment
												.getOriginalFilename()
												.substring(
														attachment
																.getOriginalFilename()
																.lastIndexOf(
																		'.') + 1)));
								report.setBooking(booking);
								report.setUser(user);
								report.setFileName(attachment
										.getOriginalFilename());
								booking.getReports().add(report);
							}

							bookingDao.saveReports(session,
									booking.getReports());

						}

						model.addAttribute("user", user);
						// SpecialistDetails owner = specialistDetailsDao
						// .fetchSpecialistDetails(session, booking.getSlot()
						// .getOwnerid());
						redirectAttributes.addAttribute("fee",
								specialist.getSessionFee());
						redirectAttributes.addAttribute("id", booking.getId());
						redirectAttributes.addAttribute("isQuery", false);
						redirectUrl = "redirect:queryPayment";
						new Mailer().sendAdminBookingRequestMail(booking);
					}

				} else {

					redirectAttributes.addAttribute("uid", ownerid);
					redirectAttributes.addFlashAttribute("error",
							"Please login or Register to get Booking!");
					redirectUrl = "redirect:viewcalendar";

				}

			}

		} catch (Exception e) {
			model.addAttribute("error", CommonUtil.exceptionHandler(e));
		}

		return redirectUrl;
	}

	@RequestMapping(value = "/addbookinginfo", method = RequestMethod.POST)
	public String addBooking(@RequestParam("title") String title,
			@RequestParam("desc") String desc,
			@RequestParam("slotid") int slotid,
			@RequestParam("phone") String phone,
			@RequestParam("files") List<MultipartFile> files,
			HttpServletRequest request, RedirectAttributes redirectAttributes,
			Model model) {
		Session session = CommonUtil.getSession(sessionFactory);
		User user = null;
		Booking booking = null;

		Slot slot = (Slot) session.get(Slot.class, slotid);
		booking = new Booking();
		booking.setTitle(title);
		booking.setDesc(desc);
		booking.setSlotid(slotid);
		booking.setSlot(slot);
		booking.setPhone(phone);
		booking.setFiles(files);
		String redirectUrl = "addbookinginfo";
		if (request.getSession().getAttribute("sessionBooking") == null) {
			request.getSession().setAttribute("sessionBooking", booking);
		}
		if (request.getUserPrincipal() != null) {
			try {
				user = userDao.findByUserName(request.getUserPrincipal()
						.getName(), session);
				booking.setSubscriberid(user.getId());
				booking.setSpecialistDetails(specialistDetailsDao
						.fetchSpecialistDetails(session, booking.getSlot()
								.getUser()));
				booking = bookingDao.addBooking(booking, session);
				if (booking != null) {

					if (files != null && !files.isEmpty()) {
						for (Iterator<MultipartFile> iterator = files
								.iterator(); iterator.hasNext();) {
							MultipartFile attachment = (MultipartFile) iterator
									.next();
							BookingReport report = new BookingReport();
							report.setUrl(CommonUtil.uploadImage(
									attachment.getBytes(),
									attachment.getOriginalFilename().substring(
											attachment.getOriginalFilename()
													.lastIndexOf('.') + 1)));
							report.setBooking(booking);
							report.setUser(user);
							report.setFileName(attachment.getOriginalFilename());
							booking.getReports().add(report);
						}

						bookingDao.saveReports(session, booking.getReports());

					}

					Query query = session
							.createQuery(
									"Update Slot set status =? where id = ?")
							.setParameter(0, "B").setParameter(1, slotid);
					query.executeUpdate();
				} else {
					model.addAttribute("error",
							"Something went wrong while we were booking your session. we regret for the"
									+ "inconvenience caused");
				}

			} catch (Exception e) {
				model.addAttribute("error", CommonUtil.exceptionHandler(e));
			}
			model.addAttribute("user", user);
			SpecialistDetails owner = specialistDetailsDao
					.fetchSpecialistDetails(session, booking.getSlot()
							.getOwnerid());
			redirectAttributes.addAttribute("fee", owner.getSessionFee());
			redirectAttributes.addAttribute("id", booking.getId());
			redirectAttributes.addAttribute("isQuery", false);
			redirectUrl = "redirect:queryPayment";
		} else {
			redirectAttributes.addAttribute("uid", slot.getOwnerid());
			redirectAttributes.addFlashAttribute("error",
					"Please login or Register to get Booking!");
			redirectUrl = "redirect:viewcalendar";
		}
		return redirectUrl;
	}

	@ResponseBody
	@RequestMapping(value = "/fetchavailability", method = RequestMethod.GET)
	public List<Event> getAvailability(@RequestParam("uid") int owner,
			@RequestParam("start") String start,
			@RequestParam("end") String end, HttpServletRequest request)
			throws ParseException {
		HttpSession httpSession = request.getSession(false);
		if (httpSession != null) {
			Session session = CommonUtil.getSession(sessionFactory);
			List<Slot> slots = slotDao.fetchSlots(session, owner, start, end);
			if (slots == null || slots.size() == 0)
				return null;
			return SlotUtil.createEventsForSlots(slots);
		}
		return null;
	}

	@RequestMapping(value = "/bookingsHistory", method = RequestMethod.GET)
	public ModelAndView getBookingsHistory(
			@RequestParam(value = "start", defaultValue = "0", required = false) int start,
			@RequestParam(value = "range", defaultValue = "10", required = false) int range,
			HttpServletRequest request) {
		ModelAndView mav = new ModelAndView("bookingsHistory");
		HttpSession httpSession = request.getSession(false);
		List<Booking> bookings = null;
		int pagenumber = 0;
		Session session = CommonUtil.getSession(sessionFactory);

		if (httpSession != null) {
			try {
				User user = userDao.findByUserName(request.getUserPrincipal()
						.getName(), session);
				if (user.getUserType().equals("U")) {
					bookings = bookingDao.fetchBookingHistBySubscriber(user,
							session, start, range);
					mav.addObject("viewtype", "userview");
					pagenumber = bookingDao.getPageCount(session, 1, user);
				} else {
					bookings = bookingDao.fetchBookingHistoryByOwner(user,
							session, start, range);
					mav.addObject("viewtype", "docview");
					pagenumber = bookingDao.getPageCount(session, 2, user);
				}
				mav.addObject("user", user);
				mav.addObject("pageno", pagenumber);
				mav.addObject("currentpage", (start / 10) + 1);
				mav.addObject("bookings", bookings);
				httpSession.setAttribute("bookings", bookings);
			} catch (Exception e) {
				mav.addObject("error", CommonUtil.exceptionHandler(e));
			}

		}
		return mav;
	}

	@ResponseBody
	@RequestMapping(value = "/fetchbookings", method = RequestMethod.GET)
	public List<Event> getEventBookings(HttpServletRequest request) {
		HttpSession httpSession = request.getSession(false);
		if (httpSession != null) {
			Session session = CommonUtil.getSession(sessionFactory);

			User user;
			try {
				user = userDao.findByUserName(request.getUserPrincipal()
						.getName(), session);
				List<Booking> bookings = bookingDao
						.fetchBookings(user, session);
				if (bookings == null || bookings.size() == 0)
					return null;

				return SlotUtil.createEventsForBookings(bookings);
			} catch (Exception e) {
				e.printStackTrace();
				// mav.addObject("error",CommonUtil.exceptionHandler(e));
			}

		}
		return null;
	}

	@RequestMapping(value = "/bookings", method = RequestMethod.GET)
	public ModelAndView home(
			@RequestParam(value = "start", defaultValue = "0", required = false) int start,
			@RequestParam(value = "range", defaultValue = "10", required = false) int range,
			HttpServletRequest request) {
		ModelAndView mav = new ModelAndView("bookings");
		HttpSession httpSession = request.getSession(false);
		List<Booking> bookings = null;
		int pagenumber = 0;
		Session session = CommonUtil.getSession(sessionFactory);
		
		List<Booking>  bookingRequest= null;
		
		if (httpSession != null) {
			try {
				User user = userDao.findByUserName(request.getUserPrincipal()
						.getName(), session);
				if (user.getUserType().equals("U")) {
					bookings = bookingDao.fetchBookingsBySubscriber(user,
							session, start, range);
					
					bookingRequest  = bookingDao.fetchBookingRequestForSubscriber(user, session);
				
					mav.addObject("viewtype", "userview");
					mav.addObject("bookingRequests", bookingRequest);
					pagenumber = bookingDao.getPageCount(session, 1, user);
				} else {
					bookings = bookingDao.fetchBookingsByOwner(user, session,
							start, range);
					mav.addObject("viewtype", "docview");
					pagenumber = bookingDao.getPageCount(session, 2, user);
				}
				mav.addObject("user", user);
				mav.addObject("pageno", pagenumber);
				mav.addObject("currentpage", (start / 10) + 1);
				mav.addObject("bookings", bookings);
				httpSession.setAttribute("bookings", bookings);
			} catch (Exception e) {
				mav.addObject("error", CommonUtil.exceptionHandler(e));
			}

		}
		return mav;
	}

	@RequestMapping(value = "/SPaymentBooking", method = RequestMethod.POST)
	public ModelAndView processSuccessPaymentForBooking(
			HttpServletRequest request, HttpServletResponse response) {

		ModelAndView mav = new ModelAndView();
		Session session = CommonUtil.getSession(sessionFactory);
		PaymentDetails paymentDetails = paymentUtil
				.populatePaymentDetails(request);
		String udf1 = request
				.getParameter(IPaymentConstants.PAYPAL_PARAM_ITEM_NUMBER);

		int bookingId = Integer.parseInt(udf1.substring(2));

		List<Booking> bookings = null;

		User user = null;

		try {

			user = userDao.findByUserName(request.getUserPrincipal().getName(),
					session);

			Booking booking = (Booking) CommonUtil.getEntity(session,
					Booking.class, bookingId);

			session = CommonUtil.getSession(sessionFactory);
			int id = paymentDAO.savePaymentDetail(paymentDetails, session);
			if (id > 0) {
				paymentDetails = (PaymentDetails) session.get(
						PaymentDetails.class, id);
				bookingDao.updatePaymentId(session, bookingId, paymentDetails);

				if (paymentDetails.getStatus().equalsIgnoreCase("success")) {

					new Mailer().sendDocBookingMail(booking);

					mav.addObject("msgtitle", "Booking sent to doctor!");
					mav.addObject("msg",
							"Your Booking Details has been sent to the Specialist.");

				} else {

					// mark the slot as available
					Query query = session
							.createQuery(
									"Update Slot set status =? where id = ?")
							.setParameter(0, "A")
							.setParameter(1, booking.getSlotid());
					query.executeUpdate();

					mav.addObject("errortitle",
							"Query could not be sent to doctor!");
					mav.addObject(
							"error",
							"Something went wrong while we were sending your booking to the Specialist. we regret for the "
									+ "inconvenience caused");
				}

				bookings = bookingDao.fetchBookings(user, session);

			} else {

				// mark the slot as available
				Query query = session
						.createQuery("Update Slot set status =? where id = ?")
						.setParameter(0, "A")
						.setParameter(1, booking.getSlotid());
				query.executeUpdate();

			}

		} catch (Exception e) {
			mav.addObject("error", CommonUtil.exceptionHandler(e));
		}

		mav.addObject("bookings", bookings);
		mav.setViewName("bookings");

		return mav;

	}

	@RequestMapping(value = "/viewcalendar", method = RequestMethod.GET)
	public ModelAndView viewCalendar(
			@RequestParam("uid") int ownerid,
			@RequestParam(value = "bookingId", defaultValue = "-1", required = false) int bookingId,
			HttpServletRequest request) {
		HttpSession httpSession = request.getSession(false);
		if (httpSession != null) {
			Session session = CommonUtil.getSession(sessionFactory);
			User user = null;
			ModelAndView mav = new ModelAndView();
			try {
				if (request.getUserPrincipal() != null)
					user = userDao.findByUserName(request.getUserPrincipal()
							.getName(), session);
				User owner = (User) CommonUtil.getEntity(session, User.class,
						ownerid);
				SpecialistDetails specialist = specialistDetailsDao
						.fetchSpecialistDetails(session, ownerid);
				mav.addObject("owner", owner);
				mav.addObject("user", user);
				mav.addObject("uid", ownerid);
				mav.addObject("bookingId", bookingId);
				mav.addObject("specialist", specialist);
				mav.setViewName("viewcalendar");
			} catch (Exception e) {
				mav.addObject("error", CommonUtil.exceptionHandler(e));
			}
			return mav;

		}
		return null;
	}

	@RequestMapping(value = "/viewcalendars", method = RequestMethod.GET)
	public ModelAndView viewCalendarSpecialist(HttpServletRequest request) {
		HttpSession httpSession = request.getSession(false);
		if (httpSession != null) {
			Session session = CommonUtil.getSession(sessionFactory);
			User user;
			ModelAndView mav = new ModelAndView();
			try {
				user = userDao.findByUserName(request.getUserPrincipal()
						.getName(), session);
				mav.addObject("user", user);
				mav.setViewName("viewcalendar");
				mav.addObject("uid", user.getId());

				request.setAttribute("uid", user.getId());
			} catch (Exception e) {
				mav.addObject("error", CommonUtil.exceptionHandler(e));
			}
			return mav;

		}
		return null;
	}
}

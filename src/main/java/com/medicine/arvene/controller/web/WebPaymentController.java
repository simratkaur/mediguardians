/**
 * 
 */
package com.medicine.arvene.controller.web;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Enumeration;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.medicine.arvene.dao.PaymentDao;
import com.medicine.arvene.model.PaymentDetails;
import com.medicine.arvene.util.CommonUtil;
import com.medicine.arvene.util.IPaymentConstants;

/**
 * @author maddie
 *
 */

@Controller
public class WebPaymentController {

	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private PaymentDao paymentDAO;

	/**
	 * 
	 * 
	 * @param paymentForm
	 * @return String like has
	 * 
	 *         JBZaLc|148b5c19edb60a323403|100|prod|madhu|madhulika@abc.com||148
	 *         b5c19edb60a323403|||||||||GQs7yium
	 *         JBZaLc|13845141931447833603|100.0
	 *         |your-medics_product||madhulika@abc.com|||||||||||GQs7yium
	 * @throws NoSuchAlgorithmException
	 */

	private String gethashString(PaymentDetails paymentForm)
			throws NoSuchAlgorithmException {

		String hashString = "";
		String hash = "";

		String hashSequence = paymentForm.getReqHashSequence();
		String[] hashVarSeq = hashSequence.split("\\|");

		for (String part : hashVarSeq) {
			String hashVar = "";

			switch (part) {
			case IPaymentConstants.PARAM_KEY:
				hashVar = paymentForm.getKey();
				break;

			case "txnid":
				hashVar = paymentForm.getTransactionId();
				break;

			case "amount":
				hashVar = String.valueOf(paymentForm.getAmount());
				break;

			case "productinfo":
				hashVar = paymentForm.getProductInfo();
				break;

			case "firstname":
				hashVar = paymentForm.getFirstName();
				break;

			case "email":
				hashVar = paymentForm.getEmail();
				break;

			case "udf1":
				hashVar = "";
				break;

			case "udf2":
				hashVar = "";
				break;

			case "udf3":
				hashVar = "";
				break;

			case "udf4":
				hashVar = "";
				break;

			case "udf5":
				hashVar = "";
				break;

			case "udf6":
				hashVar = "";
				break;

			case "udf7":
				hashVar = "";
				break;

			case "udf8":
				hashVar = "";
				break;

			case "udf9":
				hashVar = "";
				break;

			case "udf10":
				hashVar = "";
				break;

			default:
				break;

			}

			hashString = hashString.concat(hashVar);
			hashString = hashString.concat("|");

		}

		hashString = hashString.concat(paymentForm.getSalt());

		hash = hashCal(paymentForm.getHashAlgo(), hashString);

		return hash;

	}

	private String getNewTxnId() throws NoSuchAlgorithmException {
		Random rand = new Random();
		String rndm = Integer.toString(rand.nextInt())
				+ (System.currentTimeMillis() / 1000L);
		String txnid = hashCal("SHA-256", rndm).substring(0, 20);
		return txnid;
	}

	public String hashCal(String type, String str)
			throws NoSuchAlgorithmException {
		byte[] hashseq = str.getBytes();
		StringBuffer hexString = new StringBuffer();
		MessageDigest algorithm = MessageDigest.getInstance(type);
		algorithm.reset();
		algorithm.update(hashseq);
		byte messageDigest[] = algorithm.digest();

		for (int i = 0; i < messageDigest.length; i++) {
			String hex = Integer.toHexString(0xFF & messageDigest[i]);
			if (hex.length() == 1)
				hexString.append("0");
			hexString.append(hex);
		}

		return hexString.toString();

	}

	private PaymentDetails populatePaymentDetails(HttpServletRequest request) {

		PaymentDetails paymentDetails = new PaymentDetails();

		paymentDetails.setTransactionId(request
				.getParameter(IPaymentConstants.PARAM_TRANSACTION_ID));
		paymentDetails.setPayuMihpayId(request
				.getParameter(IPaymentConstants.PARAM_PAYU_ID));
		paymentDetails.setEmail(request
				.getParameter(IPaymentConstants.PARAM_EMAIL));
		paymentDetails.setFirstName(request
				.getParameter(IPaymentConstants.PARAM_FIRST_NAME));
		paymentDetails.setLastName(request
				.getParameter(IPaymentConstants.PARAM_LAST_NAME));
		paymentDetails.setPhone(request
				.getParameter(IPaymentConstants.PARAM_PHONE));
		paymentDetails.setPaymentMode(request
				.getParameter(IPaymentConstants.PARAM_MODE));
		paymentDetails.setStatus(request
				.getParameter(IPaymentConstants.PARAM_STATUS));
		paymentDetails.setAmount(Float.parseFloat(request
				.getParameter(IPaymentConstants.PARAM_AMOUNT)));
		paymentDetails.setDiscount(Float.parseFloat(request
				.getParameter(IPaymentConstants.PARAM_DISCOUNT)));
		paymentDetails.setNetAmountDebited(Float.parseFloat(request
				.getParameter(IPaymentConstants.PARAM_NET_AMOUNT)));
		paymentDetails.setPayuStatus(request
				.getParameter(IPaymentConstants.PARAM_PAYU_STATUS));
		paymentDetails.setPGType(request
				.getParameter(IPaymentConstants.PARAM_PG_TYPE));
		paymentDetails.setBankRefNum(request
				.getParameter(IPaymentConstants.PARAM_BANK_REF_NO));
		paymentDetails.setErrorCode(request
				.getParameter(IPaymentConstants.PARAM_ERR_CODE));
		paymentDetails.setErrorMessage(request
				.getParameter(IPaymentConstants.PARAM_ERR_DESC));
		paymentDetails.setTxnDate(request
				.getParameter(IPaymentConstants.PARAM_DATE_ADDED));

		return paymentDetails;

	}

	@RequestMapping(method = RequestMethod.POST, value = "/paymentFailure")
	public ModelAndView processFailurePayment(
			@ModelAttribute("paymentForm") PaymentDetails paymentForm,
			HttpServletRequest request, HttpServletResponse response) {

		Session session = null;
		Enumeration<String> paramNames = request.getParameterNames();

		while (paramNames.hasMoreElements()) {
			String paramName = paramNames.nextElement();
			String paramValue = request.getParameter(paramName);
		}

		PaymentDetails paymentDetails = populatePaymentDetails(request);

		ModelAndView model = new ModelAndView("paymentFailure");
		try {

			session = CommonUtil.getSession(sessionFactory);
			int id = paymentDAO.savePaymentDetail(paymentDetails, session);

		} catch (Exception e) {
			model.addObject("error", CommonUtil.exceptionHandler(e));
		}

		model.addObject("status",
				request.getParameter(IPaymentConstants.PARAM_STATUS));
		model.addObject("error",
				request.getParameter(IPaymentConstants.PARAM_ERR_DESC));

		return model;

	}

	/**
	 * 
	 * @param model
	 * @return
	 * 
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/payment")
	public ModelAndView processPaymentForm(
			@ModelAttribute("paymentForm") PaymentDetails paymentForm,
			HttpServletRequest request, HttpServletResponse response) {

		ModelAndView model = new ModelAndView("processPayment");

		try {
			paymentForm.setTransactionId(getNewTxnId());
			paymentForm.setChecksum(gethashString(paymentForm));
		} catch (NoSuchAlgorithmException e) {
			model.addObject("error", CommonUtil.exceptionHandler(e));
		}

		model.addObject("paymentForm", paymentForm);
		return model;
	}

	@Transactional
	@RequestMapping(method = RequestMethod.POST, value = "/paymentSuccess")
	public ModelAndView processSuccessPayment(HttpServletRequest request,
			HttpServletResponse response) {

		Session session = null;
		PaymentDetails paymentDetails = populatePaymentDetails(request);
		ModelAndView model = new ModelAndView("paymentSuccess");

		try {

			session = CommonUtil.getSession(sessionFactory);
			int id = paymentDAO.savePaymentDetail(paymentDetails, session);

		} catch (Exception e) {
			model.addObject("error", CommonUtil.exceptionHandler(e));
		}

		model.addObject("status",
				request.getParameter(IPaymentConstants.PARAM_STATUS));

		return model;

	}

	/**
	 * 
	 * 
	 * @param model
	 * @return
	 */

	@RequestMapping(method = RequestMethod.GET, value = "/payment")
	public String viewPaymentForm(Map<String, Object> model) {
		PaymentDetails paymentForm = new PaymentDetails();
		model.put("paymentForm", paymentForm);
		return "paymentForm2";

	}

}

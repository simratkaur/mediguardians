package com.medicine.arvene.controller.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.medicine.arvene.dao.BookingDao;
import com.medicine.arvene.dao.QueryDao;
import com.medicine.arvene.dao.SpecialistDetailsDao;
import com.medicine.arvene.dao.SpecialityDao;
import com.medicine.arvene.dao.UserDao;
import com.medicine.arvene.model.SpecialistDetails;
import com.medicine.arvene.model.User;
import com.medicine.arvene.service.UserService;
import com.medicine.arvene.util.CommonUtil;

@Controller
@RequestMapping
public class WebProfileController {
	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	UserService userService;

	@Autowired
	private UserDao userDao;

	@Autowired
	private BookingDao bookingDao;

	@Autowired
	private SpecialistDetailsDao specialistDetailsDao;

	@Autowired
	private SpecialityDao specialityDao;

	@Autowired
	QueryDao queryDao;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public UserDao getUserDao() {
		return userDao;
	}

	public UserService getUserService() {
		return userService;
	}
	
	
	@RequestMapping(value = "/updateProfile", method = RequestMethod.POST)
	public String updateBiography(HttpServletRequest request,
			@ModelAttribute("user") SpecialistDetails specialist,
			RedirectAttributes redirectAttributes){
		
		ModelAndView mav = new ModelAndView();
		Session session = CommonUtil.getSession(sessionFactory);
		User user;
		try {
			user = userDao.findByUserName(
					request.getUserPrincipal().getName(), session);
			
			if(user!=null){
				
				SpecialistDetails persistedUser = (SpecialistDetails) CommonUtil
						.getEntity(session, SpecialistDetails.class, specialist.getId());
				
				persistedUser.setBiography(specialist.getBiography());
				boolean saved = specialistDetailsDao.saveUser(session, persistedUser);
				
				if (saved)
					redirectAttributes.addFlashAttribute("msg", "Profile saved successfully");
				else
					redirectAttributes.addFlashAttribute("error", "OOh,We are Sorry.Something went wrong!");
				
				
			}
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
		
		
		
		
		return "redirect:profile";
	}

	@RequestMapping(value = "/profile", method = RequestMethod.GET)
	public ModelAndView profile(HttpServletRequest request) throws Exception {
		ModelAndView mav = new ModelAndView();
		Session session = CommonUtil.getSession(sessionFactory);
		User user = userDao.findByUserName(
				request.getUserPrincipal().getName(), session);
		SpecialistDetails specialist = specialistDetailsDao
				.fetchSpecialistDetails(session, user);
		mav.addObject("user", specialist);
		Map<String, String> specialityValues = specialityDao
				.fetchSpecialities(session);
		mav.addObject("specialities", specialityValues);
		mav.addObject("degree", specialityDao.fetchDegrees(session));
		mav.addObject("college", specialityDao.fetchColleges(session));
		mav.setViewName("profile");
		return mav;
	}
	
	@RequestMapping(value = "/editProfile", method = RequestMethod.GET)
	public ModelAndView editProfile(HttpServletRequest request) throws Exception {
		ModelAndView mav = new ModelAndView();
		Session session = CommonUtil.getSession(sessionFactory);
		User user = userDao.findByUserName(
				request.getUserPrincipal().getName(), session);
		SpecialistDetails specialist = specialistDetailsDao
				.fetchSpecialistDetails(session, user);
		mav.addObject("user", specialist);
		Map<String, String> specialityValues = specialityDao
				.fetchSpecialities(session);
		mav.addObject("specialities", specialityValues);
		mav.addObject("degree", specialityDao.fetchDegrees(session));
		mav.addObject("college", specialityDao.fetchColleges(session));
		mav.setViewName("editProfile");
		return mav;
	}

	@RequestMapping(value = "/saveprofile", method = RequestMethod.POST)
	public ModelAndView profile(@ModelAttribute("user") SpecialistDetails user,
			@RequestParam MultipartFile newProfileImage) throws Exception {
		ModelAndView mav = new ModelAndView();
		Session session = CommonUtil.getSession(sessionFactory);

		SpecialistDetails persistedUser = specialistDetailsDao
				.prepareUpdatedEntity(user, session);
		if (newProfileImage.getSize() > 0) {
			persistedUser.setProfileImage(CommonUtil.uploadImage(
					newProfileImage.getBytes(),
					newProfileImage.getOriginalFilename().substring(
							newProfileImage.getOriginalFilename().lastIndexOf(
									'.') + 1)));

		}

		boolean saved = specialistDetailsDao.saveUser(session, persistedUser);
		if (saved)
			mav.addObject("success", "Profile saved successfully");
		else
			mav.addObject("error", "OOh,We are Sorry.Something went wrong!");
		mav.addObject("user", persistedUser);
		Map<String, String> specialityValues = specialityDao
				.fetchSpecialities(session);
		mav.addObject("specialities", specialityValues);
		mav.addObject("degree", specialityDao.fetchDegrees(session));
		mav.addObject("college", specialityDao.fetchColleges(session));
		mav.setViewName("profile");
		return mav;
	}

	/*
	 * @Transactional
	 * 
	 * @RequestMapping(method = RequestMethod.POST, value = "/sendOtp") public
	 * @ResponseBody String sendOtp(Map<String, Object> model,
	 * HttpServletRequest request, @ModelAttribute("user") User user,
	 * 
	 * @RequestParam String phone, @RequestParam String firstName) { String msg;
	 * int otp = 100000 + new Random().nextInt(900000); try { String response =
	 * CommonUtil.sendMsg(CommonUtil.getOTPMsgContent( userDetail.getFirstName()
	 * != null ? userDetail .getFirstName() : firstName, otp), String
	 * .valueOf(phone)); String temp =
	 * response.substring(response.indexOf("ErrorMessage")); msg =
	 * temp.substring(temp.indexOf(":") + 1, temp.indexOf(","));
	 * model.put("msg", response); } catch (Exception e) { msg =
	 * CommonUtil.exceptionHandler(e); model.put("error",
	 * CommonUtil.exceptionHandler(e)); } model.put("userDetail", userDetail);
	 * return otp + "," + msg; }
	 */

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

}

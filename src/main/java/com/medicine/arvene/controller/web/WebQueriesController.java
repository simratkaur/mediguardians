/**
 * 
 */
package com.medicine.arvene.controller.web;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.medicine.arvene.dao.ConversationDao;
import com.medicine.arvene.dao.PaymentDao;
import com.medicine.arvene.dao.QueryDao;
import com.medicine.arvene.dao.ReportsDao;
import com.medicine.arvene.dao.SpecialistDetailsDao;
import com.medicine.arvene.dao.UserDao;
import com.medicine.arvene.model.Conversation;
import com.medicine.arvene.model.PaymentDetails;
import com.medicine.arvene.model.Query;
import com.medicine.arvene.model.Report;
import com.medicine.arvene.model.SpecialistDetails;
import com.medicine.arvene.model.User;
import com.medicine.arvene.util.CommonUtil;
import com.medicine.arvene.util.IPaymentConstants;
import com.medicine.arvene.util.Mailer;
import com.medicine.arvene.util.PaymentUtil;

/**
 * @author maddie
 *
 */

@Controller
@RequestMapping
public class WebQueriesController {

	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private UserDao userDao;

	@Autowired
	private QueryDao queryDao;

	@Autowired
	private ConversationDao conversationDao;

	@Autowired
	private ReportsDao reportsDao;

	@Autowired
	private SpecialistDetailsDao specialistDetailsDao;

	@Autowired
	private PaymentDao paymentDAO;

	@Autowired
	private PaymentUtil paymentUtil;

	@Autowired
	WebProfileController userDetailCtrl;
	
	
	@RequestMapping(value = "/bookingCancelled", method = RequestMethod.GET)
	public String cancelBooking(HttpServletRequest request,
			HttpServletResponse response,
			RedirectAttributes redirectAttributes, Model model) {

		try {

			redirectAttributes.addFlashAttribute("errortitle",
					"Your payment was not successful!");
			redirectAttributes.addFlashAttribute(
					"error",
					"Something went wrong while making your payment. we regret for the "
							+ "inconvenience caused.");
		} catch (Exception e) {
			model.addAttribute("error", CommonUtil.exceptionHandler(e));
		}

		return "redirect:login";

	}

	@RequestMapping(value = "/editConveration", method = RequestMethod.POST)
	public String editQuery(HttpServletRequest request,
			RedirectAttributes redirectAttributes,
			HttpServletResponse response,
			@ModelAttribute Conversation conversation, Model model) {
		User user = null;

		try {
			Session session = CommonUtil.getSession(sessionFactory);

			if (request.getUserPrincipal() != null) {

				try {
					user = userDao.findByUserName(request.getUserPrincipal()
							.getName(), session);
				} catch (Exception e) {

					redirectAttributes.addFlashAttribute("error",
							CommonUtil.exceptionHandler(e));
				}

			}
			if (user != null) {

				Conversation conversationDB = (Conversation) session.get(
						Conversation.class, conversation.getId());
				conversationDB.setText(conversation.getText());
				queryDao.updteConversation(session, conversationDB);

				// upload files if any
				if (conversation.getFiles() != null
						&& !conversation.getFiles().isEmpty()) {
					for (Iterator<MultipartFile> iterator = conversation
							.getFiles().iterator(); iterator.hasNext();) {
						MultipartFile attachment = (MultipartFile) iterator
								.next();
						Report report = new Report();
						report.setUrl(CommonUtil.uploadImage(
								attachment.getBytes(),
								attachment.getOriginalFilename().substring(
										attachment.getOriginalFilename()
												.lastIndexOf('.') + 1)));
						report.setConversation(conversationDB);
						report.setUser(user);
						report.setFileName(attachment.getOriginalFilename());
						conversation.getReports().add(report);
					}
					reportsDao.saveReports(session, conversation.getReports());

				}

				redirectAttributes.addFlashAttribute("msg",
						"Your query has been updated.");
			} else {
				redirectAttributes.addFlashAttribute("error",
						"Your session has expired.Please login to continue!");
			}

		} catch (Exception e) {

			model.addAttribute("error", CommonUtil.exceptionHandler(e));
		}

		return "redirect:queries";

	}

	@RequestMapping(value = "/queryReply", method = RequestMethod.POST)
	public String queryReply(HttpServletRequest request,
			HttpServletResponse response, @ModelAttribute Query query,
			RedirectAttributes redirectAttributes, Model model) {

		Session session = CommonUtil.getSession(sessionFactory);
		User user = null;
		try {
			// get the response, save conversation object, add it in query ,

			if (request.getUserPrincipal() != null) {

				try {
					user = userDao.findByUserName(request.getUserPrincipal()
							.getName(), session);
				} catch (Exception e) {

					model.addAttribute("error", CommonUtil.exceptionHandler(e));
				}

				Query querydb = (Query) CommonUtil.getEntity(session,
						Query.class, query.getId());
				Conversation conversation = new Conversation();
				conversation.setText(query.getNewConversation());
				conversation.setQuery(querydb);
				conversation.setUser(user);
				conversation.setType(user.getUserType());
				try {
					conversation.setCreatedAt(CommonUtil.getIstDateTime());
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					conversation.setCreatedAt(new Date());
				}

				int newConversationId = conversationDao.saveConversation(
						session, conversation);
				conversation.setId(newConversationId);

				// mark query as responded and update query flag if needed
				if (newConversationId > 0) {

					List<Conversation> conversationList = querydb
							.getConversations();

					if (null == conversationList) {
						conversationList = new ArrayList<Conversation>();
					}
					conversationList.add(conversation);
					querydb.setConversations(conversationList);

					if (user.getUserType().equalsIgnoreCase("S")) {
						querydb.setResponded(true);
					} else {
						querydb.setResponded(false);
					}

					// check for addQueryFlag
					if (conversationList.size() == CommonUtil.MAX_CONVERSATIONS) {
						querydb.setAddQueryFlag(false);
					}

					queryDao.updateQuery(session, querydb);

					if (user.getUserType().equalsIgnoreCase("S")) {
						redirectAttributes.addFlashAttribute("msg",
								"Response Sent to the patient.");
						new Mailer().sendRespQueryMail(querydb.getUser());
					} else {
						redirectAttributes.addFlashAttribute("msg",
								"Message Sent to the doctor.");
						// new
						// Mailer().sendDocQueryMail(query.getSpecialistDetails()
						// .getUser());
					}

				}

			} else {
				model.addAttribute("error",
						"Please login or Register to send the message!");
				model.addAttribute("query", query);
				// redirectAttributes.addAttribute("query", query);
			}

		} catch (Exception e) {
			model.addAttribute("error", CommonUtil.exceptionHandler(e));
		}

		// mav.setViewName("Queries");
		return "redirect:queries";

	}

	@RequestMapping(value = "/askQuery", method = RequestMethod.GET)
	public ModelAndView askQuery(
			@RequestParam(value = "uid", defaultValue = "0", required = false) Integer ownerid,
			HttpServletRequest request) {
		HttpSession httpSession = request.getSession(false);
		if (httpSession != null) {
			Session session = CommonUtil.getSession(sessionFactory);

			User user = null;
			User owner = null;
			SpecialistDetails specialistDetails = null;
			int specialist = 0;

			ModelAndView mav = new ModelAndView();
			try {
				if (request.getUserPrincipal() != null) {
					user = userDao.findByUserName(request.getUserPrincipal()
							.getName(), session);
				}
			} catch (Exception e) {
				mav.addObject("error", CommonUtil.exceptionHandler(e));
			}

			if (ownerid > 0) {
				owner = (User) CommonUtil.getEntity(session, User.class,
						ownerid);
			}

			// create a query object and add it in model

			mav.addObject("owner", owner);
			mav.addObject("user", user);

			com.medicine.arvene.model.Query query = new com.medicine.arvene.model.Query();

			try {

				if (ownerid > 0) {
					specialistDetails = specialistDetailsDao
							.fetchSpecialistDetails(session, owner);

					specialist = specialistDetails.getId();
				}

				query.setSpecialistDetails(specialistDetails);
				query.setSpecialist(specialist);

			} catch (Exception e) {

				mav.addObject("error", CommonUtil.exceptionHandler(e));
			}

			mav.addObject("query", query);
			if (ownerid > 0) {
				mav.setViewName("editQuerySpecialist");
			} else {
				mav.setViewName("editQuery");
			}

			return mav;

		}
		return null;
	}

	@RequestMapping(value = "/askQuery", method = RequestMethod.POST)
	public String processQuery(
			@ModelAttribute("query") com.medicine.arvene.model.Query query,
			HttpServletRequest request, HttpServletResponse response,
			RedirectAttributes redirectAttributes, Model model)
			throws IOException {
		Session session = CommonUtil.getSession(sessionFactory);
		String redirectUrl = "askQuery";
		User user = null;
		int queryID = query.getId();

		try {

			if (query.getSpecialist() > 0) {
				SpecialistDetails specialist = (SpecialistDetails) CommonUtil.getEntity(session,
						SpecialistDetails.class, query.getSpecialist());
				query.setCost(specialist.getQueryFee());
			}
			else
				query.setCost(CommonUtil.GENERIC_FEE);

		} catch (Exception e) {
			model.addAttribute("error", CommonUtil.exceptionHandler(e));
		}

		Conversation conversation = new Conversation();
		conversation.setText(query.getNewConversation());
		

		if (request.getSession().getAttribute("sessionQuery") == null) {
			request.getSession().setAttribute("sessionQuery", query);
		}
		if (request.getUserPrincipal() != null) {

			try {
				user = userDao.findByUserName(request.getUserPrincipal()
						.getName(), session);
			} catch (Exception e) {

				model.addAttribute("error", CommonUtil.exceptionHandler(e));
			}
			query.setUser(user);
			// todo fixed query fee
			// redirectAttributes.addAttribute("fee",
			// query.getSpecialistDetails()
			// .getSessionFee());

			if (queryID > 0) {
				// update the query
				queryDao.updateQuery(session, query);

				try {
					redirectAttributes.addFlashAttribute("msgtitle",
							"Query sent to doctor!");
					redirectAttributes
							.addFlashAttribute("msg",
									"Your updated query has been sent to the Specialist.");
					redirectUrl = "redirect:queries";

				} catch (Exception e) {
					model.addAttribute("error", CommonUtil.exceptionHandler(e));

				}

			} else {
				// insert new query
				Integer id = queryDao.saveQuery(session, query);
				if (id > 0) {

					query.setId(id);

					// update conversation
					conversation.setQuery(query);
					conversation.setUser(user);
					conversation.setType(user.getUserType());
					try {
						conversation.setCreatedAt(CommonUtil.getIstDateTime());
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						conversation.setCreatedAt(new Date());
					}

					int conversationid = conversationDao.saveConversation(
							session, conversation);

					conversation.setId(conversationid);

					// update files in Reports table

					if (query.getFiles() != null && !query.getFiles().isEmpty()) {
						for (Iterator<MultipartFile> iterator = query
								.getFiles().iterator(); iterator.hasNext();) {
							MultipartFile attachment = (MultipartFile) iterator
									.next();
							Report report = new Report();
							report.setUrl(CommonUtil.uploadImage(
									attachment.getBytes(),
									attachment.getOriginalFilename().substring(
											attachment.getOriginalFilename()
													.lastIndexOf('.') + 1)));
							report.setConversation(conversation);
							report.setUser(user);
							report.setFileName(attachment.getOriginalFilename());
							conversation.getReports().add(report);
						}
						reportsDao.saveReports(session,
								conversation.getReports());

					}
					redirectAttributes.addAttribute("fee", query.getCost());
					redirectAttributes.addAttribute("id", id);
					redirectAttributes.addAttribute("isQuery", true);
					redirectUrl = "redirect:queryPayment";

				} else {

					model.addAttribute("error",
							"Something went wrong.Please retry!");
				}

			}

		} else {
			
			model.addAttribute("error",
					"Please login or Register to send the query!");
			model.addAttribute("query", query);
			// redirectAttributes.addAttribute("query", query);
		}
		return redirectUrl;

	}

	@RequestMapping(value = "/paymentResponse", method = RequestMethod.POST)
	public String paymentResponse(HttpServletRequest request,
			HttpServletResponse response,
			RedirectAttributes redirectAttributes, Model model) {

		PaymentDetails paymentDetails = paymentUtil
				.populatePaymentDetails(request);
		paymentDetails.setUdf1(request
				.getParameter(IPaymentConstants.PARAM_USER_FIELD_1));
		model.addAttribute("paymentDetails", paymentDetails.toString());
		model.addAttribute("status", paymentDetails.getStatus());
		return "paymentResponse";
	}

	@RequestMapping(value = "/SPaypalQuery", method = RequestMethod.POST)
	public String processSuccessPaymentForQueryTest(HttpServletRequest request,
			HttpServletResponse response,
			RedirectAttributes redirectAttributes, Model model) {

		Enumeration<String> parameterNames = request.getParameterNames();

		// while (parameterNames.hasMoreElements()) {
		//
		// String paramName = parameterNames.nextElement();
		//
		// String[] paramValues = request.getParameterValues(paramName);
		// for (int i = 0; i < paramValues.length; i++) {
		// String paramValue = paramValues[i];
		// System.out.println(paramName);
		// }
		//
		// }

		Session session = null;
		PaymentDetails paymentDetails = paymentUtil
				.populatePaymentDetailsPaypal(request);
		String udf1 = request
				.getParameter(IPaymentConstants.PAYPAL_PARAM_ITEM_NUMBER);

		int queryId = Integer.parseInt(udf1.substring(2));

		try {

			session = CommonUtil.getSession(sessionFactory);
			int id = paymentDAO.savePaymentDetail(paymentDetails, session);
			if (id > 0) {
				paymentDetails = (PaymentDetails) session.get(
						PaymentDetails.class, id);
				queryDao.updatePaymentId(session, queryId, paymentDetails);

				if (paymentDetails.getStatus().equalsIgnoreCase(
						IPaymentConstants.PAYPAL_PAYMENT_STATUS_COMPLETED)) {

					Query query = (Query) CommonUtil.getEntity(session,
							Query.class, queryId);

					if (query.getSpecialistDetails() != null) {
						new Mailer().sendDocQueryMail(query
								.getSpecialistDetails());
						redirectAttributes.addFlashAttribute("msgtitle",
								"Query sent to doctor!");
						redirectAttributes.addFlashAttribute("msg",
								"Your Query has been sent to the Specialist.");
					} else {
						// send it to admin
						new Mailer().sendAdminQueryMail();
						redirectAttributes.addFlashAttribute("msgtitle",
								"Query received!");
						redirectAttributes
								.addFlashAttribute("msg",
										"Thank you.Your Query has been received. We will send it to the Specialist.");

					}

				} else {

					redirectAttributes.addFlashAttribute("errortitle",
							"Query could not be sent to doctor!");
					model.addAttribute(
							"error",
							"Something went wrong while we were sending your query to the Specialist. we regret for the"
									+ "inconvenience caused");
				}

			}

		} catch (Exception e) {
			model.addAttribute("error", CommonUtil.exceptionHandler(e));
		}

		return "redirect:login";

	}

	@RequestMapping(value = "/SPaymentQuery", method = RequestMethod.POST)
	public String processSuccessPaymentForQuery(HttpServletRequest request,
			HttpServletResponse response,
			RedirectAttributes redirectAttributes, Model model) {

		Session session = null;
		PaymentDetails paymentDetails = paymentUtil
				.populatePaymentDetails(request);
		String udf1 = request
				.getParameter(IPaymentConstants.PARAM_USER_FIELD_1);

		int queryId = Integer.parseInt(udf1.substring(2));

		try {

			session = CommonUtil.getSession(sessionFactory);
			int id = paymentDAO.savePaymentDetail(paymentDetails, session);
			if (id > 0) {
				paymentDetails = (PaymentDetails) session.get(
						PaymentDetails.class, id);
				queryDao.updatePaymentId(session, queryId, paymentDetails);

				if (paymentDetails.getStatus().equalsIgnoreCase("success")) {

					Query query = (Query) CommonUtil.getEntity(session,
							Query.class, queryId);
					new Mailer().sendDocQueryMail(query.getSpecialistDetails());

					redirectAttributes.addFlashAttribute("msgtitle",
							"Query sent to doctor!");
					redirectAttributes.addFlashAttribute("msg",
							"Your Query has been sent to the Specialist.");

				} else {

					redirectAttributes.addFlashAttribute("errortitle",
							"Query could not be sent to doctor!");
					model.addAttribute(
							"error",
							"Something went wrong while we were sending your query to the Specialist. we regret for the"
									+ "inconvenience caused");
				}

			}

		} catch (Exception e) {
			model.addAttribute("error", CommonUtil.exceptionHandler(e));
		}

		return "redirect:queries";

	}

	@RequestMapping(value = "/queryPayment", method = RequestMethod.GET)
	public ModelAndView queryPayment(HttpServletRequest request,
			@RequestParam int id, @RequestParam boolean isQuery,
			@RequestParam int fee) {
		ModelAndView mav = new ModelAndView();
		Session session = CommonUtil.getSession(sessionFactory);
		User user = null;
		try {
			user = userDao.findByUserName(request.getUserPrincipal().getName(),
					session);

			if (isQuery) {
				request.getSession().setAttribute("sessionQuery", null);
			} else {
				request.getSession().setAttribute("sessionBooking", null);
			}
			mav.addObject("fee", fee);
			PaymentDetails paymentForm = new PaymentDetails();
			paymentForm.setEmail(user.getUsername());
			paymentForm.setAmount(fee);
			paymentForm.setPhone(user.getPhone());
			paymentForm.setFirstName(user.getUsername());

			paymentForm.setUdf1("Q-" + id);

			paymentForm.setTransactionId(paymentUtil.getNewTxnId());

			if (isQuery) {
				paymentForm.setSurl(request
						.getRequestURL()
						.toString()
						.replace(request.getRequestURI(),
								request.getContextPath())
						+ "/SPaypalQuery");
				paymentForm.setFurl(request
						.getRequestURL()
						.toString()
						.replace(request.getRequestURI(),
								request.getContextPath())
						+ "/bookingCancelled");
			} else {
				paymentForm.setSurl(request
						.getRequestURL()
						.toString()
						.replace(request.getRequestURI(),
								request.getContextPath())
						+ "/SPaypalBooking");
				paymentForm.setFurl(request
						.getRequestURL()
						.toString()
						.replace(request.getRequestURI(),
								request.getContextPath())
						+ "/bookingCancelled");

			}

			paymentForm.setChecksum(paymentUtil.gethashString(paymentForm));

			mav.addObject("udf1", "Q-" + id);

			mav.addObject("paymentForm", paymentForm);
			mav.setViewName("processPayment");
		} catch (Exception e) {

			mav.addObject("error", CommonUtil.exceptionHandler(e));
		}

		return mav;
	}

	@RequestMapping(value = "/queries", method = RequestMethod.GET)
	public ModelAndView showQueries(
			HttpServletRequest request,
			@RequestParam(value = "start", defaultValue = "0", required = false) int start,
			@RequestParam(value = "range", defaultValue = "10", required = false) int range) {

		int pagenumber = 0;
		Session session = CommonUtil.getSession(sessionFactory);
		List<Query> queries = null;
		User user = null;
		ModelAndView mav = new ModelAndView();

		try {
			user = userDao.findByUserName(request.getUserPrincipal().getName(),
					session);

		} catch (Exception e) {

			mav.addObject("error", CommonUtil.exceptionHandler(e));
		}

		if (user != null) {

			mav.addObject("user", user);

			try {

				if (user.getUserType().equalsIgnoreCase("S")) {
					int id = specialistDetailsDao.fetchSpecialistDetails(
							session, user).getId();
					queries = queryDao.fetchQueryForSpecialist(session, id,
							start, range);
					int count = queryDao.fetchQueryCountBySpecialist(id,
							session);
					if (count % 10 == 0)
						pagenumber = count / 10;
					else
						pagenumber = (count / 10) + 1;
					if (pagenumber == 0)
						pagenumber = 1;

				} else {
					queries = queryDao.fetchQueryForUser(session, user.getId(),
							start, range);
					pagenumber = queryDao.fetchCountQueryByUser(session,
							user.getId());

				}

			} catch (Exception e) {

				mav.addObject("error", CommonUtil.exceptionHandler(e));
			}

			mav.addObject("queries", queries);

		}

		mav.addObject("pageno", pagenumber);
		mav.addObject("currentpage", (start / 10) + 1);
		mav.addObject("start", start);
		mav.setViewName("queries");

		return mav;

	}

	@RequestMapping(value = "/queriesHistory", method = RequestMethod.GET)
	public ModelAndView showQueriesHistory(
			HttpServletRequest request,
			@RequestParam(value = "start", defaultValue = "0", required = false) int start,
			@RequestParam(value = "range", defaultValue = "10", required = false) int range) {

		int pagenumber = 0;
		Session session = CommonUtil.getSession(sessionFactory);
		List<Query> queries = null;
		User user = null;
		ModelAndView mav = new ModelAndView();

		try {
			user = userDao.findByUserName(request.getUserPrincipal().getName(),
					session);

		} catch (Exception e) {

			mav.addObject("error", CommonUtil.exceptionHandler(e));
		}

		if (user != null) {

			mav.addObject("user", user);

			try {

				if (user.getUserType().equalsIgnoreCase("S")) {
					int id = specialistDetailsDao.fetchSpecialistDetails(
							session, user).getId();
					queries = queryDao.fetchQueryHistoryForSpecialist(session,
							id, start, range);
					int count = queryDao.getQueryHistoryCountBySpecialist(id,
							session);
					if (count % 10 == 0)
						pagenumber = count / 10;
					else
						pagenumber = (count / 10) + 1;
					if (pagenumber == 0)
						pagenumber = 1;

				} else {
					queries = queryDao.fetchQueryForUser(session, user.getId(),
							start, range);
					pagenumber = queryDao.fetchCountQueryByUser(session,
							user.getId());

				}

			} catch (Exception e) {

				mav.addObject("error", CommonUtil.exceptionHandler(e));
			}

			mav.addObject("queries", queries);

		}

		mav.addObject("pageno", pagenumber);
		mav.addObject("currentpage", (start / 10) + 1);
		mav.addObject("start", start);
		mav.setViewName("queriesHistory");

		return mav;

	}

}
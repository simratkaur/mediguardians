package com.medicine.arvene.controller.web;

import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.medicine.arvene.dao.SpecialistDetailsDao;
import com.medicine.arvene.dao.SpecialityDao;
import com.medicine.arvene.dao.UserDao;
import com.medicine.arvene.model.SpecialistDetails;
import com.medicine.arvene.model.SpecialistRegisterForm;
import com.medicine.arvene.model.User;
import com.medicine.arvene.util.CommonUtil;
import com.medicine.arvene.util.Mailer;

@Controller
@RequestMapping
public class WebRegisterController {
	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private UserDao userDao;

	@Autowired
	SpecialityDao specialityDao;

	@Autowired
	SpecialistDetailsDao specialistDetailsDao;
	
	

	@Transactional
	@RequestMapping(value = "/verifyUser", method = RequestMethod.GET, params = {
			"token", "email" })
	public @ResponseBody ModelAndView passwordResetToken(
			@RequestParam(value = "token") String token,
			@RequestParam(value = "email") String email) throws Exception {
		ModelAndView model = new ModelAndView();
		Session session = CommonUtil.getSession(sessionFactory);
		try {
			User user = userDao.findByUserName(email, session);
			if (token.equals(user.getVerificationCode())) {
				userDao.makeActive(user.getId(), session);	
				model.addObject("msg", "Congratulations! Your account is verified.Please log in to continue.");
			} else {
				model.addObject("error", "Verification token is not valid!!!!");
			}
			model.addObject("user", user);
			model.setViewName("login");
		} finally {
			session.close();
		}
		return model;
	}

	@Transactional
	@RequestMapping(value = "/activateAccount", method = RequestMethod.GET)
	public String activateAccount(Map<String, Object> model,
			HttpServletRequest request) {
		User user = new User();
		model.put("user", user);
		return "activateAccount";
	}

	@RequestMapping(value = "/faqs", method = RequestMethod.GET)
	public ModelAndView faqs() {
		ModelAndView model = new ModelAndView();
		model.setViewName("faqs");
		return model;
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public UserDao getUserDao() {
		return userDao;
	}

	@RequestMapping(value = "/howItWorks", method = RequestMethod.GET)
	public ModelAndView howItWorks() {
		ModelAndView model = new ModelAndView();
		model.setViewName("howItWorks");
		return model;
	}

	@RequestMapping(value = "/medicineReminders", method = RequestMethod.GET)
	public ModelAndView medicineReminders() {
		ModelAndView model = new ModelAndView();
		model.setViewName("medicineReminders");
		return model;
	}

	@Transactional
	@RequestMapping(method = RequestMethod.POST, value = "/specialistRegister")
	public ModelAndView processRegistration(
			@ModelAttribute("specialistRegisterForm") SpecialistRegisterForm specialistRegisterForm,
			@RequestParam MultipartFile newProfileImage,
			HttpServletRequest request, HttpServletResponse response) {
		ModelAndView model = new ModelAndView();
		Session session = CommonUtil.getSession(sessionFactory);
		boolean check = false;
		User user = new User();
		try {

			if (newProfileImage.getSize() > 1048576) {
				model.addObject("error",
						"Please choose an Image less than 1 MB ");

			} else {
				user.setUsername(specialistRegisterForm.getUsername());
				user.setPassword(new BCryptPasswordEncoder().encode(specialistRegisterForm.getPassword()));
				user.setPhone(specialistRegisterForm.getPhone());
				user.setUserType("S");
				user.setOTP(100000 + new Random().nextInt(900000));

				check = userDao.addUser(user, session);

				SpecialistDetails specialistDetails = new SpecialistDetails();
				specialistDetails.setTitle(specialistRegisterForm.getTitle());
				specialistDetails.setFirstName(specialistRegisterForm
						.getFirstName());
				specialistDetails.setLastName(specialistRegisterForm
						.getLastName());
				specialistDetails.setMiddleName(specialistRegisterForm
						.getMiddleName());
				specialistDetails.setDegrees(specialistRegisterForm.getDegrees());

				specialistDetails.setSubSpeciality(specialistRegisterForm
						.getSubSpeciality());
				specialistDetails.setAreaOfInterest(specialistRegisterForm
						.getAreaOfInterest());
				specialistDetails.setHospital(specialistRegisterForm
						.getHospital());
				specialistDetails.setHospitalCity(specialistRegisterForm
						.getHospitalCity());
				specialistDetails.setMobile(specialistRegisterForm
						.getMobile());
				specialistDetails.setBiography(specialistRegisterForm
						.getBiography());
				specialistDetails.setAwards(specialistRegisterForm.getAwards());
				specialistDetails.setAchievements(specialistRegisterForm
						.getAchievements());
				specialistDetails.setMemberShip(specialistRegisterForm
						.getMemberShip());
				specialistDetails.setSessionFee(specialistRegisterForm
						.getSessionFee());
				specialistDetails.setQueryFee(specialistRegisterForm.getQueryFee());
				specialistDetails
						.setMedregno(specialistRegisterForm.getRegno());
				specialistDetails.setGender(specialistRegisterForm.getGender());
				specialistDetails.setSpecialityid(specialistRegisterForm
						.getSpecialityid());
				specialistDetails.setOther(specialistRegisterForm.getOther());
				if (newProfileImage.getSize() > 0) {
					specialistDetails.setProfileImage(CommonUtil.uploadImage(
							newProfileImage.getBytes(),
							newProfileImage.getOriginalFilename().substring(
									newProfileImage.getOriginalFilename()
											.lastIndexOf('.') + 1)));
				}

				specialistDetails.setUser(user);
				specialistDetailsDao.addSpecialistDetails(session,
						specialistDetails);
				
				new Mailer().sendMailForApproval(specialistDetails, request); 

			}

		} catch (Exception e) {
			model.addObject("error", CommonUtil.exceptionHandler(e));
		} finally {
//			session.close();
		}
		if (check) {
			try {

				model = new ModelAndView("login");
				model.addObject(
						"msg",
						"Thankyou for registering with us, our Technical team will contact you shortly!");
				model.addObject("email", user.getUsername());
				
				
			} catch (Exception e) {
				model.addObject("error", CommonUtil.exceptionHandler(e));
			}
		} else {
			specialistRegisterForm.setUserType("S");
			ModelAndView mav = new ModelAndView("specialistRegister",
					"specialistRegisterForm", specialistRegisterForm);
			Map<String, String> specialityValues = specialityDao
					.fetchSpecialities(session);
			mav.addObject("specialities", specialityValues);
			mav.addObject("specialistRegisterForm", specialistRegisterForm);
//			mav.addObject("degree", specialityDao.fetchDegrees(session));
//			mav.addObject("college", specialityDao.fetchColleges(session));
		}
		return model;
	}

	@Transactional
	@RequestMapping(method = RequestMethod.POST, value = "/register")
	public ModelAndView processRegistration(
			@ModelAttribute("userForm") User user, HttpServletRequest request,
			HttpServletResponse response) {
		ModelAndView model = new ModelAndView();
		model.setViewName("login");
		Session session = CommonUtil.getSession(sessionFactory);
		boolean check = false;
		try {
			
//			user.setOTP(100000 + new Random().nextInt(900000));
			if (user.getPassword() != null)
				user.setPassword(new BCryptPasswordEncoder().encode(user
						.getPassword()));
			user.setVerificationCode(new BCryptPasswordEncoder().encode(String
					.valueOf(Math.random())));
			user.setUserType("U");
			check = userDao.addUser(user, session);

		} catch (Exception e) {
			model.addObject("error", CommonUtil.exceptionHandler(e));
		} finally {
			session.close();
		}
		if (check) {
			try {
//				model = new ModelAndView("redirect:tokenVerification");
//				CommonUtil.sendMsg(
//						CommonUtil.getOTPMsgContent("Customer", user.getOTP()),
//						String.valueOf(user.getPhone()));
//				model.addObject("email", user.getUsername());
				
				new Mailer().sendVerificationMail(user, request);
				model.addObject("msg", "Verification mail is sent.Please verify your account for logging in.");
				
			} catch (Exception e) {
				model.addObject("error", CommonUtil.exceptionHandler(e));
			}
		}
		return model;
	}

	@RequestMapping(value = "/saveMedicalRecords", method = RequestMethod.GET)
	public ModelAndView saveRecordsInfo() {
		ModelAndView model = new ModelAndView();
		model.setViewName("saveMedicalRecords");
		return model;
	}

	@RequestMapping(value = "/secondOpinionDoctor", method = RequestMethod.GET)
	public ModelAndView secondOpinionInfo() {
		ModelAndView model = new ModelAndView();
		model.setViewName("secondOpinionDoctor");
		return model;
	}

	@RequestMapping(value = "/sendAppLink", method = RequestMethod.GET)
	public ModelAndView sendAppLinkMessage(@RequestParam("mob") String mob) {

		ModelAndView mav = new ModelAndView("login");

		String sms = "Dear Registered Customer, "
				+ "Please download our app for android using"
				+ " http://bit.ly/1kvtOCs";
		String smsResp = "";
		String msg = "";

		try {
			smsResp = CommonUtil.sendMsg(sms, mob);

			String temp = smsResp.substring(smsResp.indexOf("ErrorMessage"));
			msg = temp.substring(temp.indexOf(":") + 1, temp.indexOf(","));

		} catch (Exception e) {
			mav.addObject("error", CommonUtil.exceptionHandler(e));
		}

		if (smsResp != "") {
			mav.addObject("msg", msg);
		}

		return mav;

	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	@RequestMapping(value = "/specialistRegister", method = RequestMethod.GET)
	public ModelAndView specialistRegistration() {
		Session session = CommonUtil.getSession(sessionFactory);
		SpecialistRegisterForm specialistRegisterForm = new SpecialistRegisterForm();
		specialistRegisterForm.setUserType("S");
		ModelAndView mav = new ModelAndView("specialistRegister",
				"specialistRegisterForm", specialistRegisterForm);
		Map<String, String> specialityValues = specialityDao
				.fetchSpecialities(session);
		mav.addObject("specialities", specialityValues);
		mav.addObject("specialistRegisterForm", specialistRegisterForm);
		mav.addObject("degree", specialityDao.fetchDegrees(session));
		return mav;
	}

	@Transactional
	@RequestMapping(method = RequestMethod.POST, value = "/tokenVerification")
	public String verifyOTP(@ModelAttribute("user") User user,
			HttpServletRequest request, HttpServletResponse response,
			Map<String, Object> model) {
		String page = "tokenVerification";
		Session session = CommonUtil.getSession(sessionFactory);
		try {
			User dbUser = userDao.findByUserName(user.getUsername(), session);
			if (user.getOTP() == dbUser.getOTP()) {
				dbUser.setIsActive(true);
				userDao.updateUser(dbUser, session);
				//new Mailer().sendUserWelcomeMail(user);
				page = "redirect:login?token";
			} else {
				model.put("error", "OTP is not valid!!!!");
			}
		} catch (Exception e) {
			model.put("error", CommonUtil.exceptionHandler(e));
		} finally {
			session.close();
		}
		return page;
	}

	@Transactional
	@RequestMapping(value = "/tokenVerification", method = RequestMethod.GET, params = { "email" })
	public @ResponseBody ModelAndView verifyToken(
			@RequestParam(value = "email") String email) throws Exception {
		ModelAndView model = new ModelAndView();
		Session session = CommonUtil.getSession(sessionFactory);
		User user;
		try {
			user = userDao.findByUserName(email, session);
		} finally {
			session.close();
		}

		if (user.getIsActive()) {
			model.addObject("error", "User is already Active!!!!");
			model.setViewName("login");
		} else {
			user.setOTP(0);
			model.addObject("user", user);
			model.setViewName("tokenVerification");
		}
		return model;
	}

//	@Transactional
//	@RequestMapping(method = RequestMethod.GET, value = "/register")
//	public String viewRegistration(Map<String, Object> model) {
//		User userForm = new User();
//		userForm.setUserType("U");
//		model.put("userForm", userForm);
//
//		return "register";
//	}

}

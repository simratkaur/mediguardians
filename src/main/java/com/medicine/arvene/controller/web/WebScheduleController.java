package com.medicine.arvene.controller.web;

import java.text.ParseException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.amazonaws.util.json.JSONObject;
import com.medicine.arvene.dao.AvailabilityDao;
import com.medicine.arvene.dao.SlotDao;
import com.medicine.arvene.dao.SpecialistDao;
import com.medicine.arvene.dao.SpecialistDetailsDao;
import com.medicine.arvene.dao.SpecialityDao;
import com.medicine.arvene.dao.UserDao;
import com.medicine.arvene.model.SpecialistDetails;
import com.medicine.arvene.model.Speciality;
import com.medicine.arvene.model.User;
import com.medicine.arvene.model.WeekSchedule;
import com.medicine.arvene.util.CommonUtil;

@Controller
public class WebScheduleController {

	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private SpecialistDao specialistDao;

	@Autowired
	private SpecialityDao specialityDao;

	@Autowired
	private SlotDao slotDao;

	@Autowired
	private AvailabilityDao availabilityDao;

	@Autowired
	private SpecialistDetailsDao specialistDetailsDao;

	@Autowired
	private UserDao userDao;

	@RequestMapping(value = "/specialists", method = RequestMethod.GET)
	public ModelAndView loadSpecialists(
			@ModelAttribute("speciality") Speciality speciality,
			@RequestParam(value = "start", defaultValue = "0", required = false) int start,
			@RequestParam(value = "range", defaultValue = "9", required = false) int range) {
		ModelAndView mav = new ModelAndView();
		Session session = CommonUtil.getSession(sessionFactory);
		List<SpecialistDetails> specialists = specialistDao.fetchSpecialists(
				session, -1, start, range);
		int pagenumber = specialistDao.getPageCount(session, -1);
		mav.addObject("specialists", specialists);
		mav.addObject("pageno", pagenumber);
		mav.addObject("currentpage", (start / 9) + 1);
		mav.addObject("speciality", speciality);

		Map<String, String> specialityMap = specialityDao
				.fetchSpecialities(session);
		JSONObject jsonObj = new JSONObject(specialityMap);

		mav.addObject("specialityMap", jsonObj);

		mav.setViewName("specialists");

		return mav;
	}

	@RequestMapping(value = "/saveavailability", method = RequestMethod.POST)
	public ModelAndView saveAvailability(
			@ModelAttribute("schedule") WeekSchedule weekSchedule,
			HttpServletRequest request) throws ParseException {
		ModelAndView mav = new ModelAndView();
		User user = null;
		HttpSession httpSession = request.getSession(false);
		if (httpSession != null) {
			// user = (User) httpSession.getAttribute("user");
		}
		Session session = CommonUtil.getSession(sessionFactory);

		try {
			user = userDao.findByUserName(request.getUserPrincipal().getName(),
					session);
		} catch (Exception e) {

			mav.addObject("error", CommonUtil.exceptionHandler(e));
		}
		availabilityDao.updateAvailabilityPeriod(user, session,
				weekSchedule.getStartDate(), weekSchedule.getEndDate());
		Query q = session.createQuery("delete Availability where userid = ?")
				.setInteger(0, user.getId());
		q.executeUpdate();
		weekSchedule.prepareWeekSessions();
		availabilityDao.generateAvailability(user.getId(),
				weekSchedule.getStartDate(), weekSchedule.getEndDate(),
				weekSchedule.getWeeksessions(), session);
		mav.addObject("schedule", weekSchedule);
		mav.setViewName("availability");
		return mav;
	}

	@RequestMapping(value = "/searchspecialists", method = RequestMethod.GET)
	public ModelAndView searchSpecialits(
			@RequestParam(value = "specialityId", defaultValue = "-1", required = false) int specialityId,
			@RequestParam(value = "start", defaultValue = "0", required = false) int start,
			@RequestParam(value = "range", defaultValue = "9", required = false) int range) {
		ModelAndView mav = new ModelAndView();
		Speciality speciality = new Speciality();
		Session session = CommonUtil.getSession(sessionFactory);
		List<SpecialistDetails> specialists = specialistDao.fetchSpecialists(
				session, specialityId, start, range);
		int pagenumber = specialistDao.getPageCount(session, specialityId);
		if(specialityId>-1) {
			speciality = (Speciality) session.get(Speciality.class,
					specialityId);
		}
		
		
		Map<String, String> specialityValues = specialityDao
				.fetchActiveSpecialities(session);
		mav.addObject("specialities", specialityValues);
		mav.addObject("specialists", specialists);
		mav.addObject("pageno", pagenumber);
		mav.addObject("currentpage", (start / 9) + 1);
		mav.addObject("speciality", speciality);

		Map<String, String> specialityMap = specialityDao
				.fetchSpecialities(session);
		JSONObject jsonObj = new JSONObject(specialityMap);
		mav.addObject("specialityMap", jsonObj);

		mav.setViewName("specialists");
		return mav;
	}

	@RequestMapping(value = "/searchspecialists", method = RequestMethod.POST)
	public ModelAndView searchSpecialits(
			@ModelAttribute("speciality") Speciality speciality,
			@RequestParam(value = "start", defaultValue = "0", required = false) int start,
			@RequestParam(value = "range", defaultValue = "9", required = false) int range) {
		ModelAndView mav = new ModelAndView();
		Session session = CommonUtil.getSession(sessionFactory);
		int id = -1;
		if (speciality != null && speciality.getId() != null)
			id = speciality.getId();
		List<SpecialistDetails> specialists = specialistDao.fetchSpecialists(
				session, id, start, range);
		int pagenumber = specialistDao.getPageCount(session, id);
		mav.addObject("specialists", specialists);
		mav.addObject("pageno", pagenumber);
		mav.addObject("currentpage", (start / 9) + 1);

		Map<String, String> specialityMap = specialityDao
				.fetchSpecialities(session);
		JSONObject jsonObj = new JSONObject(specialityMap);
		mav.addObject("specialityMap", jsonObj);

		mav.setViewName("specialists");
		return mav;
	}

	@RequestMapping(value = "/availability", method = RequestMethod.GET)
	public ModelAndView showAvailability(
			@ModelAttribute("schedule") WeekSchedule weekSchedule,
			HttpServletRequest request) {
		ModelAndView mav = new ModelAndView();
		HttpSession httpSession = request.getSession(false);
		if (httpSession != null) {
			// User user = (User)httpSession.getAttribute("user");

			Session session = CommonUtil.getSession(sessionFactory);

			User user = null;
			try {
				user = userDao.findByUserName(request.getUserPrincipal()
						.getName(), session);
			} catch (Exception e) {

				mav.addObject("error", CommonUtil.exceptionHandler(e));
			}
			mav.addObject("owner", user);
			SpecialistDetails specialistDetails = specialistDetailsDao
					.fetchSpecialistDetails(session, user);
			weekSchedule = availabilityDao.getWeekSchedule(user.getId(),
					session, specialistDetails.getAvailstart(),
					specialistDetails.getAvailend());
			mav.addObject("schedule", weekSchedule);
			mav.setViewName("availability");
		}
		return mav;
	}

}

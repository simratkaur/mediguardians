package com.medicine.arvene.controller.web;

import java.text.ParseException;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.amazonaws.util.json.JSONObject;
import com.medicine.arvene.dao.BookingDao;
import com.medicine.arvene.dao.ContactUsDao;
import com.medicine.arvene.dao.ConversationDao;
import com.medicine.arvene.dao.QueryDao;
import com.medicine.arvene.dao.ReportsDao;
import com.medicine.arvene.dao.SpecialistDao;
import com.medicine.arvene.dao.SpecialistDetailsDao;
import com.medicine.arvene.dao.SpecialityDao;
import com.medicine.arvene.dao.UserDao;
import com.medicine.arvene.model.Booking;
import com.medicine.arvene.model.BookingReport;
import com.medicine.arvene.model.ContactUsForm;
import com.medicine.arvene.model.Conversation;
import com.medicine.arvene.model.Query;
import com.medicine.arvene.model.Report;
import com.medicine.arvene.model.SpecialistDetails;
import com.medicine.arvene.model.Speciality;
import com.medicine.arvene.model.User;
import com.medicine.arvene.util.CommonUtil;
import com.medicine.arvene.util.Mailer;

@Controller
public class WebUserController {
	@Autowired
	private UserDao userDao;
	@Autowired
	private SessionFactory sessionFactory;
	@Autowired
	private BookingDao bookingDao;

	@Autowired
	private SpecialistDetailsDao specialistDetailsDao;

	@Autowired
	QueryDao queryDao;

	@Autowired
	private SpecialistDao specialistDao;

	@Autowired
	private SpecialityDao specialityDao;

	@Autowired
	private ContactUsDao contactUsDao;

	@Autowired
	WebProfileController userDetailCtrl;

	// for 403 access denied page
	@Transactional
	@RequestMapping(value = "/403")
	public ModelAndView accesssDenied() {

		ModelAndView model = new ModelAndView();

		// check if user is login
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		if (auth != null && !(auth instanceof AnonymousAuthenticationToken)) {
			UserDetails userDetail = (UserDetails) auth.getPrincipal();
			model.addObject("username", userDetail.getUsername());
		}

		model.setViewName("403");
		return model;

	}

	@RequestMapping(value = "/contactUs")
	public ModelAndView contactUs(HttpServletRequest request) {
		ModelAndView model = new ModelAndView();
		ContactUsForm contactUsForm = new ContactUsForm();

		Session session = CommonUtil.getSession(sessionFactory);
		User user = null;
		try {
			user = userDao.findByUserName(request.getUserPrincipal().getName(),
					session);
		} catch (Exception e) {
			model.addObject("error", CommonUtil.exceptionHandler(e));
		}

		if (user != null) {

			contactUsForm.setEmail(user.getUsername());
		}

		model.addObject("contactUsForm", contactUsForm);
		model.setViewName("contactUs");
		return model;
	}

	@RequestMapping(value = "/contactUs", method = RequestMethod.POST)
	public ModelAndView contactUsSubmit(
			@ModelAttribute("contactUsForm") ContactUsForm contactUsForm,
			HttpServletRequest request, HttpServletResponse response) {
		ModelAndView model = new ModelAndView("login");

		Session session = CommonUtil.getSession(sessionFactory);

		try {

			Integer id = contactUsDao.saveContactUsForm(session, contactUsForm);

			if (id > 0) {
				model.setViewName("redirect:thankyou");
				// send a mail to admin
				new Mailer().sendEnquiryMail(contactUsForm);

			} else {
				model.addObject("error",
						"Something went wrong, Please try again later.");

			}

		} catch (Exception e) {
			model.addObject("error", CommonUtil.exceptionHandler(e));
		}

		return model;
	}

	@Transactional
	@RequestMapping(value = "/forgotPwd", method = RequestMethod.GET)
	public String forgotPwd(Map<String, Object> model,
			HttpServletRequest request) {
		User userForm = new User();
		model.put("userForm", userForm);
		return "forgotPwd";
	}

	// customize the error message
	private String getErrorMessage(HttpServletRequest request, String key) {
		Exception exception = (Exception) request.getSession()
				.getAttribute(key);
		String error = "";
		try {
			if (exception instanceof BadCredentialsException) {
				error = "Invalid username and password!";
			} else {
				error = exception.getMessage();
			}
		} catch (Exception e) {
			error = "";
		}
		return error;
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public UserDao getUserDao() {
		return userDao;
	}

	public String loadHomeSpecialist(ModelAndView model,
			HttpServletRequest request, User user) throws Exception {
		Session session = CommonUtil.getSession(sessionFactory);
		int bookings = bookingDao.fetchBookingCountByOwner(user, session);
		model.addObject("bookings_count", bookings);

		SpecialistDetails specialistDetails = specialistDetailsDao
				.fetchSpecialistDetails(session, user);

		if (specialistDetails != null) {
			int queries = 0;
			// TODO commenting this temporarily
			queries = queryDao.fetchQueryCountBySpecialist(specialistDetails.getId(),
					session);
			model.addObject("specialistDetails", specialistDetails);
			model.addObject("query_count", queries);
		}

		model.addObject("user", user);
		return "main";
	}

	@Transactional
	@RequestMapping(value = { "/", "/login" }, method = RequestMethod.GET)
	public ModelAndView login(
			@RequestParam(value = "error", required = false) String error,
			@RequestParam(value = "logout", required = false) String logout,
			@RequestParam(value = "forgotPwd", required = false) String forgotPwd,
			@RequestParam(value = "token", required = false) String token,
			HttpServletRequest request) {
		String result = "login";
		ModelAndView model = new ModelAndView();
		if (error != null) {
			String temp = getErrorMessage(request,
					"SPRING_SECURITY_LAST_EXCEPTION");
			if (temp == null)
				model.addObject("error", "Invalid Credentials !!!!!");
			else if (temp.equals(""))
				model.addObject("error", error);
			else
				model.addObject("error", temp);
		}

		if (logout != null) {
			model.addObject("msg", "You've been logged out successfully.");
		}
		if (token != null) {
			model.addObject("msg",
					"User verified successfully, Please Login!!!!");
		}
		Session session = CommonUtil.getSession(sessionFactory);
		User user = null;

		try {
			if (request.getUserPrincipal() != null) {
				user = userDao.findByUserName(request.getUserPrincipal()
						.getName(), session);
				request.getSession().setAttribute("userType",
						user.getUserType());
				if (request.getSession().getAttribute("sessionQuery") != null) {
					Query query = (Query) request.getSession().getAttribute(
							"sessionQuery");

					// added for saving user details in Query
					query.setUser(user);

					Integer id = queryDao.saveQuery(session, query);
					if (id != 0) {
						
						String conversationtxt = query.getNewConversation();
						
						Conversation conversation = new Conversation();
						
						conversation.setText(conversationtxt);
						conversation.setQuery(query);
						conversation.setUser(user);
						conversation.setType(user.getUserType());
						try {
							conversation.setCreatedAt(CommonUtil.getIstDateTime());
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							conversation.setCreatedAt(new Date());
						}
						
						
						int convId = conversationDao.saveConversation(session, conversation);
						conversation.setId(convId);

						if (query.getFiles() != null
								&& !query.getFiles().isEmpty()) {
							for (Iterator<MultipartFile> iterator = query
									.getFiles().iterator(); iterator.hasNext();) {
								MultipartFile attachment = (MultipartFile) iterator
										.next();
								Report report = new Report();
								report.setUrl(CommonUtil.uploadImage(
										attachment.getBytes(),
										attachment
												.getOriginalFilename()
												.substring(
														attachment
																.getOriginalFilename()
																.lastIndexOf(
																		'.') + 1)));
								report.setConversation(conversation);
								report.setUser(user);
								report.setFileName(attachment
										.getOriginalFilename());
								conversation.getReports().add(report);
							}
							reportsDao.saveReports(session,
									conversation.getReports());

						}

						model.addObject("id", id);
						model.addObject("fee", query.getCost());
						model.addObject("isQuery", true);
						result = "redirect:queryPayment";
					}
				}
				if (request.getSession().getAttribute("sessionBooking") != null) {
					Booking booking = (Booking) request.getSession()
							.getAttribute("sessionBooking");
					booking.setSubscriberid(user.getId());
					booking.setSpecialistDetails(specialistDetailsDao
							.fetchSpecialistDetails(session, booking.getSlot()
									.getUser()));
					booking = bookingDao.addBooking(booking, session);
					if (booking != null) {
						
						if(booking.getFiles()!=null && !booking.getFiles().isEmpty()){
							for (Iterator<MultipartFile> iterator = booking.getFiles()
									.iterator(); iterator.hasNext();) {
								MultipartFile attachment = (MultipartFile) iterator.next();
								BookingReport report = new BookingReport();
								report.setUrl(CommonUtil.uploadImage(
										attachment.getBytes(),
										attachment.getOriginalFilename().substring(
												attachment.getOriginalFilename()
														.lastIndexOf('.') + 1)));
								report.setBooking(booking);
								report.setUser(user);
								report.setFileName(attachment.getOriginalFilename());
								booking.getReports().add(report);
							}	
							
							bookingDao.saveReports(session, booking.getReports());
							
							
						}
						
						//do not update for Request slot
						if(booking.getSlot().getStatus().equalsIgnoreCase("A")){
							org.hibernate.Query query = session
									.createQuery(
											"Update Slot set status =? where id = ?")
									.setParameter(0, "B")
									.setParameter(1, booking.getSlotid());
							query.executeUpdate();
						}
						
						model.addObject("id", booking.getId());
						SpecialistDetails owner = specialistDetailsDao
								.fetchSpecialistDetails(session, booking
										.getSlot().getOwnerid());
						model.addObject("fee", owner.getSessionFee());
						model.addObject("isQuery", false);
						result = "redirect:queryPayment";
					} else {
						model.addObject("errortitle", "Booking got Failed!");
						model.addObject("error",
								"Something went wrong while we were booking your session. we regret for the"
										+ "inconvenience caused");
					}
				} else if (user.getUserType().equalsIgnoreCase("S")) {
					result = loadHomeSpecialist(model, request, user);
				} else if (user.getUserType().equalsIgnoreCase("A")) {
					request.getSession().setAttribute("userCount",
							userDao.fetchActiveUserCount(session));
					result = "redirect:allSpecialist";
				}
			}
			Map<String, String> specialityValues = specialityDao
					.fetchSpecialities(session);
			model.addObject("specialities", specialityValues);
			model.addObject("speciality", new Speciality());
			Map<String, String> specialityMap = specialityDao
					.fetchSpecialities(session);
			JSONObject jsonObj = new JSONObject(specialityMap);

			model.addObject("specialityMap", jsonObj);

		} catch (Exception e) {
			model.addObject("error", CommonUtil.exceptionHandler(e));
		} finally {
			session.close();
		}
		model.setViewName(result);

		return model;
	}

	@Autowired
	ConversationDao conversationDao;

	@Autowired
	ReportsDao reportsDao;

	public ReportsDao getReportsDao() {
		return reportsDao;
	}

	public void setReportsDao(ReportsDao reportsDao) {
		this.reportsDao = reportsDao;
	}

	public ConversationDao getConversationDao() {
		return conversationDao;
	}

	public void setConversationDao(ConversationDao conversationDao) {
		this.conversationDao = conversationDao;
	}

	@Transactional
	@RequestMapping(value = "/passwordReset", method = RequestMethod.GET, params = {
			"token", "email" })
	public @ResponseBody ModelAndView passwordResetToken(
			@RequestParam(value = "token") String token,
			@RequestParam(value = "email") String email) throws Exception {
		ModelAndView model = new ModelAndView();
		Session session = CommonUtil.getSession(sessionFactory);
		try {
			User user = userDao.findByUserName(email, session);
			if (!token.equals(user.getVerificationCode())) {
				model.addObject("error", "Verification token is not valid!!!!");
			} else if (!user.getIsActive()) {
				model.addObject("error", "User is not Active!!!!");
			}
			model.addObject("user", user);
			model.setViewName("passwordReset");
		} finally {
			session.close();
		}
		return model;
	}

	@RequestMapping(value = "/privacyPolicy")
	public ModelAndView privacyPolicy() {
		ModelAndView model = new ModelAndView();
		model.setViewName("privacyPolicy");
		return model;
	}

	@Transactional
	@RequestMapping(method = RequestMethod.POST, value = "/passwordReset")
	public ModelAndView pwdReset(@ModelAttribute("user") User user,
			HttpServletRequest request, HttpServletResponse response) {
		ModelAndView model = new ModelAndView();
		Session session = CommonUtil.getSession(sessionFactory);
		try {
			String pwd = user.getPassword();
			user = userDao.findByUserName(user.getUsername(), session);
			user.setPassword(new BCryptPasswordEncoder().encode(pwd));
			userDao.updateUser(user, session);
			model.addObject("msg", "Password Reset Done, Please login");
			model.setViewName("login");
		} catch (Exception e) {
			model.addObject("error", CommonUtil.exceptionHandler(e));
		} finally {
			session.close();
		}
		return model;
	}

	@Transactional
	@RequestMapping(method = RequestMethod.POST, value = "/resendOtp")
	public @ResponseBody String sendOtp(Map<String, Object> model,
			HttpServletRequest request,
			@ModelAttribute("username") String username) {
		String msg = "";
		int otp = 100000 + new Random().nextInt(900000);
		User user = null;
		Session session = CommonUtil.getSession(sessionFactory);
		try {
			user = userDao.findByUserName(username, session);
			String response = CommonUtil.sendMsg(
					CommonUtil.getOTPMsgContent(username, otp),
					String.valueOf(user.getPhone()));
			String temp = response.substring(response.indexOf("ErrorMessage"));
			msg = temp.substring(temp.indexOf(":") + 1, temp.indexOf(","));
			user.setOTP(otp);
			userDao.updateUser(user, session);
			model.put("msg", response);
		} catch (Exception e) {
			msg = CommonUtil.exceptionHandler(e);
			model.put("error", msg);
		} finally {
			session.close();
		}
		if (user == null)
			user = new User();
		model.put("user", user);
		return otp + "," + msg;
	}

	@Transactional
	@RequestMapping(method = RequestMethod.POST, value = "/forgotPwd")
	public ModelAndView sendPwd(@ModelAttribute("userForm") User user,
			HttpServletRequest request, HttpServletResponse response) {
		ModelAndView model = new ModelAndView();
		Session session = CommonUtil.getSession(sessionFactory);
		try {
			user = userDao.findByUserName(user.getUsername(), session);
			user.setVerificationCode(new BCryptPasswordEncoder().encode(String
					.valueOf(Math.random())));
			userDao.updateUser(user, session);
			new Mailer().sendPwdMail(user, request);
			model.addObject("msg", "Password Reset Mail sent");
			model.setViewName("login");
		} catch (Exception e) {
			model.addObject("error", CommonUtil.exceptionHandler(e));
		} finally {
			session.close();
		}
		return model;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

}
package com.medicine.arvene.dao;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.medicine.arvene.model.Availability;
import com.medicine.arvene.model.User;
import com.medicine.arvene.model.WeekSchedule;
import com.medicine.arvene.util.CommonUtil;

@Component
public class AvailabilityDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private SlotDao slotDao;

	@SuppressWarnings("unchecked")
	public List<Availability> fetchAvailability(int userid, Session session) {
		List<Availability> availabilityList = new ArrayList<Availability>();
		availabilityList = session
				.createQuery("from Availability where userid=?")
				.setParameter(0, userid).list();
		return availabilityList;
	}

	public void generateAvailability(int userid, Date startDate, Date endDate,
			HashMap<Integer, List<String>> weekSessions, Session session)
			throws ParseException {
		List<Availability> availabilityList = new ArrayList<Availability>();
		Iterator<Integer> keySetIterator = weekSessions.keySet().iterator();

		while (keySetIterator.hasNext()) {
			Integer dayid = keySetIterator.next();
			List<String> timings = weekSessions.get(dayid);
			for (String timing : timings) {
				Availability availability = new Availability();
				availability.setUserid(userid);
				availability.setDayid(dayid);
				availability.setStarttime(timing);
				availability.setCreated(new Date());
				availabilityList.add(availability);
			}
		}
		Transaction tx = session.beginTransaction();
		for (int i = 0; i < availabilityList.size(); i++) {
			session.save(availabilityList.get(i));
			if (i % 20 == 0) { // 20, same as the JDBC batch size
				// flush a batch of inserts and release memory:
				session.flush();
				session.clear();
			}
		}
		tx.commit();
		// delete slots that are not booked - since new availability schedule
		// has to be published
		Query query = session
				.createQuery("delete Slot where ownerid = ? and status = ?")
				.setParameter(0, userid).setParameter(1, "A");
		query.executeUpdate();
		for (int i = 0; i < 7; i++) {
			int dayofweek = CommonUtil.getDayOfWeekFromDate(startDate);
			List<String> timings = weekSessions.get(dayofweek);
			if (timings != null) {
				for (String timing : timings) {
					slotDao.generateSlots(startDate, endDate, timing, userid,
							session);
				}
			}
			Calendar cal = Calendar.getInstance();
			cal.setTime(startDate);
			cal.add(Calendar.DATE, 1);
			startDate = cal.getTime();
		}
		session.close();
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public WeekSchedule getWeekSchedule(int userid, Session session,
			Date availStart, Date availEnd) {
		WeekSchedule schedule = new WeekSchedule();
		// fetch availability start and end period of the user
		schedule.setStartDate(availStart);
		schedule.setEndDate(availEnd);
		List<Availability> availabilityList = fetchAvailability(userid, session);
		for (Availability availability : availabilityList) {
			StringBuilder builder = new StringBuilder();
			// Sunday
			if (availability.getDayid() == 1) {
				if (schedule.getSuntimings() != null) {
					builder.append(schedule.getSuntimings() + ","
							+ availability.getStarttime());
					schedule.setSuntimings(builder.toString());
				} else
					schedule.setSuntimings(availability.getStarttime());
			}
			// Monday
			else if (availability.getDayid() == 2) {
				if (schedule.getMontimings() != null) {
					builder.append(schedule.getMontimings() + ","
							+ availability.getStarttime());
					schedule.setMontimings(builder.toString());
				} else
					schedule.setMontimings(availability.getStarttime());
			}
			// Tuesday
			else if (availability.getDayid() == 3) {
				if (schedule.getTuetimings() != null) {
					builder.append(schedule.getTuetimings() + ","
							+ availability.getStarttime());
					schedule.setTuetimings(builder.toString());
				} else
					schedule.setTuetimings(availability.getStarttime());
			}
			// Wednesday
			else if (availability.getDayid() == 4) {
				if (schedule.getWedtimings() != null) {
					builder.append(schedule.getWedtimings() + ","
							+ availability.getStarttime());
					schedule.setWedtimings(builder.toString());
				} else
					schedule.setWedtimings(availability.getStarttime());
			}
			// Thursday
			else if (availability.getDayid() == 5) {
				if (schedule.getThutimings() != null) {
					builder.append(schedule.getThutimings() + ","
							+ availability.getStarttime());
					schedule.setThutimings(builder.toString());
				} else
					schedule.setThutimings(availability.getStarttime());
			}
			// Friday
			else if (availability.getDayid() == 6) {
				if (schedule.getFritimings() != null) {
					builder.append(schedule.getFritimings() + ","
							+ availability.getStarttime());
					schedule.setFritimings(builder.toString());
				} else
					schedule.setFritimings(availability.getStarttime());
			}
			// Saturday
			else if (availability.getDayid() == 7) {
				if (schedule.getSattimings() != null) {
					builder.append(schedule.getSattimings() + ","
							+ availability.getStarttime());
					schedule.setSattimings(builder.toString());
				} else
					schedule.setSattimings(availability.getStarttime());
			}
		}
		return schedule;

	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public void updateAvailabilityPeriod(User userid, Session session,
			Date start, Date end) {
		Query query = session
				.createQuery(
						"Update SpecialistDetails set availstart = ?,availend = ? where user = ?")
				.setDate(0, start).setDate(1, end).setParameter(2, userid);
		query.executeUpdate();
	}

}

package com.medicine.arvene.dao;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.medicine.arvene.model.Booking;
import com.medicine.arvene.model.BookingReport;
import com.medicine.arvene.model.PaymentDetails;
import com.medicine.arvene.model.Report;
import com.medicine.arvene.model.User;
import com.medicine.arvene.util.CommonUtil;
import com.medicine.arvene.util.IPaymentConstants;

@Component
public class BookingDao {

	@Autowired
	SessionFactory sessionFactory;

	public List<Booking> fetchBookingRequestForSubscriber(User user,
			Session session) throws HibernateException, ParseException {
		List<Booking> bookings = new ArrayList<Booking>();
		Query query = session
				.createQuery(
						"from Booking where subscriber=? and slot.status = ? and isActive=true")
				.setParameter(0, user).setString(1, "R");

		bookings = query.list();
		if (bookings.size() > 0) {
			return bookings;
		} else {
			return null;
		}
	}

	public Booking addBooking(Booking booking, Session session)
			throws ParseException {

		booking.setSlotBookingTime(CommonUtil.getIstDateTime());
		booking.setCreated(new Date());
		Integer bookingid = (Integer) session.save(booking);
		session.flush();
		session.refresh(booking);
		Booking bookingEntity = (Booking) session.get(Booking.class, bookingid);
		return bookingEntity;
	}

	public void deleteFailedBookings(Session session,
			List<Integer> bookingIdsList) {

		Query query = session.createQuery(
				"delete from Booking where id IN (:bookingIds)")
				.setParameterList("bookingIds", bookingIdsList);
		query.executeUpdate();

	}

	public int fetchBookingCountByOwner(User user, Session session)
			throws HibernateException, ParseException {
		int count = ((Long) session
				.createQuery(
						"select count(*) from Booking where slot.user=? and slot.enddate >=? and isActive=true")
				.setParameter(0, user).setDate(1, CommonUtil.getIstDateTime())
				.uniqueResult()).intValue();
		return count;
	}

	@SuppressWarnings("unchecked")
	public List<Booking> fetchBookingHistBySubscriber(User user,
			Session session, int start, int range) throws HibernateException,
			ParseException {
		List<Booking> bookings = new ArrayList<Booking>();
		Query query = session
				.createQuery(
						"from Booking where subscriber=? and slot.enddate <? and isActive=true")
				.setParameter(0, user).setDate(1, CommonUtil.getIstDateTime());
		query.setFirstResult(start);
		query.setMaxResults(range);
		bookings = query.list();
		if (bookings.size() > 0) {
			return bookings;
		} else {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Booking> fetchBookingHistoryByOwner(User user, Session session,
			int start, int range) throws HibernateException, ParseException {
		List<Booking> bookings = new ArrayList<Booking>();
		Query query = session
				.createQuery(
						"from Booking where slot.user=? and slot.enddate <? and isActive=true")
				.setParameter(0, user)
				.setParameter(1, CommonUtil.getIstDateTime());
		query.setFirstResult(start);
		query.setMaxResults(range);
		bookings = query.list();
		if (bookings.size() > 0) {
			return bookings;
		} else {
			return null;
		}
	}

	/**
	 * ajax call api data service - not used now
	 * 
	 * @param user
	 * @param session
	 * @return
	 * @throws ParseException
	 * @throws HibernateException
	 */
	public List<Booking> fetchBookings(User user, Session session)
			throws HibernateException, ParseException {
		List<Booking> bookings = new ArrayList<Booking>();
		bookings = session
				.createQuery(
						"from Booking where subscriber=? and slot.enddate >=? and isActive=true")
				.setParameter(0, user).setDate(1, CommonUtil.getIstDateTime())
				.list();
		if (bookings.size() > 0) {
			return bookings;
		} else {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Booking> fetchBookingsByOwner(User user, Session session,
			int start, int range) throws HibernateException, ParseException {
		List<Booking> bookings = new ArrayList<Booking>();
		Query query = session
				.createQuery(
						"from Booking where slot.user=? and slot.enddate >=? and isActive=true")
				.setParameter(0, user)
				.setParameter(1, CommonUtil.getIstDateTime());
		query.setFirstResult(start);
		query.setMaxResults(range);
		bookings = query.list();
		if (bookings.size() > 0) {
			return bookings;
		} else {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Booking> fetchBookingsBySubscriber(User user, Session session,
			int start, int range) throws HibernateException, ParseException {
		List<Booking> bookings = new ArrayList<Booking>();
		Query query = session
				.createQuery(
						"from Booking where subscriber=? and slot.enddate >=? and isActive=true")
				.setParameter(0, user).setDate(1, CommonUtil.getIstDateTime());
		query.setFirstResult(start);
		query.setMaxResults(range);
		bookings = query.list();
		if (bookings.size() > 0) {
			return bookings;
		} else {
			return null;
		}
	}

	public List<Booking> fetchInactiveBookings(Session session) {

		List<Booking> bookings = new ArrayList<Booking>();
		bookings = session.createQuery("from Booking where isActive=false")
				.list();
		if (bookings.size() > 0) {
			return bookings;
		} else {
			return null;
		}
	}

	public int getPageCount(Session session, int type, User user)
			throws HibernateException, ParseException {
		int pagenumber = 0;
		int count = 0;
		if (type == 1) {
			count = ((Long) session
					.createQuery(
							"select count(*) from Booking where subscriber=? and slot.enddate >=? and isActive=true")
					.setParameter(0, user)
					.setDate(1, CommonUtil.getIstDateTime()).uniqueResult())
					.intValue();
		} else {
			count = ((Long) session
					.createQuery(
							"select count(*) from Booking where slot.user=? and slot.enddate >=? and isActive=true")
					.setParameter(0, user)
					.setDate(1, CommonUtil.getIstDateTime()).uniqueResult())
					.intValue();
		}
		if (count % 10 == 0)
			pagenumber = count / 10;
		else
			pagenumber = (count / 10) + 1;
		if (pagenumber == 0)
			pagenumber = 1;
		return pagenumber;
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public int updatePaymentId(Session session, int bookingId,
			PaymentDetails paymentDetails) {

		boolean isActive = false;
		if (paymentDetails.getStatus().equalsIgnoreCase(
				IPaymentConstants.PAYPAL_PAYMENT_STATUS_COMPLETED)) {
			isActive = true;
		}
		org.hibernate.Query query = session
				.createQuery(
						"Update Booking set paymentDetails = ?,isActive=? where id = ?")
				.setEntity(0, paymentDetails).setInteger(2, bookingId)
				.setBoolean(1, isActive);
		return query.executeUpdate();

	}

	public boolean saveReports(Session session, List<BookingReport> reports) {
		// TODO Auto-generated method stub

		boolean flag = false;
		for (BookingReport report : reports) {

			int id = (Integer) session.save(report);
			if (id > 0) {
				flag = true;
			} else {
				flag = false;
			}
		}

		return flag;

	}

	public boolean saveReport(Session session, BookingReport report) {
		// TODO Auto-generated method stub

		boolean flag = false;
		int id = (Integer) session.save(report);
		if (id > 0) {
			flag = true;
		} else {
			flag = false;
		}

		return flag;

	}

	public List<Booking> fetchBookingsForAdmin(Session session, int start,
			int range, int userId, int specialistId, String status) {

		List<Booking> bookings = null;

		String sqlStr = "from Booking where isActive = true";

		if (userId > 0) {
			sqlStr = sqlStr + " AND subscriberid =  " + userId;
		}

		if (specialistId > 0) {
			sqlStr = sqlStr + " AND specialistId =  " + specialistId;
		}

		if (!status.equalsIgnoreCase("")) {
			sqlStr = sqlStr + " AND slot.status = '" + status + "'";
		}

		org.hibernate.Query sql = session.createQuery(sqlStr);

		sql.setFirstResult(start);
		sql.setMaxResults(range);
		bookings = sql.list();

		return bookings;
	}

	public int fetchBookingsCountForAdmin(Session session, int start,
			int range, int userId, int specialistId, String status) {

		String sqlStr = "select count(*) from Booking where isActive = true";

		if (userId > 0) {
			sqlStr = sqlStr + " AND subscriberid =  " + userId;
		}

		if (specialistId > 0) {
			sqlStr = sqlStr + " AND specialistId =  " + specialistId;
		}

		if (!status.equalsIgnoreCase("")) {
			sqlStr = sqlStr + " AND slot.status = '" + status + "'";
		}

		int count = ((Long) session.createQuery(sqlStr).uniqueResult())
				.intValue();
		return count;
	}
}
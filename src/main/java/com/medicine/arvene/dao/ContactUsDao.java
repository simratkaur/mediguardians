/**
 * 
 */
package com.medicine.arvene.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.medicine.arvene.model.ContactUsForm;
import com.medicine.arvene.util.CommonUtil;

/**
 * @author maddie
 *
 */
@Component
public class ContactUsDao {

	@Autowired
	SessionFactory sessionFactory;

	public int archiveInquiry(Session session, int id) {

		int status = 0;

		Query query = session.createQuery(
				"Update ContactUsForm set isActive='0' where id = ?")
				.setInteger(0, id);
		status = query.executeUpdate();

		return status;
	}

	public int getActiveInquiriesCount(Session session) {
		int pagenumber = 0;

		Query query = session
				.createQuery("Select count(*) from ContactUsForm where isActive='1'");
		int count = ((Long) query.uniqueResult()).intValue();

		if (count % 10 == 0)
			pagenumber = count / 10;
		else
			pagenumber = (count / 10) + 1;
		if (pagenumber == 0)
			pagenumber = 1;
		return pagenumber;
	}

	public List<ContactUsForm> getAllActiveInquiries(Session session,
			int start, int range) {

		List<ContactUsForm> contactUsInquiries = null;
		Query query = session
				.createQuery("from ContactUsForm where isActive='1'");
		query.setFirstResult(start);
		query.setMaxResults(range);
		contactUsInquiries = query.list();

		return contactUsInquiries;

	}

	public int respondInquiry(Session session, int id, String response) {

		int status = 0;

		Query query = session
				.createQuery(
						"Update ContactUsForm set response = ? where id = ?")
				.setString(0, response).setInteger(1, id);
		status = query.executeUpdate();

		return status;
	}

	public Integer saveContactUsForm(Session session,
			ContactUsForm contactusForm) {

		Integer id = -1;

		if (session == null) {
			session = CommonUtil.getSession(sessionFactory);

		}

		id = (Integer) session.save(contactusForm);

		return id;

	}

}

/**
 * 
 */
package com.medicine.arvene.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.medicine.arvene.model.Conversation;
import com.medicine.arvene.model.PaymentDetails;
import com.medicine.arvene.model.Query;
import com.medicine.arvene.util.CommonUtil;

/**
 * @author maddie
 *
 */

@Component
public class ConversationDao {

	@Autowired
	SessionFactory sessionFactory;
	

	public Integer saveConversation(Session session, Conversation conversation) {
		Integer id = 0;
		id = (Integer) session.save(conversation);
		return id;
	}
	
	public void updateConversation(Session session, Conversation conversation) {
		if (session == null)
			session = CommonUtil.getSession(sessionFactory);
		session.merge(conversation);
		session.flush();
	}

	public List<Query> fetchAllNonResponsedQueries(Session session, int start,
			int range) {
		List<Query> queries = null;
		session = getFilter(session, "1", "0");
		org.hibernate.Query sql = session.createQuery("from Query");
		sql.setFirstResult(start);
		sql.setMaxResults(range);
		queries = sql.list();

		return queries;
	}
	


	public int fetchAllNonResponsedQueriesCount(Session session) {
		session = getFilter(session, "1", "0");
		int count = ((Long) session.createQuery("select count(*) from Query")
				.uniqueResult()).intValue();
		return count;
	}

	public int addResponse(Session session, int queryId, String response) {
		int status = 0;
		org.hibernate.Query query = session
				.createQuery("Update Query set response = ? where id = ?")
				.setString(0, response).setInteger(1, queryId);
		status = query.executeUpdate();
		return status;
	}

	public int fetchCountQueryByUser(Session session, int userId) {
		session = getFilter(session, "1", "-1");
		int count = ((Long) session
				.createQuery("Select count(*) from Query where userId=?")
				.setInteger(0, userId).uniqueResult()).intValue();
		int pagenumber;
		if (count % 10 == 0)
			pagenumber = count / 10;
		else
			pagenumber = (count / 10) + 1;
		if (pagenumber == 0)
			pagenumber = 1;
		return pagenumber;
	}

	public Session getFilter(Session session, String active, String responded) {

		if (!active.equalsIgnoreCase("-1")) {

			if (active.equalsIgnoreCase("1")) {
				session.enableFilter("QUERY_ACTIVE_FILTER").setParameter(
						"IS_ACTIVE", true);

			} else {
				session.enableFilter("QUERY_ACTIVE_FILTER").setParameter(
						"IS_ACTIVE", false);
			}
		}

		if (!responded.equalsIgnoreCase("-1")) {

			if (responded.equalsIgnoreCase("1")) {
				session.enableFilter("QUERY_RESP_FILTER").setParameter(
						"QUERY_RESP", true);

			} else {
				session.enableFilter("QUERY_RESP_FILTER").setParameter(
						"QUERY_RESP", false);
			}
		}

		return session;

	}

	public int fetchQueryCountBySpecialist(int specialistId, Session session) {
		session = getFilter(session, "1", "0");
		int count = ((Long) session
				.createQuery("select count(*) from Query where specialistId=?")
				.setParameter(0, specialistId).uniqueResult()).intValue();
		return count;
	}

	public List<Query> fetchQueryForSpecialist(Session session,
			int specialistId, int start, int range) {
		session = getFilter(session, "1", "0");
		List<Query> queries = null;

		org.hibernate.Query sql = session.createQuery(
				"from Query where specialistId=?").setInteger(0, specialistId);
		sql.setFirstResult(start);
		sql.setMaxResults(range);
		queries = sql.list();

		return queries;
	}

	public List<Query> fetchQueryForUser(Session session, int userId,
			int start, int range) {
		session = getFilter(session, "1", "-1");
		List<Query> queries = null;
		org.hibernate.Query sql = session.createQuery(
				"from Query where userId=?").setInteger(0, userId);
		sql.setFirstResult(start);
		sql.setMaxResults(range);
		queries = sql.list();

		return queries;
	}

	public List<Query> fetchQueryHistoryForSpecialist(Session session,
			int specialistId, int start, int range) {
		session = getFilter(session, "1", "1");
		List<Query> queries = null;

		org.hibernate.Query sql = session.createQuery(
				"from Query where specialistId=?").setInteger(0, specialistId);
		sql.setFirstResult(start);
		sql.setMaxResults(range);
		queries = sql.list();

		return queries;
	}

	public int getQueryHistoryCountBySpecialist(int specialistId,
			Session session) {
		session = getFilter(session, "1", "1");
		int count = ((Long) session
				.createQuery("select count(*) from Query where specialistId=?")
				.setParameter(0, specialistId).uniqueResult()).intValue();
		return count;
	}

	/**
	 * @return the sessionFactory
	 */
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public Integer saveQuery(Session session, Query query) {
		Integer id = 0;
		id = (Integer) session.save(query);
		return id;
	}

	/**
	 * @param sessionFactory
	 *            the sessionFactory to set
	 */
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public void updatePaymentId(Session session, int queryId, int paymentId) {

		org.hibernate.Query query = session
				.createQuery("Update Query set paymentId = ? where id = ?")
				.setInteger(0, paymentId).setInteger(1, queryId);
		query.executeUpdate();

	}

	public int updatePaymentId(Session session, int queryId,
			PaymentDetails paymentDetails) {

		int isActive = 0;
		if (paymentDetails.getStatus().equalsIgnoreCase("success")) {
			isActive = 1;
		}
		org.hibernate.Query query = session
				.createQuery(
						"Update Query set paymentDetails = ?, isActive = ? where id = ?")
				.setEntity(0, paymentDetails).setInteger(1, isActive)
				.setInteger(2, queryId);
		return query.executeUpdate();

	}

	

	

}

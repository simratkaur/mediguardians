package com.medicine.arvene.dao;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.medicine.arvene.model.NewDoc;
import com.medicine.arvene.util.CommonUtil;

@Component
@Transactional
public class NewDocDao {

	@Autowired
	private SessionFactory sessionFactory;

	public boolean addUser(NewDoc doc, Session session) throws Exception {
		boolean userAdded = false;
		if (session == null)
			session = CommonUtil.getSession(sessionFactory);
		Serializable check = session.save(doc);
		session.flush();
		if (Integer.valueOf(check.toString()) > 0)
			userAdded = true;
		return userAdded;
	}

	public int archiveInquiry(Session session, int id) {

		int status = 0;

		Query query = session.createQuery(
				"Update ContactUsForm set isActive='0' where id = ?")
				.setInteger(0, id);
		status = query.executeUpdate();

		return status;
	}

	public List<NewDoc> fetchNewDoctorDetails(Session session, int start,
			int range) throws Exception {

		List<NewDoc> newDoctors = null;
		if (session == null)
			session = CommonUtil.getSession(sessionFactory);

		Query query = session.createQuery("from NewDoc where isActive='1'");
		query.setFirstResult(start);
		query.setMaxResults(range);
		newDoctors = query.list();

		return newDoctors;
	}

	public int getNewDocCount(Session session) throws Exception {

		int pagenumber = 0;
		Query query = null;

		query = session
				.createQuery("Select count(*) from NewDoc where isActive='1'");
		int count = ((Long) query.uniqueResult()).intValue();

		if (count % 10 == 0)
			pagenumber = count / 10;
		else
			pagenumber = (count / 10) + 1;
		if (pagenumber == 0)
			pagenumber = 1;
		return pagenumber;

	}

}

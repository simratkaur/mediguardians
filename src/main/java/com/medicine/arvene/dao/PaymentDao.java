/**
 * 
 */
package com.medicine.arvene.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.medicine.arvene.model.PaymentDetails;
import com.medicine.arvene.util.CommonUtil;

/**
 * @author maddie
 *
 */
@Repository
@Transactional
public class PaymentDao {

	@Autowired
	private SessionFactory sessionFactory;

	public int savePaymentDetail(PaymentDetails paymentForm, Session session)
			throws Exception {
		int id;

		if (session == null) {
			session = CommonUtil.getSession(sessionFactory);
		}

		id = (int) session.save(paymentForm);

		return id;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

}

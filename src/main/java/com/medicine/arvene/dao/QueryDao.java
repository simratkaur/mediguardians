/**
 * 
 */
package com.medicine.arvene.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.medicine.arvene.model.Conversation;
import com.medicine.arvene.model.PaymentDetails;
import com.medicine.arvene.model.Query;
import com.medicine.arvene.util.CommonUtil;
import com.medicine.arvene.util.IPaymentConstants;

/**
 * @author maddie
 *
 */

@Component
public class QueryDao {

	@Autowired
	SessionFactory sessionFactory;

	public List<Query> fetchAllNonResponsedQueries(Session session, int start,
			int range) {
		List<Query> queries = null;
		session = getFilter(session, "1", "0");
		org.hibernate.Query sql = session.createQuery("from Query");
		sql.setFirstResult(start);
		sql.setMaxResults(range);
		queries = sql.list();

		return queries;
	}

	public int fetchAllNonResponsedQueriesCount(Session session) {
		session = getFilter(session, "1", "0");
		int count = ((Long) session.createQuery("select count(*) from Query")
				.uniqueResult()).intValue();
		return count;
	}

	public int addResponse(Session session, int queryId, String response) {
		int status = 0;
		org.hibernate.Query query = session
				.createQuery("Update Query set response = ? where id = ?")
				.setString(0, response).setInteger(1, queryId);
		status = query.executeUpdate();
		return status;
	}

	public int fetchCountQueryByUser(Session session, int userId) {
		session = getFilter(session, "1", "-1");
		int count = ((Long) session
				.createQuery("Select count(*) from Query where userId=?")
				.setInteger(0, userId).uniqueResult()).intValue();
		int pagenumber;
		if (count % 10 == 0)
			pagenumber = count / 10;
		else
			pagenumber = (count / 10) + 1;
		if (pagenumber == 0)
			pagenumber = 1;
		return pagenumber;
	}

	public Session getFilter(Session session, String active, String responded) {

		if (!active.equalsIgnoreCase("-1")) {

			if (active.equalsIgnoreCase("1")) {
				session.enableFilter("QUERY_ACTIVE_FILTER").setParameter(
						"IS_ACTIVE", true);

			} else {
				session.enableFilter("QUERY_ACTIVE_FILTER").setParameter(
						"IS_ACTIVE", false);
			}
		}

		if (!responded.equalsIgnoreCase("-1")) {

			if (responded.equalsIgnoreCase("1")) {
				session.enableFilter("QUERY_RESP_FILTER").setParameter(
						"QUERY_RESP", true);

			} else {
				session.enableFilter("QUERY_RESP_FILTER").setParameter(
						"QUERY_RESP", false);
			}
		}

		return session;

	}

	public int fetchQueryCountBySpecialist(int specialistId, Session session) {
		session = getFilter(session, "1", "0");
		int count = ((Long) session
				.createQuery("select count(*) from Query where specialistId=?")
				.setParameter(0, specialistId).uniqueResult()).intValue();
		return count;
	}

	public List<Query> fetchQueryForSpecialist(Session session,
			int specialistId, int start, int range) {
		session = getFilter(session, "1", "0");
		List<Query> queries = null;

		org.hibernate.Query sql = session.createQuery(
				"from Query where specialistId=?").setInteger(0, specialistId);
		sql.setFirstResult(start);
		sql.setMaxResults(range);
		queries = sql.list();

		return queries;
	}

	public List<Query> fetchQueryForUser(Session session, int userId,
			int start, int range) {
		session = getFilter(session, "1", "-1");
		List<Query> queries = null;
		org.hibernate.Query sql = session.createQuery(
				"from Query where userId=?").setInteger(0, userId);
		sql.setFirstResult(start);
		sql.setMaxResults(range);
		queries = sql.list();

		return queries;
	}

	public List<Query> fetchQueryHistoryForSpecialist(Session session,
			int specialistId, int start, int range) {
		session = getFilter(session, "1", "1");
		List<Query> queries = null;

		org.hibernate.Query sql = session.createQuery(
				"from Query where specialistId=?").setInteger(0, specialistId);
		sql.setFirstResult(start);
		sql.setMaxResults(range);
		queries = sql.list();

		return queries;
	}

	public int getQueryHistoryCountBySpecialist(int specialistId,
			Session session) {
		session = getFilter(session, "1", "1");
		int count = ((Long) session
				.createQuery("select count(*) from Query where specialistId=?")
				.setParameter(0, specialistId).uniqueResult()).intValue();
		return count;
	}

	/**
	 * @return the sessionFactory
	 */
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public Integer saveQuery(Session session, Query query) {
		Integer id = 0;
		id = (Integer) session.save(query);
		return id;
	}

	/**
	 * @param sessionFactory
	 *            the sessionFactory to set
	 */
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public void updatePaymentId(Session session, int queryId, int paymentId) {

		org.hibernate.Query query = session
				.createQuery("Update Query set paymentId = ? where id = ?")
				.setInteger(0, paymentId).setInteger(1, queryId);
		query.executeUpdate();

	}

	public int updatePaymentId(Session session, int queryId,
			PaymentDetails paymentDetails) {

		int isActive = 0;
		if (paymentDetails.getStatus().equalsIgnoreCase(
				IPaymentConstants.PAYPAL_PAYMENT_STATUS_COMPLETED)) {
			isActive = 1;
		}
		org.hibernate.Query query = session
				.createQuery(
						"Update Query set paymentDetails = ?, isActive = ? where id = ?")
				.setEntity(0, paymentDetails).setInteger(1, isActive)
				.setInteger(2, queryId);
		return query.executeUpdate();

	}

	public void updateQuery(Session session, Query query) {
		if (session == null)
			session = CommonUtil.getSession(sessionFactory);
		session.merge(query);
		session.flush();
	}

	public Integer saveConversation(Session session, Conversation conversation) {
		return (Integer) session.save(conversation);
	}

	public void updteConversation(Session session, Conversation conversation) {
		if (session == null)
			session = CommonUtil.getSession(sessionFactory);
		session.merge(conversation);
		session.flush();
	}

	public List<Query> fetchAllQueries(Session session, int start, int range) {
		// TODO Auto-generated method stub
		List<Query> queries = null;
		session = getFilter(session, "1", "-1");
		org.hibernate.Query sql = session.createQuery("from Query");
		sql.setFirstResult(start);
		sql.setMaxResults(range);
		queries = sql.list();
		return queries;
	}

	public int fetchAllQueriesCount(Session session) {
		// TODO Auto-generated method stub
		session = getFilter(session, "1", "-1");
		int count = ((Long) session.createQuery("select count(*) from Query")
				.uniqueResult()).intValue();
		return count;
	}

	public List<Query> fetchFilteredQueries(Session session, int start,
			int range, int userId, int specialistId, int responded) {
		// TODO Auto-generated method stub
		List<Query> queries = null;

		if (responded > 0) {
			session = getFilter(session, "1", String.valueOf(responded));
		} else {
			session = getFilter(session, "1", "-1");
		}

		String sqlStr = "from Query where id > 0";

		if (userId > 0) {
			sqlStr = sqlStr + " AND userId =  " + userId;
			// session.enableFilter("USER_FILTER").setParameter("USER_ID",
			// userId);
		}

		if (responded == -2) {
			sqlStr = sqlStr + " AND specialistId = null";
		} else if (specialistId > 0) {
			sqlStr = sqlStr + " AND specialistId = " + specialistId;
			// session.enableFilter("DOCTOR_FILTER").setParameter("DOCTOR_ID",
			// specialistId);
		}

		org.hibernate.Query sql = session.createQuery(sqlStr);

		sql.setFirstResult(start);
		sql.setMaxResults(range);
		queries = sql.list();
		return queries;

	}

	public int fetchFilteredQueriesCount(Session session, int userId,
			int specialistId, int responded) {
		// TODO Auto-generated method stub

		if (responded > 0) {
			session = getFilter(session, "1", String.valueOf(responded));
		} else {
			session = getFilter(session, "1", "-1");
		}

		String sqlStr = "select  count(*) from Query where id > 0";

		if (userId > 0) {
			sqlStr = sqlStr + " AND userId =  " + userId;
			// session.enableFilter("USER_FILTER").setParameter("USER_ID",
			// userId);
		}

		if (responded == -2) {
			sqlStr = sqlStr + " AND specialistId = null";
		} else if (specialistId > 0) {
			sqlStr = sqlStr + " AND specialistId = " + specialistId;
			// session.enableFilter("DOCTOR_FILTER").setParameter("DOCTOR_ID",
			// specialistId);
		}

		int count = ((Long) session.createQuery(sqlStr).uniqueResult())
				.intValue();
		return count;
	}

	public Query assignQueryToSpacialist(Session session, int queryId,
			int specialistId) {
		// TODO Auto-generated method stub

		if (session == null)
			session = CommonUtil.getSession(sessionFactory);
		org.hibernate.Query query = session
				.createQuery("Update Query set specialistId = ? where id = ?")
				.setInteger(0, specialistId).setInteger(1, queryId);
		query.executeUpdate();

		return (Query) session.get(Query.class, queryId);
	}

	public Conversation getLastConversation(int id, Session session) {
		org.hibernate.Query sql = session
				.createQuery(
						"from Conversation where queryId=? and type='U' order by id desc")
				.setInteger(0, id);
		sql.setMaxResults(1);
		List<Conversation> conversation = sql.list();
		if (conversation.size() > 0)
			return conversation.get(0);
		else
			return null;
	}

}

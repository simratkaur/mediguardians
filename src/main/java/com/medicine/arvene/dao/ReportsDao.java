/**
 * 
 */
package com.medicine.arvene.dao;

import java.util.List;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.medicine.arvene.model.Conversation;
import com.medicine.arvene.model.PaymentDetails;
import com.medicine.arvene.model.Query;
import com.medicine.arvene.model.Report;
import com.medicine.arvene.util.CommonUtil;

/**
 * @author maddie
 *
 */

@Component
public class ReportsDao {

	@Autowired
	SessionFactory sessionFactory;

	public boolean saveReports(Session session, Set<Report> reports) {

		boolean flag = false;
		for (Report report : reports) {

			int id = (Integer) session.save(report);
			if (id > 0) {
				flag = true;
			} else {
				flag = false;
			}
		}

		return flag;
	}

	public boolean saveReport(Session session, Report report) {

		boolean flag = false;

		int id = (Integer) session.save(report);
		if (id > 0) {
			flag = true;
		} else {
			flag = false;
		}
		return flag;
	}

	/**
	 * @param sessionFactory
	 *            the sessionFactory to set
	 */
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

}

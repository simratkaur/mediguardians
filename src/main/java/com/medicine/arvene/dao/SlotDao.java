package com.medicine.arvene.dao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.medicine.arvene.model.Booking;
import com.medicine.arvene.model.Slot;
import com.medicine.arvene.util.CommonUtil;

@Repository
@Transactional
public class SlotDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	
	public Slot saveSlot(Session session, Slot slot){
		
		slot.setCreated(new Date());
		Integer slotId = (Integer) session.save(slot);
		session.flush();
		session.refresh(slot);
		Slot slotEntity = (Slot) session.get(Slot.class, slotId);
		return slotEntity;
	}

	public List<Slot> fetchAvailableSlots(Session session, int ownerid,
			String start, String end) throws ParseException {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date startDate = dateFormat.parse(start);
		Date endDate = dateFormat.parse(end);
		if (startDate.before(CommonUtil.getIstDateTime()))
			startDate = CommonUtil.getIstDateTime();
		startDate.setHours(0);
		startDate.setMinutes(0);
		startDate.setSeconds(0);
		List<Slot> slots = null;
		Query query = session
				.createQuery(
						"From Slot where ownerid = ? and startdate >= ? and enddate <= ? and status = 'A'")
				.setParameter(0, ownerid).setParameter(1, startDate)
				.setParameter(2, endDate);
		slots = query.list();
		if (slots.size() > 0)
			return slots;
		return null;
	}

	public List<Slot> fetchSlots(Session session, int ownerid, String start,
			String end) throws ParseException {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date startDate = dateFormat.parse(start);
		Date endDate = dateFormat.parse(end);
		if (startDate.before(CommonUtil.getIstDateTime()))
			startDate = CommonUtil.getIstDateTime();
		startDate.setHours(0);
		startDate.setMinutes(0);
		startDate.setSeconds(0);
		List<Slot> slots = null;
		Query query = session
				.createQuery(
						"From Slot where ownerid = ? and startdate >= ? and enddate <= ?")
				.setParameter(0, ownerid).setParameter(1, startDate)
				.setParameter(2, endDate);
		slots = query.list();
		if (slots.size() > 0)
			return slots;
		return null;
	}

	public void generateSlots(Date startDate, Date endDate,
			String startTimeStr, int ownerid, Session session)
			throws ParseException {
		Date startDateClone = new Date(startDate.getTime());
		SimpleDateFormat dateFormatonly = new SimpleDateFormat("yyyy-MM-dd");
		String startDateWithoutTimeStr = dateFormatonly.format(startDateClone);
		SimpleDateFormat dateFormatWithTime = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm");
		String sessionStartDateWithTimeStr = startDateWithoutTimeStr + " "
				+ startTimeStr;
		Date sessionStartDateWithTime = dateFormatWithTime
				.parse(sessionStartDateWithTimeStr);
		List<Slot> slots = new ArrayList<Slot>();
		while (sessionStartDateWithTime.after(startDate)
				&& sessionStartDateWithTime.before(endDate)) {
			Slot slot = new Slot();
			slot.setOwnerid(ownerid);
			slot.setStartdate(sessionStartDateWithTime);
			DateTime jdStartTime = new DateTime(sessionStartDateWithTime);
			DateTimeFormatter dtfOut = DateTimeFormat
					.forPattern("dd/MM/yyyy HH:mm:ss");
			DateTime jdEndTime = jdStartTime.plusMinutes(15);
			slot.setEnddate(jdEndTime.toDate());
			slot.setStatus("A");
			sessionStartDateWithTime = jdStartTime.plusWeeks(1).toDate();
			slots.add(slot);
		}
		
		
		List<Slot> bookedSlots = null;
		Query query = session
				.createQuery(
						"From Slot where ownerid = ? and startdate >= ? and enddate <= ? and status = 'B'")
				.setParameter(0, ownerid).setParameter(1, startDate)
				.setParameter(2, endDate);
		bookedSlots = query.list();
		
		if(bookedSlots!=null && bookedSlots.size()>0){
			slots.removeAll(bookedSlots);

		}
		
		
		Transaction tx = session.beginTransaction();
		for (int i = 0; i < slots.size(); i++) {
			session.save(slots.get(i));
			if (i % 20 == 0) { // 20, same as the JDBC batch size
				// flush a batch of inserts and release memory:
				session.flush();
				session.clear();
			}
		}
		tx.commit();

	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public Integer updateFailedSlots(Session session, List<Integer> slotIds) {

		Integer rowsUpadted = 0;
		Query query = session
				.createQuery(
						"Update Slot set status = ? where id IN (:slotIds)")
							.setCharacter(0, 'A')
							.setParameterList("slotIds" , slotIds);

		rowsUpadted = query.executeUpdate();

		return rowsUpadted;
	}

	public Slot acceptBookingRequest(Session session, int slotId, Date startDateTmp, String startTimeStr,int ownerid) throws ParseException {
		// TODO Auto-generated method stub
		
		Integer rowsUpadted = 0;
		Slot slot = null;
		List<Slot> bookedSlots = null;
		

		Date startDateClone = new Date(startDateTmp.getTime());
		SimpleDateFormat dateFormatonly = new SimpleDateFormat("yyyy-MM-dd");
		String startDateWithoutTimeStr = dateFormatonly.format(startDateClone);
		
		SimpleDateFormat dateFormatWithTime = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm");
		String sessionStartDateWithTimeStr = startDateWithoutTimeStr + " "
				+ startTimeStr;
		Date startDate = dateFormatWithTime
				.parse(sessionStartDateWithTimeStr);

		DateTime jdStartTime = new DateTime(startDate);
		
		DateTime endTime = jdStartTime.plusMinutes(15);
		
		//check if the time for slot is not booked already
		Query checkQuery = session
				.createQuery(
						"From Slot where ownerid = ? and startdate >= ? and enddate <= ? and status = 'B'")
				.setParameter(0, ownerid).setParameter(1, startDate)
				.setParameter(2, endTime.toDate());
		bookedSlots = checkQuery.list();
		
		
		if(bookedSlots.isEmpty() ){
			
			slot = (Slot)session.get(Slot.class, slotId);
			
			slot.setStartdate(startDate);
			slot.setEnddate(endTime.toDate());
			slot.setStatus("B");
			
			slot = (Slot) session.merge(slot);
			session.flush();
			session.refresh(slot);

		}
		
		
		
		return slot;
	}

}

package com.medicine.arvene.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.medicine.arvene.model.SpecialistDetails;

@Component
public class SpecialistDao {

	@Autowired
	private SessionFactory sessionFactory;

	public List<SpecialistDetails> fetchSpecialists(Session session,
			Integer specialityid, int start, int range) {
		List<SpecialistDetails> specialists = null;
		Integer def = new Integer(-1);
		Query query = null;
		if (specialityid.equals(def)) {
			query = session
					.createQuery("from SpecialistDetails where user.isActive=true");
			query.setFirstResult(start);
			query.setMaxResults(range);
			specialists = query.list();
		} else {
			query = session
					.createQuery(
							"from SpecialistDetails where  user.isActive=true and specialityid = ?")
					.setParameter(0, specialityid);
			query.setFirstResult(start);
			query.setMaxResults(range);
			specialists = query.list();
		}
		if (specialists.size() > 0)
			return specialists;
		return null;
	}

	public int getPageCount(Session session, int specialityid) {
		int count = 0;
		int pagenumber = 0;
		Query query = null;
		if (specialityid == -1) {
			query = session
					.createQuery("select count(*)from SpecialistDetails where  user.isActive=true ");
			count = ((Long) query.uniqueResult()).intValue();
		} else {
			query = session
					.createQuery(
							"select count(*) from SpecialistDetails where  user.isActive=true and specialityid = ?")
					.setParameter(0, specialityid);
			count = ((Long) query.uniqueResult()).intValue();
		}
		if (count % 10 == 0)
			pagenumber = count / 10;
		else
			pagenumber = (count / 10) + 1;
		if (pagenumber == 0)
			pagenumber = 1;
		return pagenumber;
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

}

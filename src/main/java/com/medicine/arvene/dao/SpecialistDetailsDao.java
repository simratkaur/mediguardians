/**
 * 
 */
package com.medicine.arvene.dao;

import java.sql.Timestamp;
import java.text.ParseException;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.medicine.arvene.model.SpecialistDetails;
import com.medicine.arvene.model.User;
import com.medicine.arvene.util.CommonUtil;

/**
 * @author maddie
 *
 */
@Component
public class SpecialistDetailsDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	
	public Map<String, String> fetchAllActiveSpecialists(Session session) {
		List<SpecialistDetails> specialists = null;
		
		Query query = null;

		query = session
					.createQuery("from SpecialistDetails where user.isActive=true");
		specialists = query.list();
	
		
		Map<String, String> userMap = new LinkedHashMap<String, String>();
		for (SpecialistDetails specialist : specialists) {
			userMap.put(specialist.getId() + "", specialist.getUser().getUsername());
		}
		return userMap;
		
	}

	public Integer addSpecialistDetails(Session session,
			SpecialistDetails specialistDetails) {

		if (session == null) {
			session = CommonUtil.getSession(sessionFactory);
		}
		specialistDetails.setCreated(new Timestamp(new Date().getTime()));

		Integer id = (Integer) session.save(specialistDetails);

		return id;

	}

	public void deleteRecord(int userId, Session session) {
		session.createQuery("delete from SpecialistDetails where id=?")
				.setParameter(0,
						fetchSpecialistDetails(session, userId).getId())
				.executeUpdate();
	}

	public List<SpecialistDetails> fetchAllNewSpecialists(Session session,
			int start, int range) {

		List<SpecialistDetails> allSpecialists = null;

		if (session == null) {
			session = CommonUtil.getSession(sessionFactory);
		}

		Query query = session
				.createQuery("from SpecialistDetails where user.isActive=false");
		query.setFirstResult(start);
		query.setMaxResults(range);
		allSpecialists = query.list();
		return allSpecialists;
	}

	public SpecialistDetails fetchSpecialistDetails(Session session, int user) {

		List<SpecialistDetails> specialistDetails = null;

		if (session == null)
			session = CommonUtil.getSession(sessionFactory);

		specialistDetails = session
				.createQuery("from SpecialistDetails where userId=?")
				.setParameter(0, user).list();

		if (specialistDetails.size() > 0) {
			return specialistDetails.get(0);
		} else {
			return null;
		}

	}

	public SpecialistDetails fetchSpecialistDetails(Session session, User user) {

		List<SpecialistDetails> specialistDetails = null;

		if (session == null)
			session = CommonUtil.getSession(sessionFactory);

		specialistDetails = session
				.createQuery("from SpecialistDetails where user=?")
				.setParameter(0, user).list();

		if (specialistDetails.size() > 0) {
			return specialistDetails.get(0);
		} else {
			return null;
		}

	}

	public int getPageCount(Session session) {
		int pagenumber = 0;
		Query query = null;

		query = session
				.createQuery("Select count(*) from SpecialistDetails where user.isActive=false");
		int count = ((Long) query.uniqueResult()).intValue();

		if (count % 10 == 0)
			pagenumber = count / 10;
		else
			pagenumber = (count / 10) + 1;
		if (pagenumber == 0)
			pagenumber = 1;
		return pagenumber;
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public SpecialistDetails prepareUpdatedEntity(SpecialistDetails user,
			Session session) throws ParseException {
		SpecialistDetails persistedUser = (SpecialistDetails) CommonUtil
				.getEntity(session, SpecialistDetails.class, user.getId());

		persistedUser.setTitle(user.getTitle());
		persistedUser.setFirstName(user.getFirstName());
		persistedUser.setLastName(user.getLastName());
		persistedUser.setMiddleName(user.getMiddleName());
		persistedUser.setDegrees(user.getDegrees());

		persistedUser.setSubSpeciality(user.getSubSpeciality());
		persistedUser.setAreaOfInterest(user.getAreaOfInterest());
		persistedUser.setHospital(user.getHospital());
		persistedUser.setHospitalCity(user.getHospitalCity());
		persistedUser.setMobile(user.getMobile());
		persistedUser.setBiography(user.getBiography());
		persistedUser.setAwards(user.getAwards());
		persistedUser.setAchievements(user.getAchievements());
		persistedUser.setMemberShip(user.getMemberShip());
		persistedUser.setSpecialityid(user.getSpecialityid());
		persistedUser.setMedregno(user.getMedregno());
		persistedUser.setGender(user.getGender());
		persistedUser.setSessionFee(user.getSessionFee());
		persistedUser.setQueryFee(user.getQueryFee());
		return persistedUser;
	}

	public boolean saveUser(Session session, SpecialistDetails user) {
		boolean updated = false;
		session.merge(user);
		session.flush();
		updated = true;
		return updated;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

}

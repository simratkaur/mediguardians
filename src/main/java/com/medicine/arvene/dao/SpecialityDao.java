package com.medicine.arvene.dao;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.medicine.arvene.model.Speciality;

@Component
public class SpecialityDao {

	@Autowired
	SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	public Map<String, String> fetchActiveSpecialities(Session session) {
		@SuppressWarnings("rawtypes")
		List objects = selectActive(Speciality.class, session);
		List<Speciality> specialities = (List<Speciality>) (List<?>) objects;
		Map<String, String> specialityMap = new LinkedHashMap<String, String>();
		for (Speciality speciality : specialities) {
			specialityMap.put(speciality.getId() + "", speciality.getName());
		}
		return specialityMap;
	}

	@SuppressWarnings("unchecked")
	public List<String> fetchColleges(Session session) {
		Query query = session
				.createQuery("select college from SpecialistMaster where college is not null order by college");
		List<String> colleges = query.list();
		colleges.add("Others(Please specify)");
		return colleges;
	}

	@SuppressWarnings("unchecked")
	public List<String> fetchDegrees(Session session) {
		Query query = session
				.createQuery("select degree from SpecialistMaster where degree is not null order by degree");
		List<String> degrees = query.list();
		return degrees;
	}

	@SuppressWarnings("unchecked")
	public Map<String, String> fetchSpecialities(Session session) {
		@SuppressWarnings("rawtypes")
		List objects = selectAll(Speciality.class, session,"name");
		List<Speciality> specialities = (List<Speciality>) (List<?>) objects;
		Map<String, String> specialityMap = new LinkedHashMap<String, String>();
		for (Speciality speciality : specialities) {
			specialityMap.put(speciality.getId() + "", speciality.getName());
		}
		return specialityMap;
	}
	
	
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	@SuppressWarnings("rawtypes")
	List selectActive(Class clazz, Session session) {
		return session.createCriteria(clazz)
				.add(Restrictions.eq("isActive", true)).list();
	}

	@SuppressWarnings("rawtypes")
	List selectAll(Class clazz, Session session,String orderProp) {
		return session.createCriteria(clazz).addOrder(Order.asc(orderProp)).list();
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

}

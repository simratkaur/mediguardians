package com.medicine.arvene.dao;

import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import com.medicine.arvene.model.User;
import com.medicine.arvene.util.CommonUtil;

public class UserDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	public Map<String, String> getAllActiveUsers(Session session, String userType, String active)
			throws ParseException {

		List<User> users = new ArrayList<User>();

		// code for filtering
	
		if (!userType.equalsIgnoreCase("A")) {
			session.enableFilter("USER_TYPE_FILTER").setParameter("USER_TYPE",
					userType);

		}

		if (!active.equalsIgnoreCase("-1")) {

			if (active.equalsIgnoreCase("1")) {
				session.enableFilter("USER_ACTIVE_FILTER").setParameter(
						"IS_ACTIVE", true);

			} else {
				session.enableFilter("USER_ACTIVE_FILTER").setParameter(
						"IS_ACTIVE", false);

			}

		}

		Query sql = session.createQuery("from User");
		users = sql.list();
		
		
		Map<String, String> userMap = new LinkedHashMap<String, String>();
		for (User user : users) {
			userMap.put(user.getId() + "", user.getUsername());
		}
		return userMap;


	}
	
	

	public Integer getFilteredUsersCount(Session session, String startDate,
			String endDate, String userType, String active)
			throws ParseException {

		Integer count = 0;

		// code for filtering
		if (startDate != null && startDate != "") {
			session.enableFilter("START_DATE_FILTER").setParameter(
					"START_DATE", CommonUtil.pattern.parse(startDate));
		}

		if (endDate != null && endDate != "") {
			session.enableFilter("END_DATE_FILTER").setParameter("END_DATE",
					CommonUtil.pattern.parse(endDate));

		}

		if (!userType.equalsIgnoreCase("A")) {
			session.enableFilter("USER_TYPE_FILTER").setParameter("USER_TYPE",
					userType);

		}

		if (!active.equalsIgnoreCase("-1")) {

			if (active.equalsIgnoreCase("1")) {
				session.enableFilter("USER_ACTIVE_FILTER").setParameter(
						"IS_ACTIVE", true);

			} else {
				session.enableFilter("USER_ACTIVE_FILTER").setParameter(
						"IS_ACTIVE", false);

			}

		}

		count = ((Long) session.createQuery("select count(*) from User")
				.uniqueResult()).intValue();

		return count;

	}

	public List<User> getFilteredUsers(Session session, String startDate,
			String endDate, String userType, String active, int start, int range)
			throws ParseException {

		List<User> users = new ArrayList<User>();

		// code for filtering
		if (startDate != null && startDate != "") {
			session.enableFilter("START_DATE_FILTER").setParameter(
					"START_DATE", CommonUtil.pattern.parse(startDate));
		}

		if (endDate != null && endDate != "") {
			session.enableFilter("END_DATE_FILTER").setParameter("END_DATE",
					CommonUtil.pattern.parse(endDate));

		}

		if (!userType.equalsIgnoreCase("A")) {
			session.enableFilter("USER_TYPE_FILTER").setParameter("USER_TYPE",
					userType);

		}

		if (!active.equalsIgnoreCase("-1")) {

			if (active.equalsIgnoreCase("1")) {
				session.enableFilter("USER_ACTIVE_FILTER").setParameter(
						"IS_ACTIVE", true);

			} else {
				session.enableFilter("USER_ACTIVE_FILTER").setParameter(
						"IS_ACTIVE", false);

			}

		}

		Query sql = session.createQuery("from User");
		sql.setFirstResult(start);
		sql.setMaxResults(range);
		users = sql.list();

		return users;

	}

	public boolean addUser(User user, Session session) throws Exception {
		boolean userAdded = false;
		Serializable check = null;
//		if (user.getPassword() != null)
//			user.setPassword(new BCryptPasswordEncoder().encode(user
//					.getPassword()));
//		user.setVerificationCode(new BCryptPasswordEncoder().encode(String
//				.valueOf(Math.random())));

		if (session == null)
			session = CommonUtil.getSession(sessionFactory);
		user.setCreated(CommonUtil.getIstDateTime());
		check = session.save(user);
		session.flush();
		if (Integer.valueOf(check.toString()) > 0)
			userAdded = true;
		return userAdded;
	}

	public void deleteRecord(int id, Session session) {
		session.createQuery("delete from User where id=?").setParameter(0, id)
				.executeUpdate();
		;
	}

	public int fetchActiveUserCount(Session session) throws Exception {
		if (session == null)
			session = CommonUtil.getSession(sessionFactory);

		int count = ((Long) session.createQuery(
				"select count(*) from User where isActive='1'").uniqueResult())
				.intValue();
		return count;

	}

	@SuppressWarnings("unchecked")
	public User findByUserName(String username, Session session)
			throws Exception {
		if (session == null)
			session = CommonUtil.getSession(sessionFactory);

		List<User> users = new ArrayList<User>();
		users = session.createQuery("from User where username=?")
				.setParameter(0, username).list();
		if (users.size() > 0) {
			return users.get(0);
		} else {
			return null;
		}

	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public int makeActive(int id, Session session) {
		return session.createQuery("update User set isActive=true where id=?")
				.setParameter(0, id).executeUpdate();
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public void updateUser(User user, Session session) throws Exception {
		if (session == null)
			session = CommonUtil.getSession(sessionFactory);
		session.merge(user);
		session.flush();

	}
	
	
	
	public int updateUserStatus(int id,boolean isActive, Session session) {
		return session.createQuery("update User set isActive=? where id=?")
				.setParameter(0, isActive)
				.setParameter(1, id)
				.executeUpdate();
	}
	
	

}
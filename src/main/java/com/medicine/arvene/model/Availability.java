package com.medicine.arvene.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Entity
@Table(name = "availability")
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class Availability implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "userid")
	private Integer userid;

	@Id
	@Column(name = "dayid")
	private Integer dayid;

	@Id
	@Column(name = "starttime")
	private String starttime;

	@Id
	@Column(name = "created")
	@Temporal(TemporalType.TIMESTAMP)
	private Date created;

	public Date getCreated() {
		return created;
	}

	public Integer getDayid() {
		return dayid;
	}

	public String getStarttime() {
		return starttime;
	}

	public Integer getUserid() {
		return userid;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public void setDayid(Integer dayid) {
		this.dayid = dayid;
	}

	public void setStarttime(String starttime) {
		this.starttime = starttime;
	}

	public void setUserid(Integer userid) {
		this.userid = userid;
	}

}

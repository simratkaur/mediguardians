package com.medicine.arvene.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Entity
@Table(name = "booking")
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class Booking implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;

	@Column(name = "title")
	private String title;

	@Column(name = "descr")
	private String desc;
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "slotid", insertable = false, updatable = false)
	private Slot slot;

	private Integer slotid;

	private Integer subscriberid;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "subscriberid", insertable = false, updatable = false)
	private User subscriber;

	@Column(name = "toksession")
	private String toksession;

	@Column(name = "pubtkn")
	private String pubtkn;

	@Column(name = "subtkn")
	private String subtkn;

	@Column(name = "expiry")
	private Long expiry;

	@Column(name = "created")
	@Temporal(TemporalType.TIMESTAMP)
	private Date created;

	private String patientName;
	
	private String phone;

	@OneToOne
	@JoinColumn(name = "specialistId")
	private SpecialistDetails specialistDetails;

	@OneToOne
	@JoinColumn(name = "paymentId")
	@JsonIgnore
	private PaymentDetails paymentDetails;

	private boolean isActive;

	@Temporal(TemporalType.TIMESTAMP)
	private Date slotBookingTime;
	
	@OneToMany(mappedBy = "booking", cascade = CascadeType.ALL)
	private List<BookingReport> reports ;
	
	@Transient
	private List<MultipartFile> files;

	public Booking() {

	}

	public Date getCreated() {
		return created;
	}

	public String getDesc() {
		return desc;
	}

	public Long getExpiry() {
		return expiry;
	}

	public Integer getId() {
		return id;
	}

	public String getPatientName() {
		return patientName;
	}

	public PaymentDetails getPaymentDetails() {
		return paymentDetails;
	}

	public String getPubtkn() {
		return pubtkn;
	}

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "slotid", referencedColumnName = "slotid")
	public Slot getSlot() {
		return slot;
	}

	/**
	 * @return the slotBookingTime
	 */
	public Date getSlotBookingTime() {
		return slotBookingTime;
	}

	public Integer getSlotid() {
		return slotid;
	}

	public SpecialistDetails getSpecialistDetails() {
		return specialistDetails;
	}

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "subscriberid", referencedColumnName = "userid")
	public User getSubscriber() {
		return subscriber;
	}

	public Integer getSubscriberid() {
		return subscriberid;
	}

	public String getSubtkn() {
		return subtkn;
	}

	public String getTitle() {
		return title;
	}

	public String getToksession() {
		return toksession;
	}

	public boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(boolean isActive) {
		this.isActive = isActive;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public void setExpiry(Long expiry) {
		this.expiry = expiry;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	public void setPaymentDetails(PaymentDetails paymentDetails) {
		this.paymentDetails = paymentDetails;
	}

	public void setPubtkn(String pubtkn) {
		this.pubtkn = pubtkn;
	}

	public void setSlot(Slot slot) {
		this.slot = slot;
	}

	/**
	 * @param slotBookingTime
	 *            the slotBookingTime to set
	 */
	public void setSlotBookingTime(Date slotBookingTime) {
		this.slotBookingTime = slotBookingTime;
	}

	public void setSlotid(Integer slotid) {
		this.slotid = slotid;
	}

	public void setSpecialistDetails(SpecialistDetails specialistDetails) {
		this.specialistDetails = specialistDetails;
	}

	public void setSubscriber(User subscriber) {
		this.subscriber = subscriber;
	}

	public void setSubscriberid(Integer subscriberid) {
		this.subscriberid = subscriberid;
	}

	public void setSubtkn(String subtkn) {
		this.subtkn = subtkn;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setToksession(String toksession) {
		this.toksession = toksession;
	}

	/**
	 * @return the reports
	 */
	public List<BookingReport> getReports() {
		if(this.reports == null || this.reports.size() == 0){
			this.reports = new ArrayList<BookingReport>();
		}
		
		return reports;
	}

	/**
	 * @param reports the reports to set
	 */
	public void setReports(List<BookingReport> reports) {
		this.reports = reports;
	}

	/**
	 * @param isActive the isActive to set
	 */
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @param phone the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * @return the files
	 */
	public List<MultipartFile> getFiles() {
		return files;
	}

	/**
	 * @param files the files to set
	 */
	public void setFiles(List<MultipartFile> files) {
		this.files = files;
	}

}

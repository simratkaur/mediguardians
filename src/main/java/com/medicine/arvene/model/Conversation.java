/**
 * 
 */
package com.medicine.arvene.model;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * 
 *
 */
@Entity
@Table(name = "conversation")
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class Conversation {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "id", unique = true, nullable = false)
	int id;

	@Temporal(TemporalType.TIMESTAMP)
	private Date createdAt;

	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedAt;

	@OneToOne
	@JoinColumn(name = "userId")
	@JsonIgnore
	private User user;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "queryId")
	private Query query;

	@OneToMany(mappedBy = "conversation", cascade = CascadeType.ALL)
	private Set<Report> reports;

	private String text;
	private String type;

	@Transient
	private List<MultipartFile> files;

	public Conversation() {
		super();
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public int getId() {
		return id;
	}

	@JsonIgnore
	public Query getQuery() {
		return query;
	}

	public String getText() {
		return text;
	}

	public String getType() {
		return type;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public User getUser() {
		return user;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public void setId(int id) {
		this.id = id;
	}

	@JsonProperty
	public void setQuery(Query query) {
		this.query = query;
	}

	public void setText(String text) {
		this.text = text;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Set<Report> getReports() {
		if (this.reports == null || this.reports.size() == 0) {
			this.reports = new HashSet<>();
		}

		return reports;

	}

	public void setReports(Set<Report> reports) {
		this.reports = reports;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Conversation)) {
			return false;
		}
		Conversation other = (Conversation) obj;
		if (id != other.id) {
			return false;
		}
		return true;
	}

	/**
	 * @return the files
	 */
	public List<MultipartFile> getFiles() {
		return files;
	}

	/**
	 * @param files
	 *            the files to set
	 */
	public void setFiles(List<MultipartFile> files) {
		this.files = files;
	}

}

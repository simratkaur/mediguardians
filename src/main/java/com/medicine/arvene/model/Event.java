package com.medicine.arvene.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonAutoDetect
@JsonIgnoreProperties(ignoreUnknown = true)
public class Event {

	private int id;

	private int bookingid;

	private String title;

	private String start;

	private String end;

	private String className;

	private String backgroundColor;

	private String textColor;

	private String owner;

	public String getBackgroundColor() {
		return backgroundColor;
	}

	public int getBookingid() {
		return bookingid;
	}

	public String getClassName() {
		return className;
	}

	public String getEnd() {
		return end;
	}

	public int getId() {
		return id;
	}

	public String getOwner() {
		return owner;
	}

	public String getStart() {
		return start;
	}

	public String getTextColor() {
		return textColor;
	}

	public String getTitle() {
		return title;
	}

	public void setBackgroundColor(String backgroundColor) {
		this.backgroundColor = backgroundColor;
	}

	public void setBookingid(int bookingid) {
		this.bookingid = bookingid;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public void setEnd(String end) {
		this.end = end;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public void setStart(String start) {
		this.start = start;
	}

	public void setTextColor(String textColor) {
		this.textColor = textColor;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}

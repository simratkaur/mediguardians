/**
 * 
 */
package com.medicine.arvene.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonAutoDetect;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.medicine.arvene.util.IPaymentConstants;

/**
 * @author maddie
 *
 */
@Entity
@Table(name = "payment_details")
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonAutoDetect
@JsonIgnoreProperties(ignoreUnknown = true)
public class PaymentDetails {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "id", unique = true, nullable = false)
	int id;

	// unique id for each transaction
	@Basic(optional = false)
	private String transactionId;

	// Unique id from PayU.in
	@Basic(optional = false)
	private String payuMihpayId;

	@Basic(optional = false)
	private String email;

	@Basic(optional = false)
	private String firstName;

	private String lastName;

	private String phone;

	@Basic(optional = false)
	private String paymentMode;

	@Basic(optional = false)
	private String status;

	@Basic(optional = false)
	private float amount;

	private float discount;
	
	private float tax;
	
	private float fee;

	private float netAmountDebited;

	@Basic(optional = false)
	private String payuStatus;

	@Basic(optional = false)
	private String PGType;

	private String bankRefNum;

	private String errorCode;

	private String errorMessage;

	@Basic(optional = false)
	private String txnDate;

	@Transient
	private String productInfo;

	@Transient
	private String surl;

	@Transient
	private String furl;

	@Transient
	private String key;

	@Transient
	private String checksum;

	@Transient
	private String reqHashSequence;

	@Transient
	private String respHashSequence;

	@Transient
	private String hashAlgo;

	@Transient
	private String salt;

	@Transient
	private String serviceProvider;

	@Transient
	private String serviceUrl;

	@Transient
	private String udf1;

	@Transient
	private String pg_type;

	public PaymentDetails() {
		super();
		this.productInfo = IPaymentConstants.PRODUCT;
		this.surl = IPaymentConstants.SUCCESS_URL;
		this.furl = IPaymentConstants.FAILURE_URL;
		this.key = IPaymentConstants.KEY;
		this.reqHashSequence = IPaymentConstants.REQ_HASH_SEQUENCE;
		this.serviceProvider = IPaymentConstants.SERVICE_PROVIDER;
		this.hashAlgo = IPaymentConstants.HASH_ALGO;
		this.salt = IPaymentConstants.SALT;
		this.serviceUrl = IPaymentConstants.SERVICE_URL;

	}

	public PaymentDetails(String transactionId) {
		this();
		this.transactionId = transactionId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof PaymentDetails)) {
			return false;
		}
		PaymentDetails other = (PaymentDetails) obj;
		if (id != other.id) {
			return false;
		}
		return true;
	}

	public float getAmount() {
		return amount;
	}

	public String getBankRefNum() {
		return bankRefNum;
	}

	public String getChecksum() {
		return checksum;
	}

	public float getDiscount() {
		return discount;
	}

	public String getEmail() {
		return email;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getFurl() {
		return furl;
	}

	public String getHashAlgo() {
		return hashAlgo;
	}

	public int getId() {
		return id;
	}

	public String getKey() {
		return key;
	}

	public String getLastName() {
		return lastName;
	}

	public float getNetAmountDebited() {
		return netAmountDebited;
	}

	public String getPaymentMode() {
		return paymentMode;
	}

	public String getPayuMihpayId() {
		return payuMihpayId;
	}

	public String getPayuStatus() {
		return payuStatus;
	}

	public String getPg_type() {
		return pg_type;
	}

	public String getPGType() {
		return PGType;
	}

	public String getPhone() {
		return phone;
	}

	public String getProductInfo() {
		return productInfo;
	}

	public String getReqHashSequence() {
		return reqHashSequence;
	}

	public String getRespHashSequence() {
		return respHashSequence;
	}

	public String getSalt() {
		return salt;
	}

	public String getServiceProvider() {
		return serviceProvider;
	}

	public String getServiceUrl() {
		return serviceUrl;
	}

	public String getStatus() {
		return status;
	}

	public String getSurl() {
		return surl;
	}

	public String getTransactionId() {
		return transactionId;
	}

	/**
	 * @return the txnDate
	 */
	public String getTxnDate() {
		return txnDate;
	}

	/**
	 * @return the udf1
	 */
	public String getUdf1() {
		return udf1;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	public void setAmount(float amount) {
		this.amount = amount;
	}

	public void setBankRefNum(String bankRefNum) {
		this.bankRefNum = bankRefNum;
	}

	public void setChecksum(String checksum) {
		this.checksum = checksum;
	}

	public void setDiscount(float discount) {
		this.discount = discount;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setFurl(String furl) {
		this.furl = furl;
	}

	public void setHashAlgo(String hashAlgo) {
		this.hashAlgo = hashAlgo;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setNetAmountDebited(float netAmountDebited) {
		this.netAmountDebited = netAmountDebited;
	}

	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}

	public void setPayuMihpayId(String payuMihpayId) {
		this.payuMihpayId = payuMihpayId;
	}

	public void setPayuStatus(String payuStatus) {
		this.payuStatus = payuStatus;
	}

	public void setPg_type(String pg_type) {
		this.pg_type = pg_type;
	}

	public void setPGType(String pGType) {
		PGType = pGType;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public void setProductInfo(String productInfo) {
		this.productInfo = productInfo;
	}

	public void setReqHashSequence(String reqHashSequence) {
		this.reqHashSequence = reqHashSequence;
	}

	public void setRespHashSequence(String respHashSequence) {
		this.respHashSequence = respHashSequence;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public void setServiceProvider(String serviceProvider) {
		this.serviceProvider = serviceProvider;
	}

	public void setServiceUrl(String serviceUrl) {
		this.serviceUrl = serviceUrl;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setSurl(String surl) {
		this.surl = surl;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	/**
	 * @param txnDate
	 *            the txnDate to set
	 */
	public void setTxnDate(String txnDate) {
		this.txnDate = txnDate;
	}

	/**
	 * @param udf1
	 *            the udf1 to set
	 */
	public void setUdf1(String udf1) {
		this.udf1 = udf1;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "transactionId=" + transactionId
				+ "&payuMihpayId=" + payuMihpayId + "&email="
				+ email + "&firstName=" + firstName
				+ "&lastName=" + lastName + "&phone=" + phone
				+ "&paymentMode=" + paymentMode + "&status="
				+ status + "&amount=" + amount + "&discount="
				+ discount + "&netAmountDebited=" + netAmountDebited
				+ "&payuStatus=" + payuStatus + "&pg_type="
				+ PGType + "&bankRefNum=" + bankRefNum
				+ "&errorCode=" + errorCode
				+ "&errorMessage=" + errorMessage + "&udf1="
				+ udf1 + "&txnDate=" + txnDate;
	}

	/**
	 * @return the tax
	 */
	public float getTax() {
		return tax;
	}

	/**
	 * @param tax the tax to set
	 */
	public void setTax(float tax) {
		this.tax = tax;
	}

	/**
	 * @return the fee
	 */
	public float getFee() {
		return fee;
	}

	/**
	 * @param fee the fee to set
	 */
	public void setFee(float fee) {
		this.fee = fee;
	}

}

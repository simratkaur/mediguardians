/**
 * 
 */
package com.medicine.arvene.model;

import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.FilterDefs;
import org.hibernate.annotations.Filters;
import org.hibernate.annotations.ParamDef;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Entity
@Table(name = "query")
@FilterDefs({
		@FilterDef(name = "QUERY_RESP_FILTER", defaultCondition = "responded = :QUERY_RESP", parameters = { @ParamDef(name = "QUERY_RESP", type = "boolean") }),
		@FilterDef(name = "QUERY_ACTIVE_FILTER", defaultCondition = "isActive = :IS_ACTIVE", parameters = { @ParamDef(name = "IS_ACTIVE", type = "boolean") }) })
@Filters({
		@Filter(name = "QUERY_RESP_FILTER", condition = "responded = :QUERY_RESP"),
		@Filter(name = "QUERY_ACTIVE_FILTER", condition = "isActive = :IS_ACTIVE") })
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class Query {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "id", unique = true, nullable = false)
	int id;

	@Transient
	@JsonIgnore
	private int specialist;

	@OneToOne
	@JoinColumn(name = "specialistId")
	private SpecialistDetails specialistDetails;

	@OneToOne
	@JoinColumn(name = "userId")
	@JsonProperty(value = "user")
	private User user;

	@OneToOne
	@JoinColumn(name = "paymentId")
	@JsonIgnore
	private PaymentDetails paymentDetails;

	private boolean isActive;

	@Temporal(TemporalType.TIMESTAMP)
	private Date createdAt;

	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedAt;
	
	@JsonIgnore
	private boolean responded;

	@OneToMany(mappedBy = "query", cascade = CascadeType.ALL)
	private List<Conversation> conversations;

//	@OneToMany(mappedBy = "query", cascade = CascadeType.ALL)
//	private Set<Report> reports;

	@Transient
	private List<MultipartFile> files;
	
	@Transient
	private String newConversation;
	
	private boolean addQueryFlag; 
	
	private String patientName;
	
	private int patientAge;
	
	private String patientGender;
	
	private String patientPhone;
	
	private int cost;


	public List<MultipartFile> getFiles() {
		return files;
	}

	public void setFiles(List<MultipartFile> files) {
		this.files = files;
	}

	public List<Conversation> getConversations() {
		return conversations;
	}

	public void setConversations(List<Conversation> conversations) {
		this.conversations = conversations;
	}

//	public Set<Report> getReports() {
//		return reports;
//	}
//
//	public void setReports(Set<Report> reports) {
//		this.reports = reports;
//	}

	public boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(boolean isActive) {
		this.isActive = isActive;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public boolean getIsResponded() {
		return responded;
	}

	public void setIsResponded(boolean responded) {
		this.responded = responded;
	}

	public Query() {
		super();
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @return the paymentDetails
	 */
	public PaymentDetails getPaymentDetails() {
		return paymentDetails;
	}

	/**
	 * @return the specialist
	 */
	public int getSpecialist() {
		return specialist;
	}

	// /**
	// * @return the paymentId
	// */
	// public int getPaymentId() {
	// return paymentId;
	// }
	//
	// /**
	// * @param paymentId the paymentId to set
	// */
	// public void setPaymentId(int paymentId) {
	// this.paymentId = paymentId;
	// }

	/**
	 * @return the specialistDetails
	 */
	public SpecialistDetails getSpecialistDetails() {
		return specialistDetails;
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @param paymentDetails
	 *            the paymentDetails to set
	 */
	public void setPaymentDetails(PaymentDetails paymentDetails) {
		this.paymentDetails = paymentDetails;
	}

	/**
	 * @param specialist
	 *            the specialist to set
	 */
	public void setSpecialist(int specialist) {
		this.specialist = specialist;
	}

	/**
	 * @param specialistDetails
	 *            the specialistDetails to set
	 */
	public void setSpecialistDetails(SpecialistDetails specialistDetails) {
		this.specialistDetails = specialistDetails;
	}

	/**
	 * @param user
	 *            the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * @return the addQueryFlag
	 */
	public boolean isAddQueryFlag() {
		return addQueryFlag;
	}

	/**
	 * @param addQueryFlag the addQueryFlag to set
	 */
	public void setAddQueryFlag(boolean addQueryFlag) {
		this.addQueryFlag = addQueryFlag;
	}

	/**
	 * @return the responded
	 */
	public boolean isResponded() {
		return responded;
	}

	/**
	 * @param responded the responded to set
	 */
	public void setResponded(boolean responded) {
		this.responded = responded;
	}

	/**
	 * @return the newConversation
	 */
	public String getNewConversation() {
		return newConversation;
	}

	/**
	 * @param newConversation the newConversation to set
	 */
	public void setNewConversation(String newConversation) {
		this.newConversation = newConversation;
	}

	/**
	 * @return the patientName
	 */
	public String getPatientName() {
		return patientName;
	}

	/**
	 * @param patientName the patientName to set
	 */
	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	/**
	 * @return the patientAge
	 */
	public int getPatientAge() {
		return patientAge;
	}

	/**
	 * @param patientAge the patientAge to set
	 */
	public void setPatientAge(int patientAge) {
		this.patientAge = patientAge;
	}

	/**
	 * @return the patientGender
	 */
	public String getPatientGender() {
		return patientGender;
	}

	/**
	 * @param patientGender the patientGender to set
	 */
	public void setPatientGender(String patientGender) {
		this.patientGender = patientGender;
	}

	/**
	 * @return the patientPhone
	 */
	public String getPatientPhone() {
		return patientPhone;
	}

	/**
	 * @param patientPhone the patientPhone to set
	 */
	public void setPatientPhone(String patientPhone) {
		this.patientPhone = patientPhone;
	}

	public int getCost() {
		return cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}

}

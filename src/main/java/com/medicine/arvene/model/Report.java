package com.medicine.arvene.model;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Entity
@Table(name = "report")
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class Report {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "id", unique = true, nullable = false)
	int id;

	@Temporal(TemporalType.TIMESTAMP)
	private Date createdAt;

	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedAt;

	@OneToOne
	@JoinColumn(name = "userId")
	@JsonIgnore
	private User user;

//	@ManyToOne
//	@JoinColumn(name = "queryId")
//	private Query query;
	
	@ManyToOne
	@JoinColumn(name = "conversationId")
	private Conversation conversation;

	private String url;
	
	private String fileName;

	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

//	public Query getQuery() {
//		return query;
//	}
//
//	public void setQuery(Query query) {
//		this.query = query;
//	}
	
//	@Transient
//	private byte[] file;
//
//	public void setUrl(String url) {
//		this.url = url;
//	}
//
//	public byte[] getFile() {
//		return file;
//	}
//
//	public void setFile(byte[] file) {
//		this.file = file;
//	}
//
//	public byte[] getAttachment() {
//		return attachment;
//	}
//
//	public void setAttachment(byte[] attachment) {
//		this.attachment = attachment;
//	}
//
//	public String getAttachmentUrl() {
//		return attachmentUrl;
//	}
//
//	public void setAttachmentUrl(String attachmentUrl) {
//		this.attachmentUrl = attachmentUrl;
//	}
//
//	@Transient
//	private byte[] attachment;
//
//	@Column(name = "attachment")
//	private String attachmentUrl;

	/**
	 * @return the conversation
	 */
	public Conversation getConversation() {
		return conversation;
	}

	/**
	 * @param conversation the conversation to set
	 */
	public void setConversation(Conversation conversation) {
		this.conversation = conversation;
	}





	/**
	 * @return the fileName
	 */
	public String getFileName() {
		return fileName;
	}





	/**
	 * @param fileName the fileName to set
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}





	public String getUrl() {
		return url;
	}
	

	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}
}

package com.medicine.arvene.model;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.Basic;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonAutoDetect
@JsonIgnoreProperties(ignoreUnknown = true)
public class Result {
	@Basic(optional = false)
	private Date dateOfSync;
	private boolean status;
	private String msg;
	private Object data;
	private Map<Integer, Integer> idMap = new HashMap<Integer, Integer>();

	public Result() {
		super();
	}

	public Result(Date dateOfSync, boolean status, String msg, Object data,
			Map<Integer, Integer> idMap) {
		super();
		this.dateOfSync = dateOfSync;
		this.status = status;
		this.msg = msg;
		this.data = data;
		this.idMap = idMap;
	}

	public Object getData() {
		return data;
	}

	public Date getDateOfSync() {
		return dateOfSync;
	}

	public Map<Integer, Integer> getIdMap() {
		return idMap;
	}

	public String getMsg() {
		return msg;
	}

	public boolean getStatus() {
		return status;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public void setDateOfSync(Date dateOfSync) {
		this.dateOfSync = dateOfSync;
	}

	public void setIdMap(Map<Integer, Integer> idMap) {
		this.idMap = idMap;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

}

package com.medicine.arvene.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

@Entity
@Table(name = "slot")
public class Slot {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ownerid", insertable = false, updatable = false)
	private User user;

	@Column(name = "startdate")
	private Date startdate;

	@Column(name = "enddate")
	private Date enddate;

	@Column(name = "status")
	private String status;

	private Integer ownerid;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created", nullable = false, updatable = false)
	@Version
	private Date created;

	public Slot() {

	}

	public Date getCreated() {
		return created;
	}

	public Date getEnddate() {
		return enddate;
	}

	public Integer getId() {
		return id;
	}

	public Integer getOwnerid() {
		return ownerid;
	}

	public Date getStartdate() {
		return startdate;
	}

	public String getStatus() {
		return status;
	}

	public User getUser() {
		return this.user;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public void setEnddate(Date enddate) {
		this.enddate = enddate;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setOwnerid(Integer ownerid) {
		this.ownerid = ownerid;
	}

	public void setStartdate(Date startdate) {
		this.startdate = startdate;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setUser(User owner) {
		this.user = owner;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((enddate == null) ? 0 : enddate.hashCode());
		result = prime * result + ((ownerid == null) ? 0 : ownerid.hashCode());
		result = prime * result + ((startdate == null) ? 0 : startdate.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Slot)) {
			return false;
		}
		Slot other = (Slot) obj;
		if (enddate == null) {
			if (other.enddate != null) {
				return false;
			}
		} else if (!enddate.equals(other.enddate)) {
			return false;
		}
		if (ownerid == null) {
			if (other.ownerid != null) {
				return false;
			}
		} else if (!ownerid.equals(other.ownerid)) {
			return false;
		}
		if (startdate == null) {
			if (other.startdate != null) {
				return false;
			}
		} else if (!startdate.equals(other.startdate)) {
			return false;
		}
		return true;
	}

}

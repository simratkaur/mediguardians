package com.medicine.arvene.model;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "specialistdetails")
public class SpecialistDetails {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	private int id;

	@OneToOne
	@JoinColumn(name = "userId")
	private User user;

	private String title;
	private String firstName;
	private String lastName;
	private String middleName;
	private String degrees;
	private String other;

	private String subSpeciality;
	private String areaOfInterest;
	private String hospital;
	private String hospitalCity;
	private String mobile;
	private String biography;
	private String awards;
	private String achievements;
	private String memberShip;
	private int sessionFee;
	private int queryFee;

	private String gender;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "specialityid", insertable = false, updatable = false)
	private Speciality speciality;

	private Integer specialityid;

	private String medregno;

	private Date availstart;

	private Date availend;

	private Date created;

	@Transient
	private byte[] attachment;

	private String profileImage;

	public SpecialistDetails() {

	}

	public String getOther() {
		return other;
	}

	public void setOther(String other) {
		this.other = other;
	}

	public String getAchievements() {
		return achievements;
	}

	public String getAreaOfInterest() {
		return areaOfInterest;
	}

	public byte[] getAttachment() {
		return attachment;
	}

	/**
	 * @return the availend
	 */
	public Date getAvailend() {
		return availend;
	}

	/**
	 * @return the availstart
	 */
	public Date getAvailstart() {
		return availstart;
	}

	public String getAwards() {
		return awards;
	}

	public String getBiography() {
		return biography;
	}

	/**
	 * @return the created
	 */
	public Date getCreated() {
		return created;
	}

	public String getFirstName() {
		return firstName;
	}

	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}

	public String getHospitalCity() {
		return hospitalCity;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	public String getLastName() {
		return lastName;
	}

	/**
	 * @return the medregno
	 */
	public String getMedregno() {
		return medregno;
	}

	public String getMemberShip() {
		return memberShip;
	}

	public String getMiddleName() {
		return middleName;
	}

	public String getProfileImage() {
		return profileImage;
	}

	public int getSessionFee() {
		return sessionFee;
	}

	/**
	 * @return the speciality
	 */
	public Speciality getSpeciality() {
		return speciality;
	}

	/**
	 * @return the specialityid
	 */
	public Integer getSpecialityid() {
		return specialityid;
	}

	public String getSubSpeciality() {
		return subSpeciality;
	}

	public String getTitle() {
		return title;
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	public void setAchievements(String achievements) {
		this.achievements = achievements;
	}

	public void setAreaOfInterest(String areaOfInterest) {
		this.areaOfInterest = areaOfInterest;
	}

	public void setAttachment(byte[] attachment) {
		this.attachment = attachment;
	}

	/**
	 * @param availend
	 *            the availend to set
	 */
	public void setAvailend(Date availend) {
		this.availend = availend;
	}

	/**
	 * @param availstart
	 *            the availstart to set
	 */
	public void setAvailstart(Date availstart) {
		this.availstart = availstart;
	}

	public void setAwards(String awards) {
		this.awards = awards;
	}

	public void setBiography(String biography) {
		this.biography = biography;
	}

	/**
	 * @param created
	 *            the created to set
	 */
	public void setCreated(Date created) {
		this.created = created;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @param gender
	 *            the gender to set
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}

	public void setHospitalCity(String hospitalCity) {
		this.hospitalCity = hospitalCity;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @param medregno
	 *            the medregno to set
	 */
	public void setMedregno(String medregno) {
		this.medregno = medregno;
	}

	public void setMemberShip(String memberShip) {
		this.memberShip = memberShip;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public void setProfileImage(String profileImage) {
		this.profileImage = profileImage;
	}

	public void setSessionFee(int sessionFee) {
		this.sessionFee = sessionFee;
	}

	/**
	 * @param speciality
	 *            the speciality to set
	 */
	public void setSpeciality(Speciality speciality) {
		this.speciality = speciality;
	}

	/**
	 * @param specialityid
	 *            the specialityid to set
	 */
	public void setSpecialityid(Integer specialityid) {
		this.specialityid = specialityid;
	}

	public void setSubSpeciality(String subSpeciality) {
		this.subSpeciality = subSpeciality;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @param user
	 *            the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}

	public String getDegrees() {
		return degrees;
	}

	public void setDegrees(String degrees) {
		this.degrees = degrees;
	}

	public String getHospital() {
		return hospital;
	}

	public void setHospital(String hospital) {
		this.hospital = hospital;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public int getQueryFee() {
		return queryFee;
	}

	public void setQueryFee(int queryFee) {
		this.queryFee = queryFee;
	}

}
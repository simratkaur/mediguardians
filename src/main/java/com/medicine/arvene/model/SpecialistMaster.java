package com.medicine.arvene.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonAutoDetect;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Entity
@Table(name = "specialistmaster")
@JsonAutoDetect
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class SpecialistMaster {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "id", unique = true, nullable = false)
	private int id;
	private String degree;
	private String college;

	public SpecialistMaster() {
		super();
	}

	public String getCollege() {
		return college;
	}

	public String getDegree() {
		return degree;
	}

	public int getId() {
		return id;
	}

	public void setCollege(String college) {
		this.college = college;
	}

	public void setDegree(String degree) {
		this.degree = degree;
	}

	public void setId(int id) {
		this.id = id;
	}

}

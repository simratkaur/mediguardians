package com.medicine.arvene.model;

import java.util.Date;

public class SpecialistRegisterForm {

	private int id;
	private String username;
	private String password;

	private boolean isActive;

	private String phone;

	private String verificationCode;

	private String title;
	private String firstName;
	private String lastName;
	private String middleName;
	private String degrees;
	private String subSpeciality;
	private String areaOfInterest;
	private String designation;
	private String hospital;
	private String hospitalCity;
	private String mobile;
	private String biography;
	private String awards;
	private String achievements;
	private String memberShip;
	private Integer specialityid;
	private int sessionFee;
	private int queryFee;
	private String other;
	
	private String regno;

	private Date availstart;

	private Date availend;

	private Date created;

	private String userType;

	private String gender;
	
	public String getOther() {
		return other;
	}

	public void setOther(String other) {
		this.other = other;
	}

	public String getAchievements() {
		return achievements;
	}

	public String getAreaOfInterest() {
		return areaOfInterest;
	}

	/**
	 * @return the availend
	 */
	public Date getAvailend() {
		return availend;
	}

	/**
	 * @return the availstart
	 */
	public Date getAvailstart() {
		return availstart;
	}

	public String getAwards() {
		return awards;
	}

	public String getBiography() {
		return biography;
	}

	/**
	 * @return the created
	 */
	public Date getCreated() {
		return created;
	}

	public String getDegrees() {
		return degrees;
	}

	public String getDesignation() {
		return designation;
	}

	public String getFirstName() {
		return firstName;
	}

	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}

	public String getHospitalCity() {
		return hospitalCity;
	}

	public String getHospital() {
		return hospital;
	}

	public String getMobile() {
		return mobile;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	public String getLastName() {
		return lastName;
	}

	public String getMemberShip() {
		return memberShip;
	}

	public String getMiddleName() {
		return middleName;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @return the regno
	 */
	public String getRegno() {
		return regno;
	}

	public int getSessionFee() {
		return sessionFee;
	}

	/**
	 * @return the specialityid
	 */
	public Integer getSpecialityid() {
		return specialityid;
	}

	public String getSubSpeciality() {
		return subSpeciality;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @return the userType
	 */
	public String getUserType() {
		return userType;
	}

	/**
	 * @return the verificationCode
	 */
	public String getVerificationCode() {
		return verificationCode;
	}

	/**
	 * @return the isActive
	 */
	public boolean getIsActive() {
		return isActive;
	}

	public void setAchievements(String achievements) {
		this.achievements = achievements;
	}

	/**
	 * @param isActive
	 *            the isActive to set
	 */
	public void setIsActive(boolean isActive) {
		this.isActive = isActive;
	}

	public void setAreaOfInterest(String areaOfInterest) {
		this.areaOfInterest = areaOfInterest;
	}

	/**
	 * @param availend
	 *            the availend to set
	 */
	public void setAvailend(Date availend) {
		this.availend = availend;
	}

	/**
	 * @param availstart
	 *            the availstart to set
	 */
	public void setAvailstart(Date availstart) {
		this.availstart = availstart;
	}

	public void setAwards(String awards) {
		this.awards = awards;
	}

	public void setBiography(String biography) {
		this.biography = biography;
	}

	/**
	 * @param created
	 *            the created to set
	 */
	public void setCreated(Date created) {
		this.created = created;
	}

	public void setDegrees(String degree) {
		this.degrees = degree;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @param gender
	 *            the gender to set
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}

	public void setHospitalCity(String hospitalCity) {
		this.hospitalCity = hospitalCity;
	}

	public void setHospital(String hospitalLoc) {
		this.hospital = hospitalLoc;
	}

	public void setMobile(String hospitalPhone) {
		this.mobile = hospitalPhone;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setMemberShip(String memberShip) {
		this.memberShip = memberShip;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @param phone
	 *            the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * @param regno
	 *            the regno to set
	 */
	public void setRegno(String regno) {
		this.regno = regno;
	}

	public void setSessionFee(int sessionFee) {
		this.sessionFee = sessionFee;
	}

	/**
	 * @param specialityid
	 *            the specialityid to set
	 */
	public void setSpecialityid(Integer specialityid) {
		this.specialityid = specialityid;
	}

	public void setSubSpeciality(String subSpeciality) {
		this.subSpeciality = subSpeciality;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @param username
	 *            the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @param userType
	 *            the userType to set
	 */
	public void setUserType(String userType) {
		this.userType = userType;
	}

	/**
	 * @param verificationCode
	 *            the verificationCode to set
	 */
	public void setVerificationCode(String verificationCode) {
		this.verificationCode = verificationCode;
	}

	public int getQueryFee() {
		return queryFee;
	}

	public void setQueryFee(int queryFee) {
		this.queryFee = queryFee;
	}

}

package com.medicine.arvene.model;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.FilterDefs;
import org.hibernate.annotations.Filters;
import org.hibernate.annotations.ParamDef;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "user")
@FilterDefs({
	@FilterDef(name = "START_DATE_FILTER", 
  		   defaultCondition = "created >= :START_DATE", 
  		   parameters = {@ParamDef(name = "START_DATE", type = "date")}),
    @FilterDef(name = "END_DATE_FILTER", 
    		   defaultCondition = "created <= :END_DATE", 
    		   parameters = {@ParamDef(name = "END_DATE", type = "date")}),
    @FilterDef(name = "USER_TYPE_FILTER", 
    		   defaultCondition = "userType = :USER_TYPE", 
    		   parameters = { @ParamDef(name = "USER_TYPE", type = "string") }),   
    @FilterDef(name = "USER_ACTIVE_FILTER", 
    		   defaultCondition = "isActive = :IS_ACTIVE", 
    		   parameters = { @ParamDef(name = "IS_ACTIVE", type = "boolean") })
})
@Filters( { 
	 @Filter(name="START_DATE_FILTER", condition="created >= :START_DATE"),
	 @Filter(name="END_DATE_FILTER", condition="created <= :END_DATE"),
	 @Filter(name="USER_TYPE_FILTER", condition="userType = :USER_TYPE"),
	 @Filter(name="USER_ACTIVE_FILTER", condition="isActive = :IS_ACTIVE")
		
})
@JsonAutoDetect
@JsonIgnoreProperties(ignoreUnknown = true)
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "id", unique = true, nullable = false)
	int id;
	@Basic(optional = false)
	private String username;
	@Basic(optional = false)
	@JsonIgnore
	private String password;
	@JsonIgnore
	private boolean isActive;
	private String phone;
	@JsonIgnore
	private int OTP;
	@JsonIgnore
	private String verificationCode;

	@Basic(optional = false)
	private String userType;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date created;


	public User() {
		super();
	}

	public User(int id, String username, String password, boolean isActive,
			String phone, int oTP, String verificationCode) {
		super();
		this.id = id;
		this.username = username;
		this.password = password;
		this.isActive = isActive;
		this.phone = phone;
		OTP = oTP;
		this.verificationCode = verificationCode;
	}

	public User(int id, String username, String password, boolean isActive,
			String phone, int oTP, String verificationCode, String type) {
		super();
		this.id = id;
		this.username = username;
		this.password = password;
		this.isActive = isActive;
		this.phone = phone;
		OTP = oTP;
		this.verificationCode = verificationCode;
		this.userType = type;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (id != other.id)
			return false;
		return true;
	}

	public int getId() {
		return id;
	}

	public int getOTP() {
		return OTP;
	}

	@JsonIgnore
	public String getPassword() {
		return password;
	}

	public String getPhone() {
		return phone;
	}

	public String getUsername() {
		return username;
	}

	/**
	 * @return the userType
	 */
	public String getUserType() {
		return userType;
	}

	public String getVerificationCode() {
		return verificationCode;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	public boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(boolean isActive) {
		this.isActive = isActive;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setOTP(int oTP) {
		OTP = oTP;
	}

	@JsonProperty
	public void setPassword(String password) {
		this.password = password;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @param userType
	 *            the userType to set
	 */
	public void setUserType(String userType) {
		this.userType = userType;
	}

	public void setVerificationCode(String verificationCode) {
		this.verificationCode = verificationCode;
	}

	/**
	 * @return the created
	 */
	public Date getCreated() {
		return created;
	}

	/**
	 * @param created the created to set
	 */
	public void setCreated(Date created) {
		this.created = created;
	}

}

package com.medicine.arvene.model;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.medicine.arvene.util.CommonUtil;

@JsonAutoDetect
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class WeekSchedule {

	private String suntimings;

	private String montimings;

	private String tuetimings;

	private String wedtimings;

	private String thutimings;

	private String fritimings;

	private String sattimings;

	private String startdateStr;

	private String enddateStr;

	private Date startDate;

	private Date endDate;

	private HashMap<Integer, List<String>> weeksessions = null;

	public Date getEndDate() throws ParseException {
		if (enddateStr != null) {
			return CommonUtil.pattern.parse(enddateStr);
		}
		return null;
	}

	public String getEnddateStr() {
		return enddateStr;
	}

	public String getFritimings() {
		return fritimings;
	}

	public String getMontimings() {
		return montimings;
	}

	public String getSattimings() {
		return sattimings;
	}

	public Date getStartDate() throws ParseException {
		if (startdateStr != null) {
			return CommonUtil.pattern.parse(startdateStr);
		}
		return null;
	}

	public String getStartdateStr() {
		return startdateStr;
	}

	public String getSuntimings() {
		return suntimings;
	}

	public String getThutimings() {
		return thutimings;
	}

	public String getTuetimings() {
		return tuetimings;
	}

	public String getWedtimings() {
		return wedtimings;
	}

	public HashMap<Integer, List<String>> getWeeksessions() {
		return weeksessions;
	}

	public void prepareWeekSessions() {
		weeksessions = new HashMap<Integer, List<String>>();
		if (suntimings != null && suntimings.length() > 0)
			setSession(1, suntimings.split(","));
		if (montimings != null && montimings.length() > 0)
			setSession(2, montimings.split(","));
		if (tuetimings != null && tuetimings.length() > 0)
			setSession(3, tuetimings.split(","));
		if (wedtimings != null && wedtimings.length() > 0)
			setSession(4, wedtimings.split(","));
		if (thutimings != null && thutimings.length() > 0)
			setSession(5, thutimings.split(","));
		if (fritimings != null && fritimings.length() > 0)
			setSession(6, fritimings.split(","));
		if (sattimings != null && sattimings.length() > 0)
			setSession(7, sattimings.split(","));
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
		if (this.endDate != null)
			this.enddateStr = CommonUtil.pattern.format(this.endDate);
	}

	public void setEnddateStr(String enddateStr) {
		this.enddateStr = enddateStr;
	}

	public void setFritimings(String fritimings) {
		this.fritimings = fritimings;
	}

	public void setMontimings(String montimings) {
		this.montimings = montimings;
	}

	public void setSattimings(String sattimings) {
		this.sattimings = sattimings;
	}

	private void setSession(int day, String[] sessions) {
		List<String> sessionList = Arrays.asList(sessions);
		weeksessions.put(day, sessionList);
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
		if (this.startDate != null)
			this.startdateStr = CommonUtil.pattern.format(this.startDate);
	}

	public void setStartdateStr(String startdateStr) {
		this.startdateStr = startdateStr;
	}

	public void setSuntimings(String suntimings) {
		this.suntimings = suntimings;
	}

	public void setThutimings(String thutimings) {
		this.thutimings = thutimings;
	}

	public void setTuetimings(String tuetimings) {
		this.tuetimings = tuetimings;
	}

	public void setWedtimings(String wedtimings) {
		this.wedtimings = wedtimings;
	}

	public void setWeeksessions(HashMap<Integer, List<String>> weeksessions) {
		this.weeksessions = weeksessions;
	}

}

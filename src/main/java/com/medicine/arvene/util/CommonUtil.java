package com.medicine.arvene.util;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Random;
import java.util.TimeZone;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.exception.JDBCConnectionException;
import org.springframework.stereotype.Controller;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mysql.jdbc.exceptions.jdbc4.CommunicationsException;

@Controller
public class CommonUtil {

	public static final int GENERIC_FEE = 25;

	// total conversation in a query including doctor's response
	public static final int MAX_CONVERSATIONS = 6;

	public static SimpleDateFormat pattern = new SimpleDateFormat("dd/MM/yyyy");

	public static File byteToFile(byte[] bytearr) throws IOException {
		File file = new File("medics");
		FileUtils.writeByteArrayToFile(file, bytearr);
		return file;
	}

	public static void createFolder(String bucketName, String folderName,
			AmazonS3 client) {
		// create meta-data for your folder and set content-length to 0
		ObjectMetadata metadata = new ObjectMetadata();
		metadata.setContentLength(0);
		// create empty content
		InputStream emptyContent = new ByteArrayInputStream(new byte[0]);
		// create a PutObjectRequest passing the folder name suffixed by /
		PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName,
				folderName + "/", emptyContent, metadata);
		// send request to S3 to create folder
		client.putObject(putObjectRequest);
	}

	/**
	 * This method first deletes all the files in given folder and than the
	 * folder itself
	 */

	public static void deleteFolder(String bucketName, String folderName,
			AmazonS3 client) {
		List<S3ObjectSummary> fileList = client.listObjects(bucketName,
				folderName).getObjectSummaries();
		for (S3ObjectSummary file : fileList) {
			client.deleteObject(bucketName, file.getKey());
		}
		client.deleteObject(bucketName, folderName);
	}

	public static String exceptionHandler(Exception e) {
		String error = null;
		if (e.getCause() != null)
			error = e.getCause().getMessage();
		else
			error = e.getMessage();
		if (e instanceof CommunicationsException
				|| e instanceof JDBCConnectionException
				|| e instanceof UnknownHostException)
			error = "Server is down, Please try again later !!";
		return error;

	}

	public static String formatEventTime(Date date) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String eventFormattedTime = format.format(date);
		eventFormattedTime = eventFormattedTime.replace(' ', 'T');
		return eventFormattedTime;
	}

	public static Calendar getCal(Date date) {
		Calendar sDateCalendar = new GregorianCalendar();
		sDateCalendar.setTime(date);
		return sDateCalendar;
	}

	public static String getDatein12hrFormat(Date date) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm a");
		return dateFormat.format(date);
	}

	public static String getCurrentDate(Date date) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(
				"yyyy-MM-dd hh:mm:ss");
		return dateFormat.format(date);
	}

	public static int getDayOfWeekFromDate(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return cal.get(Calendar.DAY_OF_WEEK);
	}

	public static Object getEntity(Session session, Class<?> className, int id) {
		return session.get(className, id);
	}

	public static List<String> getFreqType() {
		List<String> fList = new ArrayList<String>();
		fList.add("Schedule");
		return fList;
	}

	public static Date getIstDate(String date) throws ParseException {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		return format.parse(date);
	}

	public static Date getIstDateTime() throws ParseException {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		format.setTimeZone(TimeZone.getTimeZone("IST"));
		Date date = new Date();
		SimpleDateFormat dateParser = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm:ss");
		return dateParser.parse(format.format(date));
	}

	public static String getOTPMsgContent(String user, int otp) {
		if (user == null)
			user = "Customer";
		return "Dear "
				+ user
				+ ", Thank you for registering with Your Medics. Your OTP for confirming your account is "
				+ otp + ".";
	}

	public static int getRandom() {
		return 1000 + new Random().nextInt(9000);
	}

	public static Session getSession(SessionFactory sessionFactory) {
		Session session;
		try {
			session = sessionFactory.getCurrentSession();
		} catch (HibernateException e) {
			session = sessionFactory.openSession();
		}
		return session;
	}

	public static List<String> getTitle() {
		List<String> titleList = new ArrayList<String>();
		titleList.add("Dr.");
		titleList.add("Prof.");
		titleList.add("Mr.");
		titleList.add("Miss.");
		titleList.add("Mrs.");
		return titleList;
	}

	public static <T> T parseJsonToObject(Object jsonStr, Class<T> clazz)
			throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		return mapper.convertValue(jsonStr, clazz);
	}

	@SuppressWarnings("deprecation")
	public static String sendMsg(String msg, String phone) throws IOException {
		String url = "http://cloud.smsindiahub.in/vendorsms/pushsms.aspx?user=sumitwadhwa&password=ym@sms@123&msisdn=91"
				+ phone
				+ "&sid=MEDICS&gwid=2&fl=0&msg="
				+ URLEncoder.encode(msg);

		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		BufferedReader in = new BufferedReader(new InputStreamReader(
				con.getInputStream()));
		String inputLine;
		String response = "";

		while ((inputLine = in.readLine()) != null) {
			response += inputLine;
		}
		in.close();
		return response;
	}

	public static String uploadImage(byte[] image, String extension)
			throws IOException {
		// credentials object identifying user for authentication
		// user must have AWSConnector and AmazonS3FullAccess for
		// this example to work
		AWSCredentials credentials = new BasicAWSCredentials(
				"AKIAI5WOQ23T7LYJW3CQ",
				"e5oMFkeW3ETnaNsA1lqZ46ub3IYjSiXIaI6P4h6z");

		// create a client connection based on credentials
		AmazonS3 s3client = new AmazonS3Client(credentials);

		// create bucket - name must be unique for all S3 users
		String bucketName = "arvene";

		// create folder into bucket
		String folderName = "images";

		String SUFFIX = "/";

		File file = byteToFile(image);
		// upload file to folder and set it to public
		String fileName = String.format("%s.%s",
				RandomStringUtils.randomAlphanumeric(20), extension);

		s3client.putObject(new PutObjectRequest(bucketName, folderName + SUFFIX
				+ fileName, file)
				.withCannedAcl(CannedAccessControlList.PublicRead));

		/*
		 * deleteFolder(bucketName, folderName, s3client);
		 * 
		 * // deletes bucket s3client.deleteBucket(bucketName);
		 */
		String url = "https://s3.ap-south-1.amazonaws.com/" + bucketName + "/"
				+ folderName + "/" + fileName;
		return url;
	}

	public static byte[] urlToByte(String urlString) throws IOException {
		URL url = new URL(urlString);
		InputStream is = url.openStream();
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte[] byteChunk = new byte[1024];
		int n;

		while ((n = is.read(byteChunk)) > 0) {
			baos.write(byteChunk, 0, n);
		}
		return baos.toByteArray();
	}

	public static boolean withinRange(Date date, String type)
			throws ParseException {
		Calendar calendar = getCal(date);
		boolean withinRange = false;
		Calendar start = getCal(getIstDateTime());
		int delta = 0;
		switch (type) {
		case "Weekly":
			delta = 7;
			break;
		case "Monthly":
			delta = 30;
			break;
		case "Yearly":
			delta = 365;
			break;
		default:
			break;
		}
		start.add(Calendar.DAY_OF_MONTH, -delta);
		start.set(Calendar.HOUR_OF_DAY, 0);
		start.set(Calendar.MINUTE, 0);
		start.set(Calendar.SECOND, 1);
		if (calendar.after(start))
			withinRange = true;
		return withinRange;
	}

	public static String getPageDesc(String url) {
		String val = "Arvene Healthcare is India's leading medical tourism facilitators offering affordable medical treatment packages and surgeries for patients seeking world class treatment at competitive rates.";
		if (url.contains("cardiology-treatment"))
			val = "Browse Arvene Healthcare for the world class cardiology treatments and hospitals in India at optimal price. Contact Us Today!!";
		else if (url.contains("specialistRegister"))
			val = "Sign up with Arvene Healthcare for medical tourism hospitals in India. We are providing health care at par or above global standards.";
		else if (url.contains("userQuery"))
			val = "Online doctor consultation in India. Consult the best doctors in India across all specialties. Submit your medical information, book appointment and get useful health tips.";
		else if (url.contains("coronary-angiography"))
			val = "Find the best coronary angiography packages for your treatment in India. Arvene Healthcare provides you best coronary angiography on affordable price.";
		else if (url.contains("coronary-angioplasty"))
			val = "We are providing coronary angioplasty treatment in India at an affordable cost. To get best options for cardiology treatment, call us at (+91) 81300-77375.";
		else if (url.contains("coronary-artery-bypass-surgery"))
			val = "We have the expertise to organize for all treatments & surgeries related to coronary artery bypass (heart bypass) in India. Call us at (+91) 81300-77375.";
		else if (url.contains("paediatric-cardiac-surgery"))
			val = "Browse Arvene Healthcare for the world class paediatric cardiac surgeries and treatments in India at optimal price. Contact us today to avail best services.";
		else if (url.contains("pacemaker-implantation"))
			val = "Low cost of permanent pacemaker implantation in India with best heart surgeons with Arvene Healthcare is fast gaining popularity with medical tourists.";
		else if (url.contains("vascular-surgery"))
			val = "We at Arvene Healthcare have the expertise to organize for vascular surgery in India at affordable price. To get best options for vascular surgery, Call us at (+91) 81300-77375.";
		else if (url.contains("heart-valve-replacement-surgery"))
			val = "Looking for heart valve replacement surgery in India? Arvene Healthcare offering the best hospital for heart valve replacement at best price. Call us at (+91) 81300-77375.";
		else if (url.contains("cardiac-diagnostic"))
			val = "Find the best cardiac diagnostic services at Arvene Healthcare. To get best options for cardiology treatment, Call us at (+91) 81300-77375.";
		else if (url.contains("cosmetic-surgery"))
			val = "At Arvene Healthcare, we have the expertise to organize for all cosmetic surgery and treatments in India. Call us at (+91) 81300-77375.";
		else if (url.contains("breast-augmentation-surgery"))
			val = "Looking for breast implant or breast augmentation surgery in Delhi. Book your appointment at Arvene Healthcare with best breast surgeon to opt the procedure in India.";
		else if (url.contains("breast-reduction-surgery"))
			val = "We at Arvene Healthcare have the expertise to organize for all treatments and surgeries related to cosmetic surgery and breast reduction surgery in India.";
		else if (url.contains("liposuction-surgery"))
			val = "We at Arvene Healthcare have the expertise to organize for all treatments and surgeries related to liposuction surgery in India. Call us at (+91) 81300-77375.";
		else if (url.contains("facelift-surgery"))
			val = "We at Arvene Healthcare have the expertise to organize for all treatments and surgeries related to Facelift Surgery in India. Call us at (+91) 81300-77375.";
		else if (url.contains("rhinoplasty-surgery"))
			val = "At Arvene Healthcare, we have the expertise to organize for all treatments and surgeries related to Rhinoplasty Surgery in India. Call us at (+91) 81300-77375.";
		else if (url.contains("cosmetic-laser-surgery"))
			val = "At Arvene Healthcare, we have the expertise to organize for all treatments and surgeries related to cosmetic laser surgery in India. Call us at (+91) 81300-77375.";
		else if (url.contains("oral-maxillofacial-surgery"))
			val = "We at Arvene Healthcare have the expertise to organize for all treatments and surgeries related to oral and maxillofacial surgery in India. Call us at (+91) 81300-77375.";
		else if (url.contains("surgical-hair-transplant"))
			val = "At Arvene Healthcare, we have the expertise to organize for all treatments and surgeries related to Hair Transplantation in India. Call us at (+91) 81300-77375.";
		else if (url.contains("breast-cancer-treatment"))
			val = "Browse Arvene Healthcare for the world class breast cancer treatments and hospitals in India at optimal price. Call us at (+91) 81300-77375.";
		else if (url.contains("cervical-cancer-treatment"))
			val = "We at Arvene Healthcare have the expertise to organize for all treatments and surgeries related to cervical cancer treatment in India. Call us at (+91) 81300-77375.";
		else if (url.contains("prostate-cancer-treatment"))
			val = "We at Arvene Healthcare have the expertise to organize for all treatments and surgeries related to Prostrate Cancer in India. Call us at (+91) 81300-77375.";
		else if (url.contains("colon-cancer-treatment"))
			val = "We at Arvene Healthcare have the expertise to organize for all treatments and surgeries related to Colon Cancer in India. Call us at (+91) 81300-77375.";
		else if (url.contains("cyberknife-radiation-therapy"))
			val = "At Arvene Healthcare, we have the expertise to organize for all treatments and surgeries related to cberknife radiation therapy. Call us at (+91) 81300-77375.";
		else if (url.contains("neuro-spine-surgery"))
			val = "Browse Arvene Healthcare for the world class neuro and spine surgery treatments and hospitals in India at optimal price. Call us at (+91) 81300-77375.";
		else if (url.contains("neuro-surgery"))
			val = "Are you looking for Neuro Surgery in India? We have the expertise to organize for all treatments and surgeries related to Neuro and Spine. Contact us today!";
		else if (url.contains("neurology"))
			val = "We at Arvene Healthcare have the expertise to organize for all treatments and surgeries related to Neuro and Spine. Call us at (+91) 81300-77375.";
		else if (url.contains("brain-tumors-treatment"))
			val = "At Arvene Healthcare, we have the expertise to organize for all treatments and surgeries related to brain tumors in India. Call us at (+91) 81300-77375.";
		else if (url.contains("spinal-fusion-surgery"))
			val = "Arvene Healthcare provides the spinal fusion, decompression & arthroplasty treatments & surgeries at affordable price. Call us at (+91) 81300-77375.";
		else if (url.contains("intracranial-aneurysm-surgery"))
			val = "We at Arvene Healthcare have the expertise to organize for intracranial aneurysm surgery and treatments in India at a low cost. Call us at (+91) 81300-77375.";
		else if (url.contains("spinal-tumor-surgery"))
			val = "At Arvene Healthcare, we have the expertise to organize for all treatments and surgeries related to spinal tumor in India. Call us at (+91) 81300-77375.";
		else if (url.contains("spinal-laminectomy-surgery"))
			val = "At Arvene Healthcare, we have the expertise to organize for all treatments and surgeries related to spinal laminectomy in India. Call us at (+91) 81300-77375.";
		else if (url.contains("orthopaedics-surgery"))
			val = "We at Arvene Healthcare have the expertise to organize for all treatments and surgeries related to orthopedics in India. Call us at (+91) 81300-77375.";
		else if (url.contains("knee-replacement-surgery"))
			val = "Looking for knee replacement surgery in India? We provide best knee joint replacement and knee implants at an affordable cost from Arvene Healthcare.";
		else if (url.contains("hip-replacement-surgery"))
			val = "We are experts in providing excellent services for bone joint and hip replacement surgery in India. We are associated with top hospitals and Doctors of India in this field.";
		else if (url.contains("shoulder-surgery"))
			val = "We at Arvene Healthcare have the expertise to organize for shoulder surgery in India at affordable price. We are associated with top hospitals and Doctors of India in this field.";
		else if (url.contains("foot-ankle-surgery"))
			val = "At Arvene Healthcare, we have the expertise to organize for all treatments and surgeries related to foot and ankle in India. We are associated with top hospitals and Doctors of India in this field.";
		else if (url.contains("hand-wrist-surgery"))
			val = "We at Arvene Healthcare have the expertise to organize for all treatments and surgeries related to hand and wrist in India. We are associated with top hospitals and Doctors of India in this field.";
		else if (url.contains("elbow-replacement-surgery"))
			val = "We at Arvene Healthcare have the expertise to organize for all treatments and surgeries related to elbow in India. We are associated with top hospitals and Doctors of India in this field.";
		else if (url.contains("organ-transplant"))
			val = "Browse Arvene Healthcare for the world class multi organ transplant like kidney transplant surgery, liver transplant surgery treatments and hospitals in India at optimal price.";
		else if (url.contains("kidney-transplant-surgery"))
			val = "Kidney Transplant Hospitals in India with Arvene Healthcare. We have the expertise to organize for all treatments and surgeries related to Kidney Transplant.";
		else if (url.contains("liver-transplant-surgery"))
			val = "Looking for liver transplant hospitals in India? We have the expertise to organize for all treatments and surgeries related to liver transplant at low cost.";
		else if (url.contains("heart-transplant"))
			val = "We at Arvene Healthcare have the expertise to organize for all treatments and surgeries related to Heart Transplant in India. Call us at (+91) 81300-77375.";
		else if (url.contains("bone-marrow-transplant"))
			val = "Bone marrow and stem cell transplant in India at best hospitals. Bone marrow transplantation cost in India with Arvene Healthcare is much affordable as compared to others.";
		else if (url.contains("human-organs-transplant-laws-india"))
			val = "The laws and rules of organ donation and transplantation in India. The buying & selling of human organs is absolutely illegal & punishable as per the human organs act 1994.";
		else if (url.contains("infertility-treatment"))
			val = "We at Arvene Healthcare have the expertise to organize for all treatments and surgeries related to Infertility in India. Call us at (+91) 81300-77375 today.";
		else if (url.contains("IVF-treatment"))
			val = "We have the expertise to organize for all treatments & surgeries related to IVF (In-Vitro Fertilization) in India. Call us at (+91) 81300-77375.";
		else if (url.contains("IUI-treatment"))
			val = "Looking for Intrauterine insemination (IUI) treatment in India? We provide IUI Treatment for fast moving sperms is separated from more sluggish or non-moving sperms.";
		else if (url.contains("bariatric-surgery"))
			val = "We at Arvene Healthcare have the expertise to organize for all treatments and surgeries related to Bariatric (Weight Loss) in India. Call us at (+91) 81300-77375.";
		else if (url.contains("gastric-bypass-surgery"))
			val = "Are you looking for gastric bypass surgery in India? We have the expertise to organize for all treatments & surgeries related to Gastric Bypass at affordable price.";
		else if (url.contains("gastric-sleeve-surgery"))
			val = "At Arvene Healthcare, we have the expertise to organize for all treatments & surgeries related to laparoscopic sleeve gastrectomy in India. Call us at (+91) 81300-77375.";
		else if (url.contains("urology-treatment"))
			val = "We at Arvene Healthcare have the expertise to organize for all treatments and surgeries related to Urology at affordable cost. Call us at (+91) 81300-77375.";
		else if (url.contains("endoscopic-surgery"))
			val = "At Arvene Healthcare, we have the expertise to organize for all treatments and surgeries related to Endoscopic in India. Call us at (+91) 81300-77375.";
		else if (url.contains("TURP-surgery"))
			val = "Find out hospitals and clinics that offer Transurethral resection of the prostate (TURP) surgery in major Indian cities along with their prices.";
		else if (url.contains("radical-prostatectomy-surgery"))
			val = "Robotic or laparoscopic radical prostatectomy offers complete removal of prostate cancer followed by faster recovery. Visit Arvene Healthcare for info on surgery cost.";
		else if (url.contains("medical-providers"))
			val = "Arvene Healthcare is associated with top hospitals and Doctors of India in this field. We can give you more and better options so that you can take the right decisions.";
		else if (url.contains("why-india"))
			val = "Why Choose India for Medical Tourism? We have best healthcare facilities, cutting edge technologies, finest doctors, monetary savings, fast track zero waiting time and more.";
		else if (url.contains("specialists"))
			val = "Our doctors have years of experience in the medical field and will treat you with good care and patience at Arvene Healthcare.";
		else if (url.contains("blog"))
			val = "Visit Arvene Healthcare Blog and read our latest update and news about cancer treatment, knee replacement surgery, kidney transplant surgery and more.";
		else if (url.contains("faq"))
			val = "If you have any query related to cancer treatment, kidney transplant surgery, bone marrow transplant and more, Please feel free to contact us!";
		else if (url.contains("about-us"))
			val = "Arvene Healthcare is an online initiative to connect to the patients globally & to offer them the best healthcare benefits in some of the most promising destinations in India.";
		else if (url.contains("cost-comparison"))
			val = "Compare the cost of medical treatments between US, Korea, Thailand, Malaysia and India. We are providing world class medical care at affordable price in India.";
		else if (url.contains("kairali"))
			val = "Arvene Healthcare offers Kairali Ayurvedic Treatments and Therapies in India at affordable cost. Call us at (+91) 81300-77375.";
		else if (url.contains("medical-process"))
			val = "Arvene Healthcare provides step by step process of medical tourism in India for ensuring your treatment is very safe and pleasurable in India.";
		else if (url.contains("air-ambulance-service"))
			val = "Find air ambulance service in India at the best cost in emergency need of patients. To book an air ambulance from your country to India, call us at (+91) 81300-77375.";
		else if (url.contains("privacy-policy"))
			val = "For Arvene Healthcare, our clients' privacy is really important, and we are committed to ensuring that their information is secure.";
		else if (url.contains("terms-conditions"))
			val = "These are Arvene Healthcare terms and conditions. The service agreement, special arrangements, provisions, requirements, specifications and others are listed here.";
		else if (url.contains("cancer-treatment"))
			val = "Browse Arvene Healthcare for the world class cancer treatments and hospitals in India at optimal price. To get best options for cancer treatment, call us at (+91) 81300-77375.";
		return val;
	}

	public static String getPageKeywords(String url) {
		String val = "Best Hospitals in India, Health Tourism in India, Medical Tourism Hospitals in India, Medical Tourism India, Medical Treatment in India";
		if (url.contains("cardiology-treatment"))
			val = "Cardiology Treatments, Cardiology Treatments in India, Best Hospitals and Cost in India";
		else if (url.contains("specialistRegister"))
			val = "Best Hospitals in India, Health Tourism in India, Medical Tourism Hospitals in India, Medical Tourism India, Medical Treatment in India";
		else if (url.contains("userQuery"))
			val = "Doctor Consultation Online, Online Doctor Consultation India, Best Hospitals and Cost in India";
		else if (url.contains("coronary-angiography"))
			val = "Coronary Angiography, Coronary Angiography in India, Best Hospitals and Cost in India";
		else if (url.contains("coronary-angioplasty"))
			val = "Angioplasty Cost in India, Angioplasty in India, Best Hospitals and Cost in India";
		else if (url.contains("coronary-artery-bypass-surgery"))
			val = "Coronary Artery Bypass (Heart Bypass) Surgery in India, Best Hospitals and Cost in India";
		else if (url.contains("paediatric-cardiac-surgery"))
			val = "Paediatric Cardiac Surgery, Paediatric Cardiac Surgery in India, Best Hospitals and Cost in India";
		else if (url.contains("pacemaker-implantation"))
			val = "Pacemaker Implantation, Pacemaker Implantation in India, Best Hospitals and Cost in India";
		else if (url.contains("vascular-surgery"))
			val = "Vascular Surgery, Vascular Surgery in India, Vascular Surgery Cost in India, Best Hospitals and Cost in India";
		else if (url.contains("heart-valve-replacement-surgery"))
			val = "Heart Valve Replacement Surgery, Heart Valve Replacement Surgery in India, Best Hospitals and Cost in India";
		else if (url.contains("cardiac-diagnostic"))
			val = "Cardiac Diagnostic Services, Cardiac Diagnostic Services in India, Best Hospitals and Cost in India";
		else if (url.contains("cosmetic-surgery"))
			val = "Cosmetic Surgery, Cosmetic Surgery in India, Best Hospitals and Cost in India";
		else if (url.contains("breast-augmentation-surgery"))
			val = "Breast Augmentation Surgery, Breast Augmentation Surgery India, Best Hospitals and Cost in India";
		else if (url.contains("breast-reduction-surgery"))
			val = "Breast Reduction Surgery, Breast Reduction Surgery in India, Best Hospitals and Cost in India";
		else if (url.contains("liposuction-surgery"))
			val = "Liposuction Surgery, Liposuction Surgery in India, Best Hospitals and Cost in India";
		else if (url.contains("facelift-surgery"))
			val = "Facelift Surgery, Facelift Surgery in India, Best Hospitals and Cost in India";
		else if (url.contains("rhinoplasty-surgery"))
			val = "Rhinoplasty Surgery, Rhinoplasty Surgery in India, Best Hospitals and Cost in India";
		else if (url.contains("cosmetic-laser-surgery"))
			val = "Cosmetic Laser Surgery, Cosmetic Laser Surgery in India, Best Hospitals and Cost in India";
		else if (url.contains("oral-maxillofacial-surgery"))
			val = "Oral & Maxillofacial Surgery, Oral & Maxillofacial Surgery in India, Best Hospitals and Cost in India";
		else if (url.contains("surgical-hair-transplant"))
			val = "Surgical Hair Transplantation, Surgical Hair Transplantation in India, Best Hospitals and Cost in India";
		else if (url.contains("breast-cancer-treatment"))
			val = "Breast Cancer Treatment, Breast Cancer Treatment in India, Best Hospitals and Cost in India";
		else if (url.contains("cervical-cancer-treatment"))
			val = "Cervical Cancer Treatment, Cervical Cancer Treatment in India, Best Hospitals and Cost in India";
		else if (url.contains("prostate-cancer-treatment"))
			val = "Prostate Cancer Treatment, Prostate Cancer Treatment in India, Best Hospitals and Cost in India";
		else if (url.contains("colon-cancer-treatment"))
			val = "Colon Cancer Treatment, Colon Cancer Treatment in India, Best Hospitals and Cost in India";
		else if (url.contains("cyberknife-radiation-therapy"))
			val = "Cyberknife Radiation Therapy, Cyberknife Radiation Therapy India, Best Hospitals and Cost in India";
		else if (url.contains("neuro-spine-surgery"))
			val = "Neuro and Spine Surgery, Neuro and Spine Surgery in India, Best Hospitals and Cost in India";
		else if (url.contains("neuro-surgery"))
			val = "Neuro Surgery, Neuro Surgery in India, Best Hospitals and Cost in India";
		else if (url.contains("neurology"))
			val = "Neurology in India, Neurology Hospitals in India, Best Hospitals and Cost in India";
		else if (url.contains("brain-tumors-treatment"))
			val = "Brain Tumors Treatment, Brain Tumors Treatment in India, Best Hospitals and Cost in India";
		else if (url.contains("spinal-fusion-surgery"))
			val = "Spinal Fusion, Decompression & Arthroplasty in India, Best Hospitals and Cost in India";
		else if (url.contains("intracranial-aneurysm-surgery"))
			val = "Intracranial Aneurysm Surgery, Intracranial Aneurysm Surgery in India, Best Hospitals and Cost in India";
		else if (url.contains("spinal-tumor-surgery"))
			val = "Spinal Tumor Surgery, Spinal Tumor Surgery India, Best Hospitals and Cost in India";
		else if (url.contains("spinal-laminectomy-surgery"))
			val = "Spinal Laminectomy India, Spinal Laminectomy Surgery in India, Best Hospitals and Cost in India";
		else if (url.contains("orthopaedics-surgery"))
			val = "Orthopaedics Surgery, Orthopaedics Surgery India, Best Hospitals and Cost in India";
		else if (url.contains("knee-replacement-surgery"))
			val = "Knee Replacement Surgery Cost in India, Knee Replacement Surgery India, Best Hospitals and Cost in India";
		else if (url.contains("hip-replacement-surgery"))
			val = "Hip Replacement Surgery, Hip Replacement Surgery India, Best Hospitals and Cost in India";
		else if (url.contains("shoulder-surgery"))
			val = "Shoulder Surgery, Shoulder Surgery India, Best Hospitals and Cost in India";
		else if (url.contains("foot-ankle-surgery"))
			val = "Foot and Ankle Surgery India, Best Hospitals and Cost in India";
		else if (url.contains("hand-wrist-surgery"))
			val = "Hand & Wrist Surgery, Hand & Wrist Surgery India, Best Hospitals and Cost in India";
		else if (url.contains("elbow-replacement-surgery"))
			val = "Elbow Surgery, Elbow Surgery in India, Best Hospitals and Cost in India";
		else if (url.contains("organ-transplant"))
			val = "Multi Organ Transplant, Multi Organ Transplant India, Best Hospitals and Cost in India";
		else if (url.contains("kidney-transplant-surgery"))
			val = "Kidney Transplant Cost in India, Kidney Transplant Hospital in India, Kidney Transplant in India, Kidney Transplant Surgery in India";
		else if (url.contains("liver-transplant-surgery"))
			val = "Liver Transplant Cost India, Liver Transplant Hospitals in India, Liver Transplant Surgery in India, Liver Transplant Surgery India";
		else if (url.contains("heart-transplant"))
			val = "Heart Transplant Surgery in India, Heart Transplant India, Best Hospitals and Cost in India";
		else if (url.contains("bone-marrow-transplant"))
			val = "Bone Marrow Transplant Cost in India, Bone Marrow Transplant in India, Bone Marrow Transplant India, Stem Cell Transplant Cost in India, Stem Cell Transplant in India";
		else if (url.contains("human-organs-transplant-laws-india"))
			val = "Human Organs Transplant Laws in India, Best Hospitals and Cost in India";
		else if (url.contains("infertility-treatment"))
			val = "Infertility Treatment, Infertility Treatment in India, Best IVF Hospitals and Cost in India";
		else if (url.contains("IVF-treatment"))
			val = "IVF Treatment (In-Vitro Fertilization) India, Best IVF Hospitals and Cost in India";
		else if (url.contains("IUI-treatment"))
			val = "IUI Treatment India, Intrauterine insemination (IUI) Treatment India, Best Hospitals and Cost in India";
		else if (url.contains("bariatric-surgery"))
			val = "Bariatric Treatment India, Best Hospitals and Cost in India";
		else if (url.contains("gastric-bypass-surgery"))
			val = "Gastric Bypass Surgery India, Best Hospitals and Cost in India";
		else if (url.contains("gastric-sleeve-surgery"))
			val = "Laparoscopic Sleeve Gastrectomy Surgery India, Best Hospitals and Cost in India";
		else if (url.contains("urology-treatment"))
			val = "Urology Treatment in India, Best Hospitals and Cost in India";
		else if (url.contains("endoscopic-surgery"))
			val = "Endoscopic Surgery in India, Best Hospitals and Cost in India";
		else if (url.contains("TURP-surgery"))
			val = "Transurethral Resection of the Prostate (TURP) India, Best Hospitals and Cost in India";
		else if (url.contains("radical-prostatectomy-surgery"))
			val = "Radical Prostatectomy Surgery India, Best Hospitals and Cost in India";
		else if (url.contains("medical-providers"))
			val = "Best Hospitals in India, Health Tourism in India, Medical Tourism Hospitals in India, Medical Tourism India, Medical Treatment in India";
		else if (url.contains("why-india"))
			val = "Best Hospitals in India, Health Tourism in India, Medical Tourism Hospitals in India, Medical Tourism India, Medical Treatment in India";
		else if (url.contains("specialists"))
			val = "Best Hospitals in India, Health Tourism in India, Medical Tourism Hospitals in India, Medical Tourism India, Medical Treatment in India";
		else if (url.contains("blog"))
			val = "Best Hospitals in India, Health Tourism in India, Medical Tourism Hospitals in India, Medical Tourism India, Medical Treatment in India";
		else if (url.contains("faq"))
			val = "Best Hospitals in India, Health Tourism in India, Medical Tourism Hospitals in India, Medical Tourism India, Medical Treatment in India";
		else if (url.contains("about-us"))
			val = "Best Hospitals in India, Health Tourism in India, Medical Tourism Hospitals in India, Medical Tourism India, Medical Treatment in India";
		else if (url.contains("cost-comparison"))
			val = "Cost Comparison of Medical Treatments, Medical Tourism India, Medical Treatment in India";
		else if (url.contains("kairali"))
			val = "Kairali Ayurvedic Treatments in India, Best Hospitals and Cost in India";
		else if (url.contains("medical-process"))
			val = "Medical Treatments Process Flow, Medical Treatments Process Flow in India";
		else if (url.contains("air-ambulance-service"))
			val = "Air Ambulance Service, Air Ambulance Service in India";
		else if (url.contains("privacy-policy"))
			val = "Best Hospitals in India, Health Tourism in India, Medical Tourism Hospitals in India, Medical Tourism India, Medical Treatment in India";
		else if (url.contains("terms-conditions"))
			val = "Best Hospitals in India, Health Tourism in India, Medical Tourism Hospitals in India, Medical Tourism India, Medical Treatment in India";
		else if (url.contains("cancer-treatment"))
			val = "Cancer Treatment Cost in India, Cancer Treatment Hospitals in India, Cancer Treatment in India";
		return val;
	}

	public static String getPageTitle(String url) {
		String val = "Arvene Healthcare - Best Doctors in India, Doctor Consultation, Medical Tourism Hospitals and Affordable Treatments";
		if (url.contains("cardiology-treatment"))
			val = "Cardiology Treatments, Best Hospitals and Cost in India | Arvene Healthcare";
		else if (url.contains("coronary-artery-bypass-surgery"))
			val = "Coronary Artery Bypass (Heart Bypass) Surgery, Best Hospitals and Cost in India | Arvene Healthcare";
		else if (url.contains("paediatric-cardiac-surgery"))
			val = "Paediatric Cardiac Surgery, Best Hospitals and Cost in India | Arvene Healthcare";
		else if (url.contains("pacemaker-implantation"))
			val = "Permanent Pacemaker Implantation, Best Hospitals and Cost in India | Arvene Healthcare";
		else if (url.contains("vascular-surgery"))
			val = "Vascular Surgery, Best Hospitals and Cost in India | Arvene Healthcare";
		else if (url.contains("heart-valve-replacement-surgery"))
			val = "Heart Valve Replacement Surgery, Best Hospitals and Cost in India | Arvene Healthcare";
		else if (url.contains("cardiac-diagnostic"))
			val = "Cardiac Diagnostic Services, Best Hospitals and Cost in India | Arvene Healthcare";
		else if (url.contains("cosmetic-surgery"))
			val = "Cosmetic Surgery, Best Hospitals and Cost in India | Arvene Healthcare";
		else if (url.contains("breast-augmentation-surgery"))
			val = "Breast Augmentation Surgery, Best Hospitals and Cost in India | Arvene Healthcare";
		else if (url.contains("breast-reduction-surgery"))
			val = "Breast Reduction Surgery, Best Hospitals and Cost in India | Arvene Healthcare";
		else if (url.contains("liposuction-surgery"))
			val = "Liposuction Surgery, Best Hospitals and Cost in India | Arvene Healthcare";
		else if (url.contains("facelift-surgery"))
			val = "Facelift Surgery, Best Hospitals and Cost in India | Arvene Healthcare";
		else if (url.contains("rhinoplasty-surgery"))
			val = "Rhinoplasty Surgery, Best Hospitals and Cost in India | Arvene Healthcare";
		else if (url.contains("cosmetic-laser-surgery"))
			val = "Cosmetic Laser Surgery, Best Hospitals and Cost in India | Arvene Healthcare";
		else if (url.contains("oral-maxillofacial-surgery"))
			val = "Oral & Maxillofacial Surgery, Best Hospitals and Cost in India | Arvene Healthcare";
		else if (url.contains("surgical-hair-transplant"))
			val = "Surgical Hair Transplantation, Best Hospitals and Cost in India | Arvene Healthcare";
		else if (url.contains("breast-cancer-treatment"))
			val = "Breast Cancer Treatment, Best Hospitals and Cost in India | Arvene Healthcare";
		else if (url.contains("cervical-cancer-treatment"))
			val = "Cervical Cancer Treatment, Best Hospitals and Cost in India | Arvene Healthcare";
		else if (url.contains("prostate-cancer-treatment"))
			val = "Prostate Cancer Treatment, Best Hospitals and Cost in India | Arvene Healthcare";
		else if (url.contains("colon-cancer-treatment"))
			val = "Colon Cancer Treatment, Best Hospitals and Cost in India | Arvene Healthcare";
		else if (url.contains("cyberknife-radiation-therapy"))
			val = "Cyberknife Radiation Therapy, Best Hospitals and Cost in India | Arvene Healthcare";
		else if (url.contains("neuro-spine-surgery"))
			val = "Neuro and Spine Surgery, Best Hospitals and Cost in India | Arvene Healthcare";
		else if (url.contains("neuro-surgery"))
			val = "Neuro Surgery, Best Hospitals and Cost in India | Arvene Healthcare";
		else if (url.contains("neurology"))
			val = "Neurology Hospitals, Best Hospitals and Cost in India | Arvene Healthcare";
		else if (url.contains("brain-tumors-treatment"))
			val = "Brain Tumors Treatment, Best Hospitals and Cost in India | Arvene Healthcare";
		else if (url.contains("spinal-fusion-surgery"))
			val = "Spinal Fusion, Decompression & Arthroplasty, Best Hospitals and Cost in India | Arvene Healthcare";
		else if (url.contains("intracranial-aneurysm-surgery"))
			val = "Intracranial Aneurysm Surgery, Best Hospitals and Cost in India | Arvene Healthcare";
		else if (url.contains("spinal-tumor-surgery"))
			val = "Spinal Tumor Surgery, Best Hospitals and Cost in India | Arvene Healthcare";
		else if (url.contains("spinal-laminectomy-surgery"))
			val = "Spinal Laminectomy Surgery, Best Hospitals and Cost in India | Arvene Healthcare";
		else if (url.contains("orthopaedics-surgery"))
			val = "Orthopaedics Surgery, Best Hospitals and Cost in India | Arvene Healthcare";
		else if (url.contains("knee-replacement-surgery"))
			val = "Knee Replacement Surgery, Hospitals and Cost in India | Arvene Healthcare";
		else if (url.contains("hip-replacement-surgery"))
			val = "Hip Replacement Surgery, Best Hospitals and Cost in India | Arvene Healthcare";
		else if (url.contains("shoulder-surgery"))
			val = "Shoulder Surgery, Best Hospitals and Cost in India | Arvene Healthcare";
		else if (url.contains("foot-ankle-surgery"))
			val = "Foot and Ankle Surgery, Best Hospitals and Cost in India | Arvene Healthcare";
		else if (url.contains("hand-wrist-surgery"))
			val = "Hand & Wrist Surgery, Best Hospitals and Cost in India | Arvene Healthcare";
		else if (url.contains("elbow-replacement-surgery"))
			val = "Elbow Surgery, Best Hospitals and Cost in India | Arvene Healthcare";
		else if (url.contains("organ-transplant"))
			val = "Multi Organ Transplant, Best Hospitals and Cost in India | Arvene Healthcare";
		else if (url.contains("kidney-transplant-surgery"))
			val = "Kidney Transplant, Renal Transplant Surgery, Best Hospitals and Cost in India | Arvene Healthcare";
		else if (url.contains("liver-transplant-surgery"))
			val = "Liver Transplant Surgery, Best Hospitals and Cost in India | Arvene Healthcare";
		else if (url.contains("heart-transplant"))
			val = "Heart Transplant Surgery, Best Hospitals and Cost in India | Arvene Healthcare";
		else if (url.contains("bone-marrow-transplant"))
			val = "Bone Marrow and Stem Cell Transplant, Best Hospitals and Cost in India | Arvene Healthcare";
		else if (url.contains("human-organs-transplant-laws-india"))
			val = "Human Organs Transplant Laws, Best Hospitals and Cost in India | Arvene Healthcare";
		else if (url.contains("infertility-treatment"))
			val = "Infertility Treatment, Best IVF Hospitals and Cost in India | Arvene Healthcare";
		else if (url.contains("IVF-treatment"))
			val = "IVF Treatment (In-Vitro Fertilization), Best IVF Hospitals and Cost in India | Arvene Healthcare";
		else if (url.contains("IUI-treatment"))
			val = "Intrauterine insemination (IUI) Treatment, Best Hospitals and Cost in India | Arvene Healthcare";
		else if (url.contains("bariatric-surgery"))
			val = "Bariatric Treatment, Best Hospitals and Cost in India | Arvene Healthcare";
		else if (url.contains("gastric-bypass-surgery"))
			val = "Gastric Bypass Surgery, Best Hospitals and Cost in India | Arvene Healthcare";
		else if (url.contains("gastric-sleeve-surgery"))
			val = "Laparoscopic Sleeve Gastrectomy Surgery, Best Hospitals and Cost in India | Arvene Healthcare";
		else if (url.contains("urology-treatment"))
			val = "Urology Treatment, Best Hospitals and Cost in India | Arvene Healthcare";
		else if (url.contains("endoscopic-surgery"))
			val = "Endoscopic Surgery, Best Hospitals and Cost in India | Arvene Healthcare";
		else if (url.contains("TURP-surgery"))
			val = "Transurethral Resection of the Prostate (TURP) Surgery, Best Hospitals and Cost in India | Arvene Healthcare";
		else if (url.contains("radical-prostatectomy-surgery"))
			val = "Radical Prostatectomy Surgery, Best Hospitals and Cost in India | Arvene Healthcare";
		else if (url.contains("medical-providers"))
			val = "Medical Providers, Best Hospitals and Cost in India | Arvene Healthcare";
		else if (url.contains("why-india"))
			val = "Why Choose India for Medical Tourism | Arvene Healthcare";
		else if (url.contains("specialists"))
			val = "Our Doctors | Arvene Healthcare";
		else if (url.contains("blog"))
			val = "Our Blog | Arvene Healthcare";
		else if (url.contains("faq"))
			val = "Frequently Asked Questions | Arvene Healthcare";
		else if (url.contains("about-us"))
			val = "About Arvene Healthcare | Health Tourism in India";
		else if (url.contains("cost-comparison"))
			val = "Cost Comparison of Medical Treatments | Arvene Healthcare";
		else if (url.contains("kairali"))
			val = "Kairali Ayurvedic Treatments, Best Hospitals and Cost in India | Arvene Healthcare";
		else if (url.contains("medical-process"))
			val = "Medical Treatments Process Flow, Best Hospitals and Cost in India | Arvene Healthcare";
		else if (url.contains("air-ambulance-service"))
			val = "Air Ambulance Service, Best Hospitals and Cost in India | Arvene Healthcare";
		else if (url.contains("privacy-policy"))
			val = "Privacy Policy | Arvene Healthcare";
		else if (url.contains("terms-conditions"))
			val = "Terms and Conditions | Arvene Healthcare";
		else if (url.contains("specialistRegister"))
			val = "Sign Up With Us | Arvene Healthcare";
		else if (url.contains("userQuery"))
			val = "Online Doctor Consultation India | Arvene Healthcare";
		else if (url.contains("coronary-angiography"))
			val = "Coronary Angiography Treatment, Best Hospitals and Cost in India | Arvene Healthcare";
		else if (url.contains("coronary-angioplasty"))
			val = "Coronary Angioplasty Treatment, Best Hospitals and Cost in India | Arvene Healthcare";
		else if (url.contains("cancer-treatment"))
			val = "Cancer Treatment, Best Hospitals and Low Cost Treatment in India | Arvene Healthcare";
		return val;
	}
}

package com.medicine.arvene.util;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.web.util.RegexRequestMatcher;
import org.springframework.security.web.util.RequestMatcher;

@SuppressWarnings("deprecation")
public class CsrfSecurityRequestMatcher implements RequestMatcher {
	private RegexRequestMatcher unprotectedMatcher = new RegexRequestMatcher(
			"/paymentSuccess", null);

	@Override
	public boolean matches(HttpServletRequest request) {
		if (unprotectedMatcher.matches(request)) {
			return false;
		}

		return true;
	}
}
/**
 * 
 */
package com.medicine.arvene.util;

/**
 * @author maddie
 *
 */
public interface IPaymentConstants {
	
	
	//PAYPAL PARAMS
	public String PAYPAL_PARAM_MC_GROSS = "mc_gross";
	public String PAYPAL_PARAM_PROTECTION_ELIGIBLE ="protection_eligibility";
	public String PAYPAL_PARAM_PAYER_ID ="payer_id";
	public String PAYPAL_PARAM_TAX ="tax";
	public String PAYPAL_PARAM_PAYMENT_DATE ="payment_date";
	public String PAYPAL_PARAM_PAYMENT_STATUS ="payment_status";
	public String PAYPAL_PARAM_CHARSET ="charset";
	public String PAYPAL_PARAM_SENDER_FIRST_NAME ="first_name";
	public String PAYPAL_PARAM_MC_FEE ="mc_fee";
	public String PAYPAL_PARAM_NOTIFY_VERSION ="notify_version";
	public String PAYPAL_PARAM_CUSTOM ="custom";
	public String PAYPAL_PARAM_PAYER_STATUS ="payer_status";
	public String PAYPAL_PARAM_BUSINESS ="business";
	public String PAYPAL_PARAM_QUANTITY ="quantity";
	public String PAYPAL_PARAM_PAYER_EMAIL ="payer_email";
	public String PAYPAL_PARAM_VERIFY_SIGN ="verify_sign";
	public String PAYPAL_PARAM_TXN_ID ="txn_id";
	public String PAYPAL_PARAM_PAYMENT_TYPE ="payment_type";
	public String PAYPAL_PARAM_SENDER_LAST_NAME	 ="last_name";
	public String PAYPAL_PARAM_PARAM_RECEIVER_EMAIL ="receiver_email";
	public String PAYPAL_PARAM_PAYMENT_FEE ="payment_fee";
	public String PAYPAL_PARAM_RECEIVER_ID ="receiver_id";
	public String PAYPAL_PARAM_TXN_TYPE ="txn_type";
	public String PAYPAL_PARAM_ITEM_NAME ="item_name";
	public String PAYPAL_PARAM_MC_CURRENCY ="mc_currency";
	public String PAYPAL_PARAM_ITEM_NUMBER ="item_number";
	public String PAYPAL_PARAM_RESIDENCE_COUNTRY ="residence_country";
	public String PAYPAL_PARAM_TEST_IPN ="test_ipn";
	public String PAYPAL_PARAM_HANDLING_AMOUNT ="handling_amount";
	public String PAYPAL_PARAM_TRANSACTION_SUBJECT ="transaction_subject";
	public String PAYPAL_PARAM_PAYMENT_GROSS ="payment_gross";
	public String PAYPAL_PARAM_SHIPPING ="shipping";
	public String PAYPAL_PARAM_AUTH ="auth";
	
	//This variable is set only if payment_status is Pending
	public String PAYPAL_PARAM_PENDING_REASON = "pending_reason";
	
	//This variable is set if payment_status is Reversed, Refunded, Canceled_Reversal, or Denied.
	public String  PAYPAL_PARAM_PENDING_REASON_CODE ="reason_code";
	
	
	//Status for PAYPAL TRANSACTIONS 
	public String PAYPAL_PAYMENT_STATUS_COMPLETED = "Completed";
	public String PAYPAL_PAYMENT_STATUS_CANCELED_REVERSAL = "Canceled_Reversal";
	public String PAYPAL_PAYMENT_STATUS_CREATED = "Created";
	public String PAYPAL_PAYMENT_STATUS_DENIED = "Denied";
	public String PAYPAL_PAYMENT_STATUS_EXPIRED = "Expired";
	public String PAYPAL_PAYMENT_STATUS_FAILED = "Failed";
	public String PAYPAL_PAYMENT_STATUS_PENDING = "Pending";
	public String PAYPAL_PAYMENT_STATUS_REFUNDED = "Refunded";
	public String PAYPAL_PAYMENT_STATUS_REVERSED = "Reversed";
	public String PAYPAL_PAYMENT_STATUS_PROCESSED = "Processed";
	
	//Unique ID generated during guest checkout (payment by credit card without logging in).
	public String PAYPAL_RECEIPT_ID = "receipt_id";

	public String REQ_HASH_SEQUENCE = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
	public String SUCCESS_URL = "https://yourmedics.in/paymentSuccess";
	public String FAILURE_URL = "https://yourmedics.in/paymentFailure";
	public String HASH_ALGO = "SHA-512";

	// FOR DEV
	/*
	 * public String SALT = "sfEU8JSF"; public String KEY = "AjV3JH"; public
	 * String PRODUCT = "test_product"; public String SERVICE_PROVIDER =
	 * "payu_paisa"; public String SERVICE_URL =
	 * "https://test.payu.in/_payment";
	 */

	// FOR PROD
	public String SALT = "u28ngwj6";
	public String KEY = "pfrTdp";
	public String PRODUCT = "yourMedics";
	public String SERVICE_PROVIDER = "payu_paisa";
	public String SERVICE_URL = "https://secure.payu.in/_payment";

	// Response Parameters

	public String PARAM_TRANSACTION_ID = "txnid";
	public String PARAM_PAYU_ID = "mihpayid";
	public String PARAM_MODE = "mode";
	public String PARAM_STATUS = "status";
	public String PARAM_PAYU_STATUS = "unmappedstatus";
	public String PARAM_KEY = "key";
	public String PARAM_AMOUNT = "amount";
	public String PARAM_DATE_ADDED = "addedon";
	public String PARAM_PRODUCT = "productinfo";
	public String PARAM_FIRST_NAME = "firstname";
	public String PARAM_LAST_NAME = "lastname";
	public String PARAM_EMAIL = "email";
	public String PARAM_PHONE = "phone";
	public String PARAM_USER_FIELD_1 = "udf1";
	public String PARAM_USER_FIELD_2 = "udf2";
	public String PARAM_USER_FIELD_3 = "udf3";
	public String PARAM_USER_FIELD_4 = "udf4";
	public String PARAM_USER_FIELD_5 = "udf5";
	public String PARAM_USER_FIELD_6 = "udf6";
	public String PARAM_USER_FIELD_7 = "udf7";
	public String PARAM_USER_FIELD_8 = "udf8";
	public String PARAM_USER_FIELD_9 = "udf9";
	public String PARAM_USER_FIELD_10 = "udf10";
	public String PARAM_HASH = "hash";
	public String PARAM_PG_TYPE = "PG_TYPE";
	public String PARAM_ENCRYP_PAY_ID = " encryptedPaymentId";
	public String PARAM_BANK_REF_NO = "bank_ref_num";
	public String PARAM_ERR_CODE = "error";
	public String PARAM_ERR_DESC = "error_Message";
	public String PARAM_DISCOUNT = "discount";
	public String PARAM_NET_AMOUNT = "net_amount_debit";

}

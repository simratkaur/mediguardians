package com.medicine.arvene.util;

import java.io.UnsupportedEncodingException;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.servlet.http.HttpServletRequest;

import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import com.medicine.arvene.model.Booking;
import com.medicine.arvene.model.ContactUsForm;
import com.medicine.arvene.model.SpecialistDetails;
import com.medicine.arvene.model.User;

public class Mailer {

	String subject;

	String message;

	String from = "contact@arvenehealthcare.com";

	String ADMIN_EMAIL = "contact@arvenehealthcare.com";

	public Mailer() {
		super();
	}

	public Mailer(String subject, String message) {
		super();
		this.subject = subject;
		this.message = message;
	}

	public JavaMailSenderImpl getMailSender() {
		// sends the e-mail
		JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
		mailSender.setHost("smtpout.secureserver.net");
		mailSender.setUsername(from);
		mailSender.setPassword("@ahc@123@contact");
		Properties javaMailProperties = new Properties();
		javaMailProperties.setProperty("mail.transport.protocol", "smtp");
		javaMailProperties.setProperty("mail.smtp.auth", "true");
		javaMailProperties.setProperty("mail.smtp.starttls.enable", "true");
		mailSender.setJavaMailProperties(javaMailProperties);
		return mailSender;
	}

	public void sendDocWelcomeMail(User user) throws AddressException,
			MessagingException {
		message = "<p><b><span>Dear Doctor</span></b><span>,</span></p>";
		message += "<p><span>&nbsp;</span></p>";
		message += "<p><b><span>Thank you for your interest in partnering with Arvene Healthcare™ </span></b></p>";
		message += "<p><b><span>&nbsp;</span></b></p>";
		message += "<p><span>Our team will contact you shortly to complete your registration process. </span></p>";
		message += "<p><span>&nbsp;</span></p>";
		message += "<p><span>We deeply value your relationship and look forward to a long term association with you.</span></p>";
		message += "<p><span style='color:#005392'>&nbsp;</span></p>";
		message += "<p><span style='color:#005392'>Warm Regards,</span></p>";
		message += "<p><span style='color:#005392'>Sumit Wadhwa-Managing Director</span></p>";
		message += "<p><span style='color:#005392'>Arvene Healthcare Pvt. Ltd.</span></p>";
		message += "<p><span style='color:#005392'>5th Floor, Quest offices, Golf course road,</span></p>";
		message += "<p><span style='color:#005392'>Sector 53, Golf course road, Gurgaon</span></p>";
		message += "<p><span style='color:#005392'>+91-11-4167-4042/+91-9910-665551</span></p>";

		subject = "Welcome on-board Arvene Healthcare.";

		MimeMultipart multipart = new MimeMultipart("related");

		BodyPart messageBodyPart = new MimeBodyPart();
		messageBodyPart.setContent(message, "text/html");
		// add it
		multipart.addBodyPart(messageBodyPart);

		new Mailer(subject, message).sendHtmlMail(user, multipart);

	}

	public void sendVerificationMail(User user, HttpServletRequest request) throws MessagingException {
		String subject = "[Arvene Healthcare] - Registration Success! Please verify your account.";
		String message = "<p><b>Hello</b></p>";
		message += "<p><b>Thank you for choosing Arvene Healthcare™, one of the fastest growing healthcare network for your healthcare assistance.</b></p>";
		message += "<a href='"
				+ request
						.getRequestURL()
						.toString()
						.replace(request.getRequestURI(),
								request.getContextPath())
				+ "/verifyUser?token=" + user.getVerificationCode() + "&email="
				+ user.getUsername()
				+ "'>Click to confirm your registration</a>";
		message += "<p><b>Connect to world class healthcare services at affordable prices!</b></p>";
		message += "<p><b>&nbsp;</span></b></p>";
		message += "<p><span>   •     </span><span>JCI &amp; NABH hospitals with advance medical &amp; diagnostic facilities</span></p>";
		message += "<p><span>   •     </span><span>Super specialist surgeons/doctors on board</span></p>";
		message += "<p><span>   •     </span><span>Affordable treatment plans</span></p>";
		message += "<p><span>   •     </span><span>One point contact for travel, visa assistance and stay requirements</span></p>";
		message += "<p><span>   •     </span><span>24*7 patient assistance to guide you on every step for your treatment</span></p>";
		message += "<p><span>   •	 </span><span>Second opinion at	affordable cost-get medical answers within hours.</span></p>";
		message += "&nbsp;";
		message += "<p><span style='color:#005392'>Wishing you best of Health</span></p>";
		message += "<p><span style='color:#005392'>&nbsp;</span></p>";
		message += "<p><span style='color:#005392'>Sumit Wadhwa-Managing Director</span></p>";
		message += "<p><span style='color:#005392'>Arvene Healthcare Pvt. Ltd.</span></p>";
		message += "<p><span style='color:#005392'>5th Floor, Quest offices, Golf course road,</span></p>";
		message += "<p><span style='color:#005392'>Sector 53, Golf course road, Gurgaon</span></p>";
		message += "<p><span style='color:#005392'>+91-11-4167-4042</span></p>";

		MimeMultipart multipart = new MimeMultipart("related");

		BodyPart messageBodyPart = new MimeBodyPart();
		messageBodyPart.setContent(message, "text/html");
		// add it
		multipart.addBodyPart(messageBodyPart);

		new Mailer(subject, message).sendHtmlMail(user, multipart);

	}

	public void sendDocQueryMail(SpecialistDetails specialistDetails)
			throws UnsupportedEncodingException, MessagingException {

		String subject = "New Query Notification Mail from Arvene";
		String message = "<p><span>Dear Dr " + specialistDetails.getFirstName()
				+ "</span></p>";
		message += "<p><span>&nbsp;</span></p>";
		message += "<p><span>You have received a new text query on Arvene Healthcare.</span></p>";
		message += "<p><span>&nbsp;</span></p>";
		message += "<p><span>Please click on </span><a href='http://www.arvenehealthcare.com'><span>www.arvenehealthcare.com</a></span><span> and sign in using your registered email id and password to answer. </span></p>";
		message += "<p><span>&nbsp;</span></p>";
		message += "<p><span>For assistance, pls. write to </span><span><a href='mailto:contact@arvenehealthcare.com'><span>contact@arvenehealthcare.com</span></a></span><span> </span></p>";
		message += "<p><span>&nbsp;</span></p>";
		message += "<p><span>Team Arvene Healthcare</span></p>";
		MimeMultipart multipart = new MimeMultipart("related");

		BodyPart messageBodyPart = new MimeBodyPart();
		messageBodyPart.setContent(message, "text/html");
		// add it
		multipart.addBodyPart(messageBodyPart);

		new Mailer(subject, message).sendHtmlMail(specialistDetails.getUser(), multipart);
	}

	public void sendAdminQueryMail() throws UnsupportedEncodingException, MessagingException {

		String subject = "New Query Notification Mail from Arvene";
		String message = "Hi,\n\n\tA new query has been received, Please visit Arvene Website to check the details and take the appropriate action"
				+ "\n\nTeam Arvene Healthcare";

		new Mailer(subject, message).sendAdminEmail(null);
	}

	public void sendHtmlMail(String email, MimeMultipart multipart)
			throws AddressException, MessagingException {
		JavaMailSenderImpl mailSender = getMailSender();
		MimeMessage mimeMessage = mailSender.createMimeMessage();
		mimeMessage.setFrom(new InternetAddress(from));

		mimeMessage.setRecipients(Message.RecipientType.TO,
				InternetAddress.parse(email));

		mimeMessage.setSubject(subject);

		mimeMessage.setContent(multipart);

		mailSender.send(mimeMessage);

	}

	public void sendHtmlMail(User user, MimeMultipart multipart)
			throws AddressException, MessagingException {
		JavaMailSenderImpl mailSender = getMailSender();
		MimeMessage mimeMessage = mailSender.createMimeMessage();
		mimeMessage.setFrom(new InternetAddress(from));

		mimeMessage.setRecipients(Message.RecipientType.TO,
				InternetAddress.parse(user.getUsername()));

		mimeMessage.setSubject(subject);

		mimeMessage.setContent(multipart);

		mailSender.send(mimeMessage);

	}

	public void sendRespQueryMail(User user)
			throws UnsupportedEncodingException, MessagingException {

		String subject = "Your Query Response Notification from Arvene Healthcare";
		message = "<p><span>Dear " + user.getUsername() + "</span></p>";
		message += "<p><span>&nbsp;</span></p>";
		message += "<p><span>You medical query has been replied by the doctor. Please visit </span><span>";
		message += "<a href='http://www.arvenehealthcare.com'><span>www.arvenehealthcare.com</span></a></span><span>";
		message += " and sign in using your registered id and password to check the medical answer.</span></p>";
		message += "<p><span>&nbsp;</span></p>";
		message += "<p><span>Wish you a speedy recovery.</span></p>";
		message += "<p><span>&nbsp;</span></p>";
		message += "<p><span>Team Arvene Healthcare</span></p>";
		MimeMultipart multipart = new MimeMultipart("related");

		BodyPart messageBodyPart = new MimeBodyPart();
		messageBodyPart.setContent(message, "text/html");
		// add it
		multipart.addBodyPart(messageBodyPart);

		new Mailer(subject, message).sendHtmlMail(user, multipart);
	}

	public void sendMail(User user) {
		SimpleMailMessage email = new SimpleMailMessage();
		email.setTo(user.getUsername());
		email.setSubject(subject);
		email.setText(message);
		email.setFrom(from);

		getMailSender().send(email);
	}

	public void sendPwdMail(User user, HttpServletRequest request) throws MessagingException {
		String subject = "Password Reset Mail from Arvene Healthcare";
		message = "<p><a href='"
				+ request
						.getRequestURL()
						.toString()
						.replace(request.getRequestURI(),
								request.getContextPath())
				+ "/passwordReset?token=" + user.getVerificationCode()
				+ "&email=" + user.getUsername()
				+ "'>Please click here to reset your password</a></p>";
		message += "<p><span>Please ignore if you have not requested for a new password. </span></p>";
		message += "<p><span>&nbsp;</span></p>";
		message += "<p><span>Thank You</span></p>";
		message += "<p><span>Team Arvene Healthcare</span></p>";
		MimeMultipart multipart = new MimeMultipart("related");

		BodyPart messageBodyPart = new MimeBodyPart();
		messageBodyPart.setContent(message, "text/html");
		// add it
		multipart.addBodyPart(messageBodyPart);

		new Mailer(subject, message).sendHtmlMail(user, multipart);
	}

	public void sendUserBookingConfirmMail(Booking booking)
			throws UnsupportedEncodingException, MessagingException {

		String subject = "Booking request confirmation Mail from Arvene";
		message = "<p><span>Dear " + booking.getSubscriber().getUsername()
				+ "</span></p>";
		message += "<p><span>&nbsp;</span></p>";
		message += "<p><span>Your paid video consultation with doctor "
				+ booking.getSpecialistDetails().getFirstName()
				+ " has been booked for a 15 minutes slot between "
				+ booking.getSlot().getStartdate() + " - "
				+ booking.getSlot().getEnddate() + "</span></p>";
		message += "<p><span>&nbsp;</span></p>";
		message += "<p><span>Please sign in at www.arvenehealthcare.com to consult your patient.  You are advised to login 15 minutes before the video consult time. Please switch on the camera and microphone of your computer and follow instructions. </span></p>";
		message += "<p><span>&nbsp;</span></p>";
		message += "<p><span>For any assistance, please write to </span><span><a href='mailto:contact@arvenehealthcare.com'><span>contact@arvenehealthcare.com</span></a></span><span> </span></p>";
		message += "<p><span>&nbsp;</span></p>";
		message += "<p><span>Wish you a speedy recovery.</span></p>";
		message += "<p><span>Team Arvene Healthcare</span></p>";

		MimeMultipart multipart = new MimeMultipart("related");

		BodyPart messageBodyPart = new MimeBodyPart();
		messageBodyPart.setContent(message, "text/html");
		// add it
		multipart.addBodyPart(messageBodyPart);

		new Mailer(subject, message).sendHtmlMail(booking.getSubscriber(), multipart);
	}

	public void sendDocNewSlotMail(Booking booking) throws MessagingException {
		subject = "New Slot added - Arvene Healthcare";
		message = "<p><span>Dear Doctor "
				+ booking.getSpecialistDetails().getFirstName() + "</span></p>";
		message += "<p><span>&nbsp;</span></p>";
		message += "<p><span>Your paid video consultation with patient has been booked for a 15 minutes slot between "
				+ booking.getSlot().getStartdate()
				+ " - "
				+ booking.getSlot().getEnddate() + "</span></p>";
		message += "<p><span>&nbsp;</span></p>";
		message += "<p><span>Please login at www.arvenehealthcare.com to consult your patient. </span></p>";
		MimeMultipart multipart = new MimeMultipart("related");

		BodyPart messageBodyPart = new MimeBodyPart();
		messageBodyPart.setContent(message, "text/html");
		// add it
		multipart.addBodyPart(messageBodyPart);

		new Mailer(subject, message).sendHtmlMail(booking.getSubscriber(), multipart);
		MimeMultipart multipart1 = new MimeMultipart("related");
		new Mailer(subject, message).sendAdminEmail(multipart1);

	}

	public void sendInquiryResponse(ContactUsForm inquiryForm)
			throws MessagingException {
		subject = "Your Enquiry has been responded - Arvene Healthcare";
		message = "<p>Hi,</p>";
		message += "<p><b>Your inquiry :</b>" + inquiryForm.getMessage()
				+ "</p>";
		message += "<p><b>Our Response :</b> " + inquiryForm.getResponse()
				+ "</p>";

		MimeMultipart multipart = new MimeMultipart("related");

		BodyPart messageBodyPart = new MimeBodyPart();
		messageBodyPart.setContent(message, "text/html");
		// add it
		multipart.addBodyPart(messageBodyPart);

		new Mailer(subject, message).sendHtmlMail(inquiryForm.getEmail(),
				multipart);

	}

	public void sendDocBookingMail(Booking booking)
			throws UnsupportedEncodingException, MessagingException {

		String subject = "New Booking Notification Mail from Arvene";
		String message = "<p><span>Dear Dr "
				+ booking.getSpecialistDetails().getFirstName() + "</span></p>";
		message += "<p><span>&nbsp;</span></p>";
		message += "<p><span>Your time slot for video consult is booked for "
				+ booking.getSlot().getStartdate() + "</span></p>";
		message += "<p><span>&nbsp;</span></p>";
		message += "<p><span>Please click on </span><span><a href='http://www.arvenehealthcare.com'><span>www.arvenehealthcare.com</span></a></span><span> and sign in using your registered email id and password to sign in and provide consultation. </span></p>";
		message += "<p><span>&nbsp;</span></p>";
		message += "<p><span>For assistance, pls. write to </span><span><a href='mailto:contact@arvenehealthcare.com'><span>contact@arvenehealthcare.com</span></a></span><span> </span></p>";
		message += "<p><span>&nbsp;</span></p>";
		message += "<p><span>Team Arvene Healthcare</span></p>";
		new Mailer(subject, message).sendMail(booking.getSubscriber());

		message = "<p><span>A new slot has been booked for user "
				+ booking.getSubscriber().getUsername() + " doctor "
				+ booking.getSpecialistDetails().getFirstName() + " at time "
				+ booking.getSlot().getStartdate() + "</span></p>";
		MimeMultipart multipart = new MimeMultipart("related");
		new Mailer(subject, message).sendAdminEmail(multipart);
	}

	public void sendUserWelcomeMail(User user) throws MessagingException {
		subject = "Welcome on-board Arvene Healthcare.";
		message = "<p>Dear Member,</p><p><span>Thank you for registering with us. A very warm welcome!!</span></p>";
		message += "<p><span>Let us take this opportunity to share with you some features of Your Medics for its optimal use*</span></p>";
		message += "<p><span><img width=596 height=156 src=\"cid:feature1\"></span></p>";
		message += "<p><span><img width=596 height=165 src=\"cid:feature2\"></span></p>";
		message += "<p><span><img width=596 height=164 src=\"cid:feature3\"></span></p>";
		message += "<p><span><img width=596 height=165 src=\"cid:feature4\"></span></p>";
		message += "<p>In case of any query or valuable feedback , please write to us at";
		message += "<a href=\"mailto:contactus@yourmedics.in\"><span class=Hyperlink0>contactus@yourmedics.in</span></a></p>";
		message += "<p>We wish you a healthy life.</p><p>-Team <b>YOUR MEDICS<o:p></o:p></b></p>";
		message += "<p style=\"float:right;font-size: 80%;\"><b>*</b><span>T&amp;C Applicable</span></p>";

		MimeMultipart multipart = new MimeMultipart("related");

		BodyPart messageBodyPart = new MimeBodyPart();
		messageBodyPart.setContent(message, "text/html");
		multipart.addBodyPart(messageBodyPart);

		String path = System.getProperty("user.dir")
				+ "/webapps/ROOT/WEB-INF/css/images/";
		messageBodyPart = new MimeBodyPart();
		DataSource fds = new FileDataSource(path + "feature1.png");
		messageBodyPart.setDataHandler(new DataHandler(fds));
		messageBodyPart.setHeader("Content-ID", "<feature1>");
		multipart.addBodyPart(messageBodyPart);

		messageBodyPart = new MimeBodyPart();
		fds = new FileDataSource(path + "feature2.png");
		messageBodyPart.setDataHandler(new DataHandler(fds));
		messageBodyPart.setHeader("Content-ID", "<feature2>");
		multipart.addBodyPart(messageBodyPart);

		messageBodyPart = new MimeBodyPart();
		fds = new FileDataSource(path + "feature3.png");
		messageBodyPart.setDataHandler(new DataHandler(fds));
		messageBodyPart.setHeader("Content-ID", "<feature3>");
		multipart.addBodyPart(messageBodyPart);

		messageBodyPart = new MimeBodyPart();
		fds = new FileDataSource(path + "feature4.png");
		messageBodyPart.setDataHandler(new DataHandler(fds));
		messageBodyPart.setHeader("Content-ID", "<feature4>");
		multipart.addBodyPart(messageBodyPart);

		new Mailer(subject, message).sendHtmlMail(user, multipart);
	}

	public void sendMailForApproval(SpecialistDetails specialist,
			HttpServletRequest request) {

		String subject = "[Arvene Healthcare] - A new doc has registered! Please verify the account.";
		String message = "Hi,\n\n\tPlease approve the below doc:" + "\n\n\t"
				+ specialist.getUser().getUsername()
				+ "\n\nTeam Arvene Healthcare";

		User user = new User();
		// TODO set admin email here
		// user.setUsername("");
		// new Mailer(subject, message).sendMail(user);

	}

	public void sendEnquiryMail(ContactUsForm contactUsForm) throws MessagingException {
		// TODO Auto-generated method stub
		String subject = "New Enquiry Notification Mail from Arvene";
		String message = "Hi,\n\n\tA new enquiry has been received as below -\n"
				+ "Enquiry - "
				+ contactUsForm.getMessage()
				+ "-\n"
				+ "Sender - "
				+ contactUsForm.getName()
				+ "-\n"
				+ "Sender email - "
				+ contactUsForm.getEmail()
				+ "-\n"
				+ "Sender phone - "
				+ contactUsForm.getPhone()
				+ "-\n"
				+ "Sender country - "
				+ contactUsForm.getCountry()
				+ "-\n"
				+ "\n\nTeam Arvene Healthcare";

		new Mailer(subject, message).sendAdminEmail(null);

	}

	public void sendAdminBookingRequestMail(Booking booking)
			throws UnsupportedEncodingException, MessagingException {

		String subject = "New Booking Request  from Arvene";
		String message = "A new slot has been requested by user "
				+ booking.getSubscriber().getUsername() + ", for doctor "
				+ booking.getSpecialistDetails().getFirstName() + " at time "
				+ booking.getSlot().getStartdate() + ".";
		new Mailer(subject, message).sendAdminEmail(null);
	}

	public void sendAdminEmail(MimeMultipart multipart) throws MessagingException {
		// TODO Auto-generated method stub
if(multipart == null){
		SimpleMailMessage email = new SimpleMailMessage();
		email.setTo(ADMIN_EMAIL);
		email.setSubject(subject);
		email.setText(message);
		email.setFrom(from);
		getMailSender().send(email);
}else
{
	BodyPart messageBodyPart = new MimeBodyPart();
	messageBodyPart.setContent(message, "text/html");
	// add it
	multipart.addBodyPart(messageBodyPart);

	new Mailer(subject, message).sendHtmlMail(ADMIN_EMAIL,
			multipart);}

	}

}

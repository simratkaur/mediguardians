package com.medicine.arvene.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;

import com.medicine.arvene.model.PaymentDetails;

@Component
public class PaymentUtil {
	
	
	
	
	public PaymentDetails populatePaymentDetailsPaypal(HttpServletRequest request) {

		PaymentDetails paymentDetails = new PaymentDetails();

		paymentDetails.setTransactionId(request
				.getParameter(IPaymentConstants.PAYPAL_PARAM_TXN_ID));
		paymentDetails.setPayuMihpayId(request
				.getParameter(IPaymentConstants.PAYPAL_PARAM_TXN_ID));
		paymentDetails.setEmail(request
				.getParameter(IPaymentConstants.PAYPAL_PARAM_PAYER_EMAIL));
		paymentDetails.setFirstName(request
				.getParameter(IPaymentConstants.PAYPAL_PARAM_SENDER_FIRST_NAME));
		paymentDetails.setLastName(request
				.getParameter(IPaymentConstants.PAYPAL_PARAM_SENDER_LAST_NAME));
//		paymentDetails.setPhone(request
//				.getParameter(IPaymentConstants.PARAM_PHONE));
		paymentDetails.setPaymentMode(request
				.getParameter(IPaymentConstants.PAYPAL_PARAM_TXN_TYPE));
		paymentDetails.setStatus(request
				.getParameter(IPaymentConstants.PAYPAL_PARAM_PAYMENT_STATUS));
		paymentDetails.setAmount(request
				.getParameter(IPaymentConstants.PAYPAL_PARAM_MC_GROSS) != null ? Float
				.parseFloat(request
						.getParameter(IPaymentConstants.PAYPAL_PARAM_MC_GROSS)) : 0);
		paymentDetails.setDiscount(request
				.getParameter(IPaymentConstants.PARAM_DISCOUNT) != null ? Float
				.parseFloat(request
						.getParameter(IPaymentConstants.PARAM_DISCOUNT)) : 0);
//		paymentDetails
//				.setNetAmountDebited(request
//						.getParameter(IPaymentConstants.PARAM_NET_AMOUNT) != null ? Float.parseFloat(request
//						.getParameter(IPaymentConstants.PARAM_NET_AMOUNT))
//						: 0);
		paymentDetails.setPayuStatus(request
				.getParameter(IPaymentConstants.PAYPAL_PARAM_PAYMENT_STATUS));
		paymentDetails.setPGType(request
				.getParameter(IPaymentConstants.PAYPAL_PARAM_PAYMENT_TYPE));
//		paymentDetails.setBankRefNum(request
//				.getParameter(IPaymentConstants.PARAM_BANK_REF_NO));
		
		//TODO NEED TO PARSE the date 
		paymentDetails.setTxnDate(request
				.getParameter(IPaymentConstants.PAYPAL_PARAM_PAYMENT_DATE));
		
		//TODO NEED TO remove once line 61 is fixed
		paymentDetails.setTxnDate(CommonUtil.getCurrentDate(new Date()));
		
		
		String status = paymentDetails.getPayuStatus();
		
		if(status.equalsIgnoreCase(IPaymentConstants.PAYPAL_PAYMENT_STATUS_PENDING)){
			paymentDetails.setErrorCode(request
					.getParameter(IPaymentConstants.PAYPAL_PARAM_PENDING_REASON));
		}
		
		if(status.equalsIgnoreCase(IPaymentConstants.PAYPAL_PAYMENT_STATUS_CANCELED_REVERSAL) ||
				status.equalsIgnoreCase(IPaymentConstants.PAYPAL_PAYMENT_STATUS_REFUNDED) ||
				status.equalsIgnoreCase(IPaymentConstants.PAYPAL_PAYMENT_STATUS_REVERSED) ||
				status.equalsIgnoreCase(IPaymentConstants.PAYPAL_PAYMENT_STATUS_DENIED)){
			
			paymentDetails.setErrorCode(request
					.getParameter(IPaymentConstants.PAYPAL_PARAM_PENDING_REASON_CODE));
		}
		
		
		
		

		return paymentDetails;

	}


	/**
	 * 
	 * 
	 * @param paymentForm
	 * @return String like has
	 * 
	 *         JBZaLc|148b5c19edb60a323403|100|prod|madhu|madhulika@abc.com||148
	 *         b5c19edb60a323403|||||||||GQs7yium
	 *         JBZaLc|13845141931447833603|100.0
	 *         |your-medics_product||madhulika@abc.com|||||||||||GQs7yium
	 * @throws NoSuchAlgorithmException
	 */

	public String gethashString(PaymentDetails paymentForm)
			throws NoSuchAlgorithmException {

		String hashString = "";
		String hash = "";

		String hashSequence = paymentForm.getReqHashSequence();
		String[] hashVarSeq = hashSequence.split("\\|");

		for (String part : hashVarSeq) {
			String hashVar = "";

			switch (part) {
			case IPaymentConstants.PARAM_KEY:
				hashVar = paymentForm.getKey();
				break;

			case "txnid":
				hashVar = paymentForm.getTransactionId();
				break;

			case "amount":
				hashVar = String.valueOf(paymentForm.getAmount());
				break;

			case "productinfo":
				hashVar = paymentForm.getProductInfo();
				break;

			case "firstname":
				hashVar = paymentForm.getFirstName();
				break;

			case "email":
				hashVar = paymentForm.getEmail();
				break;

			case "udf1":
				hashVar = paymentForm.getUdf1();
				break;

			case "udf2":
				hashVar = "";
				break;

			case "udf3":
				hashVar = "";
				break;

			case "udf4":
				hashVar = "";
				break;

			case "udf5":
				hashVar = "";
				break;

			case "udf6":
				hashVar = "";
				break;

			case "udf7":
				hashVar = "";
				break;

			case "udf8":
				hashVar = "";
				break;

			case "udf9":
				hashVar = "";
				break;

			case "udf10":
				hashVar = "";
				break;

			default:
				break;

			}

			hashString = hashString.concat(hashVar);
			hashString = hashString.concat("|");

		}

		hashString = hashString.concat(paymentForm.getSalt());

		hash = hashCal(paymentForm.getHashAlgo(), hashString);

		return hash;

	}

	public String getNewTxnId() throws NoSuchAlgorithmException {
		Random rand = new Random();
		String rndm = Integer.toString(rand.nextInt())
				+ (System.currentTimeMillis() / 1000L);
		String txnid = hashCal("SHA-256", rndm).substring(0, 20);
		return txnid;
	}

	public String hashCal(String type, String str)
			throws NoSuchAlgorithmException {
		byte[] hashseq = str.getBytes();
		StringBuffer hexString = new StringBuffer();
		MessageDigest algorithm = MessageDigest.getInstance(type);
		algorithm.reset();
		algorithm.update(hashseq);
		byte messageDigest[] = algorithm.digest();

		for (int i = 0; i < messageDigest.length; i++) {
			String hex = Integer.toHexString(0xFF & messageDigest[i]);
			if (hex.length() == 1)
				hexString.append("0");
			hexString.append(hex);
		}

		return hexString.toString();

	}

	public PaymentDetails populatePaymentDetails(HttpServletRequest request) {

		PaymentDetails paymentDetails = new PaymentDetails();

		paymentDetails.setTransactionId(request
				.getParameter(IPaymentConstants.PARAM_TRANSACTION_ID));
		paymentDetails.setPayuMihpayId(request
				.getParameter(IPaymentConstants.PARAM_PAYU_ID));
		paymentDetails.setEmail(request
				.getParameter(IPaymentConstants.PARAM_EMAIL));
		paymentDetails.setFirstName(request
				.getParameter(IPaymentConstants.PARAM_FIRST_NAME));
		paymentDetails.setLastName(request
				.getParameter(IPaymentConstants.PARAM_LAST_NAME));
		paymentDetails.setPhone(request
				.getParameter(IPaymentConstants.PARAM_PHONE));
		paymentDetails.setPaymentMode(request
				.getParameter(IPaymentConstants.PARAM_MODE));
		paymentDetails.setStatus(request
				.getParameter(IPaymentConstants.PARAM_STATUS));
		paymentDetails.setAmount(request
				.getParameter(IPaymentConstants.PARAM_AMOUNT) != null ? Float
				.parseFloat(request
						.getParameter(IPaymentConstants.PARAM_AMOUNT)) : 0);
		paymentDetails.setDiscount(request
				.getParameter(IPaymentConstants.PARAM_DISCOUNT) != null ? Float
				.parseFloat(request
						.getParameter(IPaymentConstants.PARAM_DISCOUNT)) : 0);
		paymentDetails
				.setNetAmountDebited(request
						.getParameter(IPaymentConstants.PARAM_NET_AMOUNT) != null ? Float.parseFloat(request
						.getParameter(IPaymentConstants.PARAM_NET_AMOUNT))
						: 0);
		paymentDetails.setPayuStatus(request
				.getParameter(IPaymentConstants.PARAM_PAYU_STATUS));
		paymentDetails.setPGType(request
				.getParameter(IPaymentConstants.PARAM_PG_TYPE));
		paymentDetails.setBankRefNum(request
				.getParameter(IPaymentConstants.PARAM_BANK_REF_NO));
		paymentDetails.setErrorCode(request
				.getParameter(IPaymentConstants.PARAM_ERR_CODE));
		paymentDetails.setErrorMessage(request
				.getParameter(IPaymentConstants.PARAM_ERR_DESC));
		paymentDetails.setTxnDate(request
				.getParameter(IPaymentConstants.PARAM_DATE_ADDED));

		return paymentDetails;

	}
}

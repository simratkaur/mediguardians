package com.medicine.arvene.util;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.medicine.arvene.model.Booking;
import com.medicine.arvene.model.Event;
import com.medicine.arvene.model.Slot;

@Component
public class SlotUtil {

	public static List<Event> createEventsForBookings(List<Booking> bookings) {
		List<Event> events = new ArrayList<Event>();
		for (Booking booking : bookings) {
			Event event = new Event();
			event.setId(booking.getSlot().getId());
			event.setBookingid(booking.getId());
			event.setStart(CommonUtil.formatEventTime(booking.getSlot()
					.getStartdate()));
			event.setEnd(CommonUtil.formatEventTime(booking.getSlot()
					.getEnddate()));
			event.setTitle("Booked");
			event.setTextColor("white");
			event.setBackgroundColor("blue");
			// Changes from user name -- By Madhu
			event.setOwner(booking.getSlot().getUser().getUsername());
			events.add(event);
		}
		return events;
	}

	public static List<Event> createEventsForSlots(List<Slot> slots) {
		List<Event> events = new ArrayList<Event>();
		for (Slot slot : slots) {
			Event event = new Event();
			event.setId(slot.getId());
			event.setStart(CommonUtil.formatEventTime(slot.getStartdate()));
			event.setEnd(CommonUtil.formatEventTime(slot.getEnddate()));
			if (slot.getStatus().equals("A")) {
				event.setTitle("Available");
				event.setBackgroundColor("green");
			} else if (slot.getStatus().equals("B")) {
				event.setTitle("Booked");
				event.setBackgroundColor("blue");
			} else {
				event.setTitle("Cancelled");
				event.setBackgroundColor("red");
			}
			event.setTextColor("white");
			events.add(event);
		}
		return events;
	}

}

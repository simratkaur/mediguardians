package com.medicine.arvene.util;

import org.springframework.context.annotation.Configuration;

import com.opentok.OpenTok;
import com.opentok.Session;
import com.opentok.exception.OpenTokException;

@Configuration
public class TokBoxSettings {

	private static OpenTok opentok = null;

	/**
	 * Generates the client token using the session id
	 * 
	 * @return the token for publishers / subscribers
	 * @throws OpenTokException
	 */
	public static String createToken(String sessionId) throws OpenTokException {
		String token = null;
		token = opentok.generateToken(sessionId);
		return token;
	}

	public static Session createVideoSession() throws OpenTokException {
		return opentok != null ? opentok.createSession() : null;
	}

	public static OpenTok getOpentok() {
		return opentok;
	}

	public static void setOpentok(OpenTok opentok) {
		TokBoxSettings.opentok = opentok;
	}

//	private String apikey = "45414682";
	
	private String apikey = "45713262";

//	private String secretkey = "25e7435550903cf5a93815ac5e3dbe6398953481";
	private String secretkey = "abbd56c2aee34c6c3001c32f8af354980ba876fb";

	private Long expiry;

	private String token;

	private String sessionId;

	public void buildOpenTok() {
		if (opentok == null)
			opentok = new OpenTok(Integer.parseInt(apikey), secretkey);
	}

	public String getApikey() {
		return apikey;
	}

	public Long getExpiry() {
		return expiry;
	}

	public String getSecretkey() {
		return secretkey;
	}

	public String getSessionId() {
		return sessionId;
	}

	public String getToken() {
		return token;
	}

	public void setApikey(String api) {
		apikey = api;
	}

	public void setExpiry(Long expiry) {
		this.expiry = expiry;
	}

	public void setSecretkey(String secret) {
		secretkey = secret;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public void setToken(String token) {
		this.token = token;
	}

}

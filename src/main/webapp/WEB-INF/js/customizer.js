$(document).ready(
		function() {
			var a = window.location.href.substr(window.location.href
					.lastIndexOf("/") + 1);
			"" == a ? $("#index").addClass("active") : $(
					"#header #medi_guardian_menu ul li a").each(
					function() {
						$(this).attr("href") != a
								&& "" != $(this).attr("href")
								|| (null == this.closest(".dropdown") ? $(
										this.parentNode).addClass("active")
										: $(this).closest(".dropdown")
												.addClass("active"))
					}), jQuery(".tp-banner").revolution({
				delay : 5e3,
				startwidth : 1900,
				startheight : 900,
				hideThumbs : 0,
				onHoverStop : "off",
				hideTimerBar : "on",
				navigationType : "none"
			}), $("#medi_guardian_menu").bootstrapDropdownOnHover({
				mouseOutDelay : 500,
				responsiveThreshold : 992,
				hideBackdrop : !0
			}), $("#travel_accomodation_slider").owlCarousel({
				autoPlay : !1,
				items : 3,
				itemsDesktop : [ 1199, 3 ],
				itemsDesktopSmall : [ 979, 3 ],
				itemsTabletSmall : [ 992, 2 ],
				navigation : !0
			}), $("#testimonial_slider").owlCarousel({
				autoPlay : !1,
				items : 1,
				itemsDesktop : [ 1199, 1 ],
				itemsDesktopSmall : [ 979, 1 ],
				itemsTabletSmall : [ 768, 1 ]
			});
		}), $(".tabbed-accordion .tabs").find("ul li a").on(
		"click",
		function(a) {
			var b = $(this).attr("href");
			$(".contentarea").find(".visible").removeClass("visible").addClass(
					"hidden"), $(b).addClass("visible").removeClass("hidden"),
					$(this).parents(".tabbed-accordion ul").find(".active")
							.removeClass("active"), $(this).parent("li")
							.addClass("active"), a.preventDefault()
		}), $(document).on(
		"click",
		".panel-heading a",
		function() {
			$(".mg-accordion .panel-heading").removeClass("panel-active"), $(
					this).parents(".panel-heading").addClass("panel-active")
		}), $(document).on(
		"click",
		".mg-accordion .collapse",
		function() {
			$(this).parent().find(".icon-chevron-right").removeClass(
					"icon-chevron-right").addClass("icon-chevron-down")
		}).on(
		"hidden.bs.collapse",
		function() {
			$(this).parent().find(".icon-chevron-down").removeClass(
					"icon-chevron-down").addClass("icon-chevron-right")
		}), $(".header").on(
		"click",
		".yamm-content .row .col-md-15 ul li a",
		function(a) {
			$("#" + $(this).attr("href").replace(/^.*?(#|$)/, "") + "_link")
					.click()
		}), $(window).load(function() {
	$(".preloader").fadeOut("slow"), setTimeout(function() {
		var a = window.location.hash;
		"" != a && $(a + "_link").click()
	}, 10)
}), $(window).scroll(
		function() {
			$(this).scrollTop() > 1 ? $(".header").addClass("sticky-nav") : $(
					".header").removeClass("sticky-nav")
		});
$(document)
		.ready(
				function() {
					generateCaptcha();
					$('.reset').click(function() {
						generateCaptcha();
					});
					$('#enquiry_form')
							.formValidation(
									{
										framework : 'bootstrap',
										fields : {
											name : {
												validators : {
													notEmpty : {
														message : 'The Name is required'
													}
												}
											},
											email : {
												validators : {
													notEmpty : {
														message : 'The email address is required'
													},
													emailAddress : {
														message : 'The input is not a valid email address'
													}
												}
											},
											phone : {
												validators : {
													notEmpty : {
														message : 'The phone number is required'
													},
													regexp : {
														message : 'The phone number can only contain the digits, spaces, -, (, ), + and .',
														regexp : /^[0-9\s\-()+\.]+$/
													}
												}
											},
											country : {
												validators : {
													notEmpty : {
														message : 'The country is required'
													}
												}
											},
											message : {
												validators : {
													notEmpty : {
														message : 'The message is required'
													}
												}
											},
											captcha : {
												validators : {
													callback : {
														message : 'Wrong answer',
														callback : function(
																value,
																validator,
																$field) {
															var items = $(
																	'#captchaOperation')
																	.html()
																	.split(' '), sum = parseInt(items[0])
																	+ parseInt(items[2]);
															return value == sum;
														}
													}
												}
											}
										}
									}).on('err.form.fv', function(e) {
								generateCaptcha();
							});
				});
$(document)
		.ready(
				function() {
					$('#user_sign_up_form')
							.formValidation(
									{
										framework : 'bootstrap',
										excluded : ':disabled',
										icon : {
											valid : 'glyphicon glyphicon-ok',
											validating : 'glyphicon glyphicon-refresh'
										},
										fields : {
											username : {
												validators : {
													notEmpty : {
														message : 'The username is required'
													},
													emailAddress : {
														message : 'The input is not a valid email address'
													}
												}
											},
											password : {
												validators : {
													notEmpty : {
														message : 'The password is required'
													}
												}
											},
											agree : {
												validators : {
													notEmpty : {
														message : 'Please agree to terms and conditions'
													}
												}
											}
										}
									});
					$('#loginForm')
							.formValidation(
									{
										framework : 'bootstrap',
										excluded : ':disabled',
										icon : {
											valid : 'glyphicon glyphicon-ok',
											invalid : 'glyphicon glyphicon-remove',
											validating : 'glyphicon glyphicon-refresh'
										},
										fields : {
											username : {
												validators : {
													notEmpty : {
														message : 'The username is required'
													},
													emailAddress : {
														message : 'The input is not a valid email address'
													}
												}
											},
											password : {
												validators : {
													notEmpty : {
														message : 'The password is required'
													}
												}
											},
											agree : {
												validators : {
													notEmpty : {
														message : 'Please agree to terms and conditions'
													}
												}
											}
										}
									});
					$('#docRegisterForm')
							.formValidation(
									{
										framework : 'bootstrap',
										icon : {
											invalid : 'glyphicon glyphicon-remove',
											validating : 'glyphicon glyphicon-refresh'
										},
										fields : {
											firstName : {
												validators : {
													notEmpty : {
														message : 'First name is required'
													}
												}
											},
											lastName : {
												validators : {
													notEmpty : {
														message : 'Last name is required'
													},
													regexp : {
														regexp : /^[a-z]+$/i,
														message : 'Last name should be alphabetical characters  only'
													}
												}
											},
											confirmPassword : {
												validators : {
													identical : {
														field : 'password',
														message : 'The password and  confirm password are not the same'
													}
												}
											},
											password : {
												validators : {
													notEmpty : {
														message : 'The password is required'
													}
												}
											},
											username : {
												validators : {
													notEmpty : {
														message : 'The username is required'
													},
													emailAddress : {
														message : 'The username should be a valid email address'
													}
												}
											},
											degrees : {
												validators : {
													notEmpty : {
														message : 'Please choose a degree'
													}
												}
											},
											specialityid : {
												validators : {
													notEmpty : {
														message : 'Please choose a Specialty'
													}
												}
											},
											areaOfInterest : {
												validators : {
													notEmpty : {
														message : 'Area of Interest is Required'
													}
												}
											},
											hospital : {
												validators : {
													notEmpty : {
														message : 'Hospital/Clinic is Required'
													}
												}
											},
											phone1 : {
												validators : {
													notEmpty : {
														message : 'The phone number is required'
													},
													stringLength : {
														message : 'The phone number should be of 10 digits',
														maxl : 10
													},
													regexp : {
														message : 'The phone number should contain only the digits',
														regexp : /^[0-9]+$/
													}
												}
											},
											regno : {
												validators : {
													notEmpty : {
														message : 'Medical Registration Number is required'
													}
												}
											},
											sessionFee : {
												validators : {
													notEmpty : {
														message : 'The video consult fee for each session  is required'
													},
													greaterThan : {
														value : 0,
														inclusive : false,
														message : 'The video consult fee for each session  is required'
													}
												}
											},
											queryFee : {
												validators : {
													notEmpty : {
														message : 'The text consult fee for each session  is required'
													},
													greaterThan : {
														value : 0,
														inclusive : false,
														message : 'The text consult fee for each session  is required'
													}
												}
											},
											agree : {
												validators : {
													notEmpty : {
														message : 'Please agree to terms and conditions'
													}
												}
											}
										}
									});
					$('#forgotPwdForm')
							.formValidation(
									{
										framework : 'bootstrap',
										icon : {
											valid : 'glyphicon glyphicon-ok',
											invalid : 'glyphicon glyphicon-remove',
											validating : 'glyphicon glyphicon-refresh'
										},
										fields : {
											username : {
												validators : {
													notEmpty : {
														message : 'The username is required'
													},
													emailAddress : {
														message : 'The input is not a valid email address'
													}
												}
											}
										}
									});
					$('#queryForm')
							.formValidation(
									{
										framework : 'bootstrap',
										icon : {
											invalid : 'glyphicon glyphicon-remove',
											validating : 'glyphicon glyphicon-refresh'
										},
										fields : {
											patientName : {
												validators : {
													notEmpty : {
														message : 'The Patient name is required'
													}
												}
											},
											patientAge : {
												validators : {
													notEmpty : {
														message : 'The Patient Age is required'
													}
												}
											},
											patientGender : {
												validators : {
													notEmpty : {
														message : 'The Patient Gender is required'
													}
												}
											},
											newConversation : {
												validators : {
													notEmpty : {
														message : 'Please enter your query.'
													},
													stringLength : {
														message : 'The query should be of maximum 2000 characters',
														max : 2000
													}
												}
											}
										}
									});
					$('#reqbookingform')
							.formValidation(
									{
										framework : 'bootstrap',
										icon : {
											invalid : 'glyphicon glyphicon-remove',
											validating : 'glyphicon glyphicon-refresh'
										},
										fields : {
											patientName : {
												validators : {
													notEmpty : {
														message : 'The Patient name is required'
													}
												}
											},
											title : {
												validators : {
													notEmpty : {
														message : 'Title is required'
													},
													stringLength : {
														message : 'The Title should be of maximum 20 characters',
														max : 20
													}
												}
											},
											desc : {
												validators : {
													notEmpty : {
														message : 'Description is required'
													},
													stringLength : {
														message : 'The description should be of maximum 2000 characters',
														max : 2000
													}
												}
											},
											phone1 : {
												validators : {
													notEmpty : {
														message : 'The phone number is required'
													},
													stringLength : {
														message : 'The phone number should be of 10 digits',
														maxl : 10
													},
													regexp : {
														message : 'The phone number should contain only the digits',
														regexp : /^[0-9]+$/
													}
												}
											}
										}
									});
					$('#bookingform')
							.formValidation(
									{
										framework : 'bootstrap',
										icon : {
											invalid : 'glyphicon glyphicon-remove',
											validating : 'glyphicon glyphicon-refresh'
										},
										fields : {
											patientName : {
												validators : {
													notEmpty : {
														message : 'The Patient name is required'
													}
												}
											},
											title : {
												validators : {
													notEmpty : {
														message : 'Title is required'
													},
													stringLength : {
														message : 'The Title should be of maximum 20 characters',
														max : 20
													}
												}
											},
											desc : {
												validators : {
													notEmpty : {
														message : 'Description is required'
													},
													stringLength : {
														message : 'The description should be of maximum 2000 characters',
														max : 2000
													}
												}
											},
											phone1 : {
												validators : {
													notEmpty : {
														message : 'The phone number is required'
													},
													stringLength : {
														message : 'The phone number should be of 10 digits',
														maxl : 10
													},
													regexp : {
														message : 'The phone number should contain only the digits',
														regexp : /^[0-9]+$/
													}
												}
											}
										}
									});
					$('#queryReplyForm')
							.formValidation(
									{
										framework : 'bootstrap',
										icon : {
											invalid : 'glyphicon glyphicon-remove',
											validating : 'glyphicon glyphicon-refresh'
										},
										fields : {
											newConversation : {
												validators : {
													notEmpty : {
														message : 'Please enter your query.'
													},
													stringLength : {
														message : 'The query should be of maximum 2000 characters',
														max : 2000
													}
												}
											}
										}
									});
					$('#queryEditForm')
							.formValidation(
									{
										framework : 'bootstrap',
										icon : {
											invalid : 'glyphicon glyphicon-remove',
											validating : 'glyphicon glyphicon-refresh'
										},
										fields : {
											last_Conversation : {
												validators : {
													notEmpty : {
														message : 'Please enter your query.'
													},
													stringLength : {
														message : 'The query should be of maximum 2000 characters',
														max : 2000
													}
												}
											}
										}
									});
					$('#updateBioForm1')
							.formValidation(
									{
										framework : 'bootstrap',
										icon : {
											invalid : 'glyphicon glyphicon-remove',
											validating : 'glyphicon glyphicon-refresh'
										},
										fields : {
											biography : {
												validators : {
													notEmpty : {
														message : 'Please enter your biography.'
													},
													stringLength : {
														message : 'The biography should be of maximum 500 characters',
														max : 500
													}
												}
											}
										}
									});
				});
function randomNumber(min, max) {
	return Math.floor(Math.random() * (max - min + 1) + min);
}
function generateCaptcha() {
	$('#captchaOperation').html(
			[ randomNumber(1, 9), '+', randomNumber(1, 9), '=' ].join(' '));
}
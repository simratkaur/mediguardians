<%@page import="com.medicine.arvene.util.CommonUtil"%>
<%@page import="java.util.Iterator"%>
<%@ include file="newHeader.jsp"%></section>
<link rel="stylesheet" href="${contextPath}/css/form.css">
<div class="container">
	<div class="mainContent">
		<h2 class="pageHeader"
			style="margin: auto; font-size: 31.5px; font-weight: bold">HTTP
			Status 403 - Access is denied</h2>
		<div class="row">
			<div class="col-md-12">

				<c:choose>
					<c:when test="${empty username}">
						<h3 style="font-weight: normal;">You do not have permission
							to access this page!</h3>
					</c:when>
					<c:otherwise>
						<h3 style="font-weight: normal;">
							Username : ${username} <br />You do not have permission to
							access this page! Please <a href="${contextPath}/">Login</a>
						</h3>
					</c:otherwise>
				</c:choose>
			</div>
		</div>
	</div>
</div>
<%@ include file="newFooter.jsp"%>
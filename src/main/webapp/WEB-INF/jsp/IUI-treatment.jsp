<%@ include file="newHeader.jsp"%></section>
	<!-- Hospital Short Details Starts Here -->
	<div class="short-details">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="title">
						<h1>Infertility Treatment</h1>
						<ul class="paginator">
							<li><a href="/">Home</a></li>
							<li><a>Infertility Treatment</a></li>
						</ul>
					</div>
				</div>
				<!-- End Column -->
			</div>
			<!-- End Row -->
		</div>
		<!-- End Container -->
	</div>
	<!-- Hospital Short Details Ends Here -->

	<!-- Specialty Content Starts Here -->
	<div class="speciality-content-area">
		<div class="container">

			<div class="innertab-accordion">
				<div class="row">
					<div class="col-md-3 tabnopaddright">
						<div class="tabs">
							<ul>
								<li><a href="infertility-treatment">Infertility
										Treatment</a></li>
								<li><a href="IVF-treatment">IVF</a></li>
								<li class="tab-active"><a href="IUI-treatment">IUI</a></li>
							</ul>
						</div>
					</div>
					<!-- End Column -->
					<div class="col-md-9 tabnopaddleft">
						<div class="accordion-contentarea">

							<div id="tab3" >
								<h2>
									<span>IUI: Intrauterine Insemination: Artificial
										Insemination for Infertility Treatment</span> � India
								</h2>
								<div class="row">
									<div class="col-md-9 col-sm-9 nopaddleft">
								<p>We at Arvene Healthcare have the expertise to organize for all treatments and surgeries related to Infertility. Arvene Healthcare is associated with top hospitals and Doctors of India in this field. We can give you more and better options so that you can take the right decisions. We also assist you in taking best decision based on your requirement. Arvene Healthcare will also arrange for your stay and local travel, so that you have nothing else to worry about but your treatment. Please contact us on <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a> today to avail best services.</p>
								
								<h3>IUI:</h3>
								<p>Intrauterine insemination IUI, also called artificial insemination is a basic step for the fertility treatments. The treatment can be done using partner's (husband) sperm or sperm from a donor.This low tech process can be done in two ways: natural and with ovulation induction. </p>
								<h3>
									<span>Who should consider</span> IUI?
								</h3></div>
									<!-- End Inner Column -->
									<div class="col-md-3 col-sm-3 nopaddright">
										<img src="images/specialities/45.IUI copy.jpg" alt="IUI"
											class="img-responsive">
									</div>
									<!-- End Inner Column -->
								</div>
								<!-- End Inner Row -->
								<p>The men who are not able to ejaculate in the vagina. Following are the causes of this failure</p>
								<ul>
									<li><span>Spinal cord injury</span></li>
									<li><span>Diabetes</span></li>
									<li><span>Multiple sclerosis</span></li>
									<li><span>Retrograde ejaculation</span></li>
									<li><span>Low sperm count or poor quality sperm</span></li>
								</ul>
								<h3>
									<span>Unexplained </span>infertility
								</h3>
								<p>
									<strong>Requirements in Males- </strong> IUI will be ineffective where the male has low sperm count or poor sperm shape. For this, sperm tests should be done.
								</p>
								<p>
									<strong>Requirements in Females- </strong> There should be no fertility problems. Tests should give proof of normal ovulation, open fallopian tubes, and a normal uterine cavity.
								</p>
								<p>A woman with severely damaged or blocked fallopian tubes will not be helped by IUI.</p>
								<h3>
									<span>IUI</span> Procedure
								</h3>
								<p>It is done by threading a very thin flexible catheter through the cervix and injecting washed sperm straight into the uterus, taking just a few minutes.</p>
								<h3>
									<span>When is </span>sperm collected for IUI ?
								</h3>
								<p>The sperm is usually collected through ejaculation into a sterile collection cup or in collection condoms. Most of clinics want the semen within a half hour of ejaculation, so if one lives close then the sperms are collected at home but if not, one has to go to the clinic and do it in a private setting. After this washing is done and the sperms are used for IUI.</p>
								<h3>
									<span>IUI</span> Procedure
								</h3>
								<ul>
									<li><span>Drug treatment is done for encouraging two or three eggs to mature.</span></li>
									<li><span>The treatment is monitored for measuring the growth of follicles, individualize drug doses, and to prevent any side effects. When two or three follicles reach the required size, hormone hCG is injected to induce ovulation.</span></li>
									<li><span>Sperm sample (collected in the ovulation morning, washed and concentrated) are inseminated later that day. Using a catheter, the doctor inserts the sperm through the cervix and into the uterus of the female partner.</span></li>
									<li><span>Tests of potential pregnancy and early ultrasounds are done.</span></li>
								</ul>
								<h3>
									<span>Benefits, Success rate and Support for </span>IUI
									Treatment
								</h3>
								<p>The success rates IUI falls between 5 -15 percent per cycle, on the woman's age (up to 35 years), sperm count of the male partner and the female's tubes that should be healthy. Generally three cycles of IUI are tried, if they fail then advanced methods are recommended.</p><br>
								<p>IUI is a simple and inexpensive form of therapy, thus it should be tried first, then going on to more expensive options. In comparison to other countries, India offers treatment at affordable rates.</p><br>
								<p>We can arrange for Free of cost opinions from various super speciality hospitals before doctor's consultations from the expert Doctors of our connected hospitals.</p>
IUI treatment <h5>Cost in India starts at 2000 US Dollars. To know more about it please share your reports on <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a> or whatsapp/viber/imo to +918130077375.</h5><p> 
(Please note this is a tentative plan and actual Costs are based on reports shared. Final treatment plan and estimation will be provided after the patient has been evaluated.)</p>
								<h5>
									To get best options for Infertility treatment, send us a query
									or write to us at <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
								</h5>
							</div>
							<!-- End Tab -->

						</div>
					</div>
					<!-- End Column -->
				</div>
				<!-- End Row -->
			</div>
			<!-- End Tab Accordion -->

		</div>
		<!-- End Container -->
	</div>
	<!-- Speciality Content Ends Here -->

	<%@ include file="newFooter.jsp"%>
<%@ include file="newHeader.jsp"%></section>
	<!-- Hospital Short Details Starts Here -->
	<div class="short-details">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="title">
						<h1>Infertility Treatment</h1>
						<ul class="paginator">
							<li><a href="/">Home</a></li>
							<li><a>Infertility Treatment</a></li>
						</ul>
					</div>
				</div>
				<!-- End Column -->
			</div>
			<!-- End Row -->
		</div>
		<!-- End Container -->
	</div>
	<!-- Hospital Short Details Ends Here -->

	<!-- Specialty Content Starts Here -->
	<div class="speciality-content-area">
		<div class="container">

			<div class="innertab-accordion">
				<div class="row">
					<div class="col-md-3 tabnopaddright">
						<div class="tabs">
							<ul>
								<li><a href="infertility-treatment">Infertility
										Treatment</a></li>
								<li class="tab-active"><a href="IVF-treatment">IVF</a></li>
								<li><a href="IUI-treatment">IUI</a></li>
							</ul>
						</div>
					</div>
					<!-- End Column -->
					<div class="col-md-9 tabnopaddleft">
						<div class="accordion-contentarea">
							<div id="tab2" >
								<h2>
									<span>IVF Treatment (In-Vitro Fertilization) </span>� India
								</h2><div class="row">
									<div class="col-md-9 col-sm-9 nopaddleft">
								<p>We at Arvene Healthcare have the expertise to organize for all treatments and surgeries related to Infertility. Arvene Healthcare is associated with top hospitals and Doctors of India in this field. We can give you more and better options so that you can take the right decisions. We also assist you in taking best decision based on your requirement. Arvene Healthcare will also arrange for your stay and local travel, so that you have nothing else to worry about but your treatment. Please contact us on <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a> today to avail best services.</p>
								</div>
									<!-- End Inner Column -->
									<div class="col-md-3 col-sm-3 nopaddright">
										<img src="images/specialities/44.IVF.jpg" alt="IVF"
											class="img-responsive">
									</div>
									<!-- End Inner Column -->
								</div>
								<!-- End Inner Row -->
								<h3>
									IVF:
								</h3>
								<p>One way to get medical assistance for infertility treatment is called the In Vitro fertilization or IVF, widely known as test tube babyThis involves fertilizing an ovum in a laboratory dish, after which the embryo is transferred into a woman's uterus.Infertility is no more a curse to couples with the development of latest technologies and medical treatments. There are various forms of infertility treatments in India by which women could conceive a child. It is most widely used process all around the globe.</p><br>
								<p>In Vitro Fertilization is a form of Artificial Reproductive Technology where medical techniques are undertaken for embryo transfer or plantations.</p>
								<h3>
									<span>Who should consider </span>IVF treatment?
								</h3>
								<p>According to statistics, couples under the age of 35, who are continuously trying to conceive after unprotected intercourse for more than a year or six months but there have been no results are perfect candidates for IVF. Though there is no age limit for the process, but many institutions may consider your age.</p>
								<h3>
									<span>The infertility disorders</span> that can be treated with
									IVF are:
								</h3>
								<ul>
									<li><span>Sperm-related problems or low sperm count</span></li>
									<li><span>Ovulation disorders</span></li>
									<li><span>Blocked or damaged fallopian tubes</span></li>
									<li><span>Endometriosis</span></li>
									<li><span>Treatment Procedure</span></li>
								</ul>
								<p>
									<strong> Following are five steps in IVF and embryo
										transfer: </strong>
								</p>
								<ul>
									<li><span>A. Observing the development of ripening
											egg(s) in the ovaries </span></li>
									<li><span>B. Collecting the eggs</span></li>
									<li><span>C. Getting the sperm</span></li>
									<li><span>D. Keeping eggs and sperm together in the lab while giving them appropriate conditions for fertilization and initial growth of the embryo</span></li>
									<li><span>E. Transfer of the embryos into the
											uterus</span></li>
								</ul>
								<h3>
									<span>Intracytoplasmic sperm injection </span>ICSI
								</h3>
								<p>ICSI is an ART used to treat sperm-related infertility problems. It is used for the enhancement of the fertilization phase of IVF by injecting a single sperm into the matured egg. This fertilized egg is then transferred into the uterus or fallopian tube of the woman.</p>
								<h3>
									<span>Benefits, Success rate and </span>Support
								</h3>
								<p>For the whole IVF process to become a success, it is extremely important that one achieves success at every step. To gain the best possible benefit it is necessary that the infertile couple gets the right treatment at the right time (age).</p><br>
								<p>A major benefit of IVF is that it enables an infertile couple to have a baby. Second is that it reduces the requirements of the surgery of the fallopian tubes.</p>
IVF treatment <h5>Cost in India starts at 3500 US Dollars. To know more about it please share your reports on <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a> or whatsapp/viber/imo to +918130077375.</h5><p> 
(Please note this is a tentative plan and actual Costs are based on reports shared. Final treatment plan and estimation will be provided after the patient has been evaluated.)</p>
								<h5>
									To get best options for Infertility treatment, send us a query
									or write to us at <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
								</h5>
							</div>
							<!-- End Tab -->

						</div>
					</div>
					<!-- End Column -->
				</div>
				<!-- End Row -->
			</div>
			<!-- End Tab Accordion -->

		</div>
		<!-- End Container -->
	</div>
	<!-- Speciality Content Ends Here -->

	<%@ include file="newFooter.jsp"%>
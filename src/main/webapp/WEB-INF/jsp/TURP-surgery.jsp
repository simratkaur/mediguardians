<%@ include file="newHeader.jsp"%></section>
	<!-- Hospital Short Details Starts Here -->
	<div class="short-details">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="title">
						<h1>Urology Treatment</h1>
						<ul class="paginator">
							<li><a href="/">Home</a></li>
							<li><a>Urology Treatment</a></li>
						</ul>
					</div>
				</div>
				<!-- End Column -->
			</div>
			<!-- End Row -->
		</div>
		<!-- End Container -->
	</div>
	<!-- Hospital Short Details Ends Here -->

	<!-- Specialty Content Starts Here -->
	<div class="speciality-content-area">
		<div class="container">

			<div class="innertab-accordion">
				<div class="row">
					<div class="col-md-3 tabnopaddright">
						<div class="tabs">
							<ul>
								<li><a href="urology-treatment">Urology
										Treatment</a></li>
								<li><a href="endoscopic-surgery">Endoscopy surgery</a></li>
								<li class="tab-active"><a href="TURP-surgery">TURF</a></li>
								<li><a href="radical-prostatectomy-surgery">Prostactomy</a></li>
							</ul>
						</div>
					</div>
					<!-- End Column -->
					<div class="col-md-9 tabnopaddleft">
						<div class="accordion-contentarea">


							<div id="tab3" >
								<h2>
									<span>Transurethral resection of the prostate (TURP) </span>� India
								</h2>
								<div class="row">
									<div class="col-md-9 col-sm-9 nopaddleft">
										<p>We at Arvene Healthcare have the expertise to organize for all treatments and surgeries related to Urology. Arvene Healthcare is associated with top hospitals and Doctors of India in this field. We can give you more and better options so that you can take the right decisions. We also assist you in taking best decision based on your requirement. Arvene Healthcare will also arrange for your stay and local travel, so that you have nothing else to worry about but your treatment. Please contact us on <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a> today to avail best services.</p>
										<h3>
											Transurethral resection of the prostate (TURP):
										</h3>
										<p>An endoscopic procedure for removing benign prostate enlargement (BPH) is known as transurethral resection of the prostate (TURP). This procedure is performed under general anaesthesia </p>
										<h3>
											<span>Types of</span> TURPs
										</h3>
									</div>
									<!-- End Inner Column -->
									<div class="col-md-3 col-sm-3 nopaddright">
										<img src="images/specialities/51. Laser TURP surgery.jpg"
											alt="Laser TURP surgery" class="img-responsive">
									</div>
									<!-- End Inner Column -->
								</div>
								<!-- End Inner Row -->
								<ul>
									<li><span><strong> Laser TURP: </strong> In this, an insertion of a scope is done through the urinary channel up into the prostate. The removal of the obstructing prostatic tissue is done with the help of laser.</span></li>
									<li><span><strong> Traditional TURP: </strong> In this, an insertion of a scope is done through the urinary channel up into the prostate. The removal of obstructing prostatic tissue is done with the help of an electric cutting instrument.</span></li>
								</ul>

								<p>These two procedures provide about 80% improvement in
									urinary symptoms.</p>
								<h3>
									<span>Symptoms of</span> Benign Prostate Hyperplasia (BPH)
								</h3>
								<ul>
									<li><span>Frequent urination</span></li>
									<li><span>Difficulty in starting urination</span></li>
									<li><span>Difficulty in postponing urination</span></li>
									<li><span>Waking up frequently in night to urinate</span></li>
									<li><span>A weak urinary system</span></li>
									<li><span>Sensation of incomplete bladder emptying</span></li>
									<li><span>Interruption of the stream that is stopping and starting effectt</span></li>
								</ul>
								<p>The condition of BPH is diagnosed by reviewing medical history and physical history of a patient. A digital rectal examination is also performed.</p>
								<h3>
									<span>TURP </span>Procedure
								</h3>
								<p>The surgical procedure is performed under spinal or general anaesthesia and takes about 1 hour to complete. A loop instrument known as resectoscope is passed from the urethra to scratch away the enlarged prostate gland. Piece by piece prostate gland is removed for re-establishing a channel and also for clearing away the bladder obstruction. After the surgery a catheter is also inserted for regular irrigation so as to prevent forming of the blood clot in the bladder.</p>
								<h3>
									<span>Recovery after </span>TURP Procedure
								</h3>
								<p>A patient is required to stay in the hospital for two days. Pain medication or antibiotics may be required that largely depends on specific situation. Post-surgery, when the wound will start to heal then there will be few clots or blood in the urine. The urine can become red when the irrigation in the bladder is stopped.</p>
								<h5>
									To get best options for Urology treatment, send us a query or
									write to us at <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
								</h5>
							</div>
							<!-- End Tab -->

						</div>
					</div>
					<!-- End Column -->
				</div>
				<!-- End Row -->
			</div>
			<!-- End Tab Accordion -->

		</div>
		<!-- End Container -->
	</div>
	<!-- Speciality Content Ends Here -->

<%@ include file="newFooter.jsp"%>
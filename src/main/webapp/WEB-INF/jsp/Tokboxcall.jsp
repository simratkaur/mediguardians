<%@ include file="newHeader.jsp"%></section>
<script src="//static.opentok.com/webrtc/v2.2/js/TB.min.js"></script>
<script type="text/javascript">
	var apiKey = '${apikey}';
	var sessionId = '${sessionid}';
	var token = '${token}';
</script>
<script>
	var session = OT.initSession(apiKey, sessionId);
	session.on({
		streamCreated : function(event) {
			session.subscribe(event.stream, 'subscribersDiv', {
				insertMode : 'append',
				width : "100%",
				height : "100%"
			});
		}
	});
	session.connect(token, function(error) {
		var sessionName = "${userType eq S? 'Doctor' : 'Patient'}";
		if (error) {
			console.log(error.message);
		} else {
			session.publish('myPublisherDiv', {
				name : sessionName,
				width : "20%",
				height : "20%",
				testNetwork :true
			});
		}
	});
	function disconnect() {
		session.disconnect();
	}
	function goBack() {
		disconnect();
		window.history.back();
	}
</script>
<div class="container">
	<div class="mainContent">
		<h3 class="pageHeader" style="margin: auto;">Video Chat</h3>
		<div class="row">
		
			<div class="endSessionDiv"><button class="button-custom" onclick="goBack()" style="float:right">End Session</button></div>
			
			<div class="col-md-8" class="subscriberDiv" style="padding-top: 30px">
				<div id='subscribersDiv' class="subscribersDiv"
					style="min-height: 500px; overflow: hidden; width: 100%; height: 240px; float: left"></div>
				<div id="myPublisherDiv" class="publisherDiv"
					style="margin-top:-150px; min-height: 150px; min-width: 200px; float: left;" /></div>							
			</div>
		</div>
		
	</div>
</div>
<%@ include file="newFooter.jsp"%>
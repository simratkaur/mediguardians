<%@ include file="newHeader.jsp"%></section>
<script type="text/javascript">
	$(document).ready(
			function() {

				var vquery_status = ${query_status}
				$("#query_status").val(vquery_status);

				var specialityArray = ${usersMap};

				/* var ss= ${search_username}
				console.log(); */

				$(function() {
					$("#username").autocomplete(
							{
								source : function(request, response) {
									var matcher = new RegExp($.ui.autocomplete
											.escapeRegex(request.term), "i");
									response($.map(specialityArray, function(
											value, key) {
										if (value
												&& (!request.term || matcher
														.test(value))) {
											return {
												label : value,
												value : key
											};
										}
									}));
								},
								select : function(event, ui) {
									$("#username").val(ui.item.label), $(
											"#user_id").val(ui.item.value);//ui.item is your object from the array
									return false;
								},
								minLength : 2
							})
				});

				var specialistArray = ${specialistMap};
				console.log(specialistArray);
				$(function() {
					$("#specialistname").autocomplete(
							{
								source : function(request, response) {
									var matcher = new RegExp($.ui.autocomplete
											.escapeRegex(request.term), "i");
									response($.map(specialistArray, function(
											value, key) {
										if (value
												&& (!request.term || matcher
														.test(value))) {
											return {
												label : value,
												value : key
											};
										}
									}));
								},
								select : function(event, ui) {
									$("#specialistname").val(ui.item.label), $(
											"#specialist_id")
											.val(ui.item.value);//ui.item is your object from the array
									return false;
								},
								minLength : 2
							})
				});

				$(function() {
					$("#specialistnameQuery").autocomplete(
							{
								source : function(request, response) {
									var matcher = new RegExp($.ui.autocomplete
											.escapeRegex(request.term), "i");
									response($.map(specialistArray, function(
											value, key) {
										if (value
												&& (!request.term || matcher
														.test(value))) {
											return {
												label : value,
												value : key
											};
										}
									}));
								},
								select : function(event, ui) {
									$("#specialistnameQuery")
											.val(ui.item.label), $(
											"#specialistQueryid").val(
											ui.item.value);//ui.item is your object from the array
									return false;
								},
								minLength : 2
							})
				});

			});
</script>


<section class="sec-main sec-drl">
	<div class="container">
		<div class="drl-bgwhite conn-bttn">
			<div class="row">
				<div class="col-lg-6">
					<button class="acc">Queries</button>
				</div>
				<div class="col-lg-6">
					<%-- <a href="queriesHistory" class="acc pull-right col-lin"> <c:choose>
							<c:when test="${userType == 'S'}">View Replied Queries</c:when>
							<c:otherwise>Queries History</c:otherwise>
						</c:choose>

					</a> --%>
				</div>
			</div>
		</div>
		<c:choose>
			<c:when test="${fn:length(queries) == 0}">
				<h4 class="list-group-item-text">You have no queries as of now.</h4>
			</c:when>
			<c:otherwise>

				<div class="drl-bgwhite uqt-main">
					<div class="table-responsive">
						<table class="table table-condensed  table-striped "
							id="queryTable">

							<thead class="uqt-tab-head">
								<tr>
									<th width="3%">Sr.No</th>
									<th width="19%">Username</th>
									<th width="15%">Patient Details</th>
									<th width="15%">Doctor Name</th>
									<th width="35%" class="w-th">Query</th>
									<th width="5%">Query Date</th>
									<th width="5%">Status</th>
									<th width="3%">Action</th>
								</tr>
							</thead>
							<tbody>

								<tr>
									<form:form class="center" action='newQueries' method="POST"
										id="searchUserForm" name="searchUserForm">
										<input type="hidden" name="user" id="user_id" />
										<input type="hidden" name="start" id="start" />
										<input type="hidden" name="specialist" id="specialist_id" />

										<td></td>
										<td><input class="form-control" type="text"
											name="username" placeholder="Search Username" id="username" />
										</td>
										<td>&nbsp;</td>
										<td><input class="form-control" type="text"
											name="specialistname" placeholder="Search Specialist"
											id="specialistname" /></td>
										<td></td>
										<td></td>
										<td><select name="responded" id="query_status">
												<option value="-1" selected>All</option>
												<option value="-2">Unassigned</option>
												<option value="1">Responded</option>
												<option value="0">Not Responded</option>
										</select></td>
										<td><span class="fa fa-lg fa-search"
											onclick="$('#searchUserForm').submit();"></span></td>
									</form:form>
								</tr>


								<c:forEach items="${queries}" var="query" varStatus="queryIndex">
									<tr>
										<td>${start + queryIndex.index +1 }</td>
										<td>${query.user.username }</td>

										<td>Patient Name - ${query.patientName} <br /> Age-
											${query.patientAge}<br /> Gender - ${query.patientGender}<br />
											Phone - ${query.patientPhone}
										</td>
										<td><c:choose>
												<c:when test="${query.specialistDetails ne null}">
													${query.specialistDetails.firstName}&nbsp;${query.specialistDetails.lastName }
												</c:when>
												<c:otherwise>

													<form:form class="center" action='assignQuery'
														method="POST" id="assignQueryForm" name="assignQueryForm">
														<input type="hidden" name="start" id="start" />
														<input type="hidden" name="query" id="query"
															value="${query.id}" />
														<input type="hidden" name="specialist"
															id="specialistQueryid" />
														<input class="form-control" type="text"
															name="specialistnameQuery"
															placeholder="Search Specialist" id="specialistnameQuery" />
														<button type="submit">Assign</button>
													</form:form>



												</c:otherwise>
											</c:choose></td>

										<td><c:forEach items="${query.conversations}"
												var="conversation" varStatus="conIndex1">
												<c:if test="${conIndex1.index eq 0 }">	${conversation.text}</c:if>
											</c:forEach>

											<div class=" collapse" id="col-${ query.id}">

												<c:forEach items="${query.conversations}" var="conversation"
													varStatus="conIndex">

													<c:choose>
														<c:when test="${conversation.type eq 'S' }">
															<h4>Doctor's response:</h4>
														</c:when>
													</c:choose>
													<div>${conversation.text}</div>
													<c:forEach items="${conversation.reports}" var="report"
														varStatus="reportIndex">
														<div>
															<a href="${report.url}" class="col-lin">${report.fileName}</a>
														</div>
													</c:forEach>
													<hr />
												</c:forEach>

											</div></td>
										<td>${query.createdAt }</td>
										<td><c:choose>
												<c:when test="${query.specialistDetails ne null}">
													<c:choose>
														<c:when test="${query.responded eq '0'}">
														Response Awaited.
													</c:when>
														<c:otherwise>
														Responded!
													</c:otherwise>
													</c:choose>
												</c:when>
												<c:otherwise>
														Unassigned.
												</c:otherwise>

											</c:choose></td>
										<td><span class="fa fa-eye marg-uqt-span"
											data-toggle="collapse" data-target="#col-${query.id}"></span>
											<form id="uploadFile" action="uploadFiles" method="POST" enctype="multipart/form-data" > 
											<label> <span
												class="fa fa-upload marg-uqt-span js-uploader"></span>
												<input
												type="file" style="display: none;" name="files"
												id="newProfileImageBook" multiple onchange="javascript:this.form.submit();"
												data-validation-max-size="2M">
												<input type="hidden"  name="id" value ="${query.id}"/>
												<input type="hidden"  name="type" value ="query"/>
										</label></form></td>
									</tr>
								</c:forEach>

							</tbody>



						</table>



					</div>

				</div>

			</c:otherwise>
		</c:choose>
		<div class="row">
			<div class="col-lg-12">
				<div class="drl-btn">
					<div id="pagination"></div>
				</div>
			</div>
		</div>
	</div>
</section>

<script type='text/javascript'>
	var options = {

		currentPage : '${currentpage}',
		totalPages : '${pageno}',
		onPageClicked : function(e, originalEvent, type, page) {
			fetchNewQueries(page)
		},
		size : 'medium',
		alignment : 'center'
	};
	$('#pagination').bootstrapPaginator(options);

	function fetchNewQueries(page) {
		var start = (page * 10) - 10;
		/* window.location.href="newQueries?start="+start+"&range=10"; */
		$("#start").val(start);
		$("#searchUserForm").submit();
	}
	$('.nav >li.active').removeClass("active");
	$('.nav #queries').addClass("active");
</script>

<%@ include file="newFooter.jsp"%>
<%@ include file="newHeader.jsp"%></section>
<!-- Banner Starts Here -->
<div class="inner-banner">
	<img src="images/banners/sub-banner.jpg" alt="About us"
		class="img-responsive">
</div>
<!-- Banner Ends Here -->

<!-- Short Details Starts Here -->
<div class="short-details">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="title">
					<h1>About Us</h1>
					<ul class="paginator">
						<li><a href="/">Home</a></li>
						<li><a>About Us</a></li>
					</ul>
				</div>
			</div>
			<!-- End Column -->
		</div>
		<!-- End Row -->
	</div>
	<!-- End Container -->
</div>
<!-- Short Details Ends Here -->

<!-- Content Section Starts Here -->
<div class="content-area">
	<div class="container">

		<div class="main-contentarea">
			<p>While considering the medical treatment facilities, the name
				of India mounts to the top as one of the best seats for healthcare.
				Medical tourism in India includes health care treatments like
				Oncology, Cosmetic Surgery, Cardiology, Organ Transplant and IVF.
				Medical treatment in India is done in the most modern way by the
				highly acclaimed medical professionals. Because of India's
				multi-dimensional and comparatively low cost healthcare benefits,
				people from all around the world consider coming to India for their
				medical treatments</p>
			<h3 class="title-margin">Arvene Healthcare</h3>
			<p>Arvene Healthcare is an online initiative to connect to the
				patients globally and to offer them the best healthcare benefits in
				some of the most promising destinations in India. Partnering with
				some of the celebrated names in healthcare and medical treatment
				regime, Arvene Healthcare is upholding the benchmark of trusted medical
				tourism in India. The efficient and dedicated group of professionals
				is the core strength of Arvene Healthcare. A deep understanding of the
				nuances of medical tourism in India has earned Arvene Healthcare a
				trusted name when it comes to facilitating healthcare services and
				cosmetic surgeries of international standard.</p>
			<h3 class="title-margin">
				<span>Why</span> Arvene Healthcare
			</h3>
			<ul>
				<li>An interactive portal with easy to connect options</li>
				<li>Complete assistance from the medical and travel
					professionals</li>
				<li>Arvene Healthcare is associated with the world renowned
					Hospitals that are recognized by the Joint Commission International
					(JCI) of the USA.</li>
				<li>Personalized packages tailored to the requirements of the
					individual patients</li>
				<li>Pre and post treatments arrangement of accommodation for
					the patients and family members</li>
				<li>A dedicated team to offer round the clock support for
					handling all aspects of travel and medical treatment in India</li>
				<li>Covering a wide range of therapeutic treatments</li>
				<li>Treating the patients with humane feel</li>
			</ul>
			<h3 class="title-margin">
				<span>What Arvene Healthcare</span> Offers:
			</h3>
			<ul>
				<li>A dedicated team of in-house professionals to attend to the
					queries of the sensitive patients</li>
				<li>Multiple options from various hospitals are offered to a
					single query sent by our clients; this gives them the freedom to
					evaluate the different options properly and chose the most suitable
					option that serves their purposes the best</li>
				<li>As per the query of the patients, the corresponding
					responses from the doctor are sent to the clients along with the
					doctor's credentials.</li>
				<li>Prior to the arrival of the patients, we arrange for the
					doctor and the patient communication via phone or e-mail. This
					ensures a complete transparency of the process and also creates a
					comfort zone for the patient to travel</li>
				<li>We arrange visa assistance letter from the hospital and the
					doctor to procure Indian visa.</li>
				<li>We receive the patients from the airport upon their
					arrival.</li>
				<li>For complete assistance, we visit the patients periodically
					in the hospital during their stay and also communicate with them on
					a regular basis</li>
				<li>During the patient's stay in India, we communicate with
					their near and dear ones/doctors back home periodically via phone
					or email regarding patient's medical progress</li>
				<li>For the follow up visits of the patients, we also arrange
					for post-operative hotel stay near the hospital</li>
				<li>On patient's wish, we also tailor post-operative holiday in
					India after proper consultation with the doctor</li>
				<li>The return airport transfers for the patients are also
					arranged by us</li>
				<li>We also look after that the post-operative queries that
					patients send after returning back home are duly responded by the
					respective doctors who are treating them.</li>
			</ul>
			<h3 class="title-margin">
				<span>We Keep the</span> Right Company
			</h3>
			<p>The high level of customer satisfaction is our only reward and
				we are truly looking forward to enrich our commitment to promoting
				and facilitating medical travel in India with many such rewards.
				Association with the trusted names in the healthcare industry like
				Apollo Group of Hospitals, Max Hospitals in India, Fortis Group of
				Hospitals, Institute of Brain and Spine, Medanta Hospital Delhi,
				enables Arvene Healthcare India to keep its promise of providing the
				best treatments to the patients in the most cost effective manner.</p>
			<h3 class="title-margin">
				<span>A Look</span> Forward
			</h3>
			<p>"Growth cannot be the outcome of one individual's efforts, but
				of a potent team endeavour". Arvene Healthcare firmly believes in this
				saying and look forward to tie-ups with likeminded travel &amp; tour
				companies and individuals around the world. Indian Ministry of
				Tourism has identified countries like CIS Countries, USA, UK,
				Europe, Middle East, South Africa, The Indian Subcontinent,
				Australia and South America as the most potent countries to
				contribute to the niche of medical tourism in India.</p>
		</div>
		<!-- End Accordion Container -->
	</div>
	<!-- End Container -->
</div>
<!-- Content Section Ends Here -->

<%@ include file="newFooter.jsp"%>
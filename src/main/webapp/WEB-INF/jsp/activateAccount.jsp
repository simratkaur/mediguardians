<%@ include file="newHeader.jsp"%></section>
<%-- <link rel="stylesheet" href="${contextPath}/css/form.css"> --%>
<script type="text/javascript">
	window.onload = function() {
		$("#cssmenu").hide();
		$("#userName").hide();
	}
	function Submit() {
		if (isRequired("OTP")) {
			if ($("#OTP").val().length != 6) {
				$("#errorBox").html("OTP should be of 6 characters");
				popUp();
				return false;
			} else {
				$(".custError").text("");
				return true;
			}
		} else {
			return false;
		}
	}
	function sendOtp() {
		$.ajax({
			type : "POST",
			url : "/resendOtp",
			data : {
				username : "${user.username}",
				"${_csrf.parameterName}" : "${_csrf.token}"
			},
			success : function(data) {
				var arr = data.split(",");
				if (arr[1].indexOf("Success") > -1) {
					$("#errorBox").html("Otp Sent Successfully");
					popUp();
					$(".custError").css("color", "black");
				} else {
					$("#errorBox").html(arr[1]);
					popUp();
				}
			}
		});
		return false;
	}
</script>
<div class="container">
	<div class="mainContent">
		<h3 class="pageHeader" style="margin: auto;">Please Enter OTP for
			Account Activation</h3>
		<div class="row">
			<div class="col-md-12">
				<form:form name='tokenVerification' action='tokenVerification'
					method='POST' modelAttribute="user" class="has-validation-callback">
					<fieldset class="registration-form">

						<div class="control-group">
							<div class="controls">
								<span style="display: inline-block; width: 125px;">User
									Name:</span>
								<form:input path="username" name="username" type="text"
									data-validation="email"
									data-validation-error-msg="Please enter a valid email address." />
							</div>
						</div>

						<div class="control-group">
							<div class="controls">
								<span style="display: inline-block; width: 125px;">OTP:</span>
								<form:input path="OTP" type='text' id="OTP" maxlength="6"/>
								<input type="submit" id="resendOTP" name="resentOTP"
									value="Resend OTP" onclick="return sendOtp()"
									class="btn btn-primary"
									style="width: auto; border-radius: 10px; font-size: 14px; padding: 5px 10px; margin: -4px 0 0 5px;">
							</div>
						</div>

						<div class="control-group center">
							<div class="controls">
								<input name="Activate" type="submit" value="Activate"
									class="btn btn-primary" onclick=" return Submit()" />
							</div>
						</div>

						<div class="control-group center">
							<div class="controls">
								<a href="login">Login</a>
							</div>
						</div>
						<input type="hidden" name="${_csrf.parameterName}"
							value="${_csrf.token}" />
						<form:input path="id" type="hidden" />
					</fieldset>
				</form:form>
			</div>
		</div>
	</div>
</div>
<script>
</script>
<%@ include file="newFooter.jsp"%>
<%@ include file="newHeader.jsp"%></section>

<script type="text/javascript">
	$(document).ready(
			function() {
				
				var vquery_status = ${query_status}
				$("#query_status").val(vquery_status);

				var specialityArray = ${usersMap};
			
				/* var ss= ${search_username}
				console.log(); */
				
				$(function() {
					$("#username").autocomplete(
							{
								source : function(request, response) {
									var matcher = new RegExp($.ui.autocomplete
											.escapeRegex(request.term), "i");
									response($.map(specialityArray, function(
											value, key) {
										if (value
												&& (!request.term || matcher
														.test(value))) {
											return {
												label : value,
												value : key
											};
										}
									}));
								},
								select : function(event, ui) {
									$("#username").val(ui.item.label), $("#user_id")
											.val(ui.item.value);//ui.item is your object from the array
									return false;
								},
								minLength : 2
							})
				});
				
				
				
				var specialistArray = ${specialistMap};
				console.log(specialistArray);
				
				$(function() {
					$("#specialistname").autocomplete(
							{
								source : function(request, response) {
									var matcher = new RegExp($.ui.autocomplete
											.escapeRegex(request.term), "i");
									response($.map(specialistArray, function(
											value, key) {
										if (value
												&& (!request.term || matcher
														.test(value))) {
											return {
												label : value,
												value : key
											};
										}
									}));
								},
								select : function(event, ui) {
									$("#specialistname").val(ui.item.label),
									$("#specialist_id")
											.val(ui.item.value);//ui.item is your object from the array
									return false;
								},
								minLength : 2
							})
				});
			});
	
</script>
<div class="container">
	<div class="mainContent">
		<c:choose>
			<c:when test="${userType == 'U'}">
				<c:set var="type" value="Appointments"></c:set>
				<h3 class="pageHeader" style="margin: auto;">Appointments</h3>
			</c:when>
			<c:when test="${userType == 'S'}">
				<c:set var="type" value="Bookings"></c:set>
				<h3 class="pageHeader" style="margin: auto;">Bookings</h3>
			</c:when>
			<c:when test="${userType == 'A'}">
				<c:set var="type" value="Bookings"></c:set>
				<h3 class="pageHeader" style="margin: auto;">Bookings</h3>
			</c:when>
		</c:choose>

		<br>

		<!-- 		<div style="float:right"><a href="bookingsHistory">View Bookings History</a> </div>
 -->
		<c:choose>
			<c:when test="${fn:length(bookings) == 0}">
				<h4 class="list-group-item-text">There are no ${type} as of
					now.</h4>
			</c:when>
			<c:otherwise>
				<table class="table table-striped rwd-table">
					<thead>
						<tr>
							<th style="width: 5%;">S.NO</th>
							<th style="width: 20%;">Patient Name</th>
							<th style="width: 20%;">Doctor name</th>
							<th style="width: 30%;">${type}&nbsp;Details</th>
							<th style="width: 10%;">Session Time</th>
							<th style="width: 10%;">Status</th>
							<th style="width: 5%;">Action</th>
						</tr>
					</thead>
					<tbody>
						<tr> <form:form class="center" action='adminBookings' method="POST"
										id="adminBookingsForm" name="adminBookingsForm">
										<input type="hidden" name="user" id="user_id"/>
										<input type="hidden" name="start" id="start" />
										<input type="hidden" name="specialist" id="specialist_id"/>

										<td></td>
										<td><input class="form-control" type="text" name="username" placeholder="Search Username" id="username"
													/> </td>
										<td><input class="form-control"  type="text" name="specialistname"
											placeholder="Search Specialist" id="specialistname"  /> 
											</td>
										<td></td>
										<td></td>
										<td><select name="status" id="status" >
												<option value= "" selected>All</option>
												<option value= "R" >Requested</option>
											</select>
										</td>
										<td><a onclick="$('#adminBookingsForm').submit();"> <i class="fa fa-lg fa-search" ></i></a> </td>
									</form:form>
									
						</tr>
						
					
						<c:forEach items="${bookings}" var="booking"
							varStatus="bookingIndex">
							<tr>
								<td>${param.start +bookingIndex.index +1 }</td>
								<td>${booking.subscriber.username}</td>
								<td>${booking.specialistDetails.title}
										${booking.specialistDetails.firstName}&nbsp;
										${booking.specialistDetails.lastName}</td>
								<td><b> ${booking.title}</b>
									<div id="col-${booking.id}"  class="collapse">
									 ${booking.desc}<br /> <c:forEach
										items="${booking.reports}" var="report"
										varStatus="reportIndex">
										<div>
											<a href="${report.url}" class="col-lin">${report.fileName}</a>
										</div>
									</c:forEach>
									</div>
								</td>
								<td>
								<c:choose>
										<c:when test="${booking.slot.startdate eq null || booking.slot.startdate eq ''}">
												<a href="viewcalendar?uid=${booking.specialistDetails.user.id}&bookingId=${booking.id}">Update</a>
										</c:when>
										<c:otherwise>${booking.slot.startdate}</c:otherwise>
									</c:choose>
								
								
								
								<td>	
								
									<c:choose>
										<c:when test="${booking.slot.status eq 'R'}">
														Requested
										</c:when>
									</c:choose>
								</td>
								<td><span class="fa fa-eye marg-uqt-span"
											data-toggle="collapse" data-target="#col-${booking.id}"></span><form id="uploadFile" action="uploadFiles" method="POST" enctype="multipart/form-data" > 
											<label> <span
												class="fa fa-upload marg-uqt-span js-uploader"></span>
												<input
												type="file" style="display: none;" name="files"
												id="newProfileImageBook" multiple onchange="javascript:this.form.submit();"
												data-validation-max-size="2M">
												<input type="hidden"  name="id" value ="${booking.id}"/>
												<input type="hidden"  name="type" value ="booking"/>
										</label></form></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</c:otherwise>
		</c:choose>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="drl-btn">
				<div id="pagination"></div>
			</div>
		</div>
	</div>
</div>






<%-- <div style="width:970px;">
   		   <c:if test="${fn:length(bookings) == 0}">
		   		  <h4 class="list-group-item-text">You have no ${type} as of now</h4>
			</c:if>
		   <div class="list-group">
		   <c:forEach items="${bookings}" var="booking">
		 	 <div class="list-group-item">
		 	  <c:choose>
                    <c:when test="${userType == 'U'}">
		   		 <h4 class="list-group-item-heading">${booking.slot.user.username}</h4>
		   		  <p class="list-group-item-text">Specialized in ${booking.slot.user.speciality.name}</p>
		   		  <p class="list-group-item-text">Booking Title :  ${booking.title}</p>
		   		   <a class="pull-right" href="joinsession?id=${booking.id}" class="btn btn-success btn-large btn-block" role="button">Start session</a>
		   		  <p class="list-group-item-text">Booking Desc :  ${booking.desc}</p>
		   		  <p class="list-group-item-text">Session starts at  ${booking.slot.startdate}</p>
		   		 
		   		 </c:when>
		   		  <c:when test="${userType == 'S'}">
		   		 <h4 class="list-group-item-heading">${booking.subscriber.name}</h4>
		   		  <p class="list-group-item-text">Booking Title :  ${booking.title}</p>
		   		   <a class="pull-right" href="joinsession?id=${booking.id}" class="btn btn-success btn-large btn-block" role="button">Start session</a>
		   		  <p class="list-group-item-text">Booking Desc :  ${booking.desc}</p>
		   		  <p class="list-group-item-text">Session starts at  ${booking.slot.startdate}</p>
		   		 </c:when>
		   		 </c:choose>	
		  	</div>
		  	</c:forEach>
			</div>
			
			</div>
			<div id="pagination"></div>
	</div> --%>
<script type='text/javascript'>
	var options = {
		currentPage : '${currentpage}',
		totalPages : '${pageno}',
		onPageClicked : function(e, originalEvent, type, page) {
			fetchbooking(page)
		},
		size : 'medium',
		alignment : 'center'

	};

	$('#pagination').bootstrapPaginator(options);

	function fetchbooking(page) {
		var start = (page * 10) - 10;
		/* window.location.href = "adminBookings?start=" + start + "&range=10"; */
		$("#start").val(start);
		$("#adminBookingsForm").submit();
	}
	$('.nav >li.active').removeClass("active");
	$('.nav #bookings').addClass("active");
</script>
<!-- <script type="text/javascript">
 <c:if test="${not empty error}">


		 BootstrapDialog.show({
               type:  BootstrapDialog.TYPE_DANGER,
               title: '${errortitle}',
               message: '${error}',
               buttons: [{
                   label: 'OK',
                   action: function(dialog) {
                       dialog.close();
                   }
               }]
           });    


</c:if>
<c:if test="${not empty msg}">

		 BootstrapDialog.show({
               type:  BootstrapDialog.TYPE_SUCCESS,
               title: '${msgtitle}',
               message: '${msg}',
               buttons: [{
                   label: 'OK',
                   action: function(dialog) {
                       dialog.close();
                   }
               }]
           });    
	

</c:if>
</script> -->

<%@ include file="newFooter.jsp"%>


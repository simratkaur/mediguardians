<%@ include file="newHeader.jsp"%></section>
<div class="container">
	<div class="mainContentBigger">
		<h3 class="pageHeader" style="margin: auto;">Contact Us Inquires</h3>

		<c:choose>
			<c:when test="${fn:length(inquiries) == 0}">
				<h4 class="list-group-item-text">No Inquiries as of now.</h4>
			</c:when>
			<c:otherwise>
				<table class="table table-striped rwd-table">
					<thead>
						<tr>
							<th style="width: 5%;">S.NO</th>
							<th style="width: 15%;">Email</th>
							<th style="width: 15%;">Name</th>
							<th style="width: 45%;">Inquiry</th>
							<th style="width: 15%">Status</th>
							<th style="width: 5%">Action</th>

						</tr>

					</thead>
					<tbody>
						<c:forEach items="${inquiries}" var="inquiry"
							varStatus="inquiryIndex">
							<tr>
								<td>${start + inquiryIndex.index +1 }</td>
								<td>${inquiry.email }</td>
								<td>${inquiry.name }</td>
								<td>
									<div id="shortInquiry-${inquiry.id }" style="display: block;">
										${fn:substring((inquiry.message), 0,60)}
										<c:if test="${fn:length(inquiry.message)>60}">...</c:if>
									</div>


									<div id="InquiryReply-${inquiry.id }" style="display: none;">
										<div>${inquiry.message }</div>
										<c:choose>
											<c:when
												test="${(not empty inquiry.response)  || (fn:length(inquiry.response)>0) }">
												<div>
													<b>Response:<br></b><i>${inquiry.response }</i>
												</div>
											</c:when>
											<c:otherwise>
												<form:form id="InquiryForm" action="inquiryReply"
													method="POST">
													<input type="hidden" name="id" id="id"
														value="${inquiry.id}">
													<input type="hidden" name="${_csrf.parameterName}"
														value="${_csrf.token}" />
													<textarea maxlength="1000" placeholder="Respond to Inquiry"
														name="response" rows="4" id="response" path="response"
														class="text ui-widget-content ui-corner-all response"
														style="width: 90%;"></textarea>
													<div>
														<input class="button-custom" type="submit" value="Reply">
														<input class="blue-btn" type="button" value="Cancel"
															onclick="viewInquiry(${inquiry.id})">
													</div>
												</form:form>
											</c:otherwise>
										</c:choose>




									</div>


								</td>
								<td><c:choose>
										<c:when
											test="${(not empty inquiry.response)  || (fn:length(inquiry.response)>0) }">
											<div>
												Responded<br>
											</div>
										</c:when>
										<c:otherwise>
											<div>
												Not Responded<br>
											</div>
										</c:otherwise>
									</c:choose></td>
								<td><input id="toggleInquiryDetails" type="button"
									onclick="viewInquiry(${inquiry.id})" style="width: 125px"
									class="btn-blue square-Box button-custom " value="View" /> <input
									id="toggleInquiryDetails" type="button" style="width: 125px"
									onclick="location.href='archiveInquiry?id=${inquiry.id}'"
									class="btn-blue square-Box button-custom " value="Archive" /></td>

							</tr>
						</c:forEach>
					</tbody>
				</table>
			</c:otherwise>
		</c:choose>

	</div>
	<div id="pagination"></div>
</div>
<script type='text/javascript'>
var options = {
		
		currentPage : '${currentpage}',
		totalPages : '${pageno}',
		onPageClicked : function(e, originalEvent, type, page) {
			fetchInquiries(page)
		},
		size:'medium',
		alignment : 'center'
	};
	$('#pagination').bootstrapPaginator(options);

	function fetchInquiries(page) {
		var start = (page * 10) - 10;
		window.location.href="inquiries?start="+start+"&range=10";
	}
	$('.nav >li.active').removeClass("active");
	$('.nav #queries').addClass("active");

</script>
<script>
  		 function viewInquiry(inquiryId){
  			var shortInquiryDiv = document.getElementById("shortInquiry-"+inquiryId);
  			var inquiryReplyDiv = document.getElementById("InquiryReply-"+inquiryId);
  				
  			 if(shortInquiryDiv.style.display == 'block'){
  				shortInquiryDiv.style.display = 'none';
  			 }
             else{
            	 shortInquiryDiv.style.display = 'block';
             }
  			 
  			 if(inquiryReplyDiv.style.display == 'block'){
  				inquiryReplyDiv.style.display = 'none';
   			 }
              else{
            	  inquiryReplyDiv.style.display = 'block';
              }
  			
  		}  	
  </script>
<%@ include file="newFooter.jsp"%>
<%@ include file="newHeader.jsp"%></section>
	<!-- Banner Starts Here -->
	<div class="inner-banner">
		<img src="images/banners/air-ambulance.jpg" alt="Air Ambulance"
			class="img-responsive">
	</div>
	<!-- Banner Ends Here -->

<!-- Hospital Short Details Starts Here -->
	<div class="short-details">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="title">
						<h1>Air Ambulance</h1>
						<ul class="paginator">
							<li><a href="/">Home</a></li>
							<li><a>Air Ambulance</a></li>
						</ul>
					</div>
				</div>
				<!-- End Column -->
			</div>
			<!-- End Row -->
		</div>
		<!-- End Container -->
	</div>
	<!-- Short Details Ends Here -->
	
	<!-- Content Section Starts Here -->
	<div class="content-area">
		<div class="container">
			<div class="main-contentarea">
				<p>While traveling from your country to India for Medical
					Treatment and required additional assistance in transportation for
					elderly or sick patients, our partner service providers offers a
					variety of options to transport sick and injured persons quickly,
					safely, and comfortably. With their extensive experience
					transporting clients to and from every continent, except
					Antarctica, they have the knowledge and expertise to plan the most
					direct and comfortable trip while ensuring that your loved one can
					be safely moved and properly taken care of.</p>
				<br>
				<p>Not only that, they also helps in medical clearances before
					every trip to avoid any interruption or delay. Whether your loved
					one's needs are as simple as mobility assistance or as complex as
					oxygen, IVs, and medications, they can put together a trip that
					meets their needs. Their services are often more affordable than
					expected because in most cases, they utilize the commercial airline
					system. Traveling in first or business class allows patients to
					enjoy the privacy, personal attention, and spacious, comfortable
					seating that they need. If their medical condition requires, we can
					also arrange for the patient to be transported on a commercial
					stretcher, with appropriate medical staff and equipment on board.</p>
				<br>
				<p>Throughout the trip, they are escorted by experienced and
					professional medical escort staff, which stay with them from door
					to door and take care of each and every detail. Whether an elderly
					loved one simply needs help traveling to a family wedding in
					another state, wants to return home to India, or needs to visit a
					hospital on the other side of the world, GlobalMed Services can
					help.</p>
				<br>
				<h5>
					To book Air Ambulance from your country to India, send us a query
					or write to us at <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
				</h5>

			</div>
			<!-- End Accordion Container -->
		</div>
		<!-- End Container -->
	</div>
	<!-- Content Section Ends Here -->

	<%@ include file="newFooter.jsp"%>
<%@ include file="newHeader.jsp"%></section>

<section class="sec-main sec-drl">
	<div class="container">
		<div class="row">
			<div class="dashboard-bg col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="dash-hding col-lg-6 col-md-6 col-sm-6 col-xs-6">
					<p>All Doctors</p>
				</div>
				<div class="active-user col-lg-6 col-md-6 col-sm-6 col-xs-6">
					<p>Active User Count:${activeUsers}</p>
				</div>

				<c:forEach items="${specialists}" var="specialist"
					varStatus="loopStatus">


					<div class="dr-info col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<div class="dr-img col-lg-4 col-md-4 col-sm-6 col-xs-6">
								<img src="${specialist.profileImage}"
									alt="${specialist.title}
									${specialist.firstName} ${specialist.lastName}"
									class="img-responsive">
							</div>
							<div class="dr-info-dtl col-lg-8 col-md-8 col-sm-6 col-xs-6">
								<p>${specialist.firstName}${specialist.lastName}</p>
								<p>${specialist.title}</p>
								<p>${specialist.degrees}</p>
								<p>${specialist.speciality.name}</p>
								<p>${specialist.hospital},${specialist.hospitalCity}</p>

							</div>
						</div>
						<div class="view-btn col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<button type="button" class="btn btn-lg btn-default"
								onclick="location.href='docDetails?id=${specialist.user.id}'">View
								Profile</button>
						</div>
					</div>
				</c:forEach>
			</div>

		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="drl-btn">

					<div id="pagination"></div>

				</div>
			</div>
		</div>
	</div>
</section>


<%-- <div class="container">
	<div class="">
		<h3 class="center">All Doctors</h3>

		<c:if test="${fn:length(specialists) == 0}">
			<h4 class="list-group-item-text">There are no doctors available</h4>
		</c:if>

		<div class="drl-bgwhite uqt-main">
			<div class="table-responsive">
				<table class="table table-condensed  table-striped ">
					<thead class="uqt-tab-head">
						<tr>
							<th>Sr.No</th>
							<th>Profile Image</th>
							<th>Doctor Name</th>
							<th>Details</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>	
					<c:forEach items="${specialists}" var="specialist"
				varStatus="loopStatus">
						<tr>
							<td>${start + loopStatus.index +1 }</td>
							<td class="col-sm-2"><div class="drp-img"><img class="drImage" src="${specialist.profileImage}" /></div></td>
							<td><a href="docDetails?id=${specialist.user.id}"> ${specialist.title  == null ?  'Dr.' :specialist.title}
									${specialist.firstName} ${specialist.lastName}</a><br/>
									${specialist.degrees}<br/>
									${specialist.speciality.name}<br/>
									${specialist.hospital},${specialist.hospitalCity}</td>
							<td></td>
							
							<td></td>
						</tr>
					</c:forEach>

					</tbody>
				</table>
				<form action="allSpecialist" id="searchform">

						<input type="hidden" name="start" id="start" />

					</form>
			</div>
		</div>
		

	</div>
</div> --%>



<script type='text/javascript'>
	var options = {
		currentPage : '${currentpage}',
		totalPages : '${pageno}',
		onPageClicked : function(e, originalEvent, type, page) {
			fetchSpecialists(page)
		},
		size : 'medium',
		alignment : 'center'

	};

	$('#pagination').bootstrapPaginator(options);

	function fetchSpecialists(page) {
		var start = (page * 10) - 10;
		window.location.href="allSpecialists?start="+start+"&range=10";
	}

	$('.nav >li.active').removeClass("active");
	$('.nav #specialists').addClass("active");
</script>

<%@ include file="newFooter.jsp"%>

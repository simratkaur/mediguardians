<%@ include file="newHeader.jsp"%></section>
<!-- Banner Starts Here -->
<div class="inner-banner">
	<img src="images/banners/apollo-bangalore.jpg"
		alt="METRO Hospital" class="img-responsive">
</div>
<!-- Banner Ends Here -->

<!-- Hospital Short Details Starts Here -->
<div class="short-details">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<div class="title">
					<h1>Apollo Hospital</h1>
					<p>Bangalore, India</p>
				</div>
			</div>
			<!-- End Column -->
			<!-- <div class="col-md-6 col-sm-6">
                            <div class="site-link">
                                Website : <a href="javascript:;">http://www.aimshospital.org</a>
                            </div>
                        </div>End Column -->
		</div>
		<!-- End Row -->
	</div>
	<!-- End Container -->
</div>
<!-- Hospital Short Details Ends Here -->

<!-- Hospital Details Starts Here -->
<div class="container">
	<div class="detail-container">
		<div class="hospital-details">
			<div class="row">
				<div class="col-md-9">
					<div class="row">
						<div class="col-md-6 col-divider">
							<div class="address-box box-height">
								<ul>
									<!-- <li><strong>Address</strong><span>: Ponekkara, P. O Kochi, Kochi, Kerala</span></li> -->
									<li><strong>City</strong><span>: Bangalore</span></li>
								</ul>
							</div>
						</div>
						<!-- End Column -->
						<div class="col-md-6 col-divider">
							<div class="number-box box-height">
								<ul>
									<li><strong>No. of Hospital Beds</strong><span>: 750</span></li>
									<li><strong>No. of ICU Beds</strong><span>: 180</span></li>
									<li><strong>No. of Operating Rooms</strong><span>: 22
									</span></li>
									<li><strong>Payment Mode</strong><span>: cash, bank transfer</span></li>
									<li><strong>Avg. International Patients</strong><span>: 2000+
									</span></li>
								</ul>
							</div>
						</div>
						<!-- End Column -->
					</div>
					<!-- End Row -->
				</div>
				<!-- End Column -->
				<!-- <div class="col-md-3">
                                <div class="certificate-box box-height">
                                    <h4>Accreditations</h4>
                                    <ul>
                                        <li><img src="images/certificate/icon01.png" alt="Accreditations" class="img-responsive"></li>
                                    </ul>
                                </div>
                            </div>End Column -->
			</div>
			<!-- End Row -->
		</div>
		<!-- End Hospital Details -->
	</div>
	<!-- End Detail Container -->
</div>
<!-- Hospital Details Ends Here -->

<!-- About Section Starts Here -->
<div class="about-section">
	<div class="container">
		<div class="title">
			<h2>
				<span>About</span> Apollo Hospital
			</h2>
		</div>
		<div class="description">
			<p>Apollo Hospital Bangalore is a tertiary care flagship unit of
				the Apollo Hospitals Group. It made a mark in the city of Bangalore
				in 2007 and is committed to provide quality healthcare and
				facilities within the reach of every individual.</p>
			<p>It is a perfect blend of technological excellence, adequate
				infrastructure, compassionate care and is equipped with state-of-the
				art technology. It comprises of an excellent team of more than 100
				consultants, who are experts in major medical specialties having a
				rich clinical background of either having studied or worked in
				reputed institutes of the world.</p>
			<p>Apollo Hospital Bangalore is dedicated to provide the best
				standard of patient care and hence, is JCI (Joint Commission
				International) accredited. The Joint Commission is the gold standard
				accreditation for health care organizations.</p>
			<p>Subsequent to the accreditation by JCI, USA the laboratory
				services have also been accredited by NABL. NABL (National
				Accreditation Board for Testing and Calibration Laboratories)
				provides laboratory accreditation services to laboratories that
				perform tests / calibrations in accordance with criteria based on
				internationally accepted standards and guidelines. In a nut shell,
				Apollo Hospitals, Bangalore is ‘World class healthcare under one
				roof &amp; a global healthcare destination’.</p>
			<h5>
				To get best options for treatment at Apollo Hospital, send us a
				query or write to us at <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
			</h5>

		</div>
	</div>
	<!-- End Container -->
</div>
<!-- About Section Ends Here -->

<%@ include file="newFooter.jsp"%>
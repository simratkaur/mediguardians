<%@ include file="newHeader.jsp"%></section>
<!-- Banner Starts Here -->
<div class="inner-banner">
	<img src="images/banners/apollo-cancer.jpg"
		alt="METRO Hospital" class="img-responsive">
</div>
<!-- Banner Ends Here -->

<!-- Hospital Short Details Starts Here -->
<div class="short-details">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<div class="title">
					<h1>Apollo Speciality Cancer Hospital</h1>
					<p>Chennai, India</p>
				</div>
			</div>
			<!-- End Column -->
			<!-- <div class="col-md-6 col-sm-6">
                            <div class="site-link">
                                Website : <a href="javascript:;">http://www.aimshospital.org</a>
                            </div>
                        </div>End Column -->
		</div>
		<!-- End Row -->
	</div>
	<!-- End Container -->
</div>
<!-- Hospital Short Details Ends Here -->

<!-- Hospital Details Starts Here -->
<div class="container">
	<div class="detail-container">
		<div class="hospital-details">
			<div class="row">
				<div class="col-md-9">
					<div class="row">
						<div class="col-md-6 col-divider">
							<div class="address-box box-height">
								<ul>
									<!-- <li><strong>Address</strong><span>: Ponekkara, P. O Kochi, Kochi, Kerala</span></li> -->
									<li><strong>City</strong><span>: Chennai</span></li>
								</ul>
							</div>
						</div>
						<!-- End Column -->
						<div class="col-md-6 col-divider">
							<div class="number-box box-height">
								<ul>
									<li><strong>No. of Hospital Beds</strong><span>: 260</span></li>
									<li><strong>No. of ICU Beds</strong><span>: 42</span></li>
									<li><strong>No. of Operating Rooms</strong><span>: 6
									</span></li>
									<li><strong>Payment Mode</strong><span>: cash, bank transfer</span></li>
									<li><strong>Avg. International Patients</strong><span>: 2000+
									</span></li>
								</ul>
							</div>
						</div>
						<!-- End Column -->
					</div>
					<!-- End Row -->
				</div>
				<!-- End Column -->
				<!-- <div class="col-md-3">
                                <div class="certificate-box box-height">
                                    <h4>Accreditations</h4>
                                    <ul>
                                        <li><img src="images/certificate/icon01.png" alt="Accreditations" class="img-responsive"></li>
                                    </ul>
                                </div>
                            </div>End Column -->
			</div>
			<!-- End Row -->
		</div>
		<!-- End Hospital Details -->
	</div>
	<!-- End Detail Container -->
</div>
<!-- Hospital Details Ends Here -->

<!-- About Section Starts Here -->
<div class="about-section">
	<div class="container">
		<div class="title">
			<h2>
				<span>About</span> Apollo Hospital
			</h2>
		</div>
		<div class="description">
			<p>Apollo Cancer Institute, India's first ISO certified
				healthcare provider is today ranked among the top super specialty
				hospitals , offering advanced tertiary care in Oncology,
				Orthopedics, Neurology and Neurosurgery, Head and Neck surgery and
				Reconstructive and Plastic surgery.</p>
			<p>Equipped with 300 beds, the latest and the best technology,
				manned by a large pool of world renowned specialists and supported
				by a dedicated team of medical and paramedical professionals, Apollo
				Cancer Institute offers specialty healthcare of international
				standards with outcomes matching those of the world's best
				hospitals.</p>
			<p>The hospital provides 360 degree cancer care. The
				comprehensive treatment planning system involves a Tumour Board
				which consists of a panel of competent medical, surgical and
				radiation oncologists. The Board along with diagnostic consultants
				examines referred cases and jointly decides on the best line of
				treatment for each patient. Medical counsellors, speech therapists,
				dieticians and other professionals, relevant to the case further
				support the panel.</p>
			<h5>MILESTONES</h5>
			<ul>
				<li>First Oncology Hospital in India and First Hospital in
					Chennai city to be accredited by NABH.</li>
				<li>First hospital in South-East Asia to introduce the 64 Slice
					PET-CT Scan.</li>
				<li>First hospital in India to introduce CyberKnife®.</li>
				<li>First hospital in India to have all the latest Radiotherapy
					equipment such as TrueBeam STX .</li>
				<li>First hospital in India to launch Proton Therapy shortly.
			</ul>
			<h5>TECHNOLOGY</h5>
			<ul>
				<li>Full Field Digital Mammography with Tomosynthesis (3D)
					System.</li>
				<li>64 SLICE- PET CT scan system.</li>
				<li>PET –MRI</li>
				<li>Cyberknife</li>
				<li>True Beam STX radiotherapy</li>
				<li>Proton therapy</li>
				<li>Brachytherapy</li>
			</ul>
			<h5>FACILITY</h5>
			<ul>
				<li>300 Beds</li>
				<li>Dedicated Chemotherapy Ward</li>
				<li>Dedicated Stem Cell Transplant Unit</li>
				<li>Platinum Ward Dedicated To Patient Comfort</li>
			</ul>

			<h5>
				To get best options for treatment at Apollo Hospital, send us a
				query or write to us at <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
			</h5>

		</div>
	</div>
	<!-- End Container -->
</div>
<!-- About Section Ends Here -->

<%@ include file="newFooter.jsp"%>
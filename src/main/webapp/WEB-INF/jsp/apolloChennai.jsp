<%@ include file="newHeader.jsp"%></section>
<!-- Banner Starts Here -->
<div class="inner-banner">
	<img src="images/banners/apollo-chennai.jpg"
		alt="METRO Hospital" class="img-responsive">
</div>
<!-- Banner Ends Here -->

<!-- Hospital Short Details Starts Here -->
<div class="short-details">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<div class="title">
					<h1>Apollo Hospital</h1>
					<p>Chennai, India</p>
				</div>
			</div>
			<!-- End Column -->
			<!-- <div class="col-md-6 col-sm-6">
                            <div class="site-link">
                                Website : <a href="javascript:;">http://www.aimshospital.org</a>
                            </div>
                        </div>End Column -->
		</div>
		<!-- End Row -->
	</div>
	<!-- End Container -->
</div>
<!-- Hospital Short Details Ends Here -->

<!-- Hospital Details Starts Here -->
<div class="container">
	<div class="detail-container">
		<div class="hospital-details">
			<div class="row">
				<div class="col-md-9">
					<div class="row">
						<div class="col-md-6 col-divider">
							<div class="address-box box-height">
								<ul>
									<!-- <li><strong>Address</strong><span>: Ponekkara, P. O Kochi, Kochi, Kerala</span></li> -->
									<li><strong>City</strong><span>: Chennai</span></li>
								</ul>
							</div>
						</div>
						<!-- End Column -->
						<div class="col-md-6 col-divider">
							<div class="number-box box-height">
								<ul>
									<li><strong>No. of Hospital Beds</strong><span>: 600</span></li>
									<li><strong>No. of ICU Beds</strong><span>: 150</span></li>
									<li><strong>No. of Operating Rooms</strong><span>: 20
									</span></li>
									<li><strong>Payment Mode</strong><span>: cash, bank transfer</span></li>
									<li><strong>Avg. International Patients</strong><span>: 5000+
									</span></li>
								</ul>
							</div>
						</div>
						<!-- End Column -->
					</div>
					<!-- End Row -->
				</div>
				<!-- End Column -->
				<!-- <div class="col-md-3">
                                <div class="certificate-box box-height">
                                    <h4>Accreditations</h4>
                                    <ul>
                                        <li><img src="images/certificate/icon01.png" alt="Accreditations" class="img-responsive"></li>
                                    </ul>
                                </div>
                            </div>End Column -->
			</div>
			<!-- End Row -->
		</div>
		<!-- End Hospital Details -->
	</div>
	<!-- End Detail Container -->
</div>
<!-- Hospital Details Ends Here -->

<!-- About Section Starts Here -->
<div class="about-section">
	<div class="container">
		<div class="title">
			<h2>
				<span>About</span> Apollo Hospital
			</h2>
		</div>
		<div class="description">
			<p>The flagship hospital of the Apollo Group, Apollo Hospitals,
				Chennai, was established in 1983. Undaunted and unfazed by the
				obstacles faced, Apollo Hospitals has ever since nurtured a goal
				which read as "Our mission is to bring healthcare of international
				standards within the reach of every individual. We are committed to
				the achievement and maintenance of excellence in education, research
				and healthcare for the benefit of humanity".</p>
			<p>In the years since, it has scripted one of the most
				magnificent stories of success that India has seen. Today it is one
				of the most respected hospitals in the world and is also amongst the
				most preferred destinations for patients from several parts of
				India, as well as for medical tourism and medical value travel</p>
			<p>The hospital has created a name for itself in the healthcare
				sector through cutting-edge innovation in medical procedures and
				technological advancements. It has over 60 departments, spearheaded
				by internationally trained and skillful medical experts who are
				supported by dedicated patient care personnel. They have
				state-of-the-art facilities for various health ailments and
				disorders. At Apollo Hospitals, we unite exceptional clinical
				success rates and superior technology with centuries old traditions
				of care and warmth.</p>
			<h5>
				To get best options for treatment at Apollo Hospital, send us a
				query or write to us at <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
			</h5>

		</div>
	</div>
	<!-- End Container -->
</div>
<!-- About Section Ends Here -->

<%@ include file="newFooter.jsp"%>
<%@ include file="newHeader.jsp"%></section>
<!-- Banner Starts Here -->
<div class="inner-banner">
	<img src="images/banners/artemis.jpg"
		alt="METRO Hospital" class="img-responsive">
</div>
<!-- Banner Ends Here -->

<!-- Hospital Short Details Starts Here -->
<div class="short-details">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<div class="title">
					<h1>Artemis Hospital</h1>
					<p>Gurgaon, Delhi NCR, India</p>
				</div>
			</div>
			<!-- End Column -->
			<!-- <div class="col-md-6 col-sm-6">
                            <div class="site-link">
                                Website : <a href="javascript:;">http://www.aimshospital.org</a>
                            </div>
                        </div>End Column -->
		</div>
		<!-- End Row -->
	</div>
	<!-- End Container -->
</div>
<!-- Hospital Short Details Ends Here -->

<!-- Hospital Details Starts Here -->
<div class="container">
	<div class="detail-container">
		<div class="hospital-details">
			<div class="row">
				<div class="col-md-9">
					<div class="row">
						<div class="col-md-6 col-divider">
							<div class="address-box box-height">
								<ul>
									<!-- <li><strong>Address</strong><span>: Ponekkara, P. O Kochi, Kochi, Kerala</span></li> -->
									<li><strong>City</strong><span>: Gurgaon, Delhi NCR</span></li>
								</ul>
							</div>
						</div>
						<!-- End Column -->
						<div class="col-md-6 col-divider">
							<div class="number-box box-height">
								<ul>
									<li><strong>No. of Hospital Beds</strong><span>: 400</span></li>
									<li><strong>No. of ICU Beds</strong><span>: 80</span></li>
									<li><strong>No. of Operating Rooms</strong><span>: 18
									</span></li>
									<li><strong>Payment Mode</strong><span>: cash, bank transfer</span></li>
									<li><strong>Avg. International Patients</strong><span>: 1500+
									</span></li>
								</ul>
							</div>
						</div>
						<!-- End Column -->
					</div>
					<!-- End Row -->
				</div>
				<!-- End Column -->
				<!-- <div class="col-md-3">
                                <div class="certificate-box box-height">
                                    <h4>Accreditations</h4>
                                    <ul>
                                        <li><img src="images/certificate/icon01.png" alt="Accreditations" class="img-responsive"></li>
                                    </ul>
                                </div>
                            </div>End Column -->
			</div>
			<!-- End Row -->
		</div>
		<!-- End Hospital Details -->
	</div>
	<!-- End Detail Container -->
</div>
<!-- Hospital Details Ends Here -->

<!-- About Section Starts Here -->
<div class="about-section">
	<div class="container">
		<div class="title">
			<h2>
				<span>About</span> Artemis Hospital
			</h2>
		</div>
		<div class="description">
			<p>Artemis Hospital, established in 2007, spread across 9 acres,
				is a 380 bed; state-of-the-art multi-speciality hospital located in
				Gurgaon, India. Artemis Hospital is the first JCI and NABH
				accredited hospital in Gurgaon.</p>
			<p>Designed as one of the most advanced in India, Artemis
				provides a depth of expertise in the spectrum of advanced medical &amp;
				surgical interventions, comprehensive mix of inpatient and
				outpatient services. Artemis has put modern technology in the hands
				of renowned from across the country and abroad to set new standards
				in healthcare. The medical practices and procedures followed in the
				hospital are research oriented and benchmarked against the best in
				the world. Top-notch services, in a warm, open centric environment,
				clubbed with affordability, has made us one of the most revered
				hospitals in the country.</p>
			<h5>
				To get best options for treatment at Artemis Hospital, send us a
				query or write to us at <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
			</h5>

		</div>
	</div>
	<!-- End Container -->
</div>
<!-- About Section Ends Here -->

<%@ include file="newFooter.jsp"%>
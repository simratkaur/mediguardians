<%@ include file="newHeader.jsp"%></section>
	
	
	<section class="sec-main">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="top-heading-s">
                            <h4>Sign Up With MediGuardians</h4>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 pos">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="sig-input">
                                    <input class="form-control" type="text" placeholder="Name Of Patient">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="sig-input">
                                    <input class="form-control" type="text" placeholder="Age Of Patient">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="sig-input">
                                    <form>
                                        <span class="sig-label gend">
                                            <label>Gender</label>
                                        </span>
                                        <label class="radio-inline">
                                            <input type="radio" name="optradio"><span>Male</span>
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="optradio"><span>Female</span>
                                        </label>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="sig-input">                                    
                                    <select name="code" id="input" class="mbllcode form-control">
                                        <option value="+91">+91</option>
                                        <option value="+92">+92</option>
                                    </select>
                                    <input class="form-control mbllnmbr" type="text" value="">                                    
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">
                        <div class="text-up">
                            <textarea class="form-control" rows="10" placeholder="Write Query"></textarea>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="sig-btn u-b">
                                    <button class="btn btn-block bg-blue" >Attach query files</button>
                                </div>
                            </div>                            
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="sig-btn more">
                                    <span> *Click to attach more files</span>
                                </div>
                            </div>                            
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <span>You can choose to send the reports at contact@mediguardians.com
                                    or WhatsApp at +91-81300-77375 after submitting your query. </span><br>
                                <span></span>
                            </div>
                        </div>  
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="sig-btn">
                                    <a href="#"><button class="btn btn-block bg-blue" >Submit</button></a>
                                </div>
                            </div>   
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">

                </div>
            </div>
        </section>
		

<script>
	function toggleAttachfile(shareHistory) {

		if (shareHistory.checked) {
			document.getElementById("attachment").style.display = 'none';
		} else {
			document.getElementById("attachment").style.display = 'block';
		}

	}
	$(document).ready(function() {
		$("#shareHistorydiv").hide();
	});
	function enableShareHistory(dropDown) {
		if (dropDown.value < 0) {
			$("#shareHistorydiv").hide();
		} else
			$("#shareHistorydiv").show();
	}
</script>


<%@ include file="newFooter.jsp"%>
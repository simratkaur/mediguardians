<%@ include file="newHeader.jsp"%></section>

<section class="sec-main sec-drl">
	<div class="container">
		<div class="row">
			<div class="dr-avail col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<form:form id="schedule" action="saveavailability"
					modelAttribute="schedule" method="POST">
					<div class="schedule col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<p>Availability Schedule</p>
						<p class="date-picker">
							<form:input class="datepickFuture" placeholder="Start Date"
								type="text" id="startdatepicker" path="startdateStr"
								onchange="checkDays('startdatepicker','enddatepicker')" />
							<label class="calender-icon"><i
								class="fa fa-calendar date" aria-hidden="true"></i></label>
						</p>
						<p class="date-picker">
							<form:input class="datepickFuture" placeholder="End Date"
								type="text" id="enddatepicker" path="enddateStr"
								onchange="checkDays('startdatepicker','enddatepicker')" />
							<label class="calender-icon"><i
								class="fa fa-calendar date" aria-hidden="true"></i></label>

						</p>




					</div>
					<div
						class=" col-lg-12 col-md-12 col-sm-12 col-xs-12 table-responsive">
						<table class="table table-striped dr-avail-tab">
							<thead class="tab-head">
								<tr>
									<th>Sr.No</th>
									<th>Day Name</th>
									<th class="session">Session</th>
									<th>Start Time-End Time</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>1</td>
									<td>Sunday</td>
									<td>
										<div class="session-div">
											<form:input type="text" class="sessions" name="suntimings"
												path="suntimings" id="sun_sessions" data-role="tagsinput" />
										</div>
									</td>
									<td>
										<div class="input-group bootstrap-timepicker timepicker">
											<!--  <input id="timepicker" type="text" class="form-control input-small basicExample"> -->

											<input type="text" id="sun_starttime"
												class="form-control input-small timePicker" /> <label
												class="clock-icon"><i
												class="fa fa-clock-o basicExample" aria-hidden="true"></i></label>
										</div>
										<div class="input-group bootstrap-timepicker timepicker">
											<!--  <input id="timepicker" type="text" class="form-control input-small basicExample"> -->
											<input type="text" id="sun_endtime"
												class="form-control input-small timePicker" /> <label
												class="clock-icon"><i
												class="fa fa-clock-o basicExample" aria-hidden="true"></i></label>
										</div>
									</td>
									<td class="plus-icon"><a
										href="javascript:validateTime('sun_starttime','sun_endtime')"><i
											class="fa fa-plus" aria-hidden="true"></i></a></td>
								</tr>

								<!-- Monday  -->
								<tr>
									<td>2</td>
									<td>Monday</td>
									<td>
										<div class="session-div">
											<form:input type="text" class="sessions" name="montimings"
												path="montimings" id="mon_sessions" data-role="tagsinput" />
										</div>
									</td>
									<td>
										<div class="input-group bootstrap-timepicker timepicker">
											<input type="text" id="mon_starttime"
												class="form-control input-small timePicker" /> <label
												class="clock-icon"><i
												class="fa fa-clock-o basicExample" aria-hidden="true"></i></label>
										</div>
										<div class="input-group bootstrap-timepicker timepicker">
											<input type="text" id="mon_endtime"
												class="form-control input-small timePicker" /> <label
												class="clock-icon"><i
												class="fa fa-clock-o basicExample" aria-hidden="true"></i></label>
										</div>
									</td>
									<td class="plus-icon"><a
										href="javascript:validateTime('mon_starttime','mon_endtime')"><i
											class="fa fa-plus" aria-hidden="true"></i></a></td>
								</tr>

								<!-- Tuesday  -->
								<tr>
									<td>3</td>
									<td>Tuesday</td>
									<td>
										<div class="session-div">
											<form:input type="text" class="sessions" name="tuetimings"
												path="tuetimings" id="tue_sessions" data-role="tagsinput" />
										</div>
									</td>
									<td>
										<div class="input-group bootstrap-timepicker timepicker">
											<input type="text" id="tue_starttime"
												class="form-control input-small timePicker" /> <label
												class="clock-icon"><i
												class="fa fa-clock-o basicExample" aria-hidden="true"></i></label>
										</div>
										<div class="input-group bootstrap-timepicker timepicker">
											<input type="text" id="tue_endtime"
												class="form-control input-small timePicker" /> <label
												class="clock-icon"><i
												class="fa fa-clock-o basicExample" aria-hidden="true"></i></label>
										</div>
									</td>
									<td class="plus-icon"><a
										href="javascript:validateTime('tue_starttime','tue_endtime')"><i
											class="fa fa-plus" aria-hidden="true"></i></a></td>
								</tr>

								<!--Wednesday  -->
								<tr>
									<td>4</td>
									<td>Wednesday</td>
									<td>
										<div class="session-div">
											<form:input type="text" class="sessions"  name="wedtimings"
												path="wedtimings" id="wed_sessions"  data-role="tagsinput" />
										</div>
									</td>
									<td>
										<div class="input-group bootstrap-timepicker timepicker">
											<input type="text" id="wed_starttime"
												class="form-control input-small timePicker" /> <label
												class="clock-icon"><i
												class="fa fa-clock-o basicExample" aria-hidden="true"></i></label>
										</div>
										<div class="input-group bootstrap-timepicker timepicker">
											<input type="text" id="wed_endtime"
												class="form-control input-small timePicker" /> <label
												class="clock-icon"><i
												class="fa fa-clock-o basicExample" aria-hidden="true"></i></label>
										</div>
									</td>
									<td class="plus-icon"><a
										href="javascript:validateTime('wed_starttime','wed_endtime')"><i
											class="fa fa-plus" aria-hidden="true"></i></a></td>
								</tr>

							<!-- Thursday -->
								<tr>
									<td>5</td>
									<td>Thursday</td>
									<td>
										<div class="session-div">
											<form:input type="text" class="sessions" name="thutimings"
													path="thutimings" id="thu_sessions"  data-role="tagsinput" />
										</div>
									</td>
									<td>
										<div class="input-group bootstrap-timepicker timepicker">

											<input type="text" id="thu_starttime"
												class="form-control input-small timePicker" /> <label
												class="clock-icon"><i
												class="fa fa-clock-o basicExample" aria-hidden="true"></i></label>
										</div>
										<div class="input-group bootstrap-timepicker timepicker">
											<!--  <input id="timepicker" type="text" class="form-control input-small basicExample"> -->
											<input type="text" id="thu_endtime"
												class="form-control input-small timePicker" /> <label
												class="clock-icon"><i
												class="fa fa-clock-o basicExample" aria-hidden="true"></i></label>
										</div>
									</td>
									<td class="plus-icon"><a
										href="javascript:validateTime('thu_starttime','thu_endtime')"><i
											class="fa fa-plus" aria-hidden="true"></i></a></td>
								</tr>

								<!-- Friday -->
								<tr>
									<td>6</td>
									<td>Friday</td>
									<td>
										<div class="session-div">
											<form:input type="text" class="sessions" name="fritimings"
												path="fritimings" id="fri_sessions"  data-role="tagsinput" />
										</div>
									</td>
									<td>
										<div class="input-group bootstrap-timepicker timepicker">
											<input type="text" id="fri_starttime"
												class="form-control input-small timePicker" /> <label
												class="clock-icon"><i
												class="fa fa-clock-o basicExample" aria-hidden="true"></i></label>
										</div>
										<div class="input-group bootstrap-timepicker timepicker">
											<input type="text" id="fri_endtime"
												class="form-control input-small timePicker" /> <label
												class="clock-icon"><i
												class="fa fa-clock-o basicExample" aria-hidden="true"></i></label>
										</div>
									</td>
									<td class="plus-icon"><a
										href="javascript:validateTime('fri_starttime','fri_endtime')"><i
											class="fa fa-plus" aria-hidden="true"></i></a></td>
								</tr>

								<!-- Saturday -->
								<tr>
									<td>7</td>
									<td>Saturday</td>
									<td>
										<div class="session-div">
											<form:input type="text" class="sessions" name="sattimings"
												 path="sattimings" id="sat_sessions" data-role="tagsinput" />
										</div>
									</td>
									<td>
										<div class="input-group bootstrap-timepicker timepicker">
											<input type="text" id="sat_starttime"
												class="form-control input-small timePicker" /> <label
												class="clock-icon"><i
												class="fa fa-clock-o basicExample" aria-hidden="true"></i></label>
										</div>
										<div class="input-group bootstrap-timepicker timepicker">
											<!--  <input id="timepicker" type="text" class="form-control input-small basicExample"> -->
											<input type="text" id="sat_endtime"
												class="form-control input-small timePicker" /> <label
												class="clock-icon"><i
												class="fa fa-clock-o basicExample" aria-hidden="true"></i></label>
										</div>
									</td>
									<td class="plus-icon"><a
										href="javascript:validateTime('sat_starttime','sat_endtime')"><i
											class="fa fa-plus" aria-hidden="true"></i></a></td>
								</tr>
							</tbody>
						</table>
						<div class="text-right"><input id="savebtn" type="submit" name="action"
						class="btn btn-primary" value="Save"></div>
					</div>
				</form:form>
			</div>
		</div>
	</div>
</section>





<script type='text/javascript'>
	$('.nav >li.active').removeClass("active");
	$('.nav #availability').addClass("active");
	
	function checkDays(startDateId, endDateId){
		
		var startDate = document.getElementById(startDateId).value;
		var endDate = document.getElementById(endDateId).value;
		var daysArray = [];
		
		var start = moment(startDate,"DD/MM/YYYY");
		var end = moment(endDate,"DD/MM/YYYY");
		var difference  = end.diff(start, "days");
		
		if(difference<6){
			console.log("start days is"+start.day());	
			console.log("end days is"+end.day());	
			
			var startDay = start.day();
			var endDay = end.day();
			var sunday = 0;
			var saturday = 6;
			
			var daytoDisable  = endDay+1;
			
			if(endDay==6){
				daytoDisable = 0;
			}
			
			while(daytoDisable != startDay){
		
				console.log("adding day ::"+daytoDisable)
				daysArray.push(daytoDisable);
				
				if(daytoDisable==6){
					daytoDisable = 0 ;
				}else {
					daytoDisable = daytoDisable+1;
				}
				
				
			}
			
			for (var i = 0; i < daysArray.length; i++) {
				
				var prefix  = "";
				
				if(daysArray[i]==0){
					prefix="sun_";
				}else if(daysArray[i]==1){
					prefix="mon_";
				}else if(daysArray[i]==2){
					prefix="tue_";
				}else if(daysArray[i]==3){
					prefix="wed_";
				}else if(daysArray[i]==4){
					prefix="thu_";
				}else if(daysArray[i]==5){
					prefix="fri_";
				}else if(daysArray[i]==6){
					prefix="sat_";
				}
				
				console.log(prefix);
				var startTimeId = prefix+"starttime";
				var endTimeId = prefix+"endtime";
				
				document.getElementById(startTimeId).disabled="true";
				document.getElementById(endTimeId).disabled="true";
			}
						
			
		}
		
	}
</script>

<%@ include file="newFooter.jsp"%>


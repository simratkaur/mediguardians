<%@ include file="newHeader.jsp"%></section>
<!-- Hospital Short Details Starts Here -->
<div class="short-details">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="title">
					<h1>Bariatric Treatment</h1>
					<ul class="paginator">
						<li><a href="/">Home</a></li>
						<li><a>Bariatric Treatment</a></li>
					</ul>
				</div>
			</div>
			<!-- End Column -->
		</div>
		<!-- End Row -->
	</div>
	<!-- End Container -->
</div>
<!-- Hospital Short Details Ends Here -->

<!-- Specialty Content Starts Here -->
<div class="speciality-content-area">
	<div class="container">

		<div class="innertab-accordion">
			<div class="row">
				<div class="col-md-3 tabnopaddright">
					<div class="tabs">
						<ul>
							<li class="tab-active"><a href="bariatric-surgery">Bariatric
									Treatment</a></li>
							<li><a href="gastric-bypass-surgery">Gastric Bypass</a></li>
							<li><a href="gastric-sleeve-surgery">Sleeve Gastrectomy
							</a></li>
						</ul>
					</div>
				</div>
				<!-- End Column -->
				<div class="col-md-9 tabnopaddleft">
					<div class="accordion-contentarea">

						<div id="tab1" class="visible">
							<h2>
								<span>Bariatric Treatment </span>India
							</h2>
							<div class="row">
								<div class="col-md-9 col-sm-9 nopaddleft">
									<p>
										We at Arvene Healthcare have the expertise to organize for all
										treatments and surgeries related to Bariatric (Weight Loss).
										Arvene Healthcare is associated with top hospitals and Doctors
										of India in this field. We can give you more and better
										options so that you can take the right decisions. We also
										assist you in taking best decision based on your requirement.
										Arvene Healthcare will also arrange for your stay and local
										travel, so that you have nothing else to worry about but your
										treatment. Please contact us on <a
											href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
										today to avail best services.
									</p>
									<h3>Bariatric Surgery:</h3>
									<p>Bariatric surgery or Weight loss surgery, is the
										procedure for people who are severely obese. In this weight
										loss procedure, the size of the stomach is reduced by
										implanting gastric banding medical device. This can also be
										done by re-routing and resecting the small intestines to a
										small stomach pouch and the process is termed as gastric
										bypass surgery. The other way could be a removal of the
										stomach</p>
									<h3>
										<span>Who should consider</span> Bariatric Surgery?
									</h3>
								</div>
								<!-- End Inner Column -->
								<div class="col-md-3 col-sm-3 nopaddright">
									<img
										src="images/specialities/46. Bariatric(Weight loss surgery).jpg"
										alt="Bariatric(Weight loss surgery)" class="img-responsive">
								</div>
								<!-- End Inner Column -->
							</div>
							<!-- End Inner Row -->
							<p>The candidates with following conditions can be considered
								for weight-loss surgery �</p>
							<ul>
								<li><span>Having a <strong>BMI (Body Mass
											Index) </strong>in between 35-40 along with an obesity-related
										condition of diabetes mellitus, severe sleep apnea,
										cardiovascular disease and hypertension.
								</span></li>
								<li><span>Having a <strong>BMI (Body Mass
											Index)</strong> of 40 or more.
								</span></li>
							</ul>
							<h3>
								<span>Types of </span>Bariatric Surgery
							</h3>
							<p>
								<strong>Gastric Sleeve: </strong> : Gastric Sleeve surgery, also
								referred to as Sleeve Gastrectomy, is primarily performed to
								remove about 85% of the stomach. The rest 15% of the original
								capacity of the stomach is left for shaping in the form of a
								sleeve.
							</p>
							<p>
								<strong>Gastric Bypass Surgery: </strong> The purpose of this
								procedure is to separate the stomach into two unequal sections.
								Only 5 percent of the stomach area is left for food consumption.
								The food gets emptied from this small stomach pocket in the
								upper intestine at the time of digestion.
							</p>
							<p>
								<strong> Biliopancreatic Diversion: </strong> Just like gastric
								bypass surgery, it also creates a smaller stomach. In this,
								ingested food is not fully absorbed in the intestine.
							</p>
							<p>
								<strong>LAP-BAND Surgery: </strong> Around the stomach�s upper
								part, an inflatable band is used that divides the stomach into
								two unequal parts. The stomach�s upper part is treated as a new
								stomach and the food intake is restricted and promotes the
								weight loss.
							</p>
							<h3>
								<span>Cost of </span>Bariatric Surgery
							</h3>
							<p>India is known worldwide for its cost-effective Bariatric
								surgery that has proved as the best method for weight loss.</p>
							<h5>
								To get best options for Bariatric treatment, send us a query or
								write to us at <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
							</h5>

						</div>
						<!-- End Tab -->

					</div>
				</div>
				<!-- End Column -->
			</div>
			<!-- End Row -->
		</div>
		<!-- End Tab Accordion -->

	</div>
	<!-- End Container -->
</div>
<!-- Speciality Content Ends Here -->

<%@ include file="newFooter.jsp"%>
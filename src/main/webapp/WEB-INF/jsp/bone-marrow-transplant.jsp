<%@ include file="newHeader.jsp"%></section>
	<!-- Hospital Short Details Starts Here -->
	<div class="short-details">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="title">
						<h1>Organ Transplant</h1>
						<ul class="paginator">
							<li><a href="/">Home</a></li>
							<li><a>Organ Transplant</a></li>
						</ul>
					</div>
				</div>
				<!-- End Column -->
			</div>
			<!-- End Row -->
		</div>
		<!-- End Container -->
	</div>
	<!-- Hospital Short Details Ends Here -->

	<!-- Specialty Content Starts Here -->
	<div class="speciality-content-area">
		<div class="container">

			<div class="innertab-accordion">
				<div class="row">
					<div class="col-md-3 tabnopaddright">
						<div class="tabs">
							<ul>
								<li><a href="organ-transplant">Organ
										Transplant</a></li>
								<li><a href="kidney-transplant-surgery">Kidney Transplant
										Surgery</a></li>
								<li><a href="liver-transplant-surgery">Liver Transplant
										Surgery</a></li>
								<li><a href="heart-transplant">Heart Transplant</a></li>
								<li class="tab-active"><a href="bone-marrow-transplant">Bone Marrow
										Transplants</a></li>
								<li><a href="human-organs-transplant-laws-india">Organ Transplant
										laws in India</a></li>
							</ul>
						</div>
					</div>
					<!-- End Column -->
					<div class="col-md-9 tabnopaddleft">
						<div class="accordion-contentarea">
<div id="tab5" >
								<h2>
									<span>Bone Marrow Transplant</span> -India
								</h2>
								<div class="row">
									<div class="col-md-9 col-sm-9 nopaddleft">
										<p>We at Arvene Healthcare have the expertise to organize for all treatments and surgeries related to Bone Marrow Transplant. Arvene Healthcare is associated with top hospitals and Doctors of India in this field. We can give you more and better options so that you can take the right decisions. We also assist you in taking best decision based on your requirement. Arvene Healthcare will also arrange for your stay and local travel, so that you have nothing else to worry about but your treatment. Please contact us on <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a> today to avail best services.</p>
										<h3>
									    <span>Bone Marrow </span>Transplant:
								        </h3>
								        <p>Bone marrow transplant, popularly known as stem cell transplant, includes taking of cells that can be found in the stem cells (bone marrow). These cells are filtered and are then given back to either other person or to the patient. The aim of BMT (bone marrow transplant) is to transfer healthy bone marrow cells in a patient when their unhealthy bone marrow has been treated for destroying the abnormal cells. Following are the diseases that can be successfully treated by BMT -</p><br>
										<ul>
											<li><span>Solid Tumor cancers</span></li>
											<li><span>Leukaemia</span></li>
											<li><span>Multiple myeloma</span></li>
											<li><span>Severe aplastic anaemia</span></li>
											<li><span>Immune deficiency disorders</span></li>
											<li><span>Lymphomas</span></li>
										</ul>
									</div>
									<!-- End Inner Column -->
									<div class="col-md-3 col-sm-3 nopaddright">
										<img src="images/specialities/42.bone marro transplant.jpg"
											alt="bone marro transplant" class="img-responsive">
									</div>
									<!-- End Inner Column -->
								</div>
								<!-- End Inner Row -->
								<h3>
									<span>Requirement of a </span>Bone Marrow Transplant
								</h3>
								<ul>
									<li><span>Replacing of a bone marrow with a genetically healthy bone marrow. This is prevents more damage from a genetic disease.</span></li>
									<li><span>Replacing of non-functioning and diseased bone marrow with a healthy functioning bone marrow. This is done in specific conditions like sickle cell anaemia, leukaemia and aplastic anaemia.</span></li>
									<li><span>Replacing the bone marrow for restoring its normal functioning after being given high-doses of radiation therapy and chemotherapy for treating a disease. </span></li>
									<li><span>Regenerating a new immune system that will help to fight off other cancers that are not destroyed by the radiation therapy or chemotherapy used in the transplant, or residual or existing leukaemia.</span></li>
								</ul>
								<h3>
									<span>Different Types of </span>Bone Marrow Transplants
								</h3>
								<p>A number of bone marrow transplants are there that primarily depends on the donor that include -</p>
								<ul>
									<li><span><strong>Allogeneic Bone Marrow
												Transplant: </strong> This implant requires a genetic type of the donor similar to the patient. The stem cells are then taken by apheresis or bone marrow harvest from a genetically-matched donor. This generally involves a sister or brother. Some of the other donors are-</span></li>
									<li><span><strong>Unrelated Bone Marrow
												Transplants: </strong> An unrelated donor provides genetically matched stem cells or marrow. National bone marrow registries can provide these types of donor.</span></li>
									<li><span><strong>A Parent: </strong> When the genetic match is minimally identical to the recipient and a parent is the donor then it is known as a haploid-identical match.</span></li>
									<li><span><strong>Umbilical Cord Blood
												Transplant: </strong> Right away after the delivery of an infant, the stem cells are then taken from an umbilical cord. They then reproduce into functioning and mature blood cells that more effective as compared to those stem cells taken from the bone marrow of other adult or child.</span></li>
									<li><span><strong>Autologous Bone Marrow
												Transplant: </strong> A patient is itself a donor. In this transplant, stem cells are taken from a patient by apheresis or bone marrow harvest. These cells are then given back to a patient after they have been frozen. Rescue is the name given to this process.</span></li>
								</ul>
								<h3>
									<span>Bone Marrow Transplant </span>Procedure
								</h3>
								<p>A stem cell or bone marrow transplant is a long and complicatedprocess that involves five main stages.These stages are:</p>
								<ul>
									<li><span>Tests and examinations - to assess your general level of health.</span></li>
									<li><span>Harvesting - the process of obtaining the stem cells to be used in the transplant, either from you or a donor.</span></li>
									<li><span>Conditioning - treatment to prepare your body for the transplant.</span></li>
									<li><span>Transplanting the stem cells.</span></li>
									<li><span>Recovery - you'll need to stay in hospital for at least a few weeks until the transplant starts to take effect.</span></li>
								</ul>
								<h3>
									<span>Cost of</span> Bone Marrow Transplant in India
								</h3>
								<p>The cost of Bone Marrow transplant cost in India is inexpensive among all popular Medical Tourism Destinations.</p>
Bone Marrow Transplant <h5>Cost in India starts at 35000 US Dollars. To know more about it please share your reports on <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a> or whatsapp/viber/imo to +918130077375.</h5><p> 
(Please note this is a tentative plan and actual Costs are based on reports shared. Final treatment plan and estimation will be provided after the patient has been evaluated.)</p>
								<h5>
									To get best options for Orthopaedic treatment, send us a query
									or write to us at <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
								</h5>
							</div>
							<!-- End Tab -->

						</div>
					</div>
					<!-- End Column -->
				</div>
				<!-- End Row -->
			</div>
			<!-- End Tab Accordion -->

		</div>
		<!-- End Container -->
	</div>
	<!-- Speciality Content Ends Here -->

	<%@ include file="newFooter.jsp"%>
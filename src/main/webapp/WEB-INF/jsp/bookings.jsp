<%@ include file="newHeader.jsp"%></section>
<div class="container">

	<div class="mainContent">
		<c:choose>
			<c:when test="${userType == 'U'}">
				<c:set var="type" value="Appointments"></c:set>
				<h3 class="pageHeader" style="margin: auto;">Appointments</h3>
			</c:when>
			<c:when test="${userType == 'S'}">
				<c:set var="type" value="Bookings"></c:set>
				<h3 class="pageHeader" style="margin: auto;">Bookings</h3>
			</c:when>
		</c:choose>

		<br>

		<div style="float: right">
			<a href="bookingsHistory">View Bookings History</a>
		</div>

		<c:choose>
			<c:when test="${fn:length(bookings) == 0}">
				<h4 class="list-group-item-text">You have no ${type} as of now.</h4>
			</c:when>
			<c:otherwise>
				<div class="drl-bgwhite uqt-main">
					<div class="table-responsive">
						<table class="table table-condensed  table-striped ">
							<thead class="uqt-tab-head">
								<tr>
									<th style="width: 4%;">S.NO</th>
									<th style="width: 18%;">Patient Name</th>
									<c:if test="${userType == 'U'}">
										<th style="width: 18%;">Doctor name</th>
									</c:if>
									<th style="width: 30%;">${type}Details</th>
									<th style="width: 30%;">Session Time</th>
									<th style="width: 30%;">Action</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${bookings}" var="booking"
									varStatus="bookingIndex">
									<tr>
										<td>${param.start +bookingIndex.index +1 }</td>
										<td>${booking.patientName}</td>
										<c:if test="${userType == 'U'}">
											<td>
												${booking.specialistDetails.firstName}&nbsp;
												${booking.specialistDetails.lastName}(${booking.specialistDetails.title})</td>
										</c:if>
										<td><b> ${booking.title}</b>

											<div id="col-${booking.id}" class="collapse">
												${booking.desc}<br />
												<c:forEach items="${booking.reports}" var="report"
													varStatus="reportIndex">
													<div>
														<a href="${report.url}" class="col-lin">${report.fileName}</a>
													</div>
												</c:forEach>
											</div></td>
										<td>${booking.slot.startdate}</td>
										<td><a href="joinsession?id=${booking.id}" role="button">Start
												session</a>&nbsp;&nbsp; <span class="fa fa-eye marg-uqt-span"
											data-toggle="collapse" data-target="#col-${booking.id}"></span>


										</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			</c:otherwise>
		</c:choose>

		<div class="row">
			<div class="col-lg-12">
				<div class="drl-btn">
					<div id="pagination"></div>
				</div>
			</div>
		</div>

	</div>

	<!-- Div for shwowing booking Requests -->
	<c:if test="${userType == 'U'}">

		<div>
			<c:choose>
				<c:when test="${fn:length(bookingRequests) ge 0}">
					<h4 class="list-group-item-text">Booking Requests</h4>
					<div class="drl-bgwhite uqt-main">
						<div class="table-responsive">
							<table class="table table-condensed  table-striped ">

								<thead class="uqt-tab-head">
									<tr>
										<th style="width: 5%;">S.NO</th>
										<th style="width: 30%;">Doctor name</th>
										<th style="width: 30%;">${type}Details</th>
										<th style="width: 15%;">Requested Date</th>
										<th style="width: 20%;">View Details</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${bookingRequests}" var="booking"
										varStatus="bookingIndex">
										<tr>
											<td>${param.start +bookingIndex.index +1 }</td>
											<td>
												${booking.specialistDetails.firstName}&nbsp;
												${booking.specialistDetails.lastName}(${booking.specialistDetails.title})</td>

											<td><b> ${booking.title}</b>

												<div id="req_col-${booking.id}" class="collapse">
													${booking.desc}<br />

													<c:forEach items="${booking.reports}" var="report"
														varStatus="reportIndex">
														<div>
															<a href="${report.url}" class="col-lin">${report.fileName}</a>
														</div>
													</c:forEach>
												</div></td>
											<td>${booking.created}</td>
											<td><span class="fa fa-eye marg-uqt-span"
												data-toggle="collapse" data-target="#req_col-${booking.id}"></span></td>

										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>

				</c:when>
			</c:choose>


		</div>

	</c:if>
</div>






<%-- <div style="width:970px;">
   		   <c:if test="${fn:length(bookings) == 0}">
		   		  <h4 class="list-group-item-text">You have no ${type} as of now</h4>
			</c:if>
		   <div class="list-group">
		   <c:forEach items="${bookings}" var="booking">
		 	 <div class="list-group-item">
		 	  <c:choose>
                    <c:when test="${userType == 'U'}">
		   		 <h4 class="list-group-item-heading">${booking.slot.user.username}</h4>
		   		  <p class="list-group-item-text">Specialized in ${booking.slot.user.speciality.name}</p>
		   		  <p class="list-group-item-text">Booking Title :  ${booking.title}</p>
		   		   <a class="pull-right" href="joinsession?id=${booking.id}" class="btn btn-success btn-large btn-block" role="button">Start session</a>
		   		  <p class="list-group-item-text">Booking Desc :  ${booking.desc}</p>
		   		  <p class="list-group-item-text">Session starts at  ${booking.slot.startdate}</p>
		   		 
		   		 </c:when>
		   		  <c:when test="${userType == 'S'}">
		   		 <h4 class="list-group-item-heading">${booking.subscriber.name}</h4>
		   		  <p class="list-group-item-text">Booking Title :  ${booking.title}</p>
		   		   <a class="pull-right" href="joinsession?id=${booking.id}" class="btn btn-success btn-large btn-block" role="button">Start session</a>
		   		  <p class="list-group-item-text">Booking Desc :  ${booking.desc}</p>
		   		  <p class="list-group-item-text">Session starts at  ${booking.slot.startdate}</p>
		   		 </c:when>
		   		 </c:choose>	
		  	</div>
		  	</c:forEach>
			</div>
			
			</div>
			<div id="pagination"></div>
	</div> --%>
<script type='text/javascript'>
	var options = {
		currentPage : '${currentpage}',
		totalPages : '${pageno}',
		onPageClicked : function(e, originalEvent, type, page) {
			fetchbooking(page)
		},
		size : 'medium',
		alignment : 'center'

	};

	$('#pagination').bootstrapPaginator(options);

	function fetchbooking(page) {
		var start = (page * 10) - 10;
		window.location.href = "bookings?start=" + start + "&range=10";
	}
	$('.nav >li.active').removeClass("active");
	$('.nav #bookings').addClass("active");
</script>
<!-- <script type="text/javascript">
 <c:if test="${not empty error}">


		 BootstrapDialog.show({
               type:  BootstrapDialog.TYPE_DANGER,
               title: '${errortitle}',
               message: '${error}',
               buttons: [{
                   label: 'OK',
                   action: function(dialog) {
                       dialog.close();
                   }
               }]
           });    


</c:if>
<c:if test="${not empty msg}">

		 BootstrapDialog.show({
               type:  BootstrapDialog.TYPE_SUCCESS,
               title: '${msgtitle}',
               message: '${msg}',
               buttons: [{
                   label: 'OK',
                   action: function(dialog) {
                       dialog.close();
                   }
               }]
           });    
	

</c:if>
</script> -->

<%@ include file="newFooter.jsp"%>


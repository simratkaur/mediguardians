<%@ include file="newHeader.jsp"%></section>

<div class="container">
	<div class="mainContent">
		<c:choose>
			<c:when test="${userType == 'U'}">
				<c:set var="type" value="Appointments"></c:set>
				<h3 class="pageHeader" style="margin: auto;">Appointments History</h3>
			</c:when>
			<c:when test="${userType == 'S'}">
				<c:set var="type" value="Bookings"></c:set>
				<h3 class="pageHeader" style="margin: auto;">Bookings History</h3>
			</c:when>
		</c:choose>

		<br>

		<c:choose>
			<c:when test="${fn:length(bookings) == 0}">
				<h4 class="list-group-item-text">You have no ${type} as of now.</h4>
			</c:when>
			<c:otherwise>
				<table class="table table-striped rwd-table">
					<thead>
						<tr>
							<th style="width: 4%;">S.NO</th>
							<th style="width: 18%;">Patient Name</th>
							<c:if test="${userType == 'U'}">
								<th style="width: 18%;">Doctor name</th>
							</c:if>
							<th style="width: 40%;">${type}Details</th>
							<th style="width: 20%;">Session Time</th>
							<th style="width: 30%;">Action</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${bookings}" var="booking"
							varStatus="bookingIndex">
							<tr>
								<td>${param.start +bookingIndex.index +1 }</td>
								<td>${booking.patientName}</td>
								<c:if test="${userType == 'U'}">
									<td>
										${booking.specialistDetails.firstName}&nbsp;
										${booking.specialistDetails.lastName}(${booking.specialistDetails.title})</td>
								</c:if>
								<td><b> ${booking.title}</b>
								<div id="col-${booking.id}"  class="collapse">
										 ${booking.desc}<br /> <c:forEach
											items="${booking.reports}" var="report"
											varStatus="reportIndex">
											<div>
												<a href="${report.url}" class="col-lin">${report.fileName}</a>
											</div>
										</c:forEach>
										</div>
								</td>
								<td>${booking.slot.startdate}</td>
								<td><span class="fa fa-eye marg-uqt-span"
											data-toggle="collapse" data-target="#col-${booking.id}"></span> </td> 
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</c:otherwise>
		</c:choose>
	</div>
	<div class="row">
			<div class="col-lg-12">
				<div class="drl-btn">
					<div id="pagination"></div>
				</div>
			</div>
		</div>
</div>

<script type='text/javascript'>
	var options = {
		currentPage : '${currentpage}',
		totalPages : '${pageno}',
		onPageClicked : function(e, originalEvent, type, page) {
			fetchbooking(page)
		},
		size : 'medium',
		alignment : 'center'

	};

	$('#pagination').bootstrapPaginator(options);

	function fetchbooking(page) {
		var start = (page * 10) - 10;
		window.location.href = "bookingsHistory?start=" + start + "&range=10";
	}
	$('.nav >li.active').removeClass("active");
	$('.nav #bookings').addClass("active");
</script>
<!-- <script type="text/javascript">
 <c:if test="${not empty error}">


		 BootstrapDialog.show({
               type:  BootstrapDialog.TYPE_DANGER,
               title: '${errortitle}',
               message: '${error}',
               buttons: [{
                   label: 'OK',
                   action: function(dialog) {
                       dialog.close();
                   }
               }]
           });    


</c:if>
<c:if test="${not empty msg}">

		 BootstrapDialog.show({
               type:  BootstrapDialog.TYPE_SUCCESS,
               title: '${msgtitle}',
               message: '${msg}',
               buttons: [{
                   label: 'OK',
                   action: function(dialog) {
                       dialog.close();
                   }
               }]
           });    
	

</c:if>
</script> -->

<%@ include file="newFooter.jsp"%>


<%@ include file="newHeader.jsp"%></section>
<!-- Hospital Short Details Starts Here -->
<div class="short-details">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="title">
					<h1>Neuro and Spine</h1>
					<ul class="paginator">
						<li><a href="/">Home</a></li>
						<li><a>Neuro and Spine</a></li>
					</ul>
				</div>
			</div>
			<!-- End Column -->
		</div>
		<!-- End Row -->
	</div>
	<!-- End Container -->
</div>
<!-- Hospital Short Details Ends Here -->

<!-- Specialty Content Starts Here -->
<div class="speciality-content-area">
	<div class="container">

		<div class="innertab-accordion">
			<div class="row">
				<div class="col-md-3 tabnopaddright">
					<div class="tabs">
						<ul>
							<li><a href="neuro-spine-surgery">Neuro
									and Spine</a></li>
							<li><a href="neuro-surgery">Neurosurgery</a></li>
							<li><a href="neurology">Neurology</a></li>
							<li class="tab-active"><a href="brain-tumors-treatment">Brain Tumor</a></li>
							<li><a href="spinal-fusion-surgery">Spinal Fusion</a></li>
							<li><a href="intracranial-aneurysm-surgery">Aneurysms</a></li>
							<li><a href="spinal-tumor-surgery">Spinal Tumor</a></li>
							<li><a href="spinal-laminectomy-surgery">Laminectomy</a></li>
						</ul>
					</div>
				</div>
				<!-- End Column -->
				<div class="col-md-9 tabnopaddleft">
					<div class="accordion-contentarea">

						<div id="tab4">
							<h2>
								<span>Brain Tumors Treatment </span>- India
							</h2>
							<div class="row">
								<div class="col-md-9 col-sm-9 nopaddleft">
									<p>We at Arvene Healthcare have the expertise to organize
										for all treatments and surgeries related to Brain Tumors.
										Arvene Healthcare is associated with top hospitals and Doctors
										of India in this field. We can give you more and better
										options so that you can take the right decisions. We also
										assist you in taking best decision based on your requirement.
										Arvene Healthcare will also arrange for your stay and local
										travel, so that you have nothing else to worry about but your
										treatment. Please contact us on <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
										today to avail best services.</p>
								</div>
								<!-- End Inner Column -->
								<div class="col-md-3 col-sm-3 nopaddright">
									<img src="images/specialities/26.brain tumor.jpg"
										alt="brain tumor" class="img-responsive">
								</div>
								<!-- End Inner Column -->
							</div>
							<!-- End Inner Row -->
							<h3>
								<span>What is a </span>Brain Tumor?
								<h3>
									<p>An abnormal growth of cells in the brain is called Brain
										Tumor. This condition can be seen in both adults and children.
										Brain tumors can be categorized into cancerous (malignant) and
										non-cancerous (benign). Kidney cancer, lung cancer, melanoma
										and breast cancer can also spread to the brain.</p>
									<span>What Causes</span> Brain Tumors?
								</h3>
								<p>There is no as such proven cause of brain tumors.
									However, some of the causes include -</p>
								<ul>
									<li><span>A family history of genetic disorders </span></li>
									<li><span>People with low immune system</span></li>
									<li><span>Exposure to radiation</span></li>
								</ul>
								<h3>
									<span>Symptoms of</span> Brain Tumors
								</h3>
								<p>Headaches are considered as the most common symptoms of a
									brain tumor. Further symptoms are -</p>
								<ul>
									<li><span>Problems with memory</span></li>
									<li><span>Seizures</span></li>
									<li><span>Weakness in one part of the body</span></li>
									<li><span>Problems while walking</span></li>
									<li><span>Changes in hearing and speech</span></li>
									<li><span>Tingling or numbness in the arms or legs</span></li>
									<li><span>Personality changes</span></li>
									<li><span>Balance problems</span></li>
									<li><span>Unable to concentrate</span></li>
									<li><span>Changes in vision</span></li>
								</ul>
								<h3>
									<span>Brain Tumor</span> Surgery
								</h3>
								<p>Brain tumors can be removed by the means of surgery.
									Surgery is the only treatment for non-cancerous (benign)
									tumors. Whereas cancerous (malignant) tumors require
									chemotherapy or radiation treatment along with the surgery.
									Improving overall quality of life by reducing symptoms is the
									main aim for this surgery.</p>
								<br>
								<p>Proton therapy is considered one of the best treatment
									for brain tumors. Proton therapy minimizes side effects and
									long term complications can be avoided. Proton therapy uses
									higher doses of radiation for targeting brain cancer. This also
									causes less damage to the surrounding tissues.</p>
								<h3>
									<span>Benefits of Brain </span>Tumor Surgery
									<p>The potential benefits will depend upon the individual
										situation. The goal is to cure several types of brain tumor at
										once. A brain tumor treatment can improve symptoms and improve
										quality of life.</p>
									<h3>
										<span>Recovery after Brain </span>Tumor Surgery
										<p>A patient is recommended for speech therapy,
											physiotherapy and occupational therapy. The aim of these
											therapies is to speed up the recovery period so that the
											patient may get on with his routine life as soon as possible.</p>
										<h3>
											<span>Success and Survival Rate for</span> Brain Tumors
										</h3>
										<p>Survival rate for a brain tumor depends on multiple
											factors such as size, location and type of tumor. The
											survival rate is determined in terms of percentage of people
											who survive for 5 years after being diagnosed of having a
											brain tumor.</p>
										<h3>
											<span>Cost of</span> Brain Tumor Surgery
										</h3>
										<p>India is known worldwide for its advanced medical
											facilities and promising technology for brain tumor surgery.
											Some of the best hospitals for brain tumor can be found in
											India. Also the cost of brain tumor surgery in India is
											substantially lower than other developed countries of the
											world.</p>
										<h5>
											To get best options for Neuro and Spine treatment, send us a
											query or write to us at <a
												href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
										</h5>
						</div>
						<!-- End Tab -->

					</div>
				</div>
				<!-- End Column -->
			</div>
			<!-- End Row -->
		</div>
		<!-- End Tab Accordion -->

	</div>
	<!-- End Container -->
</div>
<!-- Speciality Content Ends Here -->

<%@ include file="newFooter.jsp"%>
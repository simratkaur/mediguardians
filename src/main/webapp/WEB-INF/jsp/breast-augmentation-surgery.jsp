<%@ include file="newHeader.jsp"%></section>
<!-- Hospital Short Details Starts Here -->
<div class="short-details">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="title">
					<h1>Cosmetic Surgery</h1>
					<ul class="paginator">
						<li><a href="/">Home</a></li>
						<li><a>Cosmetic Surgery</a></li>
					</ul>
				</div>
			</div>
			<!-- End Column -->
		</div>
		<!-- End Row -->
	</div>
	<!-- End Container -->
</div>
<!-- Hospital Short Details Ends Here -->

<!-- Specialty Content Starts Here -->
<div class="speciality-content-area">
	<div class="container">

		<div class="innertab-accordion">
			<div class="row">
				<div class="col-md-3 tabnopaddright">
					<div class="tabs">
						<ul>
							<li><a href="cosmetic-surgery">Cosmetic Surgery</a></li>
							<li class="tab-active"><a href="breast-augmentation-surgery">Breast
									Augmentation</a></li>
							<li><a href="breast-reduction-surgery">Breast Reduction</a></li>
							<li><a href="liposuction-surgery">Liposuction</a></li>
							<li><a href="facelift-surgery">Facelift</a></li>
							<li><a href="rhinoplasty-surgery">Rhinoplasty</a></li>
							<li><a href="cosmetic-laser-surgery">Cosmetic Laser
									Surgery</a></li>
							<li><a href="oral-maxillofacial-surgery">Maxillofacial
									Surgery</a></li>
							<li><a href="surgical-hair-transplant">Surgical Hair
									Transplant</a></li>
						</ul>
					</div>
				</div>
				<!-- End Column -->
				<div class="col-md-9 tabnopaddleft">
					<div class="accordion-contentarea">
<div id="tab2">
							<h2>
								<span>Breast Augmentation Surgery </span> India
							</h2>
							<div class="row">
								<div class="col-md-9 col-sm-9 nopaddleft">
									<p>We at Arvene Healthcare have the expertise to organize
										for all treatments and surgeries related to Cosmetic Surgery
										and Breast Augmentation Surgery. Arvene Healthcare is
										associated with top hospitals and Doctors of India in this
										field. We can give you more and better options so that you can
										take the right decisions. We also assist you in taking best
										decision based on your requirement. Arvene Healthcare will
										also arrange for your stay and local travel, so that you have
										nothing else to worry about but your treatment. Please contact
										us on <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a> today to avail best
										services.</p>
								</div>
								<!-- End Inner Column -->
								<div class="col-md-3 col-sm-3 nopaddright">
									<img src="images/specialities/9.breast augmentation.jpg"
										alt="breast augmentation" class="img-responsive">
								</div>
								<!-- End Inner Column -->
							</div>
							<!-- End Inner Row -->
							<h3>
								<span>Breast Implant </span>Surgery
							</h3>
							<p>Breast augmentation surgery, also known as augmentation
								mammoplasty, is the process in which the implants are placed
								below the breast to broaden or enlarge the size of the breast.
								Women can opt for this surgery due to several reasons such as
								under developed breast, disproportion of the breast, or changes
								in size of the breast after breast feeding or pregnancy. Breast
								augmentation (breast implant surgery) is one of the most sought
								after treatment for improving the size and shape of the breast.
							</p>

							<ul>
								<li><span>Enlarge small breast</span></li>
								<li><span>Can reconstruct the breast following
										mastectomy</span></li>
								<li><span>Can add more volume to the breast after
										weight loss or pregnancy</span></li>
								<li><span>Can help in reshaping the asymmetric
										breast</span></li>

								<h3>
									<span>Before </span>Breast Implant Surgery
								</h3>
								<p>A thorough medical examination is required to know the
									breast design which includes symmetry, size and in accordance
									with the rest of the body. Medical tests are also conducted to
									know if there is any breast disease. Baseline mammogram and the
									blood tests are also done.</p>


								<h3>
									<span>Procedure for</span> Breast Augmentation
								</h3>
								<p>The procedure of breast augmentation is done under local
									anaesthesia and the surgery takes approximately two hours to
									finish. The emphasis is on making a particular pocket by taking
									in consideration the implant type along with the different
									breast characteristics.</p>

								<h3>
									<span>Post </span>Breast Augmentation
								</h3>
								<p>The patients can face little discomfort, swelling or
									stretching and sometimes bruising can also be expected. But
									overall it's much tolerable. After few days' time, the patients
									can start their routine activities. There can be a little
									breast sensation, but the sensation reduces after a few weeks.
								</p>
								<h3>
									<span>Cost of </span>Breast Augmentation Surgery
								</h3>
								<p>Breast implants surgery in India has become very popular
									as they are available at extremely affordable price. Many
									patients from across the world visits to seek breast implant
									surgery in India. The treatment is carried out in many Indian
									cities having some of the best hospitals equipped with latest
									technologies.</p>
									<h5>Cost of Breast Augmentation surgery in India starts at 5000 US Dollars. To know more about it please share your reports on <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a> or whatsapp/viber/imo to +918130077375.</h5><p> 
(Please note this is a tentative plan and actual Costs are based on reports shared. Final treatment plan and estimation will be provided after the patient has been evaluated.)</p>
									
								<h5>
									To get best options for cosmetic treatment, send us a query or
									write to us at <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
								</h5>
						</div>
						<!-- End Tab -->

					</div>
				</div>
				<!-- End Column -->
			</div>
			<!-- End Row -->
		</div>
		<!-- End Tab Accordion -->

	</div>
	<!-- End Container -->
</div>
<!-- Speciality Content Ends Here -->

<%@ include file="newFooter.jsp"%>
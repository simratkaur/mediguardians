<%@ include file="newHeader.jsp"%></section>
<!-- Hospital Short Details Starts Here -->
<div class="short-details">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="title">
					<h1>Cancer</h1>
					<ul class="paginator">
						<li><a href="/">Home</a></li>
						<li><a>Cancer</a></li>
					</ul>
				</div>
			</div>
			<!-- End Column -->
		</div>
		<!-- End Row -->
	</div>
	<!-- End Container -->
</div>
<!-- Hospital Short Details Ends Here -->

<!-- Specialty Content Starts Here -->
<div class="speciality-content-area">
	<div class="container">

		<div class="innertab-accordion">
			<div class="row">
				<div class="col-md-3 tabnopaddright">
					<div class="tabs">
						<ul>
							<li><a href="cancer-treatment">Cancer</a></li>
							<li class="tab-active"><a href="breast-cancer-treatment">Breast Cancer</a></li>
							<li><a href="cervical-cancer-treatment">Cervical Cancer</a></li>
							<li><a href="prostate-cancer-treatment">Postate Cancer</a></li>
							<li><a href="colon-cancer-treatment">Colon Cancer</a></li>
							<li><a href="cyberknife-radiation-therapy">Cyberknife
									Radiation Therapy</a></li>
						</ul>
					</div>
				</div>
				<!-- End Column -->
				<div class="col-md-9 tabnopaddleft">
					<div class="accordion-contentarea">
<div id="tab2">
							<h2>
								<span>Breast Cancer Treatment</span> in India
							</h2>
							<p>We at Arvene Healthcare have the expertise to organize for
								all treatments and surgeries related to Breast Cancer. Arvene
								Healthcare is associated with top hospitals and Doctors of India
								in this field. We can give you more and better options so that
								you can take the right decisions. We also assist you in taking
								best decision based on your requirement. Arvene Healthcare will
								also arrange for your stay and local travel, so that you have
								nothing else to worry about but your treatment. Please contact
								us on <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a> today to avail best services.</p>

							<h3>
								<span>What is </span>Breast Cancer?
							</h3>
							<div class="row">
								<div class="col-md-9 col-sm-9 nopaddleft">
									<p>A collection of cancer cells that arise from the cells
										of the breast is known as breast cancer. Breast cancer is
										majorly seen in women. The calcium deposits or formation small
										tumor is the beginning of any breast cancer that spread from
										the blood stream into the other organs or into the lymph
										nodes.</p>
									<h3>
										<span>Symptoms of </span>Breast Cancer
									</h3>
								</div>
								<!-- End Inner Column -->
								<div class="col-md-3 col-sm-3 nopaddright">
									<img src="images/specialities/18.breast cancer.jpg"
										alt="breast cancer" class="img-responsive">
								</div>
								<!-- End Inner Column -->
							</div>
							<!-- End Inner Row -->
							<p>Breast cancer in its early stages does not result in any
								symptoms. However, some of the symptoms are -</p>
							<ul>
								<li><span>Fluid coming out of the nipple can be
										green, bloody or clear to yellow. It can appear like pus</span></li>
								<li><span>Lump in the armpit or breast lump which
										has uneven edges and hard. This lump usually does not result
										in any pain</span></li>
								<li><span>Change in shape or size of the nipple or
										breast</span></li>
							</ul>
							<p>Tenderness, breast lump and breast pain are some of the
								common symptoms of male breast cancer.</p>

							<h3>
								<span>Types of </span>Breast Cancer
							</h3>
							<p>Invasive Breast Cancer: Invasive breast cancer develops
								when cancer cells spread outside the lining of lobules or ducts
								into the nearby tissue of the breast. Invasive breast cancer can
								be categorized into different types -</p>
							<ul>
								<li><span><strong>Triple Negative Breast
											Cancer </strong> </span></li>
								<li><span><strong>Inflammatory Breast
											Cancer </strong> </span></li>
								<li><span><strong>Invasive Ductal Breast
											Cancer </strong> </span></li>
								<li><span><strong>Paget's disease of the
											Breast </strong> </span></li>
								<li><span><strong>Invasive Lobular Breast
											Cancer </strong> </span></li>
							</ul>
							<p>
								<strong>Ductal carcinoma in situ (DCIS): </strong> This is
								considered as the one of the earliest forms of breast cancer.
								The cancer cells are present in the breast ducts though they
								have not spread to nearby breast tissue.
							</p>

							<p>
								<strong>HER2 positive breast cancer: </strong> When there is a
								large number of protein (known as HER2) in breast cancer cells
								on their surface then it results in HER2 positive breast cancer.
							</p>

							<h3>
								<span>Stages of </span>Breast Cancer
							</h3>
							<strong>Stages of breast cancer depends on four things -
							</strong>
							<ul>
								<li><span> The spread of the cancer in different
										parts of the body beyond the breast</span></li>
								<li><span>Size of the cancer</span></li>
								<li><span> Presence of cancer in the lymph nodes</span></li>
								<li><span>If cancer is non-invasive or invasive</span></li>
							</ul>

							<h3>
								<span>Diagnosis of </span>Breast Cancer
							</h3>
							<p>Physical examination of a patient is done that include
								armpits, chest area, breasts and neck area. A number of tests
								are performed for diagnosing breast cancer that include -</p>
							<ul>
								<li><span>Sentinel lymph node biopsy</span></li>
								<li><span>Breast ultrasound </span></li>
								<li><span>Mammography </span></li>
								<li><span>CT scan </span></li>

								<li><span>PET scan for checking the spread of the
										cancer</span></li>
							</ul>
							<h3>
								<span>Types of </span>Breast Cancer Surgery
							</h3>

							<ul>
								<li><span>Radical Mastectomy</span></li>
								<li><span>Lumpectomy (wide local excision)</span></li>
								<li><span>Modified Radical Mastectomy</span></li>
								<li><span>Quadrantectomy, partial or segmental
										mastectomy</span></li>
								<li><span>Total Mastectomy</span></li>
								<li><span>Radiation Therapy</span></li>
								<li><span>Chemotherapy</span></li>
								<li><span>Modified Radical Mastectomy</span></li>
								<li><span>Hormone Therapy</span></li>
							</ul>
<h5>Cost of Breast Cancer treatment in India starts at 8500 US Dollars. To know more about it please share your reports on <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a> or whatsapp/viber/imo to +918130077375.</h5><p> 
(Please note this is a tentative plan and actual Costs are based on reports shared. Final treatment plan and estimation will be provided after the patient has been evaluated.)</p>
							<h5>
								To get best options for cancer treatment, send us a query or
								write to us at <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
							</h5>
						</div>
						<!-- End Tab -->
						
					</div>
				</div>
				<!-- End Column -->
			</div>
			<!-- End Row -->
		</div>
		<!-- End Tab Accordion -->

	</div>
	<!-- End Container -->
</div>
<!-- Speciality Content Ends Here -->

<%@ include file="newFooter.jsp"%>
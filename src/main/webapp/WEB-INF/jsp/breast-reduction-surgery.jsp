<%@ include file="newHeader.jsp"%></section>
<!-- Hospital Short Details Starts Here -->
<div class="short-details">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="title">
					<h1>Cosmetic Surgery</h1>
					<ul class="paginator">
						<li><a href="/">Home</a></li>
						<li><a>Cosmetic Surgery</a></li>
					</ul>
				</div>
			</div>
			<!-- End Column -->
		</div>
		<!-- End Row -->
	</div>
	<!-- End Container -->
</div>
<!-- Hospital Short Details Ends Here -->

<!-- Specialty Content Starts Here -->
<div class="speciality-content-area">
	<div class="container">

		<div class="innertab-accordion">
			<div class="row">
				<div class="col-md-3 tabnopaddright">
					<div class="tabs">
						<ul>
							<li><a href="cosmetic-surgery">Cosmetic Surgery</a></li>
							<li><a href="breast-augmentation-surgery">Breast
									Augmentation</a></li>
							<li class="tab-active"><a href="breast-reduction-surgery">Breast Reduction</a></li>
							<li><a href="liposuction-surgery">Liposuction</a></li>
							<li><a href="facelift-surgery">Facelift</a></li>
							<li><a href="rhinoplasty-surgery">Rhinoplasty</a></li>
							<li><a href="cosmetic-laser-surgery">Cosmetic Laser
									Surgery</a></li>
							<li><a href="oral-maxillofacial-surgery">Maxillofacial
									Surgery</a></li>
							<li><a href="surgical-hair-transplant">Surgical Hair
									Transplant</a></li>
						</ul>
					</div>
				</div>
				<!-- End Column -->
				<div class="col-md-9 tabnopaddleft">
					<div class="accordion-contentarea">
						<div id="tab3">
							<h2>
								<span>Breast Reduction Surgery</span> in India
							</h2>
							<p>We at Arvene Healthcare have the expertise to organize for
								all treatments and surgeries related to Cosmetic Surgery and
								Breast Reduction Surgery. Arvene Healthcare is associated with
								top hospitals and Doctors of India in this field. We can give
								you more and better options so that you can take the right
								decisions. We also assist you in taking best decision based on
								your requirement. Arvene Healthcare will also arrange for your
								stay and local travel, so that you have nothing else to worry
								about but your treatment. Please contact us on
								<a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a> today to avail best services.</p>

							<h3>
								<span>What is </span>Breast Reduction Surgery?
							</h3>
							<div class="row">
								<div class="col-md-9 col-sm-9 nopaddleft">
									<p>Breast reduction surgery is done to decrease the size of
										the breast. There may be lot psychological problems and issues
										related to the condition of oversized breast. The problem of
										oversized breast is common in both men and women who are
										concerned with their looks or appearance.</p>
								</div>
								<!-- End Inner Column -->
								<div class="col-md-3 col-sm-3 nopaddright">
									<img src="images/specialities/10.breast reduction.jpg"
										alt="breast reduction" class="img-responsive">
								</div>
								<!-- End Inner Column -->
							</div>
							<!-- End Inner Row -->
							<h3>
								<span>Female </span>Breast Reduction
							</h3>
							<p>The problem of oversize breast in females is treated with
								the help of mammoplasty (breast reduction) surgery. Females with
								oversized breast are either not happy with their overall
								appearance and look or face problems like skin irritation,
								postures concerns and physical pain. .</p>
							<h3>
								<span>Male </span>Breast Reduction
							</h3>
							<p>The condition of male breast (gynecomastia) is because of
								the disproportion of androgen (male hormone) and oestrogen
								(female hormone), which leads to deposition of glands in the
								chest areas. This problem is generally seen in the young boys
								and is not due to any diseases or obesity. Surgery is the only
								treatment left.</p>

							<h3>
								<span>Procedure for </span>Breast Reduction Surgery
							</h3>
							<p>The time required to perform the surgery depends on the
								quantity of breast reduction or on the technique used. Different
								techniques like liposuction and short scar technique are the
								most preferred options.The whole surgery is performed under
								general anaesthesia and takes four hours to finish.</p>

							<h3>
								<span>After </span>Breast Reduction Surgery
							</h3>
							<p>The dressing is removed after 2-4 days and stitches after
								a week. The patient can resume light activities after a few days
								of the surgery. The patient should not do strenuous exercises
								for three to four weeks. Physical activity should also be
								avoided for at least six weeks.</p>
							<h3>
								<span>Cost of </span>Breast Reduction Surgery in India
							</h3>
							<p>Breast reduction in India is extremely affordable and the
								patients can avail high class medical services in all the
								plastic surgery breast reduction and cosmetic procedures.
								Liposuction technique is extremely expensive in western
								countries as compared to India.</p>
							<h5>
								To get best options for cosmetic treatment, send us a query or
								write to us at <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
							</h5>
						</div>
						<!-- End Tab -->


					</div>
				</div>
				<!-- End Column -->
			</div>
			<!-- End Row -->
		</div>
		<!-- End Tab Accordion -->

	</div>
	<!-- End Container -->
</div>
<!-- Speciality Content Ends Here -->

<%@ include file="newFooter.jsp"%>
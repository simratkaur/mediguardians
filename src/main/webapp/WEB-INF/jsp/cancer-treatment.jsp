<%@ include file="newHeader.jsp"%></section>
<!-- Hospital Short Details Starts Here -->
<div class="short-details">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="title">
					<h1>Cancer</h1>
					<ul class="paginator">
						<li><a href="/">Home</a></li>
						<li><a>Cancer</a></li>
					</ul>
				</div>
			</div>
			<!-- End Column -->
		</div>
		<!-- End Row -->
	</div>
	<!-- End Container -->
</div>
<!-- Hospital Short Details Ends Here -->

<!-- Specialty Content Starts Here -->
<div class="speciality-content-area">
	<div class="container">

		<div class="innertab-accordion">
			<div class="row">
				<div class="col-md-3 tabnopaddright">
					<div class="tabs">
						<ul>
							<li class="tab-active"><a href="cancer-treatment">Cancer</a></li>
							<li><a href="breast-cancer-treatment">Breast Cancer</a></li>
							<li><a href="cervical-cancer-treatment">Cervical Cancer</a></li>
							<li><a href="prostate-cancer-treatment">Postate Cancer</a></li>
							<li><a href="colon-cancer-treatment">Colon Cancer</a></li>
							<li><a href="cyberknife-radiation-therapy">Cyberknife
									Radiation Therapy</a></li>
						</ul>
					</div>
				</div>
				<!-- End Column -->
				<div class="col-md-9 tabnopaddleft">
					<div class="accordion-contentarea">

						<div id="tab1" class="visible">
							<h2>
								<span>Cancer Treatment</span> in India
							</h2>
							<p>We at Arvene Healthcare have the expertise to organize for
								all treatments and surgeries related to Cancer. Arvene
								Healthcare is associated with top hospitals and Doctors of India
								in this field. We can give you more and better options so that
								you can take the right decisions. We also assist you in taking
								best decision based on your requirement. Arvene Healthcare will
								also arrange for your stay and local travel, so that you have
								nothing else to worry about but your treatment. Please contact
								us on <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a> today to avail best services.</p>

							<h3>
								<span>What is </span>Cancer?
							</h3>
							<div class="row">
								<div class="col-md-9 col-sm-9 nopaddleft">
									<p>Uncontrollable and abnormal growth of cells that are
										capable to invade other body tissues is termed as Cancer. The
										cancer cells can spread to the different body parts by the
										means of lymph systems and blood. There are more than 100
										types of cancers are there that are named after the type of
										cell or organ from where they start.</p>
									<p>The types of cancer can be categorized into different
										groups.</p>
								</div>
								<!-- End Inner Column -->
								<div class="col-md-3 col-sm-3 nopaddright">
									<img src="images/specialities/17.cancer main.jpg" alt="cancer"
										class="img-responsive">
								</div>
								<!-- End Inner Column -->
							</div>
							<!-- End Inner Row -->
							<ul>
								<li><span><strong>Central Nervous System
											Cancers: </strong>type of cancer starts in the tissues of the spinal
										cord and brain. </span></li>
								<li><span><strong>Carcinoma: </strong> TThis type of
										cancer starts in the tissues or in the skin or can also cover
										internal organs </span></li>
								<li><span><strong>Leukaemia: </strong> This type of
										cancer begins in blood-forming tissue that includes bone
										marrow that produces a large number of abnormal blood cells
										that enter into the blood. </span></li>
								<li><span><strong>Sarcoma: </strong> This cancer
										begins in fat, blood vessels, cartilage, bone, muscle or any
										other supportive or connective tissue </span></li>
								<li><span><strong>Myeloma and Lymphoma: </strong>
										This type of cancers starts from the cells of the immune
										system.</span></li>
							</ul>
							<h3>
								<span>Causes of </span>Cancer
							</h3>
							<p>The disruption of DNA is the initial phase from where the
								cancer starts to begin. This disruption is due to many reasons
								that include use of certain chemicals, diet, reproductive
								history, sun exposure and tobacco. Also few cells will also come
								into precancerous phase that is called as dysplasia. The rest
								left cells will advance into carcinoma in situ state where
								cancer cells are limited to a microscopic site. A tumor takes
								about 30 years to grow and produce sufficient symptoms. It has
								also been seen that not all tumors are cancerous; they are
								either malignant or benign.</p>
							<ul>
								<li><span><strong>Malignant Tumors: </strong> These
										tumors are considered as cancerous and they surround the
										nearby tissues while spreading to different parts of the body.
								</span></li>
								<li><span><strong>Benign Tumors: </strong>: These
										tumors are considered as non-cancerous and can also be
										removed. It has also been seen that after removal they do not
										come back again. The cells in this type of tumors do not
										spread to different parts of the body. </span></li>
							</ul>

							<span>Stages of </span>Cancer
							<p>There are typically four stages of cancer -</p>
							<p>
								<strong>Stage 0: </strong> This stage explains cancer in situ
								that means 'in place'. These cancers do not spread to other
								parts of the body and are located at their place from where they
								have started. In this stage, the cancer is possible to cure by
								completely removing the tumor with the help of a surgery.
							</p>
							<p>
								<strong>Stage I: </strong> In this early stage, the cancer is
								usually small and the tumor has not developed into the
								surrounding tissues. These types of cancers do not spread to
								different parts of the body or to the lymph nodes.
							</p>
							<p>
								<strong>Stage II and III: </strong> These two stages define the
								large size of the tumors. These have grown deep inside the
								surrounding tissues and have spread only to lymph nodes and not
								to the different parts of the body.

							</p>
							<p>
								<strong>Stage IV: </strong> This stage indicates that the cancer
								or tumor has spread to different parts of the body. This stage
								is also known as metastatic or advanced cancer.
							</p>
							<h3>
								<span>Cancer Treatment </span>Options
							</h3>
							<p>A number of options are available for treating cancer.
								These treatments depend on location, stage, type and extent of
								the cancer. The different therapies include -</p>
							<p>
								<strong>Radiation Therapy: </strong> Here Cancer is treated with
								the help of ionizing radiation. The cells and their genetic
								material are destroyed by ionizing radiation in the designated
								area. This therapy makes it impossible for these cells to grow
								further.

							</p>
							<p>
								<strong>Hormone Therapy: </strong> In this therapy, the hormones
								are used for changing the way these hormones grow in the body
								that helps cancers to develop.
							</p>
							<p>
								<strong>Chemotherapy: </strong> Anti-cancer drug cells are used
								in chemotherapy. These drugs help in destroying the cancer cells
								by stopping multiplication or growth. These drugs are given by
								injecting into a muscle, applied to the skin, by mouth (orally)
								or into the vein (intravenously). This depends on the drug and
								also on the cancer type.

							</p>
							<p>
								<strong>Biological Therapy (Immunotherapy): </strong> This
								therapy indirectly or directly uses the immune system of the
								body in order to fight cancer.

							</p>
							<p>
								<strong>Surgery: </strong> Here, surgical removal of the tumor
								is done. Also lymph nodes and nearby tissue are also removed by
								the means of surgery. This surgical procedure can either use
								laser or conventional instruments.
							</p>

							<h3>
								<span>Cancer </span>Recurrence
							</h3>
							<p>The return of the cancer after its treatment and after a
								time period is known as cancer recurrence. The same cancer can
								come back again from where it has started to develop and it can
								also develop from a different part of the body. The reason of
								this recurrence is due to few cancer cells that have been left
								in the body and the treatment was unable to get completely rid
								of them.</p>
							<h3>
								<span>Cancer Treatment </span>Aftercare
							</h3>
							<p>After the cancer treatment, it is essential to drink clean
								water, minimize stress and eat right food and regular check-ups
								with your doctor. Aftercare also involves the review of physical
								exam and medical history of a patient that include endoscopy,
								imaging procedures and blood work. This aftercare is very
								important as it helps in determining the changes in health of a
								patient. Plus it also checks psychosocial and physical effects
								that may have developed after the completion of the treatment.</p>
							<h5>
								To get best options for cancer treatment, send us a query or
								write to us at <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
							</h5>
						</div>
						<!-- End Tab -->

					</div>
				</div>
				<!-- End Column -->
			</div>
			<!-- End Row -->
		</div>
		<!-- End Tab Accordion -->

	</div>
	<!-- End Container -->
</div>
<!-- Speciality Content Ends Here -->

<%@ include file="newFooter.jsp"%>
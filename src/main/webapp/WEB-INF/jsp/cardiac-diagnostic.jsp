<%@ include file="newHeader.jsp"%></section>
<!-- Hospital Short Details Starts Here -->
<div class="short-details">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="title">
					<h1>Cardiology</h1>
					<ul class="paginator">
						<li><a href="/">Home</a></li>
						<li><a>Cardiology</a></li>
					</ul>
				</div>
			</div>
			<!-- End Column -->
		</div>
		<!-- End Row -->
	</div>
	<!-- End Container -->
</div>
<!-- Hospital Short Details Ends Here -->

<!-- Specialty Content Starts Here -->
<div class="speciality-content-area">
	<div class="container">

		<div class="innertab-accordion">
			<div class="row">
				<div class="col-md-3 tabnopaddright">
					<div class="tabs">
						<ul>
							<li><a href="cardiology-treatment">Cardiology</a></li>
							<li><a href="coronary-angiography">Coronary Angiography</a></li>
							<li><a href="coronary-angioplasty">Coronary Angioplasty</a></li>
							<li><a href="coronary-artery-bypass-surgery">Coronary
									Artery Bypass</a></li>
							<li><a href="paediatric-cardiac-surgery">Paediatric
									Cardiac Surgery</a></li>
							<li><a href="pacemaker-implantation">Pacemaker
									Implantation</a></li>
							<li><a href="vascular-surgery">Vascular Surgery</a></li>
							<li><a href="heart-valve-replacement-surgery">Heart
									Valve Replacement </a></li>
							<li class="tab-active"><a href="cardiac-diagnostic">Cardiac Diagnostics</a></li>
						</ul>
					</div>
				</div>
				<!-- End Column -->
				<div class="col-md-9 tabnopaddleft">
					<div class="accordion-contentarea">
						<div id="tab9" >
							<h2>
								<span>Cardiac Diagnostic</span> Services in India
							</h2>

							<div class="row">
								<div class="col-md-9 col-sm-9 nopaddleft">
									<p>We at Arvene Healthcare have the expertise to organize
										for all treatments and surgeries related to Cardiology. Arvene
										Healthcare is associated with top hospitals and Doctors of
										India in this field. We can give you more and better options
										so that you can take the right decisions. We also assist you
										in taking best decision based on your requirement. Arvene
										Healthcare will also arrange for your stay and local travel,
										so that you have nothing else to worry about but your
										treatment. Please contact us on <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
										today to avail best services.</p>
									<p>Cardiology is a disease associated with life style.
										Modern and sedentary lifestyle have made man prone to various
										heart and other lifestyle ailments. It has therefore advisable
										to undergo regular preventive health check-ups as a
										precautionary measure.</p>

									<h4>Today there are a number of Cardiac Diagnostic
										Services available:</h4>
									<h3>
										<span>64 Slice</span> Heart Scan
									</h3>
								</div>
								<!-- End Inner Column -->
								<div class="col-md-3 col-sm-3 nopaddright">
									<img src="images/specialities/8.Cardiac diagnosis.jpg"
										alt="Cardiac diagnosis" class="img-responsive">
								</div>
								<!-- End Inner Column -->
							</div>
							<!-- End Inner Row -->
							<p>64 Slice Heart Scan, is a 10 second non-surgical 64 Slice
								Heart Scan, is a non-surgical procedure that reveals the blocks
								and deposits in the arteries of the heart. It is a preventive
								diagnosis that helps in the detection of cardiac disorder and
								can help prevent a heart attack. The procedure is also patient
								friendly and reduces patient discomfort while scanning.</p>

							<h4>Holter Monitor Studies and Event Monitors</h4>

							<h3>
								<span>A Holter</span> heart monitor test
							</h3>
							<p>A Holter heart monitor is a small EKG machine. At the time
								of the test EKG patches and wires are attached to patient's
								chest. Heart beat is monitored (while doing daily activities)
								for a day or two while wearing a monitor in a sling across the
								shoulder under your clothes.</p>

							<h3>
								<span>An event</span> heart monitor
							</h3>
							<p>It is another type of portable monitor. It continuously
								records the action of heart when a button is pushed, it saves or
								records the readings of the recent three minutes. It is worn for
								two days to two weeks. These recordings are transmitted to the
								phone.</p>



							<h3>
								<span>Transesophageal</span> Echocardiography (TEE)
							</h3>
							<p>TEE is used to determine whether a patient with abnormal
								heart rhythm is at a high risk for a stroke. This process also
								tells whether there are any blood clots in the heart.</p>

							<h3>
								<span>Stress</span> Echocardiography
							</h3>
							<p>In Stress Echocardiography heart is made to exercise for
								making it work harder than when it is resting. It helps in
								obtaining detailed pictures of the heart and its functioning. It
								looks into the overall heart functioning, the presence of heart
								diseases, and the effectiveness of the treatment being followed.
							</p>

							<h3>
								<span>Electrophysiology</span> (EP) Study
							</h3>
							<p>EP Study is a specialized process that is undertaken by a
								Electrophysiologist. During this process, one or more thin,
								flexible wires, which are known as catheters, are put into a
								blood vessel, later guiding into the heart. Every catheter has
								got two or more electrodes for measuring the electrical signals
								to heart while they are travelling from one chamber to the
								other.</p>
							<h5>
								To get best options for cardiology treatment, send us a query or
								write to us at <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
							</h5>
						</div>
						<!-- End Tab -->

					</div>
				</div>
				<!-- End Column -->
			</div>
			<!-- End Row -->
		</div>
		<!-- End Tab Accordion -->

	</div>
	<!-- End Container -->
</div>
<!-- Speciality Content Ends Here -->

<%@ include file="newFooter.jsp"%>
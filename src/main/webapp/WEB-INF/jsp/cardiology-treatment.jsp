<%@ include file="newHeader.jsp"%></section>
	<!-- Hospital Short Details Starts Here -->
	<div class="short-details">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="title">
						<h1>Cardiology</h1>
						<ul class="paginator">
							<li><a href="/">Home</a></li>
							<li><a>Cardiology</a></li>
						</ul>
					</div>
				</div>
				<!-- End Column -->
			</div>
			<!-- End Row -->
		</div>
		<!-- End Container -->
	</div>
	<!-- Hospital Short Details Ends Here -->

	<!-- Specialty Content Starts Here -->
	<div class="speciality-content-area">
		<div class="container">

			<div class="innertab-accordion">
				<div class="row">
					<div class="col-md-3 tabnopaddright">
						<div class="tabs">
							<ul>
								<li class="tab-active"><a href="cardiology-treatment">Cardiology</a></li>
								<li><a  href="coronary-angiography">Coronary Angiography</a></li>
								<li><a href="coronary-angioplasty">Coronary Angioplasty</a></li>
								<li><a href="coronary-artery-bypass-surgery">Coronary Artery
										Bypass</a></li>
								<li><a href="paediatric-cardiac-surgery">Paediatric Cardiac
										Surgery</a></li>
								<li><a href="pacemaker-implantation">Pacemaker
										Implantation</a></li>
								<li><a href="vascular-surgery">Vascular Surgery</a></li>
								<li><a href="heart-valve-replacement-surgery">Heart Valve
										Replacement </a></li>
								<li><a href="cardiac-diagnostic">Cardiac Diagnostics</a></li>
							</ul>
						</div>
					</div>
					<!-- End Column -->
					<div class="col-md-9 tabnopaddleft">
						<div class="accordion-contentarea">

							<div id="tab1" class="visible">
								<h2>
									<span>Cardiology Treatments</span> in India
								</h2>
								<p>
We at Arvene Healthcare have the expertise to organize for all treatments and surgeries related to Cardiology. Arvene Healthcare is associated with top hospitals and Doctors of India in this field. We can give you more and better options so that you can take the right decisions. We also assist you in taking best decision based on your requirement. Arvene Healthcare will also arrange for your stay and local travel, so that you have nothing else to worry about but your treatment. Please contact us on <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a> today to avail best services.
</p>
								<h3>
									<span>What is</span> Cardiology?
								</h3>
								<div class="row">
									<div class="col-md-9 col-sm-9 nopaddleft">
										<p>Cardiology is a medical specialitywhich deals in the disorders of human heart. Cardiologist looks into diagnosis and treatments of various heart problems such as heart failure, congenital heart defects, coronary artery disease, Electrophysiology and Valvular heart disease.</p>
										<p>Cardiology treatment in India is at par with the best in the world and that too at affordable cost. The cardiac facilities and technology in Indian hospitals is latest and state of the art. Hence more patients are choosing India to get treatments for their heart diseases. </p>
									</div>	
									<!-- End Inner Column -->
									<div class="col-md-3 col-sm-3 nopaddright">
										<img src="images/specialities/1.cardiology main.jpg" alt="cardiology"
											class="img-responsive">
									</div>
									<!-- End Inner Column -->
								</div>
								<!-- End Inner Row --><p>We, at Arvene Healthcare work in association with a number of top hospitals and cardiologists of the country. These are a number of treatments for which we make arrangements:</p>
										
									

								<h3>
									<span>Non-Invasive</span> Cardiology and Diagnostic
								</h3>
								<p>Non-invasive cardiology and diagnostics looks into the detection and diagnosis of heart disease. There are various tests available for diagnosing the Heart disorders. A patient with history of heart disease or chest pain with reasons that are unknown should take these tests.The non-invasive evaluation can be done through Electrocardiogram (ECG), 2-D Colour Doppler, Peripheral Doppler, TEE Echo, Holter and Stress Echo or Stress Test.
</p>
<h3>
									<span>Interventional</span> Cardiology
								</h3>
								<p>Intervention Cardiology deals with diagnosis and treatment procedures of cardiology wherein procedure involves the insertion of devices (catheter based)percutaneously, into the body of the patient for treating the disease. The procedures include coronary angiography, Balloon Angioplasty, Stent Angioplasty, Carotid Stents, Congenital Heart Defect Correction, Fractional Flow Reserve, Peripheral Atherectomy etc. These are less painful with less risk of infection and patients can recover faster.</p>
								
									
								

								
									<!-- End Inner Column -->
								
								<!-- End Inner Row -->

								<h3>
									<span>Coronary Artery Bypass</span> Graft (CABG)
								</h3>
								<p>Coronary artery bypass grafting (CABG) or heart bypass surgery is a type of surgery that improves blood flow to the heart. Surgeons use CABG to treat people who have severe coronary heart disease (CHD). 
								</p>

								<h3>
									<span>Heart Valve</span> Replacement
								</h3>
								<p>Heart valve replacement is required when the valve of the heart is not functioning properly. The valves may not open or close and in that case a surgery is required. There are various types of valves available for patients depending on age, valve condition etc.
								</p>

								<h3>
									<span>Paediatric Cardiothoracic</span> Surgery
								</h3>
								<p>Paediatric cardiothoracic Surgery is performed for repairing heart defects which occur at the time of birth, also known as congenital heart diseases. Paediatric Cardiac Surgery is critical for the child who has such defects. The defects could be Atrial Septal Defect (ASD), Patent Ductus Arteriosus and Tetralogy of FallotAbnormality(TOF).
								</p>
								<h5>
									To get best options for cardiology treatment, send us a query
									or write to us at <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
								</h5>
							</div>
							<!-- End Tab -->

						</div>
					</div>
					<!-- End Column -->
				</div>
				<!-- End Row -->
			</div>
			<!-- End Tab Accordion -->

		</div>
		<!-- End Container -->
	</div>
	<!-- Speciality Content Ends Here -->

	<%@ include file="newFooter.jsp"%>
<%@ include file="newHeader.jsp"%></section>
<!-- Hospital Short Details Starts Here -->
<div class="short-details">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="title">
					<h1>Cancer</h1>
					<ul class="paginator">
						<li><a href="/">Home</a></li>
						<li><a>Cancer</a></li>
					</ul>
				</div>
			</div>
			<!-- End Column -->
		</div>
		<!-- End Row -->
	</div>
	<!-- End Container -->
</div>
<!-- Hospital Short Details Ends Here -->

<!-- Specialty Content Starts Here -->
<div class="speciality-content-area">
	<div class="container">

		<div class="innertab-accordion">
			<div class="row">
				<div class="col-md-3 tabnopaddright">
					<div class="tabs">
						<ul>
							<li class="tab-active"><a href="cancer-treatment">Cancer</a></li>
							<li><a href="breast-cancer-treatment">Breast Cancer</a></li>
							<li><a href="cervical-cancer-treatment">Cervical Cancer</a></li>
							<li><a href="prostate-cancer-treatment">Postate Cancer</a></li>
							<li><a href="colon-cancer-treatment">Colon Cancer</a></li>
							<li><a href="cyberknife-radiation-therapy">Cyberknife
									Radiation Therapy</a></li>
						</ul>
					</div>
				</div>
				<!-- End Column -->
				<div class="col-md-9 tabnopaddleft">
					<div class="accordion-contentarea">
<div id="tab3">
							<h2>
								<span>Cervical Cancer Treatment</span> in India
							</h2>
							<p>We at Arvene Healthcare have the expertise to organize for
								all treatments and surgeries related to Cervical Cancer. Arvene
								Healthcare is associated with top hospitals and Doctors of India
								in this field. We can give you more and better options so that
								you can take the right decisions. We also assist you in taking
								best decision based on your requirement. Arvene Healthcare will
								also arrange for your stay and local travel, so that you have
								nothing else to worry about but your treatment. Please contact
								us on <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a> today to avail best services.</p>

							<h3>
								<span>What is </span>Cervical Cancer?
							</h3>
							<div class="row">
								<div class="col-md-9 col-sm-9 nopaddleft">
									<p>Cervical cancer is found at the narrow opening in the
										uterus from the vagina known as a cervix. This cancer is most
										common cancer that develops in the cervix of a woman. Cervical
										cancer usually occurs during the midlife. Most women who are
										diagnosed with this cancer are in between 35-55 years of age.
									</p>

								</div>
								<!-- End Inner Column -->
								<div class="col-md-3 col-sm-3 nopaddright">
									<img src="images/specialities/19.cervical cancer.jpg"
										alt="cervical cancer" class="img-responsive">
								</div>
								<!-- End Inner Column -->
							</div>
							<!-- End Inner Row -->


							<span>Causes of </span>Cervical Cancer
							<p>
								<strong>Human Papillomavirus (HPV)</strong> is the most common
								cause of cervical cancer. This virus can easily transmit to
								another person by having a sexual contact with a HPV person. Not
								every type of HPV can cause cervical cancer. Reduction in body's
								natural immune system can also increase the chance of HPV
								infections, thus also increasing the chances of developing
								cervical cancer. Smoking is yet another cause that increases the
								chance of cervical cancer.

							</p>

							<h3>
								<span>Symptoms of </span>Cervical Cancer
							</h3>
							<p>Cervical cancer may not show any symptoms initially. Other
								health conditions can also cause some essential symptoms of
								cervical cancer. Some of the symptoms include -</p>
							<ul>
								<li><span>Excess vaginal discharge</span></li>
								<li><span>Spotting in between periods or unusual
										vaginal bleeding</span></li>
								<li><span>Foul-smelling, watery or clear vaginal
										discharge</span></li>
								<li><span>Bleeding after intercourse or pain during
										sexual intercourse</span></li>
							</ul>
							<p>When tumors invade other body organs then it may result in
								some late symptoms that include -</p>
							<ul>
								<li><span>Constipation</span></li>
								<li><span>Back or pelvic pain</span></li>
								<li><span>Blood in the stool</span></li>
								<li><span>Weight loss</span></li>
								<li><span>Anaemia</span></li>
								<li><span>Urine leakage</span></li>
								<li><span>Shortness of breath</span></li>
								<li><span>Anorexia or appetite loss</span></li>
							</ul>

							<h3>
								<span>Diagnosis of </span>Cervical Cancer
							</h3>

							<ul>
								<li><span>Cone Biopsy </span></li>
								<li><span>Pap smears test.</span></li>
								<li><span><strong>Coloscopy</strong> - The cervix is
										examined under magnification for observing abnormal changes</span></li>


								<li><span>MRI of the pelvis</span></li>
								<li><span>Chest x-ray</span></li>
								<li><span>Intravenous pyelogram (IVP)</span></li>
								<li><span>CT scan of the pelvis</span></li>
								<li><span>MRI of the pelvis</span></li>
								<li><span>Cystoscopy</span></li>
							</ul>
							<h3>
								<span>Treatment of </span>Cervical Cancer
							</h3>
							<p>Health problems and the stage of the cancer help in
								determining a specific treatment for cervical cancer. Some of
								the treatment options include -</p>
							<ul>
								<li><span><strong>Radiation Therapy: </strong> </span></li>
								<li><span><strong>Surgery: </strong> </span></li>
								<li><span><strong>Chemotherapy: </strong> </span></li>
							</ul>
<h5>Cost of Cervical Cancer treatment in India starts at 8500 US Dollars. To know more about it please share your reports on <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a> or whatsapp/viber/imo to +918130077375.</h5><p> 
(Please note this is a tentative plan and actual Costs are based on reports shared. Final treatment plan and estimation will be provided after the patient has been evaluated.)</p>
							<h5>
								To get best options for cancer treatment, send us a query or
								write to us at <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
							</h5>
						</div>
						<!-- End Tab -->
						
					</div>
				</div>
				<!-- End Column -->
			</div>
			<!-- End Row -->
		</div>
		<!-- End Tab Accordion -->

	</div>
	<!-- End Container -->
</div>
<!-- Speciality Content Ends Here -->

<%@ include file="newFooter.jsp"%>
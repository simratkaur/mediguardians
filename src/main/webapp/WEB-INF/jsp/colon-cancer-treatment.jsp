<%@ include file="newHeader.jsp"%></section>
<!-- Hospital Short Details Starts Here -->
<div class="short-details">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="title">
					<h1>Cancer</h1>
					<ul class="paginator">
						<li><a href="/">Home</a></li>
						<li><a>Cancer</a></li>
					</ul>
				</div>
			</div>
			<!-- End Column -->
		</div>
		<!-- End Row -->
	</div>
	<!-- End Container -->
</div>
<!-- Hospital Short Details Ends Here -->

<!-- Specialty Content Starts Here -->
<div class="speciality-content-area">
	<div class="container">

		<div class="innertab-accordion">
			<div class="row">
				<div class="col-md-3 tabnopaddright">
					<div class="tabs">
						<ul>
							<li class="tab-active"><a href="cancer-treatment">Cancer</a></li>
							<li><a href="breast-cancer-treatment">Breast Cancer</a></li>
							<li><a href="cervical-cancer-treatment">Cervical Cancer</a></li>
							<li><a href="prostate-cancer-treatment">Postate Cancer</a></li>
							<li class="tab-active"><a href="colon-cancer-treatment">Colon Cancer</a></li>
							<li><a href="cyberknife-radiation-therapy">Cyberknife
									Radiation Therapy</a></li>
						</ul>
					</div>
				</div>
				<!-- End Column -->
				<div class="col-md-9 tabnopaddleft">
					<div class="accordion-contentarea">
<div id="tab5">
							<h2>
								<span>Colon Cancer Treatment</span> in India
							</h2>
							<p>We at Arvene Healthcare have the expertise to organize for
								all treatments and surgeries related to Colon Cancer. Arvene
								Healthcare is associated with top hospitals and Doctors of India
								in this field. We can give you more and better options so that
								you can take the right decisions. We also assist you in taking
								best decision based on your requirement. Arvene Healthcare will
								also arrange for your stay and local travel, so that you have
								nothing else to worry about but your treatment. Please contact
								us on <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a> today to avail best services.

							</p>
							<h3>
								<span>What is </span>Colon Cancer?
							</h3>
							<div class="row">
								<div class="col-md-9 col-sm-9 nopaddleft">
									<p>Colon cancer, also known as bowel cancer or colorectal
										cancer or cancer of the bowel, is the cancer of large
										intestine (lower part of the digestive system). Majority of
										colon cancer cases start as small and non-cancerous clumps of
										the cells known as adenomatous. After a certain period of
										time, these polyps become colon cancers.</p>
									<h3>
										<span>Types of </span>Colon Cancer
									</h3>
									<p>
										<strong> Adenocarcinoma</strong> is considered as the most
										common type of colon cancer. This type of colon cancer begins
										in the inner layer that spreads deep into the other layers.
									</p>
								</div>
								<!-- End Inner Column -->
								<div class="col-md-3 col-sm-3 nopaddright">
									<img src="images/specialities/21.colon cancer.jpg"
										alt="colon cancer" class="img-responsive">
								</div>
								<!-- End Inner Column -->
							</div>
							<!-- End Inner Row -->


							<p>
								<strong>Gastrointestinal Stromal Tumors : </strong> This is
								considered as a rare type of colon cancer that begins in a cell
								found in the gastrointestinal tract lining known as interstitial
								cells of Cajal (ICCs). These type of tumors are classified as
								sarcomas that include cartilage, fat, bones, blood vessels, deep
								skin tissues, muscle and nerves.
							</p>
							<p>
								<strong>Squamous Cell Carcinomas: </strong> Certain portions of
								the gastrointestinal tract such as the end of the anus and the
								upper part of the oesophagus are lined with squamous cells.
								Squamous cell carcinomas are those cancers that begin in these
								cells.
							</p>

							<p>
								<strong>Gastrointestinal Carcinoid Tumors: </strong> This type
								of colon cancer is considered as a slow-growing cancer that
								develops in the neuroendocrine cell in the GI tract lining.
							</p>

							<p>
								<strong>Primary Colorectal Lymphomas: </strong> The cancers that
								grow in the lymphatic system of cells (lymphocytes) are known as
								lymphomas. Primary colorectal lymphomas develop in the later
								stages of life and are more commonly seen in men as compared to
								women.
							</p>

							<h3>
								<span>Symptoms of </span>Colon Cancer
							</h3>
							<p>The early symptoms of colon cancer include -</p>
							<ul>
								<li><span>Unexplained weight loss</span></li>
								<li><span>Bleeding from the rectum or blood in the
										stools</span></li>
								<li><span>Abdominal pain</span></li>
								<li><span>Change in normal bowel habits </span></li>
							</ul>

							<ul>
								<li><span>Breathlessness</span></li>
								<li><span>Fatigue</span></li>
							</ul>

							<ul>
								<li><span>Vomiting</span></li>
								<li><span>A feeling of bloating, especially near the
										belly button</span></li>
								<li><span>Constipation</span></li>
								<li><span>Abdominal pain</span></li>
							</ul>

							<h3>
								<span>Diagnosis of </span>Colon Cancer
							</h3>
							<p>A number of tests are performed for diagnosing colon
								cancer that include -</p>
							<ul>
								<li><span><strong>Carcinoembryonic Antigen
											(CEA) Assay: </strong> The normal as well as cancer cells release CEA
										in the bloodstream. When these CEA is found in large amounts
										then it indicates colon cancer. </span></li>
								<li><span><strong>Proctoscopy: </strong>: A
										proctoscope (a thin and tube-like instrument having light and
										lens for viewing) is inserted into the rectum in this rectum
										test. </span></li>
								<li><span><strong>Physical Examination and
											History: </strong> This is done for examining the general signs of
										health and also for seeing any indications of disease that may
										include lumps. Proper evaluation of the history of a patient
										is done that include past illnesses and treatments taken.
										span></li>
								<li><span><strong>Colonoscopy: </strong> This test
										is performed for viewing the colon and rectum from within in
										order to see the presence of cancer, polyps or abnormal areas.
								</span></li>
								<li><span><strong>Digital Rectal Exam
											(DRE): </strong> The insertion of the gloved and lubricated finger is
										done into the lower part of the rectum for viewing the
										presence of lumps. </span></li>
								<li><span><strong>Biopsy: </strong> The signs of
										cancer are checked by removing tissues or cells that are then
										examined under a microscope. </span></li>

								<li><span>Positron Emission Tomography Scan (PET)</span></li>
								<li><span>CT scan</span></li>
								<li><span>Endoscopic Ultrasound (EUS)</span></li>
								<li><span>Magnetic Resonance Imaging (MRI)</span></li>
							</ul>
							<h3>
								<span>Treatment of </span>Colon Cancer
							</h3>
							<p>There are six treatment options for colon cancer that
								include -</p>
							<p>
								<strong>Targeted Therapy: </strong> Certain substances and drugs
								are used by the targeted therapy in order to target particular
								cancer cells without causing any harm to nearby normal cells.
								The two types of targeted therapy include -
							</p>
							<ul>
								<li><span><strong>Angiogenesis Inhibitors:
									</strong> This helps in stopping the growth of new blood vessels which
										tumors need to grow.</span></li>
								<li><span><strong>Monoclonal Antibodies: </strong>
										These antibodies are prepared in the laboratory with the help
										of a single type of immune system. Certain substances can be
										identified by these antibodies that are on the cancer cells or
										it also identifies certain normal substances that are helping
										in the growth of cancer cells. These antibodies then attach
										themselves with the substances for destroying cancer cells and
										also prevent them from spreading.</span></li>
							</ul>
							<p>
								<strong>Chemotherapy: </strong> Certain types of drugs are used
								for stopping the growth of cancer cells or for destroying the
								cancer cells. The injection of these drugs is done into a vein
								from where they enter the bloodstream and they then travel
								through the entire body.

							</p>
							<p>
								<strong>Radiation Therapy: </strong> High-energy beams are used
								for destroying cancer cells. Internal radiation and external
								radiation are the two types of radiation therapy.

							</p>
							<p>
								<strong>Surgery: </strong> The surgical procedure involves the
								removal of diseased segment with sufficient margins, surrounding
								lymph nodes and tissue.

							</p>

							<p>Sometimes chemotherapy or radiation therapy is provided
								after surgery in order to shrink the tumor. This makes the
								removal of the tumor easy while simultaneously lessening the
								problems associated with bowel control after surgery.</p>
							<h5>
								To get best options for cancer treatment, send us a query or
								write to us at <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
							</h5>
						</div>
						<!-- End Tab -->

						
					</div>
				</div>
				<!-- End Column -->
			</div>
			<!-- End Row -->
		</div>
		<!-- End Tab Accordion -->

	</div>
	<!-- End Container -->
</div>
<!-- Speciality Content Ends Here -->

<%@ include file="newFooter.jsp"%>
<%@ include file="newHeader.jsp"%></section>
	<!-- Hospital Short Details Starts Here -->
	<div class="short-details">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="title">
						<h1>Cardiology</h1>
						<ul class="paginator">
							<li><a href="/">Home</a></li>
							<li><a>Cardiology</a></li>
						</ul>
					</div>
				</div>
				<!-- End Column -->
			</div>
			<!-- End Row -->
		</div>
		<!-- End Container -->
	</div>
	<!-- Hospital Short Details Ends Here -->

	<!-- Specialty Content Starts Here -->
	<div class="speciality-content-area">
		<div class="container">

			<div class="innertab-accordion">
				<div class="row">
					<div class="col-md-3 tabnopaddright">
						<div class="tabs">
							<ul>
								<li><a href="cardiology-treatment">Cardiology</a></li>
								<li class="tab-active"><a  href="coronary-angiography">Coronary Angiography</a></li>
								<li><a href="coronary-angioplasty">Coronary Angioplasty</a></li>
								<li><a href="coronary-artery-bypass-surgery">Coronary Artery
										Bypass</a></li>
								<li><a href="paediatric-cardiac-surgery">Paediatric Cardiac
										Surgery</a></li>
								<li><a href="pacemaker-implantation">Pacemaker
										Implantation</a></li>
								<li><a href="vascular-surgery">Vascular Surgery</a></li>
								<li><a href="heart-valve-replacement-surgery">Heart Valve
										Replacement </a></li>
								<li><a href="cardiac-diagnostic">Cardiac Diagnostics</a></li>
							</ul>
						</div>
					</div>
					<!-- End Column -->
					<div class="col-md-9 tabnopaddleft">
						<div class="accordion-contentarea">

							<div id="tab2" >
								<h2>
									<span>Coronary Angiography</span> in India
								</h2>
								<p>We at Arvene Healthcare have the expertise to organize for all treatments and surgeries related to Cardiology. Arvene Healthcare is associated with top hospitals and Doctors of India in this field. We can give you more and better options so that you can take the right decisions. We also assist you in taking best decision based on your requirement. Arvene Healthcare will also arrange for your stay and local travel, so that you have nothing else to worry about but your treatment. Please contact us on <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a> today to avail best services.</p>
								
								<h3>
									<span>What is </span>Coronary Angiography?
								</h3>
								<div class="row">
									<div class="col-md-9 col-sm-9 nopaddleft">
										<p>Angiography is a diagnostic procedure by way of medical imaging where an X-Ray is taken of the heart to visualize the arteries, veins and the four chambers of the heart. Angiography requires the insertion of a catheter into the coronary or peripheral artery.
										</p>
									</div>
									<!-- End Inner Column -->
									<div class="col-md-3 col-sm-3 nopaddright">
										<img src="images/specialities/2. coronary Angiography.jpg"
											alt="coronary Angiography" class="img-responsive">
									</div>
									<!-- End Inner Column -->
								</div>
								<!-- End Inner Row -->
								<h3>
									<span>Cardiac Catheterization</span> Procedure
								</h3>
								<p>During Coronary Catheterization, X-Ray images of the blood in the coronary arteries are recorded. An intervention cardiologist guides a catheter through the large blood arteries till the tip of the catheter reaches the opening of the coronary arteries.
								</p>
								<p>Catheters are made of material which has high radio opacity making allowing a clearer, blood compatible X-Ray dye to be selectively injected and mixed with the blood flowing in the artery. The cardiologist activates the equipment to apply a high X-Ray dose to record the diagnostic views which can be saved and studied later.
								</p>
								<h3>
									<span>Non-Invasive </span>Angiography
								</h3>
								<p>Hospitals may also use CT Angiography or Coronary Computed Tomography Angiography, as a non- invasive method to detect blockages in the coronary arteries. 64 slice CT Angio system is most commonly available in most hospitals. 
								</p>
								
								<h3>
									<span>Coronary Cardiac CT Angiography </span>for whom?
								</h3>
								<p>Doctor is the best to select and decide who is appropriate to take CT Angiography. The usage of Coronary CTA is acceptable but there is some risk involved. Your Doctor will do careful selection of the patient to reduce any kind of risk involved.
								</p>
							
							<h3>
									<span>Benefits of </span>Coronary Catheterization
								</h3>
								<p>� There are no remains of radiation in the body of the
									patient after an x-ray examination.</p>
								<p>� Due to angiography, the need of surgery may get
									eliminated. If surgery is necessary then the whole procedure
									becomes accurate.</p>
								<p>� X-rays generally do not have any side effects.</p>
								<h3>
									<span>Post </span>Coronary Angiography 
								</h3>
								<p>After the procedure, the patient is moved to the CCU for few hours of routine monitoring. The overall movements are kept minimum to avoid any bleeding from the puncture area. At the site of recovery, your heart rate and blood pressure are also checked after regular intervals along with any kind of possible bleeding.It is also possible that the puncture site area becomes tender or sore for few days. A small bruise may also appear. 
								</p>
								
								<h5>Cost of Coronary Angiography in India starts at 400 US Dollars. To know more about it please share your reports on <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a> or whatsapp/viber/imo to +918130077375</h5>
								<p>(Please note this is a tentative plan and actual costs are based on reports shared. Final treatment plan and estimation will be provided after the patient has been evaluated.)</p>
								
								<h5>
									To get best options for cardiology treatment, send us a query
									or write to us at <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
								</h5>

							</div>
							<!-- End Tab -->

						</div>
					</div>
					<!-- End Column -->
				</div>
				<!-- End Row -->
			</div>
			<!-- End Tab Accordion -->

		</div>
		<!-- End Container -->
	</div>
	<!-- Speciality Content Ends Here -->

	<%@ include file="newFooter.jsp"%>
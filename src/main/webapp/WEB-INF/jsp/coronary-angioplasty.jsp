<%@ include file="newHeader.jsp"%></section>
<!-- Hospital Short Details Starts Here -->
<div class="short-details">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="title">
					<h1>Cardiology</h1>
					<ul class="paginator">
						<li><a href="/">Home</a></li>
						<li><a>Cardiology</a></li>
					</ul>
				</div>
			</div>
			<!-- End Column -->
		</div>
		<!-- End Row -->
	</div>
	<!-- End Container -->
</div>
<!-- Hospital Short Details Ends Here -->

<!-- Specialty Content Starts Here -->
<div class="speciality-content-area">
	<div class="container">

		<div class="innertab-accordion">
			<div class="row">
				<div class="col-md-3 tabnopaddright">
					<div class="tabs">
						<ul>
							<li><a href="cardiology-treatment">Cardiology</a></li>
							<li><a href="coronary-angiography">Coronary Angiography</a></li>
							<li class="tab-active"><a href="coronary-angioplasty">Coronary Angioplasty</a></li>
							<li><a href="coronary-artery-bypass-surgery">Coronary
									Artery Bypass</a></li>
							<li><a href="paediatric-cardiac-surgery">Paediatric
									Cardiac Surgery</a></li>
							<li><a href="pacemaker-implantation">Pacemaker
									Implantation</a></li>
							<li><a href="vascular-surgery">Vascular Surgery</a></li>
							<li><a href="heart-valve-replacement-surgery">Heart
									Valve Replacement </a></li>
							<li><a href="cardiac-diagnostic">Cardiac Diagnostics</a></li>
						</ul>
					</div>
				</div>
				<!-- End Column -->
				<div class="col-md-9 tabnopaddleft">
					<div class="accordion-contentarea">

						<div id="tab3" >
							<h2>
								<span>Coronary Angioplasty </span>in India
							</h2>
							<p>We at Arvene Healthcare have the expertise to organize for
								all treatments and surgeries related to Cardiology. Arvene
								Healthcare is associated with top hospitals and Doctors of India
								in this field. We can give you more and better options so that
								you can take the right decisions. We also assist you in taking
								best decision based on your requirement. Arvene Healthcare will
								also arrange for your stay and local travel, so that you have
								nothing else to worry about but your treatment. Please contact
								us on <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a> today to avail best services.</p>

							<h3>
								<span>What is </span>Coronary Angioplasty?
							</h3>
							<div class="row">
								<div class="col-md-9 col-sm-9 nopaddleft">
									<p>Angioplasty is a medical procedure which involves
										dilation of a narrowed or obstructed blood vessel caused by
										blockage. All kinds of vascular interventions performed with
										in minimally invasive or percutaneous method are called
										angioplasty.</p>
								</div>
								<!-- End Inner Column -->
								<div class="col-md-3 col-sm-3 nopaddright">
									<img src="images/specialities/3.angioplasty.jpg"
										alt="angioplasty" class="img-responsive">
								</div>
								<!-- End Inner Column -->
							</div>
							<!-- End Inner Row -->
							<h3>
								<span>Coronary Angioplasty </span>Procedure
							</h3>
							<p>Coronary Angioplasty is a percutaneous procedure which
								involves a small puncture to be made in the arm or groin artery.
								The cardiologist passes a thin wire through the puncture
								siteacross the blocked artery. After this a catheter with a
								balloon is passed over the inserted wire to the blocked area.
								When the tube reaches the blockage the balloon is inflated which
								in turn widens the artery the increase the blood flow. In some
								cases plaque removers or thrombus extraction devices may also be
								used. Devices like stents are placed to keep the artery open.</p>
							<h3>
								<span>Duration of C</span>Coronary Angioplasty procedure

							</h3>
							<p>Percutaneous coronary intervention usually takes about 2
								hours. During the procedure, the patient is kept awake but feels
								drowsy due to the effect of medication given to him.</p>
							<h3>
								<span>Coronary Angioplasty </span>Facilities
							</h3>
							<p>
								The Cardiologists at Hospitals have pioneered <strong>Coronary
									Artery Stenting in India</strong>. The latest advancement in Coronary
								Angioplasty in the form of <strong>Fractional Flow
									Reserve (FFR)</strong> or Optical Coherence Tomography (OCT) is
								available in India too.

							</p>

							<h3>
								<span>Benefits of </span>Coronary Angioplasty
							</h3>
							<p>Coronary Angioplasty increases the flow of blood to the
								heart by widening the narrow arteries, which in turn decreases
								the risk of a heart attack and reduces the symptoms of angina.</p>
							<h3>
								<span>Post </span>Coronary Angioplasty
							</h3>
							<ul>
								<li><span> After procedure patient gets relief from
										chest pain improving capacity of exercising. </span></li>
								<li><span> Although the procedure treats the
										condition, but does not completely cure. The vessels may show
										narrowing again over a span of 6 months to one year. A repeat
										procedure may be required. </span></li>
								<li><span> Patient should bring life style changes
										in terms of eating, exercise, give up smoking, and reduce
										taking stress so that the chances of re-procedure is minimal.
										The Doctor will prescribe few medicines which should be taken
										regularly without fail. </span></li>
								<li><span>In case it is not possible to widen the
										arteries even after procedure, because of severity of case,
										then heart surgery (CABG surgery) might be recommended. </span></li>

							</ul>
<h5>Cost of Coronary Angioplasty in India starts at 4400 US Dollars. To know more about it please share your reports on <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a> or whatsapp/viber/imo to +918130077375.</h5><p> 
(Please note this is a tentative plan and actual Costs are based on reports shared. Final treatment plan and estimation will be provided after the patient has been evaluated.)</p>

							<h5>
								To get best options for cardiology treatment, send us a query or
								write to us at <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
							</h5>
						</div>
						<!-- End Tab -->

					</div>
				</div>
				<!-- End Column -->
			</div>
			<!-- End Row -->
		</div>
		<!-- End Tab Accordion -->

	</div>
	<!-- End Container -->
</div>
<!-- Speciality Content Ends Here -->

<%@ include file="newFooter.jsp"%>
<%@ include file="newHeader.jsp"%></section>
<!-- Hospital Short Details Starts Here -->
<div class="short-details">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="title">
					<h1>Cardiology</h1>
					<ul class="paginator">
						<li><a href="/">Home</a></li>
						<li><a>Cardiology</a></li>
					</ul>
				</div>
			</div>
			<!-- End Column -->
		</div>
		<!-- End Row -->
	</div>
	<!-- End Container -->
</div>
<!-- Hospital Short Details Ends Here -->

<!-- Specialty Content Starts Here -->
<div class="speciality-content-area">
	<div class="container">

		<div class="innertab-accordion">
			<div class="row">
				<div class="col-md-3 tabnopaddright">
					<div class="tabs">
						<ul>
							<li><a href="cardiology-treatment">Cardiology</a></li>
							<li><a href="coronary-angiography">Coronary Angiography</a></li>
							<li><a href="coronary-angioplasty">Coronary Angioplasty</a></li>
							<li class="tab-active"><a href="coronary-artery-bypass-surgery">Coronary
									Artery Bypass</a></li>
							<li><a href="paediatric-cardiac-surgery">Paediatric
									Cardiac Surgery</a></li>
							<li><a href="pacemaker-implantation">Pacemaker
									Implantation</a></li>
							<li><a href="vascular-surgery">Vascular Surgery</a></li>
							<li><a href="heart-valve-replacement-surgery">Heart
									Valve Replacement </a></li>
							<li><a href="cardiac-diagnostic">Cardiac Diagnostics</a></li>
						</ul>
					</div>
				</div>
				<!-- End Column -->
				<div class="col-md-9 tabnopaddleft">
					<div class="accordion-contentarea">

						<div id="tab4" >
							<h2>
								<span>Coronary Artery Bypass (Heart Bypass) Surgery</span> in
								India
							</h2>
							<p>We at Arvene Healthcare have the expertise to organize for
								all treatments and surgeries related to Cardiology. Arvene
								Healthcare is associated with top hospitals and Doctors of India
								in this field. We can give you more and better options so that
								you can take the right decisions. We also assist you in taking
								best decision based on your requirement. Arvene Healthcare will
								also arrange for your stay and local travel, so that you have
								nothing else to worry about but your treatment. Please contact
								us on <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a> today to avail best services.</p>

							<h3>Coronary Artery Bypass Graft Surgery:</h3>
							<div class="row">
								<div class="col-md-9 col-sm-9 nopaddleft">
									<p>Coronary Artery Bypass Graft Surgery (CABG) is a
										surgical method performed to improve the flow of blood to the
										heart muscle. Because of accumulation of fatty substances and
										cholesterol over a period of time, the blood vessels may
										narrow and can cause a heart attack. The surgery helps in
										re-directing blood to the heart muscles, giving relief from
										angina (chest pain) and improves heart function.</p>
								</div>
								<!-- End Inner Column -->
								<div class="col-md-3 col-sm-3 nopaddright">
									<img src="images/specialities/4.Cardiac bypass surgery.jpg"
										alt="Cardiac bypass surgery" class="img-responsive">
								</div>
								<!-- End Inner Column -->
							</div>
							<!-- End Inner Row -->
							<h3>
								<span>Post </span>Coronary Artery Bypass Graft Surgery:

							</h3>
							<p>Post-surgery, patients are kept in the intensive care unit
								(ICU) for a short period of time (1 to 2 days) for:</p>

							<ul>
								<li><span> Monitoring the functioning of the heart
										muscles </span></li>
								<li><span>Monitoring the flow of blood &amp; oxygen
										in the heart.</span></li>
								<li><span>A catheter is inserted to drain the
										bladder and measure urine output</span></li>
								<li><span>A catheter is directly placed into the
										artery to measure blood pressure</span></li>
								<li><span> A tube is inserted into the chest to
										drain the fluid &amp; blood</span></li>
								<li><span>Patient may get discharged in 4-5 days</span></li>
								<li><span>Total recovery may take between 6 to 12
										weeks including home care &amp; regular follow-ups with the
										doctor. </span></li>
							</ul>

							<h3>
								<span>Benefits of </span>Coronary Artery Bypass Graft Surgery
							</h3>

							<ul>
								<li><span>Lower risk of stroke</span></li>
								<li><span>Lower death rate</span></li>
								<li><span>Less need for transfusion</span></li>
								<li><span>Less heart rhythm problems</span></li>
								<li><span>Less injury to the heart</span></li>
							</ul>
							<h3>
								<span>Procedure for </span>Coronary Bypass Surgery
							</h3>
							<p>During a Coronary Artery Bypass Graft Surgery (CABG), a
								blood vessel is detached from the chest wall, arms or leg veins.
								Thereafter, the new artery is grafted to the clogged area of the
								coronary artery. Through this new vessel blood flows easily to
								the heart muscles. This procedure is known as Coronary Artery
								Bypass Surgery. A patient may undergo more than one bypass graft
								if need be.</p>

							<h3>
								<span>Duration of </span>Coronary Artery Bypass Surgery
							</h3>
							<p>A Coronary Artery Bypass Graft Surgery (CABG) procedure
								generally takes 3 to 5 hours but it can vary depending upon the
								number of bypass grafts required.</p>
								<h5>Cost of Coronary Artery Bypass surgery in India starts at 5200 US Dollars. To know more about it please share your reports on <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a> or whatsapp/viber/imo to +918130077375.</h5><p> (Please note this is a tentative plan and actual Costs are based on reports shared. Final treatment plan and estimation will be provided after the patient has been evaluated.)</p>
							<h5>
								To get best options for cardiology treatment, send us a query or
								write to us at <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
							</h5>
						</div>
						<!-- End Tab -->

					</div>
				</div>
				<!-- End Column -->
			</div>
			<!-- End Row -->
		</div>
		<!-- End Tab Accordion -->

	</div>
	<!-- End Container -->
</div>
<!-- Speciality Content Ends Here -->

<%@ include file="newFooter.jsp"%>
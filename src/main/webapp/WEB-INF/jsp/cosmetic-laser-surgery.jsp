<%@ include file="newHeader.jsp"%></section>
<!-- Hospital Short Details Starts Here -->
<div class="short-details">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="title">
					<h1>Cosmetic Surgery</h1>
					<ul class="paginator">
						<li><a href="/">Home</a></li>
						<li><a>Cosmetic Surgery</a></li>
					</ul>
				</div>
			</div>
			<!-- End Column -->
		</div>
		<!-- End Row -->
	</div>
	<!-- End Container -->
</div>
<!-- Hospital Short Details Ends Here -->

<!-- Specialty Content Starts Here -->
<div class="speciality-content-area">
	<div class="container">

		<div class="innertab-accordion">
			<div class="row">
				<div class="col-md-3 tabnopaddright">
					<div class="tabs">
						<ul>
							<li class="tab-active"><a href="cosmetic-surgery">Cosmetic Surgery</a></li>
							<li><a href="breast-augmentation-surgery">Breast
									Augmentation</a></li>
							<li><a href="breast-reduction-surgery">Breast Reduction</a></li>
							<li><a href="liposuction-surgery">Liposuction</a></li>
							<li><a href="facelift-surgery">Facelift</a></li>
							<li><a href="rhinoplasty-surgery">Rhinoplasty</a></li>
							<li class="tab-active"><a href="cosmetic-laser-surgery">Cosmetic Laser
									Surgery</a></li>
							<li><a href="oral-maxillofacial-surgery">Maxillofacial
									Surgery</a></li>
							<li><a href="surgical-hair-transplant">Surgical Hair
									Transplant</a></li>
						</ul>
					</div>
				</div>
				<!-- End Column -->
				<div class="col-md-9 tabnopaddleft">
					<div class="accordion-contentarea">

						<div id="tab7">
							<h2>
								<span>Cosmetic Laser Surgery</span> in India
							</h2>
							<p>We at Arvene Healthcare have the expertise to organize for
								all treatments and surgeries related to Rhinoplasty Surgery.
								Arvene Healthcare is associated with top hospitals and Doctors
								of India in this field. We can give you more and better options
								so that you can take the right decisions. We also assist you in
								taking best decision based on your requirement. Arvene
								Healthcare will also arrange for your stay and local travel, so
								that you have nothing else to worry about but your treatment.
								Please contact us on <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a> today to avail
								best services.</p>

							<h3>
								<span>What is </span>Cosmetic Laser Surgery?
							</h3>
							<div class="row">
								<div class="col-md-9 col-sm-9 nopaddleft">
									<p>Cosmetic treatments are now increasingly performed by
										using laser technology. Cosmetic laser surgery is used for
										treating acne scars, treatment of vascular lesions, skin
										resurfacing for wrinkle reduction, precancerous lesions,
										tattoos, pigmented blemishes such as moles and age spots and
										also for hair removal.</p>
								</div>
								<!-- End Inner Column -->
								<div class="col-md-3 col-sm-3 nopaddright">
									<img src="images/specialities/14.cosmetic laser surgery.jpg"
										alt="cosmetic laser surgery" class="img-responsive">
								</div>
								<!-- End Inner Column -->
							</div>
							<!-- End Inner Row -->


							<p>Lasers (Light Amplification by Stimulated Emission of
								Radiation) generate a coherent beam and emits one specific beam
								of wavelength. The lasers are specifically directed towards
								tissue of different types so as to produce a strong heat. This
								strong heat works by destroying the tissues without disturbing
								the surrounding region. The intensity and wavelength of lasers
								depend on the different treatments.</p>
							<h3>
								<span>Types of </span>Cosmetic Lasers
							</h3>
							<p>Here are different types of lasers having a particular
								wavelength, penetration and utility range.</p>
							<p>
								<strong>Pulse Dye Laser: </strong> Vascular Lesions are treated
								with the help of this laser. Telangiectasia, blushing and vein
								can be treated by using long pulsed dye laser.

							</p>
							<p>
								<strong>Ruby Laser: </strong> : Ruby lasers are very effective
								in eliminating tattoos, freckles, peri orbital pigmentation,
								nevi and caf�-au-lait spots, brown pigmented disorders like
								actininclengtigenes and dilated blue veins.

							</p>
							<p>
								<strong>Co2 Laser Surgery: </strong> This type of laser treats
								fine lines, actinic chelitis, acne scars, sun damage, chicken
								pox scars, wrinkles and rhinophyma.

							</p>
							<p>
								<strong>Diode Laser: </strong>This type of laser is ideal for
								treating dark skin and for leg veins. In both these treatments
								long pulsed diode laser is used.

							</p>
							<p>
								<strong>Erbium YAG Laser or ND-YAG Laser: </strong> This laser
								is used to treat acne scars (resurfacing), blue tattoo removal,
								wrinkles, dermal pigmented lesions, professional tattoos and
								precancerous lesions.
							</p>
							<p>
								<strong>Argon Laser: </strong> The laser is used for treating
								skin diseases like acne rosacea, fine veins, spider nevi,
								Hemangiomas and A-V malformations.
							</p>
							<p>
								<strong>Alexandrite Laser: </strong> With the help of rapid
								pulsed (alexandrite laser) benign pigmented lesions and tattoos
								can be removed. Hair can be removed by using long pulsed
								(alexandrite laser).
							</p>
							<h3>
								<span>Cosmetic </span>Laser Treatments
							</h3>
							<p>
								<strong>Facial Laser Hair Removal: </strong> This cosmetic laser
								procedure focuses on hair roots and melanin. The laser heats the
								tissue which has resulted in hair growth. With intense heat, the
								laser breaks down the cells of the tissues. The aim is to focus
								on follicles only.

							</p>
							<p>
								<strong>Laser Surgery for Acne: </strong> : The target of the
								blue spectrum light is to hit the acne prone area. This light
								goes through the outer dermal tissue into the tissues affected
								by bacteria.

							</p>
							<p>
								<strong>Laser Skin Resurfacing: </strong> : The purpose of this
								surgery is to stimulate the growth of natural collagen and also
								eliminates dermal tissue outer layers to get a softer and
								vibrant skin. The facial problems that can be treated include
								Improving Complexion, Treats acne and chicken pox scars, Treats
								tanned and aged skin, Treats wrinkles surrounding the eyes,
								mouth and forehead, Treats birth marks, Treats large oil glands
								on the nose, Treats warts.
							</p>

							<p>
								<strong>Age Spot Removal: </strong> The aim of this surgery is
								to reduce the brown spots on the skin due to over sun exposure
								and ageing. When the laser penetrates in the skin, the
								pigmentation tissue breaks and results in removal of brown
								spots.

							</p>
							<p>
								<strong>Rosacea- </strong> - The tiny red blood vessels grows
								and break causing spots in the nose and the cheeks area. These
								are commonly known as adult acne. The spots can be treated and
								can be rectified by the laser ray.

							</p>
							<p>
								<strong>Spider Veins- </strong> The spider veins can be found
								during pregnancy or in the bodies of the overweight person.
								These veins can be closed the laser therapy and that results to
								the permanent cure of the problem.

							</p>

							<p>
								<strong>Laser Surgery Wrinkles- </strong> By the laser rays the
								wrinkles can be removed. You can achieve a plain and tight skin
								which will make you to look young and fresh.

							</p>
							<p>
								<strong>Eyelid surgery- </strong> The irregular lid drooping,
								overhanging upper lids can be treated by the rays. You can have
								proper eyes and the eyelids which will not only give you an
								obstacle less view but confidence.

							</p>
							<p>
								<strong>Laser Scar Removal: </strong> Intensity of the light on
								the deep tissue penetrates the growth of natural collagen
								production which was earlier disrupted. As a result of which the
								skin gets thick and the scar area looks like the tissues.
							</p>
							<p>
								<strong>Stretch Mark Laser Removal: </strong> It is also
								possible to diminish the stretch marks completely.With the
								emergence of new techniques and therapies, a drastic change can
								be achieved in treating both old and new stretch marks.

							</p>
							<p>
								<strong>Vascular Birthmarks: </strong> With the help of this
								laser treatment, the Telangiectasia (blood vessel) can be
								destroyed without disturbing the nearby tissues. The causes for
								vascular birthmarks can occur due to hormones process, sun
								exposure, rosacea, aging process and medications.

							</p>
							<p>
								<strong>Laser Tattoo Removal: </strong>The laser treatment can
								completely and effectively remove tattoos and also permanent
								makeup. The various wavelengths of the laser target every
								coloured ink of the tattoo. These tattoos absorbs the energy of
								the lasers and separates into little fragments which can be
								easily cleansed.

							</p>
							<p>
								<strong>Laser Eye Surgery: </strong> Excimer Laser Surgery
								(laser eye surgery) can treat problems related to vision like
								astigmatism, long and short sightedness. Laser eye surgery is
								very effective surgery which can permanently correct the vision
								and the patient is not required to wear contact lenses or
								glasses.

							</p>

							<h3>
								<span>Cosmetic Laser Surgery</span> Recovery
							</h3>
							<p>The patient can recover in a few days' time. The patient
								is also advised to follow certain precautions. To avoid
								pigmentation, exposure to sun should be completely avoided.
								Usage of cosmetic products should also be avoided. After some
								days, the patient can get back to its daily routine life.</p>
							<h3>
								<span>Cost of </span>Cosmetic Laser Surgery
							</h3>
							<p>India's medical tourism is famous for its affordable
								cosmetic laser treatments. All the cosmetic clinics in India use
								the latest technologies and provides the best services to its
								patients. Many international patients visit India to get
								cosmetic laser surgery.</p>
							<h5>
								To get best options for cosmetic treatment, send us a query or
								write to us at <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
							</h5>
						</div>
						<!-- End Tab -->
						
					</div>
				</div>
				<!-- End Column -->
			</div>
			<!-- End Row -->
		</div>
		<!-- End Tab Accordion -->

	</div>
	<!-- End Container -->
</div>
<!-- Speciality Content Ends Here -->

<%@ include file="newFooter.jsp"%>
<%@ include file="newHeader.jsp"%></section>
<!-- Hospital Short Details Starts Here -->
<div class="short-details">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="title">
					<h1>Cosmetic Surgery</h1>
					<ul class="paginator">
						<li><a href="/">Home</a></li>
						<li><a>Cosmetic Surgery</a></li>
					</ul>
				</div>
			</div>
			<!-- End Column -->
		</div>
		<!-- End Row -->
	</div>
	<!-- End Container -->
</div>
<!-- Hospital Short Details Ends Here -->

<!-- Specialty Content Starts Here -->
<div class="speciality-content-area">
	<div class="container">

		<div class="innertab-accordion">
			<div class="row">
				<div class="col-md-3 tabnopaddright">
					<div class="tabs">
						<ul>
							<li class="tab-active"><a href="cosmetic-surgery">Cosmetic Surgery</a></li>
							<li><a href="breast-augmentation-surgery">Breast
									Augmentation</a></li>
							<li><a href="breast-reduction-surgery">Breast Reduction</a></li>
							<li><a href="liposuction-surgery">Liposuction</a></li>
							<li><a href="facelift-surgery">Facelift</a></li>
							<li><a href="rhinoplasty-surgery">Rhinoplasty</a></li>
							<li><a href="cosmetic-laser-surgery">Cosmetic Laser
									Surgery</a></li>
							<li><a href="oral-maxillofacial-surgery">Maxillofacial
									Surgery</a></li>
							<li><a href="surgical-hair-transplant">Surgical Hair
									Transplant</a></li>
						</ul>
					</div>
				</div>
				<!-- End Column -->
				<div class="col-md-9 tabnopaddleft">
					<div class="accordion-contentarea">

						<div id="tab1" class="visible">
							<h2>
								<span>Cosmetic Surgery</span> in India
							</h2>
							<p>We at Arvene Healthcare have the expertise to organize for
								all treatments and surgeries related to Cosmetic Surgery. Arvene
								Healthcare is associated with top hospitals and Doctors of India
								in this field. We can give you more and better options so that
								you can take the right decisions. We also assist you in taking
								best decision based on your requirement. Arvene Healthcare will
								also arrange for your stay and local travel, so that you have
								nothing else to worry about but your treatment. Please contact
								us on <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a> today to avail best services.</p>

							<h3>
								<span>What is </span>Cosmetic Surgery?
							</h3>
							<div class="row">
								<div class="col-md-9 col-sm-9 nopaddleft">
									<p>
										<strong>Cosmetic surgery</strong> is focusses on enhancing
										human body appearance such as Breast enlargement, smoothes
										wrinkles or reshape a nose through surgical techniques.
										Cosmetic surgery also includes <strong>reconstructive
											surgery</strong> for a burn or physical trauma. The cosmetic surgery
										in India has become more acceptable and affordable coupled
										with latest and advanced technologies available. The number of
										aesthetic surgery procedures have increased.

									</p>
								</div>
								<!-- End Inner Column -->
								<div class="col-md-3 col-sm-3 nopaddright">
									<img src="images/specialities/8.cosmetic surgery main.jpg"
										alt="cosmetic surgery" class="img-responsive">
								</div>
								<!-- End Inner Column -->
							</div>
							<!-- End Inner Row -->
							<p>
								<strong>Plastic surgery</strong> refers to surgical correction
								or restoration of a form or function. Plastic surgery involves
								both a personís appearance and the ability to function. It is
								concerned to improve patientís appearance and self-image through
								both reconstructive and cosmetic procedures.

							</p>
							<h3>
								<span>Cosmetic Surgery </span>Procedures
							</h3>
							<p>
								<strong>Reconstructive Surgery: </strong> This surgery is
								performed reconstruct the abnormal structures of the body caused
								by congenital defects, trauma, infection, developmental
								abnormalities, tumors or disease. The main purpose is to improve
								function and / or achieve a normal appearance. Some examples
								are: Hand Reduction, Scar revision, Breast reductionetc

							</p>

							<p>
								<strong>Elective Cosmetic Surgery: </strong> : Being unhappy
								about your appearance such as a large nose, wrinkles or fat
								around your belly or hips are some of the common reasons for
								choosing to have cosmetic surgery.

							</p>
							<h3>
								<span>Non-Surgical</span> Cosmetic Procedures
							</h3>
							<ul>
								<li><span>Laser resurfacing to improve acne scars</span></li>
								<li><span>Teeth whitening</span></li>
								<li><span>Botox injection to smooth wrinkles</span></li>

							</ul>
							<h3>
								<span>Types of </span>plastic surgery
							</h3>
							<ul>
								<li><span>Octoplasty (ear surgery)</span></li>
								<li><span>Rhinoplasty ( nose reshaping)</span></li>
								<li><span>Forehead lift</span></li>
								<li><span>Dermabrasion</span></li>
								<li><span>Mastopexy ( breast lift)</span></li>
								<li><span>Abdominoplasty (tummy tuck)</span></li>
								<li><span>Rhytidectomy (face lift )</span></li>
								<li><span>Liposuction</span></li>
								<li><span>Blepharoplasty (eyelid surgery)</span></li>
								<li><span>Octoplasty (ear surgery)</span></li>
								<li><span>Augmentation Mammoplasty (breast
										augmentation)</span></li>
							</ul>

							<h3>
								<span>Technology and Techniques for </span>Plastic and Cosmetic
								Surgeries
							</h3>
							<p>Today most doctors in India are using minimally invasive
								techniques for plastic surgery, such as laser, which reduces the
								recovery time considerably. Most doctors in the cosmetic
								profession offer comprehensive laser treatments for the
								wholesome satisfaction of their clients. Laser treatments in
								India are highly preferred as they are available at affordable
								prices and the procedures assures exceptional results.</p>

							<h3>
								<span>Cost for </span>Cosmetic Surgery in India
							</h3>
							<p>The healthcare facilities in India offer the cosmetic
								surgeries at affordable prices. The cost of cosmetic surgery in
								India is very less expensive as compared to western countries.
								The facilities and technologies are highly advanced in India
								which invites the patients from all over the world.</p>
							<h5>
								To get best options for cosmetic treatment, send us a query or
								write to us at <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
							</h5>
						</div>
						<!-- End Tab -->

					</div>
				</div>
				<!-- End Column -->
			</div>
			<!-- End Row -->
		</div>
		<!-- End Tab Accordion -->

	</div>
	<!-- End Container -->
</div>
<!-- Speciality Content Ends Here -->

<%@ include file="newFooter.jsp"%>
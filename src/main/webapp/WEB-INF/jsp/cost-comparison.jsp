<%@ include file="newHeader.jsp"%></section>
	<!-- Banner Starts Here -->
	<div class="inner-banner">
		<img src="images/banners/sub-banner.jpg" alt="Cost Comparison"
			class="img-responsive">
	</div>
	<!-- Banner Ends Here -->

	<!-- Hospital Short Details Starts Here -->
	<div class="short-details">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="title">
						<h1>Cost Comparison of Medical Treatments</h1>
						<ul class="paginator">
							<li><a href="/">Home</a></li>
							<li><a>Cost Comparison</a></li>
						</ul>
					</div>
				</div>
				<!-- End Column -->
			</div>
			<!-- End Row -->
		</div>
		<!-- End Container -->
	</div>
	<!-- Hospital Short Details Ends Here -->

	<!-- Description Section Starts Here -->
	<div class="description">
		<div class="container">
			<p>
				The heavily subsidized treatment costs are a big reason why people
				are considering travelling abroad to avail the best medical
				facilities at only a fraction of what they have to pay in their home
				countries. Countries like India are able to afford world class
				medical care at hugely discounted rates, thanks to the low cost of
				infrastructure.<br> <br>As per a cost comparison study by
				the American Medical Association, a knee replacement surgery will
				cost<strong> $40,000 in US, $10,000 in Thailand and $13,000
					in Singapore, but $8500 in India</strong>. Similarly, in the US for a bone
				marrow transplant you will have to pay around <strong>$400,000,
					in UK $150,000 but in India it will cost just $30,000</strong>. No matter
				what treatment you opt for, you are going to save around 70-75
				percent of the total cost including health or medical services,
				wellness travel, accommodation and food services. Below is the chart
				for the cost comparison:
			</p>
		</div>
		<!-- End Container -->
	</div>
	<!-- Description Section Ends Here -->

	<!-- Cost comparison Table Starts Here -->
	<div class="cost-compare-section2">
		<div class="container">
			<div class="table-responsive">
				<table class="table table-bordered">
					<thead>
						<tr>
							<th>Procedures</th>
							<th>US ($)</th>
							<th>Costa Rica ($)</th>
							<th>India ($)</th>
							<th>Korea ($)</th>
							<th>Mexico ($)</th>
							<th>Thailand ($)</th>
							<th>Malaysia ($)</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>Heart Bypass</td>
							<td>$144,000</td>
							<td>$25,000</td>
							<td>$5,200</td>
							<td>$28,900</td>
							<td>$27,000</td>
							<td>$15,121</td>
							<td>$11,430</td>
						</tr>
						<tr>
							<td>Angioplasty</td>
							<td>$57,000</td>
							<td>$13,000</td>
							<td>$3,300</td>
							<td>$15,200</td>
							<td>$12,500</td>
							<td>$3,788</td>
							<td>$5,430</td>
						</tr>
						<tr>
							<td>Heart Valve Replacement</td>
							<td>$170,000</td>
							<td>$30,000</td>
							<td>$5,500</td>
							<td>$43,500</td>
							<td>$18,000</td>
							<td>$21,212</td>
							<td>$10,580</td>
						</tr>
						<tr>
							<td>Hip Replacement</td>
							<td>$50,000</td>
							<td>$12,500</td>
							<td>$7,000</td>
							<td>$14,120</td>
							<td>$13,000</td>
							<td>$7,879</td>
							<td>$7,500</td>
						</tr>
						<tr>
							<td>Hip Resurfacing</td>
							<td>$50,000</td>
							<td>$12,500</td>
							<td>$7,000</td>
							<td>$15,600</td>
							<td>$15,000</td>
							<td>$15,152</td>
							<td>$12,350</td>
						</tr>
						<tr>
							<td>Knee Replacement</td>
							<td>$50,000</td>
							<td>$11,500</td>
							<td>$6,200</td>
							<td>$19,800</td>
							<td>$12,000</td>
							<td>$12,297</td>
							<td>$7,000</td>
						</tr>
						<tr>
							<td>Spinal Fusion</td>
							<td>$100,000</td>
							<td>$11,500</td>
							<td>$6,500</td>
							<td>$15,400</td>
							<td>$12,000</td>
							<td>$9,091</td>
							<td>$6,000</td>
						</tr>
						<tr>
							<td>Dental Implant</td>
							<td>$2,800</td>
							<td>$900</td>
							<td>$1,000</td>
							<td>$4,200</td>
							<td>$1,800</td>
							<td>$3,636</td>
							<td>$345</td>
						</tr>
						<tr>
							<td>Lap Band</td>
							<td>$30,000</td>
							<td>$8,500</td>
							<td>$3,000</td>
							<td>N/A</td>
							<td>$6,500</td>
							<td>$11,515</td>
							<td>N/A</td>
						</tr>
						<tr>
							<td>Breast Implants</td>
							<td>$10,000</td>
							<td>$3,800</td>
							<td>$3,500</td>
							<td>$12,500</td>
							<td>$3,500</td>
							<td>$2,727</td>
							<td>N/A</td>
						</tr>
						<tr>
							<td>Rhinoplasty</td>
							<td>$8,000</td>
							<td>$4,500</td>
							<td>$4,000</td>
							<td>$5,000</td>
							<td>$3,500</td>
							<td>$3,901</td>
							<td>$1,293</td>
						</tr>
						<tr>
							<td>Face Lift</td>
							<td>$15,000</td>
							<td>$6,000</td>
							<td>$4,000</td>
							<td>$15,300</td>
							<td>$4,900</td>
							<td>$3,697</td>
							<td>$3,440</td>
						</tr>
						<tr>
							<td>Hysterectomy</td>
							<td>$15,000</td>
							<td>$5,700</td>
							<td>$2,500</td>
							<td>$11,000</td>
							<td>$5,800</td>
							<td>$2,727</td>
							<td>$5,250</td>
						</tr>
						<tr>
							<td>Gastric Sleeve</td>
							<td>$28,700</td>
							<td>$10,500</td>
							<td>$5,000</td>
							<td>N/A</td>
							<td>$9,995</td>
							<td>$13,636</td>
							<td>N/A</td>
						</tr>
						<tr>
							<td>Gastric Bypass</td>
							<td>$32,972</td>
							<td>$12,500</td>
							<td>$5,000</td>
							<td>N/A</td>
							<td>$10,950</td>
							<td>$16,667</td>
							<td>$9,450</td>
						</tr>
						<tr>
							<td>Liposuction</td>
							<td>$9,000</td>
							<td>$3,900</td>
							<td>$2,800</td>
							<td>N/A</td>
							<td>$2,800</td>
							<td>$2,303</td>
							<td>$2,299</td>
						</tr>
						<tr>
							<td>Tummy Tuck</td>
							<td>$9,750</td>
							<td>$5,300</td>
							<td>$3,000</td>
							<td>N/A</td>
							<td>$4,025</td>
							<td>$5,000</td>
							<td>N/A</td>
						</tr>
						<tr>
							<td>Lasik (both eyes)</td>
							<td>$4,400</td>
							<td>$1,800</td>
							<td>$500</td>
							<td>$6,000</td>
							<td>$1,995</td>
							<td>$1,818</td>
							<td>$477</td>
						</tr>
						<tr>
							<td>Cornea (both eyes)</td>
							<td>N/A</td>
							<td>$4,200</td>
							<td>N/A</td>
							<td>$7,000</td>
							<td>N/A</td>
							<td>$1,800</td>
							<td>N/A</td>
						</tr>
						<tr>
							<td>Retina</td>
							<td>N/A</td>
							<td>$4,500</td>
							<td>$850</td>
							<td>$10,200</td>
							<td>$3,500</td>
							<td>$4,242</td>
							<td>$3,000</td>
						</tr>
						<tr>
							<td>IVF Treatment</td>
							<td>N/A</td>
							<td>$2,800</td>
							<td>$3,250</td>
							<td>$2,180</td>
							<td>$3,950</td>
							<td>$9,091</td>
							<td>$3,819</td>
						</tr>
					</tbody>
				</table>
				<!-- End Table -->
			</div>
			<!-- Table Responsive -->
		</div>
		<!-- End Container -->
	</div>
	<!-- Cost comparison Table Ends Here -->

	<!-- About Section Starts Here -->
	<div class="about-section">
		<div class="container">
			<div class="description" style="margin-top: 0px">
				<p>
					The cost of treatment in India is much lower, almost three times
					less expensive in comparison to a number of western countries such
					as the United States and United Kingdom. Just opt for health
					tourism services through arveneHealthcare.com and get the best of
					healthcare services. We, at MediGuradians have been offering
					excellent assistance to foreign patients who visit India to avail
					advanced surgery treatments at low cost operations. Lower cost does
					not mean any compromise in quality. We are dedicated in offering
					the most advanced treatments in the world at reasonable costs so
					that you can fulfil your dream of leading a healthy life.<br>
					<br> Irrespective of exorbitantly lower costs, the quality of
					health care that you get here includes top-class medicines,
					state-of-art technology, internationally accredited doctors,
					experienced staff, and even excellent food service and world-class
					accommodation options. That is not all. You can even combine your
					medical procedure with a vacation or leisure activities such as
					Ayurveda/Spa services, beach or backwaters holiday, other beauty
					procedures, Yoga or spiritual vacation and so on. In India, you get
					the best of medical, health care and Wellness Travel services and
					that too at absolutely affordable rates.
				</p>
			</div>
		</div>
		<!-- End Container -->
	</div>
	<!-- About Section Ends Here -->

	<%@ include file="newFooter.jsp"%>
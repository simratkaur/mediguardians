<%@ include file="newHeader.jsp"%></section>
<!-- Hospital Short Details Starts Here -->
<div class="short-details">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="title">
					<h1>Cancer</h1>
					<ul class="paginator">
						<li><a href="/">Home</a></li>
						<li><a>Cancer</a></li>
					</ul>
				</div>
			</div>
			<!-- End Column -->
		</div>
		<!-- End Row -->
	</div>
	<!-- End Container -->
</div>
<!-- Hospital Short Details Ends Here -->

<!-- Specialty Content Starts Here -->
<div class="speciality-content-area">
	<div class="container">

		<div class="innertab-accordion">
			<div class="row">
				<div class="col-md-3 tabnopaddright">
					<div class="tabs">
						<ul>
							<li><a href="cancer-treatment">Cancer</a></li>
							<li><a href="breast-cancer-treatment">Breast Cancer</a></li>
							<li><a href="cervical-cancer-treatment">Cervical Cancer</a></li>
							<li><a href="prostate-cancer-treatment">Postate Cancer</a></li>
							<li><a href="colon-cancer-treatment">Colon Cancer</a></li>
							<li class="tab-active"><a href="cyberknife-radiation-therapy">Cyberknife
									Radiation Therapy</a></li>
						</ul>
					</div>
				</div>
				<!-- End Column -->
				<div class="col-md-9 tabnopaddleft">
					<div class="accordion-contentarea">
						
						<div id="tab6">
							<h2>
								<span>Cyberknife Radiation</span> Therapy
							</h2>
							<p>We at Arvene Healthcare have the expertise to organize for
								all treatments and surgeries related to Cyberknife Radiation
								Therapy. Arvene Healthcare is associated with top hospitals and
								Doctors of India in this field. We can give you more and better
								options so that you can take the right decisions. We also assist
								you in taking best decision based on your requirement. Arvene
								Healthcare will also arrange for your stay and local travel, so
								that you have nothing else to worry about but your treatment.
								Please contact us on <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a> today to avail
								best services.</p>
							<h3>
								<span>What is </span>CyberKnife?
							</h3>
							<div class="row">
								<div class="col-md-9 col-sm-9 nopaddleft">
									<p>Cyberknife robotic radiosurgery system is the first
										robotic radiosurgery system in the world that is specifically
										designed to treat tumors located at different parts of the
										body such as prostate, brain, pancreas, liver and lungs is
										known as. This treatment is non-invasive alternative to
										surgery where high doses of radiation are delivered with
										extreme accuracy. These high dose radiation beams can be given
										from any angle by precisely concentrating on the tumor while
										causing minimum damage to the nearby healthy tissue.</p>
								</div>
								<!-- End Inner Column -->
								<div class="col-md-3 col-sm-3 nopaddright">
									<img src="images/specialities/22.Cyber knife.jpg"
										alt="Cyber knife" class="img-responsive">
								</div>
								<!-- End Inner Column -->
							</div>
							<!-- End Inner Row -->
							<h3>
								<span>Types of Conditions Treated by </span>the CyberKnife
								Surgery
							</h3>
							<p>Cyberknife radiation surgery is used for treating both
								cancerous and non-cancerous tumors. Following are some of the
								conditions treated by Cyberknife radiation surgery -</p>
							<ul>
								<li><span>Orbital inflammations, metastatic orbital
										tumors and orbital lymphomas</span></li>
								<li><span>Cancers involving the brain</span></li>
								<li><span>Metastatic liver cancers</span></li>
								<li><span>Trigeminal neuralgia</span></li>
								<li><span>Benign brain tumors</span></li>
								<li><span>Lung cancers</span></li>
								<li><span>Malformations of blood vessels inside the
										brain</span></li>
								<li><span>Pancreatic cancers</span></li>
								<li><span>Benign brain tumors</span></li>
								<li><span>Cancers involving the spine</span></li>
							</ul>
							<h3>
								<span>Benefits of </span>Cyberknife Surgery
							</h3>
							<p>Cyberknife treatment offers many benefits as compared to
								other treatments. This treatment is ideal for those patients who
								are unable to go through a traditional surgery. Some of its
								benefits are -</p>
							<ul>
								<li><span>The tumors can be further treated even if
										they have received maximum allowed dosage of radiation</span></li>
								<li><span>No anaesthesia</span></li>
								<li><span>Completely frameless</span></li>
								<li><span>There is a minimum radiation exposure to
										healthy organs and tissues</span></li>
								<li><span>No pain</span></li>
								<li><span>No hospitalization</span></li>
								<li><span>No bleeding</span></li>
								<li><span>No incisions</span></li>
								<li><span>Immediate return to normal daily routine</span></li>
							</ul>

							<span>When is </span>Cyberknife a Suitable Treatment Option?
							</h3>
							<p>Following are the circumstances that requires Cyberknife
								system -</p>
							<ul>
								<li><span>When a patient does not want to go through
										a traditional surgery</span></li>
								<li><span>When a patient is unable to undergo a
										surgery</span></li>
								<li><span>Recurrence of a tumor close to a
										significant structure like the spine, that previously received
										maximum dose of radiation</span></li>
								<li><span>When surgery is not able to remove the
										entire diseased tissue </span></li>
								<li><span>When tumor is located near to a
										significant structure like optic nerves where performing a
										traditional surgery can increase the risk of damage to these
										structures</span></li>
								<li><span>When a surgical procedure is difficult to
										perform</span></li>
							</ul>
							<h3>
								<span>CyberKnife versus </span>Radiation Therapy
							</h3>
							<p>Radiation therapy has been used from a long time for
								treating malignant and benign tumors. CyberKnife is altogether a
								new and latest concept that is very different from other
								treatment methods. The major benefit of Cyberknife is its
								accurate precision that helps in maximizing the quantity of
								radiation. This radiation reaches to the abnormal growth or
								tumor that minimizes the damage to healthy organs and tissues</p>
							<p>Cyberknife is now available in India at few Hospitals with
								latest technology and at much lower price</p>
<h5>Cost of Cyber knife treatment in India starts at 9000 US Dollars. To know more about it please share your reports on <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a> or whatsapp/viber/imo to +918130077375.</h5><p> 
(Please note this is a tentative plan and actual Costs are based on reports shared. Final treatment plan and estimation will be provided after the patient has been evaluated.)</p>
							<h5>
								To get best options for cancer treatment, send us a query or
								write to us at <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
							</h5>
						</div>
						<!-- End Tab -->

						
					</div>
				</div>
				<!-- End Column -->
			</div>
			<!-- End Row -->
		</div>
		<!-- End Tab Accordion -->

	</div>
	<!-- End Container -->
</div>
<!-- Speciality Content Ends Here -->

<%@ include file="newFooter.jsp"%>
<%@ include file="newHeader.jsp"%></section>
<%-- <%@ include file="specialistIncludes.jsp"%>
<%@ include file="navigator.jsp"%> --%>


<section class="sec-main sec-drl">
	<div class="container">
		<div class="row">
			<div class="doc-aprv col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div>
					<p>Approve from view profile</p>
				</div>
			</div>

			<form:form action='approveDoctor' method="POST" modelAttribute="user"
				enctype="multipart/form-data">

			<input type="hidden" name="id" id="id" value="${user.id}">

				<div class="doc-profile col-lg-12 col-md-12 col-sm-12 col-xs-12">

					<div class="dr-avail col-lg-10 col-md-10 col-sm-10 col-xs-8">

						<div class="table-responsive">
							<table class="table table-striped table-condensed">
								<tbody>
									<!--  <tr>
                                                <td>Title</td>
                                                <td>Sumit Wadhwa</td>
                                            </tr> -->
									<tr>
										<td>Title</td>
										<td>${specialist.title}</td>
									</tr>
									<tr>
										<td>First Name</td>
										<td>${specialist.firstName}</td>
									</tr>
									<tr>
										<td>Last Name</td>
										<td>${specialist.lastName }</td>
									</tr>
									<tr>
										<td>Email Id</td>
										<td>${specialist.user.username}</td>
									</tr>
									<tr>
										<td>Degree</td>
										<td>${specialist.degrees}</td>
									</tr>
									<tr>
										<td>Specialty</td>
										<td>${specialist.speciality.name}</td>
									</tr>
									<tr>
										<td>Hospital</td>
										<td>${specialist.hospital}</td>
									</tr>
									<tr>
										<td>City</td>
										<td>${specialist.hospitalCity}</td>
									</tr>
									<tr>
										<td>Session Fee</td>
										<td>$ ${specialist.sessionFee}</td>
									</tr>
									<tr>
										<td>Query Fee</td>
										<td>$ ${specialist.queryFee}</td>
									</tr>
									<tr>
										<td>Biography</td>
										<td>${specialist.biography }</td>
									</tr>


								</tbody>
							</table>
						</div>

					</div>
					<div
						class="col-lg-2 col-md-2 col-sm-2 col-xs-4 no-padding-right  profile-img">
						<img src="${specialist.profileImage }"
							class="pull-right img-responsive" alt="">
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<button  class="btn btn-lg btn-default" type="submit" value="Approve" name="action">Approve</button>
						<button type="submit" name="action" value="Reject"  class="btn btn-lg btn-default">Reject</button>
						<button type="submit" value="Cancel" name="action" class="btn btn-lg btn-default">Cancel</button>
					</div>
				</div>
			</form:form>

		</div>
	</div>
</section>

<%@ include file="newFooter.jsp"%>

<%@ include file="newHeader.jsp"%></section>
<link rel="stylesheet" href="css/intlTelInput.css">
<script src="js/intlTelInput.js"></script>

<script type="text/javascript">
	window.onload = function() {
		$("#phone").intlTelInput();
	};
</script>

<script>

function updatePhone(){
	
	var ph1 = $("#phone").val();
	var ph = $("#phone").intlTelInput("getSelectedCountryData");
	var completePhone = "+"+ph.dialCode+ph1;		
	 $("#patientPhone").val(completePhone);		
}


(function () {
	
	var selDiv = "";
    
    document.addEventListener("DOMContentLoaded", init, false);
    
    function init() {
        document.querySelector('#newProfileImage').addEventListener('change', handleFileSelect, false);
        selDiv = document.querySelector("#selectedFiles");
    }
        
    function handleFileSelect(e) {
    	
    	console.log(e);
        
        if(!e.target.files || !window.FileReader) return;

        selDiv.innerHTML = "";
        
        var files = e.target.files;
        var filesArr = Array.prototype.slice.call(files);
        
        console.log(filesArr);
        
        
        
        filesArr.forEach(function(f,i) {
            var f = files[i];
            var html = "<li>" + f.name + "</li>";
            selDiv.innerHTML += html;  
        });
        
    }
	
})();

</script>




 <section class="sec-consult sec-main">
            <div class="consult-au-wid">


                <div class="row marg">
                    <div class="col-lg-6 col-md-6 col-sm-4 col-xs-12 pull-right">
                        <div class="consult-right-main">
                            <div class="img-consult">
                                <img src="${query.specialistDetails.profileImage }" />
                            </div>
                            <div class="consult-r-con">
                                <p>Dr. ${query.specialistDetails.firstName }
										${query.specialistDetails.lastName }</p>
                                <p>${query.specialistDetails.degrees}</p>
                                <p>${query.specialistDetails.speciality.name }</p>
                                <p>${query.specialistDetails.hospital}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-8 col-xs-12 pull-left">
                        <div class="consult-left-main">
                            <div class="consult-input">
		                            <form name='queryForm' action='askQuery' method='POST'
										id="queryForm" role="form" modelAttribute="query"
									enctype="multipart/form-data">
								<input type="hidden" name="id" path="id" value="${query.id }"></input>
								<input type="hidden" name="specialist" value="${query.specialist }"></input>
									
                                <div class="row">
                                    
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="sig-input">
                                            <input class="form-control" type="text" placeholder="Name of patient" name="patientName">
                                        </div>
                                    </div>
                                </div>
                                <div class="row ">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="sig-input">
                                            <input class="form-control" type="text" placeholder="Age of patient" name="patientAge">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">                                    
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="sig-input">
                                                <label class="lab-gend">Gender</label>
                                                <label class="radio-inline">
                                                    <input type="radio"  name="patientGender" value="M"><span>Male</span>
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio"  name="patientGender" value="F"><span>Female</span>
                                                </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                         <div class="sig-input">                                    
		                                    <input class="form-control mbllnmbr" type="text" value="" id="phone" name="phone" placeholder="Phone" onchange="updatePhone();">   
		                                    <input id="patientPhone" type="hidden" name="patientPhone" />
		                                                                     
		                                </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                                        <div class="text-up sig-input">
                                            <textarea class="form-control margtop" rows="8" placeholder="Write query"  name="newConversation"></textarea>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <div class="sig-btn u-b">
                                                    <!-- <button class="btn btn-block bg-blue" >Attach query files</button> -->
                                                    <label class="btn btn-block bg-blue"
													style="text-align: center; margin-top: 10px; height: 41px; border-radius: 0;">
													Attach query files<input type="file" name="files"
													id="newProfileImage"  multiple
													data-validation-max-size="2M"
													
													style="width: 50%; display: inherit; opacity: 0; -moz-opacity: 0; filter: progid:DXImageTransform.Microsoft.Alpha(opacity=0)" />
												</label>
                                                </div>
                                            </div>                            
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 help-block">
                                                <span>You can to send the reports at <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a> <br/> or WhatsApp at 
					                                 +91-81300-77375 after submitting your query.<br/>
					                                   In case of multiple files, select all in one go and click on attach files</span><br>
                                            </div>
                                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					                                <div class="sig-btn more">
					                                   <div id="selectedFiles"></div>
					                                </div>
					                            </div> 
                                        </div>  
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <div class="sig-btn">
                                                    <button class="btn btn-block bg-blue" type="submit">Submit</button>
                                                </div>
                                            </div>   
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">

                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
 </section>


<script>
	function toggleAttachfile(shareHistory) {

		if (shareHistory.checked) {
			document.getElementById("attachment").style.display = 'none';
		} else {
			document.getElementById("attachment").style.display = 'block';
		}

	}
	$(document).ready(function() {
		$("#shareHistorydiv").hide();
	});
	function enableShareHistory(dropDown) {
		if (dropDown.value < 0) {
			$("#shareHistorydiv").hide();
		} else
			$("#shareHistorydiv").show();
	}
</script>


<%@ include file="newFooter.jsp"%>
<%@ include file="newHeader.jsp"%></section>
<!-- Hospital Short Details Starts Here -->
<div class="short-details">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="title">
					<h1>Orthopedic Surgery</h1>
					<ul class="paginator">
						<li><a href="/">Home</a></li>
						<li><a>Orthopedic Surgery</a></li>
					</ul>
				</div>
			</div>
			<!-- End Column -->
		</div>
		<!-- End Row -->
	</div>
	<!-- End Container -->
</div>
<!-- Hospital Short Details Ends Here -->

<!-- Specialty Content Starts Here -->
<div class="speciality-content-area">
	<div class="container">

		<div class="innertab-accordion">
			<div class="row">
				<div class="col-md-3 tabnopaddright">
					<div class="tabs">
						<ul>
							<li><a href="orthopaedics-surgery">Orthopedic
									Surgery</a></li>
							<li><a href="knee-replacement-surgery">Knee Replacement</a></li>
							<li><a href="hip-replacement-surgery">Hip Replacement
									surgery </a></li>
							<li><a href="shoulder-surgery">Shoulder Surgery </a></li>
							<li><a href="foot-ankle-surgery">Foot and Ankle surgery</a></li>
							<li><a href="hand-wrist-surgery">Hand Wrist surgery</a></li>
							<li class="tab-active"><a href="elbow-replacement-surgery">Elbow Surgery</a></li>
						</ul>
					</div>
				</div>
				<!-- End Column -->
				<div class="col-md-9 tabnopaddleft">
					<div class="accordion-contentarea">
<div id="tab7">
							<h2>
								<span>Elbow Surgery </span>in India
							</h2>
							<p>We at Arvene Healthcare have the expertise to organize for
								all treatments and surgeries related to Elbow surgery. Arvene
								Healthcare is associated with top hospitals and Doctors of India
								in this field. We can give you more and better options so that
								you can take the right decisions. We also assist you in taking
								best decision based on your requirement. Arvene Healthcare will
								also arrange for your stay and local travel, so that you have
								nothing else to worry about but your treatment. Please contact
								us on <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a> today to avail best services.</p>

							<h3>Elbow Replacement Surgery</h3>
							<div class="row">
								<div class="col-md-9 col-sm-9 nopaddleft">
									<p>The elbow joint connects two bones- humerus (in the
										upper arm) and ulna (in the lower arm).Elbow Joint Replacement
										Surgery is done to replace the bones of the elbow joint with a
										synthetic one. This is common in aged adults if the joints are
										damaged by fracture. Overall success rate is 90%.</p>
									<h3>
										<span>When is </span>Elbow Replacement Surgery needed?
									</h3>
								</div>
								<!-- End Inner Column -->
								<div class="col-md-3 col-sm-3 nopaddright">
									<img src="images/specialities/37.elbow surgery.jpg"
										alt="elbow surgery" class="img-responsive">
								</div>
								<!-- End Inner Column -->
							</div>
							<!-- End Inner Row -->
							<p>When the elbow joint gets damaged to such an extent that
								you are unable to move your arm, Elbow Joint Replacement Surgery
								is recommended. The major cause of elbow joint damage includes:
							</p>
							<ul>
								<li><span>Osteoarthritis</span></li>
								<li><span>Bad results from previous elbow surgery</span></li>
								<li><span>Rheumatoid arthritis</span></li>
								<li><span>Post-traumatic arthritis</span></li>
								<li><span>Badly broken bone in the upper or lower
										arm near the elbow</span></li>
								<li><span>Badly damaged tissues in the elbow</span></li>
								<li><span>Tumor in or around the elbow</span></li>
							</ul>
							<h3>
								<span>Who needs </span>Elbow Replacement Surgery?
							</h3>
							<p>If one is experiencing deep and severe pain in the elbow
								joint, difficulty in pulling heavy stuff, no relief from
								physical therapy and anti-inflammatory medicines, then elbow
								replacement surgery is recommended.</p>
							<h3>
								<span>Types of </span>Elbow Replacement Surgery
							</h3>
							<p>At times replacement of only one portion of the joint is
								required and sometime the entire joint needs to be replaced.
								There are 4 types of Elbow Joint Replacement surgeries
								available.</p>
							<h3>
								<span>Tennis Elbow </span>Replacement Surgery
							</h3>
							<p>Tennis Elbow, is the most common treatment available for
								patients seeking surgical treatment for elbow joint pain. It is
								caused due to small tears of the ligaments that attach forearm
								muscles to the arm bone in the elbow joint.</p>
							<h3>
								<span>Elbow Arthritis</span> Replacement Surgery
							</h3>
							<p>Also known as Elbow debridement, Elbow Arthritis Surgery
								is a type of surgery wherein arthritis causes significant loss
								of movement. All the loose bodies and bone spurs that are
								hindering elbow movement are removed.</p>
							<h3>
								<span>Arthroscopic Elbow </span>Replacement Surgery
							</h3>
							<p>It is a surgical procedure wherein a small camera is
								inserted inside the elbow joint. Small incisions are made over
								the elbow and thereafter instruments are inserted to remove and
								replace damaged parts.</p>
							<h3>
								<span>Total Elbow</span> Replacement
							</h3>
							<p>Total Elbow Replacement is a surgical procedure in which
								the functional mechanics of the joint are restored by removing
								scar tissues, balancing muscles and inserting a prosthetic joint
								in place of the destroyed one.</p>
							<h3>
								<span>Benefits of</span> Elbow Replacement Surgery
							</h3>
							<p>Elbow surgery is one of the most successful joint
								replacement procedure and most patients are happy with the
								result.</p>
							<ul>
								<li><span>It relieves pain and comfort.</span></li>
								<li><span>It improves flexibility.</span></li>
								<li><span>It prevents the risk of disability.</span></li>
								<li><span> One can participate in sports, exercise
										and drive road vehicles.</span></li>
							</ul>
							<h3>
								<span>Cost of </span>Elbow Replacement Surgery
							</h3>
							<p>Indian hospitals have all the state of the art equipment
								to treat the patients of elbow surgery which is at 1/5th cost
								compared to USA.</p>
							<h5>
								To get best options for Orthopaedic treatment, send us a query
								or write to us at <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
							</h5>
						</div>
						<!-- End Tab -->
					</div>
				</div>
				<!-- End Column -->
			</div>
			<!-- End Row -->
		</div>
		<!-- End Tab Accordion -->

	</div>
	<!-- End Container -->
</div>
<!-- Speciality Content Ends Here -->

<%@ include file="newFooter.jsp"%>
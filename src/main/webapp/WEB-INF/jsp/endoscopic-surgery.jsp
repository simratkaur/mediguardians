<%@ include file="newHeader.jsp"%></section>
	<!-- Hospital Short Details Starts Here -->
	<div class="short-details">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="title">
						<h1>Urology Treatment</h1>
						<ul class="paginator">
							<li><a href="/">Home</a></li>
							<li><a>Urology Treatment</a></li>
						</ul>
					</div>
				</div>
				<!-- End Column -->
			</div>
			<!-- End Row -->
		</div>
		<!-- End Container -->
	</div>
	<!-- Hospital Short Details Ends Here -->

	<!-- Specialty Content Starts Here -->
	<div class="speciality-content-area">
		<div class="container">

			<div class="innertab-accordion">
				<div class="row">
					<div class="col-md-3 tabnopaddright">
						<div class="tabs">
							<ul>
								<li><a href="urology-treatment">Urology
										Treatment</a></li>
								<li class="tab-active"><a href="endoscopic-surgery">Endoscopy surgery</a></li>
								<li><a href="TURP-surgery">TURF</a></li>
								<li><a href="radical-prostatectomy-surgery">Prostactomy</a></li>
							</ul>
						</div>
					</div>
					<!-- End Column -->
					<div class="col-md-9 tabnopaddleft">
						<div class="accordion-contentarea">

							<div id="tab2" >
								<h2>
									<span>Endoscopic Surgery </span>� India
								</h2>
								<div class="row">
									<div class="col-md-9 col-sm-9 nopaddleft">
										<p>We at Arvene Healthcare have the expertise to organize for all treatments and surgeries related to Urology. Arvene Healthcare is associated with top hospitals and Doctors of India in this field. We can give you more and better options so that you can take the right decisions. We also assist you in taking best decision based on your requirement. Arvene Healthcare will also arrange for your stay and local travel, so that you have nothing else to worry about but your treatment. Please contact us on <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a> today to avail best services.</p>
									</div>
									<!-- End Inner Column -->
									<div class="col-md-3 col-sm-3 nopaddright">
										<img src="images/specialities/50.Endoscopic surgery.jpg"
											alt="Endoscopic surgery" class="img-responsive">
									</div>
									<!-- End Inner Column -->
								</div>
								<!-- End Inner Row -->
								<h3>
									<span>What is Endoscopic </span>Surgery?
								</h3>
								<p>This minimally invasive procedure makes use of an endoscope (a tiny video camera) and specialized surgical instruments that are attached with the endoscope for operating on kidneys, bladder and ureters.  The design of an endoscope is made in such a way that it can pass from the existing space in the body like urethra. An endoscope in some methods is passed through a very tiny incision in the skin to the organ or for the specific area to be treated. </p>
								<h3>
									<span>Types of </span>Procedures
								</h3>
								<p>
									<strong>Ureteroscopy: </strong> Through this procedure, a proper assessment of the upper urinary tract is done. Ureteroscopy is ideal for diagnosing and treating disorders like kidney stones. 
								</p>
								<p>
									<strong>Cystoscope: </strong> This procedure is performed for those patients who have a urinary problem. A doctor uses a Cystoscope for viewing within the urethra and bladder. Cystoscopy is recommended when a patient is facing following conditions �
								</p>
								<ul>
									<li><span>Stones in the urinary tract</span></li>
									<li><span>Blood in the urine</span></li>
									<li><span>Chronic pelvic pain, painful urination or interstitial cystitis</span></li>
									<li><span>Recurrent urinary tract infections</span></li>
									<li><span>Removal of tumor, cancer or polyp</span></li>
									<li><span>Loss of bladder control</span></li>
									<li><span>Urinary blockage such as stricture, prostate enlargement or narrowing of the urinary tract</span></li>
									<li><span>Unusual cells discovered in the urine sample</span></li>
								</ul>
								<p>
									<strong>Percutaneous Endoscopy: </strong> The surgical procedure performed for removing stones of the kidney by making about tiny incision through the skin is termed as percutaneous nephrolithotomy. 
								</p>
								<p>
									<strong>Endoscopic Surgery for Bladder Cancer: </strong>
									Superficial transitional cell carcinoma (a form of bladder cancer) is effectively treated with the help of transurethral resection (a type of endoscopic surgery). The tumor within the bladder is located by using Cystoscope. 
								</p>
								<p>
									<strong>Endoscopic Surgery for Urinary Stones: </strong> The removal of urinary stones is done by performing endoscopic techniques of percutaneous, Cystoscopy and Ureteroscopy. A tiny basket is used for removing kidney stones that have entered into the ureter. In order to fragment these stones into pieces so that they can leave by the means of urine, a long extended flexible laser-equipped fibre through the ureteroscope is used.
								</p>
								<h5>
									To get best options for Urology treatment, send us a query or
									write to us at <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
								</h5>
							</div>
							<!-- End Tab -->

						</div>
					</div>
					<!-- End Column -->
				</div>
				<!-- End Row -->
			</div>
			<!-- End Tab Accordion -->

		</div>
		<!-- End Container -->
	</div>
	<!-- Speciality Content Ends Here -->

<%@ include file="newFooter.jsp"%>
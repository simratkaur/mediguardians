<%@ include file="newHeader.jsp"%></section>
<!-- Hospital Short Details Starts Here -->
<div class="short-details">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="title">
					<h1>Cosmetic Surgery</h1>
					<ul class="paginator">
						<li><a href="/">Home</a></li>
						<li><a>Cosmetic Surgery</a></li>
					</ul>
				</div>
			</div>
			<!-- End Column -->
		</div>
		<!-- End Row -->
	</div>
	<!-- End Container -->
</div>
<!-- Hospital Short Details Ends Here -->

<!-- Specialty Content Starts Here -->
<div class="speciality-content-area">
	<div class="container">

		<div class="innertab-accordion">
			<div class="row">
				<div class="col-md-3 tabnopaddright">
					<div class="tabs">
						<ul>
							<li><a href="cosmetic-surgery">Cosmetic Surgery</a></li>
							<li><a href="breast-augmentation-surgery">Breast
									Augmentation</a></li>
							<li><a href="breast-reduction-surgery">Breast Reduction</a></li>
							<li><a href="liposuction-surgery">Liposuction</a></li>
							<li class="tab-active"><a href="facelift-surgery">Facelift</a></li>
							<li><a href="rhinoplasty-surgery">Rhinoplasty</a></li>
							<li><a href="cosmetic-laser-surgery">Cosmetic Laser
									Surgery</a></li>
							<li><a href="oral-maxillofacial-surgery">Maxillofacial
									Surgery</a></li>
							<li><a href="surgical-hair-transplant">Surgical Hair
									Transplant</a></li>
						</ul>
					</div>
				</div>
				<!-- End Column -->
				<div class="col-md-9 tabnopaddleft">
					<div class="accordion-contentarea">

			<div id="tab5">
							<h2>
								<span>Facelift Surgery</span> in India
							</h2>
							<div class="row">
								<div class="col-md-9 col-sm-9 nopaddleft">
									<p>We at Arvene Healthcare have the expertise to organize
										for all treatments and surgeries related to Facelift Surgery.
										Arvene Healthcare is associated with top hospitals and Doctors
										of India in this field. We can give you more and better
										options so that you can take the right decisions. We also
										assist you in taking best decision based on your requirement.
										Arvene Healthcare will also arrange for your stay and local
										travel, so that you have nothing else to worry about but your
										treatment. Please contact us on <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
										today to avail best services.</p>
								</div>
								<!-- End Inner Column -->
								<div class="col-md-3 col-sm-3 nopaddright">
									<img src="images/specialities/12.facelift surgery.jpg"
										alt="facelift surgery" class="img-responsive">
								</div>
								<!-- End Inner Column -->
							</div>
							<!-- End Inner Row -->
							<h3>
								<span>What is</span> Face - Lift Surgery?
							</h3>
							<p>Face lift surgery aims at eliminating the extra facial
								skin to give the face a younger look. With ageing, a person
								develops droopy and loose skin and also lose the muscle tone and
								fat. These may due to the stress, excess exposure to the sun.
								Surgeries like Rhinoplasty, Blepharoplasty and brow lift can
								also be performed with a facelift surgery. Non-surgical
								procedures such as Botox can also be done. Accompanying
								procedures can also be done along with face lift surgery to get
								the maximum benefit, like Liposuction, removal of the buccal
								(cheek), neck lift, brow lift, forehead lift, laser or chemical
								peel, chin implants and malar (cheek).</p>
							<h3>
								<span>Procedure of</span> Facelift
							</h3>
							<p>Surgical facelift procedures begin with incisions at the
								patient's temples. The tissues and muscles are then separated
								and the fat is removed. The deep muscles are tightened and
								excess skin is pulled up to align with the newly positioned
								muscles. The surgeon then trims the outlying skin tissues and
								stitches the incisions. An incision can also be made under your
								chin.</p>

							<h3>
								<span>Who should consider </span>Facelift Surgery?
							</h3>
							<ul>
								<li><span>Wrinkles or loose and sagging skin in the
										cheeks or near the cheek bones</span></li>
								<li><span>Candidates who do not have well defined
										jaw line</span></li>
								<li><span>Candidates having lines starting from the
										edges of the nose following the edge of the mouth.</span></li>
								<li><span>Candidates who have wrinkles or extra fat
										and loose skin in the area around the neck.</span></li>
							</ul>
							<p>A good candidate in facelift surgery is the one who is
								physically healthy and emotionally stable to undergo the
								surgery.</p>
							<h3>
								<span>Benefits of</span> Facelift
							</h3>
							<ul>
								<li><span>Pain and discomfort are usually minimal</span></li>
								<li><span>The face will look younger and more
										vibrant</span></li>
								<li><span>Results will last for around 10 years</span></li>
								<li><span>Recovery may take 1 week and the daily
										activities can be started by the next day</span></li>
								<li><span>Helps to get smooth and taut skin on the
										face and neck</span></li>
							</ul>
							<h3>
								<span>Non-Surgical </span>Face Lift
							</h3>
							<p>TThis non-surgical procedure uses radiofrequency waves
								that is gently applied through specially designed facial probes.
								The facial probes are applied in such a way so as to heat the
								deeper skin (dermis) slowly. This is done to prevent any damage
								to the superficial skin (epidermis).</p>

							<h3>
								<span>After</span> Face Lift
							</h3>
							<p>The patient has to wear bandages on the face to avoid any
								clot formation and will be removed after a week along with the
								stitches. The patient can resume routine activities and light
								exercises in about two weeks, and strenuous exercises after six
								weeks.</p>
							<h3>
								<span>Time and Cost of</span> Face Lift Surgery
							</h3>
							<p>For the face lift surgery, the patient is required to stay
								in the hospital for 1-2 days after the procedure. Total stay can
								be for 8 to 10 days. The cost of face lift surgery in India is
								very low and offers best medical facilities along with the
								latest technologies.</p>
							<h5>
								To get best options for cosmetic treatment, send us a query or
								write to us at <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
							</h5>
						</div>
						<!-- End Tab -->
					</div>
				</div>
				<!-- End Column -->
			</div>
			<!-- End Row -->
		</div>
		<!-- End Tab Accordion -->

	</div>
	<!-- End Container -->
</div>
<!-- Speciality Content Ends Here -->

<%@ include file="newFooter.jsp"%>
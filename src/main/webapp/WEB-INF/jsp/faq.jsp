<%@ include file="newHeader.jsp"%></section>
	<!-- Banner Starts Here -->
	<div class="inner-banner">
		<img src="images/banners/sub-banner.jpg" alt="FAQ"
			class="img-responsive">
	</div>
	<!-- Banner Ends Here -->

	<!-- Short Details Starts Here -->
	<div class="short-details">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="title">
						<h1>Frequently Asked Questions</h1>
						<ul class="paginator">
							<li><a href="/">Home</a></li>
							<li><a>FAQ</a></li>
						</ul>
					</div>
				</div>
				<!-- End Column -->
			</div>
			<!-- End Row -->
		</div>
		<!-- End Container -->
	</div>
	<!-- Short Details Ends Here -->

	<!-- FAQ Content Section Starts Here -->
	<div class="content-area">
		<div class="container">

			<div class="accordion-container">
				<h3>
					<span>General</span> information
				</h3>
				<div class="panel-group mg-accordion" id="accordion_one"
					role="tablist" aria-multiselectable="true">

					<div class="panel panel-default">
						<div class="panel-heading panel-active" role="tab" id="GIone">
							<h4 class="panel-title">
								<a role="button" data-toggle="collapse"
									data-parent="#accordion_one" href="#gi_one"
									aria-expanded="true" aria-controls="gi_one"> What
									accreditation standards are followed in India?<i
									class="icon-chevron-down"></i>
								</a>
							</h4>
						</div>
						<div id="gi_one" class="panel-collapse collapse in"
							role="tabpanel" aria-labelledby="GIone">
							<div class="panel-body">
								<p>The hospitals in India are aligned to JCI or the NABH accreditation which are stringent and oriented to the highest standards for medical delivery
								</p>
							</div>
						</div>
					</div>
					<!-- End Panel -->

					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="GItwo">
							<h4 class="panel-title">
								<a role="button" data-toggle="collapse"
									data-parent="#accordion_one" href="#gi_two"
									aria-expanded="true" aria-controls="gi_two"> How much time
									will I have to wait for admission to a hospital and for
									procedure to get done?<i class="icon-chevron-right"></i>
								</a>
							</h4>
						</div>
						<div id="gi_two" class="panel-collapse collapse" role="tabpanel"
							aria-labelledby="GItwo">
							<div class="panel-body">
								<p>
									In India there is no waiting period for diagnostic procedures or for treatment. After the initial diagnosis is done and the medical procedure is decided, your surgery/intervention will be scheduled quickly. You will not be required to wait in India. 
									
								</p>
							</div>
						</div>
					</div>
					<!-- End Panel -->

					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="GIthree">
							<h4 class="panel-title">
								<a role="button" data-toggle="collapse"
									data-parent="#accordion_one" href="#gi_three"
									aria-expanded="true" aria-controls="gi_three"> When making
									a trip to India for a medical treatment, what are the essential
									documents I need to carry along?<i class="icon-chevron-right"></i>
								</a>
							</h4>
						</div>
						<div id="gi_three" class="panel-collapse collapse" role="tabpanel"
							aria-labelledby="GIthree">
							<div class="panel-body">
								<ul>
									<li><span>Medical History/ Records/ Test reports/
											Doctor referral notes/ X-rays</span></li>
									<li><span>Contact details � Residence/ Driver�s
											license/ Passport copies</span></li>
									<li><span>Passport size photos � keep 10-15 in hand
											for various purposes</span></li>
									<li><span>Bank statement/ Details of Health
											insurance, if any.</span></li>
								</ul>
							</div>
						</div>
					</div>
					<!-- End Panel -->

					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="GIfour">
							<h4 class="panel-title">
								<a role="button" data-toggle="collapse"
									data-parent="#accordion_one" href="#gi_four"
									aria-expanded="true" aria-controls="gi_four"> Should I take
									vaccinations before coming to India?<i
									class="icon-chevron-right"></i>
								</a>
							</h4>
						</div>
						<div id="gi_four" class="panel-collapse collapse" role="tabpanel"
							aria-labelledby="GIfour">
							<div class="panel-body">
								<p>
									Vaccinations and up-to-date immunizations are a safeguard for
									any international travel. However, please check with the doctor
									in your home country, any Government advisory and also the
									hospital in India as to what should be taken. The recommended
									vaccinations would normally be from the following list:<br>
									<br>
								</p>
								<ul>
									<li><span>Diphtheria Pertussis and Tetanus (DPT)</span></li>
									<li><span>Measles, Mumps and Rubella (MMR)</span></li>
									<li><span>Polio (Oral Polio Vaccine or Injectable
											Polio Vaccine)</span></li>
									<li><span>Hemophilus b conjugate vaccine (Hib)</span></li>
									<li><span>Hepatitis A</span></li>
									<li><span>Hepatitis B (when stay goes beyond 60
											days)</span></li>
									<li><span>Typhoid</span></li>
									<li><span>Japanese B encephalitis (JBE)</span></li>
								</ul>
							</div>
						</div>
					</div>
					<!-- End Panel -->

				</div>
				<!-- End Panel Group -->
			</div>
			<!-- End Accordion Container -->

			<div class="accordion-container">
				<h3 class="title-margin">
					<span>Essentials Prior to</span> Visit
				</h3>
				<div class="panel-group mg-accordion" id="accordion_two"
					role="tablist" aria-multiselectable="true">

					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="GIfive">
							<h4 class="panel-title">
								<a role="button" data-toggle="collapse"
									data-parent="#accordion_two" href="#gi_five"
									aria-expanded="true" aria-controls="gi_five"> What is the
									procedure for booking a treatment at a hospital?<i
									class="icon-chevron-right"></i>
								</a>
							</h4>
						</div>
						<div id="gi_five" class="panel-collapse collapse" role="tabpanel"
							aria-labelledby="GIfive">
							<div class="panel-body">
								<p>We request you to send your query to Arvene Healthcare. Either send a query on website enquiry page or send a mail to <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>. We will discuss your case with best doctors and hospitals and get back to you with recommendations. You can then decide where you want to undertake the treatment and let us know. We will book your treatment, give tentative dates (after discussion with you) and arrange for VISA letter etc. Our interpreters will talk to you in case of language issue. 
								</p>
							</div>
						</div>
					</div>
					<!-- End Panel -->

					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="GIsix">
							<h4 class="panel-title">
								<a role="button" data-toggle="collapse"
									data-parent="#accordion_two" href="#gi_six"
									aria-expanded="true" aria-controls="gi_six"> Would the
									Hospital undertake bookings for my family/attendant�s
									accommodation?<i class="icon-chevron-right"></i>
								</a>
							</h4>
						</div>
						<div id="gi_six" class="panel-collapse collapse" role="tabpanel"
							aria-labelledby="GIsix">
							<div class="panel-body">
								<p>
									Please leave all your accommodation worries to Arvene Healthcare. We will give you options of Hotels near your hospital as per your budgets. We will book the same for you as well. Few Hospitals provide accommodation as well on chargeable basis. We will let you know if that is a possibility as well. 
									
								</p>
							</div>
						</div>
					</div>
					<!-- End Panel -->

					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="GIseven">
							<h4 class="panel-title">
								<a role="button" data-toggle="collapse"
									data-parent="#accordion_two" href="#gi_seven"
									aria-expanded="true" aria-controls="gi_seven"> Can I obtain
									an estimate of the cost of treatment?<i
									class="icon-chevron-right"></i>
								</a>
							</h4>
						</div>
						<div id="gi_seven" class="panel-collapse collapse" role="tabpanel"
							aria-labelledby="GIseven">
							<div class="panel-body">
								<p>
									Yes, Arvene Healthcare will ensure to provide that from the hospital. 
The cost as you may be aware, depends on the medical condition, the choice of treatment, the type of room opted for etc. All your medical history and essential treatment details would be analysed by the team of experts in the hospitals. Charges are likely to vary by the type of room you take.

								</p>
							</div>
						</div>
					</div>
					<!-- End Panel -->

					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="GIeight">
							<h4 class="panel-title">
								<a role="button" data-toggle="collapse"
									data-parent="#accordion_two" href="#gi_eight"
									aria-expanded="true" aria-controls="gi_eight"> Do the
									hospitals in India accept health insurance?<i
									class="icon-chevron-right"></i>
								</a>
							</h4>
						</div>
						<div id="gi_eight" class="panel-collapse collapse" role="tabpanel"
							aria-labelledby="GIeight">
							<div class="panel-body">
								<p>If you have health insurance that�s internationally valid / valid in India, please alert the hospitals about it. If it�s an approved insurance provider, hospitals might request you to get an advance payment guarantee which you will have to co-ordinate from your home country.
								</p>
							</div>
						</div>
					</div>
					<!-- End Panel -->

					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="GInine">
							<h4 class="panel-title">
								<a role="button" data-toggle="collapse"
									data-parent="#accordion_two" href="#gi_nine"
									aria-expanded="true" aria-controls="gi_nine"> What are the
									inclusions / exclusions in the cost?<i
									class="icon-chevron-right"></i>
								</a>
							</h4>
						</div>
						<div id="gi_nine" class="panel-collapse collapse" role="tabpanel"
							aria-labelledby="GInine">
							<div class="panel-body">
								<p>
									Usual
									inclusions are:<br>
								</p>
								<ul>
									<li><span>Stay and beds</span></li>
									<li><span>Diet � for patient and bystander</span></li>
									<li><span>Surgeon and anaesthetist�s fee</span></li>
									<li><span>Nursing</span></li>
									<li><span>Doctor�s / specialist consultation and
											fee</span></li>
									<li><span>Fee for surgery, theatre room</span></li>
									<li><span>Consumables used for surgery (sutures,
											tubes etc.)</span></li>
								</ul>
							</div>
						</div>
					</div>
					<!-- End Panel -->

					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="GIten">
							<h4 class="panel-title">
								<a role="button" data-toggle="collapse"
									data-parent="#accordion_two" href="#gi_ten"
									aria-expanded="true" aria-controls="gi_ten"> Exclusions<i
									class="icon-chevron-right"></i>
								</a>
							</h4>
						</div>
						<div id="gi_ten" class="panel-collapse collapse" role="tabpanel"
							aria-labelledby="GIten">
							<div class="panel-body">
								<p>
									Implants and special consumables.<br>
									<br>
								</p>
								<ul>
									<li><span>	These are choice driven. If the medical procedure needs implants or special consumables, the doctors or staff will intimate you. Implants, consumables are of different sizes, types, materials and cost options. Depending on what suits your body condition, there will be a range of choices. You can make the selection on the brands and/or their quality options based on the cost suitability and suggestion of the specialist.
									</span></li>
									<li><span>Stay beyond the usual time in the
											hospital, depending on your recovery is likely to be
											additional.</span></li>
									<li><span>Special tests, special medications and
											high value drugs will be charged extra.</span></li>
								</ul>
							</div>
						</div>
					</div>
					<!-- End Panel -->

					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="GIeleven">
							<h4 class="panel-title">
								<a role="button" data-toggle="collapse"
									data-parent="#accordion_two" href="#gi_eleven"
									aria-expanded="true" aria-controls="gi_eleven"> Will
									Hospitals assist in airport transfers?<i
									class="icon-chevron-right"></i>
								</a>
							</h4>
						</div>
						<div id="gi_eleven" class="panel-collapse collapse"
							role="tabpanel" aria-labelledby="GIeleven">
							<div class="panel-body">
								<p>Few hospitals do provide airport transfers as a complimentary service. However to be sure, we at Arvene Healthcare will be happy to arrange for your transport on chargeable basis. Rest assured your local transportation will be taken care of.
								</p>
							</div>
						</div>
					</div>
					<!-- End Panel -->

				</div>
				<!-- End Panel Group -->
			</div>
			<!-- End Accordion Container -->

			<div class="accordion-container">
				<h3 class="title-margin">
					<span>Medical</span> Visa
				</h3>
				<div class="panel-group mg-accordion" id="accordion_three"
					role="tablist" aria-multiselectable="true">

					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="GItwelve">
							<h4 class="panel-title">
								<a role="button" data-toggle="collapse"
									data-parent="#accordion_three" href="#gi_twelve"
									aria-expanded="true" aria-controls="gi_twelve"> Will
									assistance be provided by hospitals in obtaining Medical Visa?<i
									class="icon-chevron-right"></i>
								</a>
							</h4>
						</div>
						<div id="gi_twelve" class="panel-collapse collapse"
							role="tabpanel" aria-labelledby="GItwelve">
							<div class="panel-body">
								<p>Hospitals would provide assistance in this regard. Once you decide on India as your medical travel destination, an invitation letter from the respective Hospital will be required for visa processing. We will arrange the letter and send to you after your initial discussions about the treatment; this has to be presented at the �Embassy� or �High Commission of India� in your country.
								</p>
							</div>
						</div>
					</div>
					<!-- End Panel -->

					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="GIthirteen">
							<h4 class="panel-title">
								<a role="button" data-toggle="collapse"
									data-parent="#accordion_three" href="#gi_thirteen"
									aria-expanded="true" aria-controls="gi_thirteen"> Is it
									possible for my family members/ attendant travelling along with
									me for my treatment to obtain Medical Visas?<i
									class="icon-chevron-right"></i>
								</a>
							</h4>
						</div>
						<div id="gi_thirteen" class="panel-collapse collapse"
							role="tabpanel" aria-labelledby="GIthirteen">
							<div class="panel-body">
								<p>Attendants/family members of patients coming to India for medical treatment will have to apply for Medical Attendant visa (MEDX) co-terminus with the Medical visa of the patient. A letter from the hospital will be useful here as well.
								</p>
							</div>
						</div>
					</div>
					<!-- End Panel -->

					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="GIfourteen">
							<h4 class="panel-title">
								<a role="button" data-toggle="collapse"
									data-parent="#accordion_three" href="#gi_fourteen"
									aria-expanded="true" aria-controls="gi_fourteen"> Who are
									the permitted attendants and how many can travel?<i
									class="icon-chevron-right"></i>
								</a>
							</h4>
						</div>
						<div id="gi_fourteen" class="panel-collapse collapse"
							role="tabpanel" aria-labelledby="GIfourteen">
							<div class="panel-body">
								<p>Attendants should be spouse/children or those having blood relations with the patient. Not more than two attendants are allowed.
								</p>
							</div>
						</div>
					</div>
					<!-- End Panel -->

					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="GIfifteen">
							<h4 class="panel-title">
								<a role="button" data-toggle="collapse"
									data-parent="#accordion_three" href="#gi_fifteen"
									aria-expanded="true" aria-controls="gi_fifteen"> What are
									the requirements for obtaining my medical visa?<i
									class="icon-chevron-right"></i>
								</a>
							</h4>
						</div>
						<div id="gi_fifteen" class="panel-collapse collapse"
							role="tabpanel" aria-labelledby="GIfifteen">
							<div class="panel-body">
								<ul>
									<li><span>Passport with 6 month validity</span></li>
									<li><span>Passport size photographs</span></li>
									<li><span>Photo copy of passport</span></li>
									<li><span>Copy of online Filled form</span></li>
									<li><span>Proof of Residential Address</span></li>
									<li><span>Recommendation from the home country
											doctor to visit particular specialized medical centre for
											treatment</span></li>
									<li><span>Passport copy of the attendant</span></li>
									<li><span>Proof of relationship with the attendant</span></li>
									<li><span>Medical reports</span></li>
								</ul>
							</div>
						</div>
					</div>
					<!-- End Panel -->

					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="GIsixteen">
							<h4 class="panel-title">
								<a role="button" data-toggle="collapse"
									data-parent="#accordion_three" href="#gi_sixteen"
									aria-expanded="true" aria-controls="gi_sixteen"> How long
									is a medical visa valid for?<i class="icon-chevron-right"></i>
								</a>
							</h4>
						</div>
						<div id="gi_sixteen" class="panel-collapse collapse"
							role="tabpanel" aria-labelledby="GIsixteen">
							<div class="panel-body">
								<p>Initial duration of the visa is one year or the period of treatment, whichever is less. Visa will be valid for maximum three entries in a year.
								</p>
							</div>
						</div>
					</div>
					<!-- End Panel -->

					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="GIseventeen">
							<h4 class="panel-title">
								<a role="button" data-toggle="collapse"
									data-parent="#accordion_three" href="#gi_seventeen"
									aria-expanded="true" aria-controls="gi_seventeen"> If a
									necessity of a repeat visit arises, how many are allowed?<i
									class="icon-chevron-right"></i>
								</a>
							</h4>
						</div>
						<div id="gi_seventeen" class="panel-collapse collapse"
							role="tabpanel" aria-labelledby="GIseventeen">
							<div class="panel-body">
								<p>A maximum of 3 entries are allowed in one year.</p>
							</div>
						</div>
					</div>
					<!-- End Panel -->

					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="GIeighteen">
							<h4 class="panel-title">
								<a role="button" data-toggle="collapse"
									data-parent="#accordion_three" href="#gi_eighteen"
									aria-expanded="true" aria-controls="gi_eighteen"> What
									would be the approximate fee for a Medical Visa?<i
									class="icon-chevron-right"></i>
								</a>
							</h4>
						</div>
						<div id="gi_eighteen" class="panel-collapse collapse"
							role="tabpanel" aria-labelledby="GIeighteen">
							<div class="panel-body">
								<p>A visa fee for Medical visa is different by countries. It
									ranges from $13 to $153.</p>
							</div>
						</div>
					</div>
					<!-- End Panel -->

					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="GInineteen">
							<h4 class="panel-title">
								<a role="button" data-toggle="collapse"
									data-parent="#accordion_three" href="#gi_nineteen"
									aria-expanded="true" aria-controls="gi_nineteen"> Are there
									any formalities with the Medical Visa after my arrival to
									India?<i class="icon-chevron-right"></i>
								</a>
							</h4>
						</div>
						<div id="gi_nineteen" class="panel-collapse collapse"
							role="tabpanel" aria-labelledby="GInineteen">
							<div class="panel-body">
								<p>Normally the hospitals help you with such formalities. You would have to register at the Foreigner�s Regional Registration Office within 14 days of your arrival. Attendants also have to get registered themselves with the local FRROs / FROs within 14 days of entering India.
								</p>
							</div>
						</div>
					</div>
					<!-- End Panel -->

					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="GItwenty">
							<h4 class="panel-title">
								<a role="button" data-toggle="collapse"
									data-parent="#accordion_three" href="#gi_twenty"
									aria-expanded="true" aria-controls="gi_twenty"> If my
									treatment is being prolonged, would I be provided assistance
									with extension of my visa?<i class="icon-chevron-right"></i>
								</a>
							</h4>
						</div>
						<div id="gi_twenty" class="panel-collapse collapse"
							role="tabpanel" aria-labelledby="GItwenty">
							<div class="panel-body">
								<p>Medical and Medical Attendant visas are extendable by FRRO (Foreigners Regional Registration Office) in India depending upon the condition of the patient on the production of the required medical documents.
								</p>
							</div>
						</div>
					</div>
					<!-- End Panel -->

				</div>
				<!-- End Panel Group -->
			</div>
			<!-- End Accordion Container -->

			<div class="accordion-container">
				<h3 class="title-margin">
					<span>During</span> Visit / Treatment
				</h3>
				<div class="panel-group mg-accordion" id="accordion_four"
					role="tablist" aria-multiselectable="true">

					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="GItwenty1">
							<h4 class="panel-title">
								<a role="button" data-toggle="collapse"
									data-parent="#accordion_four" href="#gi_twenty1"
									aria-expanded="true" aria-controls="gi_twenty1"> How will
									my appointments with the doctor be scheduled?<i
									class="icon-chevron-right"></i>
								</a>
							</h4>
						</div>
						<div id="gi_twenty1" class="panel-collapse collapse"
							role="tabpanel" aria-labelledby="GItwenty1">
							<div class="panel-body">
								<p>Arvene Healthcare will help fix up your appointments in Hospital. The Hospital�s International Patients Division would facilitate in scheduling your appointments and consultations with the doctor. 
								</p>
							</div>
						</div>
					</div>
					<!-- End Panel -->

					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="GItwenty2">
							<h4 class="panel-title">
								<a role="button" data-toggle="collapse"
									data-parent="#accordion_four" href="#gi_twenty2"
									aria-expanded="true" aria-controls="gi_twenty2"> Would
									there be pharmacies / medical store available in the Hospitals?<i
									class="icon-chevron-right"></i>
								</a>
							</h4>
						</div>
						<div id="gi_twenty2" class="panel-collapse collapse"
							role="tabpanel" aria-labelledby="GItwenty2">
							<div class="panel-body">
								<p>Most Hospitals in India are likely to have a Pharmacy within its premises for easy accessibility.
								</p>
							</div>
						</div>
					</div>
					<!-- End Panel -->

					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="GItwenty3">
							<h4 class="panel-title">
								<a role="button" data-toggle="collapse"
									data-parent="#accordion_four" href="#gi_twenty3"
									aria-expanded="true" aria-controls="gi_twenty3"> What are
									the various room categories available in the Hospital?<i
									class="icon-chevron-right"></i>
								</a>
							</h4>
						</div>
						<div id="gi_twenty3" class="panel-collapse collapse"
							role="tabpanel" aria-labelledby="GItwenty3">
							<div class="panel-body">
								<p>
									The standard categories are:<br>
								</p>
								<ul>
									<li><span>Single accommodation units</span>
										<ul>
											<li><span>Suite Room</span></li>
											<li><span>Deluxe Rooms</span></li>
											<li><span>Normal single room</span></li>
										</ul></li>
									<li><span>Multiple sharing units</span>
										<ul>
											<li><span>Twin sharing</span></li>
											<li><span>Triple sharing</span></li>
											<li><span>Ward type</span></li>
										</ul></li>
								</ul>
							</div>
						</div>
					</div>
					<!-- End Panel -->

					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="GItwenty4">
							<h4 class="panel-title">
								<a role="button" data-toggle="collapse"
									data-parent="#accordion_four" href="#gi_twenty4"
									aria-expanded="true" aria-controls="gi_twenty4"> What are
									the communication services / facilities provided by the
									Hospital?<i class="icon-chevron-right"></i>
								</a>
							</h4>
						</div>
						<div id="gi_twenty4" class="panel-collapse collapse"
							role="tabpanel" aria-labelledby="GItwenty4">
							<div class="panel-body">
								<ul>
									<li><span>International calling services/ Local sim
											cards</span></li>
									<li><span>Newspaper � Local/ International</span></li>
									<li><span>Internet facility � Wi-Fi</span></li>
									<li><span>Room telephone</span></li>
								</ul>
							</div>
						</div>
					</div>
					<!-- End Panel -->

					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="GItwenty5">
							<h4 class="panel-title">
								<a role="button" data-toggle="collapse"
									data-parent="#accordion_four" href="#gi_twenty5"
									aria-expanded="true" aria-controls="gi_twenty5"> Would I be
									assisted with an interpreter during my stay?<i
									class="icon-chevron-right"></i>
								</a>
							</h4>
						</div>
						<div id="gi_twenty5" class="panel-collapse collapse"
							role="tabpanel" aria-labelledby="GItwenty5">
							<div class="panel-body">
								<p>English is widely spoken across India. But if assistance
									for any other language is required, interpretation service will
									be arranged on request. Some hospitals utilise the service of
									full-time in-house interpreters. It is better that you inform
									the language need in advance to the hospital where you are
									likely to go to.</p>
							</div>
						</div>
					</div>
					<!-- End Panel -->

					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="GItwenty6">
							<h4 class="panel-title">
								<a role="button" data-toggle="collapse"
									data-parent="#accordion_four" href="#gi_twenty6"
									aria-expanded="true" aria-controls="gi_twenty6"> Will
									arrangements be made to fulfil my religious obligations?<i
									class="icon-chevron-right"></i>
								</a>
							</h4>
						</div>
						<div id="gi_twenty6" class="panel-collapse collapse"
							role="tabpanel" aria-labelledby="GItwenty6">
							<div class="panel-body">
								<p>Hospitals usually have designated quiet rooms / prayer
									halls which cater to major religions/faiths. India is a secular
									country and is open to all religions. The hospital staff would
									also be able to guide you to the closest place of worship.</p>
							</div>
						</div>
					</div>
					<!-- End Panel -->

					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="GItwenty7">
							<h4 class="panel-title">
								<a role="button" data-toggle="collapse"
									data-parent="#accordion_four" href="#gi_twenty7"
									aria-expanded="true" aria-controls="gi_twenty7"> Would
									international cuisines be available?<i
									class="icon-chevron-right"></i>
								</a>
							</h4>
						</div>
						<div id="gi_twenty7" class="panel-collapse collapse"
							role="tabpanel" aria-labelledby="GItwenty7">
							<div class="panel-body">
								<p>
									Yes, international cuisines are available in major hospitals. Some menus are standard while others would be on request. Feel free to specify this to Arvene Healthcareas you enquire. We can suggest few places outside hospital where you can get specific food. That said, doctors will advise your diet based on your therapeutic needs and quick recovery! 
									
								</p>
							</div>
						</div>
					</div>
					<!-- End Panel -->

				
					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="GItwenty9">
							<h4 class="panel-title">
								<a role="button" data-toggle="collapse"
									data-parent="#accordion_four" href="#gi_twenty9"
									aria-expanded="true" aria-controls="gi_twenty9"> What are
									the other facilities provided by hospitals?<i
									class="icon-chevron-right"></i>
								</a>
							</h4>
						</div>
						<div id="gi_twenty9" class="panel-collapse collapse"
							role="tabpanel" aria-labelledby="GItwenty9">
							<div class="panel-body">
								<p>
									Certain Hospitals provide the following :<br>
								</p>
								<ul>
									<li><span>Laundry services</span></li>
									<li><span>Local sight-seeing</span></li>
									<li><span>In-house business centre</span></li>
									<li><span>Safety locker facility</span></li>
									<li><span>Gymnasium</span></li>
									<li><span>Shopping assistance</span></li>
								</ul>
								<p>Please convey your requirements to the staff.</p>
							</div>
						</div>
					</div>
					<!-- End Panel -->

				</div>
				<!-- End Panel Group -->
			</div>
			<!-- End Accordion Container -->

			<div class="accordion-container">
				<h3 class="title-margin">
					<span>Post</span> Treatment
				</h3>
				<div class="panel-group mg-accordion" id="accordion_five"
					role="tablist" aria-multiselectable="true">

					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="GIthirty">
							<h4 class="panel-title">
								<a role="button" data-toggle="collapse"
									data-parent="#accordion_five" href="#gi_thirty"
									aria-expanded="true" aria-controls="gi_thirty"> Can I
									travel immediately after treatment?<i
									class="icon-chevron-right"></i>
								</a>
							</h4>
						</div>
						<div id="gi_thirty" class="panel-collapse collapse"
							role="tabpanel" aria-labelledby="GIthirty">
							<div class="panel-body">
								<p>It will depend on the period of recuperation for the
									specific treatment. Based on your Doctor�s advice your return
									travel can be planned.</p>
							</div>
						</div>
					</div>
					<!-- End Panel -->

					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="GIthirty1">
							<h4 class="panel-title">
								<a role="button" data-toggle="collapse"
									data-parent="#accordion_five" href="#gi_thirty1"
									aria-expanded="true" aria-controls="gi_thirty1"> Are
									treatment procedures covered by major medical insurance
									companies?<i class="icon-chevron-right"></i>
								</a>
							</h4>
						</div>
						<div id="gi_thirty1" class="panel-collapse collapse"
							role="tabpanel" aria-labelledby="GIthirty1">
							<div class="panel-body">
								<p>Medical insurance can be claimed, but kindly check with
									the respective Hospital if this facility is accepted and,
									availed.</p>
							</div>
						</div>
					</div>
					<!-- End Panel -->

					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="GIthirty2">
							<h4 class="panel-title">
								<a role="button" data-toggle="collapse"
									data-parent="#accordion_five" href="#gi_thirty2"
									aria-expanded="true" aria-controls="gi_thirty2"> What are
									the acceptable modes of payment?<i class="icon-chevron-right"></i>
								</a>
							</h4>
						</div>
						<div id="gi_thirty2" class="panel-collapse collapse"
							role="tabpanel" aria-labelledby="GIthirty2">
							<div class="panel-body">
								<p>All major modes of payments are accepted in India � Cash/
									International Master or Visa Credit Card, Travellers cheque
									etc.</p>
							</div>
						</div>
					</div>
					<!-- End Panel -->

					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="GIthirty3">
							<h4 class="panel-title">
								<a role="button" data-toggle="collapse"
									data-parent="#accordion_five" href="#gi_thirty3"
									aria-expanded="true" aria-controls="gi_thirty3"> What are
									the procedures involved in the process of discharge?<i
									class="icon-chevron-right"></i>
								</a>
							</h4>
						</div>
						<div id="gi_thirty3" class="panel-collapse collapse"
							role="tabpanel" aria-labelledby="GIthirty3">
							<div class="panel-body">
								<p>You will be guided by the Hospital official in its
									process. Financial aspects would be taken care of once the
									Discharge summary is handed to you. Subsequently you will be
									given instructions on medication and recommendations for
									scheduling of follow up requirements, if any.</p>
							</div>
						</div>
					</div>
					<!-- End Panel -->

					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="GIthirty4">
							<h4 class="panel-title">
								<a role="button" data-toggle="collapse"
									data-parent="#accordion_five" href="#gi_thirty4"
									aria-expanded="true" aria-controls="gi_thirty4"> How will
									my offshore check-up with the Doctor in India after treatment
									be scheduled?<i class="icon-chevron-right"></i>
								</a>
							</h4>
						</div>
						<div id="gi_thirty4" class="panel-collapse collapse"
							role="tabpanel" aria-labelledby="GIthirty4">
							<div class="panel-body">
								<p>You can keep in touch with the respective Doctor via Arvene Healthcare web portal. </p>
							</div>
						</div>
					</div>
					<!-- End Panel -->

				</div>
				<!-- End Panel Group -->
			</div>
			<!-- End Accordion Container -->

		</div>
		<!-- End Container -->
	</div>
	<!-- FAQ Content Section Ends Here -->

	<%@ include file="newFooter.jsp"%>
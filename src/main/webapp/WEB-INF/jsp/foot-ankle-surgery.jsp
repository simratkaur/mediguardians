<%@ include file="newHeader.jsp"%></section>
<!-- Hospital Short Details Starts Here -->
<div class="short-details">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="title">
					<h1>Orthopedic Surgery</h1>
					<ul class="paginator">
						<li><a href="/">Home</a></li>
						<li><a>Orthopedic Surgery</a></li>
					</ul>
				</div>
			</div>
			<!-- End Column -->
		</div>
		<!-- End Row -->
	</div>
	<!-- End Container -->
</div>
<!-- Hospital Short Details Ends Here -->

<!-- Specialty Content Starts Here -->
<div class="speciality-content-area">
	<div class="container">

		<div class="innertab-accordion">
			<div class="row">
				<div class="col-md-3 tabnopaddright">
					<div class="tabs">
						<ul>
							<li><a href="orthopaedics-surgery">Orthopedic
									Surgery</a></li>
							<li><a href="knee-replacement-surgery">Knee Replacement</a></li>
							<li><a href="hip-replacement-surgery">Hip Replacement
									surgery </a></li>
							<li><a href="shoulder-surgery">Shoulder Surgery </a></li>
							<li class="tab-active"><a href="foot-ankle-surgery">Foot and Ankle surgery</a></li>
							<li><a href="hand-wrist-surgery">Hand Wrist surgery</a></li>
							<li><a href="elbow-replacement-surgery">Elbow Surgery</a></li>
						</ul>
					</div>
				</div>
				<!-- End Column -->
				<div class="col-md-9 tabnopaddleft">
					<div class="accordion-contentarea">
	<div id="tab5">
							<h2>
								<span>Foot and Ankle surgery </span>- India
							</h2>
							<div class="row">
								<div class="col-md-9 col-sm-9 nopaddleft">
									<p>We at Arvene Healthcare have the expertise to organize
										for all treatments and surgeries related to Foot and Ankle
										surgery. Arvene Healthcare is associated with top hospitals
										and Doctors of India in this field. We can give you more and
										better options so that you can take the right decisions. We
										also assist you in taking best decision based on your
										requirement. Arvene Healthcare will also arrange for your stay
										and local travel, so that you have nothing else to worry about
										but your treatment. Please contact us on
										<a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a> today to avail best services.</p>
									<br>
									<p>Foot and ankle surgery deals with the treatment,
										diagnosis as well as prevention of any kind of disorders
										occurring in the foot and ankle.</p>
								</div>
								<!-- End Inner Column -->
								<div class="col-md-3 col-sm-3 nopaddright">
									<img src="images/specialities/35. foot and ankle surgery.jpg"
										alt="foot and ankle surgery" class="img-responsive">
								</div>
								<!-- End Inner Column -->
							</div>
							<!-- End Inner Row -->
							<h3>
								<span>Types of Foot and </span>Ankle Surgery:
							</h3>
							<Strong>1. Bunions</Strong>
							<p>Bunions is that condition of the enlargement or overgrowth
								of the big toe joint. When it rubs with the shoes, it becomes
								the cause of pain and irritation. This can be corrected through
								surgery.</p>
							<Strong>2. Hammer Toes</Strong>
							<p>The condition when the human toes become claw-shaped or
								are permanently bent, it is known as hammer toes. These
								surgeries may take from an hour to a day.</p>
							<Strong>3. Metatarsal surgery</Strong>
							<p>The joints in forefoot are called metatarsal-phalangeal
								joints. They can get damaged by inflammation of the lining of
								the joint or can get dislocated when damaged by arthritis,
								causing irritation and pain. If other treatments do not help,
								surgery is the only solution, depending on the severity of the
								problem.</p>
							<p>
								<Strong>4. Ankle arthritis- </strong> The cause of ankle
								arthritis is osteoarthritis where the cartilage covering the
								ends of the bones get roughed-up and gradually become thin. It
								becomes the cause of swelling, deformity and pain, which needs
								surgery for correction. There are two options:Ankle fusion -
								(Removal of the damaged ankle joint and then fusing patient's
								talus bone to his tibia for making a stiff and pain-free
								ankle).Ankle replacement - (Removal of the worn-out ends of the
								foot's tibia and talus bones and their replacement with the
								artificial ends that might be made up of plastic or metal).
							</p>
							<p>
								<strong>5. Achilles tendon disorders- </strong> The largest
								tendon in a human body is the Achilles tendon. With age, it
								starts wearing, leading to swelling and pain within the main
								tendon. Mostly the method of its treatment is surgery, performed
								in a day's time and needing crutches later.
							</p>
							<p>
								<strong>6. Morton's neuroma- </strong> Morton's neuroma is a
								painful condition that creates a condition in nerves (third and
								fourth toes) that supply sensation to the two neighbouring toes.
								If the problem is severe, surgery for removal of the nerve is
								the solution.
							</p>
							<p>
								<strong>7. Tibialis posterior dysfunction- </strong> Tibialis
								posterior is the muscle supporting the shape of the human's
								instep arch. If the tendon gets inflamed that results in
								swelling and pain on the inside of the ankle, and If the
								condition is worse, the only resort is surgery for rebuilding
								the instep arch.
							</p>
							<p>
								<strong>8. Plantar fasciitis - </strong> - Plantar fascia is a
								group of fibrous tissue that spreads across the sole of the foot
								to the toes. Few bad cases require surgery for releasing the
								tissues from the heel bone.
							</p>
							<h3>
								<span>Benefits of </span>Foot and Ankle surgery
							</h3>
							<ul>
								<li><span>Relief from pain</span></li>
								<li><span>Restoration of joint function in patients</span></li>
								<li><span>Getting rid of osteoarthritis, traumatic
										arthritis or rheumatoid arthritis</span></li>
								<li><span>Ability to maintain movement in the joints</span></li>
								<li><span>A more normal walking gait pattern</span></li>
								<li><span>Protection of the surrounding joints</span></li>
								<li><span>A range of movement of ankle and foot</span></li>
							</ul>
							<h3>
								<span>Cost of Foot and </span>Ankle Surgery in India
							</h3>
							<p>The costs of any kind of surgery in the western countries
								is very high. In comparison to this, India offers the same
								quality of surgery at very reasonable cost. The best part is
								that a patient can enjoy such affordable treatments from
								internationally trained doctors and surgeons. The technology
								used in most hospitals is latest state of the art.</p>
							<h5>
								To get best options for Orthopaedic treatment, send us a query
								or write to us at <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
							</h5>
						</div>
						<!-- End Tab -->

					</div>
				</div>
				<!-- End Column -->
			</div>
			<!-- End Row -->
		</div>
		<!-- End Tab Accordion -->

	</div>
	<!-- End Container -->
</div>
<!-- Speciality Content Ends Here -->

<%@ include file="newFooter.jsp"%>
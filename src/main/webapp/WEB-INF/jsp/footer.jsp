<div id="footer">
	<div class="footer-top">
		<div class="container">

			<div class="footer-content">
				<div class="row">
					<div class="col-md-3">
						<div class="title">
							<h4 class="title-margin">Arvene Healthcare</h4>
						</div>
						<div class="detail">
							<ul>
								<li><a href="/cost-comparison">Medical cost of
										treatments</a></li>
								<li><a href="/organ-transplant">Organ Transplant</a></li>
								<li><a href="/cardiology-treatment">Cardiac treatments</a></li>
								<li><a href="/neuro-spine-surgery">Neuro and Spine
										treatments</a></li>
								<li><a href="/orthopaedics-surgery">Knee and hip
										replacement</a></li>
								<li><a href="/kairali">Rejuvenation</a></li>
								<li><a href="/medical-process">Process Flowchart</a></li>
								<li><a href="/air-ambulance-service">Air Ambulance
										service</a></li>
							</ul>
						</div>
					</div>
					<!-- End Column -->
					<div class="col-md-3">
						<div class="title">
							<h4 class="title-margin">Latest Posts</h4>
						</div>
						<div id="fb-root"></div>
						<script>
							(function(d, s, id) {
								var js, fjs = d.getElementsByTagName(s)[0];
								if (d.getElementById(id))
									return;
								js = d.createElement(s);
								js.id = id;
								js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.6";
								fjs.parentNode.insertBefore(js, fjs);
							}(document, 'script', 'facebook-jssdk'));
						</script>

						<div class="fb-page detail"
							style="width: 100%; background-color: transparent;"
							data-href="https://www.facebook.com/ArveneHealthcareIndia/"
							data-tabs="timeline" data-small-header="true"
							data-adapt-container-width="true" data-hide-cover="true"
							data-show-facepile="false" data-height="300">
							<div class="fb-xfbml-parse-ignore">
								<blockquote cite="https://www.facebook.com/mediguardians">
									<a href="https://www.facebook.com/mediguardians">Arvene
										Healthcare</a>
								</blockquote>
							</div>
						</div>
					</div>
					<!-- End Column -->
					<div class="col-md-3">
						<div class="title">
							<h4 class="title-margin">Latest Tweets</h4>
						</div>
						<div class="detail">
							<a class="twitter-timeline" data-dnt="true"
								href="https://twitter.com/Arvene Healthcare"
								data-chrome="nofooter noborders noheader transparent"
								data-widget-id="726853742184660992"></a>
							<script>
								!function(d, s, id) {
									var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/
											.test(d.location) ? 'http'
											: 'https';
									if (!d.getElementById(id)) {
										js = d.createElement(s);
										js.id = id;
										js.src = p
												+ "://platform.twitter.com/widgets.js";
										fjs.parentNode.insertBefore(js, fjs);
									}
								}(document, "script", "twitter-wjs");
							</script>
						</div>
					</div>
					<!-- End Column -->
					<div class="col-md-3">
						<div class="title">
							<h4 class="title-margin">Get in Touch</h4>
						</div>
						<div class="detail">
							<div class="get-touch">
								<span class="text">Arvene Healthcare pvt ltd., Quest
									Offices, 5th Floor, Technopolis, Sector 54, Golf course road,
									Gurgaon 122002.</span>
								<ul>
									<li><i class="icon-location"></i> <span>Arvene
											Healthcare Ltd., India</span></li>
									<li><i class="icon-phone4"></i> <span>(+91)
											81300-77375</span></li>
									<li><a href="mailto:contact@arvenehealthcare.com"><i
											class="icon-dollar"></i> <span>contact@arvenehealthcare.com</span></a></li>
									<li><a href="javascript:;"><i class="icon-skype"></i>
											<span>Arvene Healthcare</span></a></li>
								</ul>
							</div>
						</div>
					</div>
					<!-- End Column -->
				</div>
				<!-- End Row -->
			</div>
			<!-- End Footer Content -->

		</div>
		<!-- End Container -->
	</div>
	<!-- Footer Top Ends Here -->
	<!--Footer Bottom Starts Here -->
	<div class="footer-bottom">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<ul class="social-icons-text">
						<li style="width: 96px;"><a href="/privacy-policy">Privacy
								Policy</a> |</li>
						<li style="width: 96px;"><a href="/terms-conditions">Terms
								Of Service</a> |</li>
						<li style="width: 96px;">Follow us on</li>
					</ul>
					<ul class="social-icons">
						<li class="fb"><a
							href="https://www.facebook.com/Mediguardians/"><i
								class="icon-euro"></i></a></li>
						<li class="tw"><a href="https://twitter.com/mediguardians"><i
								class="icon-yen"></i></a></li>
						<li class="gp"><a
							href="https://plus.google.com/111772836288081577681"><i
								class="icon-google-plus"></i></a></li>
					</ul>
				</div>
				<!-- End Column -->
				<div class="col-md-6">
					<div class="copyrighttxt">
						<p>Copyright &copy; 2017 Arvene Healthcare. All Rights
							Reserved.</p>
					</div>
				</div>
				<!-- End Column -->
			</div>
			<!-- End Row -->
		</div>
		<!-- End Container -->
	</div>
	<!--Footer Bottom Ends Here -->

	<!-- Side Form Starts Here -->
	<div class="side-form">
		<div class="widget-area">
			<div class="buttonarea">
				<a href="javascript:;" class="btn btn-enquire" data-toggle="modal"
					data-target="#enqueryForm">Enquire Now</a>
			</div>
			<!-- <form class="enquire-form" id="enquiry_form" method="post"
			action="send-mail.php">
			<button type="button" class="btn close-btn">&times;</button>
			<h4>Send us your query</h4>
			<div class="form-group">
				<label>Name :</label> <input type="text" name="name"
					class="form-control" placeholder="Name">
			</div>
			<div class="form-group">
				<label>E-mail ID :</label> <input type="text" name="email"
					class="form-control" placeholder="E-mail ID">
			</div>
			<div class="form-group">
				<label>Mobile Number :</label> <input type="text" name="phone"
					class="form-control" placeholder="Mobile No.">
			</div>
			<div class="form-group">
				<label>Age :</label>
				<div class="input-group">
					<input type="text" name="age" class="form-control"
						placeholder="Age in Years">
					<div class="input-group-addon">Years</div>
				</div>
			</div>
			<div class="form-group">
				<label>Country :</label> <input type="text" name="country"
					class="form-control" placeholder="Country">
			</div>
			<div class="form-group">
				<label>Message :</label>
				<textarea rows="2" name="message" class="form-control"
					placeholder="Message"></textarea>
			</div>
			<div class="form-group">
				<label class="captcha_label">Are you human ?</label>
				<div class="input-group">
					<div class="input-group-addon captcha-num" id="captchaOperation"></div>
					<input type="text" name="captcha" id="captcha" class="form-control">
					<div class="input-group-addon reset">
						<i class="icon-reply"></i>
					</div>
				</div>
			</div>
			<div class="form-group">
				<input type="submit" id="send" name="send" class="btn btn-submit"
					value="Submit">
			</div>
		</form> -->
		</div>
	</div>
	<!-- Side Form Ends Here -->

	<!-- Enquire Popup Starts Here -->
	<div class="modal" id="enqueryForm" tabindex="-1" role="dialog"
		aria-labelledby="queryLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="queryLabel">Send Us Your Query</h4>
				</div>
				<div class="modal-body">
					<form class="form-area" id="enquiry_form" method="post"
						action="contactUs">
						<div class="form-group has-feedback">
							<input type="text" name="name" class="form-control"
								placeholder="Enter Your Name"> <i
								class="icon-user form-control-feedback"></i>
						</div>
						<div class="form-group has-feedback">
							<input type="text" name="email" class="form-control"
								placeholder="Enter E-mail ID"> <i
								class="icon-email3 form-control-feedback"></i>
						</div>
						<div class="form-group has-feedback">
							<input type="text" name="phone" class="form-control"
								placeholder="Enter Contact Number"> <i
								class="icon-phone form-control-feedback"></i>
						</div>
						<div class="form-group has-feedback">
							<input type="text" name="country" class="form-control"
								placeholder="Enter Country"> <i
								class="icon-globe4 form-control-feedback"></i>
						</div>
						<div class="form-group has-feedback">
							<textarea rows="8" name="message" class="form-control"
								placeholder="Message"></textarea>
							<i class="icon-comments form-control-feedback"></i>
						</div>
						<div class="form-group">
							<div class="input-group">
								<div class="input-group-addon captcha-num" id="captchaOperation"></div>
								<input type="text" name="captcha" id="captcha"
									class="form-control" placeholder="Solve the Captcha">
								<div class="input-group-addon reset">
									<i class="icon-reply"></i>
								</div>
							</div>
						</div>
						<button type="submit" class="btn btn-sendquery" id="send"
							name="send">Submit</button>
					</form>
				</div>
				<!-- End Modal Body -->
			</div>
		</div>
	</div>
	<!-- Enquire Popup Ends Here -->

	<!--Start of Zopim Live Chat Script-->
	<script type="text/javascript">
		window.$zopim || (function(d, s) {
			var z = $zopim = function(c) {
				z._.push(c)
			}, $ = z.s = d.createElement(s), e = d.getElementsByTagName(s)[0];
			z.set = function(o) {
				z.set._.push(o)
			};
			z._ = [];
			z.set._ = [];
			$.async = !0;
			$.setAttribute("charset", "utf-8");
			$.src = "//v2.zopim.com/?3sYnlcIsYBxgJlg5Fczo9X0tVM36lxVd";
			z.t = +new Date;
			$.type = "text/javascript";
			e.parentNode.insertBefore($, e)
		})(document, "script");
	</script>
	<!--End of Zopim Live Chat Script-->
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://code.jquery.com/ui/1.11.3/jquery-ui.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>

<script src="js/jquery-ui-timepicker.js"></script>
<script src="js/moment.js"></script>
<script src="js/fullcalendar.js"></script>
<script src="js/bootstrap-tagsinput.js"></script>


<!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
<script type="text/javascript"
	src="rs-plugin/js/jquery.themepunch.plugins.min.js"></script>
<script type="text/javascript"
	src="rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<!-- Dropdown Hover -->
<script src="js/jquery.bootstrap-dropdown-on-hover.min.js"></script>
<!-- Owl Carousel -->
<script src="js/owl.carousel.min.js"></script>
<!-- Form Validation -->
<script src="formvalidation/js/formValidation.min.js"></script>
<script src="formvalidation/js/framework/bootstrap.min.js"></script>
<script src="js/form-validation.js"></script>
<!-- Custom Javascript -->
<script src="js/customizer.js"></script>
<!-- <script src="js/bootstrap.js"></script>
 -->
<script src="js/custom.js"></script>

<script>
	$(document).ready(function() {
		var mySelect = $('#first-disabled2');

		$('#special').on('click', function() {
			mySelect.find('option:selected').prop('disabled', true);
			mySelect.selectpicker('refresh');
		});

		$('#special2').on('click', function() {
			mySelect.find('option:disabled').prop('disabled', false);
			mySelect.selectpicker('refresh');
		});

		$('#basic2').selectpicker({
			liveSearch : true,
			maxOptions : 1
		});

		popUp();

	});
	function popUp() {

		if ($("#result").text() != "" && $("#result").text().trim().length > 0) {

			/* $("body").css("opacity", "0.4"); */
			$("#result").show();
			if ("${msg}".length > 0) {
				$('.danger-main2').css("opacity", "1");
				$('.danger-main2').css("z-index", "11111");
				$("#result").css("color", "black");
			}

			else {
				$("#result").css("color", "red");
				$('.danger-main').css("opacity", "1");
				$('.danger-main').css("z-index", "11111");
			}

		} else {
			$("body").css("opacity", "1");
			$("#result").css("display", "none");
		}

		$('.clo').click(function() {
			$('.danger-main').css("opacity", "0");
			$('.danger-main').css("z-index", "-11111");

			$('.danger-main2').css("opacity", "0");
			$('.danger-main2').css("z-index", "-11111");
		});

	};
</script>

<script>
	function validateTime(startId, endId) {
		var startTime = $("#" + startId).val();
		var endTime = $("#" + endId).val();
		var timepattern = new RegExp("^(0[0-9]|1[0-9]|2[0-4]):[0-5][0-9]$");
		var res = timepattern.test(startTime);
		var res1 = timepattern.test(endTime);
		if (res && res1) {
			startDate = new Date();
			endDate = new Date();
			endDate.setTime(startDate.getTime());
			startDate.setHours(startTime.split(":")[0]);
			startDate.setMinutes(startTime.split(":")[1]);
			endDate.setHours(endTime.split(":")[0]);
			endDate.setMinutes(endTime.split(":")[1]);
			if (startDate < endDate) {
				var splits = startId.split("_");
				while (startDate < endDate) {
					$('#' + splits[0] + '_sessions')
							.tagsinput(
									'add',
									startDate.getHours() + ":"
											+ startDate.getMinutes());
					startDate.setMinutes(startDate.getMinutes() + 15);
				}
			} else
				alert('Invalid Time Range');
		} else {
			alert('Please enter valid time');
		}
		$("#" + startId).val('');
		$("#" + endId).val('');
	}

	$(function() {
		$('.basicExample').timepicker();

		$(".timePicker").timepicker({
			stepMinute : 15
		});
		$(".datepickFuture").datepicker({
			yearRange : '-100:+0',
			changeYear : true,
			changeMonth : true,
			minDate : "0",
			dateFormat : 'dd/mm/yy',
			beforeShow : function(input, inst) {
				var cal = inst.dpDiv;
				var top = $(this).offset().top + $(this).outerHeight();
				var left = $(this).offset().left;
				setTimeout(function() {
					cal.css({
						'top' : top,
						'left' : left
					});
				}, 10);
			}
		})

	});
</script>
<script>
	$('.date').datepicker({
		'format' : 'yyyy-m-d',
		'autoclose' : true
	});
</script>
<script src="js/bootstrap-select.js"></script>
</body>
</html>
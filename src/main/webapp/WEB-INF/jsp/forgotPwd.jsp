<%@ include file="newHeader.jsp"%></section>
<script type="text/javascript">
	window.onload = function() {
		$("#cssmenu").hide();
		$("#userName").hide();
	}
	function Submit() {
		if (isRequired("email") && validEmail("email")) {
			$(".custError").text("");
			return true;
		} else {
			return false;
		}
	}
</script>

 <section class="sec-main forget-main1">
            <div class="container">
            
            <form name='forgotPwd' action='forgotPwd' method='POST'  id="forgotPwdForm" >
                <div class="row">
                    <div class="col-lg-12">
                        <div class="forget-main">
                            <h3>Forget Your Password?</h3>
                            <p>Please enter the email address register on your account</p>
                        </div>
                    </div>
                </div>
                <div class="row form-group   has-feedback">
                    <div class="col-lg-3 col-md-3 ">

                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-3 col-xs-3">
                        <div class="sig-input forget">
                            <label>Email:</label>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-8 col-xs-8">
                        <div class="sig-input">
                            <input type="text" class="form-control"  name='username' > 
                            <button class="btn btn-primary btn-block" type="submit">Reset Password</button>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 ">

                    </div>
                </div>
                
                <input type="hidden" name="${_csrf.parameterName}"
							value="${_csrf.token}" />
				</form>
            </div>
        </section>
                <style>
            section.sec-main {
                background-color: #e4f4f8;
                min-height: 100%;
                padding-top: 30px;
                padding-bottom: 30px;
            }
            .sig-input button.btn.btn-danger.btn-block {
                margin-top: 20px;
            }
            .forget-main {
    text-align: center;
}
.sig-input button.btn.btn-primary.btn-block {
        margin-top: 20px;
        }
        </style>
        
<%@ include file="newFooter.jsp"%>
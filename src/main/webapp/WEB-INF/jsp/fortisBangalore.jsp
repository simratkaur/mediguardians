<%@ include file="newHeader.jsp"%></section>
	<!-- Banner Starts Here -->
	<div class="inner-banner">
		<img src="images/banners/fortis-bengaluru.jpg" alt="Fortis Hospital"
			class="img-responsive">
	</div>
	<!-- Banner Ends Here -->

	<!-- Hospital Short Details Starts Here -->
	<div class="short-details">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12">
					<div class="title">
						<h1>Fortis Hospital</h1>
						<p>Bengaluru, India</p>
					</div>
				</div>
				<!-- End Column -->
				<!-- <div class="col-md-6 col-sm-6">
                            <div class="site-link">
                                Website : <a href="javascript:;">http://www.aimshospital.org</a>
                            </div>
                        </div>End Column -->
			</div>
			<!-- End Row -->
		</div>
		<!-- End Container -->
	</div>
	<!-- Hospital Short Details Ends Here -->

	<!-- Hospital Details Starts Here -->
	<div class="container">
		<div class="detail-container">
			<div class="hospital-details">
				<div class="row">
					<div class="col-md-9">
						<div class="row">
							<div class="col-md-6 col-divider">
								<div class="address-box box-height">
									<ul>
										<li><strong>Address</strong><span>: 154/9,
												Bannerghatta Road, Opposite IIM-B</span></li>
										<li><strong>City</strong><span>: Bengaluru</span></li>
									</ul>
								</div>
							</div>
							<!-- End Column -->
							<div class="col-md-6 col-divider">
								<div class="number-box box-height">
									<ul>
										<li><strong>No. of Hospital Beds</strong><span>:
												400</span></li>
										<li><strong>No. of ICU Beds</strong><span>: 60</span></li>
										<li><strong>No. of Operating Rooms</strong><span>:
												9</span></li>
										<li><strong>Payment Mode</strong><span>: Cash,
												Bank Transfer</span></li>
										<li><strong>Avg. International Patients</strong><span>:
												3000+</span></li>
									</ul>
								</div>
							</div>
							<!-- End Column -->
						</div>
						<!-- End Row -->
					</div>
					<!-- End Column -->
					<div class="col-md-3">
						<div class="certificate-box box-height">
							<h4>Accreditations</h4>
							<ul>
								<li><img src="images/certificate/icon01.png" alt="Accreditations"
									class="img-responsive"></li>
							</ul>
						</div>
					</div>
					<!-- End Column -->
				</div>
				<!-- End Row -->
			</div>
			<!-- End Hospital Details -->
		</div>
		<!-- End Detail Container -->
	</div>
	<!-- Hospital Details Ends Here -->

	<!-- About Section Starts Here -->
	<div class="about-section">
		<div class="container">
			<div class="title">
				<h2>
					<span>About</span> Fortis Hospital
				</h2>
			</div>
			<div class="description">
				<p>The Fortis Hospital at Bannerghatta Road is a 400 bedded
					multi-speciality tertiary care hospital. It began operations in
					1999 and has established itself as a trusted institution in the
					field of healthcare and medical facilities. The hospital
					specialises in cutting edge medical technology and dedicated
					patient care services. Hospital has a large team of 150 experienced
					consultants and 1000 skillful para-medical staff.</p>
				<p>As one of the leading multi-speciality hospitals in the
					country, hospital offers tertiary care in over 40 specialties that
					include cardiology, cardiac surgery, orthopaedics, neurology,
					neuro-surgery, GI and Minimal Access Surgery (MAS), amongst other.
					The hospital has been instrumental in the introduction of
					facilities like the custom fit knee replacement and HIFU technology
					for prostate cancer in India. The hospital is equipped with
					state-of-the-art technologies like trans-radial angioplasty,
					trans-abdominal cardiac surgery, and computerised TKR navigation
					surgery. The hospital has been ranked as the Best Hospital in
					Bengaluru for Cardiac Care by the 8th Outlook Hospital Survey,
					2002.</p>
				<h5>
					To get best options for treatment at Fortis Hospital, send us a
					query or write to us at <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
				</h5>

			</div>
		</div>
		<!-- End Container -->
	</div>
	<!-- About Section Ends Here -->

	<%@ include file="newFooter.jsp"%>
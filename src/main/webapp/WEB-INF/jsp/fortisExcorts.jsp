<%@ include file="newHeader.jsp"%></section>
	<!-- Banner Starts Here -->
	<div class="inner-banner">
		<img src="images/banners/fortis-escorts.jpg"
			alt="Fortis Escorts Heart Institute" class="img-responsive">
	</div>
	<!-- Banner Ends Here -->

	<!-- Hospital Short Details Starts Here -->
	<div class="short-details">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12">
					<div class="title">
						<h1>Fortis Escorts Heart Institute</h1>
						<p>Delhi, India</p>
					</div>
				</div>
				<!-- End Column -->
				<!-- <div class="col-md-6 col-sm-6">
					<div class="site-link">
						Website : <a href="javascript:;">http://www.aimshospital.org</a>
					</div>
				</div>
				End Column -->
			</div>
			<!-- End Row -->
		</div>
		<!-- End Container -->
	</div>
	<!-- Hospital Short Details Ends Here -->

	<!-- Hospital Details Starts Here -->
	<div class="container">
		<div class="detail-container">
			<div class="hospital-details">
				<div class="row">
					<div class="col-md-9">
						<div class="row">
							<div class="col-md-6 col-divider">
								<div class="address-box box-height">
									<ul>
										<li><strong>Address</strong><span>: Okhla Road,
												Opp Holy Family Hospital, New Delhi, </span></li>
										<li><strong>City</strong><span>: New Delhi</span></li>
									</ul>
								</div>
							</div>
							<!-- End Column -->
							<div class="col-md-6 col-divider">
								<div class="number-box box-height">
									<ul>
										<li><strong>No. of Hospital Beds</strong><span>:
												280</span></li>
										<li><strong>No. of ICU Beds</strong><span>: </span></li>
										<li><strong>No. of Operating Rooms</strong><span>:
										</span></li>
										<li><strong>Payment Mode</strong><span>: Cash,
												Bank Transfer</span></li>
										<li><strong>Avg. International Patients</strong><span>:
												4000+</span></li>
									</ul>
								</div>
							</div>
							<!-- End Column -->
						</div>
						<!-- End Row -->
					</div>
					<!-- End Column -->
					<div class="col-md-3">
						<div class="certificate-box box-height">
							<h4>Accreditations</h4>
							<ul>
								<li><img src="images/certificate/icon01.png" alt="Accreditations"
									class="img-responsive"></li>
							</ul>
						</div>
					</div>
					<!-- End Column -->
				</div>
				<!-- End Row -->
			</div>
			<!-- End Hospital Details -->
		</div>
		<!-- End Detail Container -->
	</div>
	<!-- Hospital Details Ends Here -->

	<!-- About Section Starts Here -->
	<div class="about-section">
		<div class="container">
			<div class="title">
				<h2>
					<span>About</span> Fortis Escorts Heart Institute
				</h2>
			</div>
			<div class="description">
				<p>Fortis Escorts Heart Institute, formerly known as Escorts
					Heart Institute and Research Centre, is a pioneer in the field of
					fully dedicated cardiac care in India. It is the largest free
					standing private cardiac hospital in Asia Pacific region, and a
					part of Fortis Healthcare which is the fastest growing hospital
					network in India.</p>
				<p>Fortis Escorts Heart Institute has set benchmarks in cardiac
					care with Paediatric path breaking work over the past 25 years.
					Today, it is recognised world over as a centre of excellence
					providing the latest technology in Cardiac Bypass Surgery,
					Interventional Cardiology, Non-invasive Cardiology, Paediatric
					Cardiology and Paediatric Cardiac Surgery. The hospital is backed
					by the most advanced laboratories performing complete range of
					investigative tests in the field of Nuclear Medicine, Radiology,
					Biochemistry, Haematology, Transfusion Medicine and Microbiology.</p>
				<p>Fortis Escorts Heart Institute has a vast pool of talented
					and experienced team of doctors, who are further supported by a
					team of highly qualified, experienced &amp; dedicated support staff
					&amp; cutting edge technology like the recently installed Dual CT
					Scan. Currently, more than 200 cardiac doctors and 1600 employees
					work together to manage over 14,500 admissions and 7,200 emergency
					cases in a year.</p>
				<h5>
					To get best options for treatment at Fortis Hospital, send us a
					query or write to us at <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
				</h5>

			</div>
		</div>
		<!-- End Container -->
	</div>
	<!-- About Section Ends Here -->

	<%@ include file="newFooter.jsp"%>
<%@ include file="newHeader.jsp"%></section>
	<!-- Banner Starts Here -->
	<div class="inner-banner">
		<img src="images/banners/fortis-gurgaon.jpg"
			alt="Fortis Medical and Research Institute" class="img-responsive">
	</div>
	<!-- Banner Ends Here -->

	<!-- Hospital Short Details Starts Here -->
	<div class="short-details">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12">
					<div class="title">
						<h1>Fortis Medical and Research Institute</h1>
						<p>Gurgaon, Delhi NCR</p>
					</div>
				</div>
				<!-- End Column -->
				<!--  <div class="col-md-6 col-sm-6">
                            <div class="site-link">
                                Website : <a href="javascript:;">http://www.aimshospital.org</a>
                            </div>
                        </div>End Column -->
			</div>
			<!-- End Row -->
		</div>
		<!-- End Container -->
	</div>
	<!-- Hospital Short Details Ends Here -->

	<!-- Hospital Details Starts Here -->
	<div class="container">
		<div class="detail-container">
			<div class="hospital-details">
				<div class="row">
					<div class="col-md-9">
						<div class="row">
							<div class="col-md-6 col-divider">
								<div class="address-box box-height">
									<ul>
										<li><strong>Address</strong><span>: Gurgaon</span></li>
										<li><strong>City</strong><span>: Delhi NCR</span></li>
									</ul>
								</div>
							</div>
							<!-- End Column -->
							<div class="col-md-6 col-divider">
								<div class="number-box box-height">
									<ul>
										<li><strong>No. of Hospital Beds</strong><span>:
												1000</span></li>
										<li><strong>No. of ICU Beds</strong><span>: </span></li>
										<li><strong>No. of Operating Rooms</strong><span>:
										</span></li>
										<li><strong>Payment Mode</strong><span>: </span></li>
										<li><strong>Avg. International Patients</strong><span>:
										</span></li>
									</ul>
								</div>
							</div>
							<!-- End Column -->
						</div>
						<!-- End Row -->
					</div>
					<!-- End Column -->
					<!-- <div class="col-md-3">
                                <div class="certificate-box box-height">
                                    <h4>Accreditations</h4>
                                    <ul>
                                        <li><img src="images/certificate/icon01.png" alt="Accreditations" class="img-responsive"></li>
                                    </ul>
                                </div>
                            </div> -->
					<!-- End Column -->
				</div>
				<!-- End Row -->
			</div>
			<!-- End Hospital Details -->
		</div>
		<!-- End Detail Container -->
	</div>
	<!-- Hospital Details Ends Here -->

	<!-- About Section Starts Here -->
	<div class="about-section">
		<div class="container">
			<div class="title">
				<h2>
					<span>About</span> Fortis Medical and Research Institute
				</h2>
			</div>
			<div class="description">
				<p>Fortis Healthcare Limited is a leading, integrated healthcare
					delivery provider in the pan Asia- Pacific region. FHL incorporated
					in the year 1996 and in the short span of a decade, it has
					developed an asset base in 11 countries. Fortis Memorial Research
					Institute is a multi- super speciality, quaternary care hospital
					with reputed clinicians, including super- sub- specialists and
					speciality nurses, supported by cutting edge technology. A premium
					referral hospital, it endeavours to be the � Mecca of Healthcare�
					for Asia Pacific and beyond. This �Next Generation Hospital� is
					built on a foundation of �Trust� and rests on four strong pillars:
					Talent, Technology, Service and Infrastructure.</p>
				<p>The Comprehensive Medical Program in Fortis includes �
					Oncology, Paediatrics, Stem Cell program, Organ Transplant program,
					Neurosciences, Orthopaedics, G I and Hepatology, Renal Sciences,
					Bariatric &amp; Advanced MAS, MCH incl. IVF, Cosmetic,
					Reconstruction &amp; Plastic surgery, Cardiac Sciences, Critical
					Care Medicine, Urology.</p>
				<p>Fortis has State-of-the-Art Medical Technology &amp; Design
					which includes Pilot project of Brain Lab and LINAC, Digital
					Mammography, Critical Care and iCIP, Digital MRI, Open Lab, Stem
					Cell Lab, Time-of-Flight PET scan, 256-slice CT Scan, Digital
					Fluoroscopy, Integrated ORs and Intra-OP video link, Brain Suite
					with Carbon Fibre OR table, Bi-Plane Cath Lab, Endoscopic
					UltraSound &amp; Fibroscan, LASIK</p>
				<h5>
					To get best options for treatment at Fortis Hospital, send us a
					query or write to us at <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
				</h5>
			</div>
		</div>
		<!-- End Container -->
	</div>
	<!-- About Section Ends Here -->

	<%@ include file="newFooter.jsp"%>
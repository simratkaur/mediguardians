<%@ include file="newHeader.jsp"%></section>
	<!-- Banner Starts Here -->
	<div class="inner-banner">
		<img src="images/banners/fortis-vasantkunj.jpg"
			alt="Fortis Flt. Lt. Rajan Dhall Hospital" class="img-responsive">
	</div>
	<!-- Banner Ends Here -->

	<!-- Hospital Short Details Starts Here -->
	<div class="short-details">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12">
					<div class="title">
						<h1>Fortis Flt. Lt. Rajan Dhall Hospital</h1>
						<p>Delhi, India</p>
					</div>
				</div>
				<!-- End Column -->
				<!-- <div class="col-md-6 col-sm-6">
                            <div class="site-link">
                                Website : <a href="javascript:;">http://www.aimshospital.org</a>
                            </div>
                        </div>End Column -->
			</div>
			<!-- End Row -->
		</div>
		<!-- End Container -->
	</div>
	<!-- Hospital Short Details Ends Here -->

	<!-- Hospital Details Starts Here -->
	<div class="container">
		<div class="detail-container">
			<div class="hospital-details">
				<div class="row">
					<div class="col-md-9">
						<div class="row">
							<div class="col-md-6 col-divider">
								<div class="address-box box-height">
									<ul>
										<li><strong>Address</strong><span>: Sector B,
												Pocket 1, Aruna Asaf Ali Marg,Vasant Kunj</span></li>
										<li><strong>City</strong><span>: Delhi</span></li>
									</ul>
								</div>
							</div>
							<!-- End Column -->
							<div class="col-md-6 col-divider">
								<div class="number-box box-height">
									<ul>
										<li><strong>No. of Hospital Beds</strong><span>:
												20</span></li>
										<li><strong>No. of ICU Beds</strong><span>: 30</span></li>
										<li><strong>No. of Operating Rooms</strong><span>:
												6</span></li>
										<li><strong>Payment Mode</strong><span>: Cash,
												Bank Transfer</span></li>
										<li><strong>Avg. International Patients</strong><span>:
												3000+</span></li>
									</ul>
								</div>
							</div>
							<!-- End Column -->
						</div>
						<!-- End Row -->
					</div>
					<!-- End Column -->
					<div class="col-md-3">
						<div class="certificate-box box-height">
							<h4>Accreditations</h4>
							<ul>
								<li><img src="images/certificate/icon01.png" alt="Accreditations"
									class="img-responsive"></li>
							</ul>
						</div>
					</div>
					<!-- End Column -->
				</div>
				<!-- End Row -->
			</div>
			<!-- End Hospital Details -->
		</div>
		<!-- End Detail Container -->
	</div>
	<!-- Hospital Details Ends Here -->

	<!-- About Section Starts Here -->
	<div class="about-section">
		<div class="container">
			<div class="title">
				<h2>
					<span>About</span> Fortis Flt. Lt. Rajan Dhall Hospital
				</h2>
			</div>
			<div class="description">
				<p>Fortis Flt. Lt. Rajan Dhall Hospital is a 200 bed NABH
					certified multi-speciality tertiary care hospital. Spread over
					1,50,000 sq. ft, it is a manifestation of Fortis Healthcare's
					vision of providing a world-class integrated healthcare delivery
					system in India with the finest medical skills and compassionate
					patient care. We aim at delivering a comfortable ambience to all
					our patients� making them feel at home.</p>
				<p>Our approach is based on patient centricity, state-of-the-art
					emergency response, integrity, teamwork, ownership and innovation.
					We aim at providing compassionate patient care with our clinical
					excellence, toward achieving a single-minded objective��Saving and
					Enriching lives�.</p>
				<h5>
					To get best options for treatment at Fortis Hospital, send us a
					query or write to us at <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
				</h5>

			</div>
		</div>
		<!-- End Container -->
	</div>
	<!-- About Section Ends Here -->

	<%@ include file="newFooter.jsp"%>
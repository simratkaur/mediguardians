<%@ include file="newHeader.jsp"%></section>
	<!-- Hospital Short Details Starts Here -->
	<div class="short-details">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="title">
						<h1>Bariatric Treatment</h1>
						<ul class="paginator">
							<li><a href="/">Home</a></li>
							<li><a>Bariatric Treatment</a></li>
						</ul>
					</div>
				</div>
				<!-- End Column -->
			</div>
			<!-- End Row -->
		</div>
		<!-- End Container -->
	</div>
	<!-- Hospital Short Details Ends Here -->

	<!-- Specialty Content Starts Here -->
	<div class="speciality-content-area">
		<div class="container">

			<div class="innertab-accordion">
				<div class="row">
					<div class="col-md-3 tabnopaddright">
						<div class="tabs">
							<ul>
								<li><a href="bariatric-surgery">Bariatric
										Treatment</a></li>
								<li class="tab-active"><a href="gastric-bypass-surgery">Gastric Bypass</a></li>
								<li><a href="gastric-sleeve-surgery">Sleeve Gastrectomy </a></li>
							</ul>
						</div>
					</div>
					<!-- End Column -->
					<div class="col-md-9 tabnopaddleft">
						<div class="accordion-contentarea">

<div id="tab2" >
								<h2>
									<span>Gastric Bypass </span>- India
								</h2>
								<div class="row">
									<div class="col-md-9 col-sm-9 nopaddleft">
										<p>We at Arvene Healthcare have the expertise to organize for all treatments and surgeries related to Bariatric (Weight Loss). Arvene Healthcare is associated with top hospitals and Doctors of India in this field. We can give you more and better options so that you can take the right decisions. We also assist you in taking best decision based on your requirement. Arvene Healthcare will also arrange for your stay and local travel, so that you have nothing else to worry about but your treatment. Please contact us on <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a> today to avail best services.</p>
										<h3>
										Gastric Bypass:
										</h3>
										<p>
										This procedure can improve the overall quality of life not only in terms of appearance but also reduce a number of health problems that overweight people are prone to suffer like heart problems and diabetes.
										</p><br>
										<p>
										The procedure of gastric bypass surgery has been specifically designed for reducing the food intake for those who have failed to reduce their weight by every means. The purpose of the procedure is to make changes in the digestive system so as to limit the consumption of food. 
										</p>
										<h3>
											<span>Types of </span>Gastric Bypass Surgery
										</h3>
									</div>
									<!-- End Inner Column -->
									<div class="col-md-3 col-sm-3 nopaddright">
										<img src="images/specialities/47.Gastric Bypass surgery.jpg"
											alt="Gastric Bypass surgery" class="img-responsive">
									</div>
									<!-- End Inner Column -->
								</div>
								<!-- End Inner Row -->
								<p>
									<strong>Biliopancreatic Diversion Bypass </strong> 
								</p>
								<p>
									<strong>Roux-en-Y Gastric Bypass </strong> 
								</p>
								<h3>
									<span>Who should consider </span>Gastric Bypass Surgery?
								</h3>
								<ul>
									<li><span>BMI (Body Mass Index) of 40 or more.</span></li>
									<li><span>Weight of 100 pounds or more than ideal body weight.</span></li>
									<li><span>Associated serious health problems of heart disease and diabetes due to excess weight and who have a BMI of 35 or more.</span></li>
								</ul>
								<h3>
									<span>Procedure for </span>Gastric Bypass Surgery
								</h3>
								<ul>
									<li><span>The procedure is performed under general anaesthesia.</span></li>
									<li><span>The procedure begins by making the stomach smaller. Staples are used for dividing the stomach into two sections- a larger bottom section and a small upper section. Pouch (top section) is that part where the food eaten is stored. This pouch can hold around one ounce of food and is of walnut shape.</span></li>
									<li><span>A bypass is the next step. The small part of the small intestine is connected to a small hole in the pouch. The food will travel from this top section of the stomach (pouch) into the small intestine. Due to this entire process, the food will absorb less calories.
 
</span></li>
								</ul>
								<h3>
									<span>Gastric Bypass </span>Sleeve Surgery
								</h3>
								<p>Gastric sleeve surgery, or sleeve gastrectomy or laparoscopic sleeve gastrectomy and sleeve resection, is a newer type of Bariatric surgery.  The purpose of this surgery is to limit the amount of food that a stomach can hold. The other purpose is to remove a portion of the stomach that produces the hormone known as Ghrelin (a hormone that causes hunger). </p>
								<h3>
									<span>Benefits of </span>Sleeve Gastrectomy
								</h3>
								<ul>
									<li><span>The procedure can be performed laparoscopically.</span></li>
									<li><span>The procedure increases the feeling of fullness and reduces the volume of the stomach.</span></li>
									<li><span>The procedure is simpler than gastric bypass.</span></li>
									<li><span>Allows normal functioning of the stomach and the food is eaten in smaller quantities.</span></li>
									<li><span>The surgery involves less operative time.</span></li>
									<li><span>There is a comparatively less stay in the hospital as compared to bypass surgeries.</span></li>
									<li><span>The Ghrelin hormone is removed.</span></li>
									<li><span>It does not require an intestinal bypass.</span></li>
									<li><span>There is no dumping syndrome due to the pyloric portion of the stomach is left intact.</span></li>
								</ul>
								<h3>
									<span>Gastric Bypass Surgery </span>Recovery
								</h3>
								<p>After the surgery, the stay in the hospitals is about 2-3 days and in the case of laparoscopic procedure the stay is about 1-2 days.  Normally, the recovery period is about 5 weeks.</p>
								<h3>
									<span>Cost of </span>Gastric Bypass Surgery
								</h3>
								<p>The low cost of laparoscopic procedure attracts a large number of patients from across the world to India. All the surgeries in India are performed by trained surgeons and they also possess relevant training from the developed countries.</p>
Gastric Bypass surgery <h5>Cost in India starts at 5300 US Dollars. To know more about it please share your reports on <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a> or whatsapp/viber/imo to +918130077375.</h5><p> 
(Please note this is a tentative plan and actual Costs are based on reports shared. Final treatment plan and estimation will be provided after the patient has been evaluated.)</p>
								<h5>
									To get best options for Bariatric treatment, send us a query or
									write to us at <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
								</h5>
							</div>
							<!-- End Tab -->

						</div>
					</div>
					<!-- End Column -->
				</div>
				<!-- End Row -->
			</div>
			<!-- End Tab Accordion -->

		</div>
		<!-- End Container -->
	</div>
	<!-- Speciality Content Ends Here -->

	<%@ include file="newFooter.jsp"%>
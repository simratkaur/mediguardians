<%@ include file="newHeader.jsp"%></section>
	<!-- Hospital Short Details Starts Here -->
	<div class="short-details">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="title">
						<h1>Bariatric Treatment</h1>
						<ul class="paginator">
							<li><a href="/">Home</a></li>
							<li><a>Bariatric Treatment</a></li>
						</ul>
					</div>
				</div>
				<!-- End Column -->
			</div>
			<!-- End Row -->
		</div>
		<!-- End Container -->
	</div>
	<!-- Hospital Short Details Ends Here -->

	<!-- Specialty Content Starts Here -->
	<div class="speciality-content-area">
		<div class="container">

			<div class="innertab-accordion">
				<div class="row">
					<div class="col-md-3 tabnopaddright">
						<div class="tabs">
							<ul>
								<li><a href="bariatric-surgery">Bariatric
										Treatment</a></li>
								<li><a href="gastric-bypass-surgery">Gastric Bypass</a></li>
								<li class="tab-active"><a href="gastric-sleeve-surgery">Sleeve Gastrectomy </a></li>
							</ul>
						</div>
					</div>
					<!-- End Column -->
					<div class="col-md-9 tabnopaddleft">
						<div class="accordion-contentarea">
							<div id="tab3" >
								<h2>
									<span>Laparoscopic Sleeve Gastrectomy</span> � India
								</h2>
								<div class="row">
									<div class="col-md-9 col-sm-9 nopaddleft">
										<p>We at Arvene Healthcare have the expertise to organize for all treatments and surgeries related to Bariatric (Weight Loss). Arvene Healthcare is associated with top hospitals and Doctors of India in this field. We can give you more and better options so that you can take the right decisions. We also assist you in taking best decision based on your requirement. Arvene Healthcare will also arrange for your stay and local travel, so that you have nothing else to worry about but your treatment. Please contact us on <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a> today to avail best services.</p>
									</div>
									<!-- End Inner Column -->
									<div class="col-md-3 col-sm-3 nopaddright">
										<img src="images/specialities/48. sleeve gastrectomy.jpg"
											alt="sleeve gastrectomy" class="img-responsive">
									</div>
									<!-- End Inner Column -->
								</div>
								<!-- End Inner Row -->
								<h3>
									Gastric Sleeve Surgery:
								</h3>
								<p>
								Gastric sleeve surgery, or sleeve gastrectomy or laparoscopic sleeve gastrectomy and sleeve resection, is a newer type of Bariatric surgery.  The purpose of this surgery is to limit the amount of food that a stomach can hold. The other purpose is to remove a portion of the stomach that produces the hormone known as Ghrelin (a hormone that causes hunger). 
								</p><br>
								<p>
								It�s a non- reversible procedure that decreases the size of the stomach by 85%. There are less chances of stomach expansion over the time as compared to other Bariatric surgeries. 
								</p>
								<h3>
									<span>Benefits of </span>Laparascopic Sleeve Gastrectomy
								</h3>
								<ul>
									<li><span>The procedure can be performed laparoscopically.</span></li>
									<li><span>The procedure increases the feeling of fullness and reduces the volume of the stomach.</span></li>
									<li><span>The procedure is simpler than gastric bypass.</span></li>
									<li><span>Allows normal functioning of the stomach and the food is eaten in smaller quantities.</span></li>
									<li><span>The surgery involves less operative time.</span></li>
									<li><span>There is a comparatively less stay in the hospital as compared to bypass surgeries.</span></li>
									<li><span>The Ghrelin hormone is removed.</span></li>
									<li><span>It does not require an intestinal bypass.</span></li>
								</ul>
								<h3>
									<span>Cost of </span>Laparoscopic Sleeve Gastrectomy
								</h3>
								<p>Compared to the surgical costs in Europe and America, the cost of Laparoscopic Sleeve Gastrectomy in India is substantially lower coupled with excellent health care facilities.</p>
								<h5>
									To get best options for Bariatric treatment, send us a query or
									write to us at <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
								</h5>
							</div>
							<!-- End Tab -->

						</div>
					</div>
					<!-- End Column -->
				</div>
				<!-- End Row -->
			</div>
			<!-- End Tab Accordion -->

		</div>
		<!-- End Container -->
	</div>
	<!-- Speciality Content Ends Here -->

<%@ include file="newFooter.jsp"%>
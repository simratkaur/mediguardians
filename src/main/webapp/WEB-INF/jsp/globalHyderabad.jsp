<%@ include file="newHeader.jsp"%></section>
<!-- Banner Starts Here -->
<div class="inner-banner">
	<img src="images/banners/global-hyderabad.jpg"
		alt="METRO Hospital" class="img-responsive">
</div>
<!-- Banner Ends Here -->

<!-- Hospital Short Details Starts Here -->
<div class="short-details">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<div class="title">
					<h1>Global Hospital</h1>
					<p>Hyderabad, India</p>
				</div>
			</div>
			<!-- End Column -->
			<!-- <div class="col-md-6 col-sm-6">
                            <div class="site-link">
                                Website : <a href="javascript:;">http://www.aimshospital.org</a>
                            </div>
                        </div>End Column -->
		</div>
		<!-- End Row -->
	</div>
	<!-- End Container -->
</div>
<!-- Hospital Short Details Ends Here -->

<!-- Hospital Details Starts Here -->
<div class="container">
	<div class="detail-container">
		<div class="hospital-details">
			<div class="row">
				<div class="col-md-9">
					<div class="row">
						<div class="col-md-6 col-divider">
							<div class="address-box box-height">
								<ul>
									<!-- <li><strong>Address</strong><span>: Ponekkara, P. O Kochi, Kochi, Kerala</span></li> -->
									<li><strong>City</strong><span>: Hyderabad</span></li>
								</ul>
							</div>
						</div>
						<!-- End Column -->
						<div class="col-md-6 col-divider">
							<div class="number-box box-height">
								<ul>
									<li><strong>No. of Hospital Beds</strong><span>: 200</span></li>
									<li><strong>No. of ICU Beds</strong><span>: 30</span></li>
									<li><strong>No. of Operating Rooms</strong><span>: 6
									</span></li>
									<li><strong>Payment Mode</strong><span>: cash, bank transfer</span></li>
									<li><strong>Avg. International Patients</strong><span>: 3000+
									</span></li>
								</ul>
							</div>
						</div>
						<!-- End Column -->
					</div>
					<!-- End Row -->
				</div>
				<!-- End Column -->
				<!-- <div class="col-md-3">
                                <div class="certificate-box box-height">
                                    <h4>Accreditations</h4>
                                    <ul>
                                        <li><img src="images/certificate/icon01.png" alt="Accreditations" class="img-responsive"></li>
                                    </ul>
                                </div>
                            </div>End Column -->
			</div>
			<!-- End Row -->
		</div>
		<!-- End Hospital Details -->
	</div>
	<!-- End Detail Container -->
</div>
<!-- Hospital Details Ends Here -->

<!-- About Section Starts Here -->
<div class="about-section">
	<div class="container">
		<div class="title">
			<h2>
				<span>About</span> Global Hospital
			</h2>
		</div>
		<div class="description">
			<p>Global Hospitals Group, India’s most renowned healthcare
				services provider offering better care, cutting-edge research and
				advanced education to caregivers, is one of the country’s fast
				growing chains of Multi Super Specialty Tertiary Care Hospitals
				offering healthcare services of international standards. A 2000-bed
				Multi Super Specialty Tertiary Care facility spread across
				Hyderabad, Chennai, Bangalore and Mumbai, Global Hospitals is a
				pioneer in Multi-Organ Transplants including kidneys, liver, heart
				and lung.</p>
			<p>Global Hospitals Group is committed to providing world-class
				tertiary healthcare to people in India and abroad by fusing the
				benefits of modern technology with the clinical acumen of the
				leading specialists in their respective fields.</p>
			<p>The Mission of Global Hospitals Group is to achieve the dream
				of a healthy world through continuous innovation, dedication to
				quality, provision of compassionate and affordable medical services.</p>
			<p>Located strategically across major cities in India, Global
				Hospitals Group offer a unique environment where friendly attitude
				and personal touch of a neighbourhood hospital merge with
				leading-edge technologies, extensive research resources and
				tremendous opportunities of a prestigious healthcare network. The
				result is a dynamic and rewarding setting delivering better clinical
				outcomes for patients by doctors, nursing and allied health
				professionals.</p>
			<h5>
				To get best options for treatment at Global Hospital, send us a
				query or write to us at <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
			</h5>

		</div>
	</div>
	<!-- End Container -->
</div>
<!-- About Section Ends Here -->

<%@ include file="newFooter.jsp"%>
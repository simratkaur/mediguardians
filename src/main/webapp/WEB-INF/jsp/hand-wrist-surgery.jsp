<%@ include file="newHeader.jsp"%></section>
<!-- Hospital Short Details Starts Here -->
<div class="short-details">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="title">
					<h1>Orthopedic Surgery</h1>
					<ul class="paginator">
						<li><a href="/">Home</a></li>
						<li><a>Orthopedic Surgery</a></li>
					</ul>
				</div>
			</div>
			<!-- End Column -->
		</div>
		<!-- End Row -->
	</div>
	<!-- End Container -->
</div>
<!-- Hospital Short Details Ends Here -->

<!-- Specialty Content Starts Here -->
<div class="speciality-content-area">
	<div class="container">

		<div class="innertab-accordion">
			<div class="row">
				<div class="col-md-3 tabnopaddright">
					<div class="tabs">
						<ul>
							<li><a href="orthopaedics-surgery">Orthopedic
									Surgery</a></li>
							<li><a href="knee-replacement-surgery">Knee Replacement</a></li>
							<li><a href="hip-replacement-surgery">Hip Replacement
									surgery </a></li>
							<li><a href="shoulder-surgery">Shoulder Surgery </a></li>
							<li><a href="foot-ankle-surgery">Foot and Ankle surgery</a></li>
							<li class="tab-active"><a href="hand-wrist-surgery">Hand Wrist surgery</a></li>
							<li><a href="elbow-replacement-surgery">Elbow Surgery</a></li>
						</ul>
					</div>
				</div>
				<!-- End Column -->
				<div class="col-md-9 tabnopaddleft">
					<div class="accordion-contentarea">
<div id="tab6">
							<h2>
								<span>Hand &amp; Wrist Surgery </span>- India
							</h2>
							<div class="row">
								<div class="col-md-9 col-sm-9 nopaddleft">
									<p>We at Arvene Healthcare have the expertise to organize
										for all treatments and surgeries related to Hand and Wrist
										Surgery. Arvene Healthcare is associated with top hospitals
										and Doctors of India in this field. We can give you more and
										better options so that you can take the right decisions. We
										also assist you in taking best decision based on your
										requirement. Arvene Healthcare will also arrange for your stay
										and local travel, so that you have nothing else to worry about
										but your treatment. Please contact us on
										<a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a> today to avail best services.</p>
									<br>
									<p>Hand and wrist surgery refers to the treatment of bones
										and soft tissue injuries with the use of the latest equipments
										and tools.</p>
									<h3>
										<span>Who should consider hand &amp; </span>wrist surgery?
									</h3>
									<p>The movement and power in hands and wrists are
										controlled by muscles and tendons. Those who need it, surgery
										is a great help in relieving pain and in the improvement of
										the functioning of the hand.</p>
								</div>
								<!-- End Inner Column -->
								<div class="col-md-3 col-sm-3 nopaddright">
									<img src="images/specialities/36.hand & wrist.jpg"
										alt="hand & wrist" class="img-responsive">
								</div>
								<!-- End Inner Column -->
							</div>
							<!-- End Inner Row -->
							<h3>
								<span>Most Common Hand and </span>Wrist Injuries
							</h3>
							<p>
								<strong>Following are some of hand and wrist conditions
									which need surgery: </strong>
							</p>
							<p>
								A. <strong>Bone and Joint Problems of the Wrist and
									Hand- </strong> Fractures and dislocations, Arthritis, Joint
								contractures and Amputations
							</p>
							<p>
								B. <strong>Muscle, Tendon and Ligament- </strong>Tendon
								lacerations (ruptures), Ligament and cartilage injuries and
								Tendon inflammation
							</p>
							<p>
								C. <strong>Nerve- </strong>Nerve laceration or palsy and Nerve
								compression (carpal tunnel syndrome)
							</p>
							<p>
								D. <strong>Skin and Soft Tissue Issues</strong>Ganglion cysts in
								wrists and hands, Foreign body evaluation and removal, Nailbed
								and fingertip injuries, Infections, burns, or contractures and
								Skin grafts
							</p>
							<p>
								E. <strong>Vascular- </strong>Microvascular repair and Surgical
								treatment of vasospastic disease
							</p>
							<p>
								F. <strong>Congenital- </strong>Duplicate thumb
							</p>
							<br>
							<p>The main advantages of hand and wrist surgeries are
								immediate relief from pain, improvement in hand function and
								appearance.</p>
							<h3>
								<span>Hand Joint Replacement </span>Surgery
							</h3>
							<p>Hand surgery deals with problems of the fingers, hands and
								wrists. This procedure helps in improving the range of motion,
								for repairing muscles, tendons or damage in bones due to injury
								or disease. It also helps in restoration of the appearance of
								hands and fingers, due to any reason.</p>
							<h3>
								<span>Who should consider</span> Hand Surgery?
							</h3>
							<p>Hand injuries in automobile accidents or other forms of
								trauma can get treated</p>
							<ul>
								<li><span>Restoration of hand function - range of
										motion</span></li>
								<li><span>Advantageous for people with medical
										conditions such as carpal tunnel syndrome, arthritis or
										osteoporosis</span></li>
								<li><span>Reduce pain</span></li>
								<li><span>Restoration of range of motion</span></li>
								<li><span>Patients suffering from birth defects or
										severe infections can get treated</span></li>
							</ul>
							<h3>
								<span>What Are the Different Kinds of </span>Orthopaedic Hand
								Surgery?
							</h3>
							<p>Common types of hand surgeries are:</p>
							<ul>
								<li><span>Tendon or nerve repair</span></li>
								<li><span>Skin grafts</span></li>
								<li><span>Closed Reduction and Fixation</span></li>
								<li><span>Joint replacement</span></li>
							</ul>
							<h3>
								<span>Wrist Injuries and </span>Disorders
							</h3>
							<p>Wrist of human beings is a complex structure of our hand,
								which consists of eight kinds of carpal bones.</p>
							<p>
								<strong>There are various conditions seen in the wrist
									joint: </strong>
							</p>
							<p>
								<strong>Ligament Injury-</strong>Writs have many ligaments (soft
								tissues) to connect bones to each other. If due to any trauma,
								it gets ruptured, this surgery is performed.
							</p>
							<p>
								<strong>Fracture-</strong>This is the most commonly problem.
							</p>
							<p>
								<strong>Arthroscopy-</strong>it is useful in a number of
								conditions like removal scar tissues, assessment of joint
								surface etc.
							</p>
							<p>
								<strong>Lumps and Bumps</strong>It is a form of a lump/ cyst. It
								might be painful and surgery is required.
							</p>
							<p>
								<strong>Carpal Tunnel Syndrome</strong>This is a painful
								condition in wrist, hands and arms that is caused due to
								depression of the median nerve.
							</p>
							<h3>
								<u>Cost of Hand and Wrist Surgery in India</u>
							</h3>
							<p>The major benefit of hand and wrist surgery in India is
								its price that is absolutely affordable. Any surgery involved
								here is done at pocket-friendly. A patient can avail such
								excellent-quality services internationally-trained medical
								practitioners. In the caseof these surgeries, state-of-the-art
								equipments are used.</p>
							<h5>
								To get best options for Orthopaedic treatment, send us a query
								or write to us at <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
							</h5>
						</div>

					</div>
				</div>
				<!-- End Column -->
			</div>
			<!-- End Row -->
		</div>
		<!-- End Tab Accordion -->

	</div>
	<!-- End Container -->
</div>
<!-- Speciality Content Ends Here -->

<%@ include file="newFooter.jsp"%>
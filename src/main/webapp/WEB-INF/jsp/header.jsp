<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@page import="com.medicine.arvene.util.CommonUtil"%>
<%@page session="true"%>
<html lang="en">
<head>
<%@ page contentType="text/html; charset=UTF-8"%>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta name="google-site-verification"
	content="eubmVPqv0Q4YvZRLYMfdDkp4GF5sjTzMXOvnTPNb5Nc" />
<meta name="robots" content="index, follow"/>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title>${CommonUtil.getPageTitle(pageContext.request.servletPath)}</title>
<meta name="DESCRIPTION"
	content="<c:out value="${CommonUtil.getPageDesc(pageContext.request.servletPath)}" />">
<meta name="KEYWORDS"
	content="<c:out value="${CommonUtil.getPageKeywords(pageContext.request.servletPath)}" />">
<!-- Bootstrap -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- Medical Guide Icons -->
<link rel="stylesheet" href="css/medical-guide-icons.css">
<!-- Medi Guardians Custom Stylesheet -->
<link rel="stylesheet" href="css/medi-guardian.css">
<!-- Media Queries Stylesheet -->
<link rel="stylesheet" href="css/media-queries.css">
<!-- SLIDER REVOLUTION 4.x CSS SETTINGS -->
<link rel="stylesheet" type="text/css" href="rs-plugin/css/settings.css"
	media="screen" />
<link rel="stylesheet" type="text/css"
	href="rs-plugin/css/extralayers.css" media="screen" />
<!-- Animations css -->
<link rel="stylesheet" href="css/animations.css">
<!-- Owl Carousel -->
<link rel="stylesheet" href="css/owl.carousel.css">
<link rel="stylesheet" href="css/owl.theme.css">
<!-- Yamm Megamenu CSS -->
<link rel="stylesheet" href="css/yamm.css">

<link rel="stylesheet" href="css/bootstrap-dialog.css"></link>
<link rel="stylesheet" href="css/fullcalendar.css"></link>
<link rel="stylesheet" href="css/bootstrap-tagsinput.css"></link>

<!-- Form Validation -->
<link rel="stylesheet" href="formvalidation/css/formValidation.min.css" >
<link rel="icon" href="images/favicon.ico" type="image/x-icon">
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" />
<link rel="stylesheet"
	href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="css/bootstrap-select.css">
<link rel="stylesheet" type="text/css" href="css/font-awesome.css">
<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
<link rel="stylesheet" href="css/medi-guardian.css">

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="js/bootstrap-paginator.js"></script>
<!--  AUTOCOMPLETE-->
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/jquery.devbridge-autocomplete/1.2.26/jquery.autocomplete.min.js"></script>
<script type="text/javascript">
	function myFunction() {
		window
				.open(
						"terms-conditions",
						"Terms and Conditions",
						"width=800, height=500,top=100,left=300,location=no,menubar=no,scrollbars=yes,toolbar=no,directories=0,titlebar=0,toolbar=0,location=0,status=0,menubar=0");
	}
	(function($) {
		$(window)
				.load(
						function() {
							$('a')
									.filter(
											function() {
												return this.href
														.match(/.*\.(zip|mp3*|mpe*g|pdf|docx*|pptx*|xlsx*|rar*)(\?.*)?$/);
											}).click(
											function(e) {
												ga('send', 'event', 'download',
														'click', this.href);
											});

							$('a[href^="mailto"]').click(
									function(e) {
										ga('send', 'event', 'email', 'send',
												this.href);
									});

							$('a[href^="http"]')
									.filter(
											function() {
												if (!this.href
														.match(/.*\.(zip|mp3*|mpe*g|pdf|docx*|pptx*|xlsx*|rar*)(\?.*)?$/)) {
													if (this.href
															.indexOf('arveneHealthcare.com') == -1)
														return this.href;
												}
											}).click(
											function(e) {
												ga('send', 'event', 'outbound',
														'click', this.href);
											});

							$('a')
									.filter(
											function() {
												if (this.href
														.indexOf('arveneHealthcare.com') != -1
														|| this.href
																.indexOf('://') == -1)
													return this.hash;
											}).click(
											function(e) {
												ga('send', 'event', 'hashmark',
														'click', this.href);
											});
						});
	})(jQuery);
</script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-87289439-1', 'auto');
  ga('send', 'pageview');
</script>

<!-- Google Tag Manager -->
<script>
	(function(w, d, s, l, i) {
		w[l] = w[l] || [];
		w[l].push({
			'gtm.start' : new Date().getTime(),
			event : 'gtm.js'
		});
		var f = d.getElementsByTagName(s)[0], j = d.createElement(s), dl = l != 'dataLayer' ? '&l='
				+ l
				: '';
		j.async = true;
		j.src = 'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
		f.parentNode.insertBefore(j, f);
	})(window, document, 'script', 'dataLayer', 'GTM-MWXWB6M');
</script>
<!-- End Google Tag Manager -->
 <meta name="msvalidate.01" content="FC04AB12C3254744FC0A8FBA4488A6A9" />
</head>
<body>
	<!-- Google Tag Manager (noscript) -->
	<noscript>
		<iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MWXWB6M"
			height="0" width="0" style="display: none; visibility: hidden"></iframe>
	</noscript>
	<!-- End Google Tag Manager (noscript) -->

	<!-- Preloader Starts Here -->
	<div class="preloader"></div>
	<!-- Preloader Ends Here -->
	<div id="header" class="header medi">
		<!-- Top Bar Starts Here -->
		<div class="top-bar">
			<div class="container">
				<div class="row">
					<div class="col-md-12">

						<div class="left-content">
							<!-- <div id="google_translate_element"></div>
							<script type="text/javascript">
								function googleTranslateElementInit() {
									new google.translate.TranslateElement(
											{
												pageLanguage : 'en',
												layout : google.translate.TranslateElement.InlineLayout.SIMPLE
											}, 'google_translate_element');
								}
							</script>
							<script type="text/javascript"
								src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
 -->
						</div>
						<!-- End Left Content -->

						<div class="right-content">
							<ul>
								<li><a><img
										src="http://icons.iconarchive.com/icons/chrisbanks2/cold-fusion-hd/24/viber-icon.png">
										<img
										src="http://icons.iconarchive.com/icons/alecive/flatwoken/24/Apps-Whatsapp-icon.png">
										<i class="icon-phone4"></i>(+91) 81300-77375</a></li>
								<li><a href="mailto:contact@arvenehealthcare.com"><i
										class="icon-mail"></i>contact@arvenehealthcare.com</a></li>
								<c:choose>
									<c:when
										test="${pageContext.request.userPrincipal.name == null}">
										<li><a href="#signup" data-toggle="modal">Signup</a></li>
										<li><a href="#signin" data-toggle="modal">Login</a></li>
										<li><a href="specialistRegister">Dr Signup</a></li>
									</c:when>
									<c:otherwise>
										<c:choose>
											<c:when test="${userType == 'S'}">
												<li id="profile"><a href="profile">Profile</a></li>
												<li id="availability"><a href="availability">Availability</a></li>
											</c:when>
										</c:choose>
										<li class="dropdown consultation"><a href="javascript:;"
											class="dropdown-toggle" data-toggle="dropdown">My
												Consulations</a>
											<ul class="dropdown-menu animated pull-center"
												style="font-size: initial; z-index: 10000; background-color: #525866"
												data-animation="fadeInDown">
												<li style="display: list-item"><a href="queries">Queries</a></li>
												<li style="display: list-item"><a href="bookings">Sessions</a></li>
											</ul></li>
										<li><a href="<c:url value="j_spring_security_logout" />">Log
												out</a></li>
									</c:otherwise>
								</c:choose>
							</ul>
							<form action="${logoutUrl}" method="post" id="logoutForm">
								<input type="hidden" name="${_csrf.parameterName}"
									value="${_csrf.token}" />
							</form>
							<!-- <ul class="language-options">
                                            <li class="fb"><a href="javascript:;"><i class="icon-euro"></i></a></li>
                                            <li class="tw"><a href="javascript:;"><i class="icon-yen"></i></a></li>
                                            <li class="gp"><a href="javascript:;"><i class="icon-caddieshoppingstreamline"></i></a></li>
                                        </ul> -->
						</div>
						<!-- End Right Content -->

					</div>
					<!-- End Column -->
				</div>
				<!-- End Row -->
			</div>
			<!-- End Container -->
		</div>
		<!-- Top Bar Ends Here -->

		<!-- Navbar Starts Here -->
		<nav class="navbar navbar-default navbar-static-top yamm">
			<div class="container">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed"
						data-toggle="collapse" data-target="#medi_guardian_menu"
						aria-expanded="false">
						<span class="sr-only">Toggle navigation</span> <span
							class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="/"><img
						src="images/mediguardian-logo.png" alt="Arvene Healthcare"></a>
				</div>

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="medi_guardian_menu">
					<ul class="nav navbar-nav navbar-right" id="medi_guardian_menu">


						<c:choose>
							<c:when test="${userType == 'A'}">
								<li><a class="head" href="viewUsers">All Users</a></li>
								<li><a class="head" href="allSpecialist">Specialists</a></li>
								<li><a class="head" href="newQueries">Queries</a></li>
								<li><a class="head" href="adminBookings">Bookings</a></li>
							</c:when>
							<c:otherwise>

								<li id="index"><a href="userQuery">Consult Online</a></li>
								<li class="dropdown yamm-fw"><a href="javascript:;"
									class="dropdown-toggle" data-toggle="dropdown" role="button"
									aria-haspopup="true" aria-expanded="false">Treatments</a>
									<ul class="dropdown-menu animated" data-animation="fadeInDown">
										<li>
											<div class="yamm-content">
												<div class="row">
													<div class="col-md-15">
														<ul>
															<li><a class="head" href="cardiology-treatment">Cardiology</a></li>
															<li><a href="coronary-angiography">Coronary
																	Angiography</a></li>
															<li><a href="coronary-angioplasty">Coronary
																	Angioplasty</a></li>
															<li><a href="coronary-artery-bypass-surgery">Coronary
																	Artery Bypass</a></li>
															<li><a href="paediatric-cardiac-surgery">Paediatric
																	Cardiac Surgery</a></li>
															<li><a href="pacemaker-implantation">Pacemaker
																	Implantation</a></li>
															<li><a href="vascular-surgery">Vascular Surgery</a></li>
															<li><a href="heart-valve-replacement-surgery">Heart
																	Valve Replacement </a></li>
															<li><a href="cardiac-diagnostic">Cardiac
																	Diagnostics</a></li>
															<li><a class="head" href="cosmetic-surgery">Cosmetic
																	Surgery</a></li>
															<li><a href="breast-augmentation-surgery">Breast
																	Augmentation</a></li>

														</ul>
													</div>
													<!-- End Column -->
													<div class="col-md-15">
														<ul>
															<li><a href="breast-reduction-surgery">Breast
																	Reduction</a></li>
															<li><a href="liposuction-surgery">Liposuction</a></li>
															<li><a href="facelift-surgery">Facelift</a></li>
															<li><a href="rhinoplasty-surgery">Rhinoplasty</a></li>
															<li><a href="cosmetic-laser-surgery">Cosmetic
																	Laser Surgery</a></li>
															<li><a href="oral-maxillofacial-surgery">Maxillofacial
																	Surgery</a></li>
															<li><a href="surgical-hair-transplant">Surgical
																	Hair Transplant</a></li>
															<li><a class="head" href="cancer-treatment">Cancer</a></li>
															<li><a href="breast-cancer-treatment">Breast
																	Cancer</a></li>
															<li><a href="cervical-cancer-treatment">Cervical
																	Cancer</a></li>
															<li><a href="prostate-cancer-treatment">Postate
																	Cancer</a></li>
															<li><a href="colon-cancer-treatment">Colon
																	Cancer</a></li>
														</ul>
													</div>
													<!-- End Column -->
													<div class="col-md-15">
														<ul>
															<li><a href="cyberknife-radiation-therapy">Cyberknife
																	Radiation Therapy</a></li>
															<li><a href="neuro-spine-surgery">Neuro and
																	Spine</a></li>
															<li><a class="head" href="neuro-surgery">Neurosurgery</a></li>
															<li><a href="neurology">Neurology</a></li>
															<li><a href="brain-tumors-treatment">Brain Tumor</a></li>
															<li><a href="spinal-fusion-surgery">Spinal
																	Fusion</a></li>
															<li><a href="intracranial-aneurysm-surgery">Aneurysms</a></li>
															<li><a href="spinal-tumor-surgery">Spinal Tumor</a></li>
															<li><a href="spinal-laminectomy-surgery">Laminectomy</a></li>
															<li><a class="head" href="orthopaedics-surgery">Orthopedic
																	Surgery</a></li>
															<li><a href="knee-replacement-surgery">Knee
																	Replacement</a></li>
														</ul>
													</div>
													<!-- End Column -->
													<div class="col-md-15">
														<ul>
															<li><a href="hip-replacement-surgery">Hip
																	Replacement surgery </a></li>
															<li><a href="shoulder-surgery">Shoulder Surgery
															</a></li>
															<li><a href="foot-ankle-surgery">Foot and Ankle
																	surgery</a></li>
															<li><a href="hand-wrist-surgery">Hand Wrist
																	surgery</a></li>
															<li><a href="elbow-replacement-surgery">Elbow
																	Surgery</a></li>
															<li><a class="head" href="organ-transplant">Organ
																	Transplant</a></li>
															<li><a href="kidney-transplant-surgery">Kidney
																	Transplant Surgery</a></li>
															<li><a href="liver-transplant-surgery">Liver
																	Transplant Surgery</a></li>
															<li><a href="heart-transplant">Heart Transplant</a></li>
															<li><a href="bone-marrow-transplant">Bone Marrow
																	Transplants</a></li>
															<li><a href="human-organs-transplant-laws-india">Organ
																	Transplant laws in India</a></li>
														</ul>
													</div>
													<!-- End Column -->
													<div class="col-md-15">
														<ul>
															<li><a class="head" href="infertility-treatment">Infertility
																	Treatment</a></li>
															<li><a href="IVF-treatment">IVF</a></li>
															<li><a href="IUI-treatment">IUI</a></li>
															<li><a class="head" href="bariatric-surgery">Bariatric
																	Treatment</a></li>
															<li><a href="gastric-bypass-surgery">Gastric
																	Bypass</a></li>
															<li><a href="gastric-sleeve-surgery">Sleeve
																	Gastrectomy </a></li>
															<li><a class="head" href="urology-treatment">Urology
																	Treatment</a></li>
															<li><a href="endoscopic-surgery">Endoscopy
																	surgery</a></li>
															<li><a href="TURP-surgery">TURF</a></li>
															<li><a href="radical-prostatectomy-surgery">Prostactomy</a></li>
														</ul>
													</div>
													<!-- End Column -->
												</div>
												<!-- End Row -->
											</div> <!-- End Yamm Content -->
										</li>
									</ul></li>
								<li><a href="medical-providers">Partner Hospitals</a></li>
								<li><a href="why-india">Why India</a></li>
								<li><a href="specialists">Our Doctors</a></li>
								<li><a href="http://blog.arvenehealthcare.com">Blog</a></li>
								<li><a href="faq">FAQ's</a></li>
								<li><a href="about-us">About Us</a></li>
							</c:otherwise>
						</c:choose>
					</ul>
				</div>
				<!-- /.navbar-collapse -->
			</div>
			<!-- /.container-fluid -->
		</nav>
		<!-- Navbar Ends Here -->
	</div>
	<!-- modal sigin -->
	<div id="signin" class="modal fade" role="dialog">
		<div class="modal-dialog mo-co-dailog">
			<!-- Modal content-->
			<div class="modal-content mo-co-con">
				<div class="modal-header mo-co-head">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Sign In</h4>
				</div>
				<div class="modal-body mo-co-body">
					<div class="form-bot">
						<form name='loginForm' id="loginForm"
							action="<c:url value='/j_spring_security_check' />" method='POST'
							class="center has-validation-callback">
							<div class="form-main in-p contact-mo">
								<div class="form-group has-feedback">
									<input type="text" class="form-control" placeholder="Email ID"
										data-validation="email" name="username"
										data-validation-error-msg="Please enter a valid email address">
								</div>
								<div class="form-group has-feedback">
									<input type="password" class="form-control"
										placeholder="Password" data-validation="length"
										data-validation-length="min8" name="password"
										data-validation-error-msg="Passwords should be atleast 8 characters">
								</div>

								<div class="si-btn">
									<button class="btn btn-block bg-blue" type="submit">Sign
										In</button>
								</div>
								<div class="sig-che">
									Forget Password <a href="forgotPwd">Click here</a>
								</div>
							</div>
						</form>
					</div>
				</div>
				<div class="modal-footer mo-co-foot">
					<div class="form-bot">
						<div class="form-main mo-co">
							<!--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
							<span>Not a member ?</span> <a href="#signup" data-toggle="modal"
								data-dismiss="modal"><button class="btn btn-default">Sign
									Up</button></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- modal sigin END -->

	<!-- SIGNUP START MODAL -->
	<div id="signup" class="modal fade" role="dialog">
		<div class="modal-dialog mo-co-dailog">
			<!-- Modal content-->
			<div class="modal-content mo-co-con">
				<div class="modal-header mo-co-head">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Sign Up</h4>
				</div>
				<div class="modal-body mo-co-body">
					<form name='userForm' action='register' method='POST'
						id="user_sign_up_form" role="form">
						<div class="form-bot">
							<div class="form-main in-p contact-mo">
								<div class="form-group has-feedback">
									<input name="username" type="text" class="form-control"
										placeholder="Email ID" maxlength="30" data-validation="email"
										data-validation-error-msg="Please enter a valid email address" />
								</div>
								<div class="form-group has-feedback">
									<input name="password" type="password" class="form-control"
										placeholder="Password" maxlength="15" data-validation="length"
										data-validation-length="min8"
										data-validation-error-msg="Passwords should be atleast 8 characters" />
								</div>
								<div class="sig-chec">
									<div class="form-group has-feedback">
										<label class="checkbox-inline" style="color: #000;"> <input
											type="checkbox" required name="agree" />By signing up I
											agree to <a onclick="myFunction()">Terms &amp; Conditions</a>

										</label>
									</div>
								</div>
								<div class="si-btn">
									<button class="btn btn-block bg-blue" type="submit">Sign
										Up</button>
								</div>
								<div class="sig-che">
									Already a member? <a id="login-modal" href="#signin"
										data-toggle="modal" data-dismiss="modal">Login here</a>
								</div>
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer mo-co-foot">
					<div class="form-bot">
						<div class="form-main mo-co">
							<!--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
							<a href="specialistRegister" class="btn btn-default">Doctor's
								Sign Up</a>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
	<!-- SIGNUP END MODAL -->

	<!-- Div for showing up result -->
	<div id="result">
		<c:if test="${not empty error}">
			<div class="col-lg-12 col-md-12">
				<div class="danger-main">
					<div class=" danger">
						<span class="fa fa-close pull-right clo"></span>
						<p>${error}</p>
					</div>
				</div>
			</div>
		</c:if>
		<c:if test="${not empty msg}">
			<div class="col-lg-12">
				<div class="danger-main2">
					<div class="error-sigup success">
						<p>
							<span class="fa fa-close pull-right clo"></span> ${msg}
						</p>
					</div>
				</div>
			</div>
		</c:if>


	</div>

	<!-- Div for showing up result -END -->
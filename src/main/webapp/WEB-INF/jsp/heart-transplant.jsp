<%@ include file="newHeader.jsp"%></section>
	<!-- Hospital Short Details Starts Here -->
	<div class="short-details">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="title">
						<h1>Organ Transplant</h1>
						<ul class="paginator">
							<li><a href="/">Home</a></li>
							<li><a>Organ Transplant</a></li>
						</ul>
					</div>
				</div>
				<!-- End Column -->
			</div>
			<!-- End Row -->
		</div>
		<!-- End Container -->
	</div>
	<!-- Hospital Short Details Ends Here -->

	<!-- Specialty Content Starts Here -->
	<div class="speciality-content-area">
		<div class="container">

			<div class="innertab-accordion">
				<div class="row">
					<div class="col-md-3 tabnopaddright">
						<div class="tabs">
							<ul>
								<li><a href="organ-transplant">Organ
										Transplant</a></li>
								<li><a href="kidney-transplant-surgery">Kidney Transplant
										Surgery</a></li>
								<li><a href="liver-transplant-surgery">Liver Transplant
										Surgery</a></li>
								<li class="tab-active"><a href="heart-transplant">Heart Transplant</a></li>
								<li><a href="bone-marrow-transplant">Bone Marrow
										Transplants</a></li>
								<li><a href="human-organs-transplant-laws-india">Organ Transplant
										laws in India</a></li>
							</ul>
						</div>
					</div>
					<!-- End Column -->
					<div class="col-md-9 tabnopaddleft">
						<div class="accordion-contentarea">
<div id="tab4" >
								<h2>
									<span>Heart Transplant</span>- India
								</h2>
								<div class="row">
									<div class="col-md-9 col-sm-9 nopaddleft">
										<p>We at Arvene Healthcare have the expertise to organize for all treatments and surgeries related to Heart Transplant. Arvene Healthcare is associated with top hospitals and Doctors of India in this field. We can give you more and better options so that you can take the right decisions. We also assist you in taking best decision based on your requirement. Arvene Healthcare will also arrange for your stay and local travel, so that you have nothing else to worry about but your treatment. Please contact us on <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a> today to avail best services.</p>
										<br>
										<p>Heart transplant in India has grown considerably in past few years with the development of surgical methods and techniques. Heart is an important life support system and transplantation has given life to many patients. Heart transplant in India is very cost effective and at par with any western country in technique. </p>
									</div>
									<!-- End Inner Column -->
									<div class="col-md-3 col-sm-3 nopaddright">
										<img src="images/specialities/41.heart transplant.jpg" alt="heart transplant"
											class="img-responsive">
									</div>
									<!-- End Inner Column -->
								</div>
								<!-- End Inner Row -->
								<h3>Heart transplant:</h3>
								<p>Heart transplantation is a surgical process in which a diseased or non-functioning heart is replaced with a healthy one. The surgeon removes the heart of patients by making a transaction in the aorta, the main pulmonary artery and the superior as well as inferior vena cava, and then dividing the left atrium. After this, the donor's heart is connected by attaching the recipient and donor vena cava, aorta, pulmonary artery and left atrium.</p><br>
								<p>The donor for heart transplant is someone who is either brain dead or on ventilation. </p>
								<h3>Indications</h3>
								<h3>
									<span>This procedure can be recommended in the</span> following
									cases:
								</h3>
								<ul>
									<li><span>Severe heart disease since birth</span></li>
									<li><span>Cardiomyopathy</span></li>
									<li><span>Coronary artery disease (when other therapies fail)</span></li>
									<li><span>Heart valve disease with congestive heart failure</span></li>
									<li><span>Life-threatening abnormal heart beats (when other therapies fail)</span></li>
									<li><span>Congestive heart failure</span></li>
									<li><span>Atrial fibrillation and flutter</span></li>
									<li><span>Myocardial Infarction</span></li>
									<li><span>Mitral valve prolapsed</span></li>
								</ul>
								<h3>
									<span>Patients for whom this surgery is </span>not recommended:
								</h3>
								<ul>
									<li><span>Kidney / lung/ liver disease</span></li>
									<li><span>Insulin-dependent diabetes with poor function of other organs</span></li>
									<li><span>Blood vessel disease of the neck and leg</span></li>
									<li><span>Other life-threatening diseases</span></li>
								</ul>
								<h3>
									<span>Who needs a </span>heart transplant?
								</h3>
								<p>There is a very careful selection of the patients to qualify for this procedure. Many people with "end-stage" heart disease where the heart is not functioning properly, but the other important organs are working properly. But if the patient is suffering from infection, cancer, bad diabetes mellitus, or smoke/alcohol abuse, they cannot undergo the surgery. The donor should also match with the blood type and approximate body size of the patient.</p>
								<h3>
									<span>Process of </span>heart transplant in India
								</h3>
								<p>The heart that is preserved in a solution before the transplant. The patient is always under anaesthesia. The heart-lung bypass machine is used for the operation. This machine supplies blood to entire body and brain while the operation process is being conducted. The transplanted new heart later regulates the smooth flow of blood within the patient's body. Few hospitals in India are renowned for cost-effective Heart transplant surgery having specialized surgeons. </p><br>
								<p>The heart of the donor is matched to the immune system of the recipient. The patient has to take proper medication (immunosuppressant) for preventing the immune system from rejecting the new heart. </p>
								<h3>
									<span>Medical</span> Follow-up
								</h3>
								<p>A patient of heart transplant needs regular check-ups post-surgery. At the time of visits, the doctor undertakes all or some routine heart function tests such as blood test, ECG, Echo etc. The doctor may also want to check patient's coronary arteries annually, for monitoring any signs of narrow coronary arteries in the heart that has been transplanted.</p>
								<h5>
									To get best options for Organ transplant treatment, send us a query or write to us at <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
								</h5>
							</div>
							<!-- End Tab -->

						</div>
					</div>
					<!-- End Column -->
				</div>
				<!-- End Row -->
			</div>
			<!-- End Tab Accordion -->

		</div>
		<!-- End Container -->
	</div>
	<!-- Speciality Content Ends Here -->

	<%@ include file="newFooter.jsp"%>
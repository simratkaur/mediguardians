<%@ include file="newHeader.jsp"%></section>
	<!-- Hospital Short Details Starts Here -->
	<div class="short-details">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="title">
						<h1>Cardiology</h1>
						<ul class="paginator">
							<li><a href="/">Home</a></li>
							<li><a>Cardiology</a></li>
						</ul>
					</div>
				</div>
				<!-- End Column -->
			</div>
			<!-- End Row -->
		</div>
		<!-- End Container -->
	</div>
	<!-- Hospital Short Details Ends Here -->

	<!-- Specialty Content Starts Here -->
	<div class="speciality-content-area">
		<div class="container">

			<div class="innertab-accordion">
				<div class="row">
					<div class="col-md-3 tabnopaddright">
						<div class="tabs">
							<ul>
								<li><a href="cardiology-treatment">Cardiology</a></li>
								<li><a  href="coronary-angiography">Coronary Angiography</a></li>
								<li><a href="coronary-angioplasty">Coronary Angioplasty</a></li>
								<li><a href="coronary-artery-bypass-surgery">Coronary Artery
										Bypass</a></li>
								<li><a href="paediatric-cardiac-surgery">Paediatric Cardiac
										Surgery</a></li>
								<li><a href="pacemaker-implantation">Pacemaker
										Implantation</a></li>
								<li><a href="vascular-surgery">Vascular Surgery</a></li>
								<li class="tab-active"><a href="heart-valve-replacement-surgery">Heart Valve
										Replacement </a></li>
								<li><a href="cardiac-diagnostic">Cardiac Diagnostics</a></li>
							</ul>
						</div>
					</div>
					<!-- End Column -->
					<div class="col-md-9 tabnopaddleft">
						<div class="accordion-contentarea">

<div id="tab8" >
								<h2>
									<span>Heart Valve Replacement Surgery</span> in India
								</h2>
								<p>We at Arvene Healthcare have the expertise to organize for all treatments and surgeries related to Cardiology. Arvene Healthcare is associated with top hospitals and Doctors of India in this field. We can give you more and better options so that you can take the right decisions. We also assist you in taking best decision based on your requirement. Arvene Healthcare will also arrange for your stay and local travel, so that you have nothing else to worry about but your treatment. Please contact us on <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a> today to avail best services.</p>
								
								<h3>
									<span>What is </span>Heart Valve Replacement?
								</h3>
								<div class="row">
									<div class="col-md-9 col-sm-9 nopaddleft">
										<p>Heart Valve Replacement refers to the procedure of replacing heart valves that have narrowed and hinders the blood flow in &amp; out of the heart with the artificial valves.
										</p>
								<h3>
									<span>Types of </span>Heart Valve Replacement
								</h3>
								<p>
									<strong>Open-heart valve replacement surgery</strong>
								</p>
								</div>
									<!-- End Inner Column -->
									<div class="col-md-3 col-sm-3 nopaddright">
										<img src="images/specialities/7.heart valve replacement.jpg"
											alt="heart valve replacement" class="img-responsive">
									</div>
									<!-- End Inner Column -->
								</div>
								<!-- End Inner Row --><p>It is a surgical procedure wherein the surgery is performed on the internal structures of the heart. During the surgery, the patient is placed on a heart-lung machine that allows blood to flow through the heart while the surgery is being performed.
								</p>
								<p>
									<strong>Minimally Invasive Heart Surgery</strong>
								</p>
								<p>Minimally Invasive Heart Surgery or keyhole surgery is performed by making small incisions of about 3 to 4 inches in the heart muscle, through specialized surgical instruments. Minimally invasive heart surgery will repair Mitral valve, Aortic valve and Tricuspid valve.
								</p>
								<p>
									<strong>Valvuloplasty</strong>
								</p>
								<p>It is a technique wherein the stiff aortic valves are treated with the help of a balloon catheter. The balloon is positioned in the aortic valve and subsequently inflated to expand the size of the valve, leading to improved blood flow.
								</p>
								<h3>
									<span>Types of Valves Used in </span>Replacement
								</h3>
								<p>
									<strong>Mechanical Valves</strong>
								</p>
								<p>A mechanical valve or prosthetic refer to the artificial substitute of the native heart valve. Like your native heart valve, the mechanical valve too has a ring to support the leaflets.
								</p>
								<p>
									<strong>Tissue Valves</strong>
								</p>
								<p>It is a native valve taken from an animal. Before placing it into the human body, the tissue valve is chemically treated and replaced with the diseased valve.
								</p>
								<p>
									<strong>Homografts or Allografts</strong>
								</p>
								<p>When a valve is obtained from the body of a donor, it is known as homograft or allograft. This type of valve is beneficial for children's and pregnant women as it does not require long-term anticoagulation therapy.
								</p>
								
								<h3>
									<span>Benefits of </span>Heart Valve Surgery
								</h3>
								<p>Damaged heart valves cause a multitude of symptoms such as breathing difficulties, chest pain, edema and heart palpitations. Post Heart Valve surgery, patients get rid of all these problems.
								</p>
								<h5>Cost of Heart Valve Replacement surgery in India starts at 6800 US Dollars. To know more about it please share your reports on <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a> or whatsapp/viber/imo to +918130077375.</h5><p> 
(Please note this is a tentative plan and actual Costs are based on reports shared. Final treatment plan and estimation will be provided after the patient has been evaluated.)</p>
								
								<h5>
									To get best options for cardiology treatment, send us a query
									or write to us at <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
								</h5>
							</div>
							<!-- End Tab -->
						</div>
					</div>
					<!-- End Column -->
				</div>
				<!-- End Row -->
			</div>
			<!-- End Tab Accordion -->

		</div>
		<!-- End Container -->
	</div>
	<!-- Speciality Content Ends Here -->

	<%@ include file="newFooter.jsp"%>
<%@ include file="newHeader.jsp"%></section>
<!-- Hospital Short Details Starts Here -->
<div class="short-details">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="title">
					<h1>Orthopedic Surgery</h1>
					<ul class="paginator">
						<li><a href="/">Home</a></li>
						<li><a>Orthopedic Surgery</a></li>
					</ul>
				</div>
			</div>
			<!-- End Column -->
		</div>
		<!-- End Row -->
	</div>
	<!-- End Container -->
</div>
<!-- Hospital Short Details Ends Here -->

<!-- Specialty Content Starts Here -->
<div class="speciality-content-area">
	<div class="container">

		<div class="innertab-accordion">
			<div class="row">
				<div class="col-md-3 tabnopaddright">
					<div class="tabs">
						<ul>
							<li><a href="orthopaedics-surgery">Orthopedic
									Surgery</a></li>
							<li><a href="knee-replacement-surgery">Knee Replacement</a></li>
							<li class="tab-active"><a href="hip-replacement-surgery">Hip Replacement
									surgery </a></li>
							<li><a href="shoulder-surgery">Shoulder Surgery </a></li>
							<li><a href="foot-ankle-surgery">Foot and Ankle surgery</a></li>
							<li><a href="hand-wrist-surgery">Hand Wrist surgery</a></li>
							<li><a href="elbow-replacement-surgery">Elbow Surgery</a></li>
						</ul>
					</div>
				</div>
				<!-- End Column -->
				<div class="col-md-9 tabnopaddleft">
					<div class="accordion-contentarea">

<div id="tab3">
							<h2>
								<span>Hip Replacement Surgery</span> -India
							</h2>
							<div class="row">
								<div class="col-md-9 col-sm-9 nopaddleft">
									<p>We at Arvene Healthcare have the expertise to organize
										for all treatments and surgeries related to Hip Replacement.
										Arvene Healthcare is associated with top hospitals and Doctors
										of India in this field. We can give you more and better
										options so that you can take the right decisions. We also
										assist you in taking best decision based on your requirement.
										Arvene Healthcare will also arrange for your stay and local
										travel, so that you have nothing else to worry about but your
										treatment. Please contact us on <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
										today to avail best services.</p>
									<h3>
										<span>Hip Replacement</span> Surgery
									</h3>
									<p>Treatment of diseased hip by removing the damaged
										cartilage and bone and replacing it with synthetic one is
										called Hip replacement surgery. The materials used to create
										an artificial joint are made up of metal (titanium or cobalt
										chrome). They reduce friction thereby reducing pain and
										improving longevity.</p>
									<h3>
										<span>Causes of</span> Hip Replacement
									</h3>
								</div>
								<!-- End Inner Column -->
								<div class="col-md-3 col-sm-3 nopaddright">
									<img
										src="images/specialities/33.hip replacement relacement.jpg"
										alt="hip replacement" class="img-responsive">
								</div>
								<!-- End Inner Column -->
							</div>
							<!-- End Inner Row -->
							<p>Osteoarthritis is the most common reason that leads to hip
								replacement. It is most often caused by wear and tear of aging,
								resulting in soreness and rigidity. Other conditions which lead
								to hip replacement surgery include rheumatoid arthritis,
								traumatic arthritis, avascular necrosis, osteonecrosis and bone
								tumors.</p>
							<h3>
								<span>Types of</span> Hip Replacement Surgeries
							</h3>
							<h3>
								1. <span>Total Hip </span>Replacement
							</h3>
							<p>A normal hip joint structure is like a ball and socket
								joint. In Total Hip Replacement the diseased cartilage and bone
								of the hip joint is surgically removed and replaced with the
								synthetic material. The artificial joints are attached to the
								bone with acrylic cement.</p>
							<h3>
								2. <span>Minimally Invasive</span> Hip Replacement
							</h3>
							<p>In this type of surgery, surgeon will perform hip
								replacement through 1 or 2 smaller incisions. Patients who
								undergo minimally invasive surgery are likely to have a faster
								recovery.</p>
							<h3>
								3. <span>Birmingham </span>Hip Replacement
							</h3>
							<p>The Birmingham Hip Replacement surgery takes place in
								paediatric and young patients. During this surgery, a very small
								part of the bone on top of the thighbone is recur. A metal head
								is attached to the thighbone and fits into a metal socket
								located in the joint.</p>
							<h3>
								4. <span>Hip Replacement </span>Implants
							</h3>
							<p>Hip Replacements are performed with artificially designed
								prosthetic implants. The implants will last long and are known
								to have good long term results. These include acetabular cup,
								femoral component and the articular interface.</p>
							<h3>
								<span>Benefits of </span>Hip Replacement Surgery
							</h3>
							<p>Today a Hip replacement can actually last life long, owing
								to new technology and advancement in surgical techniques. With
								the development of new socket implants for the pelvis, the
								sockets can be replaced without hampering the other portions of
								the hip joint. People who undergo hip replacement can get back
								to work within two months of surgery.</p>
							<p>
								<strong>Some benefits of hip replacement surgery: </strong>
							</p>
							<ul>
								<li><span>Less joint pain</span></li>
								<li><span>Increased leg strength</span></li>
								<li><span>Increased mobility and stability</span></li>
								<li><span>Deformity correction</span></li>
							</ul>
							<p>
								<strong>Revision Hip Replacement Surgery</strong>
							</p>
							<p>Hip replacement surgery has success rate of 90-95%.
								However in few cases loosening of implant leads to the failure
								of total hip replacement. In such cases, a patient will undergo
								revision surgery.</p>
<h5>Cost of Hip Replacement surgery in India starts at 7000 US Dollars for single hip and 12000 US Dollars for double hip. To know more about it please share your reports on <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a> or whatsapp/viber/imo to +918130077375.</h5><p> 
(Please note this is a tentative plan and actual Costs are based on reports shared. Final treatment plan and estimation will be provided after the patient has been evaluated.)</p>
							<h5>
								To get best options for Orthopaedic treatment, send us a query
								or write to us at <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
							</h5>
						</div>
						<!-- End Tab -->

					</div>
				</div>
				<!-- End Column -->
			</div>
			<!-- End Row -->
		</div>
		<!-- End Tab Accordion -->

	</div>
	<!-- End Container -->
</div>
<!-- Speciality Content Ends Here -->

<%@ include file="newFooter.jsp"%>
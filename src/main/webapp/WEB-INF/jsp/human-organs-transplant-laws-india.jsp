<%@ include file="newHeader.jsp"%></section>
	<!-- Hospital Short Details Starts Here -->
	<div class="short-details">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="title">
						<h1>Organ Transplant</h1>
						<ul class="paginator">
							<li><a href="/">Home</a></li>
							<li><a>Organ Transplant</a></li>
						</ul>
					</div>
				</div>
				<!-- End Column -->
			</div>
			<!-- End Row -->
		</div>
		<!-- End Container -->
	</div>
	<!-- Hospital Short Details Ends Here -->

	<!-- Specialty Content Starts Here -->
	<div class="speciality-content-area">
		<div class="container">

			<div class="innertab-accordion">
				<div class="row">
					<div class="col-md-3 tabnopaddright">
						<div class="tabs">
							<ul>
								<li><a href="organ-transplant">Organ
										Transplant</a></li>
								<li><a href="kidney-transplant-surgery">Kidney Transplant
										Surgery</a></li>
								<li><a href="liver-transplant-surgery">Liver Transplant
										Surgery</a></li>
								<li><a href="heart-transplant">Heart Transplant</a></li>
								<li><a href="bone-marrow-transplant">Bone Marrow
										Transplants</a></li>
								<li class="tab-active"><a href="human-organs-transplant-laws-india">Organ Transplant
										laws in India</a></li>
							</ul>
						</div>
					</div>
					<!-- End Column -->
					<div class="col-md-9 tabnopaddleft">
						<div class="accordion-contentarea">

<div id="tab6" >
								<h2>
									<span>Human Organs Transplant Laws</span> in India
								</h2>
								<b>Laws and Rules Governing Organ Transplantation in India</b>
								<p>The primary legislation related to organ donation and transplantation in India, Transplantation of Human Organs Act, was passed in 1994 and is aimed at regulation of removal, storage and transplantation of human organs for therapeutic purposes and for prevention of commercial dealings in human organs.</p>
								<br><p>
									In India, matters related to health are governed by each state. The Act was initiated at the request of Maharashtra, Himachal Pradesh and Goa (who therefore adopted it by default) and was subsequently adopted by all states except Andhra Pradesh and Jammu &amp; Kashmir. Despite a regulatory framework, cases of commercial dealings in human organs were reported in the media. An amendment to the act was proposed by the states of Goa, Himachal Pradesh and West Bengal in 2009 to address inadequacies in the efficacy, relevance and impact of the Act. The amendment to the Act was passed by the parliament in 2011, and the rules were notified in 2014. The same is adopted by the proposing states and union territories by default and may be adopted by other states by passing a resolution.
								</p><br>
								<p>The main provisions of the Act (including the amendments and rules of 2014) are as follows:</p><br>
								
								<p>A. Brain death identified as a form of death. Process and criteria for brain death certification defined (Form 10)</p><br>
								
								<p>B. Allows transplantation of human organs and tissues from living donors and cadavers (after cardiac or brain death)</p><br>
								<p>
									C. Regulatory and advisory bodies for monitoring transplantation activity and their constitution defined.
								</p><br>
								<p>
									(i) Appropriate Authority (AA): inspects and grants registration to hospitals for transplantation enforces required standards for hospitals, conducts regular inspections to examine the quality of transplantations. It may conduct investigations into complaints regarding breach of provisions of the Act, and has the powers of a civil court to summon any person, request documents and issue search warrants.
								</p><br>
								<p>(ii)  Advisory Committee: consisting of experts in the domain who shall advise the appropriate authority.</p><br>
								<p>(iii) Authorization Committee (AC): regulates living donor transplantation by reviewing each case to ensure that the living donor is not exploited for monetary considerations and to prevent commercial dealings in transplantation. Proceedings to be video recorded and decisions notified within 24 hours. Appeals against their decision may be made to the state or central government.</p><br>
								<p>(iv) Medical board (Brain Death Committee): Panel of doctors responsible for brain death certification. In case of non-availability of neurologist or neurosurgeon, any surgeon, physician, anaesthetist or intensivist, nominated by medical administrator in-charge of the hospital may certify brain death.</p><br>
								<p>D. Living donors are classified as either a near relative or a non-related donor.</p><br>
								<p>(i) A near-relative (spouse, children, grandchildren, siblings, parents and grandparents) needs permission of the doctor in-charge of the transplant centre to donate his organ.</p><br>
								<p>(ii) A non-related donor needs permission of an Authorization Committee established by the state to donate his organs.</p><br>
								<p>E. Swap Transplantation: When a near relative living donor is medically incompatible with the recipient, the pair is permitted to do a swap transplant with another related unmatched donor/recipient pair.</p><br>
								<p>F. Authorization for organ donation after brain death</p><br>
								<p>(i) May be given before death by the person himself/herself or</p><br>
								<p>(ii) By the person in legal possession of the body. A doctor shall ask the patient or relative of every person admitted to the ICU whether any prior authorization had been made. If not, the patient or his near relative should be made aware of the option to authorize such donation.</p><br>
								<p>(iii) Authorization process for organ or tissue donation from unclaimed bodies outlined.</p><br>
								<p>H. Cost of donor management, retrieval, transportation and preservation to be borne by the recipient, institution, government, NGO or society, and not by the donor family.</p><br>
								<p>I. Procedure for organ donation in medico-legal cases defined to avoid jeopardizing determination of the cause of death and delay in retrieval of organs.</p><br>
								<p>J. Manpower and Facilities required for registration of a hospital as a transplant centre outlined.</p><br>
								<p>K. Infrastructure, equipment requirements and guidelines and standard operating procedures for tissue banks outlined.</p><br>
								<p>L. Qualifications of transplant surgeons, cornea and tissue retrieval technicians defined.</p><br>
								<p>M. Appointment of transplant coordinators (with defined qualifications) made mandatory in all transplant centres.</p><br>
								<p>N. Non-governmental organisations, registered societies and trusts working in the field of organ or tissue removal, storage or transplantation will require registration.</p><br>
								<p>O. The central government to establish a National Human Organs and Tissues Removal and Storage Network i.e. NOTTO (National Organ &amp; Tissue Transplant Organisation), ROTTO (Regional Organ &amp; Tissue Transplant Organisation) and SOTTO (State Organ &amp; Tissue Transplant Organisation). Website www.notto.nic.in. Manner of establishing National or Regional or State Human Organs and Tissues Removal and Storage Networks and their functions clearly stated.</p><br>
								<p>P. The central government shall maintain a registry of the donors and recipients of human organs and tissues.</p><br>
								<p>Q. Penalties for removal of organ without authority, making or receiving payment for supplying human organs or contravening any other provisions of the Act have been made very stringent in order to serve as a deterrent for such activities.</p><br>
								<h5>
									To get best options for Organ Transplant, send us a query
									or write to us at <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
								</h5>
							</div>
							<!-- End Tab -->
						</div>
					</div>
					<!-- End Column -->
				</div>
				<!-- End Row -->
			</div>
			<!-- End Tab Accordion -->

		</div>
		<!-- End Container -->
	</div>
	<!-- Speciality Content Ends Here -->

	<%@ include file="newFooter.jsp"%>
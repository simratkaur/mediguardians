<%@ include file="newHeader.jsp"%></section>
	<!-- Banner Starts Here -->
	<div class="inner-banner">
		<img src="images/banners/indraprastha.jpg"
			alt="METRO Hospital" class="img-responsive">
	</div>
	<!-- Banner Ends Here -->

	<!-- Hospital Short Details Starts Here -->
	<div class="short-details">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12">
					<div class="title">
						<h1>Indraprastha Apollo Hospital</h1>
						<p>Delhi, India</p>
					</div>
				</div>
				<!-- End Column -->
				<!-- <div class="col-md-6 col-sm-6">
                            <div class="site-link">
                                Website : <a href="javascript:;">http://www.aimshospital.org</a>
                            </div>
                        </div>End Column -->
			</div>
			<!-- End Row -->
		</div>
		<!-- End Container -->
	</div>
	<!-- Hospital Short Details Ends Here -->

	<!-- Hospital Details Starts Here -->
	<div class="container">
		<div class="detail-container">
			<div class="hospital-details">
				<div class="row">
					<div class="col-md-9">
						<div class="row">
							<div class="col-md-6 col-divider">
								<div class="address-box box-height">
									<ul>
										<!-- <li><strong>Address</strong><span>: Ponekkara, P. O Kochi, Kochi, Kerala</span></li> -->
										<li><strong>City</strong><span>: Delhi</span></li>
									</ul>
								</div>
							</div>
							<!-- End Column -->
							<div class="col-md-6 col-divider">
								<div class="number-box box-height">
									<ul>
										<li><strong>No. of Hospital Beds</strong><span>: </span></li>
										<li><strong>No. of ICU Beds</strong><span>: </span></li>
										<li><strong>No. of Operating Rooms</strong><span>:
										</span></li>
										<li><strong>Payment Mode</strong><span>: cash, bank transfer</span></li>
										<li><strong>Avg. International Patients</strong><span>:
										</span></li>
									</ul>
								</div>
							</div>
							<!-- End Column -->
						</div>
						<!-- End Row -->
					</div>
					<!-- End Column -->
					<!-- <div class="col-md-3">
                                <div class="certificate-box box-height">
                                    <h4>Accreditations</h4>
                                    <ul>
                                        <li><img src="images/certificate/icon01.png" alt="Accreditations" class="img-responsive"></li>
                                    </ul>
                                </div>
                            </div>End Column -->
				</div>
				<!-- End Row -->
			</div>
			<!-- End Hospital Details -->
		</div>
		<!-- End Detail Container -->
	</div>
	<!-- Hospital Details Ends Here -->

	<!-- About Section Starts Here -->
	<div class="about-section">
		<div class="container">
			<div class="title">
				<h2>
					<span>About</span> Apollo Hospital
				</h2>
			</div>
			<div class="description">
				<p>Indraprastha Apollo Hospitals, New Delhi is a multi-specialty tertiary acute care hospital with 710 beds and one of the most sought after destinations in Asia for healthcare. A state-of-the art modern facility in the heart of the capital, it is spread over 15 acres and has a built-up area of over 600,000 square feet.</p>
				<p>Indraprastha Apollo Hospitals, New Delhi is a flagship hospital of the Apollo Hospitals Group that epitomises the clinical excellence that the Apollo Group stands for. Clinical excellence aims at the best clinical outcomes for patients. Achieving the best clinical outcomes for the most complex diseases requires the best staff supported by latest technology and standardized processes</p>
				<p>We engage the best consultants through a rigorous credentialing and privileging process who are supported by the best healthcare staff. It has the latest and Best-in-Class medical technologies like PET- MR, PET-CT, Da Vinci Robotic Surgery System, BrainLab Navigation System, Portable CT Scanner, NovalisTx, Tilting MRI, Cobalt based HDR Brachytherapy, DSA Lab, Hyperbaric Chamber, Fibroscan, Endosonography, 3 Tesla MRI, 128 Slice CT scanner to provide world-class care.</p>
				<p>That Indraprastha Apollo Hospitals became the first hospital in India to be JCI accredited in 2005 stands testimony to our standardized processes. We also became the first hospital to be reaccredited in 2008 and 2011. It has NABL accredited clinical laboratories and a state of the art blood bank.</p>
				<h5>
					To get best options for treatment at Apollo Hospital, send us a
					query or write to us at <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
				</h5>

			</div>
		</div>
		<!-- End Container -->
	</div>
	<!-- About Section Ends Here -->

	<%@ include file="newFooter.jsp"%>
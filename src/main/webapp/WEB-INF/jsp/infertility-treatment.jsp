<%@ include file="newHeader.jsp"%></section>
	<!-- Hospital Short Details Starts Here -->
	<div class="short-details">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="title">
						<h1>Infertility Treatment</h1>
						<ul class="paginator">
							<li><a href="/">Home</a></li>
							<li><a>Infertility Treatment</a></li>
						</ul>
					</div>
				</div>
				<!-- End Column -->
			</div>
			<!-- End Row -->
		</div>
		<!-- End Container -->
	</div>
	<!-- Hospital Short Details Ends Here -->

	<!-- Specialty Content Starts Here -->
	<div class="speciality-content-area">
		<div class="container">

			<div class="innertab-accordion">
				<div class="row">
					<div class="col-md-3 tabnopaddright">
						<div class="tabs">
							<ul>
								<li class="tab-active"><a href="infertility-treatment">Infertility
										Treatment</a></li>
								<li><a href="IVF-treatment">IVF</a></li>
								<li><a href="IUI-treatment">IUI</a></li>
							</ul>
						</div>
					</div>
					<!-- End Column -->
					<div class="col-md-9 tabnopaddleft">
						<div class="accordion-contentarea">

							<div id="tab1" class="visible">
								<h2>
									<span>Infertility Treatment </span>� India 
								</h2><div class="row">
									<div class="col-md-9 col-sm-9 nopaddleft">
								<p>We at Arvene Healthcare have the expertise to organize for all treatments and surgeries related to Infertility. Arvene Healthcare is associated with top hospitals and Doctors of India in this field. We can give you more and better options so that you can take the right decisions. We also assist you in taking best decision based on your requirement. Arvene Healthcare will also arrange for your stay and local travel, so that you have nothing else to worry about but your treatment. Please contact us on <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a> today to avail best services.</p>
								</div>
									<!-- End Inner Column -->
									<div class="col-md-3 col-sm-3 nopaddright">
										<img src="images/specialities/43.infertility main.jpg" alt="infertility"
											class="img-responsive">
									</div>
									<!-- End Inner Column -->
								</div>
								<!-- End Inner Row -->
								<h3>
									Infertility:
								</h3>
								<p>Infertility refers to the inability to naturally conceive a child. Infertility treatment refers to procedure followed to help those having problems bearing a child.This problem may be in either man or woman, or both. It also refers to a condition of a woman when she is unable to carry a pregnancy to its full term.</p>
								<h3>
									<span>Causes of </span>Infertility
								</h3>
								<p>In a normal reproduction process proper interaction between the female and male reproductive system is required. Following are the causes of infertility (both men and women):</p>
								<ul>
									<li><span>Sperm problems</span></li>
									<li><span>Ovulation problems</span></li>
									<li><span>Tubal causes</span></li>
									<li><span>Age-related factors</span></li>
									<li><span>Unexplained infertility</span></li>
								</ul>
								<h3>
									<span>Infertility Treatment</span> Options
								</h3>
								<p>
									<strong>Ovulation induction</strong>
								</p>
								<p>In case of irregular ovulation of females, medication is started for regulating the ovulation cycle and improving the egg quality. </p>
								<p>
									<strong>IUI (Intra Uterine Insemination): </strong> A method where processed semen is placed directly in the uterus with the help of a catheter.
								</p>
								<p>
									<strong>IVF (Invitro Fertilization): </strong> IVF is the fertilization of an ovum outside the body and then transferring fertilized ovum (embryo) into the uterus.
								</p>
								<p>
									<strong>ICSI (Intra Cytoplasmic Sperm Injection): </strong> In this process, a single sperm is injected into the cytoplasm of an egg for fertilization.
								</p>
								<p>
									<strong>TESA (Testicular Sperm Aspiration): </strong> A surgical sperm harvesting technique.
								</p>
								<p>
									<strong> Embryo Freezing Excess (Surplus): </strong> Embryos are Cryo-preserved at ultra-low temperatures for years to be used subsequently.
								</p>
								<p>
									<strong>Semen/Sperm freezing: </strong> Semen/Sperm are stored frozen at ultra-low temperatures for long duration, useful in various conditions.
								</p>
								<h3>
									<span>Types of </span>Infertility
								</h3>
								<p>
									<strong>Primary Infertility: - </strong> It is that condition where a couple has not been able to conceive a pregnancy after at least a year of unprotected intercourse.
								</p>
								<p>
									<strong>Secondary Infertility: - </strong> It is a condition where the couple has conceived or carried a pregnancy to term after naturally successfully conceiving one or more children.
								</p>
								<p>
									<strong>Male Infertility : - </strong> It is a condition of infertility in males that refers to the absence of healthy sperms in the semen in order to meet ovum.
								</p>
								<p>
									<strong>Female Infertility: - </strong> It is a state of infertility in females. It is important that all the reproductive organs in a female are in a perfect shape.
								</p>
								<h3>
									<span>Diagnosis for </span>Fertility:
								</h3>
								<p>
									<strong> For Male: </strong> Along with the necessary basic blood and urine examination, semen analysis is also done.
								</p>
								<p>
									<strong>For Female : </strong> Routine tests like Rubella IgG levels, HIV , CBC, FBS, urine routine, TSH, / FSH / LH / Prolactin (Day 2 of the cycle), VDRL, Hepatitis B, and Blood grouping are done.
								</p>
								<p>
									<strong> HSG (Hysterosalpingogram): </strong> Here an X-ray picture of the uterus (Hystero) and Fallopian tubes (Salpingo) is taken.
								</p>
								<p>
									<strong> Trans-vaginal Ultra Sonogram: </strong> Undertaken for checking any abnormalities in the uterus, tubes and ovaries.
								</p>
								<p>
									<strong> Laproscopic Hysteroscopy: </strong> Done to look into the interior aspects of the uterus with an endoscope.
								</p>
								<p>
									<strong> Laparoscopy: </strong> Done for visualization of the peritoneal cavity using an endoscope.
								</p>
								<p>Depending on the results, the course of treatment is
									decided.</p>
								<p>
									<strong>Infertility Treatment </strong>
								</p>
								<p>Assisted Reproductive Technology (ART) is the name given to Infertility treatments, which are formulated for increasing the number of eggs or sperm, bringing them together, and improving the chances of pregnancy. </p>
								<h3>
									<span>Cost of</span> Infertility Treatments in India
								</h3>
								<p>Choose Infertility Treatment in India through Arvene Healthcare India to avail affordable package with a success rate of 25 to 30 percent (depends on the condition of the individual).</p>
								<p>We can arrange for Free of cost opinions from various super speciality hospitals before doctor's consultations from the expert Doctors of our connected hospitals.</p>
								<h5>
									To get best options for Infertility treatment, send us a query
									or write to us at <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
								</h5>
							</div>
							<!-- End Tab -->

						</div>
					</div>
					<!-- End Column -->
				</div>
				<!-- End Row -->
			</div>
			<!-- End Tab Accordion -->

		</div>
		<!-- End Container -->
	</div>
	<!-- Speciality Content Ends Here -->

	<%@ include file="newFooter.jsp"%>
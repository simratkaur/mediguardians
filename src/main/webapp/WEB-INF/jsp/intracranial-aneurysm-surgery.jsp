<%@ include file="newHeader.jsp"%></section>
<!-- Hospital Short Details Starts Here -->
<div class="short-details">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="title">
					<h1>Neuro and Spine</h1>
					<ul class="paginator">
						<li><a href="/">Home</a></li>
						<li><a>Neuro and Spine</a></li>
					</ul>
				</div>
			</div>
			<!-- End Column -->
		</div>
		<!-- End Row -->
	</div>
	<!-- End Container -->
</div>
<!-- Hospital Short Details Ends Here -->

<!-- Specialty Content Starts Here -->
<div class="speciality-content-area">
	<div class="container">

		<div class="innertab-accordion">
			<div class="row">
				<div class="col-md-3 tabnopaddright">
					<div class="tabs">
						<ul>
							<li><a href="neuro-spine-surgery">Neuro
									and Spine</a></li>
							<li><a href="neuro-surgery">Neurosurgery</a></li>
							<li><a href="neurology">Neurology</a></li>
							<li><a href="brain-tumors-treatment">Brain Tumor</a></li>
							<li><a href="spinal-fusion-surgery">Spinal Fusion</a></li>
							<li class="tab-active"><a href="intracranial-aneurysm-surgery">Aneurysms</a></li>
							<li><a href="spinal-tumor-surgery">Spinal Tumor</a></li>
							<li><a href="spinal-laminectomy-surgery">Laminectomy</a></li>
						</ul>
					</div>
				</div>
				<!-- End Column -->
				<div class="col-md-9 tabnopaddleft">
					<div class="accordion-contentarea">

<div id="tab6">
							<h2>
								<span>Intracranial Aneurysm surgery</span> ? India
							</h2>
							<div class="row">
								<div class="col-md-9 col-sm-9 nopaddleft">
									<p>We at Arvene Healthcare have the expertise to organize
										for all treatments and surgeries related to Neuro and Spine.
										Arvene Healthcare is associated with top hospitals and Doctors
										of India in this field. We can give you more and better
										options so that you can take the right decisions. We also
										assist you in taking best decision based on your requirement.
										Arvene Healthcare will also arrange for your stay and local
										travel, so that you have nothing else to worry about but your
										treatment. Please contact us on <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
										today to avail best services.</p>
								</div>
								<!-- End Inner Column -->
								<div class="col-md-3 col-sm-3 nopaddright">
									<img src="images/specialities/28.aneurysm .jpg" alt="aneurysm"
										class="img-responsive">
								</div>
								<!-- End Inner Column -->
							</div>
							<!-- End Inner Row -->
							<h3>
								<span>What is a </span>Brain Aneurysm?
							</h3>
							<p>A brain aneurysm referred as intracranial aneurysm or
								cerebral aneurysm. The weak and abnormal spot on a blood vessel
								that results in an outward ballooning or bulging of the arterial
								wall is referred to as brain or cerebral aneurysm. Presence of
								aneurysm in the head can result in serious medical condition. An
								aneurysm in the head ruptures resulting in a haemorrhagic stroke
								that can cause brain damage and even death.</p>
							<h3>
								<span>What are the symptoms of</span> Brain Aneurysm
							</h3>
							<p>The condition of brain aneurysm does not result in any
								symptoms and are usually discovered at the time of some other
								tests for an unrelated condition. An unruptured aneurysm presses
								on the different areas of the brain that can result in many
								problems. During this time a person suffers from blurred vision,
								neck pain, severe headaches and changes in speech. Usually the
								symptoms of a ruptured brain aneurysm occur unexpectedly.</p>
							<h3>
								<span>Causes of </span>Brain Aneurysm-
							</h3>
							<p>An aneurysm occurs due to the hardening of the arteries
								and aging. An individual may also inherit the tendency of
								developing aneurysms. Some risk factors that increase the
								chances of forming an aneurysm</p>
							<ul>
								<li><span><strong>Previous Aneurysm: </strong> There
										are chances of developing another aneurysm when a person
										already had a brain aneurysm.</span></li>
								<li><span><strong>Hypertension: </strong> There are
										more chances of subarachnoid haemorrhage in those who have a
										history of hypertension. </span></li>
								<li><span><strong>Family History: </strong> A family
										history of brain aneurysm can increase the chances of
										developing an aneurysm as compared to those who don't.</span></li>
								<li><span><strong>Smoking: </strong> Smoking is also
										one the cause of developing a brain aneurysm where an aneurysm
										may also rupture.</span></li>
							</ul>
							<h3>
								<span>Diagnosis of </span>Brain Aneurysm
							</h3>
							<p>A number of tests are performed for diagnosing the
								condition of brain aneurysm that include MRI, CT Scan,
								Angiogram, etc</p>
							<h3>
								<span>Treatment of </span>Brain Aneurysm
							</h3>
							<p>Two kind surgeries can be performed for treating
								unruptured and ruptured brain aneurysms.</p>
							<p>
								<strong>Surgical Clipping: </strong> During the surgical
								procedure, a small metal clip is placed at the base of aneurysm
								in order to separate it from usual blood circulation. This helps
								in preventing rupturing and also lowers down the pressure on the
								aneurysm. This surgery depends upon the size and location of the
								aneurysm and also on the overall health of a patient.
							</p>
							<p>
								<strong>Coil Embolization: </strong> : In this procedure, a
								doctor inserts a small tube into the affected artery and is
								placed around the aneurysm. From the tube into the aneurysm,
								small metal coils are moved. This is done for relieving the
								pressure on the aneurysm while also making it less likely to
								rupture.
							</p>
							<h3>
								<span>Cost of </span>Intracranial Aneurysm Surgery
							</h3>
							<p>The success rate of brain aneurysm surgery in India is
								very high as the surgery is performed by experienced surgeons.
								India is known for its low-cost brain aneurysm surgery that does
								not affect the quality standard of the treatment.</p>
							<h5>
								To get best options for Neuro and Spine treatment, send us a
								query or write to us at <a
									href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
							</h5>
						</div>
						<!-- End Tab -->
					</div>
				</div>
				<!-- End Column -->
			</div>
			<!-- End Row -->
		</div>
		<!-- End Tab Accordion -->

	</div>
	<!-- End Container -->
</div>
<!-- Speciality Content Ends Here -->

<%@ include file="newFooter.jsp"%>
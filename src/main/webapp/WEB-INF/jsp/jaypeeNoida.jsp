<%@ include file="newHeader.jsp"%></section>
	<!-- Banner Starts Here -->
	<div class="inner-banner">
		<img src="images/banners/jaypee-hospital.jpg" alt="JAYPEE Hospital"
			class="img-responsive">
	</div>
	<!-- Banner Ends Here -->

	<!-- Hospital Short Details Starts Here -->
	<div class="short-details">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12">
					<div class="title">
						<h1>JAYPEE Hospital</h1>
						<p>Noida, India</p>
					</div>
				</div>
				<!-- End Column -->
				<!-- <div class="col-md-6 col-sm-6">
                            <div class="site-link">
                                Website : <a href="javascript:;">http://www.aimshospital.org</a>
                            </div>
                        </div>End Column -->
			</div>
			<!-- End Row -->
		</div>
		<!-- End Container -->
	</div>
	<!-- Hospital Short Details Ends Here -->

	<!-- Hospital Details Starts Here -->
	<div class="container">
		<div class="detail-container">
			<div class="hospital-details">
				<div class="row">
					<div class="col-md-9">
						<div class="row">
							<div class="col-md-6 col-divider">
								<div class="address-box box-height">
									<ul>
										<li><strong>Address</strong><span>: Sector-128,
												Wish Town, Gautam Buddh Nagar, Noida, U.P.</span></li>
										<li><strong>City</strong><span>: Noida</span></li>
									</ul>
								</div>
							</div>
							<!-- End Column -->
							<div class="col-md-6 col-divider">
								<div class="number-box box-height">
									<ul>
										<li><strong>No. of Hospital Beds</strong><span>:
												450</span></li>
										<li><strong>No. of ICU Beds</strong><span>: 150</span></li>
										<li><strong>No. of Operating Rooms</strong><span>:
												18</span></li>
										<li><strong>Payment Mode</strong><span>: Cash,
												Bank Transfer</span></li>
										<li><strong>Avg. International Patients</strong><span>:
												1500 to 2000</span></li>
									</ul>
								</div>
							</div>
							<!-- End Column -->
						</div>
						<!-- End Row -->
					</div>
					<!-- End Column -->
					<div class="col-md-3">
						<div class="certificate-box box-height">
							<h4>Accreditations</h4>
							<ul>
								<li><img src="images/certificate/icon01.png" alt="Accreditations"
									class="img-responsive"></li>
							</ul>
						</div>
					</div>
					<!-- End Column -->
				</div>
				<!-- End Row -->
			</div>
			<!-- End Hospital Details -->
		</div>
		<!-- End Detail Container -->
	</div>
	<!-- Hospital Details Ends Here -->

	<!-- About Section Starts Here -->
	<div class="about-section">
		<div class="container">
			<div class="title">
				<h2>
					<span>About</span> JAYPEE Hospital
				</h2>
			</div>
			<div class="description">
				<p>The Jaypee Hospital was conceptualized with the vision of
					promoting world-class healthcare amongst the masses by providing
					quality and affordable medical care with commitment.</p>
				<p>Jaypee Hospital at Noida is the flagship hospital of the
					Jaypee Group, which heralds the group�s noble intention to enter
					the healthcare space. This hospital has been planned and designed
					as a 1200 bedded tertiary care multi-speciality facility and has
					commissioned 525 beds in the first phase.</p>
				<p>The Jaypee Hospital is constructed across a sprawling
					twenty-five acre campus in Sector 128, Noida which is easily
					accessible from Delhi, Noida and the Yamuna Expressway.</p>
				<p>The Jaypee Hospital is established on the following
					fundamental principles: Patient centric high quality care, Evidence
					based medicine, Ethical treatment and Value for money.</p>
				<p>Infrastructure includes 525 beds in first phase, 150 Critical
					Care beds, 24 bedded Advanced Neonatal ICU, 20 bedded Dialysis
					Unit, 325 ward beds with Suite, Deluxe, Twin Sharing and Economy
					options, 18 Modular OTs, 4 Cardiac Catheterization Lab with Hybrid
					Operating Room, 2 Linear Accelerator (IMRT, IGRT and VMAT), 1
					Brachytherapy Suite, Wide Bore CT Simulator, 2 MRI (3.0 Tesla) with
					High Intensity Focused Ultrasound, 256 Slice CT Scan, CT
					Simulation, 64 Slice PET CT, Dual Head 6 Slice SPECT CT, Gamma
					Camera, Da Vinci Robotic Surgery for comprehensive robotic surgical
					solutions.</p>

				<h5>
					To get best options for treatment at Jaypee Hospital, send us a
					query or write to us at <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
				</h5>

			</div>
		</div>
		<!-- End Container -->
	</div>
	<!-- About Section Ends Here -->

	<%@ include file="newFooter.jsp"%>
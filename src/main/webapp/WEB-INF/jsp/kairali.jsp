<%@ include file="newHeader.jsp"%></section>
	<!-- Banner Starts Here -->
	<div class="inner-banner">
		<img src="images/banners/kairali-health-group.jpg"
			alt="Kairali � The Ayurvedic healing village" class="img-responsive">
	</div>
	<!-- Banner Ends Here -->

	<!-- Hospital Short Details Starts Here -->
	<div class="short-details">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="title">
						<h1>Kairali � The Ayurvedic healing village</h1>
						<ul class="paginator">
							<li><a href="/">Home</a></li>
							<li><a>Kairali</a></li>
						</ul>
					</div>
				</div>
				<!-- End Column -->
			</div>
			<!-- End Row -->
		</div>
		<!-- End Container -->
	</div>
	<!-- Short Details Ends Here -->

	<!-- Content Section Starts Here -->
	<div class="content-area">
		<div class="container">

			<div class="main-contentarea">
				<p>World�s First Ayurvedic Health Farm for Perfect Health.
					Kairali was founded in the year 1989. But the effort behind started
					long back. People behind Kairali inherited Ayurveda from their fore
					fathers &amp; propagating throughout the world. The true Ayurveda
					percolated through generations and never lost its originality from
					one generation to the other. Kairali gave a mortal shape of their
					experience in the form of our first Ayurvedic center setup at New
					Delhi and ever since gone miles with a noble cause to enrich body,
					mind &amp; soul of people throughout the world with a holistic
					touch of Ayurveda.</p><br>
				<p>The overwhelming response to the Kairali Ayurvedic Centre, at
					Delhi prompted Mr. K. V. Ramesh and Mrs. Gita Ramesh to start one
					UNCOMPROMISED AYURVEDIC HEALTH RETREAT in more bigger and elaborate
					way at Palakkad, Kerala, (South India) the birth place of Ayurveda,
					and take people even more closer to Nature and Natural Medicine.
					Where one can actually recuperate from stress and strain, get
					treated for one's ailments while you holiday. No wonder then, this
					is the WORLD'S FIRST AYURVEDIC HEALTH FARM TO HAVE A PERFECT
					HEALTH.</p><br>
				<p>Ayurveda literally means the science of life. Ayur means
					�life� &amp; Veda means �science�. It uses all the powers of nature
					helps one heal and live a healthy life style. Today Ayurveda
					competes with the most advance Medical sciences and has effective
					remedies for various ailments and also offers solutions to stress
					&amp; strain, which is accumulated in our day to day living.</p>
				<p>Ayurveda has two way healing aspects i.e. preventing and
					curative.</p><br>
				<p>Ayurveda firstly prevents ailment. The rejuvenation programs
					fall into this category. They help in the general wellbeing and
					strengthen the immune system. Under this vertical Panchakarma is
					the therapy for overall fitness. It tunes the body, organs, mind,
					breath, nerves and purifies blood. Besides it also deals with sweda
					karma which comprises of massages with herbal oil, herbal powders
					and medicated steam.</p><br>
				<p>The curative aspect of it works towards total eradication of
					ailment. This ultimately makes human body free from all possible
					ailments. Almost all known diseases, are treated specially
					paralysis, spondilitis, arthritis, rheumatism, bone &amp; joint
					disorder, slip disk, nervous disorder and their related problems,
					diabetes, hypertension &amp; cardiac related disorders, sinusitis,
					migraine and many more. In most of these cases we have seen an
					improvement of almost 80% after Ayurvedic treatment is
					administered. If treated in the initial stage in nearly all cases
					100% cure is possible. It is also very effective for diet and
					exercise free weight reduction programs.</p><br>
				<h5>
					To get best options for wellness package at Kairali, send us a
					query or write to us at <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
				</h5>
			</div>
			<!-- End Accordion Container -->
		</div>
		<!-- End Container -->
	</div>
	<!-- Content Section Ends Here -->

	<%@ include file="newFooter.jsp"%>
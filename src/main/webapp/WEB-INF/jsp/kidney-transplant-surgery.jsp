<%@ include file="newHeader.jsp"%></section>
	<!-- Hospital Short Details Starts Here -->
	<div class="short-details">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="title">
						<h1>Organ Transplant</h1>
						<ul class="paginator">
							<li><a href="/">Home</a></li>
							<li><a>Organ Transplant</a></li>
						</ul>
					</div>
				</div>
				<!-- End Column -->
			</div>
			<!-- End Row -->
		</div>
		<!-- End Container -->
	</div>
	<!-- Hospital Short Details Ends Here -->

	<!-- Specialty Content Starts Here -->
	<div class="speciality-content-area">
		<div class="container">

			<div class="innertab-accordion">
				<div class="row">
					<div class="col-md-3 tabnopaddright">
						<div class="tabs">
							<ul>
								<li><a href="organ-transplant">Organ
										Transplant</a></li>
								<li class="tab-active"><a href="kidney-transplant-surgery">Kidney Transplant
										Surgery</a></li>
								<li><a href="liver-transplant-surgery">Liver Transplant
										Surgery</a></li>
								<li><a href="heart-transplant">Heart Transplant</a></li>
								<li><a href="bone-marrow-transplant">Bone Marrow
										Transplants</a></li>
								<li><a href="human-organs-transplant-laws-india">Organ Transplant
										laws in India</a></li>
							</ul>
						</div>
					</div>
					<!-- End Column -->
					<div class="col-md-9 tabnopaddleft">
						<div class="accordion-contentarea">

<div id="tab2" >
								<h2>
									<span>Kidney / Renal Transplant </span>-India
								</h2>
								<div class="row">
									<div class="col-md-9 col-sm-9 nopaddleft">
										<p>We at Arvene Healthcare have the expertise to organize for all treatments and surgeries related to Kidney Transplant. Arvene Healthcare is associated with top hospitals and Doctors of India in this field. We can give you more and better options so that you can take the right decisions. We also assist you in taking best decision based on your requirement. Arvene Healthcare will also arrange for your stay and local travel, so that you have nothing else to worry about but your treatment. Please contact us on <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a> today to avail best services.</p>
									</div>
									<!-- End Inner Column -->
									<div class="col-md-3 col-sm-3 nopaddright">
										<img src="images/specialities/39.kidney transplant.jpg" alt="kidney transplant"
											class="img-responsive">
									</div>
									<!-- End Inner Column -->
								</div>
								<!-- End Inner Row -->
								<h3>
									<span>What is a </span>Kidney?
								</h3>
								<p>The Kidneys are pear shaped digestive organs located in the upper abdomen, one in each side of spine. They play a key role in blood filtering and getting rid of bodily waste thereby balancing the electrolyte levels in the body. The kidneys are also vital for controlling blood pressure and stimulating the production of red blood cells. Kidneys get their blood supply through the renal arteries directly from the aorta and send blood back to the heart via the renal veins to the vena cava.</p><br>
								<p>The transplant itself is a surgical process where the surgeon places the new kidney in the abdomen and attaches it to the artery and to the vein. The ureter is attached to the kidney, which carries urine from the kidney to the bladder.</p>
								<h3>
								<span>Kidney </span>Transplant:
								</h3>
								<p>Replacement of diseased or non-working kidneys with a healthy kidney from a living donor. One can live long with one kidney if it is functioning properly. It is for the patients who has end stage renal disease. The Kidney transplant surgeries are of two types:</p>
								<p>1.	<b>Living Donor Kidney Transplant Surgery:</b> The kidney is taken from a close family member or spouse who is compatible and willing to donate. Thorough examination of the donor is done before accepting a candidate.</p>
                                <p>2.	<b>Cadaveric or Deceased Donor Kidney Transplant Surgery:</b> In this the kidney is taken from a brain-dead person and the family members have consented to donate their organs. Extensive blood tests are done on both donor and recipient.</p> 
								<h3>
									<span>Types of </span>Kidney Diseases
								</h3>
								<p>When kidneys are not functioning properly then it's called diseased. There are two types of kidney diseases- acute kidney disease where the damage is sudden and the symptoms are quick, and the other is chronic kidney disease where the decline in the kidney function is slow and progressive. The different types of kidney diseases are:</p>
								<ul>
									<li><span>Kidney cancer</span></li>
									<li><span>Early kidney failure</span></li>
									<li><span>Chronicrenal insufficiency</span></li>
									<li><span>Nephropathy</span></li>
									<li><span>Hyperfiltration</span></li>
									<li><span>Mild microalbuminuria</span></li>
									<li><span>Clinical albuminuria</span></li>
									<li><span>Advanced clinical nephropathy</span></li>
									<li><span>Kidney failure</span></li>
									<li><span>Diabetic nephropathy</span></li>
									<li><span>Nephritis</span></li>
								</ul>
								<h3>
									<span>Causes of </span>Kidney Diseases
								</h3>
								<ul>
									<li><span>Diabetes mellitus,</span></li>
									<li><span>High blood pressure or hypertention</span></li>
									<li><span>Glomerulonephritis causes your kidneys to leak red blood cells into your urine.</span></li>
									<li><span>Polycystic Kidney disease caused by clusters of fluid-filled cysts and develops in the kidneys.</span></li>
									<li><span>Renovascular disease which causes decreased blood flow to the kidneys.</span></li>
									<li><span>Chronic Pyelonephritis causes by repeated episodes of urinary tract infections.</span></li>
									<li><span>Systemic Lupus erythematosus where body mistakenly attacks own kidney tissues.</span></li>
									<li><span>Obstructive Nephropathy is a blockage of urine causing damage to kidneys.</span></li>
									<li><span>Analgesic Nephropathy and Drugs cause kidney damage when over medications quantities are taken.</span></li>
								</ul>
								<p>
									<strong>Diagnostic tests for kidney disease are blood tests, urine tests, measuring kidney function, imaging tests and kidney biopsy.</strong>
								</p>
								<h3>
									<span>Procedure of </span>Kidney transplant
								</h3>
								<p>The procedure includes new kidney placed on the lower right or left side of the patient's abdomen from where the nearby blood vessels are surgically connected. The artery and vein of the kidney will be attached just above one of the legs to an artery and vein in the lower part of the abdomen. To allow urine to pass out of the patient?s body the new kidney ureter, the tube that links the kidney to the bladder, will be connected to blood vessels and the bladder.</p><br>
								<p>A successful kidney transplant can benefit the patient. The patient will be able to get back to its normal lifestyle after the surgery and can go for a normal diet and opt for more fluid intake. After transplant the patient need not to depend on dialysis. Anaemia which is a common problem with kidney failure might also be corrected. For single organ transplants the success rate is 80 % to 90 % of and a 5-10 year survival rate.</p>
								<h3>
									<span>Post-operative Care of the</span> Kidney Transplant
								</h3>
								<p>Post operational care involves taking daily medicines to prevent the immune system from rejecting the new organ. The number of medicines will vary over the years and more anti-rejection medicines are required immediately after the transplant. Some healthy precautions should be taken like eating healthy foods, regular exercise, good sleep.</p>
								<h3>
									<span>Cost of </span>Kidney Transplant
								</h3>
								<p>Compared to international healthcare costs,cost of kidney transplant in India is very less. Hospitals in India treat 7,000 kidney patients annually from all over the world, providing the best medical care at considerably low prices.</p>
Kidney Transplant <h5>Cost in India starts at 12500 US Dollars. To know more about it please share your reports on <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a> or whatsapp/viber/imo to +918130077375.</h5><p> 
(Please note this is a tentative plan and actual Costs are based on reports shared. Final treatment plan and estimation will be provided after the patient has been evaluated.)</p>
								<h5>
									To get best options for Organ transplant treatment, send us a query or write to us at <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
								</h5>
							</div>
							<!-- End Tab -->
						</div>
					</div>
					<!-- End Column -->
				</div>
				<!-- End Row -->
			</div>
			<!-- End Tab Accordion -->

		</div>
		<!-- End Container -->
	</div>
	<!-- Speciality Content Ends Here -->

	<%@ include file="newFooter.jsp"%>
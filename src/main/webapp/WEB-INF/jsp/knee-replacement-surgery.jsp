<%@ include file="newHeader.jsp"%></section>
<!-- Hospital Short Details Starts Here -->
<div class="short-details">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="title">
					<h1>Orthopedic Surgery</h1>
					<ul class="paginator">
						<li><a href="/">Home</a></li>
						<li><a>Orthopedic Surgery</a></li>
					</ul>
				</div>
			</div>
			<!-- End Column -->
		</div>
		<!-- End Row -->
	</div>
	<!-- End Container -->
</div>
<!-- Hospital Short Details Ends Here -->

<!-- Specialty Content Starts Here -->
<div class="speciality-content-area">
	<div class="container">

		<div class="innertab-accordion">
			<div class="row">
				<div class="col-md-3 tabnopaddright">
					<div class="tabs">
						<ul>
							<li><a href="orthopaedics-surgery">Orthopedic
									Surgery</a></li>
							<li class="tab-active"><a href="knee-replacement-surgery">Knee Replacement</a></li>
							<li><a href="hip-replacement-surgery">Hip Replacement
									surgery </a></li>
							<li><a href="shoulder-surgery">Shoulder Surgery </a></li>
							<li><a href="foot-ankle-surgery">Foot and Ankle surgery</a></li>
							<li><a href="hand-wrist-surgery">Hand Wrist surgery</a></li>
							<li><a href="elbow-replacement-surgery">Elbow Surgery</a></li>
						</ul>
					</div>
				</div>
				<!-- End Column -->
				<div class="col-md-9 tabnopaddleft">
					<div class="accordion-contentarea">
<div id="tab2">
							<h2>
								<span>Knee Replacement Surgery</span> -India
							</h2>
							<div class="row">
								<div class="col-md-9 col-sm-9 nopaddleft">
									<p>We at Arvene Healthcare have the expertise to organize
										for all treatments and surgeries related to Knee Replacement.
										Arvene Healthcare is associated with top hospitals and Doctors
										of India in this field. We can give you more and better
										options so that you can take the right decisions. We also
										assist you in taking best decision based on your requirement.
										Arvene Healthcare will also arrange for your stay and local
										travel, so that you have nothing else to worry about but your
										treatment. Please contact us on <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
										today to avail best services. &gt;</p>
								</div>
								<!-- End Inner Column -->
								<div class="col-md-3 col-sm-3 nopaddright">
									<img src="images/specialities/32. knee replacement.jpg"
										alt="knee replacement" class="img-responsive">
								</div>
								<!-- End Inner Column -->
							</div>
							<!-- End Inner Row -->
							<h3>
								<span>Total Knee </span>Replacement:
							</h3>
							<p>Treatment of diseased knee by removing the damaged
								cartilage and bone and replacing it with synthetic one is called
								Total Knee replacement. The materials used to create an
								artificial joint are made up of metal (titanium or cobalt
								chrome). They reduce friction thereby reducing pain and
								improving longevity.</p>
							<h3>
								<span>Cause of knee joint </span>disintegration:
							</h3>
							<p>Arthritis is one of the major reasons for knee
								disintegration. There is slow wear and tear of the tissue on
								ends of bone leading to reduction of joint space, severe joint
								pain, and regular shifts in bone movement and friction in the
								exposed bone surface. There are three types of arthritis:-</p>
							<h3>Osteoarthritis</h3>
							<p>Osteoarthritis occurs from 'wear and tear' and is the most
								common reason for the total knee replacement. It usually occurs
								after the age of 50 and is a genetic problem.</p>
							<h3>Rheumatoid Arthritis</h3>
							<p>When the tissues surrounding the bone releases excessive
								fluid due to inflammation, it causes damage to cartilage and
								other parts of the joint and causes pain and stiffness.</p>
							<h3>Traumatic Arthritis</h3>
							<p>This type of arthritis usually occurs after a serious knee
								injury. Due to the knee fracture, the connective tissue and the
								tissue covering the ends of the bone wears away, resulting in
								pain and stiffness.</p>
							<h3>
								<span>Who needs Knee </span>Replacement?
							</h3>
							<p>If a person is not able to carry out daily routine chores
								because of pain and stiffness then he/ she may be considered for
								a total knee replacement. Most people undergo knee replacement
								after the age of 50. Complete history and physical examination
								of the patient is done before deciding for knee replacement. The
								knee implant will last upto 10-15 years.</p>
							<h3>
								<span>Types of </span>Knee Replacement
							</h3>
							<p>There are four types of Knee Replacement procedures:</p>
							<h3>
								<span>1. Partial</span> Knee Replacement
							</h3>
							<p>It is a minimally invasive procedure wherein a single part
								of the affected knee is removed and replaced with the metal
								implant. It is most often considered for patients suffering from
								Osteoarthritis.</p>
							<h3>
								<span>2. Rotating </span>Knee Replacement
							</h3>
							<p>The metal implant installed will not only move back and
								forth but also rotate inwards and outwards.</p>
							<h3>
								<span>3. Gender Specific </span>Knee Replacement
							</h3>
							<p>It is a special technique of knee replacement wherein the
								implants are designed in a different manner for both men and
								women. This is done to improve function and better stability.</p>
							<h3>
								<span>4. Custom Knee</span>Replacement
							</h3>
							<p>It is a type of knee replacement procedure where
								customized incision guides are prepared for the patient. It
								enables the surgeon to do precise removal and replacement of the
								knee joint.</p>
							<h3>
								<span>Benefits and</span> Post-operative care
							</h3>
							<p>The benefits of Total Knee Replacement are pain relief,
								improved mobility and overall improved quality of life.
								Physiotherapy is required for making knee stronger. Stay in the
								hospital can be up to 10 days.</p>
							<h3>
								<span>Cost of </span>Knee replacement Surgery in India
							</h3>
							<p>The cost of Total Knee Replacement in India is quite
								inexpensive as compared to Western countries. India also has
								technology where the implants will have life cycle of 30-35
								years and that too at much lower cost.</p>
<h5>Cost of Knee Replacement surgery in India starts at 6000 US Dollars for single knee and 10000 US Dollars for double knee. To know more about it please share your reports on <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a> or whatsapp/viber/imo to +918130077375.</h5><p> 
(Please note this is a tentative plan and actual Costs are based on reports shared. Final treatment plan and estimation will be provided after the patient has been evaluated.)</p>
							<h5>
								To get best options for Orthopaedic treatment, send us a query
								or write to us at <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
							</h5>
						</div>
						<!-- End Tab -->

					</div>
				</div>
				<!-- End Column -->
			</div>
			<!-- End Row -->
		</div>
		<!-- End Tab Accordion -->

	</div>
	<!-- End Container -->
</div>
<!-- Speciality Content Ends Here -->

<%@ include file="newFooter.jsp"%>
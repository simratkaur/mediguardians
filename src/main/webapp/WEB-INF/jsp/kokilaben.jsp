<%@ include file="newHeader.jsp"%></section>
	<!-- Banner Starts Here -->
	<div class="inner-banner">
		<img src="images/banners/kokila-dhirubhai-ambani-hospital.jpg"
			alt="Kokilaben Dhirubhai Ambani Hospital" class="img-responsive">
	</div>
	<!-- Banner Ends Here -->

	<!-- Hospital Short Details Starts Here -->
	<div class="short-details">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12">
					<div class="title">
						<h1>Kokilaben Dhirubhai Ambani Hospital</h1>
						<p>Mumbai, India</p>
					</div>
				</div>
				<!-- End Column -->
				<!-- <div class="col-md-6 col-sm-6">
                            <div class="site-link">
                                Website : <a href="javascript:;">http://www.aimshospital.org</a>
                            </div>
                        </div>End Column -->
			</div>
			<!-- End Row -->
		</div>
		<!-- End Container -->
	</div>
	<!-- Hospital Short Details Ends Here -->

	<!-- Hospital Details Starts Here -->
	<div class="container">
		<div class="detail-container">
			<div class="hospital-details">
				<div class="row">
					<div class="col-md-9">
						<div class="row">
							<div class="col-md-6 col-divider">
								<div class="address-box box-height">
									<ul>
										<li><strong>Address</strong><span>: Rao Saheb
												Achutrao, Patwardhan Marg, Four Bunglows, Mumbai</span></li>
										<li><strong>City</strong><span>: Mumbai</span></li>
									</ul>
								</div>
							</div>
							<!-- End Column -->
							<div class="col-md-6 col-divider">
								<div class="number-box box-height">
									<ul>
										<li><strong>No. of Hospital Beds</strong><span>:
												750</span></li>
										<li><strong>No. of ICU Beds</strong><span>: 180</span></li>
										<li><strong>No. of Operating Rooms</strong><span>:
												22</span></li>
										<li><strong>Payment Mode</strong><span>: Cash,
												Bank Transfer</span></li>
										<li><strong>Avg. International Patients</strong><span>:
												2000+</span></li>
									</ul>
								</div>
							</div>
							<!-- End Column -->
						</div>
						<!-- End Row -->
					</div>
					<!-- End Column -->
					<div class="col-md-3">
						<div class="certificate-box box-height">
							<h4>Accreditations</h4>
							<ul>
								<li><img src="images/certificate/icon01.png" alt="Accreditations"
									class="img-responsive"></li>
							</ul>
						</div>
					</div>
					<!-- End Column -->
				</div>
				<!-- End Row -->
			</div>
			<!-- End Hospital Details -->
		</div>
		<!-- End Detail Container -->
	</div>
	<!-- Hospital Details Ends Here -->

	<!-- About Section Starts Here -->
	<div class="about-section">
		<div class="container">
			<div class="title">
				<h2>
					<span>About</span> Kokilaben Dhirubhai Ambani Hospital
				</h2>
			</div>
			<div class="description">
				<p>A vision to strengthen healthcare in the communities we serve
					and empower patients to make informed choices, was at the genesis
					of Kokilaben Dhirubhai Ambani Hospital &amp; Medical Research
					Institute.</p>
				<p>Inaugurated in January 2009, the Hospital is a significant
					social initiative from Reliance Group. It is designed to raise
					India's global standing as a healthcare destination, with emphasis
					on excellence in clinical services, diagnostic facilities and
					research activities.</p>
				<p>KDA is the only hospital in Mumbai to function with a FULL
					TIME SPECIALIST SYSTEM that ensures the availability and access to
					the best medical talent around-the-clock. The 750-bed hospital has
					over 350 full-time specialists, 1100 Nurses and about 300
					paramedics, and growing.</p>
				<p>With a vision to offer outstanding patient care, Kokilaben
					Dhirubhai Ambani Hospital lives up to very high ethical and
					professional standards so as to meet the stringent benchmarks that
					characterize the finest medical institutions across the world.
					Equipped with excellent medical infrastructure and staff we wish to
					transform the way our country views healthcare deliverables.</p>

				<h5>
					To get best options for treatment at KDA Hospital, send us a query
					or write to us at <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
				</h5>
			</div>
		</div>
		<!-- End Container -->
	</div>
	<!-- About Section Ends Here -->

	<%@ include file="newFooter.jsp"%>
<%@ include file="newHeader.jsp"%></section>
<!-- Hospital Short Details Starts Here -->
<div class="short-details">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="title">
					<h1>Cosmetic Surgery</h1>
					<ul class="paginator">
						<li><a href="/">Home</a></li>
						<li><a>Cosmetic Surgery</a></li>
					</ul>
				</div>
			</div>
			<!-- End Column -->
		</div>
		<!-- End Row -->
	</div>
	<!-- End Container -->
</div>
<!-- Hospital Short Details Ends Here -->

<!-- Specialty Content Starts Here -->
<div class="speciality-content-area">
	<div class="container">

		<div class="innertab-accordion">
			<div class="row">
				<div class="col-md-3 tabnopaddright">
					<div class="tabs">
						<ul>
							<li><a href="cosmetic-surgery">Cosmetic Surgery</a></li>
							<li><a href="breast-augmentation-surgery">Breast
									Augmentation</a></li>
							<li><a href="breast-reduction-surgery">Breast Reduction</a></li>
							<li class="tab-active"><a href="liposuction-surgery">Liposuction</a></li>
							<li><a href="facelift-surgery">Facelift</a></li>
							<li><a href="rhinoplasty-surgery">Rhinoplasty</a></li>
							<li><a href="cosmetic-laser-surgery">Cosmetic Laser
									Surgery</a></li>
							<li><a href="oral-maxillofacial-surgery">Maxillofacial
									Surgery</a></li>
							<li><a href="surgical-hair-transplant">Surgical Hair
									Transplant</a></li>
						</ul>
					</div>
				</div>
				<!-- End Column -->
				<div class="col-md-9 tabnopaddleft">
					<div class="accordion-contentarea">

<div id="tab4">
							<h2>
								<span>Liposuction Surgery </span>in India
							</h2>
							<p>We at Arvene Healthcare have the expertise to organize for
								all treatments and surgeries related to Liposuction Surgery.
								Arvene Healthcare is associated with top hospitals and Doctors
								of India in this field. We can give you more and better options
								so that you can take the right decisions. We also assist you in
								taking best decision based on your requirement. Arvene
								Healthcare will also arrange for your stay and local travel, so
								that you have nothing else to worry about but your treatment.
								Please contact us on <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a> today to avail
								best services.</p>
							<div class="row">
								<div class="col-md-9 col-sm-9 nopaddleft">

									<h3>
										<span>What is</span> Liposuction?
									</h3>
									<p>Liposuction, is a type of cosmetic surgery in which
										excess fat is removed from desired area hence reshaping part
										of the body. By using this technique, excess or unwanted fat
										can be removed. Breast, hips, thighs, stomach, waist, neck,
										chin, calves and arms are the different body areas from where
										the fat is removed using Liposuction technique. The patients
										who opt for liposuction, generally have a stable body weight
										but would like to remove undesirable deposits of fat in
										specific parts of the body. This surgery is the answer to fat
										removal that cannot be reduced by the means of any physical
										exercise or dieting.</p>

								</div>
								<!-- End Inner Column -->
								<div class="col-md-3 col-sm-3 nopaddright">
									<img src="images/specialities/11.liposuction.jpg"
										alt="liposuction" class="img-responsive">
								</div>
								<!-- End Inner Column -->
							</div>
							<!-- End Inner Row -->


							<span>Types of </span>Liposuction
							</h3>
							<ul>
								<li><span><strong> Tumescent Liposuction: </strong>
										The surgeon will inject a solution into the patient's fatty
										areas before the fat is removed. The saline solution,
										epinephrine and a mild painkiller will contract the blood
										vessels. The solution removes the fat easily, reduce blood
										loss and eases the pain </span></li>
								<li><span><strong> Ultrasound Assisted
											Liposuction: </strong> The ultrasonic energy is used to liquefy the
										fat and is then removed from the body. </span></li>
							</ul>
							<h3>
								<span>Benefits of </span>Liposuction
							</h3>
							<ul>
								<li><span>Feel more in proportion</span></li>
								<li><span>Motivation to pursue a healthier lifestyle</span></li>
								<li><span>Increased self confidence</span></li>
								<li><span>Ability to have a many choice of clothes</span></li>
							</ul>

							<h3>
								<span>After </span>Liposuction
							</h3>
							<p>After the surgery, the patient may experience sore and
								bruised surgical area for some weeks. The patient may also face
								fluid draining out from the incisions. The post operation period
								requires a compression garment for about 3-6 months. The patient
								can get back to its normal routine life after 3-4 days.</p>
							<h3>
								<span>Cost of </span>Liposuction Surgery
							</h3>
							<p>The low cost of Liposuction technique in India attracts
								patients from all over the world. All the doctors in India are
								well trained and highly experienced offering the best treatment
								to their patients.</p>
							<h5>
								To get best options for cosmetic treatment, send us a query or
								write to us at <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
							</h5>
						</div>
						<!-- End Tab -->
					</div>
				</div>
				<!-- End Column -->
			</div>
			<!-- End Row -->
		</div>
		<!-- End Tab Accordion -->

	</div>
	<!-- End Container -->
</div>
<!-- Speciality Content Ends Here -->

<%@ include file="newFooter.jsp"%>
<%@ include file="newHeader.jsp"%></section>
	<!-- Hospital Short Details Starts Here -->
	<div class="short-details">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="title">
						<h1>Organ Transplant</h1>
						<ul class="paginator">
							<li><a href="/">Home</a></li>
							<li><a>Organ Transplant</a></li>
						</ul>
					</div>
				</div>
				<!-- End Column -->
			</div>
			<!-- End Row -->
		</div>
		<!-- End Container -->
	</div>
	<!-- Hospital Short Details Ends Here -->

	<!-- Specialty Content Starts Here -->
	<div class="speciality-content-area">
		<div class="container">

			<div class="innertab-accordion">
				<div class="row">
					<div class="col-md-3 tabnopaddright">
						<div class="tabs">
							<ul>
								<li><a href="organ-transplant">Organ
										Transplant</a></li>
								<li><a href="kidney-transplant-surgery">Kidney Transplant
										Surgery</a></li>
								<li class="tab-active"><a href="liver-transplant-surgery">Liver Transplant
										Surgery</a></li>
								<li><a href="heart-transplant">Heart Transplant</a></li>
								<li><a href="bone-marrow-transplant">Bone Marrow
										Transplants</a></li>
								<li><a href="human-organs-transplant-laws-india">Organ Transplant
										laws in India</a></li>
							</ul>
						</div>
					</div>
					<!-- End Column -->
					<div class="col-md-9 tabnopaddleft">
						<div class="accordion-contentarea">
<div id="tab3" >
								<h2>
									<span>Liver Transplant </span>-India 
								</h2>
								<div class="row">
									<div class="col-md-9 col-sm-9 nopaddleft">
										<p>We at Arvene Healthcare have the expertise to organize for all treatments and surgeries related to Liver Transplant. Arvene Healthcare is associated with top hospitals and Doctors of India in this field. We can give you more and better options so that you can take the right decisions. We also assist you in taking best decision based on your requirement. Arvene Healthcare will also arrange for your stay and local travel, so that you have nothing else to worry about but your treatment. Please contact us on <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a> today to avail best services.</p>
									</div>
									<!-- End Inner Column -->
									<div class="col-md-3 col-sm-3 nopaddright">
										<img src="images/specialities/40.Liver transplant.jpg" alt="Liver transplant"
											class="img-responsive">
									</div>
									<!-- End Inner Column -->
								</div>
								<!-- End Inner Row -->
								<h3>
									Liver Transplant:
								</h3>
								<p>Liver is the largest internal organ and acts as a storage house by removing toxic by-products of certain medications. Liver Transplant is a surgical procedure that replaces a diseased liver with a healthy liver or a segment of a liver from another person called the donor. Liver failure can occur suddenly or as a result of complications from certain medications. Liver transplants are the most accepted treatment for end stage liver disease and acute liver failure. After a transplant, the patient will be free from the disease and lead a fairly normal life.</p>
								<p>Liver Transplants are classified as</p>
								<ul>
								<li><span><b>Deceased Donor Liver Transplant (DDLT):</b> The full liver is used when the donor organ comes from a brain dead but heart beating donor.</span></li>
								<li><span><b>Living Donor Liver Transplant (LDLT):</b> A half (or hemi) liver is used when the donor organ comes from a living donor.</span></li>
								<li><span><b>Auxiliary Transplantation:</b> In this a part of the liver of a healthy adult donor (living or cadaver) is transplanted into the recipient. The patients' diseased liver remains intact until the auxiliary piece regenerates and assumes function and then the diseased liver may then be removed.</span></li>
								<h3>
									<span>Criteria for </span>Liver Transplant
								</h3>
								<p>When the liver is so diseased that is not able to maintain the normal body functions, then liver transplant is required. There are various conditions that can lead to a liver failure. They are:</p>
								<ul>
									<li><span>Chronic Hepatitis with Cirrhosis</span></li>
									<li><span>Alcoholism</span></li>
									<li><span>Biliary Artresia (malfunction of the bile
											ducts)</span></li>
									<li><span>Wilson's Disease ( a rare inherited disease with abnormal deposition of copper throughout the body and liver)</span></li>
									<li><span>Alpha 1 Antitrypsin Deficiency Liver Cancer</span></li>
									<li><span>Primary Biliary Cirrhosis </span></li>
									<li><span>Sclerosing Cholangitis (scarring and narrowing of the bile ducts causing the backup of bile in the liver)</span></li>
									<li><span>Hemochromatosis ( a common inherited disease where the body is overwhelmed with iron)</span></li>
								</ul>
								<h3>
									<span>Tests Required Before Getting a </span> Liver Transplant
								</h3>
								<p>Following diagnostic studies are performed during an evaluation. They are</p>
								<ul>
									<li><span>Doppler ultrasound to determine if the blood vessels to and from the liver are open.</span></li>
									<li><span>Pulmonary function studies to determine the lungs' ability to exchange oxygen and carbon dioxide.</span></li>
									<li><span>Echocardiogram to help evaluate the patient's heart.</span></li>
									<li><span>Computed tomography which uses X-rays and a computer to generate pictures of the liver and showing its shape and size.</span></li>
									<li><span>Blood tests examine blood type, biochemical status of blood, clotting ability and to gauge liver function. AIDS and Hepatitis tests are also included.</span></li>
								</ul>
								<h3>
									<span>Donor for </span>Liver Transplant
								</h3>
								<ul>
									<li><span><strong> Living-Donor: </strong> This process involves removing a segment of liver from a healthy living donor and implanting it into a recipient. Both the donor and recipient liver segments will grow to a normal size in a few weeks.The donor must be a blood relative or spouse. Blood type and body size are critical factors in determining an appropriate donor.</span></li>
								</ul>
								<h3>
									<span>Liver Transplant</span> Procedure
								</h3>
								<p>The procedure involves few steps:</p>
								<ul>
									<li><span>The surgery usually takes 5 to 6 hours</span></li>
									<li><span>Liver Transplantation is performed under a general anaesthesia.</span></li>
									<li><span>An incision is made in the abdomen and the diseased liver is removed. Donated liver is then inserted and attached to the major blood vessels and to the bile ducts. </span></li>
								</ul>
								<h3>
									<span>Advantages of</span> Liver Transplant:
								</h3>
								<ul>
									<li><span>Provides a chance for a longer active life.</span></li>
									<li><span>If the transplant is performed before the recipient's health deteriorates then they are able to better tolerate the surgery and recovers more quickly.</span></li>
									<li><span>The recovery can sometimes become difficult but the anti - rejection medicines makes the process easy.</span></li>
									<li><span>The donors will discharge in a week and recipients in 2 to 3 weeks.</span></li>
								</ul>
								<h3>
									<span>Post</span> Liver Transplant
								</h3>
								<p>The process involves carrying out daily life activities and recovering to the level of health. The process can be slow which includes simple activities like walking, deep breathing to ensure the lungs stay healthy to prevent pneumonia, well balanced diet and exercise to build up the muscles weakened by illness.</p>
								<h3>
									<span>Cost of</span> Liver Transplant in India
								</h3>
								<p>The cost for Liver Transplant in India is low and hence it allows people from other countries to avail and ensure the best medical care. Compared to the West, surgical fees is much less. </p>
Liver Transplant <h5>Cost in India starts at 35000 US Dollars. To know more about it please share your reports on <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a> or whatsapp/viber/imo to +918130077375.</h5><p> 
(Please note this is a tentative plan and actual Costs are based on reports shared. Final treatment plan and estimation will be provided after the patient has been evaluated.)</p>
								<h5>
									To get best options for Organ transplant treatment, send us a query or write to us at <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
								</h5>
							</div>
							<!-- End Tab -->

						</div>
					</div>
					<!-- End Column -->
				</div>
				<!-- End Row -->
			</div>
			<!-- End Tab Accordion -->

		</div>
		<!-- End Container -->
	</div>
	<!-- Speciality Content Ends Here -->

	<%@ include file="newFooter.jsp"%>
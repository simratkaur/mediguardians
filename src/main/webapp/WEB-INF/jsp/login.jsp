<%@ include file="newHeader.jsp"%>
		<div class="container">
		<div class="banner-text col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<h1>Consult India's Top Doctors</h1>
			<h4>Connect with India's leading hospitals</h4>

			<a data-toggle="modal" data-target="#enqueryForm" href="#callback">Request a call back</a>
		</div>
	</div>
</section>

<section class="below-banner">
<div class="container-fluid">
<div class="row">

		<div class="find-doctor col-lg-4 col-md-4 col-sm-4 col-xs-12">
			<p>
				Find the right doctor and hospital for you
			</p>
		</div>

		<div class="get-opinion col-lg-4 col-md-4 col-sm-4 col-xs-12">
			<p>
				Get medical opinion, cost of treatment & complete information
			</p>
		</div>

		<div class="appointment-out col-lg-4 col-md-4 col-sm-4 col-xs-12">

			<div class="appointment col-lg-10 col-md-10 col-sm-10 col-xs-12">
				<p>
					Complete assistance for appointments, admissions, travel and stay
				</p>
			</div>
		</div>
</div>
</div>
</section>

<section class="treatment-sec">
<div class="container">
<div class="row">

			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<h1>Treatments</h1>
			</div>

		
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
					
					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">

						<div class="treatment-box col-lg-12">
							<div class="radius-img">
								<img src="images/treatment2.png" alt="brain">
							</div>
							<h4>Organ Transplant</h4>

							<a href="liver-transplant-surgery"><p>Liver Transplant</p></a>
							<a href="kidney-transplant-surgery"><p>Kidney Transplant</p></a>
							<a href="bone-marrow-transplant"><p>Bone Marrow Transplant</p></a>

							<div class="learndiv">
								<a href="organ-transplant" class="learnbtn">Learn More</a>
							</div>

						</div>
						
					</div>

					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">

						<div class="treatment-box col-lg-12">
							<div class="radius-img">
								<img src="images/treatment5.png" alt="brain">
							</div>
							<h4>Cardiac Treatments </h4>

							<a href="coronary-angioplasty"><p>Coronary Angioplasty</p></a>
							<a href="heart-valve-replacement-surgery"><p>Heart Bypass Surgery</p></a>
							<a href="pacemaker-implantation"><p>Pacemaker Implantation</p></a>

							<div class="learndiv">
								<a href="cardiology-treatment" class="learnbtn">Learn More</a>
							</div>

						</div>
						
					</div>

					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">

						<div class="treatment-box col-lg-12">
							<div class="radius-img">
								<img src="images/treatment4.png" alt="brain">
							</div>
							<h4>Orthopedics</h4>

							<a href="knee-replacement-surgery"><p>Knee Replacement</p></a>
							<a href="hip-replacement-surgery"><p>Hip Replacement</p></a>
							<a href="foot-ankle-surgery"><p>Foot & Ankle Surgery</p></a>

							<div class="learndiv">
								<a href="orthopaedics-surgery" class="learnbtn">Learn More</a>
							</div>

						</div>
						
					</div>

				
			</div>

			<div class="margin-top-btm col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
				
					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">

						<div class="treatment-box col-lg-12">
							<div class="radius-img">
								<img src="images/treatment1.png" alt="brain">
							</div>
							<h4>Neuro and Spine</h4>

							<a href="brain-tumors-treatment"><p>Brain Tumor</p></a>
							<a href="intracranial-aneurysm-surgery"><p>Brain Aneurysm</p></a>
							<a href="neuro-surgery"><p>Neurosurgery</p></a>

							<div class="learndiv">
								<a href="neuro-surgery" class="learnbtn">Learn More</a>
							</div>
							

						</div>
						
					</div>

					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">

						<div class="treatment-box col-lg-12">
							<div class="radius-img">
								<img src="images/treatment3.png" alt="brain">
							</div>
							<h4>Infertility </h4>

							<a href="infertility-treatment"><p>Infertility treatment</p></a>
							<a href="IVF-treatment"><p>In-Vitro Fertilization(IVF)</p></a>
							<a href="IUI-treatment"><p>Intra Uterine Insemination(IUI</p></a>

							<div class="learndiv">
								<a href="infertility-treatment" class="learnbtn">Learn More</a>
							</div>

						</div>
						
					</div>

					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">

						<div class="treatment-box col-lg-12">
							<div class="radius-img">
								<img src="images/treatment6.png" alt="brain">
							</div>
							<h4>Cancer Treatment</h4>

							<a href="breast-cancer-treatment"><p>Breast Cancer</p></a>
							<a href="prostate-cancer-treatment"><p>Prostate Cancer</p></a>
							<a href="cyberknife-radiation-therapy"><p>Cyberknife Radiation</p></a>

							<div class="learndiv">
								<a href="cancer-treatment" class="learnbtn">Learn More</a>
							</div>

						</div>
						
					</div>
					
				
			</div>

		</div>
</div>
</div>
</section><!-- treatment-sec -->

<section class="doctors-sec">
		<div class="container">
			

				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
					<h1>Meet Our Doctors</h1>

					<p>The doctors associated with Arvene Healthcare have outstanding track record and excellent success rates dealing with 
complex medical diseases. </p>
				</div>

					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding  margin-top-btm">

						<div class="row">

							<div class="doctors-info col-lg-3 col-md-3 col-sm-3 col-xs-12">
								<a href="#"><img src="images/doctor.png" alt="Dr. Subash Gupta">
								<h3>Dr. Subash Gupta</h3>
								<p>Liver Transplant Surgery</p></a>
							</div>

							<div class="doctors-info col-lg-3 col-md-3 col-sm-3 col-xs-12">
								<a href="#"><img src="images/doctor2.png" alt="Dr. Anil Arora">
								<h3>Dr. Anil Arora </h3>
								<p>Knee and Hip Replacement</p></a>
							</div>

							<div class="doctors-info col-lg-3 col-md-3 col-sm-3 col-xs-12">
								<a href="#"><img src="images/doctor3.png" alt="Dr. Vedant Kabra">
								<h3>Dr. Vedant Kabra</h3>
								<p>Surgical Oncology</p></a>
							</div>

							<div class="doctors-info col-lg-3 col-md-3 col-sm-3 col-xs-12">
								<a href="#"><img src="images/doctor4.png" alt="Dr. Dharma Chowdhry">
								<h3>Dr. Dharma Chowdhry</h3>
								<p>Bone Marrow Transplant</p></a>
							</div>

						</div>

					</div>
							<div class="learndiv">
										<a href="specialists" class="learnbtn">View All Doctors</a>
							</div>


					
			
		</div>
</section><!-- doctors-sec -->


<section class="hospital-sec  margin-top-btm">
	<div class="container">
		<div class="row">

			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

			<h1>Hospital Network</h1>
			</div>

			<div class="hospitals col-lg-2 col-md-2 col-sm-2 col-xs-6">
				<a href="#"><img src="images/network.png" alt="hospital" class="img-responsive"></a>
			</div>

			<div class="hospitals col-lg-2 col-md-2 col-sm-2 col-xs-6">
				<a href="#"><img src="images/network2.png" alt="hospital" class="img-responsive"></a>
			</div>

			<div class="hospitals col-lg-2 col-md-2 col-sm-2 col-xs-6">
				<a href="#"><img src="images/network3.png" alt="hospital" class="img-responsive"></a>
			</div>

			<div class="hospitals col-lg-2 col-md-2 col-sm-2 col-xs-6">
				<a href="#"><img src="images/network4.png" alt="hospital" class="img-responsive"></a>
			</div>

			<div class="hospitals col-lg-2 col-md-2 col-sm-2 col-xs-6">
				<a href="#"><img src="images/network5.png" alt="hospital" class="img-responsive"></a>
			</div>

			<div class="hospitals col-lg-2 col-md-2 col-sm-2 col-xs-6">
				<a href="#"><img src="images/network6.png" alt="hospital" class="img-responsive"></a>
			</div>

		</div>
	</div>
</section><!-- hospital-sec -->


<section class="patient-sec">
	<div class="container">
		<div class="row">

			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<h1>Patient Testimonial</h1>
			</div>

			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">

				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">

					<div class="patient-detail col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="col-lg-1 col-md-1 col-sm-1 hidden-xs">
							<i class="fa fa-quote-left" aria-hidden="true"></i>
						</div>

						<div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
							<p>I came to India for liver transplant for my father and Arvene Healthcare team guided us at every step. The surgery was complex but the surgeon recommended by them saved his life</p>

							<strong>Rasheed </strong><span>- Pakistan</span>
						</div>
					</div>

				</div><!-- patient-detail -->

				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">

					<div class="patient-detail col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="col-lg-1 col-md-1 col-sm-1 hidden-xs">
							<i class="fa fa-quote-left" aria-hidden="true"></i>
						</div>

						<div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
							<p>Thank you Arvene Healthcare for finding the best cancer surgeon in India for us. We consulted  directly with the cancer specialist for a second opinion</p>

							<strong>Gikuyu </strong><span>- Kenya</span>
						</div>
					</div>

				</div><!-- patient-detail -->

				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">

					<div class="patient-detail col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="col-lg-1 col-md-1 col-sm-1 hidden-xs">
							<i class="fa fa-quote-left" aria-hidden="true"></i>
						</div>

						<div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
							<p>My parents travelled alone to India for Knee Replacement surgery and I was totally dependent on Arvene Healthcare for their care. The team took care of them like family</p>

							<strong>Emmanuel </strong><span>- Nigeria </span>
						</div>
					</div>

				</div><!-- patient-detail -->

				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">

					<div class="patient-detail col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="col-lg-1 col-md-1 col-sm-1 hidden-xs">
							<i class="fa fa-quote-left" aria-hidden="true"></i>
						</div>

						<div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
							<p>We were unable to find the right doctors for stem cell therapy for our child. Arvene Healthcare made it possible with complete travel assistance to India </p>

							<strong>Ayub </strong><span>- Saudi Arabia</span>
						</div>
					</div>

				</div><!-- patient-detail -->

			</div>
			

		</div>
	</div>
</section>


<section class="footer-banner-sec">
	<div class="container">
		<div class="row">

			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<h1>Need a Second Opinion?</h1>
					<div class="learndiv">
						<ul>
							<li><a href="tel:(+91) 81300-77375" class="learnbtn btn">(+91) 81300-77375</a></li>
							<li><a href="userQuery" class="learnbtn btn">Consult Online</a></li>
						</ul>
						
					</div>
			</div>
		</div>
	</div>
</section>
<%@ include file="newFooter.jsp"%>

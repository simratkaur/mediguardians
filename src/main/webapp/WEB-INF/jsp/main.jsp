<%@ include file="newHeader.jsp"%></section>
<%-- <%@ include file="specialistIncludes.jsp"%>
<%@ include file="navigator.jsp"%> --%>

<!-- /header -->
<!--  Dashboard div start -->

<section class="sec-main" style="min-height: 380px">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="top-heading-s">
					<h4>Dashboard</h4>
				</div>
				<br>
				<p class="list-group-item-text">
					You have <a href="bookings">${bookings_count} Session to attend</a>
					and <a href="queries">${query_count} Queries to respond</a>
				</p>
			</div>
		</div>
	</div>
</section>

<%@ include file="newFooter.jsp"%>
<%@ include file="newHeader.jsp"%></section>
	<!-- Banner Starts Here -->
	<div class="inner-banner">
		<img src="images/banners/max-hospital.jpg"
			alt="MAX Super Speciality Hospital" class="img-responsive">
	</div>
	<!-- Banner Ends Here -->

	<!-- Hospital Short Details Starts Here -->
	<div class="short-details">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12">
					<div class="title">
						<h1>MAX Super Speciality Hospital</h1>
						<p>Saket, Delhi</p>
					</div>
				</div>
				<!-- End Column -->
				<!--  <div class="col-md-6 col-sm-6">
                            <div class="site-link">
                                Website : <a href="javascript:;">http://www.aimshospital.org</a>
                            </div>
                        </div>End Column -->
			</div>
			<!-- End Row -->
		</div>
		<!-- End Container -->
	</div>
	<!-- Hospital Short Details Ends Here -->

	<!-- Hospital Details Starts Here -->
	<div class="container">
		<div class="detail-container">
			<div class="hospital-details">
				<div class="row">
					<div class="col-md-9">
						<div class="row">
							<div class="col-md-6 col-divider">
								<div class="address-box box-height">
									<ul>
										<li><strong>Address</strong><span>: Address: 1,2,
												Press Enclave Road, Saket, New Delhi</span></li>
										<li><strong>City</strong><span>: New Delhi</span></li>
									</ul>
								</div>
							</div>
							<!-- End Column -->
							<div class="col-md-6 col-divider">
								<div class="number-box box-height">
									<ul>
										<li><strong>No. of Hospital Beds</strong><span>:
												490</span></li>
										<li><strong>No. of ICU Beds</strong><span>: 80</span></li>
										<li><strong>No. of Operating Rooms</strong><span>:
												20</span></li>
										<li><strong>Payment Mode</strong><span>: Cash,
												Bank Transfer</span></li>
										<li><strong>Avg. International Patients</strong><span>:
												4000+</span></li>
									</ul>
								</div>
							</div>
							<!-- End Column -->
						</div>
						<!-- End Row -->
					</div>
					<!-- End Column -->
					<div class="col-md-3">
						<div class="certificate-box box-height">
							<h4>Accreditations</h4>
							<ul>
								<li><img src="images/certificate/icon01.png"
									alt="Accreditations" class="img-responsive"></li>
							</ul>
						</div>
					</div>
					<!-- End Column -->
				</div>
				<!-- End Row -->
			</div>
			<!-- End Hospital Details -->
		</div>
		<!-- End Detail Container -->
	</div>
	<!-- Hospital Details Ends Here -->

	<!-- About Section Starts Here -->
	<div class="about-section">
		<div class="container">
			<div class="title">
				<h2>
					<span>About</span> MAX Super Speciality Hospital
				</h2>
			</div>
			<div class="description">
				<p>Designed by internationally renowned architect Mr. Richard
					Wood and constructed in accordance with globally-accepted standards
					for hospitals, quality is our top priority. Located in the heart of
					South Delhi, Max Super Speciality Hospital, Saket, is one of the
					premier names in health care domain in the world. With over 2600
					beds and 13 top hospitals in Delhi-NCR, Punjab and Uttarakhand,
					2300 world-class doctors, Max Healthcare is one of the leading
					chain of hospitals in India. With over 500 ICU beds, the most
					advanced technology and state-of-the-art infrastructure, Max
					Healthcare is one of the best hospitals in India.</p>
				<p>The specialities include � Bariatric and weight loss surgery,
					Cardiac sciences, Neuro-Sciences, Oncology, Orthopedics,
					Reconstructive surgery, Bone Marrow transplant, Kidney transplant,
					IVF, Urology, Nephrology etc.</p>
				<h5>
					To get best options for treatment at MAX Hospital, send us a query
					or write to us at <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
				</h5>
			</div>
		</div>
		<!-- End Container -->
	</div>
	<!-- About Section Ends Here -->

<%@ include file="newFooter.jsp"%>
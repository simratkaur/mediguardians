<%@ include file="newHeader.jsp"%></section>
	<!-- Banner Starts Here -->
	<div class="inner-banner">
		<img src="images/banners/sub-banner.jpg" alt="Medical Process"
			class="img-responsive">
	</div>
	<!-- Banner Ends Here -->

	<!-- Short Details Starts Here -->
	<div class="short-details">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="title">
						<h1>Medical Process Flow</h1>
						<ul class="paginator">
							<li><a href="/">Home</a></li>
							<li><a>Medical Process Flow</a></li>
						</ul>
					</div>
				</div>
				<!-- End Column -->
			</div>
			<!-- End Row -->
		</div>
		<!-- End Container -->
	</div>
	<!-- Short Details Ends Here -->

	<!-- Content Section Starts Here -->
	<div class="content-area">
		<div class="container">

			<div class="main-contentarea">
			<img alt="Medical Process Flow" src="images/services/medical-process.jpg">
			</div>
			<!-- End Accordion Container -->
		</div>
		<!-- End Container -->
	</div>
	<!-- Content Section Ends Here -->

	<%@ include file="newFooter.jsp"%>
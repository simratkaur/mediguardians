<%@ include file="newHeader.jsp"%></section>
<!-- Hospital Short Details Starts Here -->
<div class="short-details">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="title">
					<h1>Medical Providers</h1>
					<ul class="paginator">
						<li><a href="/">Home</a></li>
						<li><a>Medical Providers</a></li>
					</ul>
				</div>
			</div>
			<!-- End Column -->
		</div>
		<!-- End Row -->
	</div>
	<!-- End Container -->
</div>
<!-- Hospital Short Details Ends Here -->

<!-- Listing Section Starts Here -->
<div class="listing-section">
	<div class="container">
		<div class="row">

			<div class="col-md-6 col-sm-6"
				onclick="location.href='apolloChennai'" style="cursor: pointer;">
				<div class="list-box">
					<div class="row">
						<h2>
							<span>Apollo</span> Hospital
						</h2>
						<div class="col-md-5">
							<div class="hospital-img-container">
								<img
									src="images/hospitals/ApolloChennai.jpg"
									alt="Apollo Hospital" class="img-responsive">
							</div>
						</div>
						<!-- End Inner Column -->
						<div class="col-md-7">
							<div class="detail-area">

								<ul class="address">
									<!-- <li class="link"><a href="javascript:;" target="_blank">www.fortishospital.com</a></li> -->
									<li class="location">Chennai</li>
								</ul>

								<p>The flagship hospital of the Apollo Group, Apollo
									Hospitals, Chennai, was established in 1983....</p>

								<h5>Speciality :</h5>
								<ul class="tags">
									<li><a href="cardiology-treatment">Cardiology</a></li>
									<li><a href="cancer-treatment">Cancer</a></li>
									<li><a href="orthopaedics-surgery">Orthopaedics</a></li>
								</ul>
							</div>
						</div>
						<!-- End Inner Column -->
					</div>
					<!-- End Inner Row -->
				</div>
				<!-- End List Box -->
			</div>
			<!-- End Column -->
			<div class="col-md-6 col-sm-6"
				onclick="location.href='indraprasthaApollo'"
				style="cursor: pointer;">
				<div class="list-box">
					<div class="row">
						<h2>
							<span>Indraprastha</span> Apollo Hospital
						</h2>
						<div class="col-md-5">
							<div class="hospital-img-container">
								<img
									src="images/hospitals/IndraprasthaApolloHospital.jpg"
									alt="Indraprastha Apollo Hospital" class="img-responsive">
							</div>
						</div>
						<!-- End Inner Column -->
						<div class="col-md-7">
							<div class="detail-area">

								<ul class="address">
									<!-- <li class="link"><a href="javascript:;" target="_blank">www.fortishospital.com</a></li> -->
									<li class="location">Delhi</li>
								</ul>

								<p>Indraprastha Apollo Hospitals, New Delhi is a
									multi-specialty tertiary acute care hospital with 710 beds...</p>

								<!-- <h5>Speciality :</h5>
								<ul class="tags">
									<li><a href="cardiology-treatment">Cardiology</a></li>
									<li><a href="cancer-treatment">Cancer</a></li>
									<li><a href="orthopaedics-surgery">Orthopaedics</a></li>
								</ul> -->
							</div>
						</div>
						<!-- End Inner Column -->
					</div>
					<!-- End Inner Row -->
				</div>
				<!-- End List Box -->
			</div>
			<!-- End Column -->

			<div class="col-md-6 col-sm-6"
				onclick="location.href='fortisGurgaon'" style="cursor: pointer;">
				<div class="list-box">
					<div class="row">
						<h2>
							<span>Fortis</span> Medical and Research Institute
						</h2>
						<div class="col-md-5">
							<div class="hospital-img-container">
								<img
									src="images/hospitals/FortisMedicalAndResearchInstitute.jpg"
									alt="Fortis Medical and Research Institute"
									class="img-responsive">
							</div>
						</div>
						<!-- End Inner Column -->
						<div class="col-md-7">
							<div class="detail-area">

								<ul class="address">
									<!-- <li class="link"><a href="javascript:;" target="_blank">www.fortishospital.com</a></li> -->
									<li class="location">Gurgaon, Delhi NCR</li>
								</ul>

								<p>Fortis Healthcare Limited is a leading, integrated
									healthcare delivery provider in the pan Asia- Pacific region...</p>

								<h5>Speciality :</h5>
								<ul class="tags">
									<li><a href="cardiology-treatment">Cardiology</a></li>
									<li><a href="cancer-treatment">Cancer</a></li>
									<li><a href="orthopaedics-surgery">Orthopaedics</a></li>
								</ul>
							</div>
						</div>
						<!-- End Inner Column -->
					</div>
					<!-- End Inner Row -->
				</div>
				<!-- End List Box -->
			</div>
			<!-- End Column -->

			<div class="col-md-6 col-sm-6" onclick="location.href='maxDelhi'"
				style="cursor: pointer;">
				<div class="list-box">
					<div class="row">
						<h2>
							<span>MAX Super </span>Speciality Hospital
						</h2>
						<div class="col-md-5">
							<div class="hospital-img-container">
								<img src="images/hospitals/MAXSuperSpecialityHospital.jpg"
									alt="MAX Super Speciality Hospital" class="img-responsive">
							</div>
						</div>
						<!-- End Inner Column -->
						<div class="col-md-7">
							<div class="detail-area">

								<ul class="address">
									<!-- 	<li class="link"><a href="javascript:;" target="_blank">www.fortishospital.com</a></li> -->
									<li class="location">1,2, Press Enclave Road, Saket, New
										Delhi</li>
								</ul>

								<p>Designed by internationally renowned architect Mr.
									Richard Wood and constructed in accordance with
									globally-accepted standards...</p>

								<!-- <h5>Speciality :</h5>
										<ul class="tags">
											<li><a href="javascript:;">cardiology</a></li>
											<li><a href="javascript:;">cardiology</a></li>
											<li><a href="javascript:;">cardiology</a></li>
										</ul> -->
							</div>
						</div>
						<!-- End Inner Column -->
					</div>
					<!-- End Inner Row -->
				</div>
				<!-- End List Box -->
			</div>
			<!-- End Column -->

			<div class="col-md-6 col-sm-6" onclick="location.href='apolloCancer'"
				style="cursor: pointer;">
				<div class="list-box">
					<div class="row">
						<h2>
							<span>Apollo </span>Cancer Hospital
						</h2>
						<div class="col-md-5">
							<div class="hospital-img-container">
								<img src="images/hospitals/ApolloCancer.jpg"
									alt="Apollo Cancer Hospital" class="img-responsive">
							</div>
						</div>
						<!-- End Inner Column -->
						<div class="col-md-7">
							<div class="detail-area">

								<ul class="address">
									<!-- <li class="link"><a href="javascript:;" target="_blank">www.fortishospital.com</a></li> -->
									<li class="location">Chennai</li>
								</ul>

								<p>Apollo Cancer Institute, India's first ISO certified
									healthcare provider is today ranked among the top super
									specialty hospitals...</p>

								<!-- <h5>Speciality :</h5>
										<ul class="tags">
											<li><a href="javascript:;">cardiology</a></li>
											<li><a href="javascript:;">cardiology</a></li>
											<li><a href="javascript:;">cardiology</a></li>
										</ul> -->
							</div>
						</div>
						<!-- End Inner Column -->
					</div>
					<!-- End Inner Row -->
				</div>
				<!-- End List Box -->
			</div>
			<!-- End Column -->

			<div class="col-md-6 col-sm-6" onclick="location.href='artemis'"
				style="cursor: pointer;">
				<div class="list-box">
					<div class="row">
						<h2>
							<span>Artemis</span> Hospital
						</h2>
						<div class="col-md-5">
							<div class="hospital-img-container">
								<img src="images/hospitals/ArtemisHospital.jpg" alt="Artemis Hospital"
									class="img-responsive">
							</div>
						</div>
						<!-- End Inner Column -->
						<div class="col-md-7">
							<div class="detail-area">

								<ul class="address">
									<!-- <li class="link"><a href="javascript:;" target="_blank">www.fortishospital.com</a></li> -->
									<li class="location">Gurgaon, NCR</li>
								</ul>

								<p>Artemis Hospital, established in 2007, spread across 9
									acres, is a 380 bed; state-of-the-art multi-speciality
									hospital...</p>
								<!-- <h5>Speciality :</h5>
										<ul class="tags">
											<li><a href="javascript:;">cardiology</a></li>
											<li><a href="javascript:;">cardiology</a></li>
											<li><a href="javascript:;">cardiology</a></li>
										</ul> -->
							</div>
						</div>
						<!-- End Inner Column -->
					</div>
					<!-- End Inner Row -->
				</div>
				<!-- End List Box -->
			</div>
			<!-- End Column -->

			<div class="col-md-6 col-sm-6"
				onclick="location.href='fortisExcorts'" style="cursor: pointer;">
				<div class="list-box">
					<div class="row">
						<h2>
							<span>Fortis Escorts</span> Heart Institute
						</h2>
						<div class="col-md-5">
							<div class="hospital-img-container">
								<img src="images/hospitals/FortisEscortsHeartInstitute.jpg"
									alt="Fortis Escorts Heart Institute" class="img-responsive">
							</div>
						</div>
						<!-- End Inner Column -->
						<div class="col-md-7">
							<div class="detail-area">

								<ul class="address">
									<!-- <li class="link"><a href="javascript:;" target="_blank">www.fortishospital.com</a></li> -->
									<li class="location">Okhla Road, Opp Holy Family Hospital,
										New Delhi</li>
								</ul>

								<p>Fortis Escorts Heart Institute is a pioneer in the field
									of fully dedicated cardiac care in India...</p>

								<h5>Speciality :</h5>
								<ul class="tags">
									<li><a href="cardiology-treatment">Cardiology</a></li>
									<li><a href="coronary-artery-bypass-surgery">Cardiac Bypass
											Surgery</a></li>
									<li><a href="paediatric-cardiac-surgery">Pediatric
											Cardiology</a></li>
								</ul>
							</div>
						</div>
						<!-- End Inner Column -->
					</div>
					<!-- End Inner Row -->
				</div>
				<!-- End List Box -->
			</div>
			<!-- End Column -->

			<div class="col-md-6 col-sm-6"
				onclick="location.href='apolloBangalore'" style="cursor: pointer;">
				<div class="list-box">
					<div class="row">
						<h2>
							<span>Apollo</span> Hospital
						</h2>
						<div class="col-md-5">
							<div class="hospital-img-container">
								<img src="images/hospitals/ApolloBangalore.jpg"
									alt="Apollo Hospital" class="img-responsive">
							</div>
						</div>
						<!-- End Inner Column -->
						<div class="col-md-7">
							<div class="detail-area">

								<ul class="address">
									<!-- <li class="link"><a href="javascript:;" target="_blank">www.fortishospital.com</a></li> -->
									<li class="location">Bangalore</li>
								</ul>

								<p>Apollo Hospital Bangalore is a tertiary care flagship
									unit of the Apollo Hospitals Group. It made a mark in the city
									of Bangalore in 2007...</p>

								<!-- <h5>Speciality :</h5>
								<ul class="tags">
									<li><a href="cardiology-treatment">Cardiology</a></li>
									<li><a href="coronary-artery-bypass-surgery">Cardiac Bypass
											Surgery</a></li>
									<li><a href="paediatric-cardiac-surgery">Pediatric
											Cardiology</a></li>
								</ul> -->
							</div>
						</div>
						<!-- End Inner Column -->
					</div>
					<!-- End Inner Row -->
				</div>
				<!-- End List Box -->
			</div>
			<!-- End Column -->

<div class="col-md-6 col-sm-6"
				onclick="location.href='globalChennai'" style="cursor: pointer;">
				<div class="list-box">
					<div class="row">
						<h2>
							<span>Global</span> Hospital
						</h2>
						<div class="col-md-5">
							<div class="hospital-img-container">
								<img src="images/hospitals/GlobalHospital.jpg"
									alt="Global Hospital" class="img-responsive">
							</div>
						</div>
						<!-- End Inner Column -->
						<div class="col-md-7">
							<div class="detail-area">

								<ul class="address">
									<!-- <li class="link"><a href="javascript:;" target="_blank">www.fortishospital.com</a></li> -->
									<li class="location">Chennai</li>
								</ul>

								<p>Global Hospitals Group, India’s most renowned healthcare
				services provider offering better care, cutting-edge research and
				advanced education to caregivers...</p>

								<!-- <h5>Speciality :</h5>
								<ul class="tags">
									<li><a href="cardiology-treatment">Cardiology</a></li>
									<li><a href="coronary-artery-bypass-surgery">Cardiac Bypass
											Surgery</a></li>
									<li><a href="paediatric-cardiac-surgery">Pediatric
											Cardiology</a></li>
								</ul> -->
							</div>
						</div>
						<!-- End Inner Column -->
					</div>
					<!-- End Inner Row -->
				</div>
				<!-- End List Box -->
			</div>
			<!-- End Column -->
			
			<div class="col-md-6 col-sm-6"
				onclick="location.href='fortisVasantKunj'" style="cursor: pointer;">
				<div class="list-box">
					<div class="row">
						<h2>
							<span>Fortis Flt. Lt. </span>Rajan Dhall Hospital
						</h2>
						<div class="col-md-5">
							<div class="hospital-img-container">
								<img src="images/hospitals/FortisFltLtRajanDhallHospital.jpg"
									alt="Fortis Flt. Lt. Rajan Dhall Hospital"
									class="img-responsive">
							</div>
						</div>
						<!-- End Inner Column -->
						<div class="col-md-7">
							<div class="detail-area">

								<ul class="address">
									<!-- <li class="link"><a href="javascript:;" target="_blank">www.fortishospital.com</a></li> -->
									<li class="location">Vasant Kunj, Delhi</li>
								</ul>

								<p>Fortis Flt. Lt. Rajan Dhall Hospital is a 200 bed NABH
									certified...</p>

								<h5>Speciality :</h5>
								<ul class="tags">
									<li><a href="cardiology-treatment">Cardiology</a></li>
									<li><a href="organ-transplant">Organ Transplant</a></li>
									<li><a href="orthopaedics-surgery">Orthopaedics</a></li>
								</ul>
							</div>
						</div>
						<!-- End Inner Column -->
					</div>
					<!-- End Inner Row -->
				</div>
				<!-- End List Box -->
			</div>
			<!-- End Column -->

			<div class="col-md-6 col-sm-6" onclick="location.href='jaypeeNoida'"
				style="cursor: pointer;">
				<div class="list-box">
					<div class="row">
						<h2>
							<span>JAYPEE </span>Hospital Noida
						</h2>
						<div class="col-md-5">
							<div class="hospital-img-container">
								<img src="images/hospitals/JaypeeHospital.jpg"
									alt="JAYPEE Hospital Noida" class="img-responsive">
							</div>
						</div>
						<!-- End Inner Column -->
						<div class="col-md-7">
							<div class="detail-area">

								<ul class="address">
									<!-- <li class="link"><a href="javascript:;" target="_blank">www.fortishospital.com</a></li> -->
									<li class="location">Sector-128, Wish Town, Gautam Buddh
										Nagar, Noida, U.P.</li>
								</ul>

								<p>The Jaypee Hospital was conceptualized with the vision of
									promoting world-class healthcare ...</p>

								<!-- <h5>Speciality :</h5>
										<ul class="tags">
											<li><a href="javascript:;">cardiology</a></li>
											<li><a href="javascript:;">cardiology</a></li>
											<li><a href="javascript:;">cardiology</a></li>
										</ul> -->
							</div>
						</div>
						<!-- End Inner Column -->
					</div>
					<!-- End Inner Row -->
				</div>
				<!-- End List Box -->
			</div>
			<!-- End Column -->

			<div class="col-md-6 col-sm-6" onclick="location.href='kokilaben'"
				style="cursor: pointer;">
				<div class="list-box">
					<div class="row">
						<h2>
							<span>Kokilaben </span>Dhirubhai Ambani Hospital
						</h2>
						<div class="col-md-5">
							<div class="hospital-img-container">
								<img src="images/hospitals/KokilaBen.jpg"
									alt="Kokilaben Dhirubhai Ambani Hospital"
									class="img-responsive">
							</div>
						</div>
						<!-- End Inner Column -->
						<div class="col-md-7">
							<div class="detail-area">

								<ul class="address">
									<!-- <li class="link"><a href="javascript:;" target="_blank">www.fortishospital.com</a></li> -->
									<li class="location">Rao Saheb Achutrao, Patwardhan Marg,
										Four Bunglows, Mumbai</li>
								</ul>

								<p>A vision to strengthen healthcare in the communities we
									serve and empower patients to make informed choices...</p>

								<!-- <h5>Speciality :</h5>
										<ul class="tags">
											<li><a href="javascript:;">cardiology</a></li>
											<li><a href="javascript:;">cardiology</a></li>
											<li><a href="javascript:;">cardiology</a></li>
										</ul> -->
							</div>
						</div>
						<!-- End Inner Column -->
					</div>
					<!-- End Inner Row -->
				</div>
				<!-- End List Box -->
			</div>
			<!-- End Column -->

			<div class="col-md-6 col-sm-6"
				onclick="location.href='globalHyderabad'" style="cursor: pointer;">
				<div class="list-box">
					<div class="row">
						<h2>
							<span>Global</span> Hospital
						</h2>
						<div class="col-md-5">
							<div class="hospital-img-container">
								<img src="images/hospitals/GlobalHyderabad.jpg"
									alt="Global Hospital" class="img-responsive">
							</div>
						</div>
						<!-- End Inner Column -->
						<div class="col-md-7">
							<div class="detail-area">

								<ul class="address">
									<!-- 	<li class="link"><a href="javascript:;" target="_blank">www.fortishospital.com</a></li> -->
									<li class="location">Hyderabad</li>
								</ul>

								<p>Global Hospitals Group, India’s most renowned healthcare
				services provider offering better care, cutting-edge research and
				advanced education to caregivers...</p>


								<!-- <h5>Speciality :</h5>
										<ul class="tags">
											<li><a href="javascript:;">cardiology</a></li>
											<li><a href="javascript:;">cardiology</a></li>
											<li><a href="javascript:;">cardiology</a></li>
										</ul> -->
							</div>
						</div>
						<!-- End Inner Column -->
					</div>
					<!-- End Inner Row -->
				</div>
				<!-- End List Box -->
			</div>
			<!-- End Column -->

			<div class="col-md-6 col-sm-6"
				onclick="location.href='fortisBangalore'" style="cursor: pointer;">
				<div class="list-box">
					<div class="row">
						<h2>
							<span>Fortis</span> Hospital
						</h2>
						<div class="col-md-5">
							<div class="hospital-img-container">
								<img src="images/hospitals/FortisHospital.jpg"
									alt="Fortis Hospital" class="img-responsive">
							</div>
						</div>
						<!-- End Inner Column -->
						<div class="col-md-7">
							<div class="detail-area">

								<ul class="address">
									<!-- 	<li class="link"><a href="javascript:;" target="_blank">www.fortishospital.com</a></li> -->
									<li class="location">Bengaluru</li>
								</ul>

								<p>The Fortis Hospital at Bannerghatta Road is a 400 bedded
									multi-speciality tertiary care hospital...</p>


								<!-- <h5>Speciality :</h5>
										<ul class="tags">
											<li><a href="javascript:;">cardiology</a></li>
											<li><a href="javascript:;">cardiology</a></li>
											<li><a href="javascript:;">cardiology</a></li>
										</ul> -->
							</div>
						</div>
						<!-- End Inner Column -->
					</div>
					<!-- End Inner Row -->
				</div>
				<!-- End List Box -->
			</div>
			<!-- End Column -->

			<div class="col-md-6 col-sm-6"
				onclick="location.href='metroHospNoida'" style="cursor: pointer;">
				<div class="list-box">
					<div class="row">
						<h2>
							<span>METRO</span> Hospital
						</h2>
						<div class="col-md-5">
							<div class="hospital-img-container">
								<img src="images/hospitals/MetroHospital.jpg"
									alt="METRO Hospital" class="img-responsive">
							</div>
						</div>
						<!-- End Inner Column -->
						<div class="col-md-7">
							<div class="detail-area">

								<ul class="address">
									<!-- <li class="link"><a href="javascript:;" target="_blank">www.fortishospital.com</a></li> -->
									<li class="location">Noida, NCR</li>
								</ul>

								<p>With a vision to provide the utmost level of healthcare
									to the common man, at the most affordable cost...</p>

								<h5>Speciality :</h5>
								<ul class="tags">
									<li><a href="cardiology-treatment">Cardiology</a></li>
									<li><a href="orthopaedics-surgery">Orthopaedics</a></li>
								</ul>
							</div>
						</div>
						<!-- End Inner Column -->
					</div>
					<!-- End Inner Row -->
				</div>
				<!-- End List Box -->
			</div>
			<!-- End Column -->
			
			<div class="col-md-6 col-sm-6"
				onclick="location.href='novaFertility'" style="cursor: pointer;">
				<div class="list-box">
					<div class="row">
						<h2>
							<span>NOVA IVI</span> Fertility Hospitals
						</h2>
						<div class="col-md-5">
							<div class="hospital-img-container">
								<img src="images/hospitals/NovaIVF.jpg"
									alt="NOVA IVI Hospitals" class="img-responsive">
							</div>
						</div>
						<!-- End Inner Column -->
						<div class="col-md-7">
							<div class="detail-area">

								<ul class="address">
									<!-- <li class="link"><a href="javascript:;" target="_blank">www.fortishospital.com</a></li> -->
									<li class="location">India</li>
								</ul>

								<p>Nova IVI Fertility is India's leading chain of fertility
				centres and has a vision to be the best-in-class in the field of
				infertility treatments...</p>

								<!-- <h5>Speciality :</h5>
								<ul class="tags">
									<li><a href="cardiology-treatment">Cardiology</a></li>
									<li><a href="orthopaedics-surgery">Orthopaedics</a></li>
								</ul> -->
							</div>
						</div>
						<!-- End Inner Column -->
					</div>
					<!-- End Inner Row -->
				</div>
				<!-- End List Box -->
			</div>
			<!-- End Column -->

		</div>
		<!-- End Row -->
	</div>
	<!-- End Container -->
</div>
<!-- Listing Section Ends Here -->

<%@ include file="newFooter.jsp"%>
<%@ include file="newHeader.jsp"%></section>
	<!-- Banner Starts Here -->
	<div class="inner-banner">
		<img src="images/banners/metro-heart-institute.jpg"
			alt="METRO Hospital" class="img-responsive">
	</div>
	<!-- Banner Ends Here -->

	<!-- Hospital Short Details Starts Here -->
	<div class="short-details">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12">
					<div class="title">
						<h1>METRO Hospital</h1>
						<p>Noida, India</p>
					</div>
				</div>
				<!-- End Column -->
				<!-- <div class="col-md-6 col-sm-6">
                            <div class="site-link">
                                Website : <a href="javascript:;">http://www.aimshospital.org</a>
                            </div>
                        </div>End Column -->
			</div>
			<!-- End Row -->
		</div>
		<!-- End Container -->
	</div>
	<!-- Hospital Short Details Ends Here -->

	<!-- Hospital Details Starts Here -->
	<div class="container">
		<div class="detail-container">
			<div class="hospital-details">
				<div class="row">
					<div class="col-md-9">
						<div class="row">
							<div class="col-md-6 col-divider">
								<div class="address-box box-height">
									<ul>
										<!-- <li><strong>Address</strong><span>: Ponekkara, P. O Kochi, Kochi, Kerala</span></li> -->
										<li><strong>City</strong><span>: Noida</span></li>
									</ul>
								</div>
							</div>
							<!-- End Column -->
							<div class="col-md-6 col-divider">
								<div class="number-box box-height">
									<ul>
										<li><strong>No. of Hospital Beds</strong><span>: </span></li>
										<li><strong>No. of ICU Beds</strong><span>: </span></li>
										<li><strong>No. of Operating Rooms</strong><span>:
										</span></li>
										<li><strong>Payment Mode</strong><span>: </span></li>
										<li><strong>Avg. International Patients</strong><span>:
										</span></li>
									</ul>
								</div>
							</div>
							<!-- End Column -->
						</div>
						<!-- End Row -->
					</div>
					<!-- End Column -->
					<!-- <div class="col-md-3">
                                <div class="certificate-box box-height">
                                    <h4>Accreditations</h4>
                                    <ul>
                                        <li><img src="images/certificate/icon01.png" alt="Accreditations" class="img-responsive"></li>
                                    </ul>
                                </div>
                            </div>End Column -->
				</div>
				<!-- End Row -->
			</div>
			<!-- End Hospital Details -->
		</div>
		<!-- End Detail Container -->
	</div>
	<!-- Hospital Details Ends Here -->

	<!-- About Section Starts Here -->
	<div class="about-section">
		<div class="container">
			<div class="title">
				<h2>
					<span>About</span> Metro Hospital
				</h2>
			</div>
			<div class="description">
				<p>With a vision to provide the utmost level of healthcare to
					the common man, at the most affordable cost, a group of NRI
					physicians led by Dr. Purshotam Lal (Padma Vibhushan, Padma Bhushan
					and Dr. B.C. Roy National awardee), a pioneer of Interventional
					Cardiology in India, founded Metro Hospitals &amp; Heart Institute. The
					first hospital was set up in Noida in June 1997 by the name of
					Metro Hospitals &amp; Heart Institute.</p>
				<p>Immediately after foraying into the heart care segment,
					Multispeciality wing was started in September 1998, followed by
					Metro Centre for Liver &amp; Digestive Diseases, Metro Center for
					Respiratory Diseases, Metro Heart Institutes at Meerut, Faridabad,
					Lajpat Nagar, Patel Nagar, Preet Vihar at Delhi and Metro Hospital
					&amp; Research Centre at Vadodara.</p>
				<p>Metro hospital and heart institute is a pioneer in the
					technological revolution in health care, rendering services to
					thousands of patients from across the globe to get them cured, in
					the presence of world class facilities.</p>
				<p>Since 1997, we have helped to enhance the lives of thousands
					of people who have chosen us for quality healthcare services.</p>
				<p>We have developed the concept of �Metro Coronary Screening�
					whereby an angiography can be done with the use of minimal dye from
					the arc. The procedure reduces the cost of an angiography. This
					procedure takes only 5 minutes and the individual can go back to
					home / work in less than an hours� time.</p>
				<p>Our professional team provides exceptional service and
					employs the highest standards of patient care. Many hospitals
					across the country refer their most difficult cases to Metro
					Hospital. This is a true testimony to the skill and knowledge of
					Dr. P Lal and his professional team.</p>
				<h5>
					To get best options for treatment at Metro Hospital, send us a
					query or write to us at <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
				</h5>

			</div>
		</div>
		<!-- End Container -->
	</div>
	<!-- About Section Ends Here -->

	<%@ include file="newFooter.jsp"%>
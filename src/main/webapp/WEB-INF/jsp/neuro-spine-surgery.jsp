<%@ include file="newHeader.jsp"%></section>
<!-- Hospital Short Details Starts Here -->
<div class="short-details">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="title">
					<h1>Neuro and Spine</h1>
					<ul class="paginator">
						<li><a href="/">Home</a></li>
						<li><a>Neuro and Spine</a></li>
					</ul>
				</div>
			</div>
			<!-- End Column -->
		</div>
		<!-- End Row -->
	</div>
	<!-- End Container -->
</div>
<!-- Hospital Short Details Ends Here -->

<!-- Specialty Content Starts Here -->
<div class="speciality-content-area">
	<div class="container">

		<div class="innertab-accordion">
			<div class="row">
				<div class="col-md-3 tabnopaddright">
					<div class="tabs">
						<ul>
							<li class="tab-active"><a href="neuro-spine-surgery">Neuro
									and Spine</a></li>
							<li><a href="neuro-surgery">Neurosurgery</a></li>
							<li><a href="neurology">Neurology</a></li>
							<li><a href="brain-tumors-treatment">Brain Tumor</a></li>
							<li><a href="spinal-fusion-surgery">Spinal Fusion</a></li>
							<li><a href="intracranial-aneurysm-surgery">Aneurysms</a></li>
							<li><a href="spinal-tumor-surgery">Spinal Tumor</a></li>
							<li><a href="spinal-laminectomy-surgery">Laminectomy</a></li>
						</ul>
					</div>
				</div>
				<!-- End Column -->
				<div class="col-md-9 tabnopaddleft">
					<div class="accordion-contentarea">

						<div id="tab1" class="visible">
							<h2>
								<span>Neuro and </span>Spine Surgery
							</h2>
							<div class="row">
								<div class="col-md-9 col-sm-9 nopaddleft">
									<p>We at Arvene Healthcare have the expertise to organize
										for all treatments and surgeries related to Neuro and Spine.
										Arvene Healthcare is associated with top hospitals and Doctors
										of India in this field. We can give you more and better
										options so that you can take the right decisions. We also
										assist you in taking best decision based on your requirement.
										Arvene Healthcare will also arrange for your stay and local
										travel, so that you have nothing else to worry about but your
										treatment. Please contact us on <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
										today to avail best services.</p>
								</div>
								<!-- End Inner Column -->
								<div class="col-md-3 col-sm-3 nopaddright">
									<img src="images/specialities/23.neuro main.jpg" alt="neuro"
										class="img-responsive">
								</div>
								<!-- End Inner Column -->
							</div>
							<!-- End Inner Row -->

							<h3>
								<span>Neuro Spine </span>Surgery:
							</h3>
							<p>Neurosurgery is primarily concerned with diagnosis,
								prevention and treatment of disorders that affect any part of
								the nervous system. The nervous system includes of brain,
								peripheral nerves, spinal cord and extra-cranial cerebrovascular
								system.</p>
							<h3>
								<span>Types of </span>Neuro and Spine Surgeries
							</h3>
							<ul>
								<li><span>Brain and Spinal Cord Tumors</span></li>
								<li><span>Complex Spine Surgery</span></li>
								<li><span>Minimally Invasive Neck and Back Surgery</span></li>
								<li><span>Gamma Knife</span></li>
								<li><span>Artificial Disc</span></li>
								<li><span>Skull-based and Pituitary Tumors</span></li>
								<li><span>Stereotactic Radiosurgery</span></li>
								<li><span>Pain Management</span></li>
								<li><span>Deep Brain Stimulation (DBS)</span></li>
								<li><span>Spinal Decompression Rehabilitation
										Therapy (IDD)</span></li>
								<li><span>Trigeminal Neuralgia</span></li>
								<li><span>Cerebral Vascular Brain Disorders</span></li>
							</ul>
							<h3>
								<span>Conditions Treated by </span>Neuro Spine Surgery
							</h3>
							<p>Many spinal conditions can be treated that include
								injuries, degeneration and diseases:</p>
							<ul>
								<li><span>Fractures</span></li>
								<li><span>Brain and Spine Tumors</span></li>
								<li><span>Degenerative disc disease</span></li>
								<li><span>Lumbar spinal stenosis</span></li>
								<li><span>Cervical spinal disc disease</span></li>
								<li><span>Scoliosis</span></li>
								<li><span>Neck pain</span></li>
								<li><span>Herniated pain</span></li>
								<li><span>Infection</span></li>
								<li><span>Lumbar spinal stenosis</span></li>
								<li><span>Lower back pain</span></li>
								<li><span>Spinal Cord Injury</span></li>
							</ul>
							<h3>
								<span>Laser </span>Spine Surgery
							</h3>
							<p>Laser spine surgery is a minimally invasive endoscopic
								procedure that does not require any cutting of the muscles and
								bones. The recovery is also quick in this procedure unlike
								conventional spine surgery. An endoscope is used for directing a
								laser beam at the affected area. The laser is passed through the
								small incision made which is in the back or neck. This is known
								as the best proven procedure for spine related problems.</p>
							<h3>
								<span>Cervical </span>Spine Surgery
							</h3>
							<p>Cervical spine surgery is performed for eliminating
								tingling, weakness, pain and numbness caused in the spine. The
								surgery also prevents the abnormal motion in the spine and also
								restores nerve function. The surgery is performed for a number
								of cervical spine problems.</p>
							<h3>
								<span>Total </span>Disc Replacement
							</h3>
							<p>Total disc replacement surgery, also referred to as
								artificial disc, is a new innovation in spine surgery. The
								degeneration of the spinal structures result in neck or back
								pain thereby decreasing the quality of life. The surgical
								procedure is done for removing pain and also preserves the
								natural motion.</p>
							<h3>
								<span>Lumbar Spinal Stenosis</span> Surgery
							</h3>
							<p>The enlargement of the spinal canal is done through this
								surgery that eases pressure on the nerve. This surgery is
								performed for relieving the pressure on the nerve roots or the
								spinal cord. Weakness in the legs, numbness and pain are reduced
								by performing this surgery. Decompressive laminectomy is the
								major type of lumbar spinal stenosis surgery.</p>
							<h3>
								<span>Carpel Tunnel</span> Syndrome
							</h3>
							<p>Carpel tunnel is the little space in the wrist from where
								many tendons and median nerve runs from the forearm of the hand.
								Carpel tunnel syndrome results in tingling, pain and different
								problems of the hand due to pressure caused on the median nerve
								in the wrist. The function of the median nerve is to control the
								feeling and movement of thumb and first three fingers except
								little finger.</p>
							<h3>
								<span>Gamma </span>Knife
							</h3>
							<p>Gamma Knife therapy is considered as the best proven and
								precise treatment for brain disorders. This therapy does not
								involve any kind of surgery and the therapy involves 201 precise
								beams of radiation directly on the tumor. The beams are more
								powerful when they work together on a single point. This therapy
								destroys the cancer cells and also stops them from multiplying.
								The tumor disappears after some time.</p>
							<h3>
								<span>Cost of Neuro and </span>Spine Surgery
							</h3>
							<p>India offers low cost neuro and spine surgery which is a
								prominent factor that attracts a large number of patients from
								across the world. All the advanced surgeries, technologies and
								medical treatments are available in India.</p>
							<h5>
								To get best options for Neuro and Spine treatment, send us a
								query or write to us at <a
									href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
							</h5>
						</div>
						<!-- End Tab -->

					</div>
				</div>
				<!-- End Column -->
			</div>
			<!-- End Row -->
		</div>
		<!-- End Tab Accordion -->

	</div>
	<!-- End Container -->
</div>
<!-- Speciality Content Ends Here -->

<%@ include file="newFooter.jsp"%>
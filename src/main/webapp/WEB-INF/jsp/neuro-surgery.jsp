<%@ include file="newHeader.jsp"%></section>
<!-- Hospital Short Details Starts Here -->
<div class="short-details">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="title">
					<h1>Neuro and Spine</h1>
					<ul class="paginator">
						<li><a href="/">Home</a></li>
						<li><a>Neuro and Spine</a></li>
					</ul>
				</div>
			</div>
			<!-- End Column -->
		</div>
		<!-- End Row -->
	</div>
	<!-- End Container -->
</div>
<!-- Hospital Short Details Ends Here -->

<!-- Specialty Content Starts Here -->
<div class="speciality-content-area">
	<div class="container">

		<div class="innertab-accordion">
			<div class="row">
				<div class="col-md-3 tabnopaddright">
					<div class="tabs">
						<ul>
							<li><a href="neuro-spine-surgery">Neuro
									and Spine</a></li>
							<li class="tab-active"><a href="neuro-surgery">Neurosurgery</a></li>
							<li><a href="neurology">Neurology</a></li>
							<li><a href="brain-tumors-treatment">Brain Tumor</a></li>
							<li><a href="spinal-fusion-surgery">Spinal Fusion</a></li>
							<li><a href="intracranial-aneurysm-surgery">Aneurysms</a></li>
							<li><a href="spinal-tumor-surgery">Spinal Tumor</a></li>
							<li><a href="spinal-laminectomy-surgery">Laminectomy</a></li>
						</ul>
					</div>
				</div>
				<!-- End Column -->
				<div class="col-md-9 tabnopaddleft">
					<div class="accordion-contentarea">

						<div id="tab2">
							<h2>
								<span>Neuro Surgery</span> ? India
							</h2>
							<div class="row">
								<div class="col-md-9 col-sm-9 nopaddleft">
									<p>We at Arvene Healthcare have the expertise to organize
										for all treatments and surgeries related to Neuro and Spine.
										Arvene Healthcare is associated with top hospitals and Doctors
										of India in this field. We can give you more and better
										options so that you can take the right decisions. We also
										assist you in taking best decision based on your requirement.
										Arvene Healthcare will also arrange for your stay and local
										travel, so that you have nothing else to worry about but your
										treatment. Please contact us on <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
										today to avail best services.</p>
								</div>
								<!-- End Inner Column -->
								<div class="col-md-3 col-sm-3 nopaddright">
									<img src="images/specialities/24.neuro surgery.jpg"
										alt="neuro surgery" class="img-responsive">
								</div>
								<!-- End Inner Column -->
							</div>
							<!-- End Inner Row -->
							<h3>Neurosurgery:</h3>
							<p>Neurosurgery is concerned with evaluation, prevention,
								diagnosis and treatment of disorders of the autonomic,
								peripheral and central nervous systems and it also include their
								vascular supply and supporting structures. This medical
								specialty is also concerned with treatment and evaluation of
								pathological processes which changes activity or function of the
								nervous system such as non-operative, operative and hypophysis
								management of pain.</p>
							<br>
							<p>The neurological surgery treats the disorders of the
								nervous system of the paediatric and adult patients -</p>
							<ul>
								<li><span>Spinal and cranial nerves disorders.</span></li>
								<li><span>Skull, brain and meninges disorders
										including their blood supply.</span></li>
							</ul>
							<h3>
								<span>Neurosurgery </span>Procedures
							</h3>
							<ul>
								<li><span>Cervical Spine Surgery</span></li>
								<li><span>Coiling of Aneurysms</span></li>
								<li><span>Peripheral Nerve Surgery</span></li>
								<li><span>Minimally Invasive Spine Surgery</span></li>
								<li><span>Functional Neurosurgery</span></li>
								<li><span>Minimally invasive Endovascular Treatment
										of Cerebrovascular Pathology</span></li>
								<li><span>Microvascular Decompression</span></li>
								<li><span>Embolization of Tumors</span></li>
								<li><span>Pituitary Surgery</span></li>
								<li><span>Brain Tumor Excision</span></li>
								<li><span>Skull Base Surgery</span></li>
								<li><span>Embolization of Cerebral and Dural based
										AVMs</span></li>
								<li><span>Trauma Surgery</span></li>
								<li><span>Parkinson's Disease and Tremors</span></li>
								<li><span>Radiosurgery</span></li>
								<li><span>Epilepsy</span></li>
							</ul>
							<h3>
								<span>Paediatric</span> Neurosurgery
							</h3>
							<p>Paediatric Neurosurgery is the medical specialty concerned
								with the diagnosis and treatment of children having neurological
								disorders such as spasticity, intractable epilepsy,
								craniosynostosis, and movement of disorders, hydrocephalus,
								spinal deformities, spinal bifida, brain tumors, head injuries
								and cranial malformations</p>
							<h3>
								<span>Endovascular </span>Surgery
							</h3>
							<p>Endovascular Surgery is a minimally invasive and
								innovative procedure to treat different problems that affect
								blood vessels namely aneurysm. An aneurysm is the condition
								where there is ballooning or swelling of the blood vessel.
								Endovascular surgery in an alternative to open surgery that
								offers numerous benefits such as less discomfort, quick
								recovery, less stress on the heart, smaller incisions, regional
								or local anesthesia and less risks for patients having other
								medical conditions.</p>
							<h3>
								<span>Arteriovenous Malformation </span>(AVM)
							</h3>
							<p>The circulatory system defects are termed as Arteriovenous
								malformations (AVMs). AVMs are made up of snarled tangles of
								veins and arteries. The oxygen rich blood is carried by arteries
								from the heart to the cells of the body and oxygen less blood is
								carried by veins to the heart and lungs. AVMs obstruct the vital
								cyclical process of arteries and veins.</p>
							<h3>
								<span>Parkinson's </span>disease
							</h3>
							<p>Parkinson's disease is the nervous system disorder
								affecting the movement of a person. This disease develops
								slowly. Tremors are regarded as a significant symptom of this
								disease and can result in slowing or stiffness of movement. Its
								symptoms include -</p>
							<ul>
								<li><span>Speech changes</span></li>
								<li><span>Tremor</span></li>
								<li><span>Impaired balance and posture</span></li>
								<li><span>Slowed movement</span></li>
								<li><span>Rigid muscles</span></li>
							</ul>
							<h3>
								<span>Deep Brain </span>Stimulation
							</h3>
							<p>Deep brain stimulation is the neurological treatment where
								mild electrical signals are stimulated in different parts of the
								brain. The electrical impulses of the brain are reorganized by
								the signals that results in improved symptoms in different
								situations affecting the brain.</p>
							<h3>Stroke</h3>
							<p>A stroke is a medical emergency in which there is an
								interruption of the blood supply inparts of the brain. That part
								of brain is deprived of food and oxygen and hence the cells
								starts to die. An immediate treatment is required in this
								condition. Factors that can cause stroke are high cholesterol,
								high blood pressure and smoking. Symptoms of stroke include
								weakness, problem in talking -slurred speech, face-drooling etc.
							</p>
							<h3>
								<span>Cost of </span>Neurosurgery
							</h3>
							<p>The Neurosurgery in India has improved by leaps and
								bounds. Best doctors are now available who are highly
								experienced and trained. Hospitals use latest equipment and
								perform high-end and complex neurosurgeries. The neurosurgery in
								India is available at a very low cost.</p>
							<h5>
								To get best options for Neuro and Spine treatment, send us a
								query or write to us at <a
									href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
							</h5>
						</div>
						<!-- End Tab -->
						

					</div>
				</div>
				<!-- End Column -->
			</div>
			<!-- End Row -->
		</div>
		<!-- End Tab Accordion -->

	</div>
	<!-- End Container -->
</div>
<!-- Speciality Content Ends Here -->

<%@ include file="newFooter.jsp"%>
<%@ include file="newHeader.jsp"%></section>
<!-- Hospital Short Details Starts Here -->
<div class="short-details">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="title">
					<h1>Neuro and Spine</h1>
					<ul class="paginator">
						<li><a href="/">Home</a></li>
						<li><a>Neuro and Spine</a></li>
					</ul>
				</div>
			</div>
			<!-- End Column -->
		</div>
		<!-- End Row -->
	</div>
	<!-- End Container -->
</div>
<!-- Hospital Short Details Ends Here -->

<!-- Specialty Content Starts Here -->
<div class="speciality-content-area">
	<div class="container">

		<div class="innertab-accordion">
			<div class="row">
				<div class="col-md-3 tabnopaddright">
					<div class="tabs">
						<ul>
							<li><a href="neuro-spine-surgery">Neuro
									and Spine</a></li>
							<li><a href="neuro-surgery">Neurosurgery</a></li>
							<li class="tab-active"><a href="neurology">Neurology</a></li>
							<li><a href="brain-tumors-treatment">Brain Tumor</a></li>
							<li><a href="spinal-fusion-surgery">Spinal Fusion</a></li>
							<li><a href="intracranial-aneurysm-surgery">Aneurysms</a></li>
							<li><a href="spinal-tumor-surgery">Spinal Tumor</a></li>
							<li><a href="spinal-laminectomy-surgery">Laminectomy</a></li>
						</ul>
					</div>
				</div>
				<!-- End Column -->
				<div class="col-md-9 tabnopaddleft">
					<div class="accordion-contentarea">

						
						<div id="tab3">
							<h2>
								<span>Neurology</span> - India
							</h2>
							<div class="row">
								<div class="col-md-9 col-sm-9 nopaddleft">
									<p>We at Arvene Healthcare have the expertise to organize
										for all treatments and surgeries related to Neuro and Spine.
										Arvene Healthcare is associated with top hospitals and Doctors
										of India in this field. We can give you more and better
										options so that you can take the right decisions. We also
										assist you in taking best decision based on your requirement.
										Arvene Healthcare will also arrange for your stay and local
										travel, so that you have nothing else to worry about but your
										treatment. Please contact us on <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
										today to avail best services.</p>
									<h3>Neurology</h3>
									<p>Neurology deals with disorders of the nervous system and
										brain. The neurological disorders include diseases of
										autonomic, central and peripheral nervous systems.
										Neurological diseases include diseases of peripheral muscles
										and nerves, neuromuscular junctions and spinal cord. The
										disorders of spinal cord, nerves and the brain are treated by
										a neurologist that include -</p>
									<ul>
										<li><span>Epilepsy</span></li>
										<li><span>Tremor</span></li>
										<li><span>Stroke</span></li>
										<li><span>Parkinson's disease</span></li>
										<li><span>Amyotrophic lateral sclerosis</span></li>
										<li><span>Heartache</span></li>
										<li><span>Pain</span></li>
										<li><span>Alzheimer's disease</span></li>
										<li><span>Sleep disorders</span></li>
										<li><span>Spinal cord and brain injuries</span></li>
										<li><span>Multiple sclerosis</span></li>
										<li><span>Peripheral nerve disorders</span></li>
									</ul>
								</div>
								<!-- End Inner Column -->
								<div class="col-md-3 col-sm-3 nopaddright">
									<img src="images/specialities/25.neurology.jpg" alt="neurology"
										class="img-responsive">
								</div>
								<!-- End Inner Column -->
							</div>
							<!-- End Inner Row -->
							<h3>
								<span>Paediatric</span> Neurology
							</h3>
							<p>Paediatric Neurology is specifically deals with
								neurological problems in children. Children may suffer from
								neurological diseases and epilepsy. Their primary role is to
								confront the symptoms of epilepsy in children.</p>
							<h3>
								<span>Neurological Disorders</span> in Children
							</h3>
							<ul>
								<li><span>Autistic disorders</span></li>
								<li><span>Congenital anomalies of the brain and
										Hydrocephalus</span></li>
								<li><span>Hyperactivity Disorder (ADHD) and
										Attention Deficit (ADD)</span></li>
								<li><span>Headaches</span></li>
								<li><span>Brain and spinal cord tumors</span></li>
								<li><span>Seizure disorders</span></li>
								<li><span>Concussion and head trauma</span></li>
								<li><span>Balance and equilibrium</span></li>
								<li><span>Metabolic-degenerative diseases of the
										brain</span></li>
								<li><span>Neuromuscular disorders</span></li>
								<li><span>Cerebral palsy</span></li>
								<li><span>Neurofibromatosis</span></li>
								<li><span>Tourette syndrome, tic disorders and
										movement disorders</span></li>
								<li><span>Electromyography</span></li>
								<li><span>Electronystagmography (ENG)</span></li>
								<li><span>Electroencephalography (EEG)</span></li>
								<li><span>Magnetic resonance imaging (MRI)</span></li>
								<li><span>Polysomnogram</span></li>
								<li><span>Ultrasound imaging</span></li>
								<li><span>Thermography</span></li>
								<li><span>Single photon emission computed tomography
										(SPECT)</span></li>
							</ul>
							<h3>
								<span>Cost of </span>Neurology
							</h3>
							<p>The cost of neurology and paediatric neurology treatment
								in India is substantially low as compared to other developed
								countries.</p>
							<h5>
								To get best options for Neuro and Spine treatment, send us a
								query or write to us at <a
									href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
							</h5>
						</div>
						<!-- End Tab -->
					</div>
				</div>
				<!-- End Column -->
			</div>
			<!-- End Row -->
		</div>
		<!-- End Tab Accordion -->

	</div>
	<!-- End Container -->
</div>
<!-- Speciality Content Ends Here -->

<%@ include file="newFooter.jsp"%>
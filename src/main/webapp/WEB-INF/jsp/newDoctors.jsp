<%@ include file="newHeader.jsp"%></section>


<div class="container">
	<div class="mainContent">
		<h3 class="pageHeader" style="margin: auto;">Doctors</h3>

		<table class="table table-striped rwd-table">
			<thead>
				<tr>
					<th style="width: 4%;">S.NO</th>
					<th style="width: 25%;">Doctor Name</th>
					<th style="width: 18%;">Email</th>
					<th style="width: 18%;">Phone</th>
					<th style="width: 10%;">Speciality</th>
					<th style="width: 16%">Clinic</th>
					<th style="width: 9%">Action</th>

				</tr>
			</thead>
			<tbody>
				<c:forEach items="${doctorsList}" var="doctor"
					varStatus="doctorsIndex">
					<tr>
						<td>${start + doctorsIndex.index +1 }</td>
						<td>${doctor.name}</td>
						<td>${doctor.email}</td>
						<td>${doctor.phone}</td>
						<td>${doctor.speciality}</td>
						<td>${doctor.clinic}</td>
						<td><input id="toggleInquiryDetails" type="button"
							onclick="location.href='archiveDocInquiry?id=${doctor.id}'"
							class="btn-blue square-Box button-custom " value="Archive" /></td>
					</tr>
				</c:forEach>

			</tbody>
		</table>
	</div>
	<div id="pagination"></div>

</div>


<script type='text/javascript'>
	var options = {

		currentPage : '${currentpage}',
		totalPages : '${pageno}',
		onPageClicked : function(e, originalEvent, type, page) {
			fetchqueries(page)
		},
		size : 'medium',
		alignment : 'center'
	};
	$('#pagination').bootstrapPaginator(options);

	function fetchqueries(page) {
		var start = (page * 10) - 10;
		window.location.href = "newDocs?start=" + start + "&range=10";
	}
	$('.nav >li.active').removeClass("active");
	$('.nav #queries').addClass("active");
</script>

<%@ include file="newFooter.jsp"%>

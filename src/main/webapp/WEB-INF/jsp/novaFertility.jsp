<%@ include file="newHeader.jsp"%></section>
<!-- Banner Starts Here -->
<div class="inner-banner">
	<img src="images/banners/nova-ivf.jpg"
		alt="METRO Hospital" class="img-responsive">
</div>
<!-- Banner Ends Here -->

<!-- Hospital Short Details Starts Here -->
<div class="short-details">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<div class="title">
					<h1>NOVA IVI Fertility Hospitals</h1>
					<p>India</p>
				</div>
			</div>
			<!-- End Column -->
			<!-- <div class="col-md-6 col-sm-6">
                            <div class="site-link">
                                Website : <a href="javascript:;">http://www.aimshospital.org</a>
                            </div>
                        </div>End Column -->
		</div>
		<!-- End Row -->
	</div>
	<!-- End Container -->
</div>
<!-- Hospital Short Details Ends Here -->

<!-- Hospital Details Starts Here -->
<div class="container">
	<div class="detail-container">
		<div class="hospital-details">
			<div class="row">
				<div class="col-md-9">
					<div class="row">
						<div class="col-md-6 col-divider">
							<div class="address-box box-height">
								<ul>
									<!-- <li><strong>Address</strong><span>: Ponekkara, P. O Kochi, Kochi, Kerala</span></li> -->
									<!-- <li><strong>City</strong><span>: </span></li> -->
								</ul>
							</div>
						</div>
						<!-- End Column -->
						<div class="col-md-6 col-divider">
							<div class="number-box box-height">
								<ul>
									<li><strong>No. of Hospital Beds</strong><span>: </span></li>
									<li><strong>No. of ICU Beds</strong><span>: </span></li>
									<li><strong>No. of Operating Rooms</strong><span>:
									</span></li>
									<li><strong>Payment Mode</strong><span>: </span></li>
									<li><strong>Avg. International Patients</strong><span>:
									</span></li>
								</ul>
							</div>
						</div>
						<!-- End Column -->
					</div>
					<!-- End Row -->
				</div>
				<!-- End Column -->
				<!-- <div class="col-md-3">
                                <div class="certificate-box box-height">
                                    <h4>Accreditations</h4>
                                    <ul>
                                        <li><img src="images/certificate/icon01.png" alt="Accreditations" class="img-responsive"></li>
                                    </ul>
                                </div>
                            </div>End Column -->
			</div>
			<!-- End Row -->
		</div>
		<!-- End Hospital Details -->
	</div>
	<!-- End Detail Container -->
</div>
<!-- Hospital Details Ends Here -->

<!-- About Section Starts Here -->
<div class="about-section">
	<div class="container">
		<div class="title">
			<h2>
				<span>About</span> NOVA IVI Hospital
			</h2>
		</div>
		<div class="description">
			<p>Nova IVI Fertility is India's leading chain of fertility
				centres and has a vision to be the best-in-class in the field of
				infertility treatments. Our core ethos is patient satisfaction and
				safety. Our mission is to provide standardised, high-quality
				fertility treatments in a cost-effective and transparent manner. Set
				up under the supervision of - and training from - IVI, Spain, our
				labs match the best in the world in terms of facilities, equipment
				and, most critically, operating standards. For instance, all our
				embryologists do their work in laboratories that conform to European
				clean room standards.</p>
			<p>In addition to providing core procedures such as IUI, IVF,
				ICSI and Andrology services, Nova IVI Fertility offers several
				state-of-the-art technologies such as Vitrification for preserving
				embryos and eggs, Embryoscope and PGS for selection of best embryos
				for transfer and ERA to select the uterus's capacity to accept
				embryos: all procedures that significantly improve the chances of a
				pregnancy following IVF-ICSI, even in patients with multiple
				previous failures.</p>
			<p>With over a century of combined experience and awards and
				qualifications from the leading centres of ART in the world, our
				team of doctors are among the most expert in the country. Some of
				our doctors also serve on panels to ensure that fertility treatments
				in India adhere to the highest ethical and medical standards in the
				world.</p>
			<p>They are ably supported by our team of embryologists at each
				centre. Trained and monitored by IVI Spain, our embryologists are
				capable of attending to the needs of our patients meticulously and
				in a timely fashion.</p>
			<p>
			<h5>
				To get best options for treatment at NOVA IVI Hospital, send us a
				query or write to us at <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
			</h5>

		</div>
	</div>
	<!-- End Container -->
</div>
<!-- About Section Ends Here -->

<%@ include file="newFooter.jsp"%>
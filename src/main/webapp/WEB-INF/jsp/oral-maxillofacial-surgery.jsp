<%@ include file="newHeader.jsp"%></section>
<!-- Hospital Short Details Starts Here -->
<div class="short-details">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="title">
					<h1>Cosmetic Surgery</h1>
					<ul class="paginator">
						<li><a href="/">Home</a></li>
						<li><a>Cosmetic Surgery</a></li>
					</ul>
				</div>
			</div>
			<!-- End Column -->
		</div>
		<!-- End Row -->
	</div>
	<!-- End Container -->
</div>
<!-- Hospital Short Details Ends Here -->

<!-- Specialty Content Starts Here -->
<div class="speciality-content-area">
	<div class="container">

		<div class="innertab-accordion">
			<div class="row">
				<div class="col-md-3 tabnopaddright">
					<div class="tabs">
						<ul>
							<li><a href="cosmetic-surgery">Cosmetic Surgery</a></li>
							<li><a href="breast-augmentation-surgery">Breast
									Augmentation</a></li>
							<li><a href="breast-reduction-surgery">Breast Reduction</a></li>
							<li><a href="liposuction-surgery">Liposuction</a></li>
							<li><a href="facelift-surgery">Facelift</a></li>
							<li><a href="rhinoplasty-surgery">Rhinoplasty</a></li>
							<li><a href="cosmetic-laser-surgery">Cosmetic Laser
									Surgery</a></li>
							<li class="tab-active"><a href="oral-maxillofacial-surgery">Maxillofacial
									Surgery</a></li>
							<li><a href="surgical-hair-transplant">Surgical Hair
									Transplant</a></li>
						</ul>
					</div>
				</div>
				<!-- End Column -->
				<div class="col-md-9 tabnopaddleft">
					<div class="accordion-contentarea">

						<div id="tab8">
							<h2>
								<span>Oral &amp; Maxillofacial Surgery</span> in India
							</h2>
							<p>We at Arvene Healthcare have the expertise to organize for
								all treatments and surgeries related to Oral and Maxillofacial
								Surgery. Arvene Healthcare is associated with top hospitals and
								Doctors of India in this field. We can give you more and better
								options so that you can take the right decisions. We also assist
								you in taking best decision based on your requirement. Arvene
								Healthcare will also arrange for your stay and local travel, so
								that you have nothing else to worry about but your treatment.
								Please contact us on <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a> today to avail
								best services.</p>

							<h3>
								<span>What is </span>Oral and Maxillofacial Surgery?
							</h3>
							<div class="row">
								<div class="col-md-9 col-sm-9 nopaddleft">
									<p>This surgery corrects a large variety of defects, injury
										and diseases present in face, head, hard and soft tissues,
										jaws, oral and maxillofacial region and the neck.</p>
									<h3>
										<span>Oral Maxillofacial surgery</span> Treatments and
										Procedures
									</h3>
								</div>
								<!-- End Inner Column -->
								<div class="col-md-3 col-sm-3 nopaddright">
									<img src="images/specialities/15.maxilofacial surgery.jpg"
										alt="maxilofacial surgery" class="img-responsive">
								</div>
								<!-- End Inner Column -->
							</div>
							<!-- End Inner Row -->

							<ul>
								<li><span>It involves the treatment of injuries of
										mouth, teeth, face and jaws which has been caused due to
										trauma. Maxillofacial and oral surgeons treat the conditions
										of fractures of lower and upper jaws and orbits, cosmetic
										managements of facial lacerations and the trauma.</span></li>
								<li><span>The surgical correction is done of facial
										and oral deformities. These are the result of congenital
										deformities like palate and cleft which is caused when one or
										all portions of the oral nasal cavity does not grow together
										in fatal development and when there is a difference between
										the skeletal growth of lower and upper jaws.</span></li>
								<li><span>To address soft and hard tissue injuries
										of the lower or upper jaws, reconstructive surgery is used.
										These injuries could be the result of the trauma or injury,
										long-term denture wear or tumor surgery.</span></li>
								<li><span>The surgeons perform the cosmetic facial
										procedures specifically on an outpatient basis under IV
										sedation or local anaesthesia.</span></li>
								<li><span>The procedure of dental implant is used to
										replace one tooth, numerous teeth or the whole mouthful of
										teeth. The dental implants are more comfortable and
										long-lasting than the conventional dentures.</span></li>
							</ul>
							<h3>
								<span>Sinus</span> Lift
							</h3>
							<p>Sinus lift (sinus augmentation) is a surgery that adds
								bone in the upper jaw area of premolars and molars. The bone is
								added between the maxillary sinuses and the jaw that can be on
								either side of the nose.</p>

							<h3>
								<span>Facial </span>Trauma
							</h3>
							<p>The facial trauma injuries caused by impersonal violence,
								accident falls, hit from a heavy object or sports injuries are
								treated by the expert surgeons. The injuries could be bony
								injuries or soft tissue like fractured facial bones of the eye
								socket, nose or cheek, fractured jaws (lower and upper jaw),
								intra oral lacerations, Avulsed teeth and facial lacerations.</p>
							<h3>
								<span>Bone</span> Grafting
							</h3>
							<p>Both minor and major bone grafting surgeries are
								performed. The procedure of bone grafting can restore the
								implant sites having poor bone structure which could be a result
								of the previous gum disease, injuries or extractions. The bone
								could be either the patient's bone taken from the hip or jaw or
								could also be taken from a tissue bank.</p>
							<h3>
								<span>Dental </span>Implants
							</h3>
							<p>Dental implants replace the teeth which function like
								normal and natural teeth by preserving the facial contours. The
								facial structure can be effectively preserved and prevents the
								deterioration of bones when several teeth are missing.</p>
							<h3>
								<span>Jaw Surgery </span>(Orthognathic Surgery)
							</h3>
							<p>The surgery involves the movement of the jaws into a
								normal and perfect position. The orthodontics aims at
								straightening the teeth and the corrective jaw surgery aims at
								relocating the misaligned jaws. These processes drastically
								improve the facial appearance and makes sure that the teeth is
								working properly</p>

							<h3>
								<span>Wisdom Teeth </span>Removal
							</h3>
							<p>It becomes necessary to extract the wisdom teeth when they
								are prohibited from correctly erupting inside the mouth. Many
								problems can arise due to the poor position of impacted teeth.
								When the impacted teeth is not fully erupted, then the bacteria
								grow leading to an infection and will result in pain, illness
								and stiffness. Serious problems of tumor or cysts can also occur
								which can eventually destroy the healthy teeth and the jawbone.
							</p>
							<h3>
								<span>TMJ Disorders</span> (Temporomandibular Joint Disorder)
							</h3>
							<p>The problem starts when there is a problem with the jaw
								point, jaw and also the facial muscles which control the moving
								and chewing the jaw. Injuries to the muscles of the neck and
								head, jaw or temporomandibular caused by a heavy whiplash will
								result into TMJ disorder. TMJ disorders will lead to many
								problems which includes pain, grafting or clicking noises while
								opening your mouth, misaligned bite, repeated neck aches and
								headaches and problem in opening the mouth wide.</p>

							<h3>
								<span>Cosmetic </span>Jawline Surgery
							</h3>
							<p>Facial cosmetic surgery is considered as the choice
								solution for the correction of physical deformities resulting
								from aging, injury, disease and birth defects. The cosmetic jaw
								surgery procedures include treatment of upper jaw (Maxillary
								Osteotomy) and lower jaw (Mandibular Osteotomy). It also
								includes jaw joint surgery, wherein the oval shaped gap between
								the upper and lower front teeth is filled.</p>

							<h3>
								<span>Cost of </span>Oral and Maxillofacial Surgery
							</h3>
							<p>The cost for this surgery in India is very affordable
								having world class facilities and services. The average cost of
								the Oral and Maxillofacial surgery in India is almost 1/10th as
								compared to the treatment offered in U.S.A. or UK</p>
							<h5>
								To get best options for cosmetic treatment, send us a query or
								write to us at <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
							</h5>
						</div>
						<!-- End Tab -->

					</div>
				</div>
				<!-- End Column -->
			</div>
			<!-- End Row -->
		</div>
		<!-- End Tab Accordion -->

	</div>
	<!-- End Container -->
</div>
<!-- Speciality Content Ends Here -->

<%@ include file="newFooter.jsp"%>
<%@ include file="newHeader.jsp"%></section>
	<!-- Hospital Short Details Starts Here -->
	<div class="short-details">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="title">
						<h1>Organ Transplant</h1>
						<ul class="paginator">
							<li><a href="/">Home</a></li>
							<li><a>Organ Transplant</a></li>
						</ul>
					</div>
				</div>
				<!-- End Column -->
			</div>
			<!-- End Row -->
		</div>
		<!-- End Container -->
	</div>
	<!-- Hospital Short Details Ends Here -->

	<!-- Specialty Content Starts Here -->
	<div class="speciality-content-area">
		<div class="container">

			<div class="innertab-accordion">
				<div class="row">
					<div class="col-md-3 tabnopaddright">
						<div class="tabs">
							<ul>
								<li class="tab-active"><a href="organ-transplant">Organ
										Transplant</a></li>
								<li><a href="kidney-transplant-surgery">Kidney Transplant
										Surgery</a></li>
								<li><a href="liver-transplant-surgery">Liver Transplant
										Surgery</a></li>
								<li><a href="heart-transplant">Heart Transplant</a></li>
								<li><a href="bone-marrow-transplant">Bone Marrow
										Transplants</a></li>
								<li><a href="human-organs-transplant-laws-india">Organ Transplant
										laws in India</a></li>
							</ul>
						</div>
					</div>
					<!-- End Column -->
					<div class="col-md-9 tabnopaddleft">
						<div class="accordion-contentarea">

							<div id="tab1" class="visible">
								<h2>
									<span>Multi Organ Transplant �</span>India 
								</h2>
								<div class="row">
									<div class="col-md-9 col-sm-9 nopaddleft">
										<p>We at Arvene Healthcare have the expertise to organize for all treatments and surgeries related to Organ Transplant. Arvene Healthcare is associated with top hospitals and Doctors of India in this field. We can give you more and better options so that you can take the right decisions. We also assist you in taking best decision based on your requirement. Arvene Healthcare will also arrange for your stay and local travel, so that you have nothing else to worry about but your treatment. Please contact us on <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a> today to avail best services..</p>
									</div>
									<!-- End Inner Column -->
									<div class="col-md-3 col-sm-3 nopaddright">
										<img src="images/specialities/38.organ transplant .jpg" alt="organ transplant"
											class="img-responsive">
									</div>
									<!-- End Inner Column -->
								</div>
								<!-- End Inner Row -->
								<h3>
									<span>Multi Organ</span> Transplant
									
								</h3>
								<p>The procedure where the organ is taken from a healthy living being in order to replace the diseased organ of the patient so that it functions properly. The transplanted organs are those organs that cannot be treated with the help of any medical drugs or with any kind of surgeries. The different organs that can be transplanted are kidneys, intestine, lungs, pancreas, heart and liver. The different tissue that can be both transplanted and donated are bone marrow, skin, heart, middle ear, tendons and the valves.</p>
								<h3>
									<span>Organ Transplant</span> Process
								</h3>
								<strong>Different factors are consideredfor organ matching and allocation</strong>
								<p>The matching criterion consists of-</p>
								<ul>
									<li><span>The relative distance between the recipient and the donor</span></li>
									<li><span>The size of the organs needed and blood type</span></li>
									<li><span>The time spent awaiting the transplant</span></li>
								</ul>
								<h3>
									<span>Factors to be considered before </span>organ matching
								</h3>
								<ul>
									<li><span>It is important to know whether the
											recipient is an adult or a child </span></li>
									<li><span>The medical urgency of the recipient </span></li>
									<li><span>The degree of the immune system should match between the recipient and donor</span></li>
								</ul>
								<h3>
									<span> Organ Donation</span> Facts
								</h3>
								<p>The organ can be donated by-
								<ul>
									<li><span>Anyone above 18 years of age can become an organ donor</span></li>
									<li><span>If the organ is donated after death, then a detailed medical assessment is done to decide which organ can be successfully donated.</span></li>
									<li><span>The conditions that can exclude the organ donation are HIV, severe infections, kidney disease, heart disease and any type of Cancer.</span></li>
								</ul>
								<h3>
									<span>Organ</span> Donors
								</h3>
								<p>Both living and brain dead can be organ donors. The person who received an injury either pathological or traumatic to that specific part of the brain that controls the process of breathing and heartbeat, the person is declared as brain dead. The brain dead person is suitable for organ donation.</p>
								<p>
									<strong>Living Donor:</strong> A living donor could be a family member like parents, brother or sister. A living donor can donate renewable cells, fluid or tissues. The living donor can also donate small bowel, a partial donation of liver and a single kidney.
								</p>
								<p>
									<strong> Deceased Donor: </strong>These are those donors who are declared as brain dead and their organs are kept workable in ventilators till the time they can be used for transplantation.
								</p>
								<h5>
									To get best options for Organ transplant treatment, send us a
									query or write to us at <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
								</h5>
							</div>
							<!-- End Tab -->

						</div>
					</div>
					<!-- End Column -->
				</div>
				<!-- End Row -->
			</div>
			<!-- End Tab Accordion -->

		</div>
		<!-- End Container -->
	</div>
	<!-- Speciality Content Ends Here -->

	<%@ include file="newFooter.jsp"%>
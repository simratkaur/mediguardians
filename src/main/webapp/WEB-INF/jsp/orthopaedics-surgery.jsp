<%@ include file="newHeader.jsp"%></section>
<!-- Hospital Short Details Starts Here -->
<div class="short-details">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="title">
					<h1>Orthopedic Surgery</h1>
					<ul class="paginator">
						<li><a href="/">Home</a></li>
						<li><a>Orthopedic Surgery</a></li>
					</ul>
				</div>
			</div>
			<!-- End Column -->
		</div>
		<!-- End Row -->
	</div>
	<!-- End Container -->
</div>
<!-- Hospital Short Details Ends Here -->

<!-- Specialty Content Starts Here -->
<div class="speciality-content-area">
	<div class="container">

		<div class="innertab-accordion">
			<div class="row">
				<div class="col-md-3 tabnopaddright">
					<div class="tabs">
						<ul>
							<li class="tab-active"><a href="orthopaedics-surgery">Orthopedic
									Surgery</a></li>
							<li><a href="knee-replacement-surgery">Knee Replacement</a></li>
							<li><a href="hip-replacement-surgery">Hip Replacement
									surgery </a></li>
							<li><a href="shoulder-surgery">Shoulder Surgery </a></li>
							<li><a href="foot-ankle-surgery">Foot and Ankle surgery</a></li>
							<li><a href="hand-wrist-surgery">Hand Wrist surgery</a></li>
							<li><a href="elbow-replacement-surgery">Elbow Surgery</a></li>
						</ul>
					</div>
				</div>
				<!-- End Column -->
				<div class="col-md-9 tabnopaddleft">
					<div class="accordion-contentarea">

						<div id="tab1" class="visible">
							<h2>
								<span>Orthopaedics Surgery </span>�India
							</h2>
							<div class="row">
								<div class="col-md-9 col-sm-9 nopaddleft">
									<p>We at Arvene Healthcare have the expertise to organize
										for all treatments and surgeries related to Orthopedics.
										Arvene Healthcare is associated with top hospitals and Doctors
										of India in this field. We can give you more and better
										options so that you can take the right decisions. We also
										assist you in taking best decision based on your requirement.
										Arvene Healthcare will also arrange for your stay and local
										travel, so that you have nothing else to worry about but your
										treatment. Please contact us on <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
										today to avail best services.</p>
									<h3>Orthopedic Surgery:</h3>
									<p>Orthopedic surgery is that part of medical science which
										deals with an operation performed in the bones, joints, and
										ligaments in the human body. This surgery also deals with
										procedures of the musculoskeletal system as well as the
										nervous system. Robotic surgery has been latest advancement in
										technology related to orthopaedic surgery.</p>
								</div>
								<!-- End Inner Column -->
								<div class="col-md-3 col-sm-3 nopaddright">
									<img src="images/specialities/31.ortho main.jpg" alt="ortho"
										class="img-responsive">
								</div>
								<!-- End Inner Column -->
							</div>
							<!-- End Inner Row -->
							<h3>
								<span>Different kinds of </span>Orthopaedic Surgery
							</h3>
							<h3>
								<span>Joint </span>replacements
							</h3>
							<p>Joint replacement surgery restoresfreedom of movement and
								alleviate the pain because of degenerated and diseased joints.
								Hip and knee replacement are the most common surgeries.
								Technological advancements, have given way to cement less
								implants and customized fittings to increase longevity.</p>
							<br>
							<p>There are various kinds of joint replacement surgeries:</p>
							<ul>
								<li><span>Total hip replacement</span></li>
								<li><span>Total knee replacement</span></li>
								<li><span>Total shoulder replacement</span></li>
								<li><span>Total elbow replacement</span></li>
								<li><span>Partial Knee Replacement</span></li>
								<li><span>Wrist replacement</span></li>
								<li><span>Hand joint replacement surgery</span></li>
								<li><span>Ankle joint replacement</span></li>
							</ul>
							<h3>
								<span>Arthroscopic (Keyhole) </span>surgery
							</h3>
							<p>Arthroscopic surgeries, also known as Keyhole surgeries
								are the latest technical advancements. Patients are able to
								recover much faster. Knee replacement surgery is most commonly
								done by keyhole method. The overall stay in hospital is also
								reduced considerably. Arthroscopic surgery deals with the
								following:</p>
							<ul>
								<li><span>Limb lengthening</span></li>
								<li><span>Correction of deformities</span></li>
								<li><span>Fusion of joints </span></li>
								<li><span>Trauma and Fracture surgery</span></li>
								<li><span>High quality Swiss �AO� systems help
										stabilize fractures</span></li>
								<li><span>Image intensifier</span></li>
								<li><span>External Fixation Device Management of
										open fractures</span></li>
								<li><span>Infected non-union of long bones</span></li>
							</ul>
							<h3>
								<span>Spinal</span> Surgery
							</h3>
							<p>Degenerative, congenital and developmental disorders of
								spine are treated with Spinal Surgery. The complete spine
								surgery program will include services from orthopedic surgeons,
								neurosurgeons, neurologists, physical and rehabilitation
								medicine and anaesthesia. Now endoscopic spinal surgery and
								minimal surgeries are also being performed and technically
								advanced surgeries.</p>
							<br>
							<p>Spine surgery includes a number of things:</p>
							<ul>
								<li><span>Disc surgery Microsurgery for disc
										Fixation systems for fractures</span></li>
								<li><span>Surgery for correction of spinal
										deformities</span></li>
								<li><span>Surgery for spinal tuberculosis</span></li>
								<li><span>Endoscopic spine surgery</span></li>
							</ul>
							<h3>
								<span>Arthritis </span>care
							</h3>
							<p>Any kind of wear and tear of cartilage is called
								Arthritis. This may lead to aches, pains and stiffness in
								joints. Rheumatism is also used for pain in muscles and joints.
								Some terms used amongst the doctors are:</p>
							<ul>
								<li><span>Osteoarthritis </span></li>
								<li><span>Rheumatoid arthritis</span></li>
								<li><span>Infective arthritis</span></li>
								<li><span>Traumatic arthritis</span></li>
							</ul>
							<h3>
								<span>Paediatric </span>Orthopaedics
							</h3>
							<p>Paediatric orthopedicsdeals in the treatment of
								musculoskeletal problems in children, such as paediatric trauma,
								metabolic diseases, congenital malformations, and genetic
								abnormalities. Following are the included corrections:</p>
							<ul>
								<li><span>Congenital limb and spinal deformity
										corrections</span></li>
								<li><span>Bone and joint infection management</span></li>
								<li><span>Juvenile arthritis management</span></li>
							</ul>
							<h3>
								<span>Orthopaedic Surgery Benefits and</span> Cost in India
							</h3>
							<p>The major benefit of Orthopedic surgery in India is
								affordable cost as compared to western countries and this
								without any compromise on the quality of treatment. The doctors
								are among the best in world and hospitals today have installed
								state of the art equipment including robotics for Othopedic
								surgeries.</p>
							<h5>
								To get best options for Orthopaedic treatment, send us a query
								or write to us at <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
							</h5>
						</div>
						<!-- End Tab -->

					</div>
				</div>
				<!-- End Column -->
			</div>
			<!-- End Row -->
		</div>
		<!-- End Tab Accordion -->

	</div>
	<!-- End Container -->
</div>
<!-- Speciality Content Ends Here -->

<%@ include file="newFooter.jsp"%>
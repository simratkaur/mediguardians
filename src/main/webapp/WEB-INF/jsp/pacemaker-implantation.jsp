<%@ include file="newHeader.jsp"%></section>
	<!-- Hospital Short Details Starts Here -->
	<div class="short-details">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="title">
						<h1>Cardiology</h1>
						<ul class="paginator">
							<li><a href="/">Home</a></li>
							<li><a>Cardiology</a></li>
						</ul>
					</div>
				</div>
				<!-- End Column -->
			</div>
			<!-- End Row -->
		</div>
		<!-- End Container -->
	</div>
	<!-- Hospital Short Details Ends Here -->

	<!-- Specialty Content Starts Here -->
	<div class="speciality-content-area">
		<div class="container">

			<div class="innertab-accordion">
				<div class="row">
					<div class="col-md-3 tabnopaddright">
						<div class="tabs">
							<ul>
								<li><a href="cardiology-treatment">Cardiology</a></li>
								<li><a  href="coronary-angiography">Coronary Angiography</a></li>
								<li><a href="coronary-angioplasty">Coronary Angioplasty</a></li>
								<li><a href="coronary-artery-bypass-surgery">Coronary Artery
										Bypass</a></li>
								<li><a href="paediatric-cardiac-surgery">Paediatric Cardiac
										Surgery</a></li>
								<li class="tab-active"><a href="pacemaker-implantation">Pacemaker
										Implantation</a></li>
								<li><a href="vascular-surgery">Vascular Surgery</a></li>
								<li><a href="heart-valve-replacement-surgery">Heart Valve
										Replacement </a></li>
								<li><a href="cardiac-diagnostic">Cardiac Diagnostics</a></li>
							</ul>
						</div>
					</div>
					<!-- End Column -->
					<div class="col-md-9 tabnopaddleft">
						<div class="accordion-contentarea">

														<div id="tab6" >
								<h3>
									<span>Pacemaker </span>Implantation
								</h3>
								<div class="row">
									<div class="col-md-9 col-sm-9 nopaddleft">
										<p>We at Arvene Healthcare have the expertise to organize for all treatments and surgeries related to Cardiology. Arvene Healthcare is associated with top hospitals and Doctors of India in this field. We can give you more and better options so that you can take the right decisions. We also assist you in taking best decision based on your requirement. Arvene Healthcare will also arrange for your stay and local travel, so that you have nothing else to worry about but your treatment. Please contact us on <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a> today to avail best services.
										</p>
									</div>
									<!-- End Inner Column -->
									<div class="col-md-3 col-sm-3 nopaddright">
										<img src="images/specialities/6.pacemaker.jpg" alt="pacemaker"
											class="img-responsive">
									</div>
									<!-- End Inner Column -->
								</div>
								<!-- End Inner Row -->
								<h3>
									<span>Why is  </span>pacemaker implantation required
								</h3>
								<p>
									Healthy human heart can make its own rhythm with a natural pacemaker that regulates the beats of the heart at a normal pace. However, sometimes, heart might not function regularly and would require a pacemaker to take care of the pace and to rectify the issue. Having a pacemaker implantation procedure would help you have a steady heart rate as the device can adjust the heartbeat by sensing it when it is very slow, irregular or very fast. It contacts the heart muscles by delivering electric impulses through its electrodes to regulate the heartbeat. You would be advised to undergo pacemaker implantation for various reasons which might be actually fatal for your life.
									<br /> &#8226; Very slow level of heartbeat,
									also known as bradycardia or fast heartbeat at an abnormal
									level, known as supraventricular tachycardia. <br /> &#8226;
									When the heart's senatorial node gets damaged. <br /> &#8226;
									When you suffer from a heart block, the electrical signals that
									regulate your heartbeat would not be transmitted correctly. <br />
									&#8226; When the electric signals faces an issue it can make
									the heart to stop functioning thereby ruling out any heartbeat
									causing a cardiac arrest.
								</p>
								<h3>
									<span>Procedure  </span>Details:
								</h3>
								<p>During the pacemaker implantation procedure, the tiny electrical device known as the pacemaker would be implanted in the patient's chest. This pacemaker that is fitted through the surgical procedure would transmit electric pulses at a regular pace to keep the human heart beat in a normal way. This surgical method is highly useful for you as it would help your heart to beat in rhythm and is a lifesaver for many with heart issues. This small device is made of metal and weighs just 20 g to 50 g and have wires attached to it. This device can be adjusted based on the requirements of your body and has the ability to sense if your heartbeat is not normal and regulates it by sending signals.
								</p>
								<h3>
									<span>About </span>pacemaker implantation:
								</h3>
								<p>This is a straight forward surgical procedure that is
									performed after administering local anaesthesia, which means
									the patient would remain awake during the entire process that
									lasts for nearly an hour. You can follow your daily routine
									within a short time and would be advised to refrain from
									strenuous activities for 4 to 6 weeks.</p>
								<h5>Cost of Pacemaker in India starts at 5200 US Dollars. To know more about it please share your reports on <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a> or whatsapp/viber/imo to +918130077375.</h5><p> 
(Please note this is a tentative plan and actual Costs are based on reports shared. Final treatment plan and estimation will be provided after the patient has been evaluated.)</p>
								<h5>
									To get best options for cardiology treatment, send us a query
									or write to us at <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
								</h5>
							</div>
							<!-- End Tab -->
							

						</div>
					</div>
					<!-- End Column -->
				</div>
				<!-- End Row -->
			</div>
			<!-- End Tab Accordion -->

		</div>
		<!-- End Container -->
	</div>
	<!-- Speciality Content Ends Here -->

	<%@ include file="newFooter.jsp"%>
<%@ include file="newHeader.jsp"%></section>
<!-- Hospital Short Details Starts Here -->
<div class="short-details">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="title">
					<h1>Cardiology</h1>
					<ul class="paginator">
						<li><a href="/">Home</a></li>
						<li><a>Cardiology</a></li>
					</ul>
				</div>
			</div>
			<!-- End Column -->
		</div>
		<!-- End Row -->
	</div>
	<!-- End Container -->
</div>
<!-- Hospital Short Details Ends Here -->

<!-- Specialty Content Starts Here -->
<div class="speciality-content-area">
	<div class="container">

		<div class="innertab-accordion">
			<div class="row">
				<div class="col-md-3 tabnopaddright">
					<div class="tabs">
						<ul>
							<li><a href="cardiology-treatment">Cardiology</a></li>
							<li><a href="coronary-angiography">Coronary Angiography</a></li>
							<li><a href="coronary-angioplasty">Coronary Angioplasty</a></li>
							<li><a href="coronary-artery-bypass-surgery">Coronary
									Artery Bypass</a></li>
							<li class="tab-active"><a href="paediatric-cardiac-surgery">Paediatric
									Cardiac Surgery</a></li>
							<li><a href="pacemaker-implantation">Pacemaker
									Implantation</a></li>
							<li><a href="vascular-surgery">Vascular Surgery</a></li>
							<li><a href="heart-valve-replacement-surgery">Heart
									Valve Replacement </a></li>
							<li><a href="cardiac-diagnostic">Cardiac Diagnostics</a></li>
						</ul>
					</div>
				</div>
				<!-- End Column -->
				<div class="col-md-9 tabnopaddleft">
					<div class="accordion-contentarea">

						<div id="tab5" >
							<h2>
								<span>Paediatric Cardiac Surgery</span> in India
							</h2>
							<p>We at Arvene Healthcare have the expertise to organize for
								all treatments and surgeries related to Cardiology. Arvene
								Healthcare is associated with top hospitals and Doctors of India
								in this field. We can give you more and better options so that
								you can take the right decisions. We also assist you in taking
								best decision based on your requirement. Arvene Healthcare will
								also arrange for your stay and local travel, so that you have
								nothing else to worry about but your treatment. Please contact
								us on <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a> today to avail best services.</p>

							<h3>Paediatric Cardiology</h3>
							<div class="row">
								<div class="col-md-9 col-sm-9 nopaddleft">

									<p>Few children are born with heart defects known as known
										as congenital heart defects, for which surgery / treatment is
										required. Paediatric Cardiac Surgery deals with operative
										procedures in the new-born children and youngsters suffering
										from cardiac dysfunctions, structural, functional and
										rhythm-related issues of the heart.</p>
									<p>Congenital heart disease is a cause of improper growth
										of the heart or blood vessels before birth.</p>
								</div>
								<!-- End Inner Column -->
								<div class="col-md-3 col-sm-3 nopaddright">
									<img src="images/specialities/5.pedia cardiac surgery.jpg"
										alt="pedia cardiac surgery" class="img-responsive">
								</div>
								<!-- End Inner Column -->
							</div>
							<!-- End Inner Row -->
							<h3>
								<span>Paediatric Cardiology </span>Facilities
							</h3>
							<p>The Paediatric cardiology department of chosen hospital
								will offer all non-invasive diagnostic and paediatric cardiac
								interventional services. The diagnostic modalities include
								foetal echocardiography, echocardiography, Holter, CT and MR
								angiography as well as diagnostic cardiac catheterisation. In
								this department Cardiologists, Surgeons, anaesthesiologists and
								other specialists work together to care of children suffering
								from cardio-vascular disorders.</p>
							<h3>
								<span>Paediatric Cardiothoracic</span> Surgery
							</h3>
							<p>There are a number of Paediatric Cardiology Surgeries such
								as balloon atrial septostomy, Valvuloplasty, Ventricular Septal
								Defect and Atrial Septal Defect, etc. The Major are:</p>
							<p>
								<strong>Balloon Atrial Septostomy (BAS): </strong> In this
								procedure a hole is made between between the right atrium and
								the left atrium. It is often used to manage patients with
								transposition of the great arteries. Here the larger hole
								improves oxygenation of the blood.

							</p>
							<p>
								<strong>Valvuloplasty/Angioplasty: </strong> Valvuloplasty is
								that process where a small balloon is inserted and inflated for
								stretching and opening a narrowed heart valve.

							</p>

							<p>
								<strong>Atrial Septal Defect (ASD) and Ventricular
									Septal Defect (VSD): </strong> Both of these are congenital defects. In
								ASD, the wall that separates the upper heart chambers (atria)
								does not close completely. The surgery here involves a placement
								of an ASD device into the heart through catheters. While in VSD,
								one or more holes in the wall that separates the right and left
								ventricle of the heart are closed by VSD device. In pre-natal
								stages, the right and left ventricles of a heart are not
								separate. As the foetus grows, a wall is formed for separating
								the two ventricles but if the wall does not form completely, a
								hole remains.

							</p>

							<p>
								<strong>Apart from the above mentioned Paediatric
									Cardiac Surgeries, some of the others have been listed below</strong>
							</p>
							<ul>
								<li><span>BT Shunt / PA plasty on bypass/Surgical
										atrial septectomy</span></li>
								<li><span>ASD / PAPVR/ GLENN/ VSD/ TOF/ PAVC/ DORV</span></li>
								<li><span>All Re-do cases/ CAVC/ FONTAN</span></li>
								<li><span>Systematic-Pulmonary Artery SHUNTS</span></li>
								<li><span>All cases(3 months old/Arterial Switch
										Operation/Truncus Arteriosus)</span></li>
								<li><span>CoA/Simple Vascular Ring/Pacemaker
										Insertion</span></li>
								<li><span>Cases(3 months
										old/Shunts/PAB/Unifocalization/CoA/Complex Rings)</span></li>
							</ul>
							<h5>Cost of ASD / VSD surgery in India starts at 5500 US Dollars. To know more about it please share your reports on <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a> or whatsapp/viber/imo to +918130077375.</h5><p> 
(Please note this is a tentative plan and actual Costs are based on reports shared. Final treatment plan and estimation will be provided after the patient has been evaluated.)</p>
							
							<h5>
								To get best options for cardiology treatment, send us a query or
								write to us at <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
							</h5>

						</div>
						<!-- End Tab -->

					</div>
				</div>
				<!-- End Column -->
			</div>
			<!-- End Row -->
		</div>
		<!-- End Tab Accordion -->

	</div>
	<!-- End Container -->
</div>
<!-- Speciality Content Ends Here -->

<%@ include file="newFooter.jsp"%>
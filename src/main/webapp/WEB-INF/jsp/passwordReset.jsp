<%@ include file="newHeader.jsp"%></section>
<script type="text/javascript">
	window.onload = function() {
		$("#cssmenu").hide();
		$("#userName").hide();
	}
	function Submit() {
		if (isRequired("password") && minChar("password")
				&& matchField("password", "repassword")) {
			$(".custError").text("");
			return true;
		} else {
			return false;
		}
	}
</script>
<div class="container">
	<div class="mainContent">
		<h3 class="pageHeader" style="margin: auto;">Please Enter New
			Password</h3>
		<div class="row">
			<div class="col-md-12">
				<form:form name='passwordReset' action='passwordReset' method='POST'
					modelAttribute="user"  class=" has-validation-callback" >
					<fieldset class="registration-form">

						<div class="control-group">
							<div class="controls">
								<span style="display: inline-block; width: 125px;">New
									Password:</span> <input type='password' name="password_confirmation" id="password"
									maxlength="15" data-validation="length"
									data-validation-length="min8"
									data-validation-error-msg="Passwords should be atleast 8 characters" />
							</div>
						</div>

						<div class="control-group">
							<div class="controls">
								<span style="display: inline-block; width: 125px;">Re-enter
									Password:</span> <input type='password' id="password_confirm" maxlength="15"  name="password" 
									data-validation="confirmation"
									data-validation-error-msg="Passwords didn't match"/>
							</div>
						</div>

						<div class="control-group">
							<div class="controls">
								<span style="display: inline-block; width: 125px;"></span> <input
									name="Reset" type="submit" value="Reset"
									class="btn btn-primary" /><!-- onclick=" return Submit()" --> 
							</div>
						</div>

						<div class="control-group">
							<div class="controls">
								<span style="display: inline-block; width: 125px;"> </span> <a
									href="login">Login</a>
							</div>
						</div>

						<input type="hidden" name="${_csrf.parameterName}"
							value="${_csrf.token}" />
						<form:input path="username" name='username' id="email"
							type="hidden" />
					</fieldset>
				</form:form>
			</div>
		</div>
	</div>
</div>
<script>
$.validate({
	modules : 'date, security'

});
</script>
<%@ include file="newFooter.jsp"%>
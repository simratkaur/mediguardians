<%@ include file="newHeader.jsp"%></section>
<!-- 
        PAYMENT PAGE START
        PAYMENT PAGE START -->
        <section class="sec-pay-a">
            <Div class="container">
                <div class="row">
                    <div class="col-lg-2">

                    </div>

                    <div class="col-lg-8">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="pay-1">
                                    <h2>Payment</h2>
                                </div>
                            </div>                    
                        </div>
                        <div class="row marg-top">
                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                <div class="pay-count">
                                    <p>1</p>
                                </div>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                <div class="pay-con-main">
                                    <div class="pay-con">
                                        <p>Order Summary</p>
                                        <div class="radio">
                                            <label><input type="radio" name="optradio">Text Consultation with super specialist</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-9 col-md-9 col-xs-8 col-sm-8">
                                            <div class="pay-input">
                                                <select class="form-control">
                                                    <option>India/Indian Rupees</option>
                                                    <option>India/Indian Rupees</option>
                                                    <option>India/Indian Rupees</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-xs-4 col-sm-4">
                                            <div class="pay-input">
                                                <input class="form-control" type="text" value="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row marg-top">
                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                <div class="pay-count">
                                    <p>2</p>
                                </div>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                <div class="pay-con-main">
                                    <div class="pay-con">
                                        <p>Promotional Code</p>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-9 col-md-9 col-xs-8 col-sm-8">
                                            <div class="pay-input">
                                                <input class="form-control" type="text" placeholder="Enter Code">
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-xs-4 col-sm-4">
                                            <div class="pay-input">
                                                <button class="btn">Verify</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row marg-top">
                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                <div class="pay-count">
                                    <p>3</p>
                                </div>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                <div class="pay-con-main">
                                    <div class="pay-con">
                                        <p>Select Payment Method</p>
                                        <div style="float: left"><form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
							<input type="hidden" name="cmd" value="_xclick">
							<input type="hidden" name="business" value="finance@arvenehealthcare.com" />
							
							<input type="hidden" name="currency_code" value="USD" />
							<input type="hidden" name="hosted_button_id" value="RAULFSVKV3AMU">
							<input type="hidden" name="item_name" value="query">
							<input type="hidden" name="item_number" value="${paymentForm.udf1}">
							
								<input type="hidden" name="amount" value="${ paymentForm.amount }">
								<input type="hidden" name="no_shipping" value="1">
								<input type='hidden' name='rm' value='2'>
							
							 <input type="hidden" name="return" value="${ paymentForm.surl }"  />
                            <input type="hidden" name="cancel_return" value="${ paymentForm.furl }" />
							
<!-- 							<input type="image" src="https://www.paypalobjects.com/en_GB/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
 -->							
 						 	<input type="image" src="images/payment/pa1.png" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
 							<img alt="" border="0" src="https://www.paypalobjects.com/en_GB/i/scr/pixel.gif" width="1" height="1">
							</form> 
							
							<!-- sand box  -->
							<%-- <form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" target="_top">
								<input type="hidden" name="cmd" value="_xclick">
								<input type="hidden" name="business" value="sandbox-merchant-us@arvenehealthcare.com" />
								<input type="hidden" name="currency_code" value="USD" />
								<input type="hidden" name="item_name" value="query">
								<input type="hidden" name="item_number" value="${paymentForm.udf1}">
								<input type="hidden" name="amount" value="${ paymentForm.amount }">
								<input type="hidden" name="no_shipping" value="1">
								<input type='hidden' name='rm' value='2'>
								
							
								 <input type="hidden" name="return" value="${ paymentForm.surl }"  />
                          	  <input type="hidden" name="cancel_return" value="${ paymentForm.furl }" />
								
								<input type="hidden" name="hosted_button_id" value="JDPL2XFNN2SAC">
								<input type="image" src="images/paypal.png" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
								<img alt="" border="0" src="https://www.sandbox.paypal.com/en_GB/i/scr/pixel.gif" width="1" height="1">
							</form> --%>
							
							<!-- sand box  --></div>
                                       <div><input type="image" src="images/payment/pa3.png" border="0" name="submit">
                                       <input type="image" src="images/payment/pa4.png" border="0" name="submit">
                                       <input type="image" src="images/payment/pa5.png" border="0" name="submit">
                                       <input type="image" src="images/payment/pa6.png" border="0" name="submit"></div>
                                      
                                    </div>
                                    <div class="umoney">
                                        <a href="#"><img src="images/payment/pa2.png"/></a>
                                        <span>(Only for india user)</span>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <form name="paymentprocessform" id="paymentprocessform"
			action="${ paymentForm.serviceUrl }" method="POST">


			<input type="hidden" name="txnid"
				value="${ paymentForm.transactionId }"></input> <input type="hidden"
				name="productinfo" value="${ paymentForm.productInfo }"></input> <input
				type="hidden" name="surl" value="${ paymentForm.surl }"></input> <input
				type="hidden" name="furl" value="${ paymentForm.furl }"></input> <input
				type="hidden" name="key" value="${ paymentForm.key }"></input> <input
				type="hidden" name="service_provider"
				value="${ paymentForm.serviceProvider }"></input> <input
				type="hidden" name="salt" value="${ paymentForm.salt }"></input> <input
				type="hidden" name="firstname" value="${ paymentForm.firstName }"></input>
			<input type="hidden" name="amount" value="${ paymentForm.amount }"></input>
			<input type="hidden" name="email" value="${ paymentForm.email }"></input>
			<input type="hidden" name="phone" value="${ paymentForm.phone }"></input>
			<input type="hidden" name="hash" value="${  paymentForm.checksum }"></input>
			<input type="hidden" name="udf1" value="${  paymentForm.udf1 }"></input>

			<!-- 						<button type="submit">Submit</button>
 -->

		</form>
                    <div class="col-lg-2">

                    </div>
                </div>
                
            </Div>
        </section>
        <!----------------------------PAYMENT PAGE END------------------------->
        <!----------------------------PAYMENT PAGE END------------------------->


        <!-----------------MY SCRIPTING START----------------------> 
        <!-----------------MY SCRIPTING START----------------------> 
        <script src="js/jquery.js"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/custom.js"></script>
        <!-----------------MY SCRIPTING END---------------------->
        <!-----------------MY SCRIPTING END---------------------->
    </body>
</html>
<%@ include file="newFooter.jsp"%>
<script type="text/javascript">

 /* window.onload = submitPayuForm(); */
 
function submitPayuForm() {
	
      var payuForm = document.forms.paymentprocessform;
      payuForm.submit();
    } 
    
</script>

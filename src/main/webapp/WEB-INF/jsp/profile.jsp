<%@page import="com.medicine.arvene.util.CommonUtil"%>
<%@ include file="newHeader.jsp"%></section>




 <section class="sec-main sec-drl sec-drp">
            <div class="container">
            
           <%--  <form:form class="has-validation-callback" action='saveprofile'
					method="POST" modelAttribute="user" enctype="multipart/form-data"> --%>
                <div class="drl-bgwhite">
                    <div class="row">
                    
                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4">
                            <div class="drp-img">
                                <img src="${user.profileImage}" />
                            </div>
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-8 col-xs-8">
                            <div class="drp-con">
                               <!--  <p class="p-edit">Dr Bruce L. Feldman <span href="#" class="click-show btn btn-primary pull-right">Edit</span></p> -->
                          		<p class="p-edit"> 
								${user.firstName } ${user.middleName }
								${user.lastName } <a href="#" ><span class="click-show btn btn-primary pull-right"> Edit</span></a></p>
                                <p> ${user.degrees}</p>
                                <p></p>
                                <div class="row margtop">
                                    <div class="col-lg-3 col-sm-3 col-xs-5 col-md-3">
                                        <p class="drl-col">Specialty</p>
                                    </div>
                                    <div class="col-lg-3 col-sm-3 col-xs-7 col-md-3">
                                        <p>${user.speciality.name}</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-3 col-sm-3 col-xs-5 col-md-3">
                                        <p class="drl-col">Designation</p>
                                    </div>
                                    <div class="col-lg-3 col-sm-3 col-xs-7 col-md-3">
                                        <p>${user.title}</p>
                                    </div>
                                </div>
                                <div class="row ">
                                    <div class="col-lg-3 col-sm-3 col-xs-5 col-md-3">
                                        <p class="drl-col">Hospital Affiliation</p>
                                    </div>
                                    <div class="col-lg-3 col-sm-3 col-xs-7 col-md-3">
                                        <p>${user.hospital}</p>
                                    </div>
                                </div>
                                <div class="row ">
                                    <div class="col-lg-2 col-sm-12 col-xs-12 col-md-2">
                                        <button class="btn btn-primary margtop" 
                                         onClick="location.href='askQuery?uid=${user.user.id}'">Text Consultation</button>
                                        <p>Fee Rs:${user.queryFee}</p>
                                    </div>
                                    <div class="col-lg-2 col-sm-12 col-xs-12 col-md-2">
                                        <button class="btn btn-primary margtop" 
                                        onClick="location.href='viewcalendar?uid=${user.user.id}'">Video Consultation</button>
                                        <p>Fee Rs:${user.sessionFee}</p>
                                    </div>
                                </div>



                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="drl-bgwhite drp-pad margtop">
                    <div class="drp-bot">
                        <h3 class="drl-col">Biography:</h3>
                         <p class="margtop p-text-hid">${user.biography }</p>
                     <!--    <p class="margtop p-text-hid">
                        	
                        Dr Sachin Kandhari did his DNB, (Neuro Surgery) from VIMHANS Hospital, New 
                            Delhi. He has worked as an associate Neurosurgeon with various hospitals. 
                            His expertise and special interests are in the field of Minimal Invasive 
                            Spinal Surgery, Spinal instrumentation, Endoscopic Neurosurgery and Neurovascular 
                            Surgery. He is one of the founding Neurosurgeons of IBS Hospitals and as its 
                            Managing Director</p> -->
                        <form:form action="updateProfile"  id="updateBioForm" method = "POST"  modelAttribute="user" >
	                        <input type="hidden" name="id" value = "${user.id }" />
	                        <div class="has-feedback">
	                        <textarea class="form-control p-texta" rows="4" placeholder="Biography" name ="biography" id ="biography"
	                        maxlength = "500">${user.biography }</textarea>
	                        </div>
	                        <div class="alignn-center">
	                            <span class="btn btn-primary dis-sho" onclick="$('#updateBioForm').submit()">Save Changes</span>
	                        </div>
                        </form:form>
                    </div>
                </div>
               <%--  </form:form>
 --%>            </div>
        </section>





<%-- <div class="container">
	<div class="mainContent">
		<h3 class="pageHeader" style="margin: auto;">Profile</h3>
		<div class="row">
			<div class="col-md-10">
				<form:form class="has-validation-callback" action='saveprofile'
					method="POST" modelAttribute="user" enctype="multipart/form-data">
					<div class="control-group profilePicDiv" style="">
						<div id="pofilePicture">
							<img src="${user.profileImage}" />
						</div>

						<label class="file-btn rounded-Box"
							style="text-align: center; margin: auto; margin-top: 10px; height: 40px; width: 200px;">
							Upload Profile Picture<input type="file" name="newProfileImage"
							id="newProfileImage"
							accept="image/x-png,image/x-bpm,image/x-jpg,image/gif,image/jpeg"
							style="width: 50%; display: inherit; opacity: 0; -moz-opacity: 0; filter: progid:DXImageTransform.Microsoft.Alpha(opacity=0)" />
						</label>
						<!-- style="width: 50%; display: inherit; opacity: 0; -moz-opacity: 0; filter: progid:DXImageTransform.Microsoft.Alpha(opacity=0)" -->

					</div>
					<div class="panel-group" id="accordion">
						<div class="panel panel-default" id="panel1">
							<div id="collapseOne" class="panel-collapse">
								<div class="panel-body">
									<div class="control-group">
										<div class="controls">
											<span style="display: inline-block; width: 125px;">
												Title :</span>
											<form:select path="title" name='title' id="title"
												readonly="true">
												<form:options items="${CommonUtil.getTitle()}"
													readonly="true" />
											</form:select>
										</div>

									</div>




									<div class="control-group">
										<!-- Name -->
										<div class="controls">
											<form:input type="hidden" id="id" name="id" path="id" />

											<span style="display: inline-block; width: 125px;">First
												Name :</span>
											<form:input type="text" path="firstName" readonly="true"
												placeholder="First Name" data-validation="required alpha"
												data-validation-error-msg="First name has to be alphabets only" />
										</div>
									</div>
									<div class="control-group">
										<div class="controls">
											<span style="display: inline-block; width: 125px;">Middle
												Name :</span>
											<form:input type="text" path="middleName" readonly="true"
												placeholder="Middle Name" />
										</div>
									</div>

									<div class="control-group">
										<div class="controls">
											<span style="display: inline-block; width: 125px;">Last
												Name :</span>
											<form:input type="text" path="lastName" readonly="true"
												placeholder="Last Name" data-validation="required alpha"
												data-validation-error-msg="Last name has to be alphabets only" />
										</div>
									</div>

									<div class="control-group">
										<div class="controls">

											<span style="display: inline-block; width: 125px;">Practicing
												from :</span>
											<form:input class="" placeholder="Start Date" readonly="true"
												type="text" id="practiceStartYrStr"
												path="practiceStartYrStr" name="practiceStartYrStr"
												data-validation="date" data-validation-format="dd/mm/yyyy"
												data-validation-error-msg="Please enter a valid date"></form:input>
											&nbsp;&nbsp;

										</div>
									</div>

									<div class="control-group">
										<div class="controls">
											<span style="display: inline-block; width: 125px;">Degree
												:</span>
											<form:select path="degree" name='degree' id="degree"
												readonly="true" data-validation="required"
												data-validation-error-msg="Degree details are Mandatory">
												<form:option value="" label="Choose Degree" />
												<form:options items="${degree}" />
											</form:select>

										</div>
									</div>
									<div class="control-group">
										<div class="controls">
											<span style="display: inline-block; width: 125px;">Passing
												Year :</span>
											<form:input length="4" type="text" path="degreeYear"
												readonly="true" placeholder="Year" class="yearpick"
												data-validation="length number"
												data-validation-length="min4"
												data-validation-error-msg="Please Enter Passing Year in YYYY format" />
										</div>
									</div>

									<div class="control-group">
										<div class="controls">
											<span style="display: inline-block; width: 125px;">College
												Name :</span>
											<form:select path="degreeCollg" name='degreeCollg'
												onclick="enableBox()" id="degreeCollg"
												data-validation="required"
												data-validation-error-msg="College Details are Mandatory">
												<form:option value="" label="Choose College" />
												<form:options items="${college}" />
											</form:select>
											<span style="display: inline-block; width: 125px;"></span>
											<form:input path="otherCollg" style="display:none"
												placeholder="Others" />
										</div>
									</div>

									<div class="control-group">
										<div class="controls">
											<span style="display: inline-block; width: 125px;">Query
												Fee :</span>
											<form:input path="queryFee" data-validation="required"
												readonly="true"
												data-validation-error-msg="Please enter Fee to be charged for each query" />
											<form:option value="0" label="Query Fee" />
												<c:forEach begin="100" end="1200" step="100" var="fee">
													<form:option value="${fee}">Rs. ${fee}</form:option>
												</c:forEach>
											</form:select>
										</div>
									</div>
									<div class="control-group">
										<div class="controls">
											<span style="display: inline-block; width: 125px;">Session
												Fee :</span>
											<form:input path="sessionFee" data-validation="required"
												readonly="true"
												data-validation-error-msg="Please enter Fee to be charged for each session" />
											<form:option value="0" label="Session Fee" />
												<c:forEach begin="100" end="1200" step="100" var="fee">
													<form:option value="${fee}">Rs. ${fee}</form:option>
												</c:forEach>
											</form:select>
										</div>
									</div>

									<div class="control-group">
										<div class="controls">
											<span style="display: inline-block; width: 125px;">Reg.
												No. :</span>
											<form:input length="10" id="mregno" name="mregno"
												readonly="true" path="medregno"
												placeholder="Medical Registration Number"
												data-validation="required"
												data-validation-error-msg="Medical Registration Number is Mandatory" />
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="panel panel-default" id="panel2">
							<div class="panel-heading">
								<h4 class="panel-title">
									<span id="collapseTwoClick" data-toggle="collapse"
										data-target="#collapseTwo" href="#collapseTwo"
										class="collapsed"> Professional Details </span>
								</h4>

							</div>

							<div id="collapseTwo" class="panel-collapse collapse">
								<div class="panel-body">
									<div class="control-group">
										<div class="controls">
											<span style="display: inline-block; width: 125px;">Specialist
												:</span>
											<form:select name="speciality" path="specialityid"
												id="specialityid" style="height:32px"
												data-validation="required"
												data-validation-error-msg="Please Enter Speciality Details">
												<form:option value="" label="Choose Speciality" />
												<form:options items="${specialities}" />
											</form:select>
										</div>
									</div>
									<div class="control-group">
										<div class="controls">
											<span style="display: inline-block; width: 125px;">Sub-Specialist
												:</span>
											<form:input path="subSpeciality" placeholder="Sub Speciality" />
										</div>
									</div>
									<div class="control-group">
										<div class="controls">
											<span style="display: inline-block; width: 125px;">Area
												of Interest:</span>
											<form:input path="areaOfInterest"
												placeholder="Area of Interest" data-validation="required"
												data-validation-error-msg="Area of Interest is Required" />
										</div>
									</div>
									<div class="control-group">
										<div class="controls">
											<span style="display: inline-block; width: 125px;">Designation
												:</span>
											<form:input path="designation" placeholder="Designation" />
										</div>
									</div>
									<div class="control-group">
										<div class="controls">
											<span style="display: inline-block; width: 125px;">Hospital/Clinic:</span>
											<form:input path="hospitalLoc" placeholder="Hospital/Clinic"
												data-validation="required"
												data-validation-error-msg="Please enter Hospital Name" />

										</div>
									</div>

									<div class="control-group">
										<div class="controls">
											<span style="display: inline-block; width: 125px;">Area
												:</span>
											<form:input path="hospitalArea" placeholder="Area"
												data-validation="required"
												data-validation-error-msg="Please enter Hospital Area" />
										</div>
									</div>

									<div class="control-group">
										<div class="controls">
											<span style="display: inline-block; width: 125px;">LandMark
												:</span>
											<form:input path="hospitalLandMark" placeholder="LandMark"
												data-validation="required"
												data-validation-error-msg="Please provide a landmark" />

										</div>
									</div>
									<div class="control-group">
										<div class="controls">
											<span style="display: inline-block; width: 125px;">City
												:</span>

											<form:input path="hospitalCity" placeholder="City"
												data-validation="required"
												data-validation-error-msg="Please enter Hospital City" />
										</div>
									</div>

									<div class="control-group">
										<div class="controls">
											<span style="display: inline-block; width: 125px;">Pin
												Code :</span>

											<form:input path="hospitalZip" placeholder="Pin Code"
												data-validation="required"
												data-validation-error-msg="Please enter Hospital Pincode" />

										</div>
									</div>

								</div>
							</div>
						</div>
						<div class="panel panel-default" id="panel3">
							<div class="panel-heading">
								<h4 class="panel-title">
									<span data-toggle="collapse" data-target="#collapseThree"
										href="#collapseThree" class="collapsed"> Miscellaneous
									</span>
								</h4>

							</div>
							<div id="collapseThree" class="panel-collapse collapse">
								<div class="panel-body">
									<div class="control-group">
										<div class="controls">
											<span style="display: inline-block; width: 125px;">Biography
												:</span>
											<form:textarea maxlength="500" path="biography" />
										</div>
									</div>
									<div class="control-group">
										<div class="controls">
											<span style="display: inline-block; width: 125px;">Awards
												:</span>
											<form:textarea maxlength="500" path="awards" />
										</div>
									</div>
									<div class="control-group">
										<div class="controls">
											<span style="display: inline-block; width: 125px;">Achievements
												:</span>
											<form:textarea maxlength="500" path="achievements" />
										</div>
									</div>
									<div class="control-group">
										<div class="controls">
											<span style="display: inline-block; width: 125px;">Membership
												:</span>
											<form:textarea maxlength="500" path="memberShip" />
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="control-group">
							<div class="controls">
								<button class="btn btn-primary btn-large btn-block"
									style="width: 20%; margin-left: 40%;"
									onclick=" return Submit()">Save</button>
							</div>
						</div>
					</div>
				</form:form>
			</div>
		</div>
	</div>
</div> --%>
<script type='text/javascript'>
	$('.nav >li.active').removeClass("active");
	$('.nav #profile').addClass("active");
</script>

<script>
	/* $.validate({
		modules : 'date, security'

	}); */

	window.onload = function() {
		enableBox();
	}
	function enableBox() {
		if ($("#degreeCollg").val() == "Others(Please specify)") {
			$("#otherCollg").show()
		} else {
			$("#otherCollg").hide();
		}
	}
	function Submit() {
		var x = document.getElementById("newProfileImage");
		if ('files' in x) {
			if (x.files.length != 0) {
				var file = x.files[0];
				if ('name' in file) {
					var ext = file.name
							.substring(file.name.lastIndexOf('.') + 1);
					if (ext != "jpg" && ext != "jpeg" && ext != "bmp"
							&& ext != "gif" && ext != "png") {
						$("#errorBox").html(
								"Can upload only .jpg, .jpeg, .bmp, .gif, .png extensions "
										+ ext);
						popUp();
						return false;
					}
				}
				if ('size' in file && file.size > 400000) {
					$("#errorBox").html("File size is greater then expected");
					popUp();
					return false;
				}
			} else {
				if ("${user.profileImage eq null}" == false) {
					$("#errorBox").html("Profile Picture is mandatory");
					popUp();
					return false;
				}
			}
		}
	}
</script>
<style>
select, input[type="text"] {
	height: 100%;
	box-sizing: border-box;
	width: 50%;
}

.panel-group {
	width: 60%;
}

@media ( max-width : 767px) {
	select, input[type="text"] {
		width: 100%;
	}
	.panel-group {
		width: 100%;
	}
}

@media ( min-width : 768px) and (max-width : 1024px) {
	select, input[type="text"] {
		width: 70%;
	}
	.panel-group {
		width: 58%;
	}
}
</style>
<script>
	$('#collapseTwo').on('DOMNodeInserted', function(e) {
		if ($("#collapseTwoClick").attr("class") == "collapsed")
			$("#collapseTwoClick").click();

		/* // This is not working may be its child node
		if ($(e.target).is('.has error')) {
			alert("collapseTwo");
			$("#collapseTwoClick").click();
		} */
	});
</script>
<%@ include file="newFooter.jsp"%>

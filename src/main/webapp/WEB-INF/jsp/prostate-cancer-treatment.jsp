<%@ include file="newHeader.jsp"%></section>
<!-- Hospital Short Details Starts Here -->
<div class="short-details">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="title">
					<h1>Cancer</h1>
					<ul class="paginator">
						<li><a href="/">Home</a></li>
						<li><a>Cancer</a></li>
					</ul>
				</div>
			</div>
			<!-- End Column -->
		</div>
		<!-- End Row -->
	</div>
	<!-- End Container -->
</div>
<!-- Hospital Short Details Ends Here -->

<!-- Specialty Content Starts Here -->
<div class="speciality-content-area">
	<div class="container">

		<div class="innertab-accordion">
			<div class="row">
				<div class="col-md-3 tabnopaddright">
					<div class="tabs">
						<ul>
							<li><a href="cancer-treatment">Cancer</a></li>
							<li><a href="breast-cancer-treatment">Breast Cancer</a></li>
							<li><a href="cervical-cancer-treatment">Cervical Cancer</a></li>
							<li class="tab-active"><a href="prostate-cancer-treatment">Postate Cancer</a></li>
							<li><a href="colon-cancer-treatment">Colon Cancer</a></li>
							<li><a href="cyberknife-radiation-therapy">Cyberknife
									Radiation Therapy</a></li>
						</ul>
					</div>
				</div>
				<!-- End Column -->
				<div class="col-md-9 tabnopaddleft">
					<div class="accordion-contentarea">
<div id="tab4">
							<h2>
								<span>Prostate Cancer Treatment</span> in India
							</h2>
							<p>We at Arvene Healthcare have the expertise to organize for
								all treatments and surgeries related to Prostrate Cancer. Arvene
								Healthcare is associated with top hospitals and Doctors of India
								in this field. We can give you more and better options so that
								you can take the right decisions. We also assist you in taking
								best decision based on your requirement. Arvene Healthcare will
								also arrange for your stay and local travel, so that you have
								nothing else to worry about but your treatment. Please contact
								us on <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a> today to avail best services.</p>

							<h3>
								<span>What is </span>Prostate Cancer?
							</h3>
							<div class="row">
								<div class="col-md-9 col-sm-9 nopaddleft">
									<p>The cancer that takes place in the prostate of a man is
										known as prostate cancer. Prostate is a small walnut-shaped
										gland that is responsible for producing seminal fluid. This
										cancer is most common type of cancer in men that develops
										slowly and remains limited to the prostate gland. This cancer
										is most commonly seen in older men over 50 years of age.</p>
									<h3>
										<span>Types of </span>Prostate Cancer
									</h3>
								</div>
								<!-- End Inner Column -->
								<div class="col-md-3 col-sm-3 nopaddright">
									<img src="images/specialities/20.prostate cancer.jpg"
										alt="prostate cancer.jpg" class="img-responsive">
								</div>
								<!-- End Inner Column -->
							</div>
							<!-- End Inner Row -->
							<ul>
								<li><span><strong> Small Cell Carcinoma: </strong>
										This is considered as the rare type of prostate cancer that
										firstly begins in specialized cells inside the prostate. </span></li>
								<li><span><strong> Adenocarcinoma: </strong>
										Adenocarcinoma is considered as the most common type of
										prostate cancer. This type of prostate cancer can spread to
										different areas (such as bones, lymph nodes or other organs)
										beyond the prostate. This is a slow growing type of prostate
										cancer. </span></li>
							</ul>
							<h3>
								<span>Causes of </span>Prostate Cancer
							</h3>
							<p>The advancement in age can increase the chances of
								developing prostate cancer</p>
							<h3>
								<span>Symptoms of </span>Prostate Cancer
							</h3>
							<ul>
								<li><span>Blood in semen or urine</span></li>
								<li><span>Frequent urination, especially at night</span></li>
								<li><span>Inability to urinate while standing up</span></li>
								<li><span>Difficulty in stopping or starting a
										stream of urine</span></li>
								<li><span>Leaking of urine while coughing or
										laughing</span></li>
								<li><span>A burning or painful sensation at the time
										of ejaculation or urination</span></li>
								<li><span>An interrupted or weak urinary stream</span></li>
							</ul>
							<h3>
								<span>Symptoms of </span>Advanced Prostate Cancer
							</h3>
							<ul>
								<li><span>Paralysis or weakness in the lower limbs,
										usually with constipation</span></li>
								<li><span>Stiffness, deep or dull pain in the upper
										thighs, pelvis, lower back or ribs. This is often followed by
										pain in the bones of those areas</span></li>
								<li><span>Swelling of the lower extremities</span></li>
								<li><span>Fatigue, vomiting or nausea</span></li>
								<li><span>Loss of appetite and weight</span></li>
							</ul>
							<h3>
								<span>Stages of </span>Prostate Cancer
							</h3>

							<h3>
								<span>Diagnosis of </span>Prostate Cancer
							</h3>
							<p>A number of tests are performed for diagnosing prostate
								cancer that include -</p>
							<p>
								<strong> Biopsy: </strong>
							</p>

							<p>
								<strong> Digital Rectal Exam (DRE): </strong>
							</p>
							<p>
								<strong> Transrectal Ultrasound: </strong>
							</p>
							<p>
								<strong> Prostate-Specific Antigen (PSA) Test: </strong>
							</p>
							<h3>
								<span>Treatment of </span>Prostate Cancer
							</h3>
							<p>A number treatment options are available for prostate
								cancer that include -</p>
							<h3>
								<span>Hormone </span>Therapy
							</h3>
							<p>The male sex hormones may result in the growth of prostate
								cancer. Certain hormones, drugs or surgeries are used for
								blocking the working of the hormone or for reducing the
								production of male hormones</p>

							<h3>Surgery</h3>
							<p>Surgery is recommended for those people who are good in
								overall health. A surgery can be categorized into different
								types -</p>
							<ul>
								<li><span><strong>Transurethral Resection
											of the Prostate (TURP): </strong> The removal of tissue from the
										prostate is done by inserting a light and thin tube having a
										cutting tool (resectoscope) through the urethra. </span></li>
								<li><span><strong>Radical Prostatectomy: </strong>
										The removal of prostate that is surrounded by seminal vesicles
										and tissue is done in this surgical procedure. </span></li>


								<li><span><strong> Pelvic Lymphadenectomy:
									</strong> The removal of lymph nodes in the pelvis is done in this
										surgical procedure</span></li>
							</ul>
							<h3>
								<span>Radiation </span>Therapy
							</h3>
							<p>High-energy x-rays are used in this therapy for destroying
								cancer cells.</p>
<h5>Cost of Prostate Cancer treatment in India starts at 9000 US Dollars. To know more about it please share your reports on <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a> or whatsapp/viber/imo to +918130077375.</h5><p> 
(Please note this is a tentative plan and actual Costs are based on reports shared. Final treatment plan and estimation will be provided after the patient has been evaluated.)</p>

							<h5>
								To get best options for cancer treatment, send us a query or
								write to us at <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
							</h5>
						</div>
						<!-- End Tab -->
						
					</div>
				</div>
				<!-- End Column -->
			</div>
			<!-- End Row -->
		</div>
		<!-- End Tab Accordion -->

	</div>
	<!-- End Container -->
</div>
<!-- Speciality Content Ends Here -->

<%@ include file="newFooter.jsp"%>
<%@ include file="newHeader.jsp"%></section>



<section class="sec-main sec-drl">
	<div class="container">
		<div class="drl-bgwhite conn-bttn">
			<div class="row">
				 <div class="col-lg-6">
                           <!--  <button class="acc"> Queries</button> -->
                        </div>
                        <div class="col-lg-6">
                            <a href="queries" class="acc pull-right col-lin"  >
                           		View Queries
                            	
                            </a>
                        </div>
			</div>
		</div>
		<c:choose>
			<c:when test="${fn:length(queries) == 0}">
				<h4 class="list-group-item-text">You have no queries as of now.</h4>
			</c:when>
			<c:otherwise>

				<div class="drl-bgwhite uqt-main">
					<div class="table-responsive">
						<table class="table table-condensed  table-striped ">
							<c:choose>
								<c:when test="${userType == 'U'}">
									<thead class="uqt-tab-head">
										<tr>
											<th>Sr.No</th>
											<th>Patient Details</th>
											<th>Doctor Name</th>
											<th class="w-th">Query</th>
											<th>Status</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>

										<c:forEach items="${queries}" var="query"
											varStatus="queryIndex">
											<tr>
												<td>${start + queryIndex.index +1 }</td>
												<td>Patient Name - ${query.patientName} <br /> Age-
													${query.patientAge}<br /> Gender - ${query.patientGender}<br />
													Phone - ${query.patientPhone}
												</td>
												<td>${query.specialistDetails.firstName
													}&nbsp;${query.specialistDetails.lastName }</td>

												<td>
													<%--  <c:if test="${conIndex.index== 0 }"><div>${conversation.text} </div></c:if> --%>
													
													
													<c:forEach items="${query.conversations}"
															var="conversation" varStatus="conIndex1"> 
															<c:if test="${conIndex1.index eq 0 }">	${conversation.text}</c:if>
													</c:forEach>

													<div class="tab-col collapse" id="col-${ query.id}">

														<c:forEach items="${query.conversations}"
															var="conversation" varStatus="conIndex">

															<c:choose>
																<c:when test="${conversation.type eq 'S' }">
																	<h3>Doctor's response:</h3>
																</c:when>
															</c:choose>															
															<div>${conversation.text}</div>
															<c:forEach items="${conversation.reports}" var="report"
																varStatus="reportIndex">
																<div>
																	<a href="${report.url}" class="col-lin">${report.fileName}</a>
																</div>	
															</c:forEach>
															<hr />
														</c:forEach>

													</div>

												</td>
												<td><c:choose>
														<c:when test="${query.responded eq '0'}">
														Response Awaited. 
													</c:when>
														<c:otherwise>
														Responded!
													</c:otherwise>
													</c:choose></td>
												<td><span class="fa fa-eye marg-uqt-span"
													data-toggle="collapse" data-target="#col-${query.id}"></span> <span
													class="fa fa-edit marg-uqt-span"></span></td>
											</tr>
										</c:forEach>

									</tbody>
								</c:when>
								<c:when test="${userType == 'S'}">
								
								 <thead class="uqt-tab-head">
                                <tr>
                                    <th>Sr.No</th>
                                    <th>Patient Details</th>
                                    <th class="w-th">Query</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            <c:forEach items="${queries}" var="query"
											varStatus="queryIndex">
                                <tr>
                                    <td>1</td>
                                    <td>Patient Name - ${query.patientName} <br /> Age-
													${query.patientAge}<br /> Gender - ${query.patientGender}<br />
													Phone - ${query.patientPhone}</td>
                                    <td>
                                        	<c:forEach items="${query.conversations}"
															var="conversation" varStatus="conIndex1"> 
															<c:if test="${conIndex1.index eq 0 }">	${conversation.text}</c:if>
													</c:forEach>

													<div class="tab-col collapse" id="col-${ query.id}">

														<c:forEach items="${query.conversations}"
															var="conversation" varStatus="conIndex">
	
															<c:choose>
																<c:when test="${conversation.type eq 'S' }">
																	<h4>Doctor's response:</h4>
																</c:when>
															</c:choose>
															
															<div>${conversation.text}</div>
															<c:forEach items="${conversation.reports}" var="report"
																varStatus="reportIndex">
																<div>
																	<a href="${report.url}" class="col-lin">${report.fileName}</a>
																</div>
															</c:forEach>
															<hr />
														</c:forEach>
												
													</div>
                                    </td>
                                    <td>
                                    	<c:choose>
												<c:when test="${query.responded eq '0'}">
														Response Awaited. 
													</c:when>
														<c:otherwise>
														Responded!
													</c:otherwise>
													</c:choose>
									</td>
                                    <td>
                                        <span class="fa fa-eye marg-uqt-span"  data-toggle="collapse" data-target="#col-${ query.id}"></span>
                                    </td>
                                </tr>
                                </c:forEach>
                                </tbody>
								</c:when>
							</c:choose>


						</table>



					</div>

				</div>

			</c:otherwise>
		</c:choose>
		<div class="row">
			<div class="col-lg-12">
				<div class="drl-btn">
					<div id="pagination"></div>
				</div>
			</div>
		</div>
	</div>
</section>

<script type='text/javascript'>
var options = {
		
		currentPage : '${currentpage}',
		totalPages : '${pageno}',
		onPageClicked : function(e, originalEvent, type, page) {
			fetchqueries(page)
		},
		size:'medium',
		alignment : 'center'
	};
	$('#pagination').bootstrapPaginator(options);

	function fetchqueries(page) {
		var start = (page * 10) - 10;
		window.location.href="queries?start="+start+"&range=10";
	}
	$('.nav >li.active').removeClass("active");
	$('.nav #queries').addClass("active");

</script>
<script>
  		 function viewQuery(queryId){
  			var shortQueryDiv = document.getElementById("shortQuery-"+queryId);
  			var queryReplyDiv = document.getElementById("query-reply-"+queryId);
  				
  			 if(shortQueryDiv.style.display == 'block'){
   				shortQueryDiv.style.display = 'none';
  			 }
             else{
            	 shortQueryDiv.style.display = 'block';
             }
  			 
  			 if(queryReplyDiv.style.display == 'block'){
  				queryReplyDiv.style.display = 'none';
   			 }
              else{
            	  queryReplyDiv.style.display = 'block';
              }
  			
  		}  	
  </script>
<%@ include file="newFooter.jsp"%>



<%@ include file="newHeader.jsp"%></section>
	<!-- Hospital Short Details Starts Here -->
	<div class="short-details">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="title">
						<h1>Urology Treatment</h1>
						<ul class="paginator">
							<li><a href="/">Home</a></li>
							<li><a>Urology Treatment</a></li>
						</ul>
					</div>
				</div>
				<!-- End Column -->
			</div>
			<!-- End Row -->
		</div>
		<!-- End Container -->
	</div>
	<!-- Hospital Short Details Ends Here -->

	<!-- Specialty Content Starts Here -->
	<div class="speciality-content-area">
		<div class="container">

			<div class="innertab-accordion">
				<div class="row">
					<div class="col-md-3 tabnopaddright">
						<div class="tabs">
							<ul>
								<li><a href="urology-treatment">Urology
										Treatment</a></li>
								<li><a href="endoscopic-surgery">Endoscopy surgery</a></li>
								<li><a href="TURP-surgery">TURF</a></li>
								<li class="tab-active"><a href="radical-prostatectomy-surgery">Prostactomy</a></li>
							</ul>
						</div>
					</div>
					<!-- End Column -->
					<div class="col-md-9 tabnopaddleft">
						<div class="accordion-contentarea">

							<div id="tab4" >
								<h2>
									<span>Radical Prostatectomy Surgery</span>� India
								</h2>
								<div class="row">
									<div class="col-md-9 col-sm-9 nopaddleft">
										<p>We at Arvene Healthcare have the expertise to organize for all treatments and surgeries related to Urology. Arvene Healthcare is associated with top hospitals and Doctors of India in this field. We can give you more and better options so that you can take the right decisions. We also assist you in taking best decision based on your requirement. Arvene Healthcare will also arrange for your stay and local travel, so that you have nothing else to worry about but your treatment. Please contact us on <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a> today to avail best services.</p><br>
										<h3>
									    Prostatectomy:
								         </h3>
										<p>
										is the surgical process of removing the entire or part of the prostrate gland due to abnormalities such as formation of tumor and enlargement mainly in the urethra. In radical Prostatectomy, along with the prostrate gland several other tissues around it is removed. It is one of the invasive ways of treating prostrate cancer. There are two ways of doing it, open surgery and Laparoscopic incisions. Laparoscopic radical Prostatectomy is also popular as robot-assisted Prostatectomy.
										</p>
										
									</div>
									<!-- End Inner Column -->
									<div class="col-md-3 col-sm-3 nopaddright">
										<img src="images/specialities/52. Prostatectomy.jpg" alt="Prostatectomy"
											class="img-responsive">
									</div>
									<!-- End Inner Column -->
								</div>
								<!-- End Inner Row -->
								<h3>
								<span>Procedure for Radical </span>Prostatectomy
								</h3>
								<p>Radical Prostatectomy is more often done for patients in the stage I and II of prostrate cancer. In state III, this surgery relieves the patient of urinary obstructions.</p><br>
								<p>In the main course open surgery, there is an incision made in either the lower belly or the perineum to reach the affected prostrate gland. In many cases, the surgeon also removes the affected lymph nodes. In the Laparoscopic form of radical Prostatectomy, incisions are made in the belly and the laparoscope is inserted through one of the incisions and medical instruments through the other.</p>
								<h3>
									<span>Robotic </span>Prostatectomy
								</h3>
								<p>Robotic-assisted laparoscopic prostatectomy is considered as one of the most innovative ways for treating prostate cancer. The procedure involves the removal of the prostate gland. As compared to traditional prostate cancer surgery, robotic-assisted laparoscopic provides better and effective outcomes. Also there are less chances of urinary discontinuance and erectile dysfunction (ED) as there is greater nerve sparing. After the procedure, a patient is required to stay in the hospital for a short duration and also there are fewer chances of postoperative pain and infection.</p>
Prostate removal (enlarged prostate) surgery <h5>Cost in India starts at 4900 US Dollars. To know more about it please share your reports on <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a> or whatsapp/viber/imo to +918130077375.</h5><p> 
(Please note this is a tentative plan and actual Costs are based on reports shared. Final treatment plan and estimation will be provided after the patient has been evaluated.)</p>
								<h5>
									To get best options for Urology treatment, send us a query or
									write to us at <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
								</h5>
							</div>
							<!-- End Tab -->
						</div>
					</div>
					<!-- End Column -->
				</div>
				<!-- End Row -->
			</div>
			<!-- End Tab Accordion -->

		</div>
		<!-- End Container -->
	</div>
	<!-- Speciality Content Ends Here -->

<%@ include file="newFooter.jsp"%>
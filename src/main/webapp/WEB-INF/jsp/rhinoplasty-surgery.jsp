<%@ include file="newHeader.jsp"%></section>
<!-- Hospital Short Details Starts Here -->
<div class="short-details">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="title">
					<h1>Cosmetic Surgery</h1>
					<ul class="paginator">
						<li><a href="/">Home</a></li>
						<li><a>Cosmetic Surgery</a></li>
					</ul>
				</div>
			</div>
			<!-- End Column -->
		</div>
		<!-- End Row -->
	</div>
	<!-- End Container -->
</div>
<!-- Hospital Short Details Ends Here -->

<!-- Specialty Content Starts Here -->
<div class="speciality-content-area">
	<div class="container">

		<div class="innertab-accordion">
			<div class="row">
				<div class="col-md-3 tabnopaddright">
					<div class="tabs">
						<ul>
							<li><a href="cosmetic-surgery">Cosmetic Surgery</a></li>
							<li><a href="breast-augmentation-surgery">Breast
									Augmentation</a></li>
							<li><a href="breast-reduction-surgery">Breast Reduction</a></li>
							<li><a href="liposuction-surgery">Liposuction</a></li>
							<li><a href="facelift-surgery">Facelift</a></li>
							<li class="tab-active"><a href="rhinoplasty-surgery">Rhinoplasty</a></li>
							<li><a href="cosmetic-laser-surgery">Cosmetic Laser
									Surgery</a></li>
							<li><a href="oral-maxillofacial-surgery">Maxillofacial
									Surgery</a></li>
							<li><a href="surgical-hair-transplant">Surgical Hair
									Transplant</a></li>
						</ul>
					</div>
				</div>
				<!-- End Column -->
				<div class="col-md-9 tabnopaddleft">
					<div class="accordion-contentarea">

						<div id="tab6">
							<h2>
								<span>Rhinoplasty Surgery</span> in India
							</h2>
							<div class="row">
								<div class="col-md-9 col-sm-9 nopaddleft">
									<p>We at Arvene Healthcare have the expertise to organize
										for all treatments and surgeries related to Rhinoplasty
										Surgery. Arvene Healthcare is associated with top hospitals
										and Doctors of India in this field. We can give you more and
										better options so that you can take the right decisions. We
										also assist you in taking best decision based on your
										requirement. Arvene Healthcare will also arrange for your stay
										and local travel, so that you have nothing else to worry about
										but your treatment. Please contact us on
										<a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a> today to avail best services.</p>
								</div>
								<!-- End Inner Column -->
								<div class="col-md-3 col-sm-3 nopaddright">
									<img src="images/specialities/13.rhinoplasty.jpg"
										alt="rhinoplasty" class="img-responsive">
								</div>
								<!-- End Inner Column -->
							</div>
							<!-- End Inner Row -->
							<h3>
								<span>What is </span>Rhinoplasty?
							</h3>
							<p>Rhinoplasty surgery (nose surgery), is also referred as
								'nose reshaping' or a 'nose job'. The procedure of rhinoplasty
								(reconstructive nose surgery) is done to enhance the shape of
								the nose, or reshaping of the nose tip and also to narrow down
								the nostril spans. People with respiratory impediment,
								congenital defect or any other form of nasal trauma can also opt
								for rhinoplasty surgery. More and more people are opting for
								this surgery for a perfect nose.</p>

							<h3>
								<span>Who Should Consider </span>Rhinoplasty (Nose Surgery)?
							</h3>
							<ul>
								<li><span>Candidates who are non-smokers</span></li>
								<li><span>Candidates who are physically healthy</span></li>
								<li><span>Candidates who aspire for realistic goals</span></li>
								<li><span>Candidates whose facial growth is complete
										and are about 13 years of age or older</span></li>
							</ul>
							<h3>
								<span>Rhinoplasty </span>Techniques
							</h3>
							<ul>
								<li><span>Revision Rhinoplasty</span></li>
								<li><span>Nose tip Rhinoplasty</span></li>
								<li><span>Augmentation Rhinoplasty</span></li>
								<li><span>Reduction Rhinoplasty</span></li>
								<li><span>Hump Nose Reduction</span></li>
								<li><span>Alar Plasty/ Alar reduction</span></li>
								<li><span>Post Cleft Lip Nasal Deformity</span></li>
								<li><span>Deviated deformed Nose/ Corrective
										Rhinoplasty</span></li>
							</ul>

							<h3>
								<span>Procedure of</span> Rhinoplasty Surgery
							</h3>
							<p>The surgery is performed under local or general
								anaesthesia. The recovery may take few weeks to heal completely.
								The surgeon prescribes medication such as antibiotics,
								analgesics and steroids to aid wound healing. The external cast
								can be removed after a week.</p>
							<h3>
								Rhinoplasty Surgery <span> expectations</span>
							</h3>
							<ul>
								<li><span>A proper nose size proportionate to other
										facial features</span></li>
								<li><span>Can treat large, upturned or wide nostrils</span></li>
								<li><span>Can improve the nose width at the bridge</span></li>
								<li><span>Can improve the nose profile having
										visible humps on the bridge</span></li>
								<li><span>Can improve nasal asymmetry and its
										deviation</span></li>
								<li><span>Can improve the shape of the nasal tip or
										upturned tip</span></li>
							</ul>
							<h3>
								<span>Cost of </span>Rhinoplasty Surgery
							</h3>
							<p>The best part of getting a rhinoplasty cosmetic surgery in
								India, is that it is much less as compared to any other western
								country. Low cost treatment in India does not affect the quality
								standard of the treatment, as India has best and latest medical
								facilities in the world.</p>
							<h5>
								To get best options for cosmetic treatment, send us a query or
								write to us at <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
							</h5>
						</div>
						<!-- End Tab -->
						

					</div>
				</div>
				<!-- End Column -->
			</div>
			<!-- End Row -->
		</div>
		<!-- End Tab Accordion -->

	</div>
	<!-- End Container -->
</div>
<!-- Speciality Content Ends Here -->

<%@ include file="newFooter.jsp"%>
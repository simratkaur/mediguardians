<%@ include file="newHeader.jsp"%></section>
<!-- Hospital Short Details Starts Here -->
<div class="short-details">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="title">
					<h1>Orthopedic Surgery</h1>
					<ul class="paginator">
						<li><a href="/">Home</a></li>
						<li><a>Orthopedic Surgery</a></li>
					</ul>
				</div>
			</div>
			<!-- End Column -->
		</div>
		<!-- End Row -->
	</div>
	<!-- End Container -->
</div>
<!-- Hospital Short Details Ends Here -->

<!-- Specialty Content Starts Here -->
<div class="speciality-content-area">
	<div class="container">

		<div class="innertab-accordion">
			<div class="row">
				<div class="col-md-3 tabnopaddright">
					<div class="tabs">
						<ul>
							<li><a href="orthopaedics-surgery">Orthopedic
									Surgery</a></li>
							<li><a href="knee-replacement-surgery">Knee Replacement</a></li>
							<li><a href="hip-replacement-surgery">Hip Replacement
									surgery </a></li>
							<li class="tab-active"><a href="shoulder-surgery">Shoulder Surgery </a></li>
							<li><a href="foot-ankle-surgery">Foot and Ankle surgery</a></li>
							<li><a href="hand-wrist-surgery">Hand Wrist surgery</a></li>
							<li><a href="elbow-replacement-surgery">Elbow Surgery</a></li>
						</ul>
					</div>
				</div>
				<!-- End Column -->
				<div class="col-md-9 tabnopaddleft">
					<div class="accordion-contentarea">
<div id="tab4">
							<h2>
								<span>Shoulder Surgery </span>-India
							</h2>
							<div class="row">
								<div class="col-md-9 col-sm-9 nopaddleft">
									<p>We at Arvene Healthcare have the expertise to organize
										for all treatments and surgeries related to Shoulder Surgery.
										Arvene Healthcare is associated with top hospitals and Doctors
										of India in this field. We can give you more and better
										options so that you can take the right decisions. We also
										assist you in taking best decision based on your requirement.
										Arvene Healthcare will also arrange for your stay and local
										travel, so that you have nothing else to worry about but your
										treatment. Please contact us on <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
										today to avail best services.</p>
									<h3>Shoulder Joint</h3>
								</div>
								<!-- End Inner Column -->
								<div class="col-md-3 col-sm-3 nopaddright">
									<img src="images/specialities/34.shoulder surgery.jpg"
										alt="shoulder surgery" class="img-responsive">
								</div>
								<!-- End Inner Column -->
							</div>
							<!-- End Inner Row -->
							<p>Shoulder Joint is a ball and socket joint similar to the
								hip joint and has the highest mobility among any other joints.
								It consists of three bones- the scapula (shoulder blade), the
								clavicle (upper arm bone) and the humerus (collarbone).</p>
							<h3>
								<span>Why Shoulder </span>Joint Replacement?
							</h3>
							<p>People who are recommended with shoulder replacement
								surgery often suffer from:</p>
							<ul>
								<li><span>Severe shoulder pain that disrupts normal
										day to day activities. </span></li>
								<li><span>Perpetual pain while sleeping. Disruption
										in sleep.</span></li>
								<li><span>Severe weakness in the shoulder joint and
										loss of motion.</span></li>
								<li><span>Failure of medical treatment and
										physiotherapy.</span></li>
							</ul>
							<h3>
								<span>Treatment Options for </span>Shoulder Surgery and Injuries
							</h3>
							<p>Depending upon patient condition and proper diagnosis the
								surgeon will recommend one of the following treatments. There
								are several options available for Shoulder surgery.</p>
							<p>
								<strong>1. Shoulder Replacement</strong>
							</p>
							<p>This is the last option left when medicines have failed.
								Shoulder joint is replacement with a prosthetic joint. This
								particular surgery is recommended to increase motion and repair
								painful arthritic shoulders. There are two types of Shoulder
								Replacement- Total (both sides of the joint are replaced) and
								Partial (only the ball part of the shoulder is replaced)</p>
							<br>
							<p>
								<strong>2. Rotator Cuff</strong>
							</p>
							<p>The most common cause of pain and disability in the
								shoulders of the adult is Rotator cuff tear. Here the Rotator
								cuff is repaired depending on the size, shape and location of
								the tear. If the tear is partial, procedure is called
								debridement.</p>
							<p>
								<strong>3. Shoulder Arthroscopy</strong>
							</p>
							<p>Performing shoulder surgery through minor incisions with
								the help of the arthroscope. The surgeon uses the arthroscope to
								visualize the internal parts of the joint. A camera is fixed at
								the end of the arthroscope and a very bright fibre optic is
								attached to the video monitor in the operating room, allowing a
								surgeon to glimpse the inside of the shoulder joint.</p>
							<h3>
								<span>Shoulder Surgery Benefits &amp; </span>Post Care
							</h3>
							<p>Improved mobility, flexibility and elimination of pain in
								the shoulder joint are the results of shoulder replacement
								procedures. Plus in India the benefit is lower cost treatment,
								advanced infrastructure and state of the art equipment.</p>
							<br>
							<p>After Shoulder Replacement Surgery, patient is required to
								wear an arm sling for a few days. The arm movement will depend
								upon the specific instructions given by the doctors. In 2 to 3
								months, a patient will get back to the normal routine activities
								post physiotherapy.</p>
<h5>Cost of Shoulder surgery in India starts at 5000 US Dollars. To know more about it please share your reports on <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a> or whatsapp/viber/imo to +918130077375.</h5><p> 
(Please note this is a tentative plan and actual Costs are based on reports shared. Final treatment plan and estimation will be provided after the patient has been evaluated.)</p>
							<h5>
								To get best options for Orthopaedic treatment, send us a query
								or write to us at <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
							</h5>
						</div>
						<!-- End Tab -->

					</div>
				</div>
				<!-- End Column -->
			</div>
			<!-- End Row -->
		</div>
		<!-- End Tab Accordion -->

	</div>
	<!-- End Container -->
</div>
<!-- Speciality Content Ends Here -->

<%@ include file="newFooter.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:set var="contextPath" value="${pageContext.request.contextPath}" />

<link rel="stylesheet" href="${contextPath}/css/bootstrap.min.css" />
<link rel="stylesheet" href="${contextPath}/css/bootstrap.css" />
<link rel="stylesheet"
	href="${contextPath}/css/bootstrap-responsive.min.css">
<link rel="stylesheet" href="${contextPath}/css/sl-slide.css">
<link rel="stylesheet" href="${contextPath}/css/bootstrap-dialog.css">
<link rel="stylesheet" href="${contextPath}/css/select2.css">
<link rel="stylesheet" href="${contextPath}/css/select2-bootstrap.css">
<link rel='stylesheet' href='${contextPath}/css/fullcalendar.css' />
<link rel='stylesheet' href='${contextPath}/css/jquery-ui.css' />
<link rel='stylesheet' href='${contextPath}/css/bootstrap-tagsinput.css' />
<link rel='stylesheet' href='${contextPath}/css/file-validator.css' />



<script src="${contextPath}/js/bootstrap.min.js"></script>
<script src="${contextPath}/js/main.js"></script>
<!-- Required javascript files for Slider -->
<script src="${contextPath}/js/jquery.ba-cond.min.js"></script>
<script src="${contextPath}/js/jquery.slitslider.js"></script>
<script src="${contextPath}/js/bootstrap-dialog.js"></script>
<script src="${contextPath}/js/select2.min.js"></script>
<script src="${contextPath}/js/moment.js"></script>
<script src="${contextPath}/js/fullcalendar.js"></script>
<script src="${contextPath}/js/bootstrap-paginator.js"></script>
<script src="${contextPath}/js/bootstrap-tagsinput.js"></script>
<script src="js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
<script src="js/jquery.form-validator.min.js"></script>
<script src="js/jquery.form-validator.js"></script>
<script src="js/file-validator.js"></script>





<script>
	function validateTime(startId, endId) {
		var startTime = $("#" + startId).val();
		var endTime = $("#" + endId).val();
		var timepattern = new RegExp("^(0[0-9]|1[0-9]|2[0-4]):[0-5][0-9]$");
		var res = timepattern.test(startTime);
		var res1 = timepattern.test(endTime);
		if (res && res1) {
			startDate = new Date();
			endDate = new Date();
			endDate.setTime(startDate.getTime());
			startDate.setHours(startTime.split(":")[0]);
			startDate.setMinutes(startTime.split(":")[1]);
			endDate.setHours(endTime.split(":")[0]);
			endDate.setMinutes(endTime.split(":")[1]);
			if (startDate < endDate) {
				var splits = startId.split("_");
				while (startDate < endDate) {
					$('#' + splits[0] + '_sessions')
							.tagsinput(
									'add',
									startDate.getHours() + ":"
											+ startDate.getMinutes());
					startDate.setMinutes(startDate.getMinutes() + 15);
				}
			} else
				alert('Invalid Date Range');
		} else {
			alert('Please enter valid time');
		}
		$("#" + startId).val('');
		$("#" + endId).val('');
	}

	$(function() {
		$(".timePicker").timepicker({
			stepMinute : 15
		});
		$(".datepick").datepicker({
			changeMonth : true,
			changeYear : true,
			yearRange : '-100:+0',
			showButtonPanel : true,
			dateFormat : 'dd/mm/yy',
			beforeShow : function(input, inst) {
				var cal = inst.dpDiv;
				var top = $(this).offset().top + $(this).outerHeight();
				var left = $(this).offset().left;
				setTimeout(function() {
					cal.css({
						'top' : top,
						'left' : left
					});
				}, 10);
			}
		})
		var max = new Date().getFullYear();
		for (var i = max - 50; i <= max; i++) {
			var opt = document.createElement('option');
			opt.value = i;
			opt.innerHTML = i;
			$(".yearpick").append(opt);
		}
	});
</script>


<!-- Added by Madhu - ENDS -->
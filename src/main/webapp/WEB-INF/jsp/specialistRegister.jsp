<%@ include file="newHeader.jsp"%></section>
<%@page import="com.medicine.arvene.util.CommonUtil"%>
<link rel="stylesheet" href="css/intlTelInput.css">
<script src="js/intlTelInput.js"></script>

<script type="text/javascript">
	window.onload = function() {
		$("#phone1").intlTelInput();
	}
	
	function updatePhone(){
		
		var ph1 = $("#phone1").val();
		var ph = $("#phone1").intlTelInput("getSelectedCountryData");
		var completePhone = "+"+ph.dialCode+ph1;		
		 $("#phone").val(completePhone);		
	}
	
	
	function Submit() {
		
		
		var x = document.getElementById("newProfileImage");
		if ('files' in x) {
			if (x.files.length != 0) {
				var file = x.files[0];
				if ('name' in file) {
					/* alert("4"); */
					/* console.log(file); */
					var ext = file.name
							.substring(file.name.lastIndexOf('.') + 1);
					if (ext != "jpg" && ext != "jpeg" && ext != "bmp"
							&& ext != "gif" && ext != "png") {
						$("#result")
								.html(
										"<div class='col-lg-12 col-md-12'><div class='danger-main'><div class='danger'><span class='fa fa-close pull-right clo'></span><p>Can upload only .jpg, .jpeg, .bmp, .gif, .png extensions "
												+ ext
												+ "</p></div></div></div>");
						popUp();
						return false;
					}
				}
				if ('size' in file && file.size > 400000) {
					$("#result")
							.html(
									"<div class='col-lg-12 col-md-12'><div class='danger-main'><div class='danger'><span class='fa fa-close pull-right clo'></span><p>File size is greater then expected</p></div></div></div>")
					popUp();
					return false;
				}
			} else {
				$("#result")
						.html(
								"<div class='col-lg-12 col-md-12'><div class='danger-main'><div class='danger'><span class='fa fa-close pull-right clo'></span><p>Profile Picture is mandatory</p></div></div></div>")
				popUp();
				return false;
			}
		}
	}
	function myAgreement() {
		window
				.open(
						"service_agreement",
						"Service Agreement",
						"width=800, height=500,top=100,left=300,location=no,menubar=no,scrollbars=yes,toolbar=no,directories=0,titlebar=0,toolbar=0,location=0,status=0,menubar=0");
	}
</script>
<script>
(function () {
	
	var selDiv = "";
    
    document.addEventListener("DOMContentLoaded", init, false);
    
    function init() {
        document.querySelector('#newProfileImage').addEventListener('change', handleFileSelect, false);
        selDiv = document.querySelector("#selectedFiles");
    }
        
    function handleFileSelect(e) {
    	
    	console.log(e);
        
        if(!e.target.files || !window.FileReader) return;

        selDiv.innerHTML = "";
        
        var files = e.target.files;
        var filesArr = Array.prototype.slice.call(files);
        
        console.log(filesArr);
        
        
        
        filesArr.forEach(function(f,i) {
        	if(!f.type.match("image.*")) {
                return;
            }

            var reader = new FileReader();
            reader.onload = function (e) {
                /* var html = "<div class='col-sm-3'><img class='preview-img img-circle' src=\"" + e.target.result + "\"></div>" + f.name + "<br clear=\"left\"/>"; */
                var html = "<div class='col-sm-offset-3 col-sm-6'><img class='preview-img img-circle' src=\"" + e.target.result + "\"></div>";

                selDiv.innerHTML += html;               
            }
            reader.readAsDataURL(f);  
        });
        
    }
	
})();

</script>

<section class="sec-main">
	<div class="container">
		<div class="row">


			<div class="col-lg-12">
				<div class="top-heading-s">
					<h4>Sign Up With Arvene Healthcare</h4>
				</div>
			</div>

			<!--------------------search barr End--------------------->
			<!--------------------search barr End--------------------->

			<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">

				<form:form id="docRegisterForm" action="specialistRegister"
					method="POST" role="form" modelAttribute="specialistRegisterForm"
					enctype="multipart/form-data">

					<div class="row  form-group   has-feedback">
						<div class="col-lg-5 col-md-5 col-sm-4 col-xs-4">
							<div class="sig-label">
								<label class="control-label">First Name</label>
							</div>
						</div>
						<div class="col-lg-7 col-md-7 col-sm-8 col-xs-8">
							<div class="sig-input">
								<!--  <input class="form-control" type="text"> -->
								<form:input type="text" class="form-control" path="firstName" maxlength="45"/>
							</div>
						</div>
					</div>


					<div class="row ">
						<div class="col-lg-5 col-md-5 col-sm-4 col-xs-4">
							<div class="sig-label">
								<label class="control-label">Middle Name</label>
							</div>
						</div>
						<div class="col-lg-7 col-md-7 col-sm-8 col-xs-8">
							<div class="sig-input">
								<form:input type="text" class="form-control" path="middleName"
									maxlength="45" />
							</div>
						</div>
					</div>
					<div class="row   form-group   has-feedback">
						<div class="col-lg-5 col-md-5  col-sm-4 col-xs-4">
							<div class="sig-label">
								<label class="control-label">Last Name</label>
							</div>
						</div>
						<div class="col-lg-7 col-md-7 col-sm-8 col-xs-8">
							<div class="sig-input">
								<form:input class="form-control" type="text" path="lastName"
									data-validation="required alpha" maxlength="45"
									data-validation-error-msg="Last Name has to be alphabets only" />
							</div>
						</div>
					</div>
					<div class="row  form-group   has-feedback">
						<div class="col-lg-5 col-md-5 col-sm-4 col-xs-4">
							<div class="sig-label">
								<label class="control-label">Gender</label>
							</div>
						</div>

						<div class="col-lg-7 col-md-7 col-sm-8 col-xs-8">
							<div class="sig-input">

								<form:radiobutton id="male" path="gender" value="M"
									checked="true" />
								&nbsp;Male &nbsp;
								<form:radiobutton id="female" path="gender" value="F" />
								&nbsp;Female &nbsp;
							</div>
						</div>
					</div>
					<div class="row  form-group   has-feedback">
						<div class="col-lg-5 col-md-5  col-sm-4 col-xs-4">
							<div class="sig-label">
								<label class="control-label">Email</label>
							</div>
						</div>
						<div class="col-lg-7 col-md-7 col-sm-8 col-xs-8">
							<div class="sig-input">
								<form:input class="form-control" type="text" path="username"
									data-validation="required" maxlength="45"/>
							</div>
						</div>
					</div>
					<div class="row  form-group   has-feedback">
						<div class="col-lg-5 col-md-5 col-sm-4 col-xs-4">
							<div class="sig-label">
								<label class="control-label">Password</label>
							</div>
						</div>
						<div class="col-lg-7 col-md-7 col-sm-8 col-xs-8">
							<div class="sig-input">
								<form:input class="form-control" type="password" id="password"
									name="password" path="password" maxlength="45"
									data-validation="required length" data-validation-length="min8" />
							</div>
						</div>
					</div>
					<!--  Confirm password -->
					<div class="row  form-group   has-feedback">
						<div class="col-lg-5 col-md-5 col-sm-4 col-xs-4">
							<div class="sig-label">
								<label class="control-label">Confirm Password</label>
							</div>
						</div>
						<div class="col-lg-7 col-md-7 col-sm-8 col-xs-8">
							<div class="sig-input">
								<input class="form-control" type="password" maxlength="45"
									id="password_confirm" name="confirmPassword"
									data-validation="required">
							</div>
						</div>
					</div>

					<!-- Degree -->

					<div class="row  form-group   has-feedback">
						<div class="col-lg-5 col-md-5 col-sm-4 col-xs-4">
							<div class="sig-label">
								<label class="control-label">Choose Degree</label>
							</div>
						</div>
						<div class="col-lg-7 col-md-7 col-sm-8 col-xs-8">
							<div class="sig-input">
								<form:input class="form-control" type="text" path="degrees"
									data-validation="required"/>
									
							</div>
						</div>
						
					</div>
					

					<!-- Specialty -->
					<div class="row  form-group   has-feedback">
						<div class="col-lg-5 col-md-5 col-sm-4 col-xs-4">
							<div class="sig-label">
								<label class="control-label">Choose Specialty</label>
							</div>
						</div>
						<div class="col-lg-7 col-md-7 col-sm-8 col-xs-8">
							<div class="sig-input">
								<form:select name="speciality" path="specialityid"
									class="form-control" id="specialityid"
									data-validation="required">
									<form:option value="" label="Choose Speciality" />
									<form:options items="${specialities}" />
								</form:select>
							</div>
						</div>
					</div>

					<!-- Area Of Interest -->
					<div class="row  form-group   has-feedback">
						<div class="col-lg-5 col-md-5 col-sm-4 col-xs-4">
							<div class="sig-label">
								<label class="control-label">Area Of Interest</label>
							</div>
						</div>
						<div class="col-lg-7 col-md-7 col-sm-8 col-xs-8">
							<div class="sig-input">
								<form:input path="areaOfInterest" class="form-control" maxlength="100"
									type="text" data-validation="required" />
							</div>
						</div>
					</div>
					<!-- Designation/Title -->
					<div class="row  form-group   has-feedback">
						<div class="col-lg-5 col-md-5 col-sm-4 col-xs-4">
							<div class="sig-label">
								<label class="control-label">Designation/Title</label>
							</div>
						</div>
						<div class="col-lg-7 col-md-7 col-sm-8 col-xs-8">
							<div class="sig-input">
								<form:input path="title" class="form-control" maxlength="45"
									type="text" data-validation="required" />
							</div>
						</div>
					</div>
					<!-- Hospital  -->
					<div class="row form-group   has-feedback">
						<div class="col-lg-5 col-md-5 col-sm-4 col-xs-4">
							<div class="sig-label">
								<label class="control-label">Hospital/Clinic</label>
							</div>
						</div>
						<div class="col-lg-7 col-md-7 col-sm-8 col-xs-8">
							<div class="sig-input">
								<form:input path="hospital" class="form-control" type="text"
									data-validation="required" maxlength="100"
									data-validation-error-msg="Please enter Hospital Name" />
							</div>
						</div>
					</div>


					<!--Mobile Number  -->
					<div class="row form-group   has-feedback">
						<div class="col-lg-5 col-md-5 col-sm-4 col-xs-4">
							<div class="sig-label">
								<label class="control-label">Mobile Number</label>
							</div>
						</div>
						<div class="col-lg-7 col-md-7 col-sm-8 col-xs-8">
							<div class="sig-input">
								<input maxlength="10" id="phone1" name="phone1"
									class="form-control mblnmbr" type="text"
									data-validation="required"  onchange = "updatePhone();"/>
									
								<form:input id="phone" type="hidden" name="phone" path="phone"/>
							</div>
						</div>	
						
					</div>

					<!-- medical registration number -->
					<div class="row form-group   has-feedback">
						<div class="col-lg-5 col-md-5  col-sm-4 col-xs-4">
							<div class="sig-label">
								<label class="control-label">Medical Registration No.</label>
							</div>
						</div>
						<div class="col-lg-7 col-md-7 col-sm-8 col-xs-8">
							<div class="sig-input">

								<form:input length="10" id="mregno" name="mregno" path="regno"
									class="form-control" type="text" data-validation="required"
									maxlength="15"
									data-validation-error-msg="Medical Registration Number is Mandatory" />
							</div>
						</div>
					</div>

					<!-- Video consult fee  -->
					<div class="row  form-group   has-feedback">
						<div class="col-lg-5 col-md-5 col-sm-4 col-xs-4">
							<div class="sig-label">
								<label class="control-label">Video Consult Fees</label>
							</div>
						</div>
						<div class="col-lg-7 col-md-7 col-sm-8 col-xs-8">
							<div class="sig-input">
								<form:select path="sessionFee" class="form-control"
									data-validation="required">
									<form:option value="0" label="Session Fee" />
									<c:forEach begin="10" end="100" step="10" var="fee">
										<form:option value="${fee}">${fee} USD</form:option>
									</c:forEach>
								</form:select>
							</div>
						</div>
						</div>
						<div class="row  form-group   has-feedback">
						<div class="col-lg-5 col-md-5 col-sm-4 col-xs-4">
							<div class="sig-label">
								<label class="control-label">Text Consult Fees</label>
							</div>
						</div>
						<div class="col-lg-7 col-md-7 col-sm-8 col-xs-8">
							<div class="sig-input">
								<form:select path="queryFee" class="form-control"
									data-validation="required">
									<form:option value="0" label="Text Fee" />
									<c:forEach begin="10" end="100" step="10" var="fee">
										<form:option value="${fee}">${fee} USD</form:option>
									</c:forEach>
								</form:select>
							</div>
						</div>
					</div>
					
					<div class="row  form-group center">
						<div class="col-lg-5 col-md-5"></div>
						<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
								<div id="selectedFiles"></div>
							</div>
					</div>
						
					
					<div class="row  form-group   has-feedback">
						<div class="col-lg-5 col-md-5"></div>
						<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
						
							<div class="sig-btn">
								<label class="btn btn-block bg-blue"
									style="text-align: center; margin-top: 10px; height: 41px; border-radius: 0;">
									Upload Profile Picture<input type="file" name="newProfileImage"
									id="newProfileImage" data-validation="mime size required"
									data-validation-allowing="jpg, png, gif, bpm, jepg"
									data-validation-max-size="2M"
									accept="image/x-png,image/x-bpm,image/x-jpg,image/gif,image/jpeg"
									data-type='image'
									style="width: 50%; display: inherit; opacity: 0; -moz-opacity: 0; filter: progid:DXImageTransform.Microsoft.Alpha(opacity=0)" />
								</label>
								
							</div>
						</div>
						
						
					</div>
					
						
					
					<!-- Terms and condition  -->
					<div class="row form-group   has-feedback">
						<div class="col-lg-5 col-md-5 "></div>
						<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
							<div class="sig-check">
								<label class="checkbox-inline" style="color: #000;"> <input
									type="checkbox" name="agree" id="agree" value="agree" required>
									I agree to the<a onclick="myAgreement()"> service agreement</a>
								</label>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12"></div>
						<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
							<div class="sig-btn">
								<button class="btn btn-block bg-blue clickk2" type="submit"
									onclick="return Submit()">Register</button>
							</div>
						</div>
					</div>
				</form:form>

			</div>
			<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
				<div class="sig-right-img">
					<div class="si-ri-img">
						<img src="images/si1.png" />
					</div>
					<div class="sig-right-con">
						<p>Dedicate profile page</p>
					</div>
				</div>
				<div class="sig-right-img">
					<div class="si-ri-img">
						<img src="images/si2.png" />
					</div>
					<div class="sig-right-con">
						<p>Connect with International patients directly</p>
					</div>
				</div>
				<div class="sig-right-img">
					<div class="si-ri-img">
						<img src="images/si3.png" />
					</div>
					<div class="sig-right-con">
						<p>Easy to set up time slots for video consults</p>
					</div>
				</div>
				<div class="sig-right-img">
					<div class="si-ri-img">
						<img src="images/si4.png" />
					</div>
					<div class="sig-right-con">
						<p>Get paid for online consultations</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>



<%@ include file="newFooter.jsp"%>


<%@page import="com.medicine.arvene.util.CommonUtil"%>
<%@ include file="newHeader.jsp"%></section>
<script type="text/javascript">
	$(document).ready(
			function() {

				var specialityArray = ${specialityMap};
				$(function() {
					$("#name").autocomplete(
							{
								source : function(request, response) {
									var matcher = new RegExp($.ui.autocomplete
											.escapeRegex(request.term), "i");
									response($.map(specialityArray, function(
											value, key) {
										if (value
												&& (!request.term || matcher
														.test(value))) {
											return {
												label : value,
												value : key
											};
										}
									}));
								},
								select : function(event, ui) {
									$("#name").val(ui.item.label), $("#id")
											.val(ui.item.value);//ui.item is your object from the array
									return false;
								},
								minLength : 2
							})
				});

			});
</script>

<section class="sec-main sec-drl">
	<div class="container">
	  		<div class="drl-bgwhite conn-bttn">
                    <div class="row">
                        <div class="col-lg-12">
                        <a href="userQuery" class="btn btn-primary pull-right">Consult a doctor Now</a>
                        </div>
                    </div>
                </div>
	
		<div class="drl-bgwhite">
			<div class="row">
				<!--------------------search barr--------------------->
				<!--------------------search barr--------------------->
				

				<div class="col-lg-4 col-md-4">`
					<div class="ser-sign-m">

						<form:form class="center" action='searchspecialists' method="POST"
							modelAttribute="speciality" id="searchForm" name="searchForm">

							<form:input type="hidden" name="speciality" path="id" />
							<form:input type="text" name="speciality"
								placeholder="Search Specialist" path="name" class="form-control" />
							<span class="fa fa-search" onclick="$('#searchForm').submit();"></span>
							<input type="hidden" name="start" id="start" />

						</form:form>

					</div>
				</div>
				<div class="col-lg-8">
					<div class="drl-heading">
						<!-- <h4>Search For a Speciality to find doctors</h4> -->
					</div>
				</div>
				<!--------------------search barr End--------------------->
				<!--------------------search barr End--------------------->
			</div>
			<div class="row margtopp">
				<c:forEach items="${specialists}" var="specialist"
					varStatus="loopStatus">

					<div class="col-lg-4 col-sm-12 col-md-12 col-xs-12 	margtopp">
						<div class="drl-main">
							<div class="row">
								<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
									<div class="drl-img">
										<img src="${specialist.profileImage}" />
									</div>
								</div>
								<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
									<div class="drl-con">
										<label>${specialist.firstName}
											${specialist.lastName} </label>
										<p>${specialist.speciality.name},${specialist.degrees}</p>
										<p>${specialist.title}</p>
										<span><a
											href="viewDoctorProfile?id=${specialist.user.id}">View
												Profile</a></span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</c:forEach>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="drl-btn">
						<div id="pagination"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>




<%-- <div class="search-doctor">
	<div id='cssmenu'>
		<form:form class="center" action='searchspecialists' method="POST"
			modelAttribute="speciality" id="searchForm" name="searchForm">

			<form:input type="hidden" name="speciality" path="id" />
			<form:input type="text" name="speciality"
				placeholder="Search Specialist" path="name"
				class="input-xlarge"/>
			<img src="css/images/search-blue.png" onclick="$('#searchForm').submit();" style="cursor: pointer;float: right;top: -44px;left: -8px; position: relative;"/>
			<input type="hidden" name="start" id="start" />

		</form:form>
	</div>
</div> --%>




<!--  <div class="bodyContent">
 -->
<script type='text/javascript'>
	var options = {
		currentPage : '${currentpage}',
		totalPages : '${pageno}',
		onPageClicked : function(e, originalEvent, type, page) {
			searchFormSubmit(page)
		},
		size : 'medium',
		alignment : 'center'

	};

	$('#pagination').bootstrapPaginator(options);

	function searchFormSubmit(page) {
		var start = (page * 9) - 9;
		$("#start").val(start);
		$("#searchForm").submit();
	}

	$('.nav >li.active').removeClass("active");
	$('.nav #specialists').addClass("active");
</script>
<%@ include file="newFooter.jsp"%>
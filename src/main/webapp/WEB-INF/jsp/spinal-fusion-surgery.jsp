<%@ include file="newHeader.jsp"%></section>
<!-- Hospital Short Details Starts Here -->
<div class="short-details">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="title">
					<h1>Neuro and Spine</h1>
					<ul class="paginator">
						<li><a href="/">Home</a></li>
						<li><a>Neuro and Spine</a></li>
					</ul>
				</div>
			</div>
			<!-- End Column -->
		</div>
		<!-- End Row -->
	</div>
	<!-- End Container -->
</div>
<!-- Hospital Short Details Ends Here -->

<!-- Specialty Content Starts Here -->
<div class="speciality-content-area">
	<div class="container">

		<div class="innertab-accordion">
			<div class="row">
				<div class="col-md-3 tabnopaddright">
					<div class="tabs">
						<ul>
							<li class="tab-active"><a href="neuro-spine-surgery">Neuro
									and Spine</a></li>
							<li><a href="neuro-surgery">Neurosurgery</a></li>
							<li><a href="neurology">Neurology</a></li>
							<li><a href="brain-tumors-treatment">Brain Tumor</a></li>
							<li><a href="spinal-fusion-surgery">Spinal Fusion</a></li>
							<li><a href="intracranial-aneurysm-surgery">Aneurysms</a></li>
							<li><a href="spinal-tumor-surgery">Spinal Tumor</a></li>
							<li><a href="spinal-laminectomy-surgery">Laminectomy</a></li>
						</ul>
					</div>
				</div>
				<!-- End Column -->
				<div class="col-md-9 tabnopaddleft">
					<div class="accordion-contentarea">

						<div id="tab5">
							<h2>
								<span>Spinal Fusion, Decompression &amp; Arthroplasty</span> ?
								India
							</h2>
							<div class="row">
								<div class="col-md-9 col-sm-9 nopaddleft">
									<p>We at Arvene Healthcare have the expertise to organize
										for all treatments and surgeries related to Neuro and Spine.
										Arvene Healthcare is associated with top hospitals and Doctors
										of India in this field. We can give you more and better
										options so that you can take the right decisions. We also
										assist you in taking best decision based on your requirement.
										Arvene Healthcare will also arrange for your stay and local
										travel, so that you have nothing else to worry about but your
										treatment. Please contact us on <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
										today to avail best services.</p>
									<h3>Spinal Fusion:</h3>
									<p>Spinal fusion surgical procedure is done to correct
										problems associated with the small bones of the spine. The
										fusion is primarily done for eliminating the pain which has
										been caused by abnormal motion of the vertebrae.</p>
									<br>
									<p>Spinal fusion relieves symptoms of several back problems
										that includes -</p>
									<ul>
										<li><span>Infection</span></li>
										<li><span>Degenerative disk disease</span></li>
										<li><span>Tumor</span></li>
										<li><span>Spondylolisthesis</span></li>
										<li><span>Fracture</span></li>
										<li><span>Scoliosis</span></li>
										<li><span>Spinal stenosis</span></li>
									</ul>
								</div>
								<!-- End Inner Column -->
								<div class="col-md-3 col-sm-3 nopaddright">
									<img src="images/specialities/27.spinal fusion .jpg"
										alt="spinal fusion" class="img-responsive">
								</div>
								<!-- End Inner Column -->
							</div>
							<!-- End Inner Row -->
							<h3>
								<span>Spinal Fusion</span> Procedure
							</h3>
							<p>The surgical procedure of spinal fusion takes several
								hours to complete. Spinal fusion involves different methods</p>
							<ul>
								<li><span>A surgeon uses metal implants for holding
										the vertebrae together till the new bone grows in between
										them.</span></li>
								<li><span>Bone is taken either from a bone bank or
										the pelvic bone. This bone is used for making a bridge between
										the vertebrae which are next to each other.</span></li>
							</ul>
							<p>Spinal fusion is used for treating fractures along with
								many other problems. It also treats the problem of spinal
								stenosis and age-related spinal problems.</p>
							<h3>
								<span>What is </span>Spinal Decompression Surgery?
							</h3>
							<p>Spinal decompression surgery is done for procedures that
								provide relief from many symptoms caused by compression or
								pressure, to the nerve roots or spinal cord. Thickened joints,
								bony growths, collapsed disc or bulging and loosened ligaments
								can narrow the openings of spinal nerve and the spinal canal,
								resulting in irritation. The nerve compression symptoms are -</p>
							<ul>
								<li><span>Unsteadiness</span></li>
								<li><span>Pain</span></li>
								<li><span>Weakness</span></li>
								<li><span>Tingling</span></li>
								<li><span>Numbness</span></li>
							</ul>
							<p>However in serious cases, force or pressure on the spinal
								nerves can cause problems with bowel function or bladder, or can
								result in paralysis.</p>
							<h3>
								<span>Techniques for</span> Decompression
							</h3>
							<ul>
								<li><span>Foraminotomy or foraminectomy</span></li>
								<li><span>Diskectomy</span></li>
								<li><span>Corpectomy</span></li>
								<li><span>Laminotomy or laminectomy</span></li>
								<li><span>Osteophyte removal</span></li>
							</ul>
							<h3>
								<span>Total Disc Arthroplasty or </span>Artificial disc
								replacement
							</h3>
							<p>Total disc arthroplasty is performed for removing painful
								degenerated disc. This degenerated disc is replaced by an
								artificial mobile disc that imitates the movement of a normal
								disc. Mobility in the motion is hence maintained. Total disc
								arthroplasty is also referred to as cervical artificial disc
								replacement, spinal arthroplasty and disc replacement.</p>
							<h3>
								<span>Who should consider</span> disc replacement surgery?
							</h3>
							<ul>
								<li><span>Candidates who are 18 years of age having
										skeletal maturity.</span></li>
								<li><span>Candidates who have been diagnosed with
										myelopathy or radiculopathy or maybe both along with the
										presence of bone spurs or disc herniation.</span></li>
								<li><span>Candidates require treatment at only one
										cervical level.</span></li>
								<li><span>Candidates whose symptoms did not improve
										even after conservative treatment measures like pain
										relievers, chiropractic care or physical therapy and exercise.</span></li>
							</ul>
							<h3>
								<span>Nonsurgical Spinal</span> Decompression
							</h3>
							<p>A new revolutionary technique for treating disc injuries
								in the low back and in the neck is termed as nonsurgical spinal
								decompression. This is a very safe procedure that utilizes FDA
								approved equipment in order to apply disruption forces to spinal
								structures in a graduated manner. Nonsurgical spinal
								decompression treatment is very efficient in treating herniated
								discs, burning arm pain, leg pain, bulging discs, degenerative
								disc disease, sciatica, facet syndromes and pinched nerves. It
								is essential to have a proper screening and only the best
								suitable candidates are accepted for this.</p>
							<h3>
								<span>Cost of </span>Spinal Disease and Disorders Treatment in
								India
							</h3>
							<p>
								If you are suffering from severe back pain and have been
								recommended Spine Surgery, one of the best options for you to
								treat spine disorders is in India. Many patients travel from all
								over the world to get low<b>-cost</b>
							</p>
							<h5>
								To get best options for Neuro and Spine treatment, send us a
								query or write to us at <a
									href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
							</h5>
						</div>
						<!-- End Tab -->
					</div>
				</div>
				<!-- End Column -->
			</div>
			<!-- End Row -->
		</div>
		<!-- End Tab Accordion -->

	</div>
	<!-- End Container -->
</div>
<!-- Speciality Content Ends Here -->

<%@ include file="newFooter.jsp"%>
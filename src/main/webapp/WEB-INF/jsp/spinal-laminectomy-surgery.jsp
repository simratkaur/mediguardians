<%@ include file="newHeader.jsp"%></section>
<!-- Hospital Short Details Starts Here -->
<div class="short-details">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="title">
					<h1>Neuro and Spine</h1>
					<ul class="paginator">
						<li><a href="/">Home</a></li>
						<li><a>Neuro and Spine</a></li>
					</ul>
				</div>
			</div>
			<!-- End Column -->
		</div>
		<!-- End Row -->
	</div>
	<!-- End Container -->
</div>
<!-- Hospital Short Details Ends Here -->

<!-- Specialty Content Starts Here -->
<div class="speciality-content-area">
	<div class="container">

		<div class="innertab-accordion">
			<div class="row">
				<div class="col-md-3 tabnopaddright">
					<div class="tabs">
						<ul>
							<li><a href="neuro-spine-surgery">Neuro
									and Spine</a></li>
							<li><a href="neuro-surgery">Neurosurgery</a></li>
							<li><a href="neurology">Neurology</a></li>
							<li><a href="brain-tumors-treatment">Brain Tumor</a></li>
							<li><a href="spinal-fusion-surgery">Spinal Fusion</a></li>
							<li><a href="intracranial-aneurysm-surgery">Aneurysms</a></li>
							<li><a href="spinal-tumor-surgery">Spinal Tumor</a></li>
							<li class="tab-active"><a href="spinal-laminectomy-surgery">Laminectomy</a></li>
						</ul>
					</div>
				</div>
				<!-- End Column -->
				<div class="col-md-9 tabnopaddleft">
					<div class="accordion-contentarea">

						<div id="tab8">
							<h2>
								<span>Spinal Laminectomy </span>- India
							</h2>
							<div class="row">
								<div class="col-md-9 col-sm-9 nopaddleft">
									<p>We at Arvene Healthcare have the expertise to organize
										for all treatments and surgeries related to Neuro and Spine.
										Arvene Healthcare is associated with top hospitals and Doctors
										of India in this field. We can give you more and better
										options so that you can take the right decisions. We also
										assist you in taking best decision based on your requirement.
										Arvene Healthcare will also arrange for your stay and local
										travel, so that you have nothing else to worry about but your
										treatment. Please contact us on <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
										today to avail best services.</p>
								</div>
								<!-- End Inner Column -->
								<div class="col-md-3 col-sm-3 nopaddright">
									<img src="images/specialities/30. laminectomy.jpg"
										alt="laminectomy" class="img-responsive">
								</div>
								<!-- End Inner Column -->
							</div>
							<!-- End Inner Row -->
							<h3>Laminectomy:</h3>
							<p>The procedure used for treating pain linked with
								compressed or pinched nerves in the spine is known as
								laminectomy. Laminectomy, also referred to as decompression
								surgery, increases the spinal canal to ease pressure on the
								nerves or spinal cord. The procedure of a laminectomy is usually
								performed on the neck or on the vertebrae in the lower back.</p>
							<h3>
								<span>Reasons for Performing </span>Laminectomy Procedure
							</h3>
							<p>One of the major reasons for laminectomy is severe,
								annoying and persistent pain in the lower back. This pain may
								interfere with normal functioning and can also limit mobility.</p>
							<h3>
								<span>Success Rates of</span> Spinal Stenosis and Laminectomy
							</h3>
							<p>The success rate is usually favourable when lumbar
								laminectomy is performed for relieving pain from spinal
								stenosis. About 70-80% of patients after laminectomy have shown
								major improvement in performing their routine daily activities.
							</p>
							<h3>
								<span>Decompressive Laminectomy for</span> Spinal Stenosis
							</h3>
							<p>Decompressive Laminectomy is basically performed for
								treating spinal stenosis. The purpose of this surgery is to ease
								pressure on the spinal nerve roots or spinal cord due to
								age-related changes in the spine. Herniated discs, spine
								injuries or tumors are treated by this surgery. This surgery is
								useful in relieving the pressure on the nerve roots that helps
								in reducing pain and a person can begin its routine activities
								soon.</p>
							<h3>
								<span>Decompressive Lumbar</span> Laminectomy
							</h3>
							<p>During the surgical procedure of decompressive lumbar
								laminectomy, the back of the spinal bones is removed in order to
								ease the pressure on the nerves. This procedure is primarily
								performed for those patients who are facing weakness, leg pain
								or numbness due to the pressure on the nerves.</p>
							<h3>
								<span>Spinal </span>Decompression
							</h3>
							<p>The surgical procedure of spinal decompression is
								performed at any place along the spine from the neck to the
								lower back. It may involve one or more vertebrae that depend on
								the extent of the stenosis. Different types of decompression
								surgery include -</p>
							<p>
								<strong> Laminaplasty: </strong> It is the extension of the
								spinal canal where lamina is cut from one side. This type is
								primarily used in the cervical area.
							</p>
							<p>
								<strong>Laminotomy: </strong> In this, small portion of
								ligaments and lamina are removed from one side. This procedure
								reduces the chance of postoperative spinal instability as the
								original support of the lamina is left at its place.
							</p>
							<p>
								<strong> Laminectomy: </strong> In this the complete bony lamina
								(a portion of the enlarged facet joints) and the thickened
								ligaments are removed.
							</p>
							<p>
								<strong> Foraminotomy: </strong> Here bone surrounding the
								neural foremen is removed. This procedure is primarily performed
								when the disc degeneration has resulted in the collapse of the
								height of the foramen and that has caused pinched nerves. This
								procedure can be performed along with laminotomy or laminectomy.
							</p>
							<h5>
								To get best options for Neuro and Spine treatment, send us a
								query or write to us at <a
									href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
							</h5>
						</div>
						<!-- End Tab -->

					</div>
				</div>
				<!-- End Column -->
			</div>
			<!-- End Row -->
		</div>
		<!-- End Tab Accordion -->

	</div>
	<!-- End Container -->
</div>
<!-- Speciality Content Ends Here -->

<%@ include file="newFooter.jsp"%>
<%@ include file="newHeader.jsp"%></section>
<!-- Hospital Short Details Starts Here -->
<div class="short-details">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="title">
					<h1>Neuro and Spine</h1>
					<ul class="paginator">
						<li><a href="/">Home</a></li>
						<li><a>Neuro and Spine</a></li>
					</ul>
				</div>
			</div>
			<!-- End Column -->
		</div>
		<!-- End Row -->
	</div>
	<!-- End Container -->
</div>
<!-- Hospital Short Details Ends Here -->

<!-- Specialty Content Starts Here -->
<div class="speciality-content-area">
	<div class="container">

		<div class="innertab-accordion">
			<div class="row">
				<div class="col-md-3 tabnopaddright">
					<div class="tabs">
						<ul>
							<li><a href="neuro-spine-surgery">Neuro
									and Spine</a></li>
							<li><a href="neuro-surgery">Neurosurgery</a></li>
							<li><a href="neurology">Neurology</a></li>
							<li><a href="brain-tumors-treatment">Brain Tumor</a></li>
							<li><a href="spinal-fusion-surgery">Spinal Fusion</a></li>
							<li><a href="intracranial-aneurysm-surgery">Aneurysms</a></li>
							<li class="tab-active"><a href="spinal-tumor-surgery">Spinal Tumor</a></li>
							<li><a href="spinal-laminectomy-surgery">Laminectomy</a></li>
						</ul>
					</div>
				</div>
				<!-- End Column -->
				<div class="col-md-9 tabnopaddleft">
					<div class="accordion-contentarea">

						<div id="tab7">
							<h2>
								<span>Spinal Tumor Surgery </span>- India
							</h2>
							<div class="row">
								<div class="col-md-9 col-sm-9 nopaddleft">
									<p>We at Arvene Healthcare have the expertise to organize
										for all treatments and surgeries related to Neuro and Spine.
										Arvene Healthcare is associated with top hospitals and Doctors
										of India in this field. We can give you more and better
										options so that you can take the right decisions. We also
										assist you in taking best decision based on your requirement.
										Arvene Healthcare will also arrange for your stay and local
										travel, so that you have nothing else to worry about but your
										treatment. Please contact us on <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
										today to avail best services.</p>
								</div>
								<!-- End Inner Column -->
								<div class="col-md-3 col-sm-3 nopaddright">
									<img src="images/specialities/29.spinal tumor.jpg" alt=""
										class="img-responsive">
								</div>
								<!-- End Inner Column -->
							</div>
							<!-- End Inner Row -->
							<h3>
								<span>What is </span> Spinal Tumor?
							</h3>
							<p>Spinal tumor involves unusual development of cells in and
								around the spinal nerves and spinal cord. They could be
								cancerous (malignant) and non-cancerous (benign) tumors. The
								functions of vertebrae, nerve roots, blood vessels, meninges and
								spinal cord are affected by these tumors. Permanent spinal cord
								or nerve damage can happen if malignant or benign spinal tumor
								is left untreated.</p>
							<h3>
								<span>Symptoms of</span> Spinal Tumors
							</h3>
							<p>The symptoms of spinal cord tumors depend on their
								location and these symptoms occur when the tumor pushes on the
								nerves or the spinal cord that exit it. Some of the symptoms
								include -</p>
							<ul>
								<li><span><strong> Motor Problems: </strong> Muscle
										related symptoms can be caused by these tumors that obstruct
										nerve communication that include loss of control over the
										bladder or bowel and muscle weakness.</span></li>
								<li><span><strong> Pain: </strong> The most
										well-known symptom of spinal cord tumor is back pain. Any
										pressure on the spinal cord may generate pain and a person may
										feel that pain is coming from the different parts of the body.
										The pain is constant and severe and can also result in burning
										or aching.</span></li>
								<li><span><strong> Sensory Changes: </strong> These
										sensory changes may take form of tingling, numbness, reduced
										sensitivity cold sensations or temperature.</span></li>
							</ul>
							<h3>
								<span>Diagnosis of </span>Spinal Tumors
							</h3>
							<p>Tests are required in order to determine the location and
								extent of the tumor.</p>
							<ul>
								<li><span>Angiogram</span></li>
								<li><span>Plain Spinal X-rays</span></li>
								<li><span>Lumbar Puncture</span></li>
								<li><span>CTS with and without contrast dye</span></li>
								<li><span>Myelogram</span></li>
								<li><span>MRI or MRA- angigram</span></li>
								<li><span>Electrical Conduction tests (used at the
										time of surgery)</span></li>
								<li><span>MRI with and without contrast dye</span></li>
							</ul>
							<h3>
								<span>Treatment of</span> Spinal Tumors
							</h3>
							<p>The purpose of the surgery is to remove the tumor as much
								as possible without affecting any neurological function.
								Chemotherapy and radiation therapy are of little help and in few
								treatments where protocols are provided. For treating benign and
								malignant primary tumors, surgery is usually recommended.
								Surgery is a successful treatment for removing tumors that are
								located outside the spinal cord. The removal of vertebrae can
								also prove beneficial for easing pain and symptoms by decreasing
								pressure on the spinal nerves. For regaining strength and muscle
								control after surgery or radiation therapy, physical therapy is
								sometimes required.</p>
							<h3>Cyberknife</h3>
							<p>Radiosurgery system is used in Cyberknife technique which
								delivers precise doses of radiation to spine tumors without
								affecting nearby structures and tissues. This technique can even
								treat inoperable tumors and cyberknife technique combined with
								chemotherapy and surgery can provide many benefits as compared
								to traditional radiation therapy. Some of its benefits include -</p>
							<ul>
								<li><span>Less recovery time and with no side
										effects</span></li>
								<li><span>Painless and non-invasive technique</span></li>
								<li><span>The technique targets the tumor without
										affecting or damaging the healthy tissues</span></li>
							</ul>
							<h5>
								To get best options for Neuro and Spine treatment, send us a
								query or write to us at <a
									href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
							</h5>
						</div>
						<!-- End Tab -->
					</div>
				</div>
				<!-- End Column -->
			</div>
			<!-- End Row -->
		</div>
		<!-- End Tab Accordion -->

	</div>
	<!-- End Container -->
</div>
<!-- Speciality Content Ends Here -->

<%@ include file="newFooter.jsp"%>
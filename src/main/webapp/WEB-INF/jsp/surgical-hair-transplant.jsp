<%@ include file="newHeader.jsp"%></section>
<!-- Hospital Short Details Starts Here -->
<div class="short-details">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="title">
					<h1>Cosmetic Surgery</h1>
					<ul class="paginator">
						<li><a href="/">Home</a></li>
						<li><a>Cosmetic Surgery</a></li>
					</ul>
				</div>
			</div>
			<!-- End Column -->
		</div>
		<!-- End Row -->
	</div>
	<!-- End Container -->
</div>
<!-- Hospital Short Details Ends Here -->

<!-- Specialty Content Starts Here -->
<div class="speciality-content-area">
	<div class="container">

		<div class="innertab-accordion">
			<div class="row">
				<div class="col-md-3 tabnopaddright">
					<div class="tabs">
						<ul>
							<li><a href="cosmetic-surgery">Cosmetic Surgery</a></li>
							<li><a href="breast-augmentation-surgery">Breast
									Augmentation</a></li>
							<li><a href="breast-reduction-surgery">Breast Reduction</a></li>
							<li><a href="liposuction-surgery">Liposuction</a></li>
							<li><a href="facelift-surgery">Facelift</a></li>
							<li><a href="rhinoplasty-surgery">Rhinoplasty</a></li>
							<li><a href="cosmetic-laser-surgery">Cosmetic Laser
									Surgery</a></li>
							<li><a href="oral-maxillofacial-surgery">Maxillofacial
									Surgery</a></li>
							<li class="tab-active"><a href="surgical-hair-transplant">Surgical Hair
									Transplant</a></li>
						</ul>
					</div>
				</div>
				<!-- End Column -->
				<div class="col-md-9 tabnopaddleft">
					<div class="accordion-contentarea">

						<div id="tab9">
							<h2>
								<span>Surgical Hair Transplantation</span> in India
							</h2>
							<div class="row">
								<div class="col-md-9 col-sm-9 nopaddleft">
									<p>We at Arvene Healthcare have the expertise to organize
										for all treatments and surgeries related to Hair
										Transplantation. Arvene Healthcare is associated with top
										hospitals and Doctors of India in this field. We can give you
										more and better options so that you can take the right
										decisions. We also assist you in taking best decision based on
										your requirement. Arvene Healthcare will also arrange for your
										stay and local travel, so that you have nothing else to worry
										about but your treatment. Please contact us on
										<a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a> today to avail best services.</p>
									<h3>
										<span>What is </span>Hair Transplant?
									</h3>
								</div>
								<!-- End Inner Column -->
								<div class="col-md-3 col-sm-3 nopaddright">
									<img src="images/specialities/16.hair transplant.jpg"
										alt="hair transplant" class="img-responsive">
								</div>
								<!-- End Inner Column -->
							</div>
							<!-- End Inner Row -->
							<p>The problem of hair loss can be due to change in hormones,
								a family history of baldness (hereditary) and ageing. The
								problem of hair loss will become even more severe if the hair
								loss begins early. The problem of hair loss can occur in both
								men and women and also in childrenHair transplantation is a
								surgical technique that moves individual hair follicles from
								back of the scalp called the 'donor site' to a bald or balding
								part of the scalp known as the 'recipient site'. The hair is
								genetically programmed in such a way that will continuously grow
								even when it has been transplanted to a different site.</p>
							<h3>
								<span>Types of</span> Hair Loss
							</h3>
							<p>Alopecia is the medical name used for hair loss.
								Androgenetic Alopecia (pattern baldness) is the most frequent
								type of alopecia that is seen in one-third women and men.</p>

							<h3>
								<span>Hair Transplant</span> Procedure
							</h3>
							<p>The surgery takes place under local anaesthesia. The
								surgeon first harvests the donor hair grafts from the strip of a
								skin which is taken from the region in between the two ears. The
								strip is separated into tiny units of skin grafts containing 1
								to 3 hairs known as follicular units. The surgeon then makes the
								recipient sites which are produced in the area of hair loss. In
								the bald skin, the sites are produced by making little slits The
								new transplanted hair starts to grow after 3 to 4 month's time
								and the hair continue to grow for about half an inch for every
								month.</p>

							<p>The benefit of hair transplant is that the new hair can be
								easily styled, washed and combed just like your normal hair. The
								process of follicle hair location is very safe and the results
								are excellent. Follicular Unit Extraction (FUE) and Follicular
								Unit Transplantation (FUT) are the two approaches for surgical
								hair restoration process.</p>
							<h3>
								<span>Cost of</span> Hair Transplant Surgery
							</h3>
							<p>The hair transplant cost in India is very affordable and
								almost every cosmetic clinic centre has latest medical
								facilities and services.</p>
							<h5>
								To get best options for cosmetic treatment, send us a query or
								write to us at <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
							</h5>
						</div>
						<!-- End Tab -->
						
					</div>
				</div>
				<!-- End Column -->
			</div>
			<!-- End Row -->
		</div>
		<!-- End Tab Accordion -->

	</div>
	<!-- End Container -->
</div>
<!-- Speciality Content Ends Here -->

<%@ include file="newFooter.jsp"%>
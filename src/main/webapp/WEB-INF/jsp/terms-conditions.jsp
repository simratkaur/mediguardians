<html xmlns:v="urn:schemas-microsoft-com:vml"
xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:w="urn:schemas-microsoft-com:office:word"
xmlns:m="http://schemas.microsoft.com/office/2004/12/omml"
xmlns="http://www.w3.org/TR/REC-html40">

<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name=ProgId content=Word.Document>
<meta name=Generator content="Microsoft Word 15">
<meta name=Originator content="Microsoft Word 15">
<link rel=File-List href="User%20sign%20up%20TnC_files/filelist.xml">
<!--[if gte mso 9]><xml>
 <o:DocumentProperties>
  <o:Author>Kaur, Simrat</o:Author>
  <o:LastAuthor>Simrat Kaur</o:LastAuthor>
  <o:Revision>2</o:Revision>
  <o:TotalTime>0</o:TotalTime>
  <o:Created>2016-10-15T19:06:00Z</o:Created>
  <o:LastSaved>2016-10-15T19:06:00Z</o:LastSaved>
  <o:Pages>1</o:Pages>
  <o:Words>6272</o:Words>
  <o:Characters>35755</o:Characters>
  <o:Company>Accenture</o:Company>
  <o:Lines>297</o:Lines>
  <o:Paragraphs>83</o:Paragraphs>
  <o:CharactersWithSpaces>41944</o:CharactersWithSpaces>
  <o:Version>15.00</o:Version>
 </o:DocumentProperties>
</xml><![endif]-->
<!--[if gte mso 9]><xml>
 <w:WordDocument>
  <w:TrackMoves>false</w:TrackMoves>
  <w:TrackFormatting/>
  <w:PunctuationKerning/>
  <w:ValidateAgainstSchemas/>
  <w:SaveIfXMLInvalid>false</w:SaveIfXMLInvalid>
  <w:IgnoreMixedContent>false</w:IgnoreMixedContent>
  <w:AlwaysShowPlaceholderText>false</w:AlwaysShowPlaceholderText>
  <w:DoNotPromoteQF/>
  <w:LidThemeOther>EN-US</w:LidThemeOther>
  <w:LidThemeAsian>X-NONE</w:LidThemeAsian>
  <w:LidThemeComplexScript>X-NONE</w:LidThemeComplexScript>
  <w:Compatibility>
   <w:BreakWrappedTables/>
   <w:SnapToGridInCell/>
   <w:WrapTextWithPunct/>
   <w:UseAsianBreakRules/>
   <w:DontGrowAutofit/>
   <w:SplitPgBreakAndParaMark/>
   <w:EnableOpenTypeKerning/>
   <w:DontFlipMirrorIndents/>
   <w:OverrideTableStyleHps/>
  </w:Compatibility>
  <w:DoNotOptimizeForBrowser/>
  <m:mathPr>
   <m:mathFont m:val="Cambria Math"/>
   <m:brkBin m:val="before"/>
   <m:brkBinSub m:val="&#45;-"/>
   <m:smallFrac m:val="off"/>
   <m:dispDef/>
   <m:lMargin m:val="0"/>
   <m:rMargin m:val="0"/>
   <m:defJc m:val="centerGroup"/>
   <m:wrapIndent m:val="1440"/>
   <m:intLim m:val="subSup"/>
   <m:naryLim m:val="undOvr"/>
  </m:mathPr></w:WordDocument>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <w:LatentStyles DefLockedState="false" DefUnhideWhenUsed="false"
  DefSemiHidden="false" DefQFormat="false" DefPriority="99"
  LatentStyleCount="371">
  <w:LsdException Locked="false" Priority="0" QFormat="true" Name="Normal"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 1"/>
  <w:LsdException Locked="false" Priority="9" SemiHidden="true"
   UnhideWhenUsed="true" QFormat="true" Name="heading 2"/>
  <w:LsdException Locked="false" Priority="9" SemiHidden="true"
   UnhideWhenUsed="true" QFormat="true" Name="heading 3"/>
  <w:LsdException Locked="false" Priority="9" SemiHidden="true"
   UnhideWhenUsed="true" QFormat="true" Name="heading 4"/>
  <w:LsdException Locked="false" Priority="9" SemiHidden="true"
   UnhideWhenUsed="true" QFormat="true" Name="heading 5"/>
  <w:LsdException Locked="false" Priority="9" SemiHidden="true"
   UnhideWhenUsed="true" QFormat="true" Name="heading 6"/>
  <w:LsdException Locked="false" Priority="9" SemiHidden="true"
   UnhideWhenUsed="true" QFormat="true" Name="heading 7"/>
  <w:LsdException Locked="false" Priority="9" SemiHidden="true"
   UnhideWhenUsed="true" QFormat="true" Name="heading 8"/>
  <w:LsdException Locked="false" Priority="9" SemiHidden="true"
   UnhideWhenUsed="true" QFormat="true" Name="heading 9"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="index 1"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="index 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="index 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="index 4"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="index 5"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="index 6"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="index 7"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="index 8"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="index 9"/>
  <w:LsdException Locked="false" Priority="39" SemiHidden="true"
   UnhideWhenUsed="true" Name="toc 1"/>
  <w:LsdException Locked="false" Priority="39" SemiHidden="true"
   UnhideWhenUsed="true" Name="toc 2"/>
  <w:LsdException Locked="false" Priority="39" SemiHidden="true"
   UnhideWhenUsed="true" Name="toc 3"/>
  <w:LsdException Locked="false" Priority="39" SemiHidden="true"
   UnhideWhenUsed="true" Name="toc 4"/>
  <w:LsdException Locked="false" Priority="39" SemiHidden="true"
   UnhideWhenUsed="true" Name="toc 5"/>
  <w:LsdException Locked="false" Priority="39" SemiHidden="true"
   UnhideWhenUsed="true" Name="toc 6"/>
  <w:LsdException Locked="false" Priority="39" SemiHidden="true"
   UnhideWhenUsed="true" Name="toc 7"/>
  <w:LsdException Locked="false" Priority="39" SemiHidden="true"
   UnhideWhenUsed="true" Name="toc 8"/>
  <w:LsdException Locked="false" Priority="39" SemiHidden="true"
   UnhideWhenUsed="true" Name="toc 9"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Normal Indent"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="footnote text"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="annotation text"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="header"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="footer"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="index heading"/>
  <w:LsdException Locked="false" Priority="35" SemiHidden="true"
   UnhideWhenUsed="true" QFormat="true" Name="caption"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="table of figures"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="envelope address"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="envelope return"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="footnote reference"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="annotation reference"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="line number"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="page number"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="endnote reference"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="endnote text"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="table of authorities"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="macro"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="toa heading"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Bullet"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Number"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List 4"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List 5"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Bullet 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Bullet 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Bullet 4"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Bullet 5"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Number 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Number 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Number 4"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Number 5"/>
  <w:LsdException Locked="false" Priority="10" QFormat="true" Name="Title"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Closing"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Signature"/>
  <w:LsdException Locked="false" Priority="1" SemiHidden="true"
   UnhideWhenUsed="true" Name="Default Paragraph Font"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Body Text"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Body Text Indent"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Continue"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Continue 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Continue 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Continue 4"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Continue 5"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Message Header"/>
  <w:LsdException Locked="false" Priority="11" QFormat="true" Name="Subtitle"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Salutation"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Date"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Body Text First Indent"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Body Text First Indent 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Note Heading"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Body Text 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Body Text 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Body Text Indent 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Body Text Indent 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Block Text"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Hyperlink"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="FollowedHyperlink"/>
  <w:LsdException Locked="false" Priority="22" QFormat="true" Name="Strong"/>
  <w:LsdException Locked="false" Priority="20" QFormat="true" Name="Emphasis"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Document Map"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Plain Text"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="E-mail Signature"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="HTML Top of Form"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="HTML Bottom of Form"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Normal (Web)"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="HTML Acronym"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="HTML Address"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="HTML Cite"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="HTML Code"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="HTML Definition"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="HTML Keyboard"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="HTML Preformatted"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="HTML Sample"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="HTML Typewriter"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="HTML Variable"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Normal Table"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="annotation subject"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="No List"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Outline List 1"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Outline List 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Outline List 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Simple 1"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Simple 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Simple 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Classic 1"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Classic 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Classic 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Classic 4"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Colorful 1"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Colorful 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Colorful 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Columns 1"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Columns 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Columns 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Columns 4"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Columns 5"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Grid 1"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Grid 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Grid 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Grid 4"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Grid 5"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Grid 6"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Grid 7"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Grid 8"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table List 1"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table List 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table List 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table List 4"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table List 5"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table List 6"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table List 7"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table List 8"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table 3D effects 1"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table 3D effects 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table 3D effects 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Contemporary"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Elegant"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Professional"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Subtle 1"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Subtle 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Web 1"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Web 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Web 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Balloon Text"/>
  <w:LsdException Locked="false" Priority="39" Name="Table Grid"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Theme"/>
  <w:LsdException Locked="false" SemiHidden="true" Name="Placeholder Text"/>
  <w:LsdException Locked="false" Priority="1" QFormat="true" Name="No Spacing"/>
  <w:LsdException Locked="false" Priority="60" Name="Light Shading"/>
  <w:LsdException Locked="false" Priority="61" Name="Light List"/>
  <w:LsdException Locked="false" Priority="62" Name="Light Grid"/>
  <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1"/>
  <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2"/>
  <w:LsdException Locked="false" Priority="65" Name="Medium List 1"/>
  <w:LsdException Locked="false" Priority="66" Name="Medium List 2"/>
  <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1"/>
  <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2"/>
  <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3"/>
  <w:LsdException Locked="false" Priority="70" Name="Dark List"/>
  <w:LsdException Locked="false" Priority="71" Name="Colorful Shading"/>
  <w:LsdException Locked="false" Priority="72" Name="Colorful List"/>
  <w:LsdException Locked="false" Priority="73" Name="Colorful Grid"/>
  <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 1"/>
  <w:LsdException Locked="false" Priority="61" Name="Light List Accent 1"/>
  <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 1"/>
  <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 1"/>
  <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 1"/>
  <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 1"/>
  <w:LsdException Locked="false" SemiHidden="true" Name="Revision"/>
  <w:LsdException Locked="false" Priority="34" QFormat="true"
   Name="List Paragraph"/>
  <w:LsdException Locked="false" Priority="29" QFormat="true" Name="Quote"/>
  <w:LsdException Locked="false" Priority="30" QFormat="true"
   Name="Intense Quote"/>
  <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 1"/>
  <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 1"/>
  <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 1"/>
  <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 1"/>
  <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 1"/>
  <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 1"/>
  <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 1"/>
  <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 1"/>
  <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 2"/>
  <w:LsdException Locked="false" Priority="61" Name="Light List Accent 2"/>
  <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 2"/>
  <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 2"/>
  <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 2"/>
  <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 2"/>
  <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 2"/>
  <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 2"/>
  <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 2"/>
  <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 2"/>
  <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 2"/>
  <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 2"/>
  <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 2"/>
  <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 2"/>
  <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 3"/>
  <w:LsdException Locked="false" Priority="61" Name="Light List Accent 3"/>
  <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 3"/>
  <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 3"/>
  <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 3"/>
  <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 3"/>
  <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 3"/>
  <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 3"/>
  <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 3"/>
  <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 3"/>
  <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 3"/>
  <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 3"/>
  <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 3"/>
  <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 3"/>
  <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 4"/>
  <w:LsdException Locked="false" Priority="61" Name="Light List Accent 4"/>
  <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 4"/>
  <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 4"/>
  <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 4"/>
  <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 4"/>
  <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 4"/>
  <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 4"/>
  <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 4"/>
  <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 4"/>
  <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 4"/>
  <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 4"/>
  <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 4"/>
  <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 4"/>
  <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 5"/>
  <w:LsdException Locked="false" Priority="61" Name="Light List Accent 5"/>
  <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 5"/>
  <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 5"/>
  <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 5"/>
  <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 5"/>
  <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 5"/>
  <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 5"/>
  <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 5"/>
  <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 5"/>
  <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 5"/>
  <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 5"/>
  <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 5"/>
  <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 5"/>
  <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 6"/>
  <w:LsdException Locked="false" Priority="61" Name="Light List Accent 6"/>
  <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 6"/>
  <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 6"/>
  <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 6"/>
  <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 6"/>
  <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 6"/>
  <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 6"/>
  <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 6"/>
  <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 6"/>
  <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 6"/>
  <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 6"/>
  <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 6"/>
  <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 6"/>
  <w:LsdException Locked="false" Priority="19" QFormat="true"
   Name="Subtle Emphasis"/>
  <w:LsdException Locked="false" Priority="21" QFormat="true"
   Name="Intense Emphasis"/>
  <w:LsdException Locked="false" Priority="31" QFormat="true"
   Name="Subtle Reference"/>
  <w:LsdException Locked="false" Priority="32" QFormat="true"
   Name="Intense Reference"/>
  <w:LsdException Locked="false" Priority="33" QFormat="true" Name="Book Title"/>
  <w:LsdException Locked="false" Priority="37" SemiHidden="true"
   UnhideWhenUsed="true" Name="Bibliography"/>
  <w:LsdException Locked="false" Priority="39" SemiHidden="true"
   UnhideWhenUsed="true" QFormat="true" Name="TOC Heading"/>
  <w:LsdException Locked="false" Priority="41" Name="Plain Table 1"/>
  <w:LsdException Locked="false" Priority="42" Name="Plain Table 2"/>
  <w:LsdException Locked="false" Priority="43" Name="Plain Table 3"/>
  <w:LsdException Locked="false" Priority="44" Name="Plain Table 4"/>
  <w:LsdException Locked="false" Priority="45" Name="Plain Table 5"/>
  <w:LsdException Locked="false" Priority="40" Name="Grid Table Light"/>
  <w:LsdException Locked="false" Priority="46" Name="Grid Table 1 Light"/>
  <w:LsdException Locked="false" Priority="47" Name="Grid Table 2"/>
  <w:LsdException Locked="false" Priority="48" Name="Grid Table 3"/>
  <w:LsdException Locked="false" Priority="49" Name="Grid Table 4"/>
  <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark"/>
  <w:LsdException Locked="false" Priority="51" Name="Grid Table 6 Colorful"/>
  <w:LsdException Locked="false" Priority="52" Name="Grid Table 7 Colorful"/>
  <w:LsdException Locked="false" Priority="46"
   Name="Grid Table 1 Light Accent 1"/>
  <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 1"/>
  <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 1"/>
  <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 1"/>
  <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 1"/>
  <w:LsdException Locked="false" Priority="51"
   Name="Grid Table 6 Colorful Accent 1"/>
  <w:LsdException Locked="false" Priority="52"
   Name="Grid Table 7 Colorful Accent 1"/>
  <w:LsdException Locked="false" Priority="46"
   Name="Grid Table 1 Light Accent 2"/>
  <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 2"/>
  <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 2"/>
  <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 2"/>
  <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 2"/>
  <w:LsdException Locked="false" Priority="51"
   Name="Grid Table 6 Colorful Accent 2"/>
  <w:LsdException Locked="false" Priority="52"
   Name="Grid Table 7 Colorful Accent 2"/>
  <w:LsdException Locked="false" Priority="46"
   Name="Grid Table 1 Light Accent 3"/>
  <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 3"/>
  <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 3"/>
  <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 3"/>
  <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 3"/>
  <w:LsdException Locked="false" Priority="51"
   Name="Grid Table 6 Colorful Accent 3"/>
  <w:LsdException Locked="false" Priority="52"
   Name="Grid Table 7 Colorful Accent 3"/>
  <w:LsdException Locked="false" Priority="46"
   Name="Grid Table 1 Light Accent 4"/>
  <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 4"/>
  <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 4"/>
  <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 4"/>
  <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 4"/>
  <w:LsdException Locked="false" Priority="51"
   Name="Grid Table 6 Colorful Accent 4"/>
  <w:LsdException Locked="false" Priority="52"
   Name="Grid Table 7 Colorful Accent 4"/>
  <w:LsdException Locked="false" Priority="46"
   Name="Grid Table 1 Light Accent 5"/>
  <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 5"/>
  <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 5"/>
  <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 5"/>
  <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 5"/>
  <w:LsdException Locked="false" Priority="51"
   Name="Grid Table 6 Colorful Accent 5"/>
  <w:LsdException Locked="false" Priority="52"
   Name="Grid Table 7 Colorful Accent 5"/>
  <w:LsdException Locked="false" Priority="46"
   Name="Grid Table 1 Light Accent 6"/>
  <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 6"/>
  <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 6"/>
  <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 6"/>
  <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 6"/>
  <w:LsdException Locked="false" Priority="51"
   Name="Grid Table 6 Colorful Accent 6"/>
  <w:LsdException Locked="false" Priority="52"
   Name="Grid Table 7 Colorful Accent 6"/>
  <w:LsdException Locked="false" Priority="46" Name="List Table 1 Light"/>
  <w:LsdException Locked="false" Priority="47" Name="List Table 2"/>
  <w:LsdException Locked="false" Priority="48" Name="List Table 3"/>
  <w:LsdException Locked="false" Priority="49" Name="List Table 4"/>
  <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark"/>
  <w:LsdException Locked="false" Priority="51" Name="List Table 6 Colorful"/>
  <w:LsdException Locked="false" Priority="52" Name="List Table 7 Colorful"/>
  <w:LsdException Locked="false" Priority="46"
   Name="List Table 1 Light Accent 1"/>
  <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 1"/>
  <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 1"/>
  <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 1"/>
  <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 1"/>
  <w:LsdException Locked="false" Priority="51"
   Name="List Table 6 Colorful Accent 1"/>
  <w:LsdException Locked="false" Priority="52"
   Name="List Table 7 Colorful Accent 1"/>
  <w:LsdException Locked="false" Priority="46"
   Name="List Table 1 Light Accent 2"/>
  <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 2"/>
  <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 2"/>
  <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 2"/>
  <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 2"/>
  <w:LsdException Locked="false" Priority="51"
   Name="List Table 6 Colorful Accent 2"/>
  <w:LsdException Locked="false" Priority="52"
   Name="List Table 7 Colorful Accent 2"/>
  <w:LsdException Locked="false" Priority="46"
   Name="List Table 1 Light Accent 3"/>
  <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 3"/>
  <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 3"/>
  <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 3"/>
  <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 3"/>
  <w:LsdException Locked="false" Priority="51"
   Name="List Table 6 Colorful Accent 3"/>
  <w:LsdException Locked="false" Priority="52"
   Name="List Table 7 Colorful Accent 3"/>
  <w:LsdException Locked="false" Priority="46"
   Name="List Table 1 Light Accent 4"/>
  <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 4"/>
  <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 4"/>
  <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 4"/>
  <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 4"/>
  <w:LsdException Locked="false" Priority="51"
   Name="List Table 6 Colorful Accent 4"/>
  <w:LsdException Locked="false" Priority="52"
   Name="List Table 7 Colorful Accent 4"/>
  <w:LsdException Locked="false" Priority="46"
   Name="List Table 1 Light Accent 5"/>
  <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 5"/>
  <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 5"/>
  <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 5"/>
  <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 5"/>
  <w:LsdException Locked="false" Priority="51"
   Name="List Table 6 Colorful Accent 5"/>
  <w:LsdException Locked="false" Priority="52"
   Name="List Table 7 Colorful Accent 5"/>
  <w:LsdException Locked="false" Priority="46"
   Name="List Table 1 Light Accent 6"/>
  <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 6"/>
  <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 6"/>
  <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 6"/>
  <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 6"/>
  <w:LsdException Locked="false" Priority="51"
   Name="List Table 6 Colorful Accent 6"/>
  <w:LsdException Locked="false" Priority="52"
   Name="List Table 7 Colorful Accent 6"/>
 </w:LatentStyles>
</xml><![endif]-->
<style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:Helvetica;
	panose-1:2 11 6 4 2 2 2 2 2 4;
	mso-font-charset:0;
	mso-generic-font-family:swiss;
	mso-font-pitch:variable;
	mso-font-signature:-536859905 -1073711037 9 0 511 0;}
@font-face
	{font-family:"Cambria Math";
	panose-1:2 4 5 3 5 4 6 3 2 4;
	mso-font-charset:1;
	mso-generic-font-family:roman;
	mso-font-format:other;
	mso-font-pitch:variable;
	mso-font-signature:0 0 0 0 0 0;}
@font-face
	{font-family:"Arial Unicode MS";
	panose-1:2 11 6 4 2 2 2 2 2 4;
	mso-font-charset:0;
	mso-generic-font-family:roman;
	mso-font-format:other;
	mso-font-pitch:variable;
	mso-font-signature:3 0 0 0 1 0;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{mso-style-unhide:no;
	mso-style-parent:"";
	margin:0in;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;
	mso-fareast-font-family:"Arial Unicode MS";
	border:none;}
a:link, span.MsoHyperlink
	{mso-style-unhide:no;
	mso-style-parent:"";
	text-decoration:underline;
	text-underline:single;}
a:visited, span.MsoHyperlinkFollowed
	{mso-style-noshow:yes;
	mso-style-priority:99;
	color:fuchsia;
	mso-themecolor:followedhyperlink;
	text-decoration:underline;
	text-underline:single;}
p.Body, li.Body, div.Body
	{mso-style-name:Body;
	mso-style-unhide:no;
	mso-style-parent:"";
	margin:0in;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:11.0pt;
	font-family:"Helvetica",sans-serif;
	mso-fareast-font-family:"Arial Unicode MS";
	mso-bidi-font-family:"Arial Unicode MS";
	color:black;
	border:none;}
span.Hyperlink0
	{mso-style-name:"Hyperlink\.0";
	mso-style-unhide:no;
	mso-style-parent:Hyperlink;
	text-decoration:underline;
	text-underline:single;}
.MsoChpDefault
	{mso-style-type:export-only;
	mso-default-props:yes;
	font-size:10.0pt;
	mso-ansi-font-size:10.0pt;
	mso-bidi-font-size:10.0pt;
	mso-fareast-font-family:"Arial Unicode MS";
	border:none;}
.MsoPapDefault
	{mso-style-type:export-only;}
 /* Page Definitions */
 @page
	{mso-footnote-separator:url("User%20sign%20up%20TnC_files/header.htm") fs;
	mso-footnote-continuation-separator:url("User%20sign%20up%20TnC_files/header.htm") fcs;
	mso-endnote-separator:url("User%20sign%20up%20TnC_files/header.htm") es;
	mso-endnote-continuation-separator:url("User%20sign%20up%20TnC_files/header.htm") ecs;}
@page WordSection1
	{size:8.5in 11.0in;
	margin:1.0in 1.0in 1.0in 1.0in;
	mso-header-margin:.5in;
	mso-footer-margin:.6in;
	mso-header:url("User%20sign%20up%20TnC_files/header.htm") h1;
	mso-footer:url("User%20sign%20up%20TnC_files/header.htm") f1;
	mso-paper-source:0;}
div.WordSection1
	{page:WordSection1;}
-->
</style>
<!--[if gte mso 10]>
<style>
 /* Style Definitions */
 table.MsoNormalTable
	{mso-style-name:"Table Normal";
	mso-tstyle-rowband-size:0;
	mso-tstyle-colband-size:0;
	mso-style-noshow:yes;
	mso-style-priority:99;
	mso-style-parent:"";
	mso-padding-alt:0in 5.4pt 0in 5.4pt;
	mso-para-margin:0in;
	mso-para-margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:10.0pt;
	font-family:"Times New Roman",serif;
	border:none;}
</style>
<![endif]--><!--[if gte mso 9]><xml>
 <o:shapedefaults v:ext="edit" spidmax="1026"/>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <o:shapelayout v:ext="edit">
  <o:idmap v:ext="edit" data="1"/>
 </o:shapelayout></xml><![endif]-->
</head>

<body lang=EN-US link="#000000" vlink=fuchsia style='tab-interval:.5in'>

<div class=WordSection1>

<p class=Body>TERMS &amp; CONDITIONS</p>

<p class=Body><span style='mso-spacerun:yes'>�</span></p>

<p class=Body><span lang=DE style='mso-ansi-language:DE'>1.<span
style='mso-spacerun:yes'> </span>GENERAL</span></p>

<p class=Body>a)<span style='mso-spacerun:yes'>� </span>This document is an
electronic record in terms of Information Technology Act, 2000 and rules made
there under as applicable and the amended provisions pertaining to electronic
records in various statutes as amended by the Information Technology Act, 2000.
This electronic record is generated by a computer system and does not require
any physical or digital signatures.</p>

<p class=Body><span style='mso-spacerun:yes'>�</span></p>

<p class=Body>b)<span style='mso-spacerun:yes'>� </span>This document is
published in accordance with the provisions of Rule 3 (1) of the Information
Technology (Intermediaries guidelines) Rules, 2011 that require publishing the
rules and regulations, privacy policy and Terms of Use for access or usage of
www.arvenehealthcare.com.</p>

<p class=Body><span style='mso-spacerun:yes'>�</span></p>

<p class=Body>c) The domain names www.arvenehealthcare.com
(&quot;Website&quot;) is owned and operated by Arvene Healthcare Pvt. Ltd.
(&quot;Company&quot;) a private limited company registered under the Companies
Act, 2013, and having its registered office at G-38, Quest Offices Pvt Ltd.,
Technopolis, 5th Floor, Golf course road, Sector-54, Gurgaon, Haryana-122002,
INDIA, where such expression shall, unless repugnant to the context thereof, be
deemed to include its respective representatives, administrators, employees,
directors, officers, agents and their successors and assigns.</p>

<p class=Body><span style='mso-spacerun:yes'>�</span></p>

<p class=Body>d) For the purpose of these Terms of Use (&quot;Terms&quot;),
wherever the context so requires,</p>

<p class=Body>i) The term 'You', 'Your', 'User' &amp; 'Subscriber' shall mean
any legal person or entity accessing or using the Services provided on this
Website , who is competent to enter into binding contracts, as per the
provisions of the Indian Contract Act, 1872, whether for himself or for anyone
who has authorized such person to submit his information in order to avail the
services of the Website<span style='mso-spacerun:yes'>� </span>;</p>

<p class=Body>ii) The terms 'We', 'Us', 'Our' &amp; 'Arvene Healthcare' shall
mean the Website<span style='mso-spacerun:yes'>� </span>and/or the Company, as
the context so requires.</p>

<p class=Body>iii) The term 'Services' shall mean those Services offered on the
Website lication by the Company, as enumerated under Clause 5.</p>

<p class=Body>iv) The terms 'Party' &amp; 'Parties' shall respectively be used
to refer to the User and the Company individually and collectively, as the
context so requires.</p>

<p class=Body><span style='mso-spacerun:yes'>�</span></p>

<p class=Body>e) The headings of each section in these Terms are only for the
purpose of organizing the various provisions under these Terms in an orderly
manner, and shall not be used by either Party to interpret the provisions
contained herein in any manner. Further, it is specifically agreed to by the
Parties that the headings shall have no legal or contractual value.</p>

<p class=Body><span style='mso-spacerun:yes'>�</span></p>

<p class=Body>f) The use of the Website<span style='mso-spacerun:yes'>�
</span>by the User is solely governed by these Terms as well as the Privacy
Policy (&quot;Policy&quot;), available at Privacy Policy and any modifications
or amendments made thereto by the Company from time to time, at its sole
discretion. Visiting the home page of the Website<span
style='mso-spacerun:yes'>� </span>and/or using any of the Services provided on
the Website<span style='mso-spacerun:yes'>� </span>shall be deemed to signify
the User's unequivocal acceptance of these Terms and the aforementioned Policy,
and the User expressly agrees to be bound by the same. The User expressly
agrees and acknowledges that the Terms and Policy are co-terminus, and that
expiry/termination of either one will lead to the termination of the other,
save as provided in Clause 3 hereunder.</p>

<p class=Body><span style='mso-spacerun:yes'>�</span></p>

<p class=Body>g) The User unequivocally agrees that these Terms and the
aforementioned Policy constitute a legally binding agreement between the User
and the Company, and that the User shall be subject to the rules, guidelines,
policies, terms, and conditions applicable to any service that is provided by
the Website , and that the same shall be deemed to be incorporated into these
Terms, and shall be treated as part and parcel of the same. The User
acknowledges and agrees that no signature or express act is required to make
these Terms and the Policy binding on the User, and that the User's act of
registering with the Website<span style='mso-spacerun:yes'>� </span>constitutes
the User's full and final acceptance of these Terms and the aforementioned
Policy.</p>

<p class=Body><span style='mso-spacerun:yes'>�</span></p>

<p class=Body>h) The Company reserves the sole and exclusive right to amend or
modify these Terms without any prior permission or intimation to the User, and
the User expressly agrees that any such amendments or modifications shall come
into effect immediately. The User has a duty to periodically check the terms
and stay updated on its requirements. If the User continues to use the Website<span
style='mso-spacerun:yes'>� </span>following such a change, the User will be
deemed to have consented to any and all amendments / modifications made to the
Terms. In so far as the User complies with these Terms, he is granted a
personal, non-exclusive, non-transferable, revocable, limited privilege to
enter and use the Website .</p>

<p class=Body><span style='mso-spacerun:yes'>�</span></p>

<p class=Body>2.<span style='mso-spacerun:yes'>������� </span>ELIGIBILITY</p>

<p class=Body>The User represents and warrants that he is competent and
eligible to enter into legally binding agreements and that he has the requisite
authority to bind himself to these Terms, as determined solely by the provisions
of the Indian Contract Act, 1872. The User may not use this Website<span
style='mso-spacerun:yes'>� </span>if he is not competent to contract under the
Indian Contract Act, 1872, or is disqualified from doing so by any other
applicable law, rule or regulation currently in force.</p>

<p class=Body><span style='mso-spacerun:yes'>�</span></p>

<p class=Body>3.<span style='mso-spacerun:yes'>� </span><span
style='mso-spacerun:yes'>������</span>TERMS</p>

<p class=Body>These Terms shall continue to form a valid and binding contract
between the Parties, and shall continue to be in full force and effect until:</p>

<p class=Body><span style='mso-spacerun:yes'>�</span></p>

<p class=Body>a) The User continues to access and use the Website ; or</p>

<p class=Body>b)The Transaction between the Parties, if any, concludes to the
satisfaction of both Parties;</p>

<p class=Body><span style='mso-spacerun:yes'>�</span></p>

<p class=Body>Whichever is longer. The Parties agree that certain portions of
these Terms (&quot;Clauses&quot;), such as Clauses 11, 12, 13, 16 &amp; 18,
shall continue to remain in full force and effect indefinitely, even after the
expiry or termination of these Terms as contemplated herein.</p>

<p class=Body><span style='mso-spacerun:yes'>�</span></p>

<p class=Body>4.<span style='mso-spacerun:yes'>������� </span>TERMINATION</p>

<p class=Body>The Company reserves the right, in its sole discretion, to
unilaterally terminate the User's access to the Services offered on the Website
and the Mobile App, or any portion thereof, at any time, without notice or
cause. The User shall continue to be bound by these Terms, and it is expressly
agreed to by the Parties that the User shall not have the right to terminate
these Terms till the expiry of the same, as described in 3 herein above.</p>

<p class=Body><span style='mso-spacerun:yes'>�</span></p>

<p class=Body>5.<span style='mso-spacerun:yes'>������� </span>ONLINE PLATFORM</p>

<p class=Body><span style='mso-spacerun:yes'>�</span></p>

<p class=Body>The Website<span style='mso-spacerun:yes'>� </span>is an online
platform where you can upload your health records electronically and avail
online consultation either through mail/text or video from health experts on
the Website itself.<span style='mso-spacerun:yes'>� </span>The user allows the
administrators of the Arvene healthcare website to evaluate, share and pass on
the information in order to provide free/paid consultations or second opinion
based on the inputs of prescription provided by the User. Users can make the
payment online to Us Once the Users have logged into the website, they will
have the option to take a paid text/video consultation based on the discretion
of the team of Arvene healthcare. </p>

<p class=Body>When you sign up on the website it means that the user is giving
consent to disclose medical records and history while consulting with the
Doctor and Arvene Healthcare. This platform is not intended to replace a
physical visit to a doctor because of limitations of the platform. Usually the
doctor will try his/her best to answer the email/text queries within 48 hrs but
in some cases that may take longer and We will not be responsible for any such
delays from the healthcare provider/doctors side.<span
style='mso-spacerun:yes'>� </span>The user is required to be online at the time
of video consulting, else the company will not be hold responsible for
re-scheduling the call. It will be a sole discretion of the service provider
and company go take up any such request.</p>

<p class=Body><o:p>&nbsp;</o:p></p>

<p class=Body>Arvene Healthcare is a web based platform which is based on
patient centric features and provide following Services:</p>

<p class=Body>Uploading medical history including:</p>

<p class=Body><span style='mso-spacerun:yes'>��� </span>Basic information with
the specific columns like Name/Age/Sex/Body<span style='mso-spacerun:yes'>��
</span>Weight/Height etc.</p>

<p class=Body><span style='mso-spacerun:yes'>��� </span>Generic information
pertaining to the genetic disease in his/her family.</p>

<p class=Body><span lang=DE style='mso-ansi-language:DE'><span
style='mso-spacerun:yes'>��� </span>Allergies (if any)</span></p>

<p class=Body><span style='mso-spacerun:yes'>��� </span>Past cured disease</p>

<p class=Body><span style='mso-spacerun:yes'>��� </span>Current Disease</p>

<p class=Body><span style='mso-spacerun:yes'>��� </span>Current Medications</p>

<p class=Body><span style='mso-spacerun:yes'>��� </span>Editing and updating
medical history.</p>

<p class=Body><span style='mso-spacerun:yes'>��� </span>The records may not be
maintained by Arvene Healthcare on the platform. </p>

<p class=Body><span style='mso-spacerun:yes'>���� </span>Arvene Healthcare will
not take any responsibility for lost medical records.</p>

<p class=Body><span style='mso-spacerun:yes'>������� </span></p>

<p class=Body>6.<span style='mso-spacerun:yes'>����������������
</span>REGISTRATION</p>

<p class=Body><span style='mso-spacerun:yes'>�</span></p>

<p class=Body>To fully avail the Services of the Application and use it,
registration by the User or an individual who is authorized by such User , is
required.<span style='mso-spacerun:yes'>� </span>As a part of registration
process you agree to provide Arvene Healthcare current, complete, and accurate
registration information as prompted to do and to maintain and update this
information as required to keep it updated, complete and accurate. You are
required to register by providing your entire medical history with the specific
columns as mentioned below: The mandatory fields are notified to the user to
carry forward a consultation which includes but not limited to:</p>

<p class=Body><span style='mso-spacerun:yes'>����� </span>Name, Email id, Phone
number, Age, Sex, Body Weight, Height etc.</p>

<p class=Body><span style='mso-spacerun:yes'>����� </span>Family history of
diseases.</p>

<p class=Body><span style='mso-spacerun:yes'>���� </span>Allergies: Any allergy
that the User wants to save in terms of drugs or food allergy.</p>

<p class=Body><span style='mso-spacerun:yes'>��� </span>Past cured diseases.
User can upload past records, edit and save the same. </p>

<p class=Body><span style='mso-spacerun:yes'>��� </span>Current Disease(s)</p>

<p class=Body><span style='mso-spacerun:yes'>��� </span>All the information
provided shall be uploaded on a secure cloud based platform.</p>

<p class=Body><span style='mso-spacerun:yes'>�</span></p>

<p class=Body>Membership of this website is available only to those above the
age of 18 years barring those &quot;Incompetent to Contract&quot; which inter
alia include insolvents. If You are a minor and wish to use the Application,
You may do so through Your legal guardian and Arvene Healthcare Pvt. Ltd.
reserves the right to terminate Your account on knowledge of You being a minor
and having registered on the Application or availing any of its Services.</p>

<p class=Body><span style='mso-spacerun:yes'>�</span></p>

<p class=Body>Further, at any time during Your use of this Application,
including but not limited to the time of registration, You are solely
responsible for protecting the confidentiality of Your username and password,
and any activity under the account shall be deemed to have been done by You. In
the case that You provide Us with false and/or inaccurate details or We have
reason to believe You have done so, We hold the right to permanently suspend
Your account.</p>

<p class=Body><span style='mso-spacerun:yes'>�</span></p>

<p class=Body>7.<span style='mso-spacerun:yes'>� </span>COMMUNICATION</p>

<p class=Body>By using this Website , and providing his contact information to
the Company through the Website , the User hereby agrees and consents to
receiving calls, autodialed and/or pre-recorded message calls, e-mails and SMSs
from the Company and/or any of its affiliates or partners at any time, subject
to the Policy. In the event that the User wishes to stop receiving any such
marketing or promotional calls / email messages / text messages, the User may
send an e-mail to the effect to contact@arvenehealthcare.com with the subject [Stop
Promos]. The User agrees and acknowledges that it may take up to thirty(30)
business days for the Company to give effect to such a request by the User.</p>

<p class=Body><span style='mso-spacerun:yes'>�</span></p>

<p class=Body>The User expressly agrees that notwithstanding anything-contained
herein above, he may be contacted by the Company or any of its affiliates /
partners relating to any service availed of by the User on the Website<span
style='mso-spacerun:yes'>� </span>or anything pursuant thereto.</p>

<p class=Body><span style='mso-spacerun:yes'>�</span></p>

<p class=Body>It is expressly agreed to by the Parties that any information
shared by the User with the Company shall be governed by the Policy.</p>

<p class=Body><span style='mso-spacerun:yes'>�</span></p>

<p class=Body>8.<span style='mso-spacerun:yes'>��� </span><span
style='mso-spacerun:yes'>�����</span>CHARGES</p>

<p class=Body>The use of this Website<span style='mso-spacerun:yes'>� </span>by
the User, including browsing the Website<span style='mso-spacerun:yes'>�
</span>is free of cost. The Company reserves the right to amend this no-fee
policy and charge the User for the use of the Website/Application. In such an
event, the User will be intimated of the same when he attempts to access the
Website , and the User shall have the option of declining to avail of the
Services offered on the Website . Any such change, if made, shall come into
effect immediately upon such change being notified to the User, unless
specified otherwise.</p>

<p class=Body><span style='mso-spacerun:yes'>�</span></p>

<p class=Body>9.<span style='mso-spacerun:yes'> </span>USER OBLIGATIONS</p>

<p class=Body>The User agrees and acknowledges that he is a restricted user of
this Website , and that he:</p>

<p class=Body>a) Is bound not to cut, copy, distribute, modify, recreate,
reverse engineer, distribute, disseminate, post, publish or create derivative
works from, transfer, or sell any information or software obtained from the
Website . Any such use / limited use of the Website<span
style='mso-spacerun:yes'>� </span>will only be allowed with the prior express written
permission of the Company. For the removal of doubt, it is clarified that
unlimited or wholesale reproduction, copying of the content for commercial or
non-commercial purposes and unwarranted modification of data and information
contained on the Website<span style='mso-spacerun:yes'>� </span>is expressly
prohibited.</p>

<p class=Body>b) agrees not to access (or attempt to access) the Website<span
style='mso-spacerun:yes'>� </span>and/or the materials or Services by any means
other than through the interface provided by the Website . The use of
deep-link, robot, spider or other automatic device, program, algorithm or
methodology, or any similar or equivalent manual process, to access, acquire,
copy or monitor any portion of the Website<span style='mso-spacerun:yes'>�
</span>or its content, or in any way reproduce or circumvent the navigational
structure or presentation of the Website , materials or any content, or to obtain
or attempt to obtain any materials, documents or information through any means
not specifically made available through the Website<span
style='mso-spacerun:yes'>� </span>will lead to suspension or termination of the
User's access to the Website , as detailed in Clause 10 herein below. The User
acknowledges and agrees that by accessing or using the Website<span
style='mso-spacerun:yes'>� </span>or any of the Services provided therein, he
may be exposed to content that he may consider offensive, indecent or otherwise
objectionable. The Company disclaims any and all liabilities arising in
relation to such offensive content on the Website . The User expressly agrees
and acknowledges that all the Services displayed on the Website<span
style='mso-spacerun:yes'>� </span>are not owned by the Company/Website , and
that the same may be the exclusive property of certain third parties who have
chosen to market their Services through the Company's Website , and that the
Company is in no way responsible for the content of the same. The User may
however report any such offensive or objectionable content, which the Company
may then remove from the Website , at its sole discretion.</p>

<p class=Body>c) In places where Website<span style='mso-spacerun:yes'>�
</span>permits the User to post or upload data/information, the User undertakes
to ensure that such material is not offensive or objectionable, and is in
accordance with applicable laws. The User expressly agrees that any such
material that is deemed to be objectionable/offensive may be removed from the
Website<span style='mso-spacerun:yes'>� </span>immediately and without notice,
and further that the User's access to the Website<span
style='mso-spacerun:yes'>� </span>may also be permanently revoked, at the sole
discretion of the Company.</p>

<p class=Body>d) Further undertakes not to:</p>

<p class=Body>i. Abuse, harass, threaten, defame, disillusion, erode, abrogate,
demean or otherwise violate the legal rights of any other person or entity;</p>

<p class=Body>ii. Engage in any activity that interferes with or disrupts
access to the Website<span style='mso-spacerun:yes'>� </span>or the Services
provided therein (or the servers and networks which are connected to the
Website );</p>

<p class=Body>iii. Impersonate any person or entity, or falsely state or
otherwise misrepresent his/her affiliation with a person or entity;</p>

<p class=Body>iv. Publish, post, disseminate, any information which is grossly
harmful, harassing, blasphemous, defamatory, obscene, pornographic, pedophilic,
libelous, invasive of another's privacy, hateful, or racially, ethnically
objectionable, disparaging, relating or encouraging money laundering or
gambling, or otherwise unlawful in any manner whatever under any law, rule or
regulation currently in force; or unlawfully threatening or unlawfully
harassing including but not limited to &quot;indecent representation of women&quot;
within the meaning of the Indecent Representation of Women (Prohibition) Act,
1986;</p>

<p class=Body>v. Post any image/file/data that infringes the copyright, patent
or trademark of another person or legal entity;</p>

<p class=Body>vi. Upload or distribute files that contain viruses, corrupted
files, or any other similar software or programs that may damage the operation
of the Website ;</p>

<p class=Body>vii. Download any file posted/uploaded by another user of the
Website<span style='mso-spacerun:yes'>� </span>that the User is aware, or
should reasonably be aware, cannot be legally distributed in such a manner;</p>

<p class=Body>viii. Neither probe, scan or test the vulnerability of the
Website<span style='mso-spacerun:yes'>� </span>or any network connected to the
Website , nor breach the security or authentication measures on the
Website<span style='mso-spacerun:yes'>� </span>or any network connected to the
Website . The User may not reverse look-up, trace or seek to trace any
information relating to any other user of, or visitor to, the Website , or any
other customer of the Website , including any user account maintained on the
Website<span style='mso-spacerun:yes'>� </span>not operated/managed by the
User, or exploit the Website<span style='mso-spacerun:yes'>� </span>or
information made available or offered by or through the Website , in any
manner;</p>

<p class=Body>ix. Disrupt or interfere with the security of, or otherwise cause
harm to, the Website , systems resources, accounts, passwords, servers or
networks connected to or accessible through the Websites/ Mobile Apps or any
affiliated or linked Websites/ Mobile Apps;</p>

<p class=Body>x. Collect or store data about other users of the Website .</p>

<p class=Body>xi. Use the Website<span style='mso-spacerun:yes'>� </span>or any
material or content therein for any purpose that is unlawful or prohibited by
these Terms, or to solicit the performance of any illegal activity or other
activity which infringes the rights of this Website<span
style='mso-spacerun:yes'>� </span>or any other third party(ies);</p>

<p class=Body>xii. Violate any code of conduct or guideline which may be
applicable for or to any particular or service offered on the Website ;</p>

<p class=Body>xiii. Violate any applicable laws, rules or regulations currently
in force within or outside India;</p>

<p class=Body>xiv. Violate any portion of these Terms or the Policy, including
but not limited to any applicable additional terms of the Website<span
style='mso-spacerun:yes'>� </span>contained herein or elsewhere, whether made
by amendment, modification, or otherwise;</p>

<p class=Body>xv. Threaten the unity, integrity, defence, security or
sovereignty of India, friendly relations with foreign states, or public order,
or cause incitement to the commission of any cognizable offence, or prevent the
investigation of any offence, or insult any other nation.</p>

<p class=Body>xvi. Publish, post, or disseminate information that is false,
inaccurate or misleading;</p>

<p class=Body>xvii.<span style='mso-spacerun:yes'>� </span>Directly or
indirectly offer, attempt to offer, trade, or attempt to trade, any item the
dealing of which is prohibited or restricted in any manner under the provisions
of any applicable law, rule, regulation or guideline for the time being in
force.</p>

<p class=Body>xviii. Commit any act that causes the Company to lose (in whole
or in part) the Services of its internet service provider (&quot;ISP&quot;) or
in any manner disrupts the Services of any other supplier/service provider of
the Company/Website ;</p>

<p class=Body>xix. Engage in advertising to, or solicitation of, other users of
the Website<span style='mso-spacerun:yes'>� </span>to buy or sell any products
or Services not currently displayed on the Website . The User may not transmit
any chain letters or unsolicited commercial or junk email/messages to other
users via the Website<span style='mso-spacerun:yes'>� </span>or through any
other internet based platform infringing the reputation of the company or its
Services. It shall be a violation of these Terms to use any information
obtained from the Website<span style='mso-spacerun:yes'>� </span>in order to
harass, abuse, or harm another person, or in order to contact, advertise to,
solicit, or sell to another user of the Website<span style='mso-spacerun:yes'>�
</span>without the express prior written consent of the Company.</p>

<p class=Body><span style='mso-spacerun:yes'>�</span></p>

<p class=Body>e) The User expressly understands and agrees the following.</p>

<p class=Body>i. In order to use the Services offered through the Website , You
need to be a Registered User. You agree to provide us with accurate and
complete registration information.</p>

<p class=Body>ii. It is the sole responsibility of the User to inform Company
of any changes to that information.</p>

<p class=Body>iii. Each registration is for a single individual only, unless
specifically designated otherwise on the registration page. Each registration
can hold multiple accounts as per provision of the Website/Application. The
number of accounts will be limited to a maximum of 6 sub accounts for one
registration. All the terms are applicable to the sub accounts also.</p>

<p class=Body>iv. You are responsible for maintaining the confidentiality of
your account credentials.</p>

<p class=Body>v. You shall be responsible for all uses of your account, whether
or not authorized by You. You agree to immediately notify us of any unauthorized
access or use of Your account or password.</p>

<p class=Body>vi. When a user registers on the Website , You will be asked to
provide us with certain information including, without limitation, Your name,
username, contact number, date of birth, gender, and a valid email address.</p>

<p class=Body>vii. In addition to these Terms of Use, You understand and agree
that We may collect and disclose certain information about You to third
parties. In order to understand how We collect and use your information, please
visit our Privacy Policy.</p>

<p class=Body><span style='mso-spacerun:yes'>�</span></p>

<p class=Body>f)<span style='mso-spacerun:yes'>� </span>The User hereby
expressly authorizes the Company/Website<span style='mso-spacerun:yes'>�
</span>to disclose any and all information relating to the User in the
possession of the Company/Website<span style='mso-spacerun:yes'>� </span>to law
enforcement or other government officials, as the Company may in its sole
discretion, believe necessary or appropriate in connection with the
investigation and/or resolution of possible crimes, especially those involve
personal injury and theft / infringement of intellectual property. The User
further understands that the Company/Website<span style='mso-spacerun:yes'>�
</span>might be directed to disclose any information (including the identity of
persons providing information or materials on the Website ) as necessary to
satisfy any judicial order, law, regulation or valid governmental request.</p>

<p class=Body><span style='mso-spacerun:yes'>�</span></p>

<p class=Body>The User expressly agrees and acknowledges that the
Company/Website<span style='mso-spacerun:yes'>� </span>has no obligation to
monitor the materials posted on the Website , but that it has the right to
remove or edit any content that in its sole discretion violates, or is alleged
to violate, any applicable law or either the spirit or letter of these Terms.
Notwithstanding this right, the User remains solely responsible for the content
of the materials posted on the Website<span style='mso-spacerun:yes'>�
</span>by him/her. In no event shall the Company/Website<span
style='mso-spacerun:yes'>� </span>assume or be deemed to have any
responsibility or liability for any content posted, or for any claims, damages
or losses resulting from use of any such content and/or the appearance of any
content on the Website . The User hereby represents and warrants that he/she
has all necessary rights in and to all content provided as well as all
information contained therein, and that such content does not infringe any
proprietary or other rights of any third party(ies), nor does it contain any
libelous, tortuous, or otherwise unlawful or offensive material, and the User
hereby accepts full responsibility for any consequences that may arise due to
the publishing of any such material on the Website .</p>

<p class=Body><span style='mso-spacerun:yes'>�</span></p>

<p class=Body>g)<span style='mso-spacerun:yes'>� </span>The User agrees that:</p>

<p class=Body><span style='mso-spacerun:yes'>�</span></p>

<p class=Body>i.<span style='mso-spacerun:yes'>� </span>Any content on this
website and particularly any such content relating to medical conditions and
their treatment is solely for general informational purposes/alert purposes and
is not intended as, shall not be construed to be, and is no substitute for the
advise provided by a qualified and practicing expert medical professional.</p>

<p class=Body><span style='mso-spacerun:yes'>�</span></p>

<p class=Body>ii. He/She must never ignore qualified medical guidance or
treatment or postpone seeking qualified medical diagnosis or treatment because
of data on the Website lication. The material on the Website lication should
not be utilized in lieu of a appointment, call, consultation, or guidance to,
with, or from a qualified healthcare professional, inclusive of a personal
physician.</p>

<p class=Body><span style='mso-spacerun:yes'>�</span></p>

<p class=Body>iii. The User agrees that the Company may terminate the User's
access to or use of the Arvene Healthcare system and Services at any time if we
the Company is unable at any time to determine or verify the User's
qualifications or credentials.</p>

<p class=Body><span style='mso-spacerun:yes'>�</span></p>

<p class=Body>iv.<span style='mso-spacerun:yes'>� </span>The User will
implement and maintain appropriate administrative, physical and technical
safeguards to protect information within the Arvene Healthcare system from
access, use or alteration and will always use the user ID assigned to him or a
member of the User's workforce. The User is required to maintain appropriate
security with regard to all personnel, systems, and administrative processes used
by him or members of his workforce to transmit, store and process electronic
health information through the use of the Arvene Healthcare system. The User
will immediately notify the Company of any breach or suspected breach of the
security of the Arvene Healthcare system, or any unauthorized use or disclosure
of information within or obtained from the Arvene Healthcare system, and will
take such action to mitigate the breach or suspected breach as the Company may
direct, and will cooperate with the Company in investigating and mitigating
such breach.</p>

<p class=Body><span style='mso-spacerun:yes'>�</span></p>

<p class=Body>v.<span style='mso-spacerun:yes'>� </span>The User represents and
warrants that he/she will, at all times during the use of the Arvene Healthcare
system and thereafter, comply with all laws directly or indirectly applicable
that may now or hereafter govern the gathering, use, transmission, processing,
receipt, reporting, disclosure, maintenance, and storage of the patient
information, and use best efforts to cause all persons or entities under
his/her direction or control to comply with such laws, including but not
limited to the Information Technology Act, 2000 and the rules made thereunder.
The User is at all times during the use of the Arvene Healthcare system and
thereafter, solely responsible for obtaining and maintaining all patient consents,
and all other legally necessary consents or permissions required to disclose,
process, retrieve, transmit, and view the patient information. The Company does
not assume any responsibility for the User's use or misuse of patient
information or other information transmitted, monitored, stored or received
while using Arvene Healthcare. The Company reserves the right to amend or
delete any material (along with the right to revoke any membership or restrict
access) that in its sole discretion violates the above.</p>

<p class=Body><span style='mso-spacerun:yes'>�</span></p>

<p class=Body>vi. The Company makes no representations concerning the
completeness, accuracy or utility of any information or Services offers in the
Arvene Healthcare system. The Company has no liability for the consequences to
the Doctor or his/her patients.</p>

<p class=Body><span style='mso-spacerun:yes'>�</span></p>

<p class=Body><span style='mso-spacerun:yes'>�</span></p>

<p class=Body><span style='mso-spacerun:yes'>�</span></p>

<p class=Body>10.<span style='mso-spacerun:yes'>���� </span>SUSPENSION OF USER
ACCESS AND ACTIVITY</p>

<p class=Body>Notwithstanding other legal remedies that may be available to it,
the Company may in its sole discretion limit the User's access and/ or activity
by immediately removing the User's access credentials either temporarily or
indefinitely, or suspend/ terminate the User's membership, and/or refuse to
provide User with access to the Website , without being required to provide the
User with notice or cause:</p>

<p class=Body>a)<span style='mso-spacerun:yes'>� </span>If the User is in
breach of any of these Terms or the Policy;</p>

<p class=Body>b) If the User has provided wrong, inaccurate, incomplete or
incorrect information;</p>

<p class=Body>c) If the User's actions may cause any harm, damage or loss to
the other users or to the Website /Company,.</p>

<p class=Body><span style='mso-spacerun:yes'>�</span></p>

<p class=Body><span style='mso-spacerun:yes'>�</span>11.<span
style='mso-spacerun:yes'>����� </span>INDEMNITY AND LIMITATIONS</p>

<p class=Body>The User hereby expressly agrees to defend, indemnify and hold
harmless the Website<span style='mso-spacerun:yes'>� </span>and the Company,
its parent, subsidiaries, affiliates, employees, directors, officers, agents
and their successors and assigns and against any and all claims, liabilities,
damages, losses, costs and expenses, including attorney's fees, caused by or
arising out of claims based upon the User's actions or inactions, including but
not limited to any warranties, representations or undertakings, or in relation
to the non-fulfilment of any of the User's obligations under this Agreement, or
arising out of the User's infringement of any applicable laws, rules and
regulations, including but not limited to infringement of intellectual property
rights, payment of statutory dues and taxes, claims of libel, defamation,
violation of rights of privacy or publicity, loss of service by other
subscribers, or the infringement of any other rights of a third party.</p>

<p class=Body><span style='mso-spacerun:yes'>�</span></p>

<p class=Body>In no event shall the Company/Website<span
style='mso-spacerun:yes'>� </span>be liable to compensate the User or any third
party for any special, incidental, indirect, consequential or punitive damages
whatsoever, including those resulting from loss of use, data or profits,
whether or not foreseeable, and whether or not the Company/Website<span
style='mso-spacerun:yes'>� </span>had been advised of the possibility of such
damages, or based on any theory of liability, including breach of contract or
warranty, negligence or other tortuous action, or any other claim arising out
of or in connection with the User's use of or access to the Website<span
style='mso-spacerun:yes'>� </span>and/or the products, Services or materials
contained therein.</p>

<p class=Body>The diagnosis/advises/recommendations/ prescriptions or any other
suggestions given by the doctor will be independent of Mediguardians and the
company shall have no liability regarding the same.</p>

<p class=Body><o:p>&nbsp;</o:p></p>

<p class=Body>The reports shared by the patients are at sole discretion of the
patient to share a particular report or entire summary saved in the account.
The service providers ( doctors ) on the platform are not bound to confirm the
appointment without giving any advance notice. In such cases the payment will
be refunded within 60 days of communication to <span class=Hyperlink0><a
href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a></span><span
style='mso-tab-count:1'>� </span></p>

<p class=Body><o:p>&nbsp;</o:p></p>

<p class=Body>No fee/charges are refunded. Any refund will be sole discretion
of the company.</p>

<p class=Body><o:p>&nbsp;</o:p></p>

<p class=Body>This platform is intended for follow ups and second opinion and
strictly not advisable for any emergency medical condition.</p>

<p class=Body><o:p>&nbsp;</o:p></p>

<p class=Body>The video consults will require certain basic technology in the
users computer/tablet/smart phones and any issues with technical drop of call
or network unavailability at the time of consults will be sole responsibility
of the user.</p>

<p class=Body><o:p>&nbsp;</o:p></p>

<p class=Body>Third party interface may be required to built in the platform
and complete functionality which is not under any control of Ours.</p>

<p class=Body><o:p>&nbsp;</o:p></p>

<p class=Body>The limitations and exclusions in this section apply to the
maximum extent permitted by applicable law, and the Parties expressly agree
that in the event of any statute, rule, regulation or amendment coming into
force that would result in the Company/Website<span style='mso-spacerun:yes'>�
</span>incurring any form of liability whatsoever, these Terms and the Policy
will stand terminated one (1) day before the coming into effect of such
statute, rule, regulation or amendment. It is further agreed to by the Parties
that the contents of this Section shall survive even after the termination or
expiry of the Terms and/or Policy.</p>

<p class=Body><span style='mso-spacerun:yes'>�</span></p>

<p class=Body><span style='mso-spacerun:yes'>�</span></p>

<p class=Body><span style='mso-spacerun:yes'>�</span></p>

<p class=Body><span lang=DE style='mso-ansi-language:DE'>12.<span
style='mso-spacerun:yes'>������� </span>INTELLECTUAL PROPERTY RIGHTS</span></p>

<p class=Body>Unless expressly agreed to in writing, nothing contained herein
shall give the User a right to use any of the Website 's trade names,
trademarks, service marks, logos, domain names, information, questions,
answers, solutions, reports and other distinctive brand features, save
according to the provisions of these Terms. All logos, trademarks, brand names,
service marks, domain names, including material, designs, and graphics created
by and developed by the Website<span style='mso-spacerun:yes'>� </span>and
other distinctive brand features of the Website<span style='mso-spacerun:yes'>�
</span>are the property of the Company. Furthermore, with respect to the
Website<span style='mso-spacerun:yes'>� </span>created by the Company, the
Company shall be the exclusive owner of all the designs, graphics and the like,
related to the Website .</p>

<p class=Body><span style='mso-spacerun:yes'>�</span></p>

<p class=Body>The User may not use any of the intellectual property displayed
on the Website<span style='mso-spacerun:yes'>� </span>in any manner that is
likely to cause confusion among existing or prospective users of the Website ,
or that in any manner disparages or discredits the Company/Website , to be
determined in the sole discretion of the Company.</p>

<p class=Body><span style='mso-spacerun:yes'>�</span></p>

<p class=Body>The User is further aware that any reproduction or infringement
of the intellectual property of owners of such rights by the User may result in
legal action being initiated against the User by the respective owners of the
intellectual property so reproduced / infringed upon. It is agreed to by the
Parties that the contents of this Section shall survive even after the
termination or expiry of the Terms and/or Policy.</p>

<p class=Body><span style='mso-spacerun:yes'>�</span></p>

<p class=Body>13.<span style='mso-spacerun:yes'>������� </span>DISCLAIMER OF
WARRANTIES AND LIABILITIES</p>

<p class=Body>a) Except as otherwise expressly stated on the Website , all
Services offered on the Website<span style='mso-spacerun:yes'>� </span>are
offered on an &quot;as is&quot; basis without any warranty whatsoever, either
express or implied.</p>

<p class=Body>b) The Content on the Website<span style='mso-spacerun:yes'>�
</span>should not be substituted for medical or health advice. Nothing on the
Website<span style='mso-spacerun:yes'>� </span>is aimed at medical diagnosis or
treatment or as a suggestion of a course of treatment for a specific Subscriber
or User. The Content does not aim to be an auxiliary for qualified medical
guidance, analysis, or treatment.</p>

<p class=Body>d) The Company/ Website/ Application Or any of its agent, servant
or assigns shall not be liable for any direct or indirect, wilful or otherwise,
act or omission that can be attributed to the Services on the Website/
Application. Or any of its agent, servant or assigns have any liability
whatsoever in case any third party claims, demands, suit, actions, or other
proceedings are initiated against any of its personnel or any other person
engaged by the Company. The Company/Website<span style='mso-spacerun:yes'>�
</span>cannot be held responsible in a court of law, for any situations of
damages including but not limiting to medical damage or negligence arising in a
User's practice whether the situation arose as a result of information or
knowledge obtained from Arvene Healthcare or not or related to the Services
provided or agreed to be provided by Arvene Healthcare.</p>

<p class=Body>e) The User agrees and undertakes that he is accessing the
Website<span style='mso-spacerun:yes'>� </span>and he is using his best and
prudent judgment before availing any service listed on the Website , or
accessing/using any information displayed thereon.</p>

<p class=Body>f) In case the User uploads any details of a third person related
or known to the User, uploading such details would deemed to have been done
after the User obtaining express consent from such third person.</p>

<p class=Body>g) The Website<span style='mso-spacerun:yes'>� </span>and the
Company accepts no liability for any errors or omissions, whether on behalf of
itself or third parties, or for any damage caused to the User, the User's belongings,
or any third party, resulting from the use or misuse of service availed of by
the User from the Website .</p>

<p class=Body>h) The Company/Website<span style='mso-spacerun:yes'>�
</span>does not guarantee that the functions and Services contained in the
Website<span style='mso-spacerun:yes'>� </span>will be uninterrupted or
error-free, or that the Website<span style='mso-spacerun:yes'>� </span>or its
server will be free of viruses or other harmful components, and the User hereby
expressly accepts any and all associated risks involved with the User's use of
the Website .</p>

<p class=Body>i) It is further agreed to by the Parties that the contents of
this Section shall survive even after the termination or expiry of the Terms
and/or Policy.</p>

<p class=Body>j) You understand that for the exchange of information between
You and Us, the usage of local exchange and internet backbone carrier lines,
routers, switches and other devices which maybe collectively called carrier
lines shall be made. These carrier lines are maintained by third party
carriers, internet service providers and utilities. You understand that we have
no control over the same and therefore we assume no liability with regards to
the confidentiality, security, integrity, privacy and the use of any
information as it is transmitted over these carrier lines. We further assume no
liability for the interception, interruption, failure, transmission, or
corruption of any data. Your use of the carrier lines is solely at your own
risk.</p>

<p class=Body><o:p>&nbsp;</o:p></p>

<p class=Body>k) We will not be in breach of any promise made to You by Us
unless we receive a written notice specifying about the default and the nature
thereof and since the receipt of such notice we haven��t attempted to cure such
default for a time period of 30 days. Or if cure within such period is not
practicable then, to be diligently proceeding to cure the default.</p>

<p class=Body><o:p>&nbsp;</o:p></p>

<p class=Body>l) You understand that other users have access to our system and though
they have agreed to abide by these terms of services, there might still be some
users who might deviate from the promise and use the system in unethical and
illegal ways. We assume no liability for the same.</p>

<p class=Body><o:p>&nbsp;</o:p></p>

<p class=Body>m) If We do have some liability towards you or any third party
for any loss or harm or damage then both parties agree that such liability
shall under no circumstances exceed the value of any and all fee received by Us
from You.</p>

<p class=Body><o:p>&nbsp;</o:p></p>

<p class=Body>n) We do not personally recommend or endorse any sort of specific
tests. Neither does the company recommend or endorse any treatments, drugs,
techniques, products, physicians, organisations, medical professionals,
articles, abstracts, procedures, opinions, and other information as displayed
on the online platform. You understand that if you are dissatisfied with the
service of the website then your only remedy is to discontinue using the
services of the website.</p>

<p class=Body><o:p>&nbsp;</o:p></p>

<p class=Body>o) Content included on the website is provided by third party
content providers or other users, we have no editorial control or
responsibility of such content appearing on the website.</p>

<p class=Body><o:p>&nbsp;</o:p></p>

<p class=Body><span style='mso-spacerun:yes'>�</span></p>

<p class=Body>14.<span style='mso-spacerun:yes'>������� </span>SUBMISSIONS</p>

<p class=Body>Any comments, ideas, suggestions, initiation, or any other
content contributed by the User to the Company or this Website<span
style='mso-spacerun:yes'>� </span>will be deemed to include a royalty-free,
perpetual, irrevocable, nonexclusive right and license for the Company to
adopt, publish, reproduce, disseminate, transmit, distribute, copy, use, create
derivative works, display worldwide, or act on such content, without additional
approval or consideration, in any media, or technology now known or later
developed, for the full term of any rights that may exist in such content, and
the User hereby waives any claim to the contrary. The User hereby represents
and warrants that he owns or otherwise controls all of the rights to the
content contributed to the Website , and that use of such content by the
Company/Website<span style='mso-spacerun:yes'>� </span>does not infringe upon
or violate the rights of any third party. In the event of any action initiated
against the Company/Website<span style='mso-spacerun:yes'>� </span>by any such
affected third party, the User hereby expressly agrees to indemnify and hold
harmless the Company/Website , for its use of any such information provided to
it by the User. The Company reserves its right to defend itself in any such
legal disputes that may arise, and recover the costs incurred in such
proceedings from the User.</p>

<p class=Body><span style='mso-spacerun:yes'>�</span></p>

<p class=Body>15.<span style='mso-spacerun:yes'>������� </span>FORCE MAJEURE</p>

<p class=Body>Neither the Company nor the Website<span
style='mso-spacerun:yes'>� </span>shall be liable for damages for any delay or
failure to perform its obligations hereunder if such delay or failure is due to
cause beyond its control or without its fault or negligence, due to Force
Majeure events including but not limited to acts of war, acts of God,
earthquake, riot, sabotage, labor shortage or dispute, internet interruption,
technical failure, breakage of sea cable, hacking, piracy, cheating, illegal or
unauthorized.</p>

<p class=Body><span style='mso-spacerun:yes'>�</span></p>

<p class=Body>16.<span style='mso-spacerun:yes'>������� </span>DISPUTE
RESOLUTION AND JURISDICTION</p>

<p class=Body>It is expressly agreed to by the Parties hereto that the
formation, interpretation and performance of these Terms and any disputes
arising here from will be resolved through a two-step Alternate Dispute
Resolution (&quot;ADR&quot;) mechanism. It is further agreed to by the Parties
that the contents of this Section shall survive even after the termination or
expiry of the Terms and/or Policy.</p>

<p class=Body><span style='mso-spacerun:yes'>�</span></p>

<p class=Body>a)<span style='mso-spacerun:yes'>����������������
</span>Mediation: In case of any dispute between the parties, the Parties will
attempt to resolve the same amicably amongst themselves, to the mutual
satisfaction of both Parties. In the event that the Parties are unable to reach
such an amicable solution within thirty (30) days of one Party communicating
the existence of a dispute to the other Party, the dispute will be resolved by
arbitration, as detailed herein below;</p>

<p class=Body><span style='mso-spacerun:yes'>�</span></p>

<p class=Body>b)<span style='mso-spacerun:yes'>���������������
</span>Arbitration. In the event that the Parties are unable to amicably
resolve a dispute by mediation, said dispute will be referred to arbitration by
a sole arbitrator to be appointed by the Company, and the award passed by such
sole arbitrator will be valid and binding on both Parties. The Parties shall
bear their own costs for the proceedings, although the sole arbitrator may, in
his/her sole discretion, direct either Party to bear the entire cost of the
proceedings. The arbitration shall be conducted in English, and the seat of
Arbitration shall be the city of Gurgaon in the state of Haryana, India.</p>

<p class=Body><span style='mso-spacerun:yes'>�</span></p>

<p class=Body>c)<span style='mso-spacerun:yes'>� </span>The
Parties expressly agree that the Terms, Policy and any other agreements entered
into between the Parties are governed by the laws, rules and regulations of
India, and that the Courts at Gurgaon shall have exclusive jurisdiction over
any disputes arising between the Parties.</p>

<p class=Body><span style='mso-spacerun:yes'>�</span></p>

<p class=Body><span style='mso-spacerun:yes'>�</span></p>

<p class=Body>17.<span style='mso-spacerun:yes'>������� </span>NOTICES</p>

<p class=Body>Any and all communication relating to any dispute or grievance
experienced by the User may be communicated to the Company by the User reducing
the same to writing, and sending the same to the registered office of the
Company by Registered Post Acknowledgement Due / Speed Post Acknowledgement Due
(RPAD / SPAD)</p>

<p class=Body><span style='mso-spacerun:yes'>�</span></p>

<p class=Body><span lang=DE style='mso-ansi-language:DE'>18.<span
style='mso-spacerun:yes'>������ </span>MISCELLANEOUS PROVISIONS</span></p>

<p class=Body>a)<span style='mso-spacerun:yes'>� </span>Entire
Agreement: These Terms, read with the Policy form the complete and final
contract between the User and the Company with respect to the subject matter
hereof and supersedes all other communications, representations and agreements
(whether oral, written or otherwise) relating thereto;</p>

<p class=Body>b)<span style='mso-spacerun:yes'> </span>Waiver:
The failure of either Party at any time to require performance of any provision
of these Terms shall in no manner affect such Party's right at a later time to
enforce the same. No waiver by either Party of any breach of these Terms,
whether by conduct or otherwise, in any one or more instances, shall be deemed
to be or construed as a further or continuing waiver of any such breach, or a
waiver of any other breach of these Terms.</p>

<p class=Body>c)<span style='mso-spacerun:yes'>����������������
</span>Severability: If any provision/clause of these Terms is held to be
invalid, illegal or unenforceable by any court or authority of competent
jurisdiction, the validity, legality and enforceability of the remaining
provisions/clauses of these Terms shall in no way be affected or impaired thereby,
and each such provision/clause of these Terms shall be valid and enforceable to
the fullest extent permitted by law. In such case, these Terms shall be
reformed to the minimum extent necessary to correct any invalidity, illegality
or unenforceability, while preserving to the maximum extent the original
rights, intentions and commercial expectations of the Parties hereto, as
expressed herein.</p>

</div>

</body>

</html>

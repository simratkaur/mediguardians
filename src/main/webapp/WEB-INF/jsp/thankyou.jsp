<%@ include file="newHeader.jsp"%></section>
	<!-- Thank You Section Starts Here -->
	<section class="thankyou">
		<div class="txt-img">
			<div class="container">
				<h1>Thank You!</h1>
				<div class="foot">
					<div class="container">
						<p>Your details have been successfully submitted. We will
							revert to your request shortly.</p>
						<h3>
							For any other information, you can mail us at <a
								href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
						</h3>
					</div>
				</div>
				<img src="images/background/email-img.gif" alt="email"
					class="img-responsive">
				<center>
					<a href="/" class="btn btn-click">Click here to go
						back to home page</a>
				</center>
			</div>
		</div>

	</section>
	<!-- Thank You Section Ends Here -->

	<!-- <div id="footer"></div> -->

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script 
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script  src="js/bootstrap.min.js"></script>
	<!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
	<script  type="text/javascript"
		src="rs-plugin/js/jquery.themepunch.plugins.min.js"></script>
	<script  type="text/javascript"
		src="rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
	<!-- Dropdown Hover -->
	<script  src="js/jquery.bootstrap-dropdown-on-hover.min.js"></script>
	<!-- Owl Carousel -->
	<script  src="js/owl.carousel.min.js"></script>
	<!-- Form Validation -->
	<script src="formvalidation/js/formValidation.min.js"></script>
	<script src="formvalidation/js/framework/bootstrap.min.js"></script>
	<!-- Custom Javascript -->
	<script  src="js/customizer.js"></script>
	<script  src="js/form-validation.js"></script>
	<!-- Google Code for Query Form - Fill up Conversion Page -->
	<script  type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 882214865;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "buwxCPngnGcQ0Y_WpAM";
var google_remarketing_only = false;
/* ]]> */
</script>
	<script  type="text/javascript"
		src="//www.googleadservices.com/pagead/conversion.js">
</script>
	<noscript>
		<div style="display: inline;">
			<img height="1" width="1" style="border-style: none;" alt=""
				src="//www.googleadservices.com/pagead/conversion/882214865/?label=buwxCPngnGcQ0Y_WpAM&amp;guid=ON&amp;script=0" />
		</div>
	</noscript>
</body>
</html>
<%@ include file="newHeader.jsp"%></section>
<%-- <link rel="stylesheet" href="${contextPath}/css/form.css"> --%>
<!-- Google Code for YourMedics SignUp Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 928188240;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "4vdrCLK-jGQQ0I7MugM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/928188240/?label=4vdrCLK-jGQQ0I7MugM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<!-- Facebook Pixel Code -->
	<script>
	!function(f, b, e, v, n, t, s) {
		if (f.fbq)
			return;
		n = f.fbq = function() {
			n.callMethod ? n.callMethod.apply(n, arguments) : n.queue
					.push(arguments)
		};
		if (!f._fbq)
			f._fbq = n;
		n.push = n;
		n.loaded = !0;
		n.version = '2.0';
		n.queue = [];
		t = b.createElement(e);
		t.async = !0;
		t.src = v;
		s = b.getElementsByTagName(e)[0];
		s.parentNode.insertBefore(t, s)
	}(window, document, 'script', '//connect.facebook.net/en_US/fbevents.js');

	fbq('init', '1469176033391988');
	fbq('track', "PageView");
</script>
<noscript>
	<img height="1" width="1" style="display: none"
		src="https://www.facebook.com/tr?id=1469176033391988&ev=PageView&noscript=1" />
</noscript>
<!-- End Facebook Pixel Code -->
<script type="text/javascript">
	window.onload = function() {
		$("#cssmenu").hide();
		$("#userName").hide();
	}
	function Submit() {
		if (isRequired("OTP")) {
			if ($("#OTP").val().length != 6) {
				$("#errorBox").html("OTP should be of 6 characters");
				popUp();
				return false;
			} else {
				$(".custError").text("");
				return true;
			}
		} else {
			return false;
		}
	}
	function sendOtp() {
		$.ajax({
			type : "POST",
			url : "resendOtp",
			data : {
				username : "${user.username}",
				"${_csrf.parameterName}" : "${_csrf.token}"
			},
			success : function(data) {
				var arr = data.split(",");
				if (arr[1].indexOf("Success") > -1) {
					$("#errorBox").html("Otp Sent Successfully");
					popUp();
					$(".custError").css("color", "black");
				} else {
					$("#errorBox").html(arr[1]);
					popUp();
				}
			}
		});
		return false;
	}
</script>
<div class="container">
	<div class="mainContent">
		<h3 class="pageHeader" style="margin: auto;">Please Enter OTP for
			Account Activation</h3>
		<div class="row">
			<div class="col-md-12">
				<form:form name='tokenVerification' action='tokenVerification'
					method='POST' modelAttribute="user">
					<fieldset class="registration-form">

						<div class="control-group">
							<div class="controls">
								<span style="display: inline-block; width: 125px;">User
									Name:</span>
								<form:input path="username" name='username' readOnly="true" />
							</div>
						</div>

						<div class="control-group">
							<div class="controls">
								<span style="display: inline-block; width: 125px;">OTP:</span>
								<form:input path="OTP" type='text' id="OTP" maxlength="6" />
								<input type="submit" id="resendOTP" name="resentOTP"
									value="Resend OTP" onclick="return sendOtp()"
									class="btn btn-primary"
									style="width: auto; border-radius: 10px; font-size: 14px; padding: 5px 10px; margin: -4px 0 0 5px;">
								<span style="text-align:center;"><img id="loading" src="${contextPath}/css/images/processing.gif" alt="" /></span>
								
							</div>
						</div>

						<div class="control-group center">
							<div class="controls">
								<input name="Activate" type="submit" value="Activate"
									class="btn btn-primary" onclick=" return Submit()" />
							</div>
						</div>

						<div class="control-group center">
							<div class="controls">
								<a href="login">Login</a>
							</div>
						</div>
						<input type="hidden" name="${_csrf.parameterName}"
							value="${_csrf.token}" />
						<form:input path="id" type="hidden" />
					</fieldset>
				</form:form>
			</div>
		</div>
	</div>
</div>
<script>
	$.validate({
		modules : 'date, security'

	});
</script>
<%@ include file="newFooter.jsp"%>
<%@ include file="newHeader.jsp"%></section>
	<!-- Hospital Short Details Starts Here -->
	<div class="short-details">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="title">
						<h1>Urology Treatment</h1>
						<ul class="paginator">
							<li><a href="/">Home</a></li>
							<li><a>Urology Treatment</a></li>
						</ul>
					</div>
				</div>
				<!-- End Column -->
			</div>
			<!-- End Row -->
		</div>
		<!-- End Container -->
	</div>
	<!-- Hospital Short Details Ends Here -->

	<!-- Specialty Content Starts Here -->
	<div class="speciality-content-area">
		<div class="container">

			<div class="innertab-accordion">
				<div class="row">
					<div class="col-md-3 tabnopaddright">
						<div class="tabs">
							<ul>
								<li class="tab-active"><a href="urology-treatment">Urology
										Treatment</a></li>
								<li><a href="endoscopic-surgery">Endoscopy surgery</a></li>
								<li><a href="TURP-surgery">TURF</a></li>
								<li><a href="radical-prostatectomy-surgery">Prostactomy</a></li>
							</ul>
						</div>
					</div>
					<!-- End Column -->
					<div class="col-md-9 tabnopaddleft">
						<div class="accordion-contentarea">

							<div id="tab1" class="visible">
								<h2>
									<span>Urology Treatment </span>� India
								</h2>
								<div class="row">
									<div class="col-md-9 col-sm-9 nopaddleft">
										<p>We at Arvene Healthcare have the expertise to organize for all treatments and surgeries related to Urology. Arvene Healthcare is associated with top hospitals and Doctors of India in this field. We can give you more and better options so that you can take the right decisions. We also assist you in taking best decision based on your requirement. Arvene Healthcare will also arrange for your stay and local travel, so that you have nothing else to worry about but your treatment. Please contact us on <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a> today to avail best services.</p>
									</div>
									<!-- End Inner Column -->
									<div class="col-md-3 col-sm-3 nopaddright">
										<img src="images/specialities/49.Urology main.jpg" alt="Urology"
											class="img-responsive">
									</div>
									<!-- End Inner Column -->
								</div>
								<!-- End Inner Row -->
								<h3>
								Urology:
								</h3>
								<p>
								The surgical specialty which concentrates on the diseases of the female and male urinary tracts along with male reproductive organs is known as urology. The urinary tract in women opens in vulva while the urinary system in men overlaps with the reproductive system. In both men and women, the reproductive and urinary tracts are very close to each other and any disorder in one can also affect the other. This surgical specialty (urology) combines management of surgical problems like the correction of congenital abnormalities, cancers and also correcting stress incontinence, as well as non-surgical problems like benign prostatic hyperplasia and urinary tract infections. 
								</p>
								<h3>
									<span>Urologic </span>Oncology
								</h3>
								<p>Surgical treatment of malignant genitourinary diseases such as adrenal glands, testicles, kidneys, cancer of the prostate, bladder, penis, testicles and ureters. </p>
								<h3>
									<span>Paediatric </span>Urology
								</h3>
								<p>Urologic disorders in children such as congenital abnormalities, enuresis, cryptorchism, underdeveloped genitalia, vesicoureteral reflux and congenital abnormalities of the genito-urinary tract.</p>
								<h3>
									<span>Female</span> Urology
								</h3>
								<p>Urinary incontinence, overactive bladder and pelvic organ prolapsed. Proper knowledge of urodynamic skills along with female pelvic floor together is required for diagnosing and treating these disorders.</p>
								
								<h3>Andrology</h3>
								<p>Male reproductive system such as erectile dysfunction, ejaculatory disorders and male infertility. Andrology usually overlaps with endocrinology as male sexuality is majorly controlled by hormones. </p>
								<h3>
									<span>Urology </span>Procedures
								</h3>
								<p>The common urology procedures include -</p>
								<ul>
									<li><span>Bladder surgery</span></li>
									<li><span>Renal (Kidney) surgery</span></li>
									<li><span>Surgery to the penis</span></li>
									<li><span>Kidney removal (nephrectomy)</span></li>
									<li><span>Urethra surgery</span></li>
									<li><span>Surgery of the ureters such as removal of calculus in the ureters</span></li>
									<li><span>Testicular surgery</span></li>
									<li><span>Removal of the prostate or prostatic surgery</span></li>
									<li><span>Pelvic lymph node dissection</span></li>
								</ul>
								<h3>
									<span>Urologic </span>Surgery
								</h3>
								<p>When medication is unable to relieve symptoms, then surgery is recommended for treating urologic condition. Some of the conditions are kidney or bladder cancer, prostate cancer, vesicoureteral reflux and ureteropelvic junction (UPJ). Laparoscopy is less invasive surgery. </p>
								<h3>
									<span>Robotic</span> Surgery
								</h3>
								<p>Robotic-assisted Laparoscopic Radical Cystectomy: This is a minimally invasive surgical procedure is primarily performed for bladder cancer. Da Vinci surgical system is utilized in this procedure where the movements of a surgeon are imitated by a robotic arm in order to increase their precision. This is considered as the effective surgery for those patients who are suffering from bladder cancer. During the procedure, small incisions are made without affecting vital muscle tissue and nerve. This procedure results in shorter hospital stays and faster recovery time.</p>
								<h5>
									To get best options for Urology treatment, send us a query or
									write to us at <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
								</h5>
							</div>
							<!-- End Tab -->

						</div>
					</div>
					<!-- End Column -->
				</div>
				<!-- End Row -->
			</div>
			<!-- End Tab Accordion -->

		</div>
		<!-- End Container -->
	</div>
	<!-- Speciality Content Ends Here -->

<%@ include file="newFooter.jsp"%>

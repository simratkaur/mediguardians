<%@ include file="newHeader.jsp"%></section> <section class="sec-user">
            <div class="row marg">
                <div class="col-lg-12">
                    <div class="user-q-main">
                        <div class="user-con-m">
                            <div class="user-s">
                                <p><span class="fa fa-check"></span> Consult a certified doctor-get answers to your medical queries </p>
                                <p><span class="fa fa-check"></span> Take a second opinion from the comfort of your home </p>
                                <p><span class="fa fa-check"></span> Free assistance and support to help you chose the right doctor</p>
                                <p><span class="fa fa-check"></span> Get a medical opinion within hours</p>
                                <p><span class="fa fa-check"></span> Multiple follow up consults at no extra charges</p>
                                <p><span class="fa fa-check"></span> Upload multiple files, prescriptions, reports, X-rays and scans</p>
                            </div>
                            <a href="askQuery"><button class="btn btn-blue">Consult Now</button></a>
                        </div>

                    </div>
                </div>
            </div>
        </section>
        <%@ include file="newFooter.jsp"%>
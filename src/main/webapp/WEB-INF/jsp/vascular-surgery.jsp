<%@ include file="newHeader.jsp"%></section>
<!-- Hospital Short Details Starts Here -->
<div class="short-details">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="title">
					<h1>Cardiology</h1>
					<ul class="paginator">
						<li><a href="/">Home</a></li>
						<li><a>Cardiology</a></li>
					</ul>
				</div>
			</div>
			<!-- End Column -->
		</div>
		<!-- End Row -->
	</div>
	<!-- End Container -->
</div>
<!-- Hospital Short Details Ends Here -->

<!-- Specialty Content Starts Here -->
<div class="speciality-content-area">
	<div class="container">

		<div class="innertab-accordion">
			<div class="row">
				<div class="col-md-3 tabnopaddright">
					<div class="tabs">
						<ul>
							<li><a href="cardiology-treatment">Cardiology</a></li>
							<li><a href="coronary-angiography">Coronary Angiography</a></li>
							<li><a href="coronary-angioplasty">Coronary Angioplasty</a></li>
							<li><a href="coronary-artery-bypass-surgery">Coronary
									Artery Bypass</a></li>
							<li><a href="paediatric-cardiac-surgery">Paediatric
									Cardiac Surgery</a></li>
							<li><a href="pacemaker-implantation">Pacemaker
									Implantation</a></li>
							<li class="tab-active"><a href="vascular-surgery">Vascular Surgery</a></li>
							<li><a href="heart-valve-replacement-surgery">Heart
									Valve Replacement </a></li>
							<li><a href="cardiac-diagnostic">Cardiac Diagnostics</a></li>
						</ul>
					</div>
				</div>
				<!-- End Column -->
				<div class="col-md-9 tabnopaddleft">
					<div class="accordion-contentarea">

						<div id="tab7" >
							<h2>
								<span>Vascular Surgery</span> in India
							</h2>
							<p>We at Arvene Healthcare have the expertise to organize for
								all treatments and surgeries related to Cardiology. Arvene
								Healthcare is associated with top hospitals and Doctors of India
								in this field. We can give you more and better options so that
								you can take the right decisions. We also assist you in taking
								best decision based on your requirement. Arvene Healthcare will
								also arrange for your stay and local travel, so that you have
								nothing else to worry about but your treatment. Please contact
								us on <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a> today to avail best services.</p>


							<h3>Vascular Surgery:</h3>
							<div class="row">
								<div class="col-md-9 col-sm-9 nopaddleft">
									<p>Vascular Surgery deals with surgical interventions of
										arteries and veins. It cures diseases of the vascular system
										and deals with treatment of the areas of dilatation of the
										blood vessel that can rupture if not treated.</p>
									<p>Blockages of blood vessels can cause lack of blood flow
										to the body, stroke and other serious complications. The
										treatment requires removal of the area of blockage.</p>
								</div>
								<!-- End Inner Column -->
								<div class="col-md-3 col-sm-3 nopaddright">
									<img src="images/specialities/7.vascular surgery.jpg"
										alt="vascular surgery" class="img-responsive">
								</div>
								<!-- End Inner Column -->
							</div>
							<!-- End Inner Row -->
							<p>Veins contain leaflet valves for preventing blood from
								flowing backwards when the veins become varicose (enlarged),
								they do not meet properly, and the valves do not work allowing
								the blood to flow backwards and they are enlarged even more.</p>


							<h3>
								<span>Causes for</span> Cardiovascular Disease
							</h3>
							<ul>
								<li><span>Long hours of standing or sitting</span></li>
								<li><span>Heredity</span></li>
								<li><span>Female hormones, especially those of
										pregnancy</span></li>
								<li><span>Muscular compartment forces travelling
										through connecting veins, create higher pressure in the
										superficial veins leading to the enlargement of the vessels.</span></li>
							</ul>

							<h3>
								<span>Symptoms of</span> Varicose veins
							</h3>
							<ul>
								<li><span>Swelling in the ankles</span></li>
								<li><span>Heavy legs, worsened at night</span></li>
								<li><span>Appearance of spider veins
										(telangiectasia)</span></li>
								<li><span>Discoloration near the affected veins
										(brownish-blue)</span></li>
								<li><span>Even small tale a long time to heal</span></li>
								<li><span>Redness, dryness, and itchiness of areas
										of skin</span></li>
							</ul>

							<h3>
								<span>Vascular</span> Surgery
							</h3>
							<p>One of the prime treatments of varicose veins is surgery.
								Another kind of supportive treatments can be medicines and
								compression stocking. Saphenous-femoral junction ligation and
								vein stripping operation involves the removal of all or a part
								of the saphenous vein main trunk. Other procedures include:
								Endovenous Laser Ablation and Sclerotherapy'</p>


							<h3>
								<span>Carotid Artery</span> Disease
							</h3>
							<p>Deposition of plaque in one of the two carotid arteries is
								called Carotid artery disease. A duplex ultrasound is performed
								that makes use of painless sound waves for showing the blood
								vessels and measuring the speed of the blood flow.</p>
							<h3>
								<span>Carotid</span> Endarterectomy
							</h3>
							<p>Carotid endarterectomy (CEA) is a surgical process where
								fatty deposits that block one of the two carotid arteries, is
								removed, improving the blood flow to brain. It is mainly
								performed in old age people.</p>




							<h3>
								<span>Postoperative care of</span> Vascular Surgery
							</h3>
							<ul>
								<li><span>Lifestyle changes such as less consumption
										of saturated fat, cholesterol and calories</span></li>
								<li><span>Regular Exercise, walking, Avoidance of
										smoking</span></li>

								<li><span>Maintain ideal body weight</span></li>

							</ul>

							<h5>
								To get best options for cardiology treatment, send us a query or
								write to us at <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a>
							</h5>
						</div>
						<!-- End Tab -->

					</div>
				</div>
				<!-- End Column -->
			</div>
			<!-- End Row -->
		</div>
		<!-- End Tab Accordion -->

	</div>
	<!-- End Container -->
</div>
<!-- Speciality Content Ends Here -->

<%@ include file="newFooter.jsp"%>
<%@page import="com.medicine.arvene.util.CommonUtil"%>
<%@ include file="newHeader.jsp"%></section>


<section class="sec-main sec-drl sec-drp">
            <div class="container">
                <div class="drl-bgwhite">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                            <div class="drp-img">
                               <img src="${specialist.profileImage}" />
                            </div>
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">
                            <div class="drp-con">
                                <label>
								${specialist.firstName } ${specialist.middleName }
								${specialist.lastName }</label>
                                <p> ${specialist.degrees}</p>
                                <p></p>
                                <div class="row margtop">
                                    <div class="col-lg-3 col-sm-3 col-xs-5 col-md-3">
                                        <p class="drl-col">Specialty</p>
                                    </div>
                                    <div class="col-lg-3 col-sm-3 col-xs-7 col-md-3">
                                        <p>${specialist.speciality.name}</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-3 col-sm-3 col-xs-5 col-md-3">
                                        <p class="drl-col">Designation</p>
                                    </div>
                                    <div class="col-lg-3 col-sm-3 col-xs-7 col-md-3">
                                        <p>${specialist.title}</p>
                                    </div>
                                </div>
                                <div class="row ">
                                    <div class="col-lg-3 col-sm-3 col-xs-5 col-md-3">
                                        <p class="drl-col">Hospital Affiliation</p>
                                    </div>
                                    <div class="col-lg-3 col-sm-3 col-xs-7 col-md-3">
                                        <p>${specialist.hospital}</p>
                                    </div>
                                </div>
                                <div class="row ">
                                    <div class="col-lg-2 col-sm-12 col-xs-12 col-md-2" style="margin-right: 10px">
                                        <button class="btn btn-primary margtop" 
                                         onClick="location.href='askQuery?uid=${specialist.user.id}'">Text Consultation</button>
                                        <p>Fee : <i class="fa fa-dollar"></i>${specialist.queryFee}</p>
                                    </div>
                                    <div class="col-lg-2 col-sm-12 col-xs-12 col-md-2">
                                        <button class="btn btn-primary margtop" 
                                        onClick="location.href='viewcalendar?uid=${specialist.user.id}'">Video Consultation</button>
                                        <p>Fee :<i class="fa fa-dollar"></i>${specialist.sessionFee}</p>
                                    </div>
                                </div>
                                
                                
                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="drl-bgwhite drp-pad margtop">
                    <div class="drp-bot">
                        <h3 class="drl-col">Biography:</h3>
                        <p class="margtop">${specialist.biography }</p>
                    </div>
                </div>
            </div>
        </section>

<%@ include file="newFooter.jsp"%>
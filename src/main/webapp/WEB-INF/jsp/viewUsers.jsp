<%@ include file="newHeader.jsp"%></section>




<section class="sec-main sec-drl">
	<div class="container">
		<div class="drl-bgwhite conn-bttn">

			<form id="filterForm" class="center" action="viewUsers" method="GET"
				class="form-inline">
				<div class="row">

					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
						<div class="alu-input">
							<input type="text" class="form-control datePickerCurrent" placeholder="Start Date"
								id="startDate" name="startDate"  value="${ startDate}">
						</div>
					</div>
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
						<div class="alu-input">
							<input type="text" class="form-control datePickerCurrent" placeholder="End Date"
								id="endDate" name="endDate" value="${ endDate}">
						</div>
					</div>
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
						<div class="alu-input">
							<select class="form-control" name="userType">
								<option selected value="A">All</option>
								<option value="S">Doctors</option>
								<option value="U">Users</option>
							</select>
						</div>
					</div>
					<div class="col-lg-2 col-md-2 col-sm-5 col-xs-5">
						<div class="alu-input">
							<select class="form-control" name="active">
								<option selected value="-1">All</option>
								<option value="1">Active</option>
								<option value="0">Inactive</option>
							</select>
						</div>
					</div>

					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">

						<span class="fa fa-lg fa-search"
							onclick="$('#filterForm').submit();"></span>

					</div>



					<!-- <input type="submit" class="btn btn-primary" value="Search" /> -->


				</div>
			</form>
		</div>
		<div class="drl-bgwhite uqt-main">

			<c:choose>
				<c:when test="${fn:length(users) == 0}">
					<!-- <div>
						<h4 class="list-group-item-text">There are no users found.</h4>
					</div> -->
				</c:when>
				<c:otherwise>
					<%-- <div>
						<h6 class="list-group-item-text" style="margin: 10px;">${totalUsers}
							users found.</h6>
					</div> --%>
					<div class="table-responsive">
						<table class="table table-condensed">
							<thead class="uqt-tab-head">
								<tr>
									<th>Sr.No</th>
									<th>Email</th>
									<th>Phone</th>
									<th>User Type</th>
									<th>Signup Date</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>

								<c:forEach items="${users}" var="user" varStatus="userIndex">
									<tr
										class="${(userIndex.index % 2 eq 0) ? 'custom-color-tr' : ''}">
										<td>${start + userIndex.index +1 }</td>
										<td>${user.username }</td>
										<td>${user.phone}</td>
										<td><c:choose>
												<c:when test="${user.userType eq 'S' }">Doctor</c:when>
												<c:otherwise>Patient</c:otherwise>
											</c:choose></td>

										<td>${user.created}</td>
										<td><c:choose>
												<c:when test="${user.isActive eq true }">
													<form method="POST" action="updateUserStatus"
														id="deactivateForm-${user.id}">
														<input type="hidden" name="id" value="${user.id }" />
														<!-- 		<span class="dacti-allu">
														<button type="submit" name="active" value="false"
														class="btn btn-default">Deactivate</button></span> -->

														<input type="hidden" name="active" value="false" /> <span
															class="dacti-allu"
															onclick="$('#deactivateForm-${user.id}').submit();">Deactivate</span>
													</form>

												</c:when>
												<c:otherwise>

													<form method="POST" action="updateUserStatus"
														id="activateForm-${user.id}">
														<input type="hidden" name="id" value="${user.id }" />
														<!-- <span class="acti-allu"> </span>-->
														<!-- <button type="submit" name="active" value="true"
														class="">Activate</button> -->

														<input type="hidden" name="active" value="false" /> <span
															class=acti-allu
															onclick="$('#activateForm-${user.id}').submit();">Activate</span>
													</form>




													<!-- <span class="acti-allu"><button type="button"
												class="btn btn-default">Active</button></span> -->
												</c:otherwise>
											</c:choose></td>
									</tr>
								</c:forEach>

							</tbody>
						</table>
					</div>
				</c:otherwise>
			</c:choose>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="drl-btn">

					<div id="pagination"></div>

				</div>
			</div>
		</div>
	</div>
</section>



<%-- <div class="container">
	<div class="mainContentBigger">
		<h3 class="pageHeader" style="margin: auto;">Users</h3>

		<div class="center">
			<form id="filterForm" class="center" action="viewUsers" method="GET"
				class="form-inline">

				<div class="">
					<div class="row">
						<input type="text" name="startDate" class="datePickerCurrent"
							value="${ startDate}" /> <input type="text" name="endDate"
							class="datePickerCurrent" value="${ endDate}" /> <select
							class="span2" name="userType">
							<option selected value="A">All</option>
							<option value="S">Doctors</option>
							<option value="U">Users</option>
						</select> <select class="span2" name="active">
							<option selected value="-1">All</option>
							<option value="1">Active</option>
							<option value="0">Inactive</option>
						</select> <input type="submit" class="btn btn-primary" value="Search" />
					</div>

				</div>



			</form>


		</div>


		<c:choose>
			<c:when test="${fn:length(users) == 0}">
				<div>
					<h4 class="list-group-item-text">There are no users found.</h4>
				</div>
			</c:when>
			<c:otherwise>
				<div>
					<h6 class="list-group-item-text" style="margin: 10px;">${totalUsers}
						users found.</h6>
				</div>
				<table class="table table-striped rwd-table">
					<thead>
						<tr>
							<th style="width: 5%;">S.NO</th>
							<th style="width: 20%;">Email</th>
							<th style="width: 30%;">Phone</th>
							<th style="width: 10%;">User Type</th>
							<th style="width: 15%;">Sign Up Date</th>
							<th style="width: 20%;">Active</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${users}" var="user" varStatus="userIndex">
							<tr>
								<td>${start + userIndex.index +1 }</td>
								<td>${user.username}</td>
								<td>${user.phone}</td>
								<td>${user.userType}</td>
								<td>${user.created}</td>
								<td>${user.isActive}</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</c:otherwise>
		</c:choose>

	</div>
	<div id="pagination"></div>
</div> --%>
<script type='text/javascript'>

var startDate='${startDate}' ;
var endDate='${endDate}' ;
var userType='${userType}';
var active='${active}';
var options = {
		

		currentPage : '${currentpage}',
		totalPages : '${pageno}',
		onPageClicked : function(e, originalEvent, type, page) {
			fetchUsers(page,startDate,endDate,userType, active)
		},
		size:'medium',
		alignment : 'center'
	};
	$('#pagination').bootstrapPaginator(options);

	function fetchUsers(page,startDate,endDate,userType, active) {
		var start = (page * 10) - 10;
		window.location.href="viewUsers?start="+start+"&range=10"+"&startDate="+startDate+"&endDate="+endDate+
				"&userType="+userType+"&active="+active;
	}
	$('.nav >li.active').removeClass("active");
	$('.nav #queries').addClass("active");

</script>

<script type="text/javascript">

$(document).ready(

		
		function() {
			$(".datePickerCurrent").datepicker({
			      changeMonth : true,
			      changeYear : true,
			/*       yearRange : '-50:+1', 
			      showButtonPanel : true, */
				 dateFormat : 'dd/mm/yy',
			       maxDate: '0', 
			      minDate: new Date(2016, 02, 16),
			       beforeShow : function(input, inst) {
						var cal = inst.dpDiv;
						var top = $(this).offset().top + $(this).outerHeight();
						var left = $(this).offset().left;
						setTimeout(function() {
							cal.css({
								'top' : top,
								'left' : left
							});
						}, 10);
					}
			  }
			
			  )
 
		});

</script>

<%@ include file="newFooter.jsp"%>
<%@ include file="newHeader.jsp"%></section>
<!-- Required javascript files for Slider -->
<script src="js/jquery.ba-cond.min.js"></script>
<script src="js/jquery.slitslider.js"></script>
<!-- <script src="js/bootstrap-dialog.js"></script>
 --><script src="js/select2.min.js"></script>
<script src="js/moment.js"></script>
<script src="js/fullcalendar.js"></script>
<script src="js/bootstrap-tagsinput.js"></script>



<link rel="stylesheet" href="css/bootstrap-responsive.min.css">
<link rel="stylesheet" href="css/sl-slide.css">
<script src="js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
<link rel="stylesheet" href="css/intlTelInput.css">
<script src="js/intlTelInput.js"></script>

<script>
	window.onload = function() {
		$("#phone1").intlTelInput();
		$("#phone2").intlTelInput();
	}
	
	function updatePhone(){
		
		var ph1 = $("#phone1").val();
		var ph = $("#phone1").intlTelInput("getSelectedCountryData");
		var completePhone = "+"+ph.dialCode+ph1;		
		 $("#req_phone").val(completePhone);		
	}
	
	function updatePhoneBooking(){
		var ph1 = $("#phone2").val();
		var ph = $("#phone2").intlTelInput("getSelectedCountryData");
		var completePhone = "+"+ph.dialCode+ph1;		
		 $("#book_phone").val(completePhone);	
	}
	
	(function () {
		
		var selDiv = "";
	    
	    document.addEventListener("DOMContentLoaded", init, false);
	    
	    function init() {
	    	
	    	 document.querySelector('#newProfileImage').addEventListener('change', handleFileSelect, false);
		     selDiv = document.querySelector("#selectedFiles");
		        
	        document.querySelector('#newProfileImageBook').addEventListener('change', handleFileSelect, false);
	        selDivBook = document.querySelector("#selectedFilesBook");
	    }
	        
	    function handleFileSelect(e) {
	    	
	    	console.log(e);
	        
	        if(!e.target.files || !window.FileReader) return;

	        selDiv.innerHTML = "";
	        
	        var files = e.target.files;
	        var filesArr = Array.prototype.slice.call(files);
	        
	        console.log(filesArr);
	        
	        
	        
	        filesArr.forEach(function(f,i) {
	            var f = files[i];
	            var html = "<li>" + f.name + "</li>";
	            selDiv.innerHTML += html;  
	        });
	        
	    }
		
	})();
	
(function () {
		
		var selDiv = "";
	    
	    document.addEventListener("DOMContentLoaded", init, false);
	    
	    function init() {
	    	
	        document.querySelector('#newProfileImageBook').addEventListener('change', handleFileSelect, false);
	        selDivBook = document.querySelector("#selectedFilesBook");
	    }
	        
	    function handleFileSelect(e) {
	    	
	    	console.log(e);
	        
	        if(!e.target.files || !window.FileReader) return;
	        selDivBook.innerHTML = "";
	        
	        var files = e.target.files;
	        var filesArr = Array.prototype.slice.call(files);
	        
	        console.log(filesArr);
	        
	        
	        
	        filesArr.forEach(function(f,i) {
	            var f = files[i];
	            var html = "<li>" + f.name + "</li>";
	            selDivBook.innerHTML += html;  
	        });
	        
	    }
		
	})();
	
	
$(document).ready(

function() {
	$(".timePicker").timepicker({
		stepMinute : 15
	});
	
	$(".datepickFuture").datepicker({
		yearRange : '-100:+0',
		changeYear : true,
		changeMonth : true,
		minDate : "0",
		dateFormat : 'dd/mm/yy',
		beforeShow : function(input, inst) {
			var cal = inst.dpDiv;
			var top = $(this).offset().top + $(this).outerHeight();
			var left = $(this).offset().left;
			setTimeout(function() {
				cal.css({
					'top' : top,
					'left' : left
				});
			}, 10);
		}
	})
});
</script>
<section>
		<c:choose>
			<c:when test="${ userType eq 'A' && bookingId ge 0 }">
			
				<form id="schedule" action="acceptBookingRequest" method="POST" enctype="multipart/form-data" >
						<input type="hidden"  name="ownerid" value ="${uid}"/>
						<input type="hidden"  name="bookingId" value ="${bookingId }"/>
						
						<div class="row form-group">
							<div class="col-sm-offset-3 col-sm-3">
								<input class="datepickFuture form-control" placeholder="Start Date" type="text" id="startdatepicker" 
									name="date" />
									
							</div>
							<div class="col-sm-3">
							<input type="text" name="time" class="timePicker form-control"  />
							</div>
							
							
							
						
						</div>
						
						<div class="row form-group btn-footer">
                            
                                <div class="col-sm-offset-3 col-sm-3">
                                    <label class="btn btn-default btn-file">
                                        Browse <input type="file" style="display: none;"  name="files"
											id="newProfileImageBook"  multiple
											data-validation-max-size="2M">
		                             </label>
                                </div>
                                
                                <div class="col-sm-offset-3  col-sm-3"><button type="submit" class="btn btn-primary">Save</button></div>
                                
                                <div class="help-block">                               
	                                <div class="sig-btn more ">
	                                   <div id="selectedFilesBook" ></div>
	                                </div>
	                            </div>
                                
                                
								
                            </div>
						
				
				</form>
			
			</c:when>	
		</c:choose>
	
</section>


<section class="sec-calender">
	<div class="width-80">
		<div class="row marg">
			<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
				<div class="calend-left-pro">
					<div class="img-calend-pro">
						<img class="img-cal-pro" src="${specialist.profileImage }" />
						<h3>${specialist.firstName}
							${specialist.lastName}</h3>
						<label>(${specialist.degrees})</label>
						<p>${specialist.title}</p>
						<p>${specialist.speciality.name }</p>
						<!-- <p>23 years in practice</p> -->
						<p>${specialist.hospital}</p>
						<p>${specialist.hospitalCity}</p>
					</div>

				</div>
			</div>
			<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">

				<div class="calend-right-main">
					<div class="">
						<div id="calendar"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>






<%-- <section class="sec-main sec-drl sec-drp">
	<div class="container">
		<div class="drl-bgwhite">
			<div class="">
				<div class="col-sm-12">
					<img class="drImage" src="${specialist.profileImage } " />
					<div class="drDetails">
						<h4 class="drName">${specialist.title}${specialist.firstName}
							${specialist.lastName}(${specialist.degrees})</h4>
						<div class="drDescription">
							<div>${specialist.speciality.name }</div>

							<c:if test="${specialist.yearsOfExp gt 0}">
					<div class="drDescStart">${specialist.yearsOfExp}&nbsp;years&nbsp;in&nbsp;
						practice</div> 

				</c:if>
							<div class="drDescEnd">${specialist.hospital}&nbsp;
								${specialist.hospitalCity}</div>
								
							 <a class="btn btn-primary margtop" 
                                      onClick="location.href='requestBooking?uid=${specialist.user.id}'" 
                                        >Request Video Consultation</a>
                                        <p>Fee Rs:${specialist.sessionFee}</p>
						</div>
					</div>
				</div>
				<div class="queryContentRight">
					<div id="calendar"></div>
					<div style="text-align: center;">
						<!-- <img id="loadingIMG"
							src="images/processing.gif" alt="" /> -->
					</div>
				</div>
			</div>
		</div>
	</div>
</section>  --%>


<!--Request bookingModal  -->
        <div class="modal fade" id="reqBooking">
            <div class="modal-dialog">
                <div class="modal-content patient-modal">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">No slots are Available. Request A Booking.</h4>
                        <h6 class="validateTips">All form fields are required.</h6>
                    </div>
                    <div class="modal-body">
                        <form id="reqbookingform" method="POST" action="requestBooking" enctype="multipart/form-data" 
                        		 class="patient-modal-form" role="form">
                        
                        
                      			<input type="hidden" name="uid" id="uid"
									value="${specialist.user.id }">

							<input type="hidden" name="${_csrf.parameterName}"
								value="${_csrf.token}" />
							<div class="form-group">
                                <input type="text" class="form-control patient-modal-input" name="title"
									id="title_req"  placeholder="Booking Title">
                            </div>
                            
                            <div class="form-group">            
								<input type="text" name="phone1"  placeholder="phone"
									id="phone1" class="form-control patient-modal-input"  onchange="updatePhone();">
								<input type="hidden" name="phone" id="req_phone" />					
                            </div>
                      
                            <div class="form-group">
                            
                            <textarea name="desc" maxlength="2000" rows="3" id="desc_req" placeholder="Booking Description"
									class="form-control patient-modal-input"></textarea>
                            </div>

							
                            <div class="form-group btn-footer">
                            
                                <div class="col-sm-6 nopadding-right">
                                    <label class="btn btn-default btn-file">
                                        Browse <input type="file" style="display: none;"  name="files"
											id="newProfileImageBook"  multiple
											data-validation-max-size="2M">
		                             </label>
                                </div>
                                <div class="col-sm-6 nopadding-right">
                                    <button type="submit" class="btn btn-default pull-right btn-book">Request Booking</button>
                                </div>
                                
                                <div class="help-block">
	                                <span>You can to send the reports at <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a> <br/> or WhatsApp at 
	                                 +91-81300-77375 after submitting your query.<br/>
	                                   In case of multiple files, select all in one go and click on attach files</span><br>
	                                <div class="sig-btn more ">
	                                   <div id="selectedFilesBook" ></div>
	                                </div>
	                            </div>
                                
                                
								
                            </div>
                            
                        
                        </form>
                    </div>
                    
                </div>
            </div>
        </div>
        
        <!-- Request bookingModal  end -->

<!--  Old Request booking Modal-->
<div id="reqBooking_old" class="modal fade" role="dialog">
	<div class="modal-dialog mo-co-dailog">
		<!-- Modal content-->
		<div class="modal-content mo-co-con">
			<div class="modal-header mo-co-head">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">No slots are Available. Request A Booking</h4>
				<h6 class="validateTips text-right">All form fields are required.</h6>
			</div>
			<div class="modal-body mo-co-body">
				<div class="form-bot">
					<form id="reqbookingform" method="POST" action="requestBooking" enctype="multipart/form-data">
						<fieldset>
							<div class="form-group row">
								
								<div class="col-sm-3"><label for="name">Title</label></div>
								<div class="col-sm-8"> <input type="text" name="title"
									id="title_req" class="form-control">
								</div>
							</div>
							<div class="row form-group">
								<div class="col-sm-3"><label for="phone1">Phone</label></div>
								<div class="col-sm-8"><input type="text" name="phone1"
									id="phone1" class="form-control"  onchange="updatePhone();"></div>
							<input type="hidden" name="phone" id="req_phone" />
							</div>

							<div class="row form-group">
								<div class="col-sm-3"><label for="email">Description</label></div>
								<div class="col-sm-8"><textarea name="desc" maxlength="2000" rows="5" id="desc_req"
									class="form-control"></textarea></div>
								
								<input type="hidden" name="uid" id="uid"
									value="${specialist.user.id }">
							</div>
							<div class="row form-group">
							 <div class="sig-btn  col-sm-offset-3 col-sm-8">
								<label class="btn btn-block bg-blue"
									style="text-align: center; margin-top: 10px; height: 41px; border-radius: 0;">
									Attach files<input type="file" name="files"
									id="newProfileImage"  multiple
									data-validation-max-size="2M"
									
									style="width: 50%; display: inherit; opacity: 0; -moz-opacity: 0; filter: progid:DXImageTransform.Microsoft.Alpha(opacity=0)" />
								</label>
							</div>
								
							</div>
							
							<div class="row form-group left">
							<div class=" col-sm-offset-3 col-sm-8 help-block">
                                 <span>You can to send the reports at <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a> <br/> or WhatsApp at 
                                 +91-9910665551 after submitting your query.<br/>
                                   In case of multiple files, select all in one go and click on attach files</span><br>
                                
                                <div class="sig-btn more">
                                   <div id="selectedFiles"></div>
                                </div>
                            </div>
                            </div>

							<div class="row form-group">
							<button type="submit" class="btn btn-primary">Submit</button>
							</div>

							<input type="hidden" name="${_csrf.parameterName}"
								value="${_csrf.token}" />

						</fieldset>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>


<!-- bookingModal  -->
        <div class="modal fade" id="bookingModal">
            <div class="modal-dialog">
                <div class="modal-content patient-modal">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Add Booking Information</h4>
                    </div>
                    <div class="modal-body">
                        <form id="bookingform" method="POST" action="addbookinginfo" enctype="multipart/form-data" 
                        		 class="patient-modal-form" role="form">
                        
                        
                      			<input type="hidden" name="slotid" id="slotid">
								 <input type="hidden" name="subscriberid" id="subscriberid">

							<input type="hidden" name="${_csrf.parameterName}"
								value="${_csrf.token}" />
							<div class="form-group">
                                <input type="text" class="form-control patient-modal-input" name="title"
									id="title_req"  placeholder="Booking Title">
                            </div>
                            
                            <div class="form-group"> 
                                <input type="text" name="phone1"  placeholder="Phone"
									id="phone2" class="form-control patient-modal-input"  onchange="updatePhoneBooking();">
								<input type="hidden" name="phone" id="book_phone" />							
                            </div>
                      
                            <div class="form-group">
                            
                            <textarea name="desc" maxlength="2000" rows="3" id="desc_req" placeholder="Booking Description"
									class="form-control patient-modal-input"></textarea>
                            </div>

                            <div class="form-group btn-footer">
                                <div class="col-sm-6 nopadding-right">
                                    <label class="btn btn-default btn-file">
                                        Browse <input type="file" style="display: none;"  name="files"
											id="newProfileImageBook"  multiple
											data-validation-max-size="2M">
		                             </label>
                                </div>
                                <div class="col-sm-6 nopadding-right">
                                    <button type="submit" class="btn btn-default pull-right btn-book">Book Video</button>
                                </div>
                                
                                
								<div class="help-block">
	                                <span>You can to send the reports at <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a> <br/> or WhatsApp at 
	                                 +91-9910665551 after submitting your query.<br/>
	                                   In case of multiple files, select all in one go and click on attach files</span><br>
	                                <div class="sig-btn more ">
	                                   <div id="selectedFilesBook" ></div>
	                                </div>
	                            </div>
                            </div>
                            
                            
                         
                        
                        </form>
                    </div>
                    
                </div>
            </div>
        </div>
        
        <!-- bookingModal  end -->


<!-- Old Booking Modal-->

<div id="bookingModal1" class="modal fade" role="dialog">
	<div class="modal-dialog mo-co-dailog">
		<!-- Modal content-->
		<div class="modal-content mo-co-con">
			<div class="modal-header mo-co-head">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Add Booking Information</h4>
				<h6 class="validateTips">All form fields are required.</h6>
			</div>
			<div class="modal-body">
				<div class="form-bot">
					<form id="bookingform" method="POST" action="addbookinginfo" enctype="multipart/form-data">
						<fieldset>
							<div class="form-group row">
								
								<div class="col-sm-3"><label for="name">Title</label></div>
								<div class="col-sm-8"> <input type="text" name="title"
									id="title_req" class="form-control">
								</div>
							</div>
							<div class="row form-group">
								<div class="col-sm-3"><label for="phone1">Phone</label></div>
								<div class="col-sm-8"><input type="text" name="phone1"
									id="phone2" class="form-control"  onchange="updatePhoneBooking();"></div>
							<input type="hidden" name="phone" id="book_phone" />
							</div>

							<div class="row form-group">
								<div class="col-sm-3"><label for="email">Description</label></div>
								<div class="col-sm-8"><textarea name="desc" maxlength="2000" rows="5" id="desc_req"
									class="form-control"></textarea></div>
								
								<input type="hidden" name="uid" id="uid"
									value="${specialist.user.id }">
							</div>
							<div class="row form-group">
							 <div class="sig-btn  col-sm-offset-3 col-sm-8">
								<label class="btn btn-block bg-blue"
									style="text-align: center; margin-top: 10px; height: 41px; border-radius: 0;">
									Attach files<input type="file" name="files"
									id="newProfileImageBook"  multiple
									data-validation-max-size="2M"
									
									style="width: 50%; display: inherit; opacity: 0; -moz-opacity: 0; filter: progid:DXImageTransform.Microsoft.Alpha(opacity=0)" />
								</label>
							</div>
							</div>
							<div class="row form-group left">
							<div class=" col-sm-offset-3 col-sm-8 help-block">
                                <span>You can to send the reports at <a href="mailto:contact@arvenehealthcare.com">contact@arvenehealthcare.com</a> <br/> or WhatsApp at 
                                 +91-9910665551 after submitting your query.<br/>
                                   In case of multiple files, select all in one go and click on attach files</span><br>
                                <div class="sig-btn more ">
                                   <div id="selectedFilesBook" ></div>
                                </div>
                            </div>
                            </div>

							<div class="row form-group">
							<button type="submit" class="btn btn-primary">Submit</button>
							</div>
							
							<input type="hidden" name="slotid" id="slotid">
							 <input type="hidden" name="subscriberid" id="subscriberid">

							<input type="hidden" name="${_csrf.parameterName}"
								value="${_csrf.token}" />

						</fieldset>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>


<!-- Error slot -->

<div id="bookedMessage" class="modal fade" role="dialog">
	<div class="modal-dialog mo-co-dailog error-modal">
		<!-- Modal content-->
		<div class="modal-content mo-co-con">
			<div class="modal-header mo-co-head">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title error">Booked already!</h4>
			</div>
			<div class="modal-body mo-co-body">
			
				<div>This session is already booked.Please try any other slot.</div>
				
			</div>
		</div>
	</div>
</div>


<!--  Dashboard div start -->
<%-- <div id="dialog-form" title="Add Booking Information">
	<p class="validateTips">All form fields are required.</p>

	<form id="bookingform" method="POST" action="addbookinginfo"
		enctype="multipart/form-data">
		<fieldset>
			<div class=" form-group">
				<label for="name">Title</label> <input type="text" name="title"
					id="title" class="text ui-widget-content ui-corner-all">
			</div>

			<label for="patientName">Patient Name</label> <input type="text"
				name="patientName" id="patientName"
				class="text ui-widget-content ui-corner-all"> <label
				for="phone">Phone</label> <input type="text" name="phone" id="phone"
				class="text ui-widget-content ui-corner-all"> <label
				for="desc">Description</label>
			<textarea name="desc" maxlength="2000" rows="5" name="desc" id="desc"
				class="text ui-widget-content ui-corner-all"></textarea>

			<div class="sig-btn">
				<label class="btn btn-block bg-blue"
					style="text-align: center; margin-top: 10px; height: 41px; border-radius: 0;">
					Attach query files<input type="file" name="files"
					id="newProfileImage" multiple data-validation-max-size="2M"
					style="width: 50%; display: inherit; opacity: 0; -moz-opacity: 0; filter: progid:DXImageTransform.Microsoft.Alpha(opacity=0)" />
				</label>
			</div>

			<input type="hidden" name="slotid" id="slotid"> <input
				type="hidden" name="subscriberid" id="subscriberid">
			<!-- Allow form submission with keyboard without duplicating the dialog button -->
			<input type="submit" tabindex="-1"
				style="position: absolute; top: -1000px"> <input
				type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />

		</fieldset>
	</form>
</div> --%>

<script>
	/* $(document).ready(
			function() {
				title = $("#title"), desc = $("#desc");
				tips = $(".validateTips");

				function checkLength(o, n, min, max) {
					if (o.val().length > max || o.val().length < min) {
						o.addClass("ui-state-error");
						updateTips("Length of " + n + " must be between " + min
								+ " and " + max + ".");
						return false;
					} else {
						return true;
					}
				}

				function updateTips(t) {
					tips.text(t).addClass("ui-state-highlight");
					setTimeout(function() {
						tips.removeClass("ui-state-highlight", 1500);
					}, 500);
				}

				bookNow = function() {
					var valid = true;
					 valid = valid && checkLength(title, "title", 3, 20);
					valid = valid && checkLength(desc, "notes", 6, 20000); 
					if (valid) {
						$("#bookingform").submit();
					}

				};

				dialog = $("#dialog-form").dialog({
					autoOpen : false,
					height : 400,
					width : 500,
					modal : false,
					buttons : {
						"Book Now" : bookNow,
						Cancel : function() {
							dialog.dialog("close");
						}
					},
					close : function() {
						form[0].reset();
					}
				});

				form = dialog.find("form");
				
				
				
				title_req = $("#title_req"), desc_req = $("#desc_req");
				
				reqbooking = function() {
					var valid = true;
					valid = valid && checkLength(title_req, "title", 3, 20);
					valid = valid && checkLength(desc_req, "notes", 6, 20000);
					if (valid) {
						$("#reqbookingform").submit();
					}

				};

				
				dialogRequest = $("#request-booking").dialog({
					autoOpen : false,
					height : 400,
					width : 500,
					modal : false,
					buttons : {
						"Book Now" : reqbooking,
						Cancel : function() {
							dialogRequest.dialog("close");
						}
					},
					close : function() {
						form[0].reset();
					}
				});

			}); */

	$(document)
			.ready(
					function() {

						$("#calendar")
								.fullCalendar(
										{
											aspectRatio : 2,
											header : {
											//left: 'prev,next today'
											},
											defaultView : ($(window).width() < 514) ? 'agendaDay'
													: 'agendaWeek',
											slotDuration : '00:15:00',
											viewRender : function(currentView) {
												var minDate = moment(), maxDate = moment()
														.add(4, 'weeks');
												// Past
												if (minDate >= currentView.start
														&& minDate <= currentView.end) {
													$(".fc-prev-button").prop(
															'disabled', true);
													$(".fc-prev-button")
															.addClass(
																	'fc-state-disabled');
												} else {
													$(".fc-prev-button")
															.removeClass(
																	'fc-state-disabled');
													$(".fc-prev-button").prop(
															'disabled', false);
												}
												// Future
												if (maxDate >= currentView.start
														&& maxDate <= currentView.end) {
													$(".fc-next-button").prop(
															'disabled', true);
													$(".fc-next-button")
															.addClass(
																	'fc-state-disabled');
												} else {
													$(".fc-next-button")
															.removeClass(
																	'fc-state-disabled');
													$(".fc-next-button").prop(
															'disabled', false);
												}
											},
											events : function(start, end,
													timezone, callback) {
												$
														.ajax({
															url : 'fetchavailability?uid=${param.uid}',
															dataType : 'json',
															data : {
																start : start
																		.format('YYYY-MM-DD'),
																end : end
																		.format('YYYY-MM-DD')
															},
															success : function(
																	eventsdata) {
																
																
																console.log(eventsdata);
																var events = [];
																for (var i = 0; i < eventsdata.length; i++) {
																	events
																			.push(eventsdata[i]);
																}
																callback(events);
															},
															error : function(xhr,status,error){
																console.log(xhr,status,error);
																
																
															/* 	dialogRequest.dialog("open"); */
																$('#reqBooking').modal('show')
															}
															
															
														});

											},
											eventClick : function(calEvent,
													jsEvent, view) {
												if (calEvent.title != 'Booked') {
													$("#slotid").val(
															calEvent.id);
													$("#subscriberid").val(
															'${user.id}');
													/* dialog.dialog("open"); */
													$('#bookingModal').modal('show');
													
												} else {
													
													/* error = 'This session is already booked.Please try any other slot'; */
													$('#bookedMessage').modal('show');
													
												/* 	BootstrapDialog
															.show({
																type : BootstrapDialog.TYPE_DANGER,
																title : 'Booked already!',
																message : 'This session is already booked.Please try any other slot',
																buttons : [ {
																	label : 'OK',
																	action : function(
																			dialog) {
																		dialog
																				.close();
																	}
																} ]
															}); */
												}
											},
											windowResize : function(view) {
												if ($(window).width() < 514) {
													$('#calendar')
															.fullCalendar(
																	'changeView',
																	'agendaDay');
													$('#calendar')
															.fullCalendar(
																	'option',
																	'height',
																	get_calendar_height());
												} else if ($(window).width() < 769) {
													$('#calendar')
															.fullCalendar(
																	'changeView',
																	'agendaWeek');
													$('#calendar')
															.fullCalendar(
																	'option',
																	'height',
																	get_calendar_height());
												} else {
													$('#calendar')
															.fullCalendar(
																	'changeView',
																	'agendaWeek');
													$('#calendar')
															.fullCalendar(
																	'option',
																	'height',
																	get_calendar_height());
												}
											}
										})
					});
</script>

<script type='text/javascript'>

function get_calendar_height() {
    return $(window).height() - 100;
}
	$("html").removeClass("video");
	$('.nav >li.active').removeClass("active");
	$('.nav #schedule').addClass("active");
	 if ($(window).width() < 514) {
		$('#calendar').fullCalendar(
				'option',
				'height',
				get_calendar_height() );
	} else if ($(window).width() < 769) {
		$('#calendar').fullCalendar(
				'option',
				'height',
				get_calendar_height());
	} else {
		$('#calendar').fullCalendar(
				'option',
				'height',
				 get_calendar_height());
	} 
	
	
	
</script>
<%@ include file="newFooter.jsp"%>
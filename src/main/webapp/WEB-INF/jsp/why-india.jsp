<%@ include file="newHeader.jsp"%></section>
	<!-- Banner Starts Here -->
	<div class="inner-banner">
		<img src="images/banners/sub-banner.jpg" alt="Why India"
			class="img-responsive">
	</div>
	<!-- Banner Ends Here -->

	<!-- Short Details Starts Here -->
	<div class="short-details">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="title">
						<h1>Why India</h1>
						<ul class="paginator">
							<li><a href="/">Home</a></li>
							<li><a>Why India</a></li>
						</ul>
					</div>
				</div>
				<!-- End Column -->
			</div>
			<!-- End Row -->
		</div>
		<!-- End Container -->
	</div>
	<!-- Short Details Ends Here -->

	<!-- Content Section Starts Here -->
	<div class="content-area">
		<div class="container">

			<div class="main-contentarea">
				<p>India is most sought after and fast emerging as a focal point
					of world medical tourism. It's like the world is converging on
					India for getting affordable and effective treatment packages;
					ranging from Cardiology to Organ transplants, Neuro &amp; Spine
					surgeries to Minimally Invasive Robotic Surgery, Bariatric surgery
					to Cosmetic treatments. Over the years, India has grown to become a
					top-notch destination for medical value travel because it scores
					high over a range of factors that determines the overall quality of
					care. Imagine a complex surgical procedure being done in a world
					class global hospital by acclaimed medical specialists at a fifth
					to tenth of what it normally takes! That�'s India.</p>
				<h3>
					<span>India:</span>A Premier Healthcare Destination
				</h3>
				<p>Why India? The basics for successful healthcare solutions:</p>
				<ul>
					<li><span><strong>Facilities</strong><br>
						<p>The high-end healthcare system in India is as good as the
								best in the world. India maintains not only a robust
								accreditation system but also a large number of accredited
								facilities. There are 347 NABH accredited hospitals that match
								any global infrastructure. India also has 23 JCI (Joint
								Commission International) accredited hospitals and compares well
								with other countries in Asia. These set of approved hospitals in
								India can provide care at par or above global standards.</p></span></li>
					<li><span><strong>Cutting edge technologies</strong><br>
						<p>Cutting edge technology to support medical diagnostics and
								medical procedures are employed by specialists in medical
								facilities. All recognized hospitals have invested a lot in
								supportive technology and operative techniques. Complicated
								heart surgeries, cancer care and surgeries, neuro and even
								general surgeries require high-end technology to continually
								better outcomes, minimize complications, enable faster recovery
								and reduce length of hospital stay. The recent advancements in
								robotic surgeries, radiation surgery or radio therapies with
								cyberknife stereotactic options, IMRT / IGRT, transplant support
								systems, advanced neuro and spinal options are all available in
								India. India�s medical management and acclaimed specialists are
								quite comfortable in challenging themselves to new frontiers to
								provide solutions, always building on their expertise.</p></span></li>
					<li><span><strong>Finest doctors</strong><br>
						<p>India has not only hospitals with world-class facilities
								but skilled world-class doctors and medical personnel too. The
								country has the largest pool of doctors and paramedics in South
								Asia. Many of them have established their credentials as leaders
								around the world. With a large number of doctors, there is a
								high level of competency and capability in adoption of newer
								technologies and innovation and fresh treatment methods.
								Communicate and talk to the doctors in the accredited facilities
								prior to your visit and they will study your needs and customize
								the treatment for you!</p></span></li>
					<li><span><strong>Monetary Savings</strong><br>
						<p>When Quality comes at an affordable cost it is an
								unbeatable advantage and this union of highest quality and cost
								advantage is unique for India. The benefit is unimaginable when
								it comes to major treatments such as for leukaemia where the
								difference in cost is 10 to 20 times. For other treatments, it
								could be anything from a fifth to a tenth when compared to
								Western countries. The huge number of people who step into India
								from other countries do not do so for cheap healthcare but for
								quality healthcare at an affordable cost. They are not
								compromised at any level, but regain health at a fraction of the
								cost.</p></span></li>
					<li><span><strong>Fast Track � Zero Waiting
								Time</strong><br>
						<p>Quick and immediate attention for surgeries and all
								interventions are assured in India. Getting an appointment for
								bypass surgery or a planned angioplasty in certain countries
								takes almost 3-6 months. And there these treatments are very
								costly too. It�s zero waiting time in India for any procedure,
								be it heart surgery, kidney care, cancer treatment, neuro-spinal
								procedure, knee/hip/joint replacements, dental, cosmetic
								surgeries, weight loss surgery etc.</p></span></li>
					<li><span><strong>Feeling the pulse</strong><br>
						<p>For greater understanding between patients and healthcare
								personnel, the warmth and hospitality of Indian hospitals is a
								big factor in choosing India as a healthcare destination. Among
								the top medical destinations of the world, India has the highest
								percentage of English language speaking people. Amidst the
								variety of culture and traditions, if there is one thing that is
								common in India, that is the English language. If other language
								options are essential, there are expert interpreters who will be
								arranged by the hospitals. All leading to reassuring hospitality
								and great after care.</p></span></li>
				</ul>
			</div>
			<!-- End Accordion Container -->
		</div>
		<!-- End Container -->
	</div>
	<!-- Content Section Ends Here -->

	<%@ include file="newFooter.jsp"%>